/* pk-save.pk - `save' command.  */

/* Copyright (C) 2020, 2021, 2022, 2023, 2024 Jose E. Marchesi */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var pk_save_from = 0#B;
var pk_save_size = 0#B;
var pk_save_append = 0;
var pk_save_verbose = 0;

pk_help_add_topic
  :entry Poke_HelpEntry {
          category = "commands",
          topic = "save",
          summary = "save a range of an IO space to a file",
          description = format ("
Get the bytes corresponding to a given mapped value and create a
memory IO space that contains them.

Synopsis:

  save [:ios IOS] [:from OFFSET] [:size OFFSET] [:file STRING] \\
       [:append BOOL] [:verbose BOOL]

Arguments:

  :ios (int)
         IOS id from where to get the data.  Defaults to the current
         IO space.

  :from (offset)
         Beginning of the data to save, in the selected IO space.
         Defaults to %v.

  :size (offset)

         Size of the data to save.  This argument is truncated to
         bytes.  Defaults to %v.

  :file (string)
         Name of the file where to save the data.

  :append (bool)
         By default the output file is truncated if it already
         exists. If this argument is true, then the data is instead
         appended at the end of the file.  Defaults to %i32d.

  :verbose (bool)
         Be verbose.  Defaults to %i32d.

If the given value is not mapped `extract' raises E_map.

See `.doc save' for more information.",
                                pk_save_from, pk_save_size,
                                pk_save_append, pk_save_verbose)
         };

fun save = (int<32> ios = get_ios,
            string file = "",
            offset<int<64>,b> from = pk_save_from,
            offset<int<64>,b> size = pk_save_size,
            int<32> append = pk_save_append,
            int<32> verbose = pk_save_verbose) void:
{
  if (file == "" || size == 0#B)
    return;

  ios_save_bytes :ios ios :file file :from from :size size
                 :append_p append;
}
