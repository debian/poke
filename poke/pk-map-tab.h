/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_PK_MAP_TAB_PK_MAP_TAB_TAB_H_INCLUDED
# define YY_PK_MAP_TAB_PK_MAP_TAB_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef PK_MAP_TAB_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define PK_MAP_TAB_DEBUG 1
#  else
#   define PK_MAP_TAB_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define PK_MAP_TAB_DEBUG 1
# endif /* ! defined YYDEBUG */
#endif  /* ! defined PK_MAP_TAB_DEBUG */
#if PK_MAP_TAB_DEBUG
extern int pk_map_tab_debug;
#endif

/* Token kinds.  */
#ifndef PK_MAP_TAB_TOKENTYPE
# define PK_MAP_TAB_TOKENTYPE
  enum pk_map_tab_tokentype
  {
    PK_MAP_TAB_EMPTY = -2,
    PK_MAP_TAB_EOF = 0,            /* "end of file"  */
    PK_MAP_TAB_error = 256,        /* error  */
    PK_MAP_TAB_UNDEF = 257,        /* "invalid token"  */
    ERR = 258,                     /* ERR  */
    SEP = 259,                     /* SEP  */
    ENTRY = 260,                   /* ENTRY  */
    TAG = 261,                     /* TAG  */
    DATA = 262                     /* DATA  */
  };
  typedef enum pk_map_tab_tokentype pk_map_tab_token_kind_t;
#endif

/* Value type.  */
#if ! defined PK_MAP_TAB_STYPE && ! defined PK_MAP_TAB_STYPE_IS_DECLARED
union PK_MAP_TAB_STYPE
{
#line 265 "pk-map-tab.y"

  char *string;
  int integer;
  struct pk_map_parsed_map *map;
  struct pk_map_parsed_entry *entry;
  struct tagged_value *tagged_value;

#line 87 "pk-map-tab.h"

};
typedef union PK_MAP_TAB_STYPE PK_MAP_TAB_STYPE;
# define PK_MAP_TAB_STYPE_IS_TRIVIAL 1
# define PK_MAP_TAB_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined PK_MAP_TAB_LTYPE && ! defined PK_MAP_TAB_LTYPE_IS_DECLARED
typedef struct PK_MAP_TAB_LTYPE PK_MAP_TAB_LTYPE;
struct PK_MAP_TAB_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define PK_MAP_TAB_LTYPE_IS_DECLARED 1
# define PK_MAP_TAB_LTYPE_IS_TRIVIAL 1
#endif




int pk_map_tab_parse (struct pk_map_parser *map_parser);


#endif /* !YY_PK_MAP_TAB_PK_MAP_TAB_TAB_H_INCLUDED  */
