/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_JITTER_JITTER_JITTER_JITTER_ROUTINE_PARSER_H_INCLUDED
# define YY_JITTER_JITTER_JITTER_JITTER_ROUTINE_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef JITTER_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define JITTER_DEBUG 1
#  else
#   define JITTER_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define JITTER_DEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined JITTER_DEBUG */
#if JITTER_DEBUG
extern int jitter_debug;
#endif
/* "%code requires" blocks.  */
#line 203 "../../jitter/jitter/jitter-routine.y"

  #include <stdio.h>

  #include <jitter/jitter.h>
  #include <jitter/jitter-instruction.h>
  #include <jitter/jitter-mutable-routine.h>
  #include <jitter/jitter-vm.h>

  /* A structure whose pointer is passed to the parser function. */
  struct parser_arg
  {
    /* In case of parse error we will need to return a pointer to a copy of this
       to the user; this is useful to store error details. */
    struct jitter_routine_parse_error parse_status;

    /* VM-dependent data for the routine's VM.  Not modified. */
    const struct jitter_vm *vm;

    /* The routine which is being parsed, allowed to contain no instructions at
       the beginning of parsing. */
// FIXME: in case of parse errors restore the routine to its initial state. /////////////////////////////
    struct jitter_mutable_routine *routine;
  };

  /* Parse a routine for the pointed VM from a file or a string in memory,
     adding code to the pointed VM routine.
     These functions work of course on any VM, but are slightly inconvenient for
     the user to call directly.  For this reason they are wrapped in the vm1.c
     template into VM-specific functions not requiring a VM struct pointer.

     The result is NULL on success, and on error a pointer to a fresh struct of
     type struct jitter_routine_parse_error containing details about the error,
     to be destroyed by the user via jitter_routine_parse_error_destroy .

     In case of parse error the routine is *not* modified. */
  struct jitter_routine_parse_error*
  jitter_parse_mutable_routine_from_file_star (FILE *input_file,
                                               struct jitter_mutable_routine *p,
                                               const struct jitter_vm *vm)
    __attribute__ ((nonnull (1, 2, 3)));
  struct jitter_routine_parse_error*
  jitter_parse_mutable_routine_from_file (const char *input_file_name,
                                          struct jitter_mutable_routine *p,
                                          const struct jitter_vm *vm)
    __attribute__ ((nonnull (1, 2, 3)));
  struct jitter_routine_parse_error*
  jitter_parse_mutable_routine_from_string (const char *string,
                                            struct jitter_mutable_routine *p,
                                            const struct jitter_vm *vm)
    __attribute__ ((nonnull (1, 2, 3)));

#line 109 "../../jitter/jitter/jitter-routine-parser.h"

/* Token kinds.  */
#ifndef JITTER_TOKENTYPE
# define JITTER_TOKENTYPE
  enum jitter_tokentype
  {
    JITTER_EMPTY = -2,
    JITTER_EOF = 0,                /* "end of file"  */
    JITTER_error = 256,            /* error  */
    JITTER_UNDEF = 257,            /* "invalid token"  */
    INSTRUCTION_NAME = 258,        /* INSTRUCTION_NAME  */
    REGISTER = 259,                /* REGISTER  */
    LABEL_LITERAL = 260,           /* LABEL_LITERAL  */
    LABEL = 261,                   /* LABEL  */
    COMMA = 262,                   /* COMMA  */
    OPEN_PARENS = 263,             /* OPEN_PARENS  */
    CLOSE_PARENS = 264,            /* CLOSE_PARENS  */
    SIGNED_BINARY_LITERAL = 265,   /* SIGNED_BINARY_LITERAL  */
    SIGNED_OCTAL_LITERAL = 266,    /* SIGNED_OCTAL_LITERAL  */
    SIGNED_DECIMAL_LITERAL = 267,  /* SIGNED_DECIMAL_LITERAL  */
    SIGNED_HEXADECIMAL_LITERAL = 268, /* SIGNED_HEXADECIMAL_LITERAL  */
    UNSIGNED_BINARY_LITERAL = 269, /* UNSIGNED_BINARY_LITERAL  */
    UNSIGNED_OCTAL_LITERAL = 270,  /* UNSIGNED_OCTAL_LITERAL  */
    UNSIGNED_DECIMAL_LITERAL = 271, /* UNSIGNED_DECIMAL_LITERAL  */
    UNSIGNED_HEXADECIMAL_LITERAL = 272, /* UNSIGNED_HEXADECIMAL_LITERAL  */
    BYTESPERWORD = 273,            /* BYTESPERWORD  */
    LGBYTESPERWORD = 274,          /* LGBYTESPERWORD  */
    BITSPERWORD = 275,             /* BITSPERWORD  */
    JITTER_INT_MIN_ = 276,         /* JITTER_INT_MIN_  */
    JITTER_INT_MAX_ = 277,         /* JITTER_INT_MAX_  */
    JITTER_UINT_MAX_ = 278,        /* JITTER_UINT_MAX_  */
    UNSIGNED_MINUS = 279,          /* UNSIGNED_MINUS  */
    UNSIGNED_NOT = 280,            /* UNSIGNED_NOT  */
    UNSIGNED_PLUS = 281,           /* UNSIGNED_PLUS  */
    UNSIGNED_OR = 282,             /* UNSIGNED_OR  */
    UNSIGNED_XOR = 283,            /* UNSIGNED_XOR  */
    UNSIGNED_AND = 284,            /* UNSIGNED_AND  */
    TIMES = 285,                   /* TIMES  */
    UNSIGNED_TIMES = 286,          /* UNSIGNED_TIMES  */
    DIV = 287,                     /* DIV  */
    UNSIGNED_DIV = 288,            /* UNSIGNED_DIV  */
    MOD = 289,                     /* MOD  */
    UNSIGNED_MOD = 290,            /* UNSIGNED_MOD  */
    PREFIX_NEGATION = 291,         /* PREFIX_NEGATION  */
    POWER = 292,                   /* POWER  */
    UNSIGNED_POWER = 293,          /* UNSIGNED_POWER  */
    INVALID_TOKEN = 294            /* INVALID_TOKEN  */
  };
  typedef enum jitter_tokentype jitter_token_kind_t;
#endif
/* Token kinds.  */
#define JITTER_EMPTY -2
#define JITTER_EOF 0
#define JITTER_error 256
#define JITTER_UNDEF 257
#define INSTRUCTION_NAME 258
#define REGISTER 259
#define LABEL_LITERAL 260
#define LABEL 261
#define COMMA 262
#define OPEN_PARENS 263
#define CLOSE_PARENS 264
#define SIGNED_BINARY_LITERAL 265
#define SIGNED_OCTAL_LITERAL 266
#define SIGNED_DECIMAL_LITERAL 267
#define SIGNED_HEXADECIMAL_LITERAL 268
#define UNSIGNED_BINARY_LITERAL 269
#define UNSIGNED_OCTAL_LITERAL 270
#define UNSIGNED_DECIMAL_LITERAL 271
#define UNSIGNED_HEXADECIMAL_LITERAL 272
#define BYTESPERWORD 273
#define LGBYTESPERWORD 274
#define BITSPERWORD 275
#define JITTER_INT_MIN_ 276
#define JITTER_INT_MAX_ 277
#define JITTER_UINT_MAX_ 278
#define UNSIGNED_MINUS 279
#define UNSIGNED_NOT 280
#define UNSIGNED_PLUS 281
#define UNSIGNED_OR 282
#define UNSIGNED_XOR 283
#define UNSIGNED_AND 284
#define TIMES 285
#define UNSIGNED_TIMES 286
#define DIV 287
#define UNSIGNED_DIV 288
#define MOD 289
#define UNSIGNED_MOD 290
#define PREFIX_NEGATION 291
#define POWER 292
#define UNSIGNED_POWER 293
#define INVALID_TOKEN 294

/* Value type.  */
#if ! defined JITTER_STYPE && ! defined JITTER_STYPE_IS_DECLARED
union JITTER_STYPE
{
#line 255 "../../jitter/jitter/jitter-routine.y"

  union jitter_word literal;

#line 211 "../../jitter/jitter/jitter-routine-parser.h"

};
typedef union JITTER_STYPE JITTER_STYPE;
# define JITTER_STYPE_IS_TRIVIAL 1
# define JITTER_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined JITTER_LTYPE && ! defined JITTER_LTYPE_IS_DECLARED
typedef struct JITTER_LTYPE JITTER_LTYPE;
struct JITTER_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define JITTER_LTYPE_IS_DECLARED 1
# define JITTER_LTYPE_IS_TRIVIAL 1
#endif




int jitter_parse (struct parser_arg *parser_arg, void* jitter_scanner);


#endif /* !YY_JITTER_JITTER_JITTER_JITTER_ROUTINE_PARSER_H_INCLUDED  */
