/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Substitute the type names.  */
#define YYSTYPE         JITTER_STYPE
#define YYLTYPE         JITTER_LTYPE
/* Substitute the variable and function names.  */
#define yyparse         jitter_parse
#define yylex           jitter_lex
#define yyerror         jitter_error
#define yydebug         jitter_debug
#define yynerrs         jitter_nerrs

/* First part of user prologue.  */
#line 23 "../../jitter/jitter/jitter-routine.y"

  #include <stdio.h>
  #include <limits.h>
  #include <jitter/jitter.h>
  #include <jitter/jitter-instruction.h>
  #include <jitter/jitter-mutable-routine.h>

  #include <jitter/jitter-routine-parser.h>
  #include <jitter/jitter-routine-scanner.h>
  #include <jitter/jitter-malloc.h>
  #include <jitter/jitter-string.h>
  #include <jitter/jitter-fatal.h>

  /* This is required for Bison parsers, called in case of parse errors for
     "reporting"; instead of reporting I use it to record the error position. */
  static void // FIXME: I think that this is required for Bison parsers.
  jitter_error (YYLTYPE *locp,
                struct parser_arg *parser_arg,
                yyscan_t scanner, const char *message);

/* JITTER_HANDLE_STATUS: expand to a statement that, according to the
   status (of type enum jitter_routine_edit_status) either does nothing or
   registers the current parse error details in parser_arg->parse_status .  This
   can be called from both actions and the jitter_error function.  In the case
   of actions we would need to execute YYABORT right after this, but that would
   not work in jitter_error ; hence the need for
   JITTER_HANDLE_STATUS_WITHOUT_YYABORT. */
#define JITTER_HANDLE_STATUS_WITHOUT_YYABORT(the_status)                        \
  do                                                                            \
    {                                                                           \
      /* jitter_scanner is visible from this, since the expansion is in         \
         actions. */                                                            \
      enum jitter_routine_edit_status                                           \
        _jitter_handle_edit_status_status = (the_status);                       \
      switch (_jitter_handle_edit_status_status)                                \
        {                                                                       \
        case jitter_routine_edit_status_success:                                \
          /* Do nothing: there is no error and parsing should continue. */      \
          break;                                                                \
        case jitter_routine_edit_status_label_defined_twice:                    \
        case jitter_routine_edit_status_too_many_parameters:                    \
        case jitter_routine_edit_status_last_instruction_incomplete:            \
        case jitter_routine_edit_status_invalid_instruction:                    \
        case jitter_routine_edit_status_invalid_register:                       \
        case jitter_routine_edit_status_register_class_mismatch:                \
        case jitter_routine_edit_status_nonexisting_register_class:             \
        case jitter_routine_edit_status_invalid_parameter_kind:                 \
        case jitter_routine_edit_status_other_parse_error:                      \
          /* Update the parse status and location so that at parser exit the    \
             caller will find precise information. */                           \
          parser_arg->parse_status.status = _jitter_handle_edit_status_status;  \
          parser_arg->parse_status.error_line_no                                \
            = jitter_get_lineno (jitter_scanner);                               \
          parser_arg->parse_status.error_token_text                             \
            = jitter_clone_string (jitter_get_text (jitter_scanner));           \
          /* Here we would call YYABORT. */                                     \
          break;                                                                \
        default:                                                                \
          jitter_fatal ("unimplemented or invalid case %i",                     \
                        _jitter_handle_edit_status_status);                     \
        };                                                                      \
    }                                                                           \
  while (false)
#define JITTER_HANDLE_STATUS(the_status)                     \
  do                                                         \
    {                                                        \
      /* First do what JITTER_HANDLE_STATUS_WITHOUT_YYABORT  \
         does... */                                          \
      enum jitter_routine_edit_status                        \
        _jitter_handle_edit_status_status_y = (the_status);  \
      JITTER_HANDLE_STATUS_WITHOUT_YYABORT                   \
         (_jitter_handle_edit_status_status_y);              \
      /* ...Then use YYABORT in case of error. */            \
      if (_jitter_handle_edit_status_status_y                \
          != jitter_routine_edit_status_success)             \
        YYABORT;                                             \
    }                                                        \
  while (false)

/* Set the given lvalue, with the %type whose definition is union
   jitter_literal, converting jitter_get_text (jitter_scanner) using the given
   function.  These are useful to convert to a specified base, in the case of
   signed and unsigned literals.  The lvalues will be either
   $$.fixnum or $$.ufixnum , and the functions to
   call will be jitter_strtoll or jitter_strtoull . */
#define JITTER_SET_BINARY(lvalue, function)           \
  do                                                  \
    {                                                 \
      char *text = jitter_get_text (jitter_scanner);  \
      * strchr (text, 'b') = '0';                     \
      lvalue = function (text, NULL, 2);              \
    }                                                 \
  while (false)
#define JITTER_SET_OCTAL(lvalue, function)            \
  do                                                  \
    {                                                 \
      char *text = jitter_get_text (jitter_scanner);  \
      char *oindex = strchr (text, 'o');              \
      if (oindex != NULL)                             \
        * oindex = '0';                               \
      lvalue = function (text, NULL, 8);              \
    }                                                 \
  while (false)
#define JITTER_SET_DECIMAL(lvalue, function)          \
  do                                                  \
    {                                                 \
      char *text = jitter_get_text (jitter_scanner);  \
      lvalue = function (text, NULL, 10);             \
    }                                                 \
  while (false)
#define JITTER_SET_HEXADECIMAL(lvalue, function)      \
  do                                                  \
    {                                                 \
      char *text = jitter_get_text (jitter_scanner);  \
      lvalue = function (text, NULL, 16);             \
    }                                                 \
  while (false)

/* Expand to an assignment of the given field of the given result as an
   operation involving the given infix operator, and the fields with the same
   names as the field of the result of the two given operands.  This is intended
   for uses such as JITTER_SET_OPERATION(fixnum, $$, $1, +, $3)
   . */
#define JITTER_SET_OPERATION(field, result, operand1, operator, operand2)  \
  do                                                                       \
    {                                                                      \
      result.field = operand1.field operator operand2.field;               \
    }                                                                      \
  while (false)


static jitter_int
jitter_power (jitter_int base, jitter_int exponent)
{
  if (exponent < 0)
    return 1;
  jitter_int res = 1;
  for (; exponent > 0; exponent --)
    res *= base;
  return res;
}
static jitter_uint
jitter_upower (jitter_uint base, jitter_uint exponent)
{
  jitter_uint res = 1;
  for (; exponent > 0; exponent --)
    res *= base;
  return res;
}

#line 229 "../../jitter/jitter/jitter-routine-parser.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "jitter-routine-parser.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_INSTRUCTION_NAME = 3,           /* INSTRUCTION_NAME  */
  YYSYMBOL_REGISTER = 4,                   /* REGISTER  */
  YYSYMBOL_LABEL_LITERAL = 5,              /* LABEL_LITERAL  */
  YYSYMBOL_LABEL = 6,                      /* LABEL  */
  YYSYMBOL_COMMA = 7,                      /* COMMA  */
  YYSYMBOL_OPEN_PARENS = 8,                /* OPEN_PARENS  */
  YYSYMBOL_CLOSE_PARENS = 9,               /* CLOSE_PARENS  */
  YYSYMBOL_SIGNED_BINARY_LITERAL = 10,     /* SIGNED_BINARY_LITERAL  */
  YYSYMBOL_SIGNED_OCTAL_LITERAL = 11,      /* SIGNED_OCTAL_LITERAL  */
  YYSYMBOL_SIGNED_DECIMAL_LITERAL = 12,    /* SIGNED_DECIMAL_LITERAL  */
  YYSYMBOL_SIGNED_HEXADECIMAL_LITERAL = 13, /* SIGNED_HEXADECIMAL_LITERAL  */
  YYSYMBOL_UNSIGNED_BINARY_LITERAL = 14,   /* UNSIGNED_BINARY_LITERAL  */
  YYSYMBOL_UNSIGNED_OCTAL_LITERAL = 15,    /* UNSIGNED_OCTAL_LITERAL  */
  YYSYMBOL_UNSIGNED_DECIMAL_LITERAL = 16,  /* UNSIGNED_DECIMAL_LITERAL  */
  YYSYMBOL_UNSIGNED_HEXADECIMAL_LITERAL = 17, /* UNSIGNED_HEXADECIMAL_LITERAL  */
  YYSYMBOL_BYTESPERWORD = 18,              /* BYTESPERWORD  */
  YYSYMBOL_LGBYTESPERWORD = 19,            /* LGBYTESPERWORD  */
  YYSYMBOL_BITSPERWORD = 20,               /* BITSPERWORD  */
  YYSYMBOL_JITTER_INT_MIN_ = 21,           /* JITTER_INT_MIN_  */
  YYSYMBOL_JITTER_INT_MAX_ = 22,           /* JITTER_INT_MAX_  */
  YYSYMBOL_JITTER_UINT_MAX_ = 23,          /* JITTER_UINT_MAX_  */
  YYSYMBOL_UNSIGNED_MINUS = 24,            /* UNSIGNED_MINUS  */
  YYSYMBOL_UNSIGNED_NOT = 25,              /* UNSIGNED_NOT  */
  YYSYMBOL_UNSIGNED_PLUS = 26,             /* UNSIGNED_PLUS  */
  YYSYMBOL_UNSIGNED_OR = 27,               /* UNSIGNED_OR  */
  YYSYMBOL_UNSIGNED_XOR = 28,              /* UNSIGNED_XOR  */
  YYSYMBOL_UNSIGNED_AND = 29,              /* UNSIGNED_AND  */
  YYSYMBOL_TIMES = 30,                     /* TIMES  */
  YYSYMBOL_UNSIGNED_TIMES = 31,            /* UNSIGNED_TIMES  */
  YYSYMBOL_DIV = 32,                       /* DIV  */
  YYSYMBOL_UNSIGNED_DIV = 33,              /* UNSIGNED_DIV  */
  YYSYMBOL_MOD = 34,                       /* MOD  */
  YYSYMBOL_UNSIGNED_MOD = 35,              /* UNSIGNED_MOD  */
  YYSYMBOL_PREFIX_NEGATION = 36,           /* PREFIX_NEGATION  */
  YYSYMBOL_POWER = 37,                     /* POWER  */
  YYSYMBOL_UNSIGNED_POWER = 38,            /* UNSIGNED_POWER  */
  YYSYMBOL_INVALID_TOKEN = 39,             /* INVALID_TOKEN  */
  YYSYMBOL_YYACCEPT = 40,                  /* $accept  */
  YYSYMBOL_routine = 41,                   /* routine  */
  YYSYMBOL_instruction_or_label = 42,      /* instruction_or_label  */
  YYSYMBOL_instruction = 43,               /* instruction  */
  YYSYMBOL_label = 44,                     /* label  */
  YYSYMBOL_instruction_name = 45,          /* instruction_name  */
  YYSYMBOL_arguments = 46,                 /* arguments  */
  YYSYMBOL_after_one_argument = 47,        /* after_one_argument  */
  YYSYMBOL_int_expression = 48,            /* int_expression  */
  YYSYMBOL_argument = 49                   /* argument  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined JITTER_LTYPE_IS_TRIVIAL && JITTER_LTYPE_IS_TRIVIAL \
             && defined JITTER_STYPE_IS_TRIVIAL && JITTER_STYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   105

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  40
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  10
/* YYNRULES -- Number of rules.  */
#define YYNRULES  45
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  65

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   294


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39
};

#if JITTER_DEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   287,   287,   289,   293,   294,   298,   302,   312,   318,
     320,   323,   325,   329,   331,   333,   335,   337,   339,   341,
     343,   345,   346,   347,   348,   349,   350,   351,   352,   355,
     357,   359,   361,   363,   365,   367,   369,   371,   374,   376,
     378,   380,   382,   387,   390,   394
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if JITTER_DEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "INSTRUCTION_NAME",
  "REGISTER", "LABEL_LITERAL", "LABEL", "COMMA", "OPEN_PARENS",
  "CLOSE_PARENS", "SIGNED_BINARY_LITERAL", "SIGNED_OCTAL_LITERAL",
  "SIGNED_DECIMAL_LITERAL", "SIGNED_HEXADECIMAL_LITERAL",
  "UNSIGNED_BINARY_LITERAL", "UNSIGNED_OCTAL_LITERAL",
  "UNSIGNED_DECIMAL_LITERAL", "UNSIGNED_HEXADECIMAL_LITERAL",
  "BYTESPERWORD", "LGBYTESPERWORD", "BITSPERWORD", "JITTER_INT_MIN_",
  "JITTER_INT_MAX_", "JITTER_UINT_MAX_", "UNSIGNED_MINUS", "UNSIGNED_NOT",
  "UNSIGNED_PLUS", "UNSIGNED_OR", "UNSIGNED_XOR", "UNSIGNED_AND", "TIMES",
  "UNSIGNED_TIMES", "DIV", "UNSIGNED_DIV", "MOD", "UNSIGNED_MOD",
  "PREFIX_NEGATION", "POWER", "UNSIGNED_POWER", "INVALID_TOKEN", "$accept",
  "routine", "instruction_or_label", "instruction", "label",
  "instruction_name", "arguments", "after_one_argument", "int_expression",
  "argument", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-46)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int8 yypact[] =
{
     -46,     8,   -46,   -46,   -46,   -46,   -46,   -46,    47,   -46,
     -46,    65,   -46,   -46,   -46,   -46,   -46,   -46,   -46,   -46,
     -46,   -46,   -46,   -46,   -46,   -46,    65,    65,   -46,    67,
      10,    12,   -25,   -25,    65,    65,    65,    65,    65,    65,
      65,    65,    65,    65,    65,    65,    65,    47,   -46,   -46,
     -28,   -28,   -28,   -28,   -25,   -25,   -25,   -25,   -25,   -25,
     -25,   -25,   -25,    10,   -46
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       2,     0,     1,     8,     7,     3,     4,     5,     9,    45,
      44,     0,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,     0,     0,     6,    43,
      11,     0,    28,    37,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    10,    27,
      33,    32,    39,    40,    38,    29,    34,    30,    35,    31,
      36,    41,    42,    11,    12
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -46,   -46,   -46,   -46,   -46,   -46,   -46,   -45,   -11,   -27
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
       0,     1,     5,     6,     7,     8,    28,    48,    29,    30
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int8 yytable[] =
{
      31,    38,    39,    40,    41,    42,    43,    44,     2,    45,
      46,     3,    45,    46,     4,    32,    33,    47,    64,     0,
      63,    49,     0,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    34,     0,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,     0,    45,
      46,     9,    10,     0,     0,    11,     0,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    11,     0,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    34,     0,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,     0,    45,    46
};

static const yytype_int8 yycheck[] =
{
      11,    29,    30,    31,    32,    33,    34,    35,     0,    37,
      38,     3,    37,    38,     6,    26,    27,     7,    63,    -1,
      47,     9,    -1,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    24,    -1,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    -1,    37,
      38,     4,     5,    -1,    -1,     8,    -1,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,     8,    -1,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    24,    -1,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    -1,    37,    38
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,    41,     0,     3,     6,    42,    43,    44,    45,     4,
       5,     8,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    46,    48,
      49,    48,    48,    48,    24,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    37,    38,     7,    47,     9,
      48,    48,    48,    48,    48,    48,    48,    48,    48,    48,
      48,    48,    48,    49,    47
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    40,    41,    41,    42,    42,    43,    44,    45,    46,
      46,    47,    47,    48,    48,    48,    48,    48,    48,    48,
      48,    48,    48,    48,    48,    48,    48,    48,    48,    48,
      48,    48,    48,    48,    48,    48,    48,    48,    48,    48,
      48,    48,    48,    49,    49,    49
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     2,     1,     1,     2,     1,     1,     0,
       2,     0,     3,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     3,     2,     3,
       3,     3,     3,     3,     3,     3,     3,     2,     3,     3,
       3,     3,     3,     1,     1,     1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = JITTER_EMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == JITTER_EMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (&yylloc, parser_arg, jitter_scanner, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use JITTER_error or JITTER_UNDEF. */
#define YYERRCODE JITTER_UNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if JITTER_DEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YYLOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YYLOCATION_PRINT

#  if defined YY_LOCATION_PRINT

   /* Temporary convenience wrapper in case some people defined the
      undocumented and private YY_LOCATION_PRINT macros.  */
#   define YYLOCATION_PRINT(File, Loc)  YY_LOCATION_PRINT(File, *(Loc))

#  elif defined JITTER_LTYPE_IS_TRIVIAL && JITTER_LTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  int res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
}

#   define YYLOCATION_PRINT  yy_location_print_

    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT(File, Loc)  YYLOCATION_PRINT(File, &(Loc))

#  else

#   define YYLOCATION_PRINT(File, Loc) ((void) 0)
    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT  YYLOCATION_PRINT

#  endif
# endif /* !defined YYLOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, Location, parser_arg, jitter_scanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct parser_arg *parser_arg, void* jitter_scanner)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  YY_USE (yylocationp);
  YY_USE (parser_arg);
  YY_USE (jitter_scanner);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct parser_arg *parser_arg, void* jitter_scanner)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  YYLOCATION_PRINT (yyo, yylocationp);
  YYFPRINTF (yyo, ": ");
  yy_symbol_value_print (yyo, yykind, yyvaluep, yylocationp, parser_arg, jitter_scanner);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp,
                 int yyrule, struct parser_arg *parser_arg, void* jitter_scanner)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)],
                       &(yylsp[(yyi + 1) - (yynrhs)]), parser_arg, jitter_scanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, parser_arg, jitter_scanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !JITTER_DEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !JITTER_DEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, struct parser_arg *parser_arg, void* jitter_scanner)
{
  YY_USE (yyvaluep);
  YY_USE (yylocationp);
  YY_USE (parser_arg);
  YY_USE (jitter_scanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (struct parser_arg *parser_arg, void* jitter_scanner)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined JITTER_LTYPE_IS_TRIVIAL && JITTER_LTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

    /* The location stack: array, bottom, top.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls = yylsa;
    YYLTYPE *yylsp = yyls;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[3];



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = JITTER_EMPTY; /* Cause a token to be read.  */

  yylsp[0] = yylloc;
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yyls1, yysize * YYSIZEOF (*yylsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
        yyls = yyls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == JITTER_EMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, &yylloc, jitter_scanner);
    }

  if (yychar <= JITTER_EOF)
    {
      yychar = JITTER_EOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == JITTER_error)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = JITTER_UNDEF;
      yytoken = YYSYMBOL_YYerror;
      yyerror_range[1] = yylloc;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;

  /* Discard the shifted token.  */
  yychar = JITTER_EMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  yyerror_range[1] = yyloc;
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 7: /* label: LABEL  */
#line 302 "../../jitter/jitter/jitter-routine.y"
        { char *label = jitter_get_text (jitter_scanner);
          label [strlen (label) - 1] = '\0';  /* Remove the trailing colon. */
          JITTER_HANDLE_STATUS
             (jitter_mutable_routine_append_symbolic_label_safe
                 (NULL,
                  parser_arg->routine,
                  label)); }
#line 1451 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 8: /* instruction_name: INSTRUCTION_NAME  */
#line 312 "../../jitter/jitter/jitter-routine.y"
                   { char *name = jitter_get_text (jitter_scanner);
                     JITTER_HANDLE_STATUS
                        (jitter_mutable_routine_append_instruction_name_safe
                            (parser_arg->routine, name)); }
#line 1460 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 13: /* int_expression: SIGNED_BINARY_LITERAL  */
#line 329 "../../jitter/jitter/jitter-routine.y"
                              { JITTER_SET_BINARY((yyval.literal).fixnum,
                                                  jitter_strtoll); }
#line 1467 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 14: /* int_expression: SIGNED_OCTAL_LITERAL  */
#line 331 "../../jitter/jitter/jitter-routine.y"
                              { JITTER_SET_OCTAL((yyval.literal).fixnum,
                                                 jitter_strtoll); }
#line 1474 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 15: /* int_expression: SIGNED_DECIMAL_LITERAL  */
#line 333 "../../jitter/jitter/jitter-routine.y"
                              { JITTER_SET_DECIMAL((yyval.literal).fixnum,
                                                   jitter_strtoll); }
#line 1481 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 16: /* int_expression: SIGNED_HEXADECIMAL_LITERAL  */
#line 335 "../../jitter/jitter/jitter-routine.y"
                              { JITTER_SET_HEXADECIMAL((yyval.literal).fixnum,
                                                       jitter_strtoll); }
#line 1488 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 17: /* int_expression: UNSIGNED_BINARY_LITERAL  */
#line 337 "../../jitter/jitter/jitter-routine.y"
                              { JITTER_SET_BINARY((yyval.literal).ufixnum,
                                                  jitter_strtoull); }
#line 1495 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 18: /* int_expression: UNSIGNED_OCTAL_LITERAL  */
#line 339 "../../jitter/jitter/jitter-routine.y"
                              { JITTER_SET_OCTAL((yyval.literal).ufixnum,
                                                 jitter_strtoull); }
#line 1502 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 19: /* int_expression: UNSIGNED_DECIMAL_LITERAL  */
#line 341 "../../jitter/jitter/jitter-routine.y"
                              { JITTER_SET_DECIMAL((yyval.literal).ufixnum,
                                                   jitter_strtoull); }
#line 1509 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 20: /* int_expression: UNSIGNED_HEXADECIMAL_LITERAL  */
#line 343 "../../jitter/jitter/jitter-routine.y"
                              { JITTER_SET_HEXADECIMAL((yyval.literal).ufixnum,
                                                       jitter_strtoull); }
#line 1516 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 21: /* int_expression: BYTESPERWORD  */
#line 345 "../../jitter/jitter/jitter-routine.y"
                              { (yyval.literal).ufixnum = JITTER_SIZEOF_VOID_P; }
#line 1522 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 22: /* int_expression: LGBYTESPERWORD  */
#line 346 "../../jitter/jitter/jitter-routine.y"
                              { (yyval.literal).ufixnum = JITTER_LG_BYTES_PER_WORD; }
#line 1528 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 23: /* int_expression: BITSPERWORD  */
#line 347 "../../jitter/jitter/jitter-routine.y"
                              { (yyval.literal).ufixnum = JITTER_SIZEOF_VOID_P * CHAR_BIT; }
#line 1534 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 24: /* int_expression: JITTER_INT_MIN_  */
#line 348 "../../jitter/jitter/jitter-routine.y"
                              { (yyval.literal).fixnum = JITTER_INT_MIN; }
#line 1540 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 25: /* int_expression: JITTER_INT_MAX_  */
#line 349 "../../jitter/jitter/jitter-routine.y"
                              { (yyval.literal).fixnum = JITTER_INT_MAX; }
#line 1546 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 26: /* int_expression: JITTER_UINT_MAX_  */
#line 350 "../../jitter/jitter/jitter-routine.y"
                              { (yyval.literal).ufixnum = JITTER_UINT_MAX; }
#line 1552 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 27: /* int_expression: OPEN_PARENS int_expression CLOSE_PARENS  */
#line 351 "../../jitter/jitter/jitter-routine.y"
                                          { (yyval.literal) = (yyvsp[-1].literal); }
#line 1558 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 28: /* int_expression: UNSIGNED_MINUS int_expression  */
#line 353 "../../jitter/jitter/jitter-routine.y"
     { union jitter_word zero = {.ufixnum = 0};
       JITTER_SET_OPERATION(ufixnum, (yyval.literal), zero, -, (yyvsp[0].literal)); }
#line 1565 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 29: /* int_expression: int_expression TIMES int_expression  */
#line 356 "../../jitter/jitter/jitter-routine.y"
     { JITTER_SET_OPERATION(fixnum, (yyval.literal), (yyvsp[-2].literal), *, (yyvsp[0].literal)); }
#line 1571 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 30: /* int_expression: int_expression DIV int_expression  */
#line 358 "../../jitter/jitter/jitter-routine.y"
     { JITTER_SET_OPERATION(fixnum, (yyval.literal), (yyvsp[-2].literal), /, (yyvsp[0].literal)); }
#line 1577 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 31: /* int_expression: int_expression MOD int_expression  */
#line 360 "../../jitter/jitter/jitter-routine.y"
     { JITTER_SET_OPERATION(fixnum, (yyval.literal), (yyvsp[-2].literal), %, (yyvsp[0].literal)); }
#line 1583 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 32: /* int_expression: int_expression UNSIGNED_PLUS int_expression  */
#line 362 "../../jitter/jitter/jitter-routine.y"
     { JITTER_SET_OPERATION(fixnum, (yyval.literal), (yyvsp[-2].literal), +, (yyvsp[0].literal)); }
#line 1589 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 33: /* int_expression: int_expression UNSIGNED_MINUS int_expression  */
#line 364 "../../jitter/jitter/jitter-routine.y"
     { JITTER_SET_OPERATION(fixnum, (yyval.literal), (yyvsp[-2].literal), -, (yyvsp[0].literal)); }
#line 1595 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 34: /* int_expression: int_expression UNSIGNED_TIMES int_expression  */
#line 366 "../../jitter/jitter/jitter-routine.y"
     { JITTER_SET_OPERATION(fixnum, (yyval.literal), (yyvsp[-2].literal), *, (yyvsp[0].literal)); }
#line 1601 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 35: /* int_expression: int_expression UNSIGNED_DIV int_expression  */
#line 368 "../../jitter/jitter/jitter-routine.y"
     { JITTER_SET_OPERATION(fixnum, (yyval.literal), (yyvsp[-2].literal), /, (yyvsp[0].literal)); }
#line 1607 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 36: /* int_expression: int_expression UNSIGNED_MOD int_expression  */
#line 370 "../../jitter/jitter/jitter-routine.y"
     { JITTER_SET_OPERATION(fixnum, (yyval.literal), (yyvsp[-2].literal), %, (yyvsp[0].literal)); }
#line 1613 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 37: /* int_expression: UNSIGNED_NOT int_expression  */
#line 372 "../../jitter/jitter/jitter-routine.y"
     { union jitter_word minus_one = {.fixnum = -1L};
       JITTER_SET_OPERATION(ufixnum, (yyval.literal), (yyvsp[0].literal), ^, minus_one); }
#line 1620 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 38: /* int_expression: int_expression UNSIGNED_AND int_expression  */
#line 375 "../../jitter/jitter/jitter-routine.y"
     { JITTER_SET_OPERATION(ufixnum, (yyval.literal), (yyvsp[-2].literal), &, (yyvsp[0].literal)); }
#line 1626 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 39: /* int_expression: int_expression UNSIGNED_OR int_expression  */
#line 377 "../../jitter/jitter/jitter-routine.y"
     { JITTER_SET_OPERATION(ufixnum, (yyval.literal), (yyvsp[-2].literal), |, (yyvsp[0].literal)); }
#line 1632 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 40: /* int_expression: int_expression UNSIGNED_XOR int_expression  */
#line 379 "../../jitter/jitter/jitter-routine.y"
     { JITTER_SET_OPERATION(ufixnum, (yyval.literal), (yyvsp[-2].literal), ^, (yyvsp[0].literal)); }
#line 1638 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 41: /* int_expression: int_expression POWER int_expression  */
#line 381 "../../jitter/jitter/jitter-routine.y"
     { (yyval.literal).fixnum = jitter_power ((yyvsp[-2].literal).fixnum, (yyvsp[0].literal).fixnum); }
#line 1644 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 42: /* int_expression: int_expression UNSIGNED_POWER int_expression  */
#line 383 "../../jitter/jitter/jitter-routine.y"
     { (yyval.literal).ufixnum = jitter_upower ((yyvsp[-2].literal).ufixnum, (yyvsp[0].literal).ufixnum); }
#line 1650 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 43: /* argument: int_expression  */
#line 387 "../../jitter/jitter/jitter-routine.y"
                 { JITTER_HANDLE_STATUS
                      (jitter_mutable_routine_append_literal_parameter_safe
                         (parser_arg->routine, (yyvsp[0].literal))); }
#line 1658 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 44: /* argument: LABEL_LITERAL  */
#line 390 "../../jitter/jitter/jitter-routine.y"
                 { char *text = jitter_get_text (jitter_scanner);
                   JITTER_HANDLE_STATUS
                      (jitter_mutable_routine_append_symbolic_label_parameter_safe
                         (NULL, parser_arg->routine, text)); }
#line 1667 "../../jitter/jitter/jitter-routine-parser.c"
    break;

  case 45: /* argument: REGISTER  */
#line 394 "../../jitter/jitter/jitter-routine.y"
                 { char *text = jitter_get_text (jitter_scanner);
                   char register_class_character = text [1];
                   int register_id = strtol (text + 2, NULL, 10);
                   JITTER_HANDLE_STATUS
                      (jitter_mutable_routine_append_symbolic_register_parameter_safe
                         (parser_arg->routine, register_class_character,
                          register_id)); }
#line 1679 "../../jitter/jitter/jitter-routine-parser.c"
    break;


#line 1683 "../../jitter/jitter/jitter-routine-parser.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == JITTER_EMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (&yylloc, parser_arg, jitter_scanner, YY_("syntax error"));
    }

  yyerror_range[1] = yylloc;
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= JITTER_EOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == JITTER_EOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, parser_arg, jitter_scanner);
          yychar = JITTER_EMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, yylsp, parser_arg, jitter_scanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  ++yylsp;
  YYLLOC_DEFAULT (*yylsp, yyerror_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, parser_arg, jitter_scanner, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != JITTER_EMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, parser_arg, jitter_scanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, yylsp, parser_arg, jitter_scanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 403 "../../jitter/jitter/jitter-routine.y"


static void
jitter_error (YYLTYPE *locp,
              struct parser_arg *parser_arg,
              yyscan_t jitter_scanner, const char *message)
{
  JITTER_HANDLE_STATUS_WITHOUT_YYABORT
     (jitter_routine_edit_status_other_parse_error);
}

/* This function could be implemented elsewhere, but in practice I suppose it is
   only used when the parser is also used: therefore having it in this
   compilation unit will make the code a little smaller. */
const char*
jitter_routine_edit_status_to_string (enum jitter_routine_edit_status s)
{
  switch (s)
    {
    case jitter_routine_edit_status_success:
      return "success";
    case jitter_routine_edit_status_label_defined_twice:
      return "label defined twice";
    case jitter_routine_edit_status_too_many_parameters:
      return "too many parameters";
    case jitter_routine_edit_status_last_instruction_incomplete:
      return "last instruction incomplete";
    case jitter_routine_edit_status_invalid_instruction:
      return "invalid instruction";
    case jitter_routine_edit_status_register_class_mismatch:
      return "register class mismatch";
    case jitter_routine_edit_status_nonexisting_register_class:
      return "nonexisting register class";
    case jitter_routine_edit_status_invalid_register:
      return "invalid register";
    case jitter_routine_edit_status_invalid_parameter_kind:
      return "invalid parameter kind";
    case jitter_routine_edit_status_other_parse_error:
      return "parse error";
    default:
      jitter_fatal ("jitter_routine_edit_status_to_string: invalid argument %i",
                    (int) s);
    };
}

/* The comment about code size before jitter_routine_edit_status_to_string
   applies here as well. */
__attribute__ ((nonnull (1)))
static void
jitter_routine_parse_error_finalize (struct jitter_routine_parse_error *e)
{
  free (e->file_name);
  free (e->error_token_text);
}

void
jitter_routine_parse_error_destroy (struct jitter_routine_parse_error *e)
{
  jitter_routine_parse_error_finalize (e);
  free (e);
}

/* This is the main parser function doing all the work.  The other parsing
   functions, meant as part of a more convenient API for the user, all rely on
   this.
   The pointed scanner must be already initialized when this is called, and
   it's the caller's responsibility to finalize it. */
static struct jitter_routine_parse_error *
jitter_parse_core (const char *file_name_to_print,
                   yyscan_t scanner, struct jitter_mutable_routine *p,
                   const struct jitter_vm *vm)
{
  struct parser_arg pa;
  /* Initialise the parse status to a no-error value. */
  pa.parse_status.status = jitter_routine_edit_status_success;
  pa.parse_status.file_name = jitter_clone_string (file_name_to_print);
  pa.parse_status.error_line_no = -1;
  pa.parse_status.error_token_text = NULL;

  /* Use pa to let the parser access the mutable routine it needs to add to, and
     its VM. */
  pa.routine = p;
  pa.vm = (struct jitter_vm *) vm;

  /* Notice that the lexer state will need to be destroyed, in case of both
     success and failure; but it will be destroyed by the caller, not here. */
  if (jitter_parse (& pa, scanner))
    {
      /* Parse error.  Make a copy of the parse status, a pointer of which will
         be returned to the user.  The user will have to destroy it with
         jitter_routine_parse_error_destroy .  It simply works by keeping each
         string field in the struct, and the struct itself, malloc-allocated. */
      struct jitter_routine_parse_error *res
        = jitter_xmalloc (sizeof (struct jitter_routine_parse_error));
      * res = pa.parse_status;
      return res;
    }
  else
    {
      /* Parse success.  Destroy the parse status malloc-allocated part, and we
         are done. */
      jitter_routine_parse_error_finalize (& pa.parse_status);
      return NULL;
    }
}

static struct jitter_routine_parse_error *
jitter_parse_mutable_routine_from_named_file_star (const char *input_file_name,
                                                   FILE *input_file,
                                                   struct jitter_mutable_routine
                                                   *p,
                                                   const struct jitter_vm *vm)
{
  yyscan_t scanner;
  jitter_lex_init (& scanner);
  jitter_set_in (input_file, scanner);

  struct jitter_routine_parse_error *res
    = jitter_parse_core (input_file_name, scanner, p, vm);

  jitter_set_in (NULL, scanner);
  jitter_lex_destroy (scanner);
  return res;
}

struct jitter_routine_parse_error *
jitter_parse_mutable_routine_from_file_star (FILE *input_file,
                                             struct jitter_mutable_routine *p,
                                             const struct jitter_vm *vm)
{
  return jitter_parse_mutable_routine_from_named_file_star ("<input>",
                                                            input_file,
                                                            p,
                                                            vm);
}

struct jitter_routine_parse_error *
jitter_parse_mutable_routine_from_file (const char *input_file_name,
                                        struct jitter_mutable_routine *p,
                                        const struct jitter_vm *vm)
{
  FILE *f;
  if ((f = fopen (input_file_name, "r")) == NULL)
    jitter_fatal ("could not open %s for reading", input_file_name);

  struct jitter_routine_parse_error *res
    = jitter_parse_mutable_routine_from_named_file_star (input_file_name,
                                                         f, p, vm);
  fclose (f);
  return res;
}

struct jitter_routine_parse_error *
jitter_parse_mutable_routine_from_string (const char *string,
                                          struct jitter_mutable_routine *p,
                                          const struct jitter_vm *vm)
{
  yyscan_t scanner;
  jitter_lex_init (& scanner);
  jitter__scan_string (string, scanner);
  struct jitter_routine_parse_error *res
    = jitter_parse_core ("<string>", scanner, p, vm);
  jitter_lex_destroy (scanner);
  return res;
}
