/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Substitute the type names.  */
#define YYSTYPE         JITTERC_STYPE
#define YYLTYPE         JITTERC_LTYPE
/* Substitute the variable and function names.  */
#define yyparse         jitterc_parse
#define yylex           jitterc_lex
#define yyerror         jitterc_error
#define yydebug         jitterc_debug
#define yynerrs         jitterc_nerrs

/* First part of user prologue.  */
#line 27 "../../jitter/jitterc/jitterc.y"

/* Include the Gnulib header. */
#include <config.h>

#include <stdio.h>
#include <ctype.h>
#include <c-ctype.h>
#include <jitter/jitter-bitwise.h>
#include <jitter/jitter-malloc.h>
#include <jitter/jitter-fatal.h>
#include <jitter/jitter-parse-int.h>
#include <jitter/jitter-string.h>
#include <gl_xlist.h>
#include <gl_array_list.h>

#include "jitterc-vm.h"
#include "jitterc-mangle.h"
#include "jitterc-rewrite.h"
#include "jitterc-utility.h"
#include "jitterc-parser.h"
#include "jitterc-scanner.h"

/* This is currently a fatal error.  I could longjmp away instead. */
static void
jitterc_error (YYLTYPE *locp, struct jitterc_vm *vm,
               yyscan_t scanner, char *message)
  __attribute__ ((noreturn));

#define JITTERC_PARSE_ERROR(message)                      \
  do                                                      \
    {                                                     \
      jitterc_error (jitterc_get_lloc (jitterc_scanner),  \
                       vm, jitterc_scanner, message);     \
    }                                                     \
  while (false)

/* Set the given property of the last instruction to the given enum case,
   checking that each property is not set more than once.  This is useful
   for enumerate-valued properties such as hotness and relocatability. */
#define JITTERC_SET_PROPERTY(property, value)                  \
  do                                                           \
    {                                                          \
      enum jitterc_ ## property *property                      \
        = & jitterc_vm_last_instruction (vm)->property;        \
      if (* property != jitterc_ ## property ## _unspecified)  \
        JITTERC_PARSE_ERROR("duplicate " # property);          \
      * property = jitterc_ ## property ## _ ## value;         \
    }                                                          \
  while (false)

/* What would be yytext in a non-reentrant scanner. */
#define JITTERC_TEXT \
  (jitterc_get_text (jitterc_scanner))

 /* What would be yylineno in a non-reentrant scanner. */
#define JITTERC_LINENO \
  (jitterc_get_lineno (jitterc_scanner))

/* A copy of what would be yytext in a non-reentrant scanner. */
#define JITTERC_TEXT_COPY \
  (jitter_clone_string (JITTERC_TEXT))

/* Assign the given lvalue with a string concatenation of its current value and
   the new string from the code block, preceded by a #line CPP directive unless
   #line-generation was disabled.  Free both strings (but not the pointed
   struct, which normally comes from internal Bison data structures). */
#define JITTERC_APPEND_CODE(lvalue, code_block_pointerq)                        \
  do                                                                            \
    {                                                                           \
       struct jitterc_code_block *code_block_pointer = code_block_pointerq;     \
       int line_number = code_block_pointer->line_number;                       \
       char *new_code = code_block_pointer->code;                               \
       char *line_line = xmalloc (strlen (vm->source_file_name) + 100);         \
       if (vm->generate_line)                                                   \
         sprintf (line_line, "#line %i \"%s\"\n",                               \
                  line_number,                                                  \
                  vm->source_file_name);                                        \
       else                                                                     \
         line_line [0] = '\0';                                                  \
       size_t line_line_length = strlen (line_line);                            \
       size_t lvalue_length = strlen (lvalue);                                  \
       char *concatenation                                                      \
         = xrealloc (lvalue,                                                    \
                     lvalue_length + line_line_length                           \
                     + strlen (new_code) + 1);                                  \
       strcpy (concatenation + lvalue_length,                                   \
               line_line);                                                      \
       strcpy (concatenation + lvalue_length + line_line_length,                \
               new_code);                                                       \
       free (line_line);                                                        \
       free (new_code);                                                         \
       /* Poison the pointer still in the struct, just for defensiveness. */    \
       code_block_pointer->code = NULL;                                         \
       lvalue = concatenation;                                                  \
    }                                                                           \
  while (false)                                                                 \

/* Return a struct jitterc_code_block containing a copy of the pointed text,
   starting at the given line number. */
struct jitterc_code_block
jitterc_make_code_struct (struct jitterc_vm *vm,
                          const char *text, int line_number)
{
  /* Count newline characters. */
  int newline_no = 0;
  int i;
  for (i = 0; text [i] != '\0'; i++)
    if (text [i] == '\n')
      newline_no ++;

  /* Make the struct to be returned.  Its initial content must be
     malloc-allocated, here with jitter_clone_string, because of 
     JITTERC_APPEND_CODE which frees its parameters. */
  struct jitterc_code_block res;
  char *s = xmalloc (1);
  strcpy (s, "");
  res.line_number = line_number - newline_no - 1;
  res.code = jitter_clone_string (text);
  JITTERC_APPEND_CODE(s, & res);
  res.code = s;
  return res;
}

/* FIXME: unfactor this code back into the only rule which should need it. */
#define KIND_CASE(character, suffix)                      \
  case character:                                         \
    if (k & jitterc_instruction_argument_kind_ ## suffix) \
      JITTERC_PARSE_ERROR("duplicate " #suffix " kind");  \
    k |= jitterc_instruction_argument_kind_ ## suffix;    \
  break;
#define KIND_CASE_DEFAULT(out, character)                   \
  default:                                                  \
    if (c_isupper (character))                                \
      {                                                     \
        if (k & jitterc_instruction_argument_kind_register) \
          JITTERC_PARSE_ERROR("duplicate register kind");   \
        k |= jitterc_instruction_argument_kind_register;    \
        out.register_class_letter = tolower (character);    \
      }                                                     \
    else                                                    \
      JITTERC_PARSE_ERROR("invalid kind letter");


#line 222 "../../jitter/jitterc/jitterc-parser.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "jitterc-parser.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_VM = 3,                         /* VM  */
  YYSYMBOL_LEGAL_NOTICE = 4,               /* LEGAL_NOTICE  */
  YYSYMBOL_END = 5,                        /* END  */
  YYSYMBOL_CODE = 6,                       /* CODE  */
  YYSYMBOL_STRING = 7,                     /* STRING  */
  YYSYMBOL_SET = 8,                        /* SET  */
  YYSYMBOL_INITIAL_HEADER_C = 9,           /* INITIAL_HEADER_C  */
  YYSYMBOL_INITIAL_VM1_C = 10,             /* INITIAL_VM1_C  */
  YYSYMBOL_INITIAL_VM2_C = 11,             /* INITIAL_VM2_C  */
  YYSYMBOL_INITIAL_VM_MAIN_C = 12,         /* INITIAL_VM_MAIN_C  */
  YYSYMBOL_EARLY_HEADER_C = 13,            /* EARLY_HEADER_C  */
  YYSYMBOL_LATE_HEADER_C = 14,             /* LATE_HEADER_C  */
  YYSYMBOL_PRINTER_C = 15,                 /* PRINTER_C  */
  YYSYMBOL_REWRITER_C = 16,                /* REWRITER_C  */
  YYSYMBOL_EARLY_C = 17,                   /* EARLY_C  */
  YYSYMBOL_LATE_C = 18,                    /* LATE_C  */
  YYSYMBOL_INITIALIZATION_C = 19,          /* INITIALIZATION_C  */
  YYSYMBOL_FINALIZATION_C = 20,            /* FINALIZATION_C  */
  YYSYMBOL_STATE_EARLY_C = 21,             /* STATE_EARLY_C  */
  YYSYMBOL_STATE_BACKING_STRUCT_C = 22,    /* STATE_BACKING_STRUCT_C  */
  YYSYMBOL_STATE_RUNTIME_STRUCT_C = 23,    /* STATE_RUNTIME_STRUCT_C  */
  YYSYMBOL_STATE_INITIALIZATION_C = 24,    /* STATE_INITIALIZATION_C  */
  YYSYMBOL_STATE_RESET_C = 25,             /* STATE_RESET_C  */
  YYSYMBOL_STATE_FINALIZATION_C = 26,      /* STATE_FINALIZATION_C  */
  YYSYMBOL_INSTRUCTION_BEGINNING_C = 27,   /* INSTRUCTION_BEGINNING_C  */
  YYSYMBOL_INSTRUCTION_END_C = 28,         /* INSTRUCTION_END_C  */
  YYSYMBOL_BARE_ARGUMENT = 29,             /* BARE_ARGUMENT  */
  YYSYMBOL_IDENTIFIER = 30,                /* IDENTIFIER  */
  YYSYMBOL_WRAPPED_FUNCTIONS = 31,         /* WRAPPED_FUNCTIONS  */
  YYSYMBOL_WRAPPED_GLOBALS = 32,           /* WRAPPED_GLOBALS  */
  YYSYMBOL_INSTRUCTION = 33,               /* INSTRUCTION  */
  YYSYMBOL_OPEN_PAREN = 34,                /* OPEN_PAREN  */
  YYSYMBOL_CLOSE_PAREN = 35,               /* CLOSE_PAREN  */
  YYSYMBOL_COMMA = 36,                     /* COMMA  */
  YYSYMBOL_SEMICOLON = 37,                 /* SEMICOLON  */
  YYSYMBOL_IN = 38,                        /* IN  */
  YYSYMBOL_OUT = 39,                       /* OUT  */
  YYSYMBOL_RULE = 40,                      /* RULE  */
  YYSYMBOL_WHEN = 41,                      /* WHEN  */
  YYSYMBOL_REWRITE = 42,                   /* REWRITE  */
  YYSYMBOL_INTO = 43,                      /* INTO  */
  YYSYMBOL_TRUE_ = 44,                     /* TRUE_  */
  YYSYMBOL_FALSE_ = 45,                    /* FALSE_  */
  YYSYMBOL_RULE_PLACEHOLDER = 46,          /* RULE_PLACEHOLDER  */
  YYSYMBOL_HOT = 47,                       /* HOT  */
  YYSYMBOL_COLD = 48,                      /* COLD  */
  YYSYMBOL_RELOCATABLE = 49,               /* RELOCATABLE  */
  YYSYMBOL_NON_RELOCATABLE = 50,           /* NON_RELOCATABLE  */
  YYSYMBOL_NON_BRANCHING = 51,             /* NON_BRANCHING  */
  YYSYMBOL_BRANCHING = 52,                 /* BRANCHING  */
  YYSYMBOL_CALLER = 53,                    /* CALLER  */
  YYSYMBOL_CALLEE = 54,                    /* CALLEE  */
  YYSYMBOL_RETURNING = 55,                 /* RETURNING  */
  YYSYMBOL_COMMUTATIVE = 56,               /* COMMUTATIVE  */
  YYSYMBOL_NON_COMMUTATIVE = 57,           /* NON_COMMUTATIVE  */
  YYSYMBOL_TWO_OPERANDS = 58,              /* TWO_OPERANDS  */
  YYSYMBOL_REGISTER_CLASS = 59,            /* REGISTER_CLASS  */
  YYSYMBOL_FAST_REGISTER_NO = 60,          /* FAST_REGISTER_NO  */
  YYSYMBOL_REGISTER_OR_STACK_LETTER = 61,  /* REGISTER_OR_STACK_LETTER  */
  YYSYMBOL_SLOW_REGISTERS = 62,            /* SLOW_REGISTERS  */
  YYSYMBOL_NO_SLOW_REGISTERS = 63,         /* NO_SLOW_REGISTERS  */
  YYSYMBOL_STACK = 64,                     /* STACK  */
  YYSYMBOL_C_TYPE = 65,                    /* C_TYPE  */
  YYSYMBOL_C_INITIAL_VALUE = 66,           /* C_INITIAL_VALUE  */
  YYSYMBOL_C_ELEMENT_TYPE = 67,            /* C_ELEMENT_TYPE  */
  YYSYMBOL_LONG_NAME = 68,                 /* LONG_NAME  */
  YYSYMBOL_ELEMENT_NO = 69,                /* ELEMENT_NO  */
  YYSYMBOL_NON_TOS_OPTIMIZED = 70,         /* NON_TOS_OPTIMIZED  */
  YYSYMBOL_TOS_OPTIMIZED = 71,             /* TOS_OPTIMIZED  */
  YYSYMBOL_NO_GUARD_OVERFLOW = 72,         /* NO_GUARD_OVERFLOW  */
  YYSYMBOL_NO_GUARD_UNDERFLOW = 73,        /* NO_GUARD_UNDERFLOW  */
  YYSYMBOL_GUARD_OVERFLOW = 74,            /* GUARD_OVERFLOW  */
  YYSYMBOL_GUARD_UNDERFLOW = 75,           /* GUARD_UNDERFLOW  */
  YYSYMBOL_FIXNUM = 76,                    /* FIXNUM  */
  YYSYMBOL_BITSPERWORD = 77,               /* BITSPERWORD  */
  YYSYMBOL_BYTESPERWORD = 78,              /* BYTESPERWORD  */
  YYSYMBOL_LGBYTESPERWORD = 79,            /* LGBYTESPERWORD  */
  YYSYMBOL_YYACCEPT = 80,                  /* $accept  */
  YYSYMBOL_vm = 81,                        /* vm  */
  YYSYMBOL_sections = 82,                  /* sections  */
  YYSYMBOL_section = 83,                   /* section  */
  YYSYMBOL_vm_section = 84,                /* vm_section  */
  YYSYMBOL_legal_notice = 85,              /* legal_notice  */
  YYSYMBOL_vm_section_contents = 86,       /* vm_section_contents  */
  YYSYMBOL_setting = 87,                   /* setting  */
  YYSYMBOL_c_section = 88,                 /* c_section  */
  YYSYMBOL_wrapped_functions_section = 89, /* wrapped_functions_section  */
  YYSYMBOL_wrapped_globals_section = 90,   /* wrapped_globals_section  */
  YYSYMBOL_identifiers = 91,               /* identifiers  */
  YYSYMBOL_rule_section = 92,              /* rule_section  */
  YYSYMBOL_optional_identifier = 93,       /* optional_identifier  */
  YYSYMBOL_rule_guard = 94,                /* rule_guard  */
  YYSYMBOL_rule_instruction_pattern = 95,  /* rule_instruction_pattern  */
  YYSYMBOL_rule_instruction_patterns_zero_or_more = 96, /* rule_instruction_patterns_zero_or_more  */
  YYSYMBOL_rule_instruction_patterns_one_or_more = 97, /* rule_instruction_patterns_one_or_more  */
  YYSYMBOL_rule_argument_pattern = 98,     /* rule_argument_pattern  */
  YYSYMBOL_placeholder = 99,               /* placeholder  */
  YYSYMBOL_optional_placeholder = 100,     /* optional_placeholder  */
  YYSYMBOL_rule_argument_patterns_zero_or_more = 101, /* rule_argument_patterns_zero_or_more  */
  YYSYMBOL_rule_argument_patterns_one_or_more = 102, /* rule_argument_patterns_one_or_more  */
  YYSYMBOL_rule_expressions_zero_or_more = 103, /* rule_expressions_zero_or_more  */
  YYSYMBOL_rule_expressions_one_or_more = 104, /* rule_expressions_one_or_more  */
  YYSYMBOL_rule_expression = 105,          /* rule_expression  */
  YYSYMBOL_rule_operation = 106,           /* rule_operation  */
  YYSYMBOL_rule_instruction_templates_zero_or_more = 107, /* rule_instruction_templates_zero_or_more  */
  YYSYMBOL_rule_instruction_template = 108, /* rule_instruction_template  */
  YYSYMBOL_register_class_section = 109,   /* register_class_section  */
  YYSYMBOL_register_class_section_contents = 110, /* register_class_section_contents  */
  YYSYMBOL_stack_section = 111,            /* stack_section  */
  YYSYMBOL_stack_section_contents = 112,   /* stack_section_contents  */
  YYSYMBOL_register_or_stack_letter = 113, /* register_or_stack_letter  */
  YYSYMBOL_instruction_section = 114,      /* instruction_section  */
  YYSYMBOL_115_1 = 115,                    /* $@1  */
  YYSYMBOL_arguments = 116,                /* arguments  */
  YYSYMBOL_one_or_more_arguments = 117,    /* one_or_more_arguments  */
  YYSYMBOL_argument = 118,                 /* argument  */
  YYSYMBOL_119_2 = 119,                    /* $@2  */
  YYSYMBOL_optional_printer_name = 120,    /* optional_printer_name  */
  YYSYMBOL_modes = 121,                    /* modes  */
  YYSYMBOL_modes_rest = 122,               /* modes_rest  */
  YYSYMBOL_mode_character = 123,           /* mode_character  */
  YYSYMBOL_bare_argument = 124,            /* bare_argument  */
  YYSYMBOL_properties = 125,               /* properties  */
  YYSYMBOL_hotness = 126,                  /* hotness  */
  YYSYMBOL_relocatability = 127,           /* relocatability  */
  YYSYMBOL_branchingness = 128,            /* branchingness  */
  YYSYMBOL_callrelatedness = 129,          /* callrelatedness  */
  YYSYMBOL_identifier = 130,               /* identifier  */
  YYSYMBOL_string = 131,                   /* string  */
  YYSYMBOL_code = 132,                     /* code  */
  YYSYMBOL_literal = 133,                  /* literal  */
  YYSYMBOL_literals = 134,                 /* literals  */
  YYSYMBOL_135_3 = 135                     /* $@3  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined JITTERC_LTYPE_IS_TRIVIAL && JITTERC_LTYPE_IS_TRIVIAL \
             && defined JITTERC_STYPE_IS_TRIVIAL && JITTERC_STYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  77
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   217

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  80
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  56
/* YYNRULES -- Number of rules.  */
#define YYNRULES  142
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  248

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   334


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79
};

#if JITTERC_DEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   339,   339,   342,   344,   348,   349,   350,   351,   352,
     353,   354,   355,   356,   360,   369,   380,   382,   387,   392,
     394,   396,   398,   400,   402,   404,   406,   408,   410,   412,
     414,   416,   418,   420,   422,   424,   429,   431,   433,   438,
     443,   448,   451,   456,   473,   474,   478,   479,   484,   490,
     491,   496,   499,   505,   514,   525,   539,   544,   545,   550,
     551,   556,   559,   566,   567,   570,   576,   579,   585,   587,
     589,   592,   594,   596,   601,   607,   608,   613,   619,   624,
     631,   632,   635,   638,   641,   644,   647,   653,   660,   661,
     664,   667,   670,   673,   676,   679,   682,   685,   688,   695,
     700,   699,   754,   756,   760,   761,   766,   765,   811,   812,
     816,   824,   825,   832,   833,   840,   854,   872,   874,   875,
     876,   877,   881,   882,   886,   887,   891,   892,   896,   897,
     902,   909,   910,   911,   915,   922,   927,   930,   931,   932,
     936,   937,   937
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if JITTERC_DEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "VM", "LEGAL_NOTICE",
  "END", "CODE", "STRING", "SET", "INITIAL_HEADER_C", "INITIAL_VM1_C",
  "INITIAL_VM2_C", "INITIAL_VM_MAIN_C", "EARLY_HEADER_C", "LATE_HEADER_C",
  "PRINTER_C", "REWRITER_C", "EARLY_C", "LATE_C", "INITIALIZATION_C",
  "FINALIZATION_C", "STATE_EARLY_C", "STATE_BACKING_STRUCT_C",
  "STATE_RUNTIME_STRUCT_C", "STATE_INITIALIZATION_C", "STATE_RESET_C",
  "STATE_FINALIZATION_C", "INSTRUCTION_BEGINNING_C", "INSTRUCTION_END_C",
  "BARE_ARGUMENT", "IDENTIFIER", "WRAPPED_FUNCTIONS", "WRAPPED_GLOBALS",
  "INSTRUCTION", "OPEN_PAREN", "CLOSE_PAREN", "COMMA", "SEMICOLON", "IN",
  "OUT", "RULE", "WHEN", "REWRITE", "INTO", "TRUE_", "FALSE_",
  "RULE_PLACEHOLDER", "HOT", "COLD", "RELOCATABLE", "NON_RELOCATABLE",
  "NON_BRANCHING", "BRANCHING", "CALLER", "CALLEE", "RETURNING",
  "COMMUTATIVE", "NON_COMMUTATIVE", "TWO_OPERANDS", "REGISTER_CLASS",
  "FAST_REGISTER_NO", "REGISTER_OR_STACK_LETTER", "SLOW_REGISTERS",
  "NO_SLOW_REGISTERS", "STACK", "C_TYPE", "C_INITIAL_VALUE",
  "C_ELEMENT_TYPE", "LONG_NAME", "ELEMENT_NO", "NON_TOS_OPTIMIZED",
  "TOS_OPTIMIZED", "NO_GUARD_OVERFLOW", "NO_GUARD_UNDERFLOW",
  "GUARD_OVERFLOW", "GUARD_UNDERFLOW", "FIXNUM", "BITSPERWORD",
  "BYTESPERWORD", "LGBYTESPERWORD", "$accept", "vm", "sections", "section",
  "vm_section", "legal_notice", "vm_section_contents", "setting",
  "c_section", "wrapped_functions_section", "wrapped_globals_section",
  "identifiers", "rule_section", "optional_identifier", "rule_guard",
  "rule_instruction_pattern", "rule_instruction_patterns_zero_or_more",
  "rule_instruction_patterns_one_or_more", "rule_argument_pattern",
  "placeholder", "optional_placeholder",
  "rule_argument_patterns_zero_or_more",
  "rule_argument_patterns_one_or_more", "rule_expressions_zero_or_more",
  "rule_expressions_one_or_more", "rule_expression", "rule_operation",
  "rule_instruction_templates_zero_or_more", "rule_instruction_template",
  "register_class_section", "register_class_section_contents",
  "stack_section", "stack_section_contents", "register_or_stack_letter",
  "instruction_section", "$@1", "arguments", "one_or_more_arguments",
  "argument", "$@2", "optional_printer_name", "modes", "modes_rest",
  "mode_character", "bare_argument", "properties", "hotness",
  "relocatability", "branchingness", "callrelatedness", "identifier",
  "string", "code", "literal", "literals", "$@3", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-171)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      77,    -4,  -171,    11,    11,    11,    11,    11,    11,    11,
      11,    11,    11,    11,    11,    11,    11,    11,    11,    11,
      11,    11,    11,   -17,   -17,  -171,   -17,   -45,   -45,    45,
    -171,    77,  -171,  -171,  -171,  -171,  -171,  -171,  -171,  -171,
    -171,   -17,    52,    -4,  -171,    53,    54,    56,    57,    79,
      80,   101,   102,   108,   109,   115,   132,   137,   139,   140,
     143,   147,   148,   149,   150,  -171,  -171,  -171,   151,   -17,
     152,   -17,    40,  -171,  -171,   119,    -1,  -171,  -171,   159,
    -171,  -171,  -171,  -171,  -171,  -171,  -171,  -171,  -171,  -171,
    -171,  -171,  -171,  -171,  -171,  -171,  -171,  -171,  -171,  -171,
    -171,  -171,  -171,  -171,  -171,   133,   -17,   -70,   119,   119,
     159,   159,   159,   163,   159,   159,   159,   -70,    -1,    -1,
      -1,    -1,    -1,    -1,   175,  -171,  -171,   -28,   146,   145,
    -171,    86,  -171,  -171,  -171,  -171,   119,  -171,  -171,   119,
     119,   119,  -171,    -1,    -1,    -1,    -1,  -171,  -171,  -171,
    -171,  -171,  -171,  -171,  -171,  -171,   155,  -171,   156,   -14,
     -28,   -17,   -17,  -171,  -171,  -171,   157,  -171,  -171,  -171,
     153,   153,  -171,  -171,  -171,  -171,  -171,  -171,  -171,  -171,
     123,   -28,  -171,  -171,   -28,  -171,   154,   160,    82,    86,
    -171,  -171,  -171,  -171,  -171,  -171,  -171,  -171,  -171,  -171,
    -171,  -171,    11,   123,   123,   123,   123,  -171,   -70,  -171,
      82,   181,   -17,    82,  -171,  -171,  -171,  -171,   158,  -171,
     162,  -171,  -171,   186,  -171,  -171,  -171,  -171,  -171,   -17,
    -171,  -171,  -171,   165,    82,    82,  -171,   -70,  -171,  -171,
    -171,  -171,   166,   168,  -171,    82,  -171,  -171
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       3,    16,    15,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    41,    41,   100,    44,     0,     0,     0,
       2,     3,     6,     5,     7,     8,     9,    13,    10,    11,
      12,     0,     0,    16,   135,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   131,   133,   132,     0,    41,
       0,     0,     0,    45,    99,    80,    88,     1,     4,     0,
      14,    17,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    42,    40,     0,    49,     0,    80,    80,
       0,     0,     0,     0,     0,     0,     0,     0,    88,    88,
      88,    88,    88,    88,     0,   134,    18,   102,    51,     0,
      50,    59,   136,   137,   138,   139,    80,    86,    85,    80,
      80,    80,    79,    88,    88,    88,    88,    98,    97,    94,
      93,    96,    95,    87,   113,   114,     0,   103,   104,     0,
     111,     0,    75,   116,    56,   115,    61,    55,    48,    60,
      57,    57,    84,    82,    83,    81,    92,    90,    89,    91,
     117,     0,   106,   110,   111,    52,    46,    76,    63,     0,
      58,    53,    54,   122,   123,   124,   125,   126,   127,   128,
     129,   130,     0,   117,   117,   117,   117,   105,   140,   112,
       0,     0,    75,     0,    68,    69,    71,    78,    64,    73,
       0,    70,    62,     0,   118,   120,   119,   121,   141,   108,
      47,    43,    77,     0,     0,    63,   101,   140,   107,   109,
      72,    65,    66,     0,   142,     0,    74,    67
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -171,  -171,   167,  -171,  -171,  -171,   161,  -171,  -171,  -171,
    -171,   -19,  -171,  -171,  -171,  -171,  -171,    44,  -171,  -129,
      30,  -171,    18,   -27,   -36,  -170,  -171,    -2,  -171,  -171,
      10,  -171,   -67,   183,  -171,  -171,  -171,    31,  -171,  -171,
    -171,  -171,    29,  -146,    55,   -81,  -171,  -171,  -171,  -171,
     -23,    19,    15,   -68,   -22,  -171
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_uint8 yydefgoto[] =
{
       0,    29,    30,    31,    32,    33,    42,    43,    34,    35,
      36,    68,    37,    72,   211,   128,   129,   130,   166,   216,
     191,   168,   169,   217,   241,   218,   219,   186,   187,    38,
     113,    39,   124,    75,    40,    71,   156,   157,   158,   208,
     238,   159,   183,   160,   170,   202,   203,   204,   205,   206,
     220,   126,    45,   221,   229,   237
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      69,    69,   167,    73,    41,    70,   132,   133,   134,   135,
     154,   155,    65,    66,   184,   163,    74,    44,    79,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,   184,   136,
     230,   190,   190,   233,    67,    77,    69,   165,   105,   146,
     103,   147,   148,   149,   150,   151,   152,    80,    82,    83,
     167,    84,    85,   171,   242,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   242,   176,   177,   178,   179,
       1,     2,   106,   131,    86,    87,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    88,    89,    23,    24,
      25,    65,    66,    90,    91,   163,   213,    26,   137,   138,
      92,   171,   224,   225,   226,   227,   214,   215,   164,   139,
     140,   141,   164,   143,   144,   145,    27,    93,   131,   188,
     228,    28,    94,    67,    95,    96,   172,   165,    97,   173,
     174,   175,    98,    99,   100,   101,   102,   104,   132,   133,
     134,   135,   132,   133,   134,   135,   125,   127,   142,   228,
     193,   194,   195,   196,   197,   198,   199,   200,   201,   107,
     153,   108,   109,   161,   110,   111,   231,   112,   162,   188,
     180,   236,   181,   189,   234,   210,   235,   212,    78,   164,
     240,   192,   245,   246,    81,   185,   239,   222,   243,   247,
     232,    76,   207,   209,   182,   244,     0,   223
};

static const yytype_int16 yycheck[] =
{
      23,    24,   131,    26,     8,    24,    76,    77,    78,    79,
      38,    39,    29,    30,   160,    29,    61,     6,    41,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,   184,   107,
     210,   170,   171,   213,    61,     0,    69,    61,    71,   117,
      69,   118,   119,   120,   121,   122,   123,     5,     5,     5,
     189,     5,     5,   131,   234,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,   245,   143,   144,   145,   146,
       3,     4,    42,   106,     5,     5,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,     5,     5,    31,    32,
      33,    29,    30,     5,     5,    29,    34,    40,   108,   109,
       5,   189,   203,   204,   205,   206,    44,    45,    46,   110,
     111,   112,    46,   114,   115,   116,    59,     5,   161,   162,
     208,    64,     5,    61,     5,     5,   136,    61,     5,   139,
     140,   141,     5,     5,     5,     5,     5,     5,    76,    77,
      78,    79,    76,    77,    78,    79,     7,    34,     5,   237,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    60,
       5,    62,    63,    37,    65,    66,     5,    68,    43,   212,
      35,     5,    36,    36,    36,    41,    34,    37,    31,    46,
      35,   171,    36,    35,    43,   161,   229,   189,   235,   245,
     212,    28,   181,   184,   159,   237,    -1,   202
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     4,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    31,    32,    33,    40,    59,    64,    81,
      82,    83,    84,    85,    88,    89,    90,    92,   109,   111,
     114,     8,    86,    87,     6,   132,   132,   132,   132,   132,
     132,   132,   132,   132,   132,   132,   132,   132,   132,   132,
     132,   132,   132,   132,   132,    29,    30,    61,    91,   130,
      91,   115,    93,   130,    61,   113,   113,     0,    82,   130,
       5,    86,     5,     5,     5,     5,     5,     5,     5,     5,
       5,     5,     5,     5,     5,     5,     5,     5,     5,     5,
       5,     5,     5,    91,     5,   130,    42,    60,    62,    63,
      65,    66,    68,   110,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,   112,     7,   131,    34,    95,    96,
      97,   130,    76,    77,    78,    79,   133,   110,   110,   131,
     131,   131,     5,   131,   131,   131,   133,   112,   112,   112,
     112,   112,   112,     5,    38,    39,   116,   117,   118,   121,
     123,    37,    43,    29,    46,    61,    98,    99,   101,   102,
     124,   133,   110,   110,   110,   110,   112,   112,   112,   112,
      35,    36,   124,   122,   123,    97,   107,   108,   130,    36,
      99,   100,   100,    47,    48,    49,    50,    51,    52,    53,
      54,    55,   125,   126,   127,   128,   129,   117,   119,   122,
      41,    94,    37,    34,    44,    45,    99,   103,   105,   106,
     130,   133,   102,   132,   125,   125,   125,   125,   133,   134,
     105,     5,   107,   105,    36,    34,     5,   135,   120,   130,
      35,   104,   105,   103,   134,    36,    35,   104
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_uint8 yyr1[] =
{
       0,    80,    81,    82,    82,    83,    83,    83,    83,    83,
      83,    83,    83,    83,    84,    85,    86,    86,    87,    88,
      88,    88,    88,    88,    88,    88,    88,    88,    88,    88,
      88,    88,    88,    88,    88,    88,    88,    88,    88,    89,
      90,    91,    91,    92,    93,    93,    94,    94,    95,    96,
      96,    97,    97,    98,    98,    98,    99,   100,   100,   101,
     101,   102,   102,   103,   103,   103,   104,   104,   105,   105,
     105,   105,   105,   105,   106,   107,   107,   107,   108,   109,
     110,   110,   110,   110,   110,   110,   110,   111,   112,   112,
     112,   112,   112,   112,   112,   112,   112,   112,   112,   113,
     115,   114,   116,   116,   117,   117,   119,   118,   120,   120,
     121,   122,   122,   123,   123,   124,   124,   125,   125,   125,
     125,   125,   126,   126,   127,   127,   128,   128,   129,   129,
     129,   130,   130,   130,   131,   132,   133,   133,   133,   133,
     134,   135,   134
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     1,     0,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     3,     1,     0,     2,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     0,     2,     8,     0,     1,     0,     2,     2,     0,
       1,     1,     3,     2,     2,     1,     1,     0,     1,     0,
       1,     1,     3,     0,     1,     3,     1,     3,     1,     1,
       1,     1,     3,     1,     4,     0,     1,     3,     2,     4,
       0,     3,     3,     3,     3,     2,     2,     4,     0,     3,
       3,     3,     3,     2,     2,     2,     2,     2,     2,     1,
       0,     9,     0,     1,     1,     3,     0,     5,     0,     1,
       2,     0,     2,     1,     1,     1,     1,     0,     2,     2,
       2,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       0,     0,     3
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = JITTERC_EMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == JITTERC_EMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (&yylloc, vm, jitterc_scanner, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use JITTERC_error or JITTERC_UNDEF. */
#define YYERRCODE JITTERC_UNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if JITTERC_DEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YYLOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YYLOCATION_PRINT

#  if defined YY_LOCATION_PRINT

   /* Temporary convenience wrapper in case some people defined the
      undocumented and private YY_LOCATION_PRINT macros.  */
#   define YYLOCATION_PRINT(File, Loc)  YY_LOCATION_PRINT(File, *(Loc))

#  elif defined JITTERC_LTYPE_IS_TRIVIAL && JITTERC_LTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  int res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
}

#   define YYLOCATION_PRINT  yy_location_print_

    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT(File, Loc)  YYLOCATION_PRINT(File, &(Loc))

#  else

#   define YYLOCATION_PRINT(File, Loc) ((void) 0)
    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT  YYLOCATION_PRINT

#  endif
# endif /* !defined YYLOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, Location, vm, jitterc_scanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct jitterc_vm *vm, void* jitterc_scanner)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  YY_USE (yylocationp);
  YY_USE (vm);
  YY_USE (jitterc_scanner);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct jitterc_vm *vm, void* jitterc_scanner)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  YYLOCATION_PRINT (yyo, yylocationp);
  YYFPRINTF (yyo, ": ");
  yy_symbol_value_print (yyo, yykind, yyvaluep, yylocationp, vm, jitterc_scanner);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp,
                 int yyrule, struct jitterc_vm *vm, void* jitterc_scanner)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)],
                       &(yylsp[(yyi + 1) - (yynrhs)]), vm, jitterc_scanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, vm, jitterc_scanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !JITTERC_DEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !JITTERC_DEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, struct jitterc_vm *vm, void* jitterc_scanner)
{
  YY_USE (yyvaluep);
  YY_USE (yylocationp);
  YY_USE (vm);
  YY_USE (jitterc_scanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (struct jitterc_vm *vm, void* jitterc_scanner)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined JITTERC_LTYPE_IS_TRIVIAL && JITTERC_LTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

    /* The location stack: array, bottom, top.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls = yylsa;
    YYLTYPE *yylsp = yyls;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[3];



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = JITTERC_EMPTY; /* Cause a token to be read.  */

  yylsp[0] = yylloc;
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yyls1, yysize * YYSIZEOF (*yylsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
        yyls = yyls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == JITTERC_EMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, &yylloc, jitterc_scanner);
    }

  if (yychar <= JITTERC_EOF)
    {
      yychar = JITTERC_EOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == JITTERC_error)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = JITTERC_UNDEF;
      yytoken = YYSYMBOL_YYerror;
      yyerror_range[1] = yylloc;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;

  /* Discard the shifted token.  */
  yychar = JITTERC_EMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  yyerror_range[1] = yyloc;
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 15: /* legal_notice: LEGAL_NOTICE  */
#line 370 "../../jitter/jitterc/jitterc.y"
  { /* Add the current text, temporarily disabling #lineno directives even if
       they are enabled for C code.  The non-reentrancy is dirty but acceptable
       in this code generator, meant to always remain single-threaded. */
    bool old_generate_line = vm->generate_line;
    vm->generate_line = false;
    (yyval.code_block) = jitterc_make_code_struct (vm, JITTERC_TEXT, JITTERC_LINENO);
    JITTERC_APPEND_CODE(vm->legal_notice, & (yyval.code_block));
    vm->generate_line = old_generate_line; }
#line 1673 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 18: /* setting: SET identifier string  */
#line 387 "../../jitter/jitterc/jitterc.y"
                         { jitterc_vm_add_setting (vm, (yyvsp[-1].string), (yyvsp[0].string));
                           free ((yyvsp[-1].string)); }
#line 1680 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 19: /* c_section: INITIAL_HEADER_C code END  */
#line 393 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->initial_header_c_code, & (yyvsp[-1].code_block)); }
#line 1686 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 20: /* c_section: INITIAL_VM1_C code END  */
#line 395 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->initial_vm1_c_code, & (yyvsp[-1].code_block)); }
#line 1692 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 21: /* c_section: INITIAL_VM2_C code END  */
#line 397 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->initial_vm2_c_code, & (yyvsp[-1].code_block)); }
#line 1698 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 22: /* c_section: INITIAL_VM_MAIN_C code END  */
#line 399 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->initial_vm_main_c_code, & (yyvsp[-1].code_block)); }
#line 1704 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 23: /* c_section: EARLY_HEADER_C code END  */
#line 401 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->early_header_c_code, & (yyvsp[-1].code_block)); }
#line 1710 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 24: /* c_section: LATE_HEADER_C code END  */
#line 403 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->late_header_c_code, & (yyvsp[-1].code_block)); }
#line 1716 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 25: /* c_section: PRINTER_C code END  */
#line 405 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->printer_c_code, & (yyvsp[-1].code_block)); }
#line 1722 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 26: /* c_section: REWRITER_C code END  */
#line 407 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->rewriter_c_code, & (yyvsp[-1].code_block)); }
#line 1728 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 27: /* c_section: EARLY_C code END  */
#line 409 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->early_c_code, & (yyvsp[-1].code_block)); }
#line 1734 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 28: /* c_section: LATE_C code END  */
#line 411 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->before_main_c_code, & (yyvsp[-1].code_block)); }
#line 1740 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 29: /* c_section: INITIALIZATION_C code END  */
#line 413 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->initialization_c_code, & (yyvsp[-1].code_block)); }
#line 1746 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 30: /* c_section: FINALIZATION_C code END  */
#line 415 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->finalization_c_code, & (yyvsp[-1].code_block)); }
#line 1752 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 31: /* c_section: STATE_EARLY_C code END  */
#line 417 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->state_early_c_code, & (yyvsp[-1].code_block)); }
#line 1758 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 32: /* c_section: STATE_BACKING_STRUCT_C code END  */
#line 419 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->state_backing_struct_c_code, & (yyvsp[-1].code_block)); }
#line 1764 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 33: /* c_section: STATE_RUNTIME_STRUCT_C code END  */
#line 421 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->state_runtime_struct_c_code, & (yyvsp[-1].code_block)); }
#line 1770 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 34: /* c_section: STATE_INITIALIZATION_C code END  */
#line 423 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->state_initialization_c_code, & (yyvsp[-1].code_block)); }
#line 1776 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 35: /* c_section: STATE_RESET_C code END  */
#line 425 "../../jitter/jitterc/jitterc.y"
    { /* This field is NULL at initialisation. */
      if (vm->state_reset_c_code == NULL)
        vm->state_reset_c_code = jitter_clone_string ("");
      JITTERC_APPEND_CODE(vm->state_reset_c_code, & (yyvsp[-1].code_block)); }
#line 1785 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 36: /* c_section: STATE_FINALIZATION_C code END  */
#line 430 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->state_finalization_c_code, & (yyvsp[-1].code_block)); }
#line 1791 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 37: /* c_section: INSTRUCTION_BEGINNING_C code END  */
#line 432 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->instruction_beginning_c_code, & (yyvsp[-1].code_block)); }
#line 1797 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 38: /* c_section: INSTRUCTION_END_C code END  */
#line 434 "../../jitter/jitterc/jitterc.y"
    { JITTERC_APPEND_CODE(vm->instruction_end_c_code, & (yyvsp[-1].code_block)); }
#line 1803 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 39: /* wrapped_functions_section: WRAPPED_FUNCTIONS identifiers END  */
#line 439 "../../jitter/jitterc/jitterc.y"
  { jitterc_clone_list_from (vm->wrapped_functions, (yyvsp[-1].string_list)); }
#line 1809 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 40: /* wrapped_globals_section: WRAPPED_GLOBALS identifiers END  */
#line 444 "../../jitter/jitterc/jitterc.y"
  { jitterc_clone_list_from (vm->wrapped_globals, (yyvsp[-1].string_list)); }
#line 1815 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 41: /* identifiers: %empty  */
#line 448 "../../jitter/jitterc/jitterc.y"
                          { (yyval.string_list) = gl_list_nx_create_empty (GL_ARRAY_LIST,
                                                          NULL, NULL, NULL,
                                                          true); }
#line 1823 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 42: /* identifiers: identifier identifiers  */
#line 451 "../../jitter/jitterc/jitterc.y"
                          { gl_list_add_last ((yyvsp[0].string_list), (yyvsp[-1].string));
                            (yyval.string_list) = (yyvsp[0].string_list); }
#line 1830 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 43: /* rule_section: RULE optional_identifier REWRITE rule_instruction_patterns_zero_or_more INTO rule_instruction_templates_zero_or_more rule_guard END  */
#line 461 "../../jitter/jitterc/jitterc.y"
  { struct jitterc_rule *rule
      = jitterc_make_rule ((yyvsp[-4].instruction_patterns),
                           (yyvsp[-2].instruction_templates),
                           (yyvsp[-1].template_expression),
                           ((yyvsp[-6].string) != NULL
                            ? (yyvsp[-6].string)
                            : jitter_clone_string ("unnamed")),
                           JITTERC_LINENO);
    jitterc_add_rule (vm, rule); }
#line 1844 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 44: /* optional_identifier: %empty  */
#line 473 "../../jitter/jitterc/jitterc.y"
                 { (yyval.string) = NULL; }
#line 1850 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 45: /* optional_identifier: identifier  */
#line 474 "../../jitter/jitterc/jitterc.y"
                 { (yyval.string) = (yyvsp[0].string); }
#line 1856 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 46: /* rule_guard: %empty  */
#line 478 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = jitterc_make_template_expression_boolean (true, JITTERC_LINENO); }
#line 1862 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 47: /* rule_guard: WHEN rule_expression  */
#line 480 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = (yyvsp[0].template_expression); }
#line 1868 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 48: /* rule_instruction_pattern: identifier rule_argument_patterns_zero_or_more  */
#line 485 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_pattern) = jitterc_make_instruction_pattern ((yyvsp[-1].string), (yyvsp[0].argument_patterns), JITTERC_LINENO); }
#line 1874 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 49: /* rule_instruction_patterns_zero_or_more: %empty  */
#line 490 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_patterns) = jitterc_make_empty_list (); }
#line 1880 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 50: /* rule_instruction_patterns_zero_or_more: rule_instruction_patterns_one_or_more  */
#line 492 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_patterns) = (yyvsp[0].instruction_patterns); }
#line 1886 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 51: /* rule_instruction_patterns_one_or_more: rule_instruction_pattern  */
#line 497 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_patterns) = jitterc_make_empty_list ();
    gl_list_add_last ((yyval.instruction_patterns), (yyvsp[0].instruction_pattern)); }
#line 1893 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 52: /* rule_instruction_patterns_one_or_more: rule_instruction_pattern SEMICOLON rule_instruction_patterns_one_or_more  */
#line 500 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_patterns) = (yyvsp[0].instruction_patterns);
    gl_list_add_first ((yyval.instruction_patterns), (yyvsp[-2].instruction_pattern)); }
#line 1900 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 53: /* rule_argument_pattern: bare_argument optional_placeholder  */
#line 506 "../../jitter/jitterc/jitterc.y"
  {
    union jitter_word irrelevant;
    (yyval.argument_pattern) = jitterc_make_argument_pattern ((yyvsp[-1].bare_argument).kind,
                                        false,
                                        irrelevant,
                                        (yyvsp[0].string),
                                        JITTERC_LINENO);
  }
#line 1913 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 54: /* rule_argument_pattern: literal optional_placeholder  */
#line 515 "../../jitter/jitterc/jitterc.y"
  {
    union jitter_word literal = { .fixnum = (yyvsp[-1].fixnum) };
    enum jitterc_instruction_argument_kind literal_kind
      = jitterc_instruction_argument_kind_literal;
    (yyval.argument_pattern) = jitterc_make_argument_pattern (literal_kind,
                                        true,
                                        literal,
                                        (yyvsp[0].string),
                                        JITTERC_LINENO);
  }
#line 1928 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 55: /* rule_argument_pattern: placeholder  */
#line 526 "../../jitter/jitterc/jitterc.y"
  {
    union jitter_word irrelevant;
    enum jitterc_instruction_argument_kind any_kind
      = jitterc_instruction_argument_kind_unspecified;
    (yyval.argument_pattern) = jitterc_make_argument_pattern (any_kind,
                                        false,
                                        irrelevant,
                                        (yyvsp[0].string),
                                        JITTERC_LINENO);
  }
#line 1943 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 56: /* placeholder: RULE_PLACEHOLDER  */
#line 539 "../../jitter/jitterc/jitterc.y"
                    { /* Strip away the prefix. */
                      (yyval.string) = jitter_clone_string (JITTERC_TEXT + 1); }
#line 1950 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 57: /* optional_placeholder: %empty  */
#line 544 "../../jitter/jitterc/jitterc.y"
                 { (yyval.string) = NULL; }
#line 1956 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 58: /* optional_placeholder: placeholder  */
#line 545 "../../jitter/jitterc/jitterc.y"
                 { (yyval.string) = (yyvsp[0].string); }
#line 1962 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 59: /* rule_argument_patterns_zero_or_more: %empty  */
#line 550 "../../jitter/jitterc/jitterc.y"
  { (yyval.argument_patterns) = jitterc_make_empty_list (); }
#line 1968 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 60: /* rule_argument_patterns_zero_or_more: rule_argument_patterns_one_or_more  */
#line 552 "../../jitter/jitterc/jitterc.y"
  { (yyval.argument_patterns) = (yyvsp[0].argument_patterns); }
#line 1974 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 61: /* rule_argument_patterns_one_or_more: rule_argument_pattern  */
#line 557 "../../jitter/jitterc/jitterc.y"
  { (yyval.argument_patterns) = jitterc_make_empty_list ();
    gl_list_add_last ((yyval.argument_patterns), (yyvsp[0].argument_pattern)); }
#line 1981 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 62: /* rule_argument_patterns_one_or_more: rule_argument_pattern COMMA rule_argument_patterns_one_or_more  */
#line 560 "../../jitter/jitterc/jitterc.y"
  { (yyval.argument_patterns) = (yyvsp[0].argument_patterns);
    gl_list_add_first ((yyval.argument_patterns), (yyvsp[-2].argument_pattern)); }
#line 1988 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 63: /* rule_expressions_zero_or_more: %empty  */
#line 566 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expressions) = jitterc_make_empty_list (); }
#line 1994 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 64: /* rule_expressions_zero_or_more: rule_expression  */
#line 568 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expressions) = jitterc_make_empty_list ();
    gl_list_add_last ((yyval.template_expressions), (yyvsp[0].template_expression)); }
#line 2001 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 65: /* rule_expressions_zero_or_more: rule_expression COMMA rule_expressions_one_or_more  */
#line 571 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expressions) = (yyvsp[0].template_expressions);
    gl_list_add_first ((yyval.template_expressions), (yyvsp[-2].template_expression)); }
#line 2008 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 66: /* rule_expressions_one_or_more: rule_expression  */
#line 577 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expressions) = jitterc_make_empty_list ();
    gl_list_add_last ((yyval.template_expressions), (yyvsp[0].template_expression)); }
#line 2015 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 67: /* rule_expressions_one_or_more: rule_expression COMMA rule_expressions_one_or_more  */
#line 580 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expressions) = (yyvsp[0].template_expressions);
    gl_list_add_first ((yyval.template_expressions), (yyvsp[-2].template_expression)); }
#line 2022 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 68: /* rule_expression: TRUE_  */
#line 586 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = jitterc_make_template_expression_boolean (true, JITTERC_LINENO); }
#line 2028 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 69: /* rule_expression: FALSE_  */
#line 588 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = jitterc_make_template_expression_boolean (false, JITTERC_LINENO); }
#line 2034 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 70: /* rule_expression: literal  */
#line 590 "../../jitter/jitterc/jitterc.y"
  { union jitter_word w = { .fixnum = (yyvsp[0].fixnum) };
    (yyval.template_expression) = jitterc_make_template_expression_fixnum (w, JITTERC_LINENO); }
#line 2041 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 71: /* rule_expression: placeholder  */
#line 593 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = jitterc_make_template_expression_placeholder ((yyvsp[0].string), JITTERC_LINENO); }
#line 2047 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 72: /* rule_expression: OPEN_PAREN rule_expression CLOSE_PAREN  */
#line 595 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = (yyvsp[-1].template_expression); }
#line 2053 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 73: /* rule_expression: rule_operation  */
#line 597 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = (yyvsp[0].template_expression); }
#line 2059 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 74: /* rule_operation: identifier OPEN_PAREN rule_expressions_zero_or_more CLOSE_PAREN  */
#line 602 "../../jitter/jitterc/jitterc.y"
  { (yyval.template_expression) = jitterc_make_template_expression_operation ((yyvsp[-3].string), (yyvsp[-1].template_expressions), JITTERC_LINENO); }
#line 2065 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 75: /* rule_instruction_templates_zero_or_more: %empty  */
#line 607 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_templates) = jitterc_make_empty_list (); }
#line 2071 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 76: /* rule_instruction_templates_zero_or_more: rule_instruction_template  */
#line 609 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_templates) = jitterc_make_empty_list ();
    gl_list_add_last ((yyval.instruction_templates), (yyvsp[0].instruction_template)); }
#line 2078 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 77: /* rule_instruction_templates_zero_or_more: rule_instruction_template SEMICOLON rule_instruction_templates_zero_or_more  */
#line 614 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_templates) = (yyvsp[0].instruction_templates);
    gl_list_add_first ((yyval.instruction_templates), (yyvsp[-2].instruction_template)); }
#line 2085 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 78: /* rule_instruction_template: identifier rule_expressions_zero_or_more  */
#line 620 "../../jitter/jitterc/jitterc.y"
  { (yyval.instruction_template) = jitterc_make_instruction_template ((yyvsp[-1].string), (yyvsp[0].template_expressions), JITTERC_LINENO); }
#line 2091 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 79: /* register_class_section: REGISTER_CLASS register_or_stack_letter register_class_section_contents END  */
#line 625 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_register_class_set_letter ((yyvsp[-1].register_class), (yyvsp[-2].character));
      jitterc_vm_add_register_class (vm, (yyvsp[-1].register_class)); }
#line 2098 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 80: /* register_class_section_contents: %empty  */
#line 631 "../../jitter/jitterc/jitterc.y"
    { (yyval.register_class) = jitterc_make_register_class (); }
#line 2104 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 81: /* register_class_section_contents: LONG_NAME string register_class_section_contents  */
#line 633 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_register_class_set_long_name ((yyvsp[0].register_class), (yyvsp[-1].string));
      (yyval.register_class) = (yyvsp[0].register_class); }
#line 2111 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 82: /* register_class_section_contents: C_TYPE string register_class_section_contents  */
#line 636 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_register_class_set_c_type ((yyvsp[0].register_class), (yyvsp[-1].string));
      (yyval.register_class) = (yyvsp[0].register_class); }
#line 2118 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 83: /* register_class_section_contents: C_INITIAL_VALUE string register_class_section_contents  */
#line 639 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_register_class_set_c_initial_value ((yyvsp[0].register_class), (yyvsp[-1].string));
      (yyval.register_class) = (yyvsp[0].register_class); }
#line 2125 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 84: /* register_class_section_contents: FAST_REGISTER_NO literal register_class_section_contents  */
#line 642 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_register_class_set_fast_register_no ((yyvsp[0].register_class), (yyvsp[-1].fixnum));
      (yyval.register_class) = (yyvsp[0].register_class); }
#line 2132 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 85: /* register_class_section_contents: NO_SLOW_REGISTERS register_class_section_contents  */
#line 645 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_register_class_set_use_slow_registers ((yyvsp[0].register_class), 0);
      (yyval.register_class) = (yyvsp[0].register_class); }
#line 2139 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 86: /* register_class_section_contents: SLOW_REGISTERS register_class_section_contents  */
#line 648 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_register_class_set_use_slow_registers ((yyvsp[0].register_class), 1);
      (yyval.register_class) = (yyvsp[0].register_class); }
#line 2146 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 87: /* stack_section: STACK register_or_stack_letter stack_section_contents END  */
#line 654 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_letter ((yyvsp[-1].stack), (yyvsp[-2].character));
      jitterc_vm_add_stack (vm, (yyvsp[-1].stack)); }
#line 2153 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 88: /* stack_section_contents: %empty  */
#line 660 "../../jitter/jitterc/jitterc.y"
    { (yyval.stack) = jitterc_vm_make_stack (); }
#line 2159 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 89: /* stack_section_contents: LONG_NAME string stack_section_contents  */
#line 662 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_long_name ((yyvsp[0].stack), (yyvsp[-1].string));
      (yyval.stack) = (yyvsp[0].stack); }
#line 2166 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 90: /* stack_section_contents: C_ELEMENT_TYPE string stack_section_contents  */
#line 665 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_c_element_type ((yyvsp[0].stack), (yyvsp[-1].string));
      (yyval.stack) = (yyvsp[0].stack); }
#line 2173 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 91: /* stack_section_contents: ELEMENT_NO literal stack_section_contents  */
#line 668 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_element_no ((yyvsp[0].stack), (yyvsp[-1].fixnum));
      (yyval.stack) = (yyvsp[0].stack); }
#line 2180 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 92: /* stack_section_contents: C_INITIAL_VALUE string stack_section_contents  */
#line 671 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_c_initial_value ((yyvsp[0].stack), (yyvsp[-1].string));
      (yyval.stack) = (yyvsp[0].stack); }
#line 2187 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 93: /* stack_section_contents: NO_GUARD_UNDERFLOW stack_section_contents  */
#line 674 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_guard_underflow ((yyvsp[0].stack), 0);
      (yyval.stack) = (yyvsp[0].stack); }
#line 2194 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 94: /* stack_section_contents: NO_GUARD_OVERFLOW stack_section_contents  */
#line 677 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_guard_overflow ((yyvsp[0].stack), 0);
      (yyval.stack) = (yyvsp[0].stack); }
#line 2201 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 95: /* stack_section_contents: GUARD_UNDERFLOW stack_section_contents  */
#line 680 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_guard_underflow ((yyvsp[0].stack), 1);
      (yyval.stack) = (yyvsp[0].stack); }
#line 2208 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 96: /* stack_section_contents: GUARD_OVERFLOW stack_section_contents  */
#line 683 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_guard_overflow ((yyvsp[0].stack), 1);
      (yyval.stack) = (yyvsp[0].stack); }
#line 2215 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 97: /* stack_section_contents: TOS_OPTIMIZED stack_section_contents  */
#line 686 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_implementation ((yyvsp[0].stack), jitterc_stack_implementation_tos);
      (yyval.stack) = (yyvsp[0].stack); }
#line 2222 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 98: /* stack_section_contents: NON_TOS_OPTIMIZED stack_section_contents  */
#line 689 "../../jitter/jitterc/jitterc.y"
    { jitterc_vm_stack_set_implementation ((yyvsp[0].stack),
                                           jitterc_stack_implementation_no_tos);
      (yyval.stack) = (yyvsp[0].stack); }
#line 2230 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 99: /* register_or_stack_letter: REGISTER_OR_STACK_LETTER  */
#line 695 "../../jitter/jitterc/jitterc.y"
                           { (yyval.character) = JITTERC_TEXT [0]; }
#line 2236 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 100: /* $@1: %empty  */
#line 700 "../../jitter/jitterc/jitterc.y"
  { jitterc_vm_append_instruction (vm, jitterc_make_instruction ()); }
#line 2242 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 101: /* instruction_section: INSTRUCTION $@1 identifier OPEN_PAREN arguments CLOSE_PAREN properties code END  */
#line 702 "../../jitter/jitterc/jitterc.y"
  { /* Make an instruction, and initialize its fields. */
    struct jitterc_instruction *ins = jitterc_vm_last_instruction (vm);
    ins->name = (yyvsp[-6].string);
    ins->mangled_name = jitterc_mangle (ins->name);
    ins->code = (yyvsp[-1].code_block).code;
    /* The arguments have already been added one by one by the argument rule. */
    if (ins->hotness == jitterc_hotness_unspecified)
      ins->hotness = jitterc_hotness_hot;
    if (ins->relocatability == jitterc_relocatability_unspecified)
      ins->relocatability = jitterc_relocatability_relocatable;
    if (ins->callerness == jitterc_callerness_unspecified)
      ins->callerness = jitterc_callerness_non_caller;
    if (ins->calleeness == jitterc_calleeness_unspecified)
      ins->calleeness = jitterc_calleeness_non_callee;
    if (ins->returningness == jitterc_returningness_unspecified)
      ins->returningness = jitterc_returningness_non_returning;
    if (ins->has_fast_labels)
      /* An instruction with fast labels can also use non-fast branches, without
         explicitly specifying attributes (Rationale: the branching attribute
         serves to recognise the instruction as potentially defective, and an
         instruction with fast label is potentially defective already). */
      ins->branchingness = jitterc_branchingness_branching;
    if (   ins->has_fast_labels
        && ins->relocatability == jitterc_relocatability_non_relocatable)
      /* FIXME: I might want to allow this. */
      JITTERC_PARSE_ERROR("a non-relocatable instruction has fast labels");
    if (   ins->callerness == jitterc_callerness_caller
        && ins->relocatability == jitterc_relocatability_non_relocatable)
      /* FIXME: I might want to allow this as well. */
      JITTERC_PARSE_ERROR("non-relocatable instructions cannot (currently) be callers");

    /* An instruction which is a caller, callee or returning is also
       automatically branching.  Rationale: the macros needed for branching
       and their replacements are the same also used for procedures. */
    if (   ins->callerness == jitterc_callerness_caller
        || ins->calleeness == jitterc_calleeness_callee
        || ins->returningness == jitterc_returningness_returning)
      {
        if (ins->branchingness == jitterc_branchingness_non_branching)
          JITTERC_PARSE_ERROR ("a non-branching instruction cannot be caller, callee or returning");
        else
          ins->branchingness = jitterc_branchingness_branching;
      }

    /* Branchingness can be affected by callerness, calleeness and
       returningness; only now we can decide on the default branchingness, if
       itsvalue was not explicitly specified. */
    if (ins->branchingness == jitterc_branchingness_unspecified)
      ins->branchingness = jitterc_branchingness_non_branching;
  }
#line 2297 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 106: /* $@2: %empty  */
#line 766 "../../jitter/jitterc/jitterc.y"
    { struct jitterc_instruction_argument *arg
        = jitterc_make_instruction_argument ();
      arg->mode = (yyvsp[-1].mode);
      arg->kind = (yyvsp[0].bare_argument).kind;
      if (arg->kind & jitterc_instruction_argument_kind_register)
        arg->register_class_character = (yyvsp[0].bare_argument).register_class_letter;
      if (arg->kind & jitterc_instruction_argument_kind_literal)
        {
          if (arg->mode & jitterc_instruction_argument_mode_out)
            JITTERC_PARSE_ERROR("a literal cannot be an output");

          /* FIXME: this might need to be generalized or cleaned up
             in the future. */
          arg->literal_type = jitterc_literal_type_fixnum;
        }
      if (arg->kind & jitterc_instruction_argument_kind_label)
        {
          if (arg->mode & jitterc_instruction_argument_mode_out)
            JITTERC_PARSE_ERROR("a label cannot be an output");
        }
      if (arg->kind & jitterc_instruction_argument_kind_fast_label)
        {
          jitterc_vm_last_instruction (vm)->has_fast_labels = true;
          if (arg->mode & jitterc_instruction_argument_mode_out)
            JITTERC_PARSE_ERROR("a fast label cannot be an output");
          if (arg->kind != jitterc_instruction_argument_kind_fast_label)
            JITTERC_PARSE_ERROR("a fast label must be the only kind");
        }
      jitterc_vm_append_argument (vm, arg);
    }
#line 2332 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 107: /* argument: modes bare_argument $@2 literals optional_printer_name  */
#line 797 "../../jitter/jitterc/jitterc.y"
    {
      struct jitterc_instruction_argument *arg
        = jitterc_vm_last_argument (vm);
      if (   ! (arg->kind & jitterc_instruction_argument_kind_literal)
             && (yyvsp[-1].boolean))
        JITTERC_PARSE_ERROR("literals for a non-literal argument");
      if (   ! (arg->kind & jitterc_instruction_argument_kind_literal)
          && ((yyvsp[0].string) != NULL))
        JITTERC_PARSE_ERROR("a non-literal argument cannot have a printer");
      arg->c_literal_printer_name = (yyvsp[0].string);
    }
#line 2348 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 108: /* optional_printer_name: %empty  */
#line 811 "../../jitter/jitterc/jitterc.y"
                 { (yyval.string) = NULL; }
#line 2354 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 109: /* optional_printer_name: identifier  */
#line 812 "../../jitter/jitterc/jitterc.y"
                 { (yyval.string) = (yyvsp[0].string); }
#line 2360 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 110: /* modes: mode_character modes_rest  */
#line 817 "../../jitter/jitterc/jitterc.y"
    { if ((yyvsp[-1].mode) & (yyvsp[0].mode))
        JITTERC_PARSE_ERROR("duplicate mode");
      (yyval.mode) = (yyvsp[-1].mode) | (yyvsp[0].mode); }
#line 2368 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 111: /* modes_rest: %empty  */
#line 824 "../../jitter/jitterc/jitterc.y"
    { (yyval.mode) = jitterc_instruction_argument_mode_unspecified; }
#line 2374 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 112: /* modes_rest: mode_character modes_rest  */
#line 826 "../../jitter/jitterc/jitterc.y"
    { if ((yyvsp[-1].mode) & (yyvsp[0].mode))
        JITTERC_PARSE_ERROR("duplicate mode");
      (yyval.mode) = (yyvsp[-1].mode) | (yyvsp[0].mode); }
#line 2382 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 113: /* mode_character: IN  */
#line 832 "../../jitter/jitterc/jitterc.y"
       { (yyval.mode) = jitterc_instruction_argument_mode_in; }
#line 2388 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 114: /* mode_character: OUT  */
#line 833 "../../jitter/jitterc/jitterc.y"
       { (yyval.mode) = jitterc_instruction_argument_mode_out; }
#line 2394 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 115: /* bare_argument: REGISTER_OR_STACK_LETTER  */
#line 841 "../../jitter/jitterc/jitterc.y"
  {
    enum jitterc_instruction_argument_kind k
      = jitterc_instruction_argument_kind_unspecified;
    char c = JITTERC_TEXT [0];
    switch (c)
      {
      KIND_CASE('n', literal)
      KIND_CASE('l', label)
      KIND_CASE('f', fast_label)
      KIND_CASE_DEFAULT((yyval.bare_argument), c)
      }
    (yyval.bare_argument).kind = k;
  }
#line 2412 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 116: /* bare_argument: BARE_ARGUMENT  */
#line 855 "../../jitter/jitterc/jitterc.y"
  {
    char *text = JITTERC_TEXT;
    enum jitterc_instruction_argument_kind k
      = jitterc_instruction_argument_kind_unspecified;
    int i;
    for (i = 0; text [i] != '\0'; i ++)
      switch (text [i])
        {
        KIND_CASE('n', literal)
        KIND_CASE('l', label)
        KIND_CASE('f', fast_label)
        KIND_CASE_DEFAULT((yyval.bare_argument), text [i])
        }
    (yyval.bare_argument).kind = k;
  }
#line 2432 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 122: /* hotness: HOT  */
#line 881 "../../jitter/jitterc/jitterc.y"
        { JITTERC_SET_PROPERTY(hotness, hot); }
#line 2438 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 123: /* hotness: COLD  */
#line 882 "../../jitter/jitterc/jitterc.y"
        { JITTERC_SET_PROPERTY(hotness, cold); }
#line 2444 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 124: /* relocatability: RELOCATABLE  */
#line 886 "../../jitter/jitterc/jitterc.y"
                  { JITTERC_SET_PROPERTY(relocatability, relocatable); }
#line 2450 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 125: /* relocatability: NON_RELOCATABLE  */
#line 887 "../../jitter/jitterc/jitterc.y"
                  { JITTERC_SET_PROPERTY(relocatability, non_relocatable); }
#line 2456 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 126: /* branchingness: NON_BRANCHING  */
#line 891 "../../jitter/jitterc/jitterc.y"
                { JITTERC_SET_PROPERTY(branchingness, non_branching); }
#line 2462 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 127: /* branchingness: BRANCHING  */
#line 892 "../../jitter/jitterc/jitterc.y"
                { JITTERC_SET_PROPERTY(branchingness, branching); }
#line 2468 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 128: /* callrelatedness: CALLER  */
#line 896 "../../jitter/jitterc/jitterc.y"
           { JITTERC_SET_PROPERTY(callerness, caller); }
#line 2474 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 129: /* callrelatedness: CALLEE  */
#line 897 "../../jitter/jitterc/jitterc.y"
           { JITTERC_SET_PROPERTY(calleeness, callee);
             const struct jitterc_instruction *ins = jitterc_vm_last_instruction (vm);
             if (gl_list_size (ins->arguments) > 0)
               JITTERC_PARSE_ERROR
                  ("a callee instruction cannot have arguments"); }
#line 2484 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 130: /* callrelatedness: RETURNING  */
#line 902 "../../jitter/jitterc/jitterc.y"
            { JITTERC_SET_PROPERTY(returningness, returning); }
#line 2490 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 131: /* identifier: BARE_ARGUMENT  */
#line 909 "../../jitter/jitterc/jitterc.y"
                            { (yyval.string) = JITTERC_TEXT_COPY; }
#line 2496 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 132: /* identifier: REGISTER_OR_STACK_LETTER  */
#line 910 "../../jitter/jitterc/jitterc.y"
                            { (yyval.string) = JITTERC_TEXT_COPY; }
#line 2502 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 133: /* identifier: IDENTIFIER  */
#line 911 "../../jitter/jitterc/jitterc.y"
                            { (yyval.string) = JITTERC_TEXT_COPY; }
#line 2508 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 134: /* string: STRING  */
#line 915 "../../jitter/jitterc/jitterc.y"
             { /* FIXME: unescape properly. */
               char *text = JITTERC_TEXT;
               text [strlen (text) - 1] = '\0'; text ++;
               (yyval.string) = jitter_clone_string (text); }
#line 2517 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 135: /* code: CODE  */
#line 923 "../../jitter/jitterc/jitterc.y"
  { (yyval.code_block) = jitterc_make_code_struct (vm, JITTERC_TEXT, JITTERC_LINENO); }
#line 2523 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 136: /* literal: FIXNUM  */
#line 927 "../../jitter/jitterc/jitterc.y"
                  { /* Since the string has been matched by the scanner
                       the conversion is actually safe in this case. */
                    (yyval.fixnum) = jitter_string_to_long_long_unsafe (JITTERC_TEXT); }
#line 2531 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 137: /* literal: BITSPERWORD  */
#line 930 "../../jitter/jitterc/jitterc.y"
                  { (yyval.fixnum) = JITTER_BITS_PER_WORD; }
#line 2537 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 138: /* literal: BYTESPERWORD  */
#line 931 "../../jitter/jitterc/jitterc.y"
                  { (yyval.fixnum) = JITTER_BYTES_PER_WORD; }
#line 2543 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 139: /* literal: LGBYTESPERWORD  */
#line 932 "../../jitter/jitterc/jitterc.y"
                  { (yyval.fixnum) = JITTER_LG_BYTES_PER_WORD; }
#line 2549 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 140: /* literals: %empty  */
#line 936 "../../jitter/jitterc/jitterc.y"
           { (yyval.boolean) = false; }
#line 2555 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 141: /* $@3: %empty  */
#line 937 "../../jitter/jitterc/jitterc.y"
           { struct jitterc_instruction_argument *arg
               = jitterc_vm_last_argument (vm);
             /* FIXME: this will need generalization later on. */
             union jitterc_literal_value literal_value
               = {.fixnum = (yyvsp[0].fixnum)};
             struct jitterc_literal *literal
               = jitterc_make_literal (jitterc_literal_type_fixnum,
                                         literal_value);
             gl_list_add_last (arg->literals, literal); }
#line 2569 "../../jitter/jitterc/jitterc-parser.c"
    break;

  case 142: /* literals: literal $@3 literals  */
#line 946 "../../jitter/jitterc/jitterc.y"
           { (yyval.boolean) = true; }
#line 2575 "../../jitter/jitterc/jitterc-parser.c"
    break;


#line 2579 "../../jitter/jitterc/jitterc-parser.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == JITTERC_EMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (&yylloc, vm, jitterc_scanner, YY_("syntax error"));
    }

  yyerror_range[1] = yylloc;
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= JITTERC_EOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == JITTERC_EOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, vm, jitterc_scanner);
          yychar = JITTERC_EMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, yylsp, vm, jitterc_scanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  ++yylsp;
  YYLLOC_DEFAULT (*yylsp, yyerror_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, vm, jitterc_scanner, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != JITTERC_EMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, vm, jitterc_scanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, yylsp, vm, jitterc_scanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 948 "../../jitter/jitterc/jitterc.y"


void
jitterc_error (YYLTYPE *locp, struct jitterc_vm *vm, yyscan_t jitterc_scanner,
                 char *message)
{
  printf ("%s:%i: %s near \"%s\".\n",
          (vm != NULL) ? vm->source_file_name : "<INPUT>",
          jitterc_get_lineno (jitterc_scanner), message, JITTERC_TEXT);
  exit (EXIT_FAILURE);
}

void
jitterc_scan_error (void *jitterc_scanner)
{
  struct jitterc_vm *vm = NULL; /* A little hack to have vm in scope. */
  JITTERC_PARSE_ERROR("scan error");
}

static struct jitterc_vm *
jitterc_parse_file_star_with_name (FILE *input_file, const char *file_name,
                                   bool generate_line)
{
  yyscan_t scanner;
  jitterc_lex_init (&scanner);
  jitterc_set_in (input_file, scanner);

  struct jitterc_vm *res = jitterc_make_vm ();
  res->source_file_name = jitter_clone_string (file_name);

  /* Set res->generate_line now, before the parsing phase actually starts.  This
     way the code generated at parsing time will be affected. */
  res->generate_line = generate_line;

  /* FIXME: if I ever make parsing errors non-fatal, call jitterc_lex_destroy before
     returning, and finalize the program -- which might be incomplete! */
  if (jitterc_parse (res, scanner))
    jitterc_error (jitterc_get_lloc (scanner), res, scanner, "parse error");
  jitterc_set_in (NULL, scanner);
  jitterc_lex_destroy (scanner);

  /* Now that we have all the unspecialized instructions and all the rules we
     can analyze the VM. */
  jitterc_analyze_vm (res);

  return res;
}

struct jitterc_vm *
jitterc_parse_file_star (FILE *input_file, bool generate_line)
{
  return jitterc_parse_file_star_with_name (input_file, "<stdin>",
                                            generate_line);
}

struct jitterc_vm *
jitterc_parse_file (const char *input_file_name, bool generate_line)
{
  FILE *f;
  if ((f = fopen (input_file_name, "r")) == NULL)
    jitter_fatal ("failed opening file %s", input_file_name);

  /* FIXME: if I ever make parse errors non-fatal, I'll need to close the file
     before returning. */
  struct jitterc_vm *res
    = jitterc_parse_file_star_with_name (f, input_file_name, generate_line);
  fclose (f);
  return res;
}
