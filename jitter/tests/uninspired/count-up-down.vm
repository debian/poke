# An (-*- asm -*-) Uninspired VM program incrementing and decrementing.
# Copyright (C) 2017 Luca Saiu
# Written by Luca Saiu

# This file is part of GNU Jitter.

# GNU Jitter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# GNU Jitter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with GNU Jitter.  If not, see <https://www.gnu.org/licenses/>.


# Initialize the register to be decremented to some number, and the other to be
# incremented to zero.
        mov 12345, %r0
        mov 0,     %r1

# While the first register is not equal to zero decrement it and increment the
# second.
$loop:
        beq %r0, 0, $out
        sub %r0, 1, %r0
        add %r1, 1, %r1
        b   $loop

# Print the second register value: it should be equal to the initial value of
# the first.
$out:
        printfixnum %r1
