## An -*- sh -*- script (to be preprocessed by M4sh and then by config.status:
## if the file extension of this file is not .in.m4sh then you are not looking
## at the actual source), part of Jitter.

## Print configuration info about Jitter and compiler/linker flags to be used
## for code generated by Jitter.

## Copyright (C) 2017-2021 Luca Saiu
## Updated in 2022 by Luca Saiu
## Written by Luca Saiu

## This file is part of GNU Jitter.

## GNU Jitter is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.

## GNU Jitter is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## You should have received a copy of the GNU General Public License
## along with GNU Jitter.  If not, see <https://www.gnu.org/licenses/>.


# M4sh initialization.
################################################################

# Include the M4sh script "library", part of every M4sh script here.  That
# library starts with the AS_INIT call.
m4_include([jitter-script.m4sh])

# Define the globals required by jitter-script.m4sh :
script_printed_name=jitter-config


# Global settings and main variables.
################################################################

# Print help information and exit with success.
help ()
{
    cat <<EOF
Usage: ${script_name} OPTION @<:@OPTION...@:>@

Print GNU Jitter configuration information and compiler/linker flags to be used
for generated code.

${help_section_indentation}Configuration options:
  --best-dispatch            Print the best enabled dispatch and exit
  --dispatches               Print all the enabled dispatches in order of
                             increasing efficiency, all on the same line, and
                             exit
  --has-dispatch=DISPATCH    Exit immediately, with success if the dispatch
                             named DISPATCH is enabed, with failure otherwise

${help_section_indentation}Dispatch selection:
  --dispatch=DISPATCH        Choose the given dispatch for the following
                             (default: the best one which is enabled); see
                             --dispatches for the list of enabled dispatches
                             and --best-dispatch for the best one

${help_section_indentation}Preprocessing, compilation and linking flags:
  --cflags                   Print C compiler flags for the chosen dispatch.
                             These flags are *mandatory* to use when compiling
                             the vmprefix-vm2.c file generated by Jitter, to
                             ensure correctness; while the same flags are also
                             safe to use on other files they could negatively
                             affect performance.
  --cppflags                 Print C preprocessor flags for the chosen
                             dispatch.
                             These flags must be used to preprocess any file
                             including any Jitter header.
  --ldadd                    Print C compiler (driver) '-l' options for liking
                             the chosen dispatch library.
                             These flags are to be supplied at link time when
                             linking the Jitter runtime library.
  --ldflags                  Print C compiler (driver) linking flags for
                             linking the chosen dispatch library.
                             These flags are to be supplied at link time when
                             linking the Jitter runtime library.
  --libadd                   Print arguments consisting in Libtool convenience
                             libraries, to be linked into a library rather
                             than a program.
                             These arguments are to be supplied at link time
                             when linking the Jitter runtime library.

${help_section_indentation}Installation path options:
  --template-path            Print the directory where template files are
                             installed, then exit
  --flag-path                Print the directory where flag files are
                             installed, containing one subdirectory per
                             dispatch, then exit
  --include-path             Print the full path of the directory containing
                             the jitter/ header directory, then exit

${help_section_indentation}Architecture options:
  --build                    Print the system build triplet and exit
  --host                     Print the system host triplet and exit
  --assembly                 Print the Jitter assembly name for the host
                             system, if any, and exit

${help_section_indentation}GNU Libtextstyle wrapper options:
  --have-libtextstyle        Exit immediately, with success if the
                             Libtextstyle wrapper is available, with failure
                             otherwise.
  --libtextstyle-cppflags    Print C preprocessor flags needed to actaully use
                             the GNU Libtextstyle wrapper, if available; print
                             nothing if the wrapper is not available.
                             The user should conditionalise her C code
                             checking whether JITTER_WITH_LIBTEXTSTYLE is
                             defined: the flags shown by this macros either
                             define it from the command line or do nothing.
  --libtextstyle-ldadd       Print C compiler (driver) '-l' options for
                             linking Jitter's Libtextstyle wrapper library and
                             the Libtextstyle library itself into a program.
                             These flags are to be supplied at link time when
                             linking a program using the wrapper.
  --libtextstyle-libadd      Print C compiler (driver) arguments for linking
                             Jitter's Libtextstyle wrapper library and
                             the Libtextstyle library itself into another
                             library, using GNU LibTool.
                             These arguments are to be supplied at link time
                             when linking a larger library which inclues the
                             wrapper.

${help_section_indentation}GNU Readline wrapper/replacement options:
  --readline-ldadd           Print C compiler (driver) '-l' options for
                             linking Jitter's Readline wrapper/replacement
                             library and GNU Readline itself (when used) into
                             a program.
                             These flags are to be supplied at link time when
                             linking a program using the wrapper/replacement.
  --readline-libadd          Print C compiler (driver) arguments for linking
                             Jitter's Readline wrapper/replcament library and
                             GNU Readline itself (when used) into another
                             library, using GNU LibTool.
                             These arguments are to be supplied at link time
                             when linking a larger library which inclues the
                             wrapper.

${help_section_indentation}Debugging and scripting:
  --dump-version             Print the Jitter version only, without any
                             surrounding text; this is convenient for scripts

${help_section_indentation}Common GNU-style options:
  -?, --help                 Give this help list
  -V, --version              Print program version

Only long-format options can have arguments.

Report @JITTER_PACKAGE_NAME@ bugs to: <@JITTER_PACKAGE_BUGREPORT@>.
@JITTER_PACKAGE_NAME@ home page: <@JITTER_PACKAGE_URL@>.
General help using GNU software: <https://www.gnu.org/gethelp/>.
EOF
    exit 0
}

# Print enabled dispatches, then exit with success.
dispatches_and_exit ()
{
    # The output should be a single line, which one space separating each
    # dispatch name, and no space at the beginning or the end.
    output=""
    for d in @JITTER_ENABLED_DISPATCHES@; do
        output="$output $d"
    done
    my_echo $output
    exit 0
}

# Print the best enabled dispatch, then exit with success.
best_dispatch_and_exit ()
{
    my_echo @JITTER_BEST_DISPATCH@
    exit 0
}


# Utility functions for printing.
################################################################

# Print a copy of stdin to stdout, suppressing spaces at the beginning and the
# at the end of lines, and contracting multiple-space sequences into single
# spaces.
# No arguments.
suppress_redundant_spaces ()
{
    @JITTER_HOST_SED@ 's/^ *//;s/ *$//;s/  */ /g'
}

# Append the value of the given option for $dispatch to the output variable.
# One argument: the option name, lower-case.
append_to_output ()
{
    flagname="$1"
    if test "x$dispatch" = "x"; then
        dispatch="@JITTER_BEST_DISPATCH@"
    else
        dispatch="$dispatch"
    fi
    case "$dispatch" in
jitter_for_dispatch([a_dispatch],
[         a_dispatch)
              case "$flagname" in
jitter_for_flag([a_flag],
[                 a_flag) new="@JITTER_[]jitter_tocpp(a_dispatch)[_]jitter_tocpp(a_flag)@";;])dnl jitter_for_flag
                  *) fail_semantic "impossible: flag $flagname for a_dispatch";;
              esac;;])dnl jitter_for_dispatch
        *)
            fail_semantic "this should never happen: dispatch $dispatch";;
    esac
    output="$output $new"
}

# Append the value of the given dispatch-independent option to the output
# variable.  One argument: the option name, lower-case.
append_to_output_dispatch_independent ()
{
    optionname="$1"
    case "$optionname" in
        libtextstyle-cppflags)
            new="@JITTER_LIBTEXTSTYLE_CPPFLAGS@";;
        libtextstyle-ldadd)
            new="@JITTER_LIBTEXTSTYLE_LDADD@";;
        libtextstyle-libadd)
            new="@JITTER_LIBTEXTSTYLE_LIBADD@";;
        readline-ldadd)
            new="@JITTER_READLINE_LDADD@";;
        readline-libadd)
            new="@JITTER_READLINE_LIBADD@";;
        *)
            fail_semantic "this should never happen: optionname $optionname";;
    esac
    output="$output $new"
}

# Utility functions for common checks.
################################################################

# Fail with a fatal error if the current value of $option_argument is different
# from "".  This is used from the main loop, after recognizing that the current
# option $option supports no argument.
no_option_argument ()
{
    if test "x$option_argument" != "x"; then
        fail_usage "the option $option has no argument"
    fi
}

# Fail with a fatal error if the current value of $option_argument is "".  This
# is used from the main loop, after recognizing that the current option $option
# requires a mandatory argument.
mandatory_option_argument ()
{
    if test "x$option_argument" = "x"; then
        fail_usage "the option $option has a mandatory argument"
    fi
}


# Command-line checks.
################################################################

# Fail if there are no command-line arguments.
if test "$#" = "0"; then
    fail_usage "no option given"
fi


# Main loop.
################################################################

# Initialize a string containing the output to be printed in the end.
output=""

# Scan every option left-to-right.
while test "$#" != "0"; do
    argument="$1"
    shift

    # Fail if the argument is not an option.
    if ! (my_echo "$argument" | grep '^-' > /dev/null); then
        fail_usage "$argument is a non-option argument"
    fi

    # At this point $argument is either an option or an option with an argument.
    # Split the two parts.
    if my_echo "$argument" | grep '=' > /dev/null; then
        # There is an option argument.  Split around the '=' character.
        # Notice that arguments with options only support the GNU-style syntax
        # --long-option=value ; the '=' character is part of $argument .  Short
        # options cannot have arguments here.
        option="$(my_echo $argument | @JITTER_HOST_SED@ 's/=.*$//')"
        option_argument="$(my_echo $argument | @JITTER_HOST_SED@ 's/^@<:@^=@:>@*=//')"
    else
        # There is no argument.
        option="$argument"
        option_argument=""
    fi

    case "$option" in
        # Configuration options
        --best-dispatch)
            no_option_argument
            best_dispatch_and_exit;;
        --dispatches)
            no_option_argument
            dispatches_and_exit;;
        --has-dispatch)
            mandatory_option_argument
            for m in @JITTER_ENABLED_DISPATCHES@; do
                if test "x$option_argument" = "x$m"; then
                    exit 0
                fi
            done
            exit 1;;

        # dispatch selection:
        --dispatch)
            mandatory_option_argument
            known="no"
            for m in @JITTER_ENABLED_DISPATCHES@; do
                if test "x$option_argument" = "x$m"; then
                    known="yes"
                fi
            done
            if test "x$known" != "xyes"; then
                fail_semantic "unknown or disabled dispatch \"$option_argument\""
            fi
            dispatch="$option_argument"
            ;;

        # Compilation/linking flag options for the best dispatch:
        --cflags)
            no_option_argument
            append_to_output cflags;;
        --cppflags)
            no_option_argument
            append_to_output cppflags
            # Unless cross-compiling, append a -I argument referring the
            # installation path.  This is defined separately from the rest, as
            # the installation prefix can be decided very late, at Jitter
            # installation time.
            # (When cross-compiling this would be difficult to do correctly.)
            if test "x@JITTER_CROSS_COMPILING@" != 'xyes'; then
              output="$output -I $includedir"
            fi;;
        --ldadd)
            no_option_argument
            append_to_output ldadd;;
        --ldflags)
            no_option_argument
            append_to_output ldflags;;
        --libadd)
            no_option_argument
            append_to_output libadd;;

        # Installation path options.
        --template-path)
            no_option_argument
            echo_and_exit "$templatedir";;
        --flag-path)
            no_option_argument
            echo_and_exit "$flagdir";;
        --include-path)
            no_option_argument
            echo_and_exit "$includedir";;

        # Architecture options.
        --host)
            no_option_argument
            echo_and_exit "@host@";;
        --build)
            no_option_argument
            echo_and_exit "@build@";;
        --assembly)
            no_option_argument
            if test "x@JITTER_ASSEMBLY_SUBDIRECTORY@" != 'xdummy'; then
                my_echo "@JITTER_ASSEMBLY_SUBDIRECTORY@"
            fi
            exit 0;;

        # GNU Libtextstyle wrapper options.
        --have-libtextstyle)
            no_option_argument
            if test "x@JITTER_LIBTEXTSTYLE_CPPFLAGS@" != 'x'; then
                exit 0
            else
                exit 1
            fi;;
        --libtextstyle-cppflags)
            no_option_argument
            append_to_output_dispatch_independent libtextstyle-cppflags;;
        --libtextstyle-ldadd)
            no_option_argument
            append_to_output_dispatch_independent libtextstyle-ldadd;;
        --libtextstyle-libadd)
            no_option_argument 
            append_to_output_dispatch_independent libtextstyle-libadd;;

        # GNU Readline wrapper (or replacement) options.
        --readline-ldadd)
            no_option_argument
            append_to_output_dispatch_independent readline-ldadd;;
        --readline-libadd)
            no_option_argument 
            append_to_output_dispatch_independent readline-libadd;;

        # Debugging and scripting options.
        --dump-version)
            no_option_argument
            echo_and_exit "@PACKAGE_VERSION@";;

        # Common GNU-style options.
        --version|-V)
            no_option_argument
            version;;
        --help|-\?)
            no_option_argument
            help;;

        # Unrecognized option.
        *)
            fail_usage "invalid option $option";;
    esac
done

# If we've not exited yet, we have composed an output to be printed at the end
# at this point.  Do that and we can exit with success.
# Here we suppress redundant spaces.
my_echo "$output" | suppress_redundant_spaces
