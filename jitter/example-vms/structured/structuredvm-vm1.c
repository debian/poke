/* This machine-generated file includes source code from GNU Jitter.

   Copyright (C) 2016-2021 Luca Saiu
   Written by Luca Saiu

   GNU Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   GNU Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Jitter.  If not, see <https://www.gnu.org/licenses/>.


  Copyright (C) 2017, 2019, 2020, 2021 Luca Saiu
  Copyright (C) 2021 pEp Foundation
  Written by Luca Saiu

  This file is part of Structured, a GNU Jitter example.

  Structured is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Structured is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Structured.  If not, see <https://www.gnu.org/licenses/>.

*/

/* User-specified code, initial vm1 part: beginning. */

/* User-specified code, initial vm1 part: end */

/* VM library: main VM C file template.

   Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021 Luca Saiu
   Written by Luca Saiu

   This file is part of GNU Jitter.

   GNU Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GNU Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Jitter.  If not, see <https://www.gnu.org/licenses/>. */


/* Generated file warning.
 * ************************************************************************** */

/* Unless this file is named exactly "vm1.c" , without any prefix, you are
   looking at a machine-generated derived file.  The original source is the vm.c
   template from Jitter, with added code implementing the structuredvm VM. */




#include <jitter/jitter-early-header.h>

/* When we are using Gnulib the standard header files included below will in
   fact be Gnulib replacements; make sure that the Gnulib macros are
   recognised.
   It is in fact possible that  HAVE_CONFIG_H  is defined even in other
   contexts; it should be harmless to include config.h anyway. */
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif // #ifdef HAVE_CONFIG_H

#include <assert.h>
#include <string.h>

#include <jitter/jitter.h>

#if defined (STRUCTUREDVM_PROFILE_SAMPLE)
#include <sys/time.h>
#endif // #if defined (STRUCTUREDVM_PROFILE_SAMPLE)

#include <jitter/jitter-hash.h>
#include <jitter/jitter-instruction.h>
#include <jitter/jitter-mmap.h>
#include <jitter/jitter-mutable-routine.h>
#include <jitter/jitter-print.h>
#include <jitter/jitter-rewrite.h>
#include <jitter/jitter-routine.h>
#include <jitter/jitter-routine-parser.h>
#include <jitter/jitter-specialize.h>
#include <jitter/jitter-defect.h>
#include <jitter/jitter-patch-in.h>

/* I don't need to include <jitter/jitter-executor.h> here, nor to define
   JITTER_THIS_CAN_INCLUDE_JITTER_EXECUTOR_H ; doing so carelessly might
   lead to subtle bugs, that it is better to prevent.
   Of course I can reconsider this decision in the future. */

#include <jitter/jitter-data-locations.h>

#include "structuredvm-vm.h"
//#include "structuredvm-specialized-instructions.h"
//#include "structuredvm-meta-instructions.h"
#include <jitter/jitter-fatal.h>




/* Check requirements for particular features.
 * ************************************************************************** */

/* VM sample-profiling is only supported with GCC.  Do not bother activating it
   with other compilers, when the numbers would turn out to be unreliable in the
   end. */
#if  defined (STRUCTUREDVM_PROFILE_SAMPLE)        \
     && ! defined (JITTER_HAVE_ACTUAL_GCC)
# error "Sample-profiling is only supported with GCC: it requires (machine-independent)"
# error "GNU C extended asm.  It is not worth supporting other compilers if"
# error "the numbers turn out to be unreliable in the end."
#endif

/* Warn about the unreliability of sample-profiling with simple dispatches
   unless one of the complex dispatches is in use. */
#if  defined (STRUCTUREDVM_PROFILE_SAMPLE)                 \
     && ! defined (JITTER_DISPATCH_MINIMAL_THREADING)  \
     && ! defined (JITTER_DISPATCH_NO_THREADING)
# warning "Sample-profiling is unreliable with simple dispatches: the sample"
# warning "incrementation code can be executed at any point in the VM"
# warning "instruction, not necessarily at the same point (the beginning) for"
# warning "every VM instruction."
#endif




/* Machine-generated data structures.
 * ************************************************************************** */

/* Machine-generated data structures defining this VM.  Initializing a static
   struct is problematic, as it requires constant expressions for each field --
   and const pointers don't qualify.  This is why we initialize the struct
   fields below in structuredvm_initialize. */
static struct jitter_vm
the_structuredvm_vm;

struct jitter_vm * const
structuredvm_vm = & the_structuredvm_vm;

struct jitter_list_header * const
structuredvm_states = & the_structuredvm_vm.states;

/* It is convenient to have this initialised at start up, even before calling
   any initialisation function.  This makes it reliable to read this when, for
   example, handling --version . */
static const struct jitter_vm_configuration
structuredvm_vm_the_configuration
  = {
      STRUCTUREDVM_LOWER_CASE_PREFIX /* lower_case_prefix */,
      STRUCTUREDVM_UPPER_CASE_PREFIX /* upper_case_prefix */,
      STRUCTUREDVM_HASH_PREFIX /* hash_prefix */,
      STRUCTUREDVM_MAX_FAST_REGISTER_NO_PER_CLASS
        /* max_fast_register_no_per_class */,
      STRUCTUREDVM_MAX_NONRESIDUAL_LITERAL_NO /* max_nonresidual_literal_no */,
      STRUCTUREDVM_DISPATCH_HUMAN_READABLE /* dispatch_human_readable */,
      /* The instrumentation field can be seen as a bit map.  See the comment
         in jitter/jitter-vm.h . */
      (jitter_vm_instrumentation_none
#if defined (STRUCTUREDVM_PROFILE_COUNT)
       | jitter_vm_instrumentation_count
#endif
#if defined (STRUCTUREDVM_PROFILE_SAMPLE)
       | jitter_vm_instrumentation_sample
#endif
       ) /* instrumentation */
    };

const struct jitter_vm_configuration * const
structuredvm_vm_configuration
  = & structuredvm_vm_the_configuration;




/* Initialization and finalization: internal functions, not for the user.
 * ************************************************************************** */

/* Initialize threads.  This only needs to be called once at initialization, and
   the user doesn't need to bother with it.  Defined along with the executor. */
void
structuredvm_initialize_threads (void);

/* Check that the encodings in enum jitter_specialized_instruction_opcode (as
   used in the specializer) are coherent with machine-generated code.  Making a
   mistake here would introduce subtle bugs, so it's better to be defensive. */
static void
structuredvm_check_specialized_instruction_opcode_once (void)
{
  static bool already_checked = false;
  if (already_checked)
    return;

  assert (((enum jitter_specialized_instruction_opcode)
           structuredvm_specialized_instruction_opcode__eINVALID)
          == jitter_specialized_instruction_opcode_INVALID);
  assert (((enum jitter_specialized_instruction_opcode)
           structuredvm_specialized_instruction_opcode__eBEGINBASICBLOCK)
          == jitter_specialized_instruction_opcode_BEGINBASICBLOCK);
  assert (((enum jitter_specialized_instruction_opcode)
           structuredvm_specialized_instruction_opcode__eEXITVM)
          == jitter_specialized_instruction_opcode_EXITVM);
  assert (((enum jitter_specialized_instruction_opcode)
           structuredvm_specialized_instruction_opcode__eDATALOCATIONS)
          == jitter_specialized_instruction_opcode_DATALOCATIONS);
  assert (((enum jitter_specialized_instruction_opcode)
           structuredvm_specialized_instruction_opcode__eNOP)
          == jitter_specialized_instruction_opcode_NOP);
  assert (((enum jitter_specialized_instruction_opcode)
           structuredvm_specialized_instruction_opcode__eUNREACHABLE0)
          == jitter_specialized_instruction_opcode_UNREACHABLE0);
  assert (((enum jitter_specialized_instruction_opcode)
           structuredvm_specialized_instruction_opcode__eUNREACHABLE1)
          == jitter_specialized_instruction_opcode_UNREACHABLE1);
  assert (((enum jitter_specialized_instruction_opcode)
           structuredvm_specialized_instruction_opcode__ePRETENDTOJUMPANYWHERE)
          == jitter_specialized_instruction_opcode_PRETENDTOJUMPANYWHERE);

  already_checked = true;
}

/* A prototype for a machine-generated function not needing a public
   declaration, only called thru a pointer within struct jitter_vm . */
int
structuredvm_specialize_instruction (struct jitter_mutable_routine *p,
                                 const struct jitter_instruction *ins);

/* Initialize the pointed special-purpose data structure. */
static void
structuredvm_initialize_special_purpose_data
   (volatile struct jitter_special_purpose_state_data *d)
{
  d->pending_notifications = 0;
  jitter_initialize_pending_signal_notifications
     (& d->pending_signal_notifications);

  /* Initialise profiling fields. */
  jitter_profile_runtime_initialize (structuredvm_vm,
                                     (struct jitter_profile_runtime *)
                                     & d->profile_runtime);
}

/* Finalize the pointed special-purpose data structure. */
static void
structuredvm_finalize_special_purpose_data
   (volatile struct jitter_special_purpose_state_data *d)
{
  jitter_finalize_pending_signal_notifications
     (d->pending_signal_notifications);

  jitter_profile_runtime_finalize (structuredvm_vm,
                                   (struct jitter_profile_runtime *)
                                   & d->profile_runtime);
}




/* Check that we link with the correct Jitter library.
 * ************************************************************************** */

/* It is possible to make a mistake at link time, and link a VM compiled with
   some dispatch with the Jitter runtime for a different dispatch.  That
   would cause crashes that is better to prevent.  This is a way to detect such
   mistakes very early, by causing a link-time failure in case of mismatch. */
extern volatile const bool
JITTER_DISPATCH_DEPENDENT_GLOBAL_NAME;




/* Low-level debugging features relying on assembly: data locations.
 * ************************************************************************** */

#if defined (JITTER_HAVE_KNOWN_BINARY_FORMAT) && ! defined (JITTER_DISPATCH_SWITCH)
/* A declaration for data locations, as visible from C.  The global is defined in
   assembly in its own separate section thru the machinery in
   jitter/jitter-sections.h . */
extern const char
JITTER_DATA_LOCATION_NAME(structuredvm) [];
#endif // #if ...

void
structuredvm_dump_data_locations (jitter_print_context output)
{
#ifndef JITTER_DISPATCH_SWITCH
  jitter_dump_data_locations (output, & the_structuredvm_vm);
#else
  jitter_print_char_star (output,
                          "VM data location information unavailable\n");
#endif // #ifndef JITTER_DISPATCH_SWITCH
}




/* Initialization and finalization.
 * ************************************************************************** */

#if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
JITTER_DEFECT_DESCRIPTOR_DECLARATIONS_(structuredvm)

/* This global is defined from C: there is no particular need of doing it in
   assembly.  It is initialised in structuredvm_execute_or_initialize , where C
   labels are visible. */ // FIXME: unless it turns out to be simpler in assembly ...
jitter_int
structuredvm_defect_descriptors_correct_displacement;
#endif // #if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
#if defined (JITTER_HAVE_PATCH_IN)
JITTER_PATCH_IN_DESCRIPTOR_DECLARATIONS_(structuredvm)
#endif // #if defined (JITTER_HAVE_PATCH_IN)

#ifndef JITTER_DISPATCH_SWITCH
/* True iff thread sizes are all non-negative and non-huge.  We refuse to
   disassemble otherwise, and when replication is enabled we refuse to run
   altogether.  See the comment right below. */
static bool
structuredvm_threads_validated = false;
#endif // #ifndef JITTER_DISPATCH_SWITCH

/* Omit structuredvm_validate_thread_sizes_once for switch-dispatching, as threads
   don't exist at all in that case.*/
#ifndef JITTER_DISPATCH_SWITCH
/* Check that VM instruction sizes are all non-negative, and that no thread
   starts before the end of the previous one.  Even one violation of such
   conditions is a symptom that the code has not been compiled with
   -fno-reorder-blocks , which would have disastrous effects with replication.
   It's better to validate threads at startup and fail immediately than to crash
   at run time.

   If even one thread appears to be wrong then refuse to disassemble when
   replication is disabled, and refuse to run altogether if replication is
   enabled. */
static void
structuredvm_validate_threads_once (void)
{
  /* Return if this is not the first time we got here. */
  static bool already_validated = false;
  if (already_validated)
    return;

#ifdef JITTER_REPLICATE
# define JITTER_FAIL(error_text)                                             \
    do                                                                       \
      {                                                                      \
        fprintf (stderr,                                                     \
                 "About specialized instruction %i (%s) at %p, size %liB\n", \
                 i, structuredvm_specialized_instruction_names [i],              \
                 structuredvm_threads [i],                                       \
                 structuredvm_thread_sizes [i]);                                 \
        jitter_fatal ("%s: you are not compiling with -fno-reorder-blocks",  \
                      error_text);                                           \
      }                                                                      \
    while (false)
#else
# define JITTER_FAIL(ignored_error_text)  \
    do                                    \
      {                                   \
        everything_valid = false;         \
        goto out;                         \
      }                                   \
    while (false)
#endif // #ifdef JITTER_REPLICATE

  /* The minimum address the next instruction code has to start at.

     This relies on NULL being zero, or in general lower in magnitude than any
     valid pointer.  It is not worth the trouble to be pedantic, as this will be
     true on every architecture where I can afford low-level tricks. */
  jitter_thread lower_bound = NULL;

  /* Check every thread.  We rely on the order here, following specialized
     instruction opcodes. */
  int i;
  bool everything_valid = true;
  for (i = 0; i < STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO; i ++)
    {
      jitter_thread thread = structuredvm_threads [i];
      long size = structuredvm_thread_sizes [i];

      /* Check that the current thread has non-negative non-huge size and
         doesn't start before the end of the previous one.  If this is true for
         all threads we can conclude that they are non-overlapping as well. */
      if (__builtin_expect (size < 0, false))
        JITTER_FAIL("a specialized instruction has negative code size");
      if (__builtin_expect (size > (1 << 24), false))
        JITTER_FAIL("a specialized instruction has huge code size");
      if (__builtin_expect (lower_bound > thread, false))
        JITTER_FAIL("non-sequential thread");

      /* The next thread cannot start before the end of the current one. */
      lower_bound = ((char*) thread) + size;
    }

#undef JITTER_FAIL

#ifndef JITTER_REPLICATE
 out:
#endif // #ifndef JITTER_REPLICATE

  /* If we have validated every thread size then disassembling appears safe. */
  if (everything_valid)
    structuredvm_threads_validated = true;

  /* We have checked the thread sizes, once and for all.  If this function gets
     called again, thru a second structuredvm initialization, it will immediately
     return. */
  already_validated = true;
}
#endif // #ifndef JITTER_DISPATCH_SWITCH

#if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
/* The actual replacement table.  We only need it when defect replacement is in
   use. */
jitter_uint
structuredvm_replacement_table [STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO];

/* The defective-instruction array for this VM.  The first
   defective_specialized_instruction_no elements of the array contain the
   specialized_instruction_ids of defective instructions; the remaining elements
   are set to -1.  This is initialised by jitter_fill_replacement_table . */
jitter_int
structuredvm_defective_specialized_instructions [STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO];
#endif // #if defined (JITTER_HAVE_DEFECT_REPLACEMENT)

void
structuredvm_initialize (void)
{
  /* Check that the Jitter library we linked is the right one.  This check
     actually only useful to force the global to be used.  I prefer not to use
     an assert, because assertions can be disabled. */
  if (! JITTER_DISPATCH_DEPENDENT_GLOBAL_NAME)
    jitter_fatal ("impossible to reach: the thing should fail at link time");

#ifdef JITTER_REPLICATE
  /* Initialize the executable-memory subsystem. */
  jitter_initialize_executable ();
#endif // #ifdef JITTER_REPLICATE

  /* Initialise the print-context machinery. */
  jitter_print_initialize ();

  /* Perform some sanity checks which only need to be run once. */
  structuredvm_check_specialized_instruction_opcode_once ();

  /* We have to initialize threads before structuredvm_threads , since the struct
     needs threads. */
  structuredvm_initialize_threads ();

#ifndef JITTER_DISPATCH_SWITCH
  /* Validate threads, to make sure the generated code was not compiled with
     incorrect options.  This only needs to be done once. */
  structuredvm_validate_threads_once ();
#endif // ifndef JITTER_DISPATCH_SWITCH

  /* Initialize the object pointed by structuredvm_vm (see the comment above as to
     why we do it here).  Before actually setting the fields to valid data, fill
     the whole struct with a -- hopefully -- invalid pattern, just to catch
     bugs. */
  static bool vm_struct_initialized = false;
  if (! vm_struct_initialized)
    {
      memset (& the_structuredvm_vm, 0xff, sizeof (struct jitter_vm));

      /* Make the configuration struct reachable from the VM struct. */
      the_structuredvm_vm.configuration = structuredvm_vm_configuration;
      //structuredvm_print_vm_configuration (stdout, & the_structuredvm_vm.configuration);

      /* Initialize meta-instruction pointers for implicit instructions.
         VM-independent program specialization relies on those, so they have to
         be accessible to the Jitter library, out of generated code.  Since
         meta-instructions are sorted alphabetically in the array, the index
         is not fixed. */
      the_structuredvm_vm.exitvm_meta_instruction
        = (structuredvm_meta_instructions + structuredvm_meta_instruction_id_exitvm);
      the_structuredvm_vm.unreachable_meta_instruction
        = (structuredvm_meta_instructions
           + structuredvm_meta_instruction_id_unreachable);

      /* Threads or pointers to native code blocks of course don't exist with
         switch-dispatching. */
#ifndef JITTER_DISPATCH_SWITCH
      the_structuredvm_vm.threads = (jitter_thread *)structuredvm_threads;
      the_structuredvm_vm.thread_sizes = (long *) structuredvm_thread_sizes;
      the_structuredvm_vm.threads_validated = structuredvm_threads_validated;
#if defined (JITTER_HAVE_KNOWN_BINARY_FORMAT)
      the_structuredvm_vm.data_locations = JITTER_DATA_LOCATION_NAME(structuredvm);
#else
      the_structuredvm_vm.data_locations = NULL;
#endif // #if defined (JITTER_HAVE_KNOWN_BINARY_FORMAT)
#endif // #ifndef JITTER_DISPATCH_SWITCH

      the_structuredvm_vm.specialized_instruction_residual_arities
        = structuredvm_specialized_instruction_residual_arities;
      the_structuredvm_vm.specialized_instruction_label_bitmasks
        = structuredvm_specialized_instruction_label_bitmasks;

      /* FIXME: I might want to conditionalize this. */
      the_structuredvm_vm.specialized_instruction_relocatables
        = structuredvm_specialized_instruction_relocatables;

      the_structuredvm_vm.specialized_instruction_callers
        = structuredvm_specialized_instruction_callers;
      the_structuredvm_vm.specialized_instruction_callees
        = structuredvm_specialized_instruction_callees;

      the_structuredvm_vm.specialized_instruction_names
        = structuredvm_specialized_instruction_names;
      the_structuredvm_vm.specialized_instruction_no
        = STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO;

      the_structuredvm_vm.meta_instruction_string_hash
        = & structuredvm_meta_instruction_hash;
      the_structuredvm_vm.meta_instructions
        = (struct jitter_meta_instruction *) structuredvm_meta_instructions;
      the_structuredvm_vm.meta_instruction_no = STRUCTUREDVM_META_INSTRUCTION_NO;
      the_structuredvm_vm.max_meta_instruction_name_length
        = STRUCTUREDVM_MAX_META_INSTRUCTION_NAME_LENGTH;
      the_structuredvm_vm.specialized_instruction_to_unspecialized_instruction
        = structuredvm_specialized_instruction_to_unspecialized_instruction;
      the_structuredvm_vm.register_class_character_to_register_class
        = structuredvm_register_class_character_to_register_class;
      the_structuredvm_vm.specialize_instruction = structuredvm_specialize_instruction;
      the_structuredvm_vm.rewrite = structuredvm_rewrite;

#if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
      /* Fill the replacement table.  Since the array in question is a global
         with a fixed size, this needs to be done only once. */
      jitter_fill_replacement_table
         (structuredvm_replacement_table,
          structuredvm_defective_specialized_instructions,
          & the_structuredvm_vm,
          structuredvm_worst_case_replacement_table,
          structuredvm_call_related_specialized_instruction_ids,
          structuredvm_call_related_specialized_instruction_id_no,
          structuredvm_specialized_instruction_call_relateds,
          JITTER_DEFECT_DESCRIPTORS_NAME (structuredvm),
          (JITTER_DEFECT_DESCRIPTORS_SIZE_IN_BYTES_NAME (structuredvm)
           / sizeof (struct jitter_defect_descriptor)),
          JITTER_DEFECT_CORRECT_DISPLACEMENT_NAME (structuredvm));
      the_structuredvm_vm.replacement_table = structuredvm_replacement_table;
      the_structuredvm_vm.defective_specialized_instructions
        = structuredvm_defective_specialized_instructions;
      the_structuredvm_vm.specialized_instruction_call_relateds
        = structuredvm_specialized_instruction_call_relateds;
#else /* no defect replacement */
      /* In this configuration it is impossible to have defects: set every
         defect count to zero. */
      the_structuredvm_vm.defect_no = 0;
      the_structuredvm_vm.defective_specialized_instruction_no = 0;
      the_structuredvm_vm.defective_call_related_specialized_instruction_no = 0;
      the_structuredvm_vm.replacement_specialized_instruction_no = 0;
      the_structuredvm_vm.replacement_table = NULL;
      the_structuredvm_vm.defective_specialized_instructions = NULL;
      the_structuredvm_vm.specialized_instruction_call_relateds = NULL;
#endif // #if defined (JITTER_HAVE_DEFECT_REPLACEMENT)

      /* Initialize the empty list of states. */
      JITTER_LIST_INITIALIZE_HEADER (& the_structuredvm_vm.states);

      vm_struct_initialized = true;
    }

#ifdef JITTER_HAVE_PATCH_IN
    /* Since the patch-in table is destroyed at finalisation time we have to
       rebuild it at every initialisation, out of the previous conditional. */
    the_structuredvm_vm.specialized_instruction_fast_label_bitmasks
      = structuredvm_specialized_instruction_fast_label_bitmasks;
    the_structuredvm_vm.patch_in_descriptors =
      JITTER_PATCH_IN_DESCRIPTORS_NAME(structuredvm);
    const size_t patch_in_descriptor_size
      = sizeof (struct jitter_patch_in_descriptor);
    the_structuredvm_vm.patch_in_descriptor_no
      = (JITTER_PATCH_IN_DESCRIPTORS_SIZE_IN_BYTES_NAME(structuredvm)
         / patch_in_descriptor_size);
    /* Cheap sanity check: if the size in bytes is not a multiple of
       the element size, we are doing something very wrong. */
    if (JITTER_PATCH_IN_DESCRIPTORS_SIZE_IN_BYTES_NAME(structuredvm)
        % patch_in_descriptor_size != 0)
      jitter_fatal ("patch-in descriptors total size %li not a multiple "
                    "of the element size %li",
                    (long) (JITTER_PATCH_IN_DESCRIPTORS_SIZE_IN_BYTES_NAME
                            (structuredvm)),
                    (long) patch_in_descriptor_size);
    /* Initialize the patch-in table for this VM. */
    the_structuredvm_vm.patch_in_table
      = jitter_make_patch_in_table (the_structuredvm_vm.patch_in_descriptors,
                                    the_structuredvm_vm.patch_in_descriptor_no,
                                    STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO);
#else
    the_structuredvm_vm.specialized_instruction_fast_label_bitmasks = NULL;
#endif // #ifdef JITTER_HAVE_PATCH_IN

  jitter_initialize_meta_instructions (& structuredvm_meta_instruction_hash,
                                         structuredvm_meta_instructions,
                                         STRUCTUREDVM_META_INSTRUCTION_NO);

#if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
# if 0
  jitter_dump_replacement_table (stderr, structuredvm_replacement_table,
                                 & the_structuredvm_vm);
  jitter_dump_defects (stderr, structuredvm_defective_specialized_instructions,
                       & the_structuredvm_vm,
                       structuredvm_specialized_instruction_call_relateds);
# endif
#endif // #if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
#if defined (JITTER_HAVE_PATCH_IN)
  //printf ("======================= Patch-in descriptors: BEGIN\n");
  //JITTER_DUMP_PATCH_IN_DESCRIPTORS(structuredvm);
  //printf ("======================= Patch-in descriptors: END\n");
#endif // #if defined (JITTER_HAVE_PATCH_IN)
}

void
structuredvm_finalize (void)
{
  /* There's no need to touch the_structuredvm_vm ; we can keep it as it is, as it
     contains no dynamically-allocated fields. */
  /* Threads need no finalization. */
  jitter_finalize_meta_instructions (& structuredvm_meta_instruction_hash);

#ifdef JITTER_HAVE_PATCH_IN
  /* Destroy the patch-in table for this VM. */
  jitter_destroy_patch_in_table (the_structuredvm_vm.patch_in_table,
                                 STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO);
#endif // #ifdef JITTER_HAVE_PATCH_IN

#ifdef JITTER_REPLICATE
  /* Finalize the executable-memory subsystem. */
  jitter_finalize_executable ();
#endif // #ifdef JITTER_REPLICATE

  /* Finalize the state list.  If it is not empty then something has gone
     wrong earlier. */
  if (the_structuredvm_vm.states.first != NULL
      || the_structuredvm_vm.states.last != NULL)
    jitter_fatal ("not every state structure was destroyed before STRUCTUREDVM "
                  "finalisation.");
}




/* VM-dependant mutable routine initialization.
 * ************************************************************************** */

struct jitter_mutable_routine*
structuredvm_make_mutable_routine (void)
{
  return jitter_make_mutable_routine (structuredvm_vm);
}




/* Sample profiling: internal API.
 * ************************************************************************** */

#if defined (STRUCTUREDVM_PROFILE_SAMPLE)

/* Sample profiling depends on some system features: fail immediately if they
   are not available */
#if ! defined (JITTER_HAVE_SIGACTION) || ! defined (JITTER_HAVE_SETITIMER)
# jitter_fatal "sample-profiling depends on sigaction and setitimer"
#endif

static struct itimerval
structuredvm_timer_interval;

static struct itimerval
structuredvm_timer_disabled_interval;

/* The sampling data, currently global.  The current implementation does not
   play well with threads, but it can be changed later keeping the same user
   API. */
struct structuredvm_sample_profile_state
{
  /* The state currently sample-profiling.  Since such a state can be only one
     right now this field is useful for printing error messages in case the user
     sets up sample-profiling from two states at the same time by mistake.
     This field is also useful for temporarily suspending and then reenabling
     sampling, when The Array is being resized: if the signal handler sees that
     this field is NULL it will not touch the fields. */
  struct structuredvm_state *state_p;

  /* A pointer to the counts field within the sample_profile_runtime struct. */
  uint32_t *counts;

  /* A pointer to the current specialised instruction opcode within the
     sample_profile_runtime struct. */
  volatile jitter_int * specialized_opcode_p;

  /* A pointer to the field counting the number of samples, again within the
     sample_profile_runtime struct. */
  unsigned int *sample_no_p;
};

/* The (currently) one and only global state for sample-profiling. */
static struct structuredvm_sample_profile_state
structuredvm_sample_profile_state;

static void
structuredvm_sigprof_handler (int signal)
{
#if 0
  assert (structuredvm_sample_profile_state.state_p != NULL);
#endif

  jitter_int specialized_opcode
    = * structuredvm_sample_profile_state.specialized_opcode_p;
  if (__builtin_expect ((specialized_opcode >= 0
                         && (specialized_opcode
                             < STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO)),
                        true))
    structuredvm_sample_profile_state.counts [specialized_opcode] ++;

  (* structuredvm_sample_profile_state.sample_no_p) ++;
}

void
structuredvm_profile_sample_initialize (void)
{
  /* Perform a sanity check over the sampling period. */
  if (JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS <= 0 ||
      JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS >= 1000)
    jitter_fatal ("invalid JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS: %f",
                  (double) JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS);
  struct sigaction action;
  sigaction (SIGPROF, NULL, & action);
  action.sa_handler = structuredvm_sigprof_handler;
  sigaction (SIGPROF, & action, NULL);

  long microseconds
    = (long) (JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS * 1000);
  structuredvm_timer_interval.it_interval.tv_sec = 0;
  structuredvm_timer_interval.it_interval.tv_usec = microseconds;
  structuredvm_timer_interval.it_value = structuredvm_timer_interval.it_interval;

  structuredvm_sample_profile_state.state_p = NULL;
  structuredvm_timer_disabled_interval.it_interval.tv_sec = 0;
  structuredvm_timer_disabled_interval.it_interval.tv_usec = 0;
  structuredvm_timer_disabled_interval.it_value
    = structuredvm_timer_disabled_interval.it_interval;
}

void
structuredvm_profile_sample_start (struct structuredvm_state *state_p)
{
  struct jitter_sample_profile_runtime *spr
    = ((struct jitter_sample_profile_runtime *)
       & STRUCTUREDVM_STATE_TO_SPECIAL_PURPOSE_STATE_DATA (state_p)
           ->profile_runtime.sample_profile_runtime);

  if (structuredvm_sample_profile_state.state_p != NULL)
    {
      if (state_p != structuredvm_sample_profile_state.state_p)
        jitter_fatal ("currently it is only possible to sample-profile from "
                      "one state at the time: trying to sample-profile from "
                      "the state %p when already sample-profiling from the "
                      "state %p",
                      state_p, structuredvm_sample_profile_state.state_p);
      else
        {
          /* This situation is a symptom of a bug, but does not need to lead
             to a fatal error. */
          printf ("WARNING: starting profile on the state %p when profiling "
                  "was already active in the same state.\n"
                  "Did you call longjmp from VM code?", state_p);
          fflush (stdout);
        }
    }
  structuredvm_sample_profile_state.state_p = state_p;
  structuredvm_sample_profile_state.sample_no_p = & spr->sample_no;
  structuredvm_sample_profile_state.counts = spr->counts;
  structuredvm_sample_profile_state.specialized_opcode_p
    = & spr->current_specialized_instruction_opcode;
  //fprintf (stderr, "SAMPLE START\n"); fflush (NULL);
  if (setitimer (ITIMER_PROF, & structuredvm_timer_interval, NULL) != 0)
    jitter_fatal ("setitimer failed when establishing a timer");
}

void
structuredvm_profile_sample_stop (void)
{
  if (setitimer (ITIMER_PROF, & structuredvm_timer_disabled_interval, NULL) != 0)
    jitter_fatal ("setitimer failed when disabling a timer");

  structuredvm_sample_profile_state.state_p = NULL;

  /* The rest is just for defenisveness' sake. */
  * structuredvm_sample_profile_state.specialized_opcode_p = -1;
  structuredvm_sample_profile_state.sample_no_p = NULL;
  structuredvm_sample_profile_state.counts = NULL;
  structuredvm_sample_profile_state.specialized_opcode_p = NULL;
}
#endif // #if defined (STRUCTUREDVM_PROFILE_SAMPLE)




/* Slow register initialisation.
 * ************************************************************************** */

/* Initialise slow registers (for register classes defining an initial value) in
   a given Array, starting from a given rank up to another given rank.  The
   argument old_slow_register_no_per_class indicates the number of already
   initialised ranks, which this functions does not touch;
   new_slow_register_no_per_class indicates the new number of ranks.  Every rank
   from old_slow_register_no_per_class + 1 to new_slow_register_no_per_class,
   both included, will be initialised. */
static void
structuredvm_initialize_slow_registers (char *initial_array_pointer,
                                    jitter_int old_slow_register_no_per_class,
                                    jitter_int new_slow_register_no_per_class)
{
  /* Compute the address of the first slow registers, which is the beginning
     of the first rank. */
  union structuredvm_any_register *first_slow_register
    = ((union structuredvm_any_register *)
       ((char *) initial_array_pointer
        + STRUCTUREDVM_FIRST_SLOW_REGISTER_UNBIASED_OFFSET));

  /* Initialise every *new* rank, without touching the old ones. */
  jitter_int i;
  for (i = old_slow_register_no_per_class;
       i < new_slow_register_no_per_class;
       i ++)
    {
      /* A pointer to the i-th rank of slow registers.  Every register
         in the rank is new and in general (according to its class) may
         need initialisation. */
      union structuredvm_any_register *rank
        = first_slow_register + (i * STRUCTUREDVM_REGISTER_CLASS_NO);
      STRUCTUREDVM_INITIALIZE_SLOW_REGISTER_RANK (rank);
    }
#if 0
      fprintf (stderr, "initialised %li (up from %li) slow registers per class, array at %p\n",
               (long) new_slow_register_no_per_class,
               (long) old_slow_register_no_per_class,
               initial_array_pointer);
#endif
}




/* Array re-allocation.
 * ************************************************************************** */

char *
structuredvm_make_place_for_slow_registers (struct structuredvm_state *s,
                                        jitter_int new_slow_register_no_per_class)
{
  if (new_slow_register_no_per_class < 0)
    jitter_fatal ("structuredvm_make_place_for_slow_registers: negative slow "
                  "register number");
  jitter_int old_slow_register_no_per_class
    = s->structuredvm_state_backing.jitter_slow_register_no_per_class;
  /* Change nothing if we already have enough space for the required number of
     slow registers.  The no-change case will be the most common one, and
     this function might be worth optimizing. */
  if (__builtin_expect (new_slow_register_no_per_class
                        > old_slow_register_no_per_class,
                        false))
    {
#if defined (STRUCTUREDVM_PROFILE_SAMPLE)
      /* If sample-profiling is currently in progress on this state suspend it
         temporarily. */
      bool suspending_sample_profiling
        = (structuredvm_sample_profile_state.state_p == s);
      if (suspending_sample_profiling)
        structuredvm_profile_sample_stop ();
#endif // #if defined (STRUCTUREDVM_PROFILE_SAMPLE)

#if 0
      printf ("Increasing slow register-no (per class) from %li to %li\n", (long) old_slow_register_no_per_class, (long)new_slow_register_no_per_class);
      printf ("Array size %li -> %li\n", (long) STRUCTUREDVM_ARRAY_SIZE(old_slow_register_no_per_class), (long) STRUCTUREDVM_ARRAY_SIZE(new_slow_register_no_per_class));
#endif
      /* Save the new value for new_slow_register_no_per_class in the state
         structure; reallocate The Array. */
      s->structuredvm_state_backing.jitter_slow_register_no_per_class
        = new_slow_register_no_per_class;
      s->structuredvm_state_backing.jitter_array
        = jitter_xrealloc ((void *) s->structuredvm_state_backing.jitter_array,
                           STRUCTUREDVM_ARRAY_SIZE(new_slow_register_no_per_class));

     /* Initialise the slow registers we have just added, for every class. */
     structuredvm_initialize_slow_registers (s->structuredvm_state_backing.jitter_array,
                                         old_slow_register_no_per_class,
                                         new_slow_register_no_per_class);

#if defined (STRUCTUREDVM_PROFILE_SAMPLE)
      /* Now we can resume sample-profiling on this state if we suspended it. */
      if (suspending_sample_profiling)
        structuredvm_profile_sample_start (s);
#endif // #if defined (STRUCTUREDVM_PROFILE_SAMPLE)
#if 0
      fprintf (stderr, "slow registers are now %li per class, Array at %p (biased %p)\n",
               ((long)
                s->structuredvm_state_backing.jitter_slow_register_no_per_class),
               s->structuredvm_state_backing.jitter_array,
               s->structuredvm_state_backing.jitter_array + JITTER_ARRAY_BIAS);
#endif
    }

  /* Return the new (or unchanged) base, by simply adding the bias to the
     Array as it is now. */
  return s->structuredvm_state_backing.jitter_array + JITTER_ARRAY_BIAS;
}

void
structuredvm_ensure_enough_slow_registers_for_executable_routine
   (const struct jitter_executable_routine *er, struct structuredvm_state *s)
{
  structuredvm_make_place_for_slow_registers (s, er->slow_register_per_class_no);
}




/* Program text frontend.
 * ************************************************************************** */

struct structuredvm_routine_parse_error *
structuredvm_parse_mutable_routine_from_file_star (FILE *input_file,
                                               struct jitter_mutable_routine *p)
{
  return jitter_parse_mutable_routine_from_file_star (input_file, p,
                                                      structuredvm_vm);
}

struct structuredvm_routine_parse_error *
structuredvm_parse_mutable_routine_from_file (const char *input_file_name,
                                          struct jitter_mutable_routine *p)
{
  return jitter_parse_mutable_routine_from_file (input_file_name, p,
                                                 structuredvm_vm);
}

struct structuredvm_routine_parse_error *
structuredvm_parse_mutable_routine_from_string (const char *string,
                                            struct jitter_mutable_routine *p)
{
  return jitter_parse_mutable_routine_from_string (string, p, structuredvm_vm);
}




/* State making and destroying.
 * ************************************************************************** */

/* State initialisation (with a given number of slow registers), reset and
   finalisation are machine-generated. */

void
structuredvm_state_initialize (struct structuredvm_state *sp)
{
  structuredvm_state_initialize_with_slow_registers (sp, 0);
}

struct structuredvm_state *
structuredvm_state_make_with_slow_registers (jitter_uint slow_register_no_per_class)
{
  struct structuredvm_state *res = jitter_xmalloc (sizeof (struct structuredvm_state));
  structuredvm_state_initialize_with_slow_registers (res,
                                                 slow_register_no_per_class);
  return res;
}

struct structuredvm_state *
structuredvm_state_make (void)
{
  return structuredvm_state_make_with_slow_registers (0);
}

void
structuredvm_state_destroy (struct structuredvm_state *state)
{
  structuredvm_state_finalize (state);
  free (state);
}




/* Executing code: unified routine API.
 * ************************************************************************** */

void
structuredvm_ensure_enough_slow_registers_for_routine
   (jitter_routine r, struct structuredvm_state *s)
{
  struct jitter_executable_routine *e
    = jitter_routine_make_executable_if_needed (r);
  structuredvm_ensure_enough_slow_registers_for_executable_routine (e, s);
}

enum structuredvm_exit_status
structuredvm_execute_routine (jitter_routine r,
                          struct structuredvm_state *s)
{
  struct jitter_executable_routine *e
    = jitter_routine_make_executable_if_needed (r);
  return structuredvm_execute_executable_routine (e, s);
}





/* Defects and replacements: user API.
 * ************************************************************************** */

/* These functions are all trivial wrappers around the functionality declared
   in jitter/jitter-defect.h, hiding the VM pointer. */

void
structuredvm_defect_print_summary (jitter_print_context cx)
{
  jitter_defect_print_summary (cx, structuredvm_vm);
}

void
structuredvm_defect_print (jitter_print_context cx,
                       unsigned indentation_column_no)
{
  jitter_defect_print (cx, structuredvm_vm, indentation_column_no);
}

void
structuredvm_defect_print_replacement_table (jitter_print_context cx,
                                         unsigned indentation_column_no)
{
  jitter_defect_print_replacement_table (cx, structuredvm_vm, indentation_column_no);
}




/* Profiling: user API.
 * ************************************************************************** */

/* These functions are all trivial wrappers around the functionality declared
   in jitter/jitter-profile.h, hiding the VM pointer. */

struct structuredvm_profile_runtime *
structuredvm_state_profile_runtime (struct structuredvm_state *s)
{
  volatile struct jitter_special_purpose_state_data *spd
    = STRUCTUREDVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA
        (s->structuredvm_state_backing.jitter_array);
  return (struct structuredvm_profile_runtime *) & spd->profile_runtime;
}

struct structuredvm_profile_runtime *
structuredvm_profile_runtime_make (void)
{
  return jitter_profile_runtime_make (structuredvm_vm);
}

void
structuredvm_profile_runtime_destroy (struct structuredvm_profile_runtime *p)
{
  jitter_profile_runtime_destroy (structuredvm_vm, p);
}

void
structuredvm_profile_runtime_clear (struct structuredvm_profile_runtime * p)
{
  jitter_profile_runtime_clear (structuredvm_vm, p);
}

void
structuredvm_profile_runtime_merge_from (struct structuredvm_profile_runtime *to,
                                     const struct structuredvm_profile_runtime *from)
{
  jitter_profile_runtime_merge_from (structuredvm_vm, to, from);
}

void
structuredvm_profile_runtime_merge_from_state (struct structuredvm_profile_runtime *to,
                                           const struct structuredvm_state *from_state)
{
  const struct structuredvm_profile_runtime* from
    = structuredvm_state_profile_runtime ((struct structuredvm_state *) from_state);
  jitter_profile_runtime_merge_from (structuredvm_vm, to, from);
}

void
structuredvm_profile_runtime_print_unspecialized
   (jitter_print_context ct,
    const struct structuredvm_profile_runtime *p)
{
  jitter_profile_runtime_print_unspecialized (ct, structuredvm_vm, p);
}

void
structuredvm_profile_runtime_print_specialized (jitter_print_context ct,
                                            const struct structuredvm_profile_runtime
                                            *p)
{
  jitter_profile_runtime_print_specialized (ct, structuredvm_vm, p);
}

struct structuredvm_profile *
structuredvm_profile_unspecialized_from_runtime
   (const struct structuredvm_profile_runtime *p)
{
  return jitter_profile_unspecialized_from_runtime (structuredvm_vm, p);
}

struct structuredvm_profile *
structuredvm_profile_specialized_from_runtime (const struct structuredvm_profile_runtime
                                           *p)
{
  return jitter_profile_specialized_from_runtime (structuredvm_vm, p);
}




/* Evrything following this point is machine-generated.
 * ************************************************************************** */

/* What follows could be conceptually split into several generated C files, but
   having too many of them would be inconvenient for the user to compile and
   link.  For this reason we currently generate just three files: one is this,
   which also contains the specializer, another is for the executor, and then a
   header -- a main module is optional.  The executor will be potentially very
   large, so it is best compiled separately.  The specializer might be large as
   well at this stage, even if its compilation is usually much less
   expensive. */
/* These two macros are convenient for making VM-specific identifiers
   using VM-independent macros from a public header, without polluting
   the global namespace. */
#define JITTER_VM_PREFIX_LOWER_CASE structuredvm
#define JITTER_VM_PREFIX_UPPER_CASE STRUCTUREDVM

/* User-specified code, printer part: beginning. */

static void
structured_literal_printer (jitter_print_context out, jitter_uint u)
{
  jitter_print_begin_class (out, "structuredvm-number");
  jitter_print_jitter_uint (out, 10, u);
  jitter_print_end_class (out);
}
  
/* User-specified code, printer part: end */

//#include <stdbool.h>

//#include <jitter/jitter.h>
//#include <jitter/jitter-instruction.h>

//#include "structuredvm-meta-instructions.h"

// FIXME: comment.
struct jitter_hash_table
structuredvm_meta_instruction_hash;


static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_b_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_be_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_beqi_mstack_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_bf_mstack_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_bg_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_bge_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_bl_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_ble_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_bne_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_bneqi_mstack_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_bt_mstack_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_call_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_copy_mto_mr_mstack_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & structuredvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_divided_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register, & structuredvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_equali_mstack_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, structured_literal_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_input_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & structuredvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_minus_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register, & structuredvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_minusi_mstack_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, structured_literal_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_mov_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register, & structuredvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_plus_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register, & structuredvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_plusi_mstack_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, structured_literal_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_pop_mstack_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & structuredvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_print_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_push_mstack_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum_or_literal_label, & structuredvm_register_class_r, structured_literal_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_remainder_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register, & structuredvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_times_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register, & structuredvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_uminus_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, structured_literal_printer }, { jitter_meta_instruction_parameter_kind_register, & structuredvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_underpop_mstack_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & structuredvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type structuredvm_underpush_mstack_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register_or_literal_fixnum, & structuredvm_register_class_r, jitter_default_literal_parameter_printer } };


const struct jitter_meta_instruction
structuredvm_meta_instructions [STRUCTUREDVM_META_INSTRUCTION_NO]
  = {
      { 0, "b", 1, true, false, false, true /* this ignores replacements */, structuredvm_b_meta_instruction_parameter_types },
      { 1, "be", 3, true, false, false, true /* this ignores replacements */, structuredvm_be_meta_instruction_parameter_types },
      { 2, "beqi-stack", 2, true, false, false, true /* this ignores replacements */, structuredvm_beqi_mstack_meta_instruction_parameter_types },
      { 3, "bf-stack", 1, true, false, false, true /* this ignores replacements */, structuredvm_bf_mstack_meta_instruction_parameter_types },
      { 4, "bg", 3, true, false, false, true /* this ignores replacements */, structuredvm_bg_meta_instruction_parameter_types },
      { 5, "bge", 3, true, false, false, true /* this ignores replacements */, structuredvm_bge_meta_instruction_parameter_types },
      { 6, "bl", 3, true, false, false, true /* this ignores replacements */, structuredvm_bl_meta_instruction_parameter_types },
      { 7, "ble", 3, true, false, false, true /* this ignores replacements */, structuredvm_ble_meta_instruction_parameter_types },
      { 8, "bne", 3, true, false, false, true /* this ignores replacements */, structuredvm_bne_meta_instruction_parameter_types },
      { 9, "bneqi-stack", 2, true, false, false, true /* this ignores replacements */, structuredvm_bneqi_mstack_meta_instruction_parameter_types },
      { 10, "bt-stack", 1, true, false, false, true /* this ignores replacements */, structuredvm_bt_mstack_meta_instruction_parameter_types },
      { 11, "call", 1, true, true, false, true /* this ignores replacements */, structuredvm_call_meta_instruction_parameter_types },
      { 12, "copy-to-r-stack", 1, false, false, false, true /* this ignores replacements */, structuredvm_copy_mto_mr_mstack_meta_instruction_parameter_types },
      { 13, "different-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 14, "divided", 3, false, false, false, false /* this ignores replacements */, structuredvm_divided_meta_instruction_parameter_types },
      { 15, "divided-stack", 0, false, false, false, false /* this ignores replacements */, NULL },
      { 16, "drop-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 17, "dup-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 18, "equal-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 19, "equali-stack", 1, false, false, false, true /* this ignores replacements */, structuredvm_equali_mstack_meta_instruction_parameter_types },
      { 20, "exitvm", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 21, "greater-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 22, "greaterorequal-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 23, "input", 1, false, false, false, false /* this ignores replacements */, structuredvm_input_meta_instruction_parameter_types },
      { 24, "input-stack", 0, false, false, false, false /* this ignores replacements */, NULL },
      { 25, "isnonzero-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 26, "less-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 27, "lessorequal-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 28, "logicaland-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 29, "logicalnot-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 30, "logicalor-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 31, "minus", 3, false, false, false, true /* this ignores replacements */, structuredvm_minus_meta_instruction_parameter_types },
      { 32, "minus-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 33, "minusi-stack", 1, false, false, false, true /* this ignores replacements */, structuredvm_minusi_mstack_meta_instruction_parameter_types },
      { 34, "mov", 2, false, false, false, true /* this ignores replacements */, structuredvm_mov_meta_instruction_parameter_types },
      { 35, "plus", 3, false, false, false, true /* this ignores replacements */, structuredvm_plus_meta_instruction_parameter_types },
      { 36, "plus-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 37, "plusi-stack", 1, false, false, false, true /* this ignores replacements */, structuredvm_plusi_mstack_meta_instruction_parameter_types },
      { 38, "pop-stack", 1, false, false, false, true /* this ignores replacements */, structuredvm_pop_mstack_meta_instruction_parameter_types },
      { 39, "print", 1, false, false, false, false /* this ignores replacements */, structuredvm_print_meta_instruction_parameter_types },
      { 40, "print-stack", 0, false, false, false, false /* this ignores replacements */, NULL },
      { 41, "procedure-prolog", 0, true, false, true, true /* this ignores replacements */, NULL },
      { 42, "push-stack", 1, false, false, false, true /* this ignores replacements */, structuredvm_push_mstack_meta_instruction_parameter_types },
      { 43, "push-unspecified-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 44, "remainder", 3, false, false, false, false /* this ignores replacements */, structuredvm_remainder_meta_instruction_parameter_types },
      { 45, "remainder-stack", 0, false, false, false, false /* this ignores replacements */, NULL },
      { 46, "return-to-undertop", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 47, "swap-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 48, "times", 3, false, false, false, true /* this ignores replacements */, structuredvm_times_meta_instruction_parameter_types },
      { 49, "times-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 50, "uminus", 2, false, false, false, true /* this ignores replacements */, structuredvm_uminus_meta_instruction_parameter_types },
      { 51, "uminus-stack", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 52, "underpop-stack", 1, false, false, false, true /* this ignores replacements */, structuredvm_underpop_mstack_meta_instruction_parameter_types },
      { 53, "underpush-stack", 1, false, false, false, true /* this ignores replacements */, structuredvm_underpush_mstack_meta_instruction_parameter_types },
      { 54, "unreachable", 0, true, false, false, true /* this ignores replacements */, NULL }
    };

/* The register class descriptor for r registers. */
const struct jitter_register_class
structuredvm_register_class_r
  = {
      structuredvm_register_class_id_r,
      'r',
      "general_register",
      "GENERAL_REGISTER",
      STRUCTUREDVM_REGISTER_r_FAST_REGISTER_NO,
      1 /* Use slow registers */
    };


/* A pointer to every existing register class descriptor. */
const struct jitter_register_class * const
structuredvm_regiter_classes []
  = {
      & structuredvm_register_class_r
    };

const struct jitter_register_class *
structuredvm_register_class_character_to_register_class (char c)
{
  switch (c)
    {
    case 'r': return & structuredvm_register_class_r;
    default:  return NULL;
    }
}

//#include "structuredvm-specialized-instructions.h"

const char * const
structuredvm_specialized_instruction_names [STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      "!INVALID",
      "!BEGINBASICBLOCK",
      "!EXITVM",
      "!DATALOCATIONS",
      "!NOP",
      "!UNREACHABLE0",
      "!UNREACHABLE1",
      "!PRETENDTOJUMPANYWHERE",
      "b/fR",
      "be/%rR/%rR/fR",
      "be/%rR/n0/fR",
      "be/%rR/nR/fR",
      "be/n0/%rR/fR",
      "be/n0/n0/fR",
      "be/n0/nR/fR",
      "be/nR/%rR/fR",
      "be/nR/n0/fR",
      "be/nR/nR/fR",
      "beqi-stack/n-1/fR",
      "beqi-stack/n0/fR",
      "beqi-stack/n1/fR",
      "beqi-stack/n2/fR",
      "beqi-stack/nR/fR",
      "bf-stack/fR",
      "bg/%rR/%rR/fR",
      "bg/%rR/n0/fR",
      "bg/%rR/nR/fR",
      "bg/n0/%rR/fR",
      "bg/n0/n0/fR",
      "bg/n0/nR/fR",
      "bg/nR/%rR/fR",
      "bg/nR/n0/fR",
      "bg/nR/nR/fR",
      "bge/%rR/%rR/fR",
      "bge/%rR/n0/fR",
      "bge/%rR/nR/fR",
      "bge/n0/%rR/fR",
      "bge/n0/n0/fR",
      "bge/n0/nR/fR",
      "bge/nR/%rR/fR",
      "bge/nR/n0/fR",
      "bge/nR/nR/fR",
      "bl/%rR/%rR/fR",
      "bl/%rR/n0/fR",
      "bl/%rR/nR/fR",
      "bl/n0/%rR/fR",
      "bl/n0/n0/fR",
      "bl/n0/nR/fR",
      "bl/nR/%rR/fR",
      "bl/nR/n0/fR",
      "bl/nR/nR/fR",
      "ble/%rR/%rR/fR",
      "ble/%rR/n0/fR",
      "ble/%rR/nR/fR",
      "ble/n0/%rR/fR",
      "ble/n0/n0/fR",
      "ble/n0/nR/fR",
      "ble/nR/%rR/fR",
      "ble/nR/n0/fR",
      "ble/nR/nR/fR",
      "bne/%rR/%rR/fR",
      "bne/%rR/n0/fR",
      "bne/%rR/nR/fR",
      "bne/n0/%rR/fR",
      "bne/n0/n0/fR",
      "bne/n0/nR/fR",
      "bne/nR/%rR/fR",
      "bne/nR/n0/fR",
      "bne/nR/nR/fR",
      "bneqi-stack/n-1/fR",
      "bneqi-stack/n0/fR",
      "bneqi-stack/n1/fR",
      "bneqi-stack/n2/fR",
      "bneqi-stack/nR/fR",
      "bt-stack/fR",
      "call/fR/retR",
      "copy-to-r-stack/%rR",
      "different-stack",
      "divided/%rR/%rR/%rR/retR",
      "divided/%rR/n2/%rR/retR",
      "divided/%rR/nR/%rR/retR",
      "divided/nR/%rR/%rR/retR",
      "divided/nR/n2/%rR/retR",
      "divided/nR/nR/%rR/retR",
      "divided-stack/retR",
      "drop-stack",
      "dup-stack",
      "equal-stack",
      "equali-stack/n0",
      "equali-stack/n1",
      "equali-stack/n2",
      "equali-stack/nR",
      "exitvm",
      "greater-stack",
      "greaterorequal-stack",
      "input/%rR/retR",
      "input-stack/retR",
      "isnonzero-stack",
      "less-stack",
      "lessorequal-stack",
      "logicaland-stack",
      "logicalnot-stack",
      "logicalor-stack",
      "minus/%rR/%rR/%rR",
      "minus/%rR/n1/%rR",
      "minus/%rR/n2/%rR",
      "minus/%rR/nR/%rR",
      "minus/nR/%rR/%rR",
      "minus/nR/n1/%rR",
      "minus/nR/n2/%rR",
      "minus/nR/nR/%rR",
      "minus-stack",
      "minusi-stack/n1",
      "minusi-stack/n2",
      "minusi-stack/nR",
      "mov/%rR/%rR",
      "mov/n0/%rR",
      "mov/n1/%rR",
      "mov/n-1/%rR",
      "mov/n2/%rR",
      "mov/nR/%rR",
      "plus/%rR/%rR/%rR",
      "plus/%rR/n1/%rR",
      "plus/%rR/n2/%rR",
      "plus/%rR/nR/%rR",
      "plus/n1/%rR/%rR",
      "plus/n1/n1/%rR",
      "plus/n1/n2/%rR",
      "plus/n1/nR/%rR",
      "plus/n2/%rR/%rR",
      "plus/n2/n1/%rR",
      "plus/n2/n2/%rR",
      "plus/n2/nR/%rR",
      "plus/nR/%rR/%rR",
      "plus/nR/n1/%rR",
      "plus/nR/n2/%rR",
      "plus/nR/nR/%rR",
      "plus-stack",
      "plusi-stack/n-1",
      "plusi-stack/n1",
      "plusi-stack/n2",
      "plusi-stack/nR",
      "pop-stack/%rR",
      "print/%rR/retR",
      "print/nR/retR",
      "print-stack/retR",
      "procedure-prolog",
      "push-stack/%rR",
      "push-stack/n0",
      "push-stack/n1",
      "push-stack/n-1",
      "push-stack/n2",
      "push-stack/nR",
      "push-stack/lR",
      "push-unspecified-stack",
      "remainder/%rR/%rR/%rR/retR",
      "remainder/%rR/n2/%rR/retR",
      "remainder/%rR/nR/%rR/retR",
      "remainder/nR/%rR/%rR/retR",
      "remainder/nR/n2/%rR/retR",
      "remainder/nR/nR/%rR/retR",
      "remainder-stack/retR",
      "return-to-undertop",
      "swap-stack",
      "times/%rR/%rR/%rR",
      "times/%rR/n2/%rR",
      "times/%rR/nR/%rR",
      "times/nR/%rR/%rR",
      "times/nR/n2/%rR",
      "times/nR/nR/%rR",
      "times-stack",
      "uminus/%rR/%rR",
      "uminus/nR/%rR",
      "uminus-stack",
      "underpop-stack/%rR",
      "underpush-stack/%rR",
      "underpush-stack/nR",
      "unreachable",
      "!REPLACEMENT-b/fR/retR",
      "!REPLACEMENT-be/%rR/%rR/fR/retR",
      "!REPLACEMENT-be/%rR/n0/fR/retR",
      "!REPLACEMENT-be/%rR/nR/fR/retR",
      "!REPLACEMENT-be/n0/%rR/fR/retR",
      "!REPLACEMENT-be/n0/n0/fR/retR",
      "!REPLACEMENT-be/n0/nR/fR/retR",
      "!REPLACEMENT-be/nR/%rR/fR/retR",
      "!REPLACEMENT-be/nR/n0/fR/retR",
      "!REPLACEMENT-be/nR/nR/fR/retR",
      "!REPLACEMENT-beqi-stack/n-1/fR/retR",
      "!REPLACEMENT-beqi-stack/n0/fR/retR",
      "!REPLACEMENT-beqi-stack/n1/fR/retR",
      "!REPLACEMENT-beqi-stack/n2/fR/retR",
      "!REPLACEMENT-beqi-stack/nR/fR/retR",
      "!REPLACEMENT-bf-stack/fR/retR",
      "!REPLACEMENT-bg/%rR/%rR/fR/retR",
      "!REPLACEMENT-bg/%rR/n0/fR/retR",
      "!REPLACEMENT-bg/%rR/nR/fR/retR",
      "!REPLACEMENT-bg/n0/%rR/fR/retR",
      "!REPLACEMENT-bg/n0/n0/fR/retR",
      "!REPLACEMENT-bg/n0/nR/fR/retR",
      "!REPLACEMENT-bg/nR/%rR/fR/retR",
      "!REPLACEMENT-bg/nR/n0/fR/retR",
      "!REPLACEMENT-bg/nR/nR/fR/retR",
      "!REPLACEMENT-bge/%rR/%rR/fR/retR",
      "!REPLACEMENT-bge/%rR/n0/fR/retR",
      "!REPLACEMENT-bge/%rR/nR/fR/retR",
      "!REPLACEMENT-bge/n0/%rR/fR/retR",
      "!REPLACEMENT-bge/n0/n0/fR/retR",
      "!REPLACEMENT-bge/n0/nR/fR/retR",
      "!REPLACEMENT-bge/nR/%rR/fR/retR",
      "!REPLACEMENT-bge/nR/n0/fR/retR",
      "!REPLACEMENT-bge/nR/nR/fR/retR",
      "!REPLACEMENT-bl/%rR/%rR/fR/retR",
      "!REPLACEMENT-bl/%rR/n0/fR/retR",
      "!REPLACEMENT-bl/%rR/nR/fR/retR",
      "!REPLACEMENT-bl/n0/%rR/fR/retR",
      "!REPLACEMENT-bl/n0/n0/fR/retR",
      "!REPLACEMENT-bl/n0/nR/fR/retR",
      "!REPLACEMENT-bl/nR/%rR/fR/retR",
      "!REPLACEMENT-bl/nR/n0/fR/retR",
      "!REPLACEMENT-bl/nR/nR/fR/retR",
      "!REPLACEMENT-ble/%rR/%rR/fR/retR",
      "!REPLACEMENT-ble/%rR/n0/fR/retR",
      "!REPLACEMENT-ble/%rR/nR/fR/retR",
      "!REPLACEMENT-ble/n0/%rR/fR/retR",
      "!REPLACEMENT-ble/n0/n0/fR/retR",
      "!REPLACEMENT-ble/n0/nR/fR/retR",
      "!REPLACEMENT-ble/nR/%rR/fR/retR",
      "!REPLACEMENT-ble/nR/n0/fR/retR",
      "!REPLACEMENT-ble/nR/nR/fR/retR",
      "!REPLACEMENT-bne/%rR/%rR/fR/retR",
      "!REPLACEMENT-bne/%rR/n0/fR/retR",
      "!REPLACEMENT-bne/%rR/nR/fR/retR",
      "!REPLACEMENT-bne/n0/%rR/fR/retR",
      "!REPLACEMENT-bne/n0/n0/fR/retR",
      "!REPLACEMENT-bne/n0/nR/fR/retR",
      "!REPLACEMENT-bne/nR/%rR/fR/retR",
      "!REPLACEMENT-bne/nR/n0/fR/retR",
      "!REPLACEMENT-bne/nR/nR/fR/retR",
      "!REPLACEMENT-bneqi-stack/n-1/fR/retR",
      "!REPLACEMENT-bneqi-stack/n0/fR/retR",
      "!REPLACEMENT-bneqi-stack/n1/fR/retR",
      "!REPLACEMENT-bneqi-stack/n2/fR/retR",
      "!REPLACEMENT-bneqi-stack/nR/fR/retR",
      "!REPLACEMENT-bt-stack/fR/retR",
      "!REPLACEMENT-call/fR/retR",
      "!REPLACEMENT-exitvm/retR",
      "!REPLACEMENT-procedure-prolog/retR",
      "!REPLACEMENT-return-to-undertop/retR",
      "!REPLACEMENT-unreachable/retR"
    };
// #include <stdlib.h>

// #include "structuredvm-specialized-instructions.h"
const size_t
structuredvm_specialized_instruction_residual_arities [STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      0, /* !INVALID */
      1, /* !BEGINBASICBLOCK */
      0, /* !EXITVM */
      0, /* !DATALOCATIONS */
      0, /* !NOP */
      0, /* !UNREACHABLE0 */
      0, /* !UNREACHABLE1 */
      0, /* !PRETENDTOJUMPANYWHERE */
      1, /* b/fR */
      3, /* be/%rR/%rR/fR */
      2, /* be/%rR/n0/fR */
      3, /* be/%rR/nR/fR */
      2, /* be/n0/%rR/fR */
      1, /* be/n0/n0/fR */
      2, /* be/n0/nR/fR */
      3, /* be/nR/%rR/fR */
      2, /* be/nR/n0/fR */
      3, /* be/nR/nR/fR */
      1, /* beqi-stack/n-1/fR */
      1, /* beqi-stack/n0/fR */
      1, /* beqi-stack/n1/fR */
      1, /* beqi-stack/n2/fR */
      2, /* beqi-stack/nR/fR */
      1, /* bf-stack/fR */
      3, /* bg/%rR/%rR/fR */
      2, /* bg/%rR/n0/fR */
      3, /* bg/%rR/nR/fR */
      2, /* bg/n0/%rR/fR */
      1, /* bg/n0/n0/fR */
      2, /* bg/n0/nR/fR */
      3, /* bg/nR/%rR/fR */
      2, /* bg/nR/n0/fR */
      3, /* bg/nR/nR/fR */
      3, /* bge/%rR/%rR/fR */
      2, /* bge/%rR/n0/fR */
      3, /* bge/%rR/nR/fR */
      2, /* bge/n0/%rR/fR */
      1, /* bge/n0/n0/fR */
      2, /* bge/n0/nR/fR */
      3, /* bge/nR/%rR/fR */
      2, /* bge/nR/n0/fR */
      3, /* bge/nR/nR/fR */
      3, /* bl/%rR/%rR/fR */
      2, /* bl/%rR/n0/fR */
      3, /* bl/%rR/nR/fR */
      2, /* bl/n0/%rR/fR */
      1, /* bl/n0/n0/fR */
      2, /* bl/n0/nR/fR */
      3, /* bl/nR/%rR/fR */
      2, /* bl/nR/n0/fR */
      3, /* bl/nR/nR/fR */
      3, /* ble/%rR/%rR/fR */
      2, /* ble/%rR/n0/fR */
      3, /* ble/%rR/nR/fR */
      2, /* ble/n0/%rR/fR */
      1, /* ble/n0/n0/fR */
      2, /* ble/n0/nR/fR */
      3, /* ble/nR/%rR/fR */
      2, /* ble/nR/n0/fR */
      3, /* ble/nR/nR/fR */
      3, /* bne/%rR/%rR/fR */
      2, /* bne/%rR/n0/fR */
      3, /* bne/%rR/nR/fR */
      2, /* bne/n0/%rR/fR */
      1, /* bne/n0/n0/fR */
      2, /* bne/n0/nR/fR */
      3, /* bne/nR/%rR/fR */
      2, /* bne/nR/n0/fR */
      3, /* bne/nR/nR/fR */
      1, /* bneqi-stack/n-1/fR */
      1, /* bneqi-stack/n0/fR */
      1, /* bneqi-stack/n1/fR */
      1, /* bneqi-stack/n2/fR */
      2, /* bneqi-stack/nR/fR */
      1, /* bt-stack/fR */
      2, /* call/fR/retR */
      1, /* copy-to-r-stack/%rR */
      0, /* different-stack */
      4, /* divided/%rR/%rR/%rR/retR */
      3, /* divided/%rR/n2/%rR/retR */
      4, /* divided/%rR/nR/%rR/retR */
      4, /* divided/nR/%rR/%rR/retR */
      3, /* divided/nR/n2/%rR/retR */
      4, /* divided/nR/nR/%rR/retR */
      1, /* divided-stack/retR */
      0, /* drop-stack */
      0, /* dup-stack */
      0, /* equal-stack */
      0, /* equali-stack/n0 */
      0, /* equali-stack/n1 */
      0, /* equali-stack/n2 */
      1, /* equali-stack/nR */
      0, /* exitvm */
      0, /* greater-stack */
      0, /* greaterorequal-stack */
      2, /* input/%rR/retR */
      1, /* input-stack/retR */
      0, /* isnonzero-stack */
      0, /* less-stack */
      0, /* lessorequal-stack */
      0, /* logicaland-stack */
      0, /* logicalnot-stack */
      0, /* logicalor-stack */
      3, /* minus/%rR/%rR/%rR */
      2, /* minus/%rR/n1/%rR */
      2, /* minus/%rR/n2/%rR */
      3, /* minus/%rR/nR/%rR */
      3, /* minus/nR/%rR/%rR */
      2, /* minus/nR/n1/%rR */
      2, /* minus/nR/n2/%rR */
      3, /* minus/nR/nR/%rR */
      0, /* minus-stack */
      0, /* minusi-stack/n1 */
      0, /* minusi-stack/n2 */
      1, /* minusi-stack/nR */
      2, /* mov/%rR/%rR */
      1, /* mov/n0/%rR */
      1, /* mov/n1/%rR */
      1, /* mov/n-1/%rR */
      1, /* mov/n2/%rR */
      2, /* mov/nR/%rR */
      3, /* plus/%rR/%rR/%rR */
      2, /* plus/%rR/n1/%rR */
      2, /* plus/%rR/n2/%rR */
      3, /* plus/%rR/nR/%rR */
      2, /* plus/n1/%rR/%rR */
      1, /* plus/n1/n1/%rR */
      1, /* plus/n1/n2/%rR */
      2, /* plus/n1/nR/%rR */
      2, /* plus/n2/%rR/%rR */
      1, /* plus/n2/n1/%rR */
      1, /* plus/n2/n2/%rR */
      2, /* plus/n2/nR/%rR */
      3, /* plus/nR/%rR/%rR */
      2, /* plus/nR/n1/%rR */
      2, /* plus/nR/n2/%rR */
      3, /* plus/nR/nR/%rR */
      0, /* plus-stack */
      0, /* plusi-stack/n-1 */
      0, /* plusi-stack/n1 */
      0, /* plusi-stack/n2 */
      1, /* plusi-stack/nR */
      1, /* pop-stack/%rR */
      2, /* print/%rR/retR */
      2, /* print/nR/retR */
      1, /* print-stack/retR */
      0, /* procedure-prolog */
      1, /* push-stack/%rR */
      0, /* push-stack/n0 */
      0, /* push-stack/n1 */
      0, /* push-stack/n-1 */
      0, /* push-stack/n2 */
      1, /* push-stack/nR */
      1, /* push-stack/lR */
      0, /* push-unspecified-stack */
      4, /* remainder/%rR/%rR/%rR/retR */
      3, /* remainder/%rR/n2/%rR/retR */
      4, /* remainder/%rR/nR/%rR/retR */
      4, /* remainder/nR/%rR/%rR/retR */
      3, /* remainder/nR/n2/%rR/retR */
      4, /* remainder/nR/nR/%rR/retR */
      1, /* remainder-stack/retR */
      0, /* return-to-undertop */
      0, /* swap-stack */
      3, /* times/%rR/%rR/%rR */
      2, /* times/%rR/n2/%rR */
      3, /* times/%rR/nR/%rR */
      3, /* times/nR/%rR/%rR */
      2, /* times/nR/n2/%rR */
      3, /* times/nR/nR/%rR */
      0, /* times-stack */
      2, /* uminus/%rR/%rR */
      2, /* uminus/nR/%rR */
      0, /* uminus-stack */
      1, /* underpop-stack/%rR */
      1, /* underpush-stack/%rR */
      1, /* underpush-stack/nR */
      0, /* unreachable */
      2, /* !REPLACEMENT-b/fR/retR */
      4, /* !REPLACEMENT-be/%rR/%rR/fR/retR */
      3, /* !REPLACEMENT-be/%rR/n0/fR/retR */
      4, /* !REPLACEMENT-be/%rR/nR/fR/retR */
      3, /* !REPLACEMENT-be/n0/%rR/fR/retR */
      2, /* !REPLACEMENT-be/n0/n0/fR/retR */
      3, /* !REPLACEMENT-be/n0/nR/fR/retR */
      4, /* !REPLACEMENT-be/nR/%rR/fR/retR */
      3, /* !REPLACEMENT-be/nR/n0/fR/retR */
      4, /* !REPLACEMENT-be/nR/nR/fR/retR */
      2, /* !REPLACEMENT-beqi-stack/n-1/fR/retR */
      2, /* !REPLACEMENT-beqi-stack/n0/fR/retR */
      2, /* !REPLACEMENT-beqi-stack/n1/fR/retR */
      2, /* !REPLACEMENT-beqi-stack/n2/fR/retR */
      3, /* !REPLACEMENT-beqi-stack/nR/fR/retR */
      2, /* !REPLACEMENT-bf-stack/fR/retR */
      4, /* !REPLACEMENT-bg/%rR/%rR/fR/retR */
      3, /* !REPLACEMENT-bg/%rR/n0/fR/retR */
      4, /* !REPLACEMENT-bg/%rR/nR/fR/retR */
      3, /* !REPLACEMENT-bg/n0/%rR/fR/retR */
      2, /* !REPLACEMENT-bg/n0/n0/fR/retR */
      3, /* !REPLACEMENT-bg/n0/nR/fR/retR */
      4, /* !REPLACEMENT-bg/nR/%rR/fR/retR */
      3, /* !REPLACEMENT-bg/nR/n0/fR/retR */
      4, /* !REPLACEMENT-bg/nR/nR/fR/retR */
      4, /* !REPLACEMENT-bge/%rR/%rR/fR/retR */
      3, /* !REPLACEMENT-bge/%rR/n0/fR/retR */
      4, /* !REPLACEMENT-bge/%rR/nR/fR/retR */
      3, /* !REPLACEMENT-bge/n0/%rR/fR/retR */
      2, /* !REPLACEMENT-bge/n0/n0/fR/retR */
      3, /* !REPLACEMENT-bge/n0/nR/fR/retR */
      4, /* !REPLACEMENT-bge/nR/%rR/fR/retR */
      3, /* !REPLACEMENT-bge/nR/n0/fR/retR */
      4, /* !REPLACEMENT-bge/nR/nR/fR/retR */
      4, /* !REPLACEMENT-bl/%rR/%rR/fR/retR */
      3, /* !REPLACEMENT-bl/%rR/n0/fR/retR */
      4, /* !REPLACEMENT-bl/%rR/nR/fR/retR */
      3, /* !REPLACEMENT-bl/n0/%rR/fR/retR */
      2, /* !REPLACEMENT-bl/n0/n0/fR/retR */
      3, /* !REPLACEMENT-bl/n0/nR/fR/retR */
      4, /* !REPLACEMENT-bl/nR/%rR/fR/retR */
      3, /* !REPLACEMENT-bl/nR/n0/fR/retR */
      4, /* !REPLACEMENT-bl/nR/nR/fR/retR */
      4, /* !REPLACEMENT-ble/%rR/%rR/fR/retR */
      3, /* !REPLACEMENT-ble/%rR/n0/fR/retR */
      4, /* !REPLACEMENT-ble/%rR/nR/fR/retR */
      3, /* !REPLACEMENT-ble/n0/%rR/fR/retR */
      2, /* !REPLACEMENT-ble/n0/n0/fR/retR */
      3, /* !REPLACEMENT-ble/n0/nR/fR/retR */
      4, /* !REPLACEMENT-ble/nR/%rR/fR/retR */
      3, /* !REPLACEMENT-ble/nR/n0/fR/retR */
      4, /* !REPLACEMENT-ble/nR/nR/fR/retR */
      4, /* !REPLACEMENT-bne/%rR/%rR/fR/retR */
      3, /* !REPLACEMENT-bne/%rR/n0/fR/retR */
      4, /* !REPLACEMENT-bne/%rR/nR/fR/retR */
      3, /* !REPLACEMENT-bne/n0/%rR/fR/retR */
      2, /* !REPLACEMENT-bne/n0/n0/fR/retR */
      3, /* !REPLACEMENT-bne/n0/nR/fR/retR */
      4, /* !REPLACEMENT-bne/nR/%rR/fR/retR */
      3, /* !REPLACEMENT-bne/nR/n0/fR/retR */
      4, /* !REPLACEMENT-bne/nR/nR/fR/retR */
      2, /* !REPLACEMENT-bneqi-stack/n-1/fR/retR */
      2, /* !REPLACEMENT-bneqi-stack/n0/fR/retR */
      2, /* !REPLACEMENT-bneqi-stack/n1/fR/retR */
      2, /* !REPLACEMENT-bneqi-stack/n2/fR/retR */
      3, /* !REPLACEMENT-bneqi-stack/nR/fR/retR */
      2, /* !REPLACEMENT-bt-stack/fR/retR */
      2, /* !REPLACEMENT-call/fR/retR */
      1, /* !REPLACEMENT-exitvm/retR */
      1, /* !REPLACEMENT-procedure-prolog/retR */
      1, /* !REPLACEMENT-return-to-undertop/retR */
      1 /* !REPLACEMENT-unreachable/retR */
    };
const unsigned long // FIXME: shall I use a shorter type when possible?
structuredvm_specialized_instruction_label_bitmasks [STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      /* It's important that !BEGINBASICBLOCK has a zero here: it does not need residual patching. */
      0, /* !INVALID */
      0, /* !BEGINBASICBLOCK */
      0, /* !EXITVM */
      0, /* !DATALOCATIONS */
      0, /* !NOP */
      0, /* !UNREACHABLE0 */
      0, /* !UNREACHABLE1 */
      0, /* !PRETENDTOJUMPANYWHERE */
      0 | (1UL << 0), /* b/fR */
      0 | (1UL << 2), /* be/%rR/%rR/fR */
      0 | (1UL << 1), /* be/%rR/n0/fR */
      0 | (1UL << 2), /* be/%rR/nR/fR */
      0 | (1UL << 1), /* be/n0/%rR/fR */
      0 | (1UL << 0), /* be/n0/n0/fR */
      0 | (1UL << 1), /* be/n0/nR/fR */
      0 | (1UL << 2), /* be/nR/%rR/fR */
      0 | (1UL << 1), /* be/nR/n0/fR */
      0 | (1UL << 2), /* be/nR/nR/fR */
      0 | (1UL << 0), /* beqi-stack/n-1/fR */
      0 | (1UL << 0), /* beqi-stack/n0/fR */
      0 | (1UL << 0), /* beqi-stack/n1/fR */
      0 | (1UL << 0), /* beqi-stack/n2/fR */
      0 | (1UL << 1), /* beqi-stack/nR/fR */
      0 | (1UL << 0), /* bf-stack/fR */
      0 | (1UL << 2), /* bg/%rR/%rR/fR */
      0 | (1UL << 1), /* bg/%rR/n0/fR */
      0 | (1UL << 2), /* bg/%rR/nR/fR */
      0 | (1UL << 1), /* bg/n0/%rR/fR */
      0 | (1UL << 0), /* bg/n0/n0/fR */
      0 | (1UL << 1), /* bg/n0/nR/fR */
      0 | (1UL << 2), /* bg/nR/%rR/fR */
      0 | (1UL << 1), /* bg/nR/n0/fR */
      0 | (1UL << 2), /* bg/nR/nR/fR */
      0 | (1UL << 2), /* bge/%rR/%rR/fR */
      0 | (1UL << 1), /* bge/%rR/n0/fR */
      0 | (1UL << 2), /* bge/%rR/nR/fR */
      0 | (1UL << 1), /* bge/n0/%rR/fR */
      0 | (1UL << 0), /* bge/n0/n0/fR */
      0 | (1UL << 1), /* bge/n0/nR/fR */
      0 | (1UL << 2), /* bge/nR/%rR/fR */
      0 | (1UL << 1), /* bge/nR/n0/fR */
      0 | (1UL << 2), /* bge/nR/nR/fR */
      0 | (1UL << 2), /* bl/%rR/%rR/fR */
      0 | (1UL << 1), /* bl/%rR/n0/fR */
      0 | (1UL << 2), /* bl/%rR/nR/fR */
      0 | (1UL << 1), /* bl/n0/%rR/fR */
      0 | (1UL << 0), /* bl/n0/n0/fR */
      0 | (1UL << 1), /* bl/n0/nR/fR */
      0 | (1UL << 2), /* bl/nR/%rR/fR */
      0 | (1UL << 1), /* bl/nR/n0/fR */
      0 | (1UL << 2), /* bl/nR/nR/fR */
      0 | (1UL << 2), /* ble/%rR/%rR/fR */
      0 | (1UL << 1), /* ble/%rR/n0/fR */
      0 | (1UL << 2), /* ble/%rR/nR/fR */
      0 | (1UL << 1), /* ble/n0/%rR/fR */
      0 | (1UL << 0), /* ble/n0/n0/fR */
      0 | (1UL << 1), /* ble/n0/nR/fR */
      0 | (1UL << 2), /* ble/nR/%rR/fR */
      0 | (1UL << 1), /* ble/nR/n0/fR */
      0 | (1UL << 2), /* ble/nR/nR/fR */
      0 | (1UL << 2), /* bne/%rR/%rR/fR */
      0 | (1UL << 1), /* bne/%rR/n0/fR */
      0 | (1UL << 2), /* bne/%rR/nR/fR */
      0 | (1UL << 1), /* bne/n0/%rR/fR */
      0 | (1UL << 0), /* bne/n0/n0/fR */
      0 | (1UL << 1), /* bne/n0/nR/fR */
      0 | (1UL << 2), /* bne/nR/%rR/fR */
      0 | (1UL << 1), /* bne/nR/n0/fR */
      0 | (1UL << 2), /* bne/nR/nR/fR */
      0 | (1UL << 0), /* bneqi-stack/n-1/fR */
      0 | (1UL << 0), /* bneqi-stack/n0/fR */
      0 | (1UL << 0), /* bneqi-stack/n1/fR */
      0 | (1UL << 0), /* bneqi-stack/n2/fR */
      0 | (1UL << 1), /* bneqi-stack/nR/fR */
      0 | (1UL << 0), /* bt-stack/fR */
      0 | (1UL << 0), /* call/fR/retR */
      0, /* copy-to-r-stack/%rR */
      0, /* different-stack */
      0, /* divided/%rR/%rR/%rR/retR */
      0, /* divided/%rR/n2/%rR/retR */
      0, /* divided/%rR/nR/%rR/retR */
      0, /* divided/nR/%rR/%rR/retR */
      0, /* divided/nR/n2/%rR/retR */
      0, /* divided/nR/nR/%rR/retR */
      0, /* divided-stack/retR */
      0, /* drop-stack */
      0, /* dup-stack */
      0, /* equal-stack */
      0, /* equali-stack/n0 */
      0, /* equali-stack/n1 */
      0, /* equali-stack/n2 */
      0, /* equali-stack/nR */
      0, /* exitvm */
      0, /* greater-stack */
      0, /* greaterorequal-stack */
      0, /* input/%rR/retR */
      0, /* input-stack/retR */
      0, /* isnonzero-stack */
      0, /* less-stack */
      0, /* lessorequal-stack */
      0, /* logicaland-stack */
      0, /* logicalnot-stack */
      0, /* logicalor-stack */
      0, /* minus/%rR/%rR/%rR */
      0, /* minus/%rR/n1/%rR */
      0, /* minus/%rR/n2/%rR */
      0, /* minus/%rR/nR/%rR */
      0, /* minus/nR/%rR/%rR */
      0, /* minus/nR/n1/%rR */
      0, /* minus/nR/n2/%rR */
      0, /* minus/nR/nR/%rR */
      0, /* minus-stack */
      0, /* minusi-stack/n1 */
      0, /* minusi-stack/n2 */
      0, /* minusi-stack/nR */
      0, /* mov/%rR/%rR */
      0, /* mov/n0/%rR */
      0, /* mov/n1/%rR */
      0, /* mov/n-1/%rR */
      0, /* mov/n2/%rR */
      0, /* mov/nR/%rR */
      0, /* plus/%rR/%rR/%rR */
      0, /* plus/%rR/n1/%rR */
      0, /* plus/%rR/n2/%rR */
      0, /* plus/%rR/nR/%rR */
      0, /* plus/n1/%rR/%rR */
      0, /* plus/n1/n1/%rR */
      0, /* plus/n1/n2/%rR */
      0, /* plus/n1/nR/%rR */
      0, /* plus/n2/%rR/%rR */
      0, /* plus/n2/n1/%rR */
      0, /* plus/n2/n2/%rR */
      0, /* plus/n2/nR/%rR */
      0, /* plus/nR/%rR/%rR */
      0, /* plus/nR/n1/%rR */
      0, /* plus/nR/n2/%rR */
      0, /* plus/nR/nR/%rR */
      0, /* plus-stack */
      0, /* plusi-stack/n-1 */
      0, /* plusi-stack/n1 */
      0, /* plusi-stack/n2 */
      0, /* plusi-stack/nR */
      0, /* pop-stack/%rR */
      0, /* print/%rR/retR */
      0, /* print/nR/retR */
      0, /* print-stack/retR */
      0, /* procedure-prolog */
      0, /* push-stack/%rR */
      0, /* push-stack/n0 */
      0, /* push-stack/n1 */
      0, /* push-stack/n-1 */
      0, /* push-stack/n2 */
      0, /* push-stack/nR */
      0 | (1UL << 0), /* push-stack/lR */
      0, /* push-unspecified-stack */
      0, /* remainder/%rR/%rR/%rR/retR */
      0, /* remainder/%rR/n2/%rR/retR */
      0, /* remainder/%rR/nR/%rR/retR */
      0, /* remainder/nR/%rR/%rR/retR */
      0, /* remainder/nR/n2/%rR/retR */
      0, /* remainder/nR/nR/%rR/retR */
      0, /* remainder-stack/retR */
      0, /* return-to-undertop */
      0, /* swap-stack */
      0, /* times/%rR/%rR/%rR */
      0, /* times/%rR/n2/%rR */
      0, /* times/%rR/nR/%rR */
      0, /* times/nR/%rR/%rR */
      0, /* times/nR/n2/%rR */
      0, /* times/nR/nR/%rR */
      0, /* times-stack */
      0, /* uminus/%rR/%rR */
      0, /* uminus/nR/%rR */
      0, /* uminus-stack */
      0, /* underpop-stack/%rR */
      0, /* underpush-stack/%rR */
      0, /* underpush-stack/nR */
      0, /* unreachable */
      0 | (1UL << 0), /* !REPLACEMENT-b/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-be/%rR/%rR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-be/%rR/n0/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-be/%rR/nR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-be/n0/%rR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-be/n0/n0/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-be/n0/nR/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-be/nR/%rR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-be/nR/n0/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-be/nR/nR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-beqi-stack/n-1/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-beqi-stack/n0/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-beqi-stack/n1/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-beqi-stack/n2/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-beqi-stack/nR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bf-stack/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bg/%rR/%rR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bg/%rR/n0/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bg/%rR/nR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bg/n0/%rR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bg/n0/n0/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bg/n0/nR/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bg/nR/%rR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bg/nR/n0/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bg/nR/nR/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bge/%rR/%rR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bge/%rR/n0/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bge/%rR/nR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bge/n0/%rR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bge/n0/n0/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bge/n0/nR/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bge/nR/%rR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bge/nR/n0/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bge/nR/nR/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bl/%rR/%rR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bl/%rR/n0/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bl/%rR/nR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bl/n0/%rR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bl/n0/n0/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bl/n0/nR/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bl/nR/%rR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bl/nR/n0/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bl/nR/nR/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-ble/%rR/%rR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-ble/%rR/n0/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-ble/%rR/nR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-ble/n0/%rR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-ble/n0/n0/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-ble/n0/nR/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-ble/nR/%rR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-ble/nR/n0/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-ble/nR/nR/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bne/%rR/%rR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bne/%rR/n0/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bne/%rR/nR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bne/n0/%rR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bne/n0/n0/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bne/n0/nR/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bne/nR/%rR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bne/nR/n0/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-bne/nR/nR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bneqi-stack/n-1/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bneqi-stack/n0/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bneqi-stack/n1/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bneqi-stack/n2/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-bneqi-stack/nR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bt-stack/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-call/fR/retR */
      0, /* !REPLACEMENT-exitvm/retR */
      0, /* !REPLACEMENT-procedure-prolog/retR */
      0, /* !REPLACEMENT-return-to-undertop/retR */
      0 /* !REPLACEMENT-unreachable/retR */
    };
#ifdef JITTER_HAVE_PATCH_IN
const unsigned long // FIXME: shall I use a shorter type when possible?
structuredvm_specialized_instruction_fast_label_bitmasks [STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      /* It's important that !BEGINBASICBLOCK has a zero here: it does not need residual patching. */
      0, /* !INVALID */
      0, /* !BEGINBASICBLOCK */
      0, /* !EXITVM */
      0, /* !DATALOCATIONS */
      0, /* !NOP */
      0, /* !UNREACHABLE0 */
      0, /* !UNREACHABLE1 */
      0, /* !PRETENDTOJUMPANYWHERE */
      0 | (1UL << 0), /* b/fR */
      0 | (1UL << 2), /* be/%rR/%rR/fR */
      0 | (1UL << 1), /* be/%rR/n0/fR */
      0 | (1UL << 2), /* be/%rR/nR/fR */
      0 | (1UL << 1), /* be/n0/%rR/fR */
      0 | (1UL << 0), /* be/n0/n0/fR */
      0 | (1UL << 1), /* be/n0/nR/fR */
      0 | (1UL << 2), /* be/nR/%rR/fR */
      0 | (1UL << 1), /* be/nR/n0/fR */
      0 | (1UL << 2), /* be/nR/nR/fR */
      0 | (1UL << 0), /* beqi-stack/n-1/fR */
      0 | (1UL << 0), /* beqi-stack/n0/fR */
      0 | (1UL << 0), /* beqi-stack/n1/fR */
      0 | (1UL << 0), /* beqi-stack/n2/fR */
      0 | (1UL << 1), /* beqi-stack/nR/fR */
      0 | (1UL << 0), /* bf-stack/fR */
      0 | (1UL << 2), /* bg/%rR/%rR/fR */
      0 | (1UL << 1), /* bg/%rR/n0/fR */
      0 | (1UL << 2), /* bg/%rR/nR/fR */
      0 | (1UL << 1), /* bg/n0/%rR/fR */
      0 | (1UL << 0), /* bg/n0/n0/fR */
      0 | (1UL << 1), /* bg/n0/nR/fR */
      0 | (1UL << 2), /* bg/nR/%rR/fR */
      0 | (1UL << 1), /* bg/nR/n0/fR */
      0 | (1UL << 2), /* bg/nR/nR/fR */
      0 | (1UL << 2), /* bge/%rR/%rR/fR */
      0 | (1UL << 1), /* bge/%rR/n0/fR */
      0 | (1UL << 2), /* bge/%rR/nR/fR */
      0 | (1UL << 1), /* bge/n0/%rR/fR */
      0 | (1UL << 0), /* bge/n0/n0/fR */
      0 | (1UL << 1), /* bge/n0/nR/fR */
      0 | (1UL << 2), /* bge/nR/%rR/fR */
      0 | (1UL << 1), /* bge/nR/n0/fR */
      0 | (1UL << 2), /* bge/nR/nR/fR */
      0 | (1UL << 2), /* bl/%rR/%rR/fR */
      0 | (1UL << 1), /* bl/%rR/n0/fR */
      0 | (1UL << 2), /* bl/%rR/nR/fR */
      0 | (1UL << 1), /* bl/n0/%rR/fR */
      0 | (1UL << 0), /* bl/n0/n0/fR */
      0 | (1UL << 1), /* bl/n0/nR/fR */
      0 | (1UL << 2), /* bl/nR/%rR/fR */
      0 | (1UL << 1), /* bl/nR/n0/fR */
      0 | (1UL << 2), /* bl/nR/nR/fR */
      0 | (1UL << 2), /* ble/%rR/%rR/fR */
      0 | (1UL << 1), /* ble/%rR/n0/fR */
      0 | (1UL << 2), /* ble/%rR/nR/fR */
      0 | (1UL << 1), /* ble/n0/%rR/fR */
      0 | (1UL << 0), /* ble/n0/n0/fR */
      0 | (1UL << 1), /* ble/n0/nR/fR */
      0 | (1UL << 2), /* ble/nR/%rR/fR */
      0 | (1UL << 1), /* ble/nR/n0/fR */
      0 | (1UL << 2), /* ble/nR/nR/fR */
      0 | (1UL << 2), /* bne/%rR/%rR/fR */
      0 | (1UL << 1), /* bne/%rR/n0/fR */
      0 | (1UL << 2), /* bne/%rR/nR/fR */
      0 | (1UL << 1), /* bne/n0/%rR/fR */
      0 | (1UL << 0), /* bne/n0/n0/fR */
      0 | (1UL << 1), /* bne/n0/nR/fR */
      0 | (1UL << 2), /* bne/nR/%rR/fR */
      0 | (1UL << 1), /* bne/nR/n0/fR */
      0 | (1UL << 2), /* bne/nR/nR/fR */
      0 | (1UL << 0), /* bneqi-stack/n-1/fR */
      0 | (1UL << 0), /* bneqi-stack/n0/fR */
      0 | (1UL << 0), /* bneqi-stack/n1/fR */
      0 | (1UL << 0), /* bneqi-stack/n2/fR */
      0 | (1UL << 1), /* bneqi-stack/nR/fR */
      0 | (1UL << 0), /* bt-stack/fR */
      0 | (1UL << 0), /* call/fR/retR */
      0, /* copy-to-r-stack/%rR */
      0, /* different-stack */
      0, /* divided/%rR/%rR/%rR/retR */
      0, /* divided/%rR/n2/%rR/retR */
      0, /* divided/%rR/nR/%rR/retR */
      0, /* divided/nR/%rR/%rR/retR */
      0, /* divided/nR/n2/%rR/retR */
      0, /* divided/nR/nR/%rR/retR */
      0, /* divided-stack/retR */
      0, /* drop-stack */
      0, /* dup-stack */
      0, /* equal-stack */
      0, /* equali-stack/n0 */
      0, /* equali-stack/n1 */
      0, /* equali-stack/n2 */
      0, /* equali-stack/nR */
      0, /* exitvm */
      0, /* greater-stack */
      0, /* greaterorequal-stack */
      0, /* input/%rR/retR */
      0, /* input-stack/retR */
      0, /* isnonzero-stack */
      0, /* less-stack */
      0, /* lessorequal-stack */
      0, /* logicaland-stack */
      0, /* logicalnot-stack */
      0, /* logicalor-stack */
      0, /* minus/%rR/%rR/%rR */
      0, /* minus/%rR/n1/%rR */
      0, /* minus/%rR/n2/%rR */
      0, /* minus/%rR/nR/%rR */
      0, /* minus/nR/%rR/%rR */
      0, /* minus/nR/n1/%rR */
      0, /* minus/nR/n2/%rR */
      0, /* minus/nR/nR/%rR */
      0, /* minus-stack */
      0, /* minusi-stack/n1 */
      0, /* minusi-stack/n2 */
      0, /* minusi-stack/nR */
      0, /* mov/%rR/%rR */
      0, /* mov/n0/%rR */
      0, /* mov/n1/%rR */
      0, /* mov/n-1/%rR */
      0, /* mov/n2/%rR */
      0, /* mov/nR/%rR */
      0, /* plus/%rR/%rR/%rR */
      0, /* plus/%rR/n1/%rR */
      0, /* plus/%rR/n2/%rR */
      0, /* plus/%rR/nR/%rR */
      0, /* plus/n1/%rR/%rR */
      0, /* plus/n1/n1/%rR */
      0, /* plus/n1/n2/%rR */
      0, /* plus/n1/nR/%rR */
      0, /* plus/n2/%rR/%rR */
      0, /* plus/n2/n1/%rR */
      0, /* plus/n2/n2/%rR */
      0, /* plus/n2/nR/%rR */
      0, /* plus/nR/%rR/%rR */
      0, /* plus/nR/n1/%rR */
      0, /* plus/nR/n2/%rR */
      0, /* plus/nR/nR/%rR */
      0, /* plus-stack */
      0, /* plusi-stack/n-1 */
      0, /* plusi-stack/n1 */
      0, /* plusi-stack/n2 */
      0, /* plusi-stack/nR */
      0, /* pop-stack/%rR */
      0, /* print/%rR/retR */
      0, /* print/nR/retR */
      0, /* print-stack/retR */
      0, /* procedure-prolog */
      0, /* push-stack/%rR */
      0, /* push-stack/n0 */
      0, /* push-stack/n1 */
      0, /* push-stack/n-1 */
      0, /* push-stack/n2 */
      0, /* push-stack/nR */
      0, /* push-stack/lR */
      0, /* push-unspecified-stack */
      0, /* remainder/%rR/%rR/%rR/retR */
      0, /* remainder/%rR/n2/%rR/retR */
      0, /* remainder/%rR/nR/%rR/retR */
      0, /* remainder/nR/%rR/%rR/retR */
      0, /* remainder/nR/n2/%rR/retR */
      0, /* remainder/nR/nR/%rR/retR */
      0, /* remainder-stack/retR */
      0, /* return-to-undertop */
      0, /* swap-stack */
      0, /* times/%rR/%rR/%rR */
      0, /* times/%rR/n2/%rR */
      0, /* times/%rR/nR/%rR */
      0, /* times/nR/%rR/%rR */
      0, /* times/nR/n2/%rR */
      0, /* times/nR/nR/%rR */
      0, /* times-stack */
      0, /* uminus/%rR/%rR */
      0, /* uminus/nR/%rR */
      0, /* uminus-stack */
      0, /* underpop-stack/%rR */
      0, /* underpush-stack/%rR */
      0, /* underpush-stack/nR */
      0, /* unreachable */
      0, /* !REPLACEMENT-b/fR/retR */
      0, /* !REPLACEMENT-be/%rR/%rR/fR/retR */
      0, /* !REPLACEMENT-be/%rR/n0/fR/retR */
      0, /* !REPLACEMENT-be/%rR/nR/fR/retR */
      0, /* !REPLACEMENT-be/n0/%rR/fR/retR */
      0, /* !REPLACEMENT-be/n0/n0/fR/retR */
      0, /* !REPLACEMENT-be/n0/nR/fR/retR */
      0, /* !REPLACEMENT-be/nR/%rR/fR/retR */
      0, /* !REPLACEMENT-be/nR/n0/fR/retR */
      0, /* !REPLACEMENT-be/nR/nR/fR/retR */
      0, /* !REPLACEMENT-beqi-stack/n-1/fR/retR */
      0, /* !REPLACEMENT-beqi-stack/n0/fR/retR */
      0, /* !REPLACEMENT-beqi-stack/n1/fR/retR */
      0, /* !REPLACEMENT-beqi-stack/n2/fR/retR */
      0, /* !REPLACEMENT-beqi-stack/nR/fR/retR */
      0, /* !REPLACEMENT-bf-stack/fR/retR */
      0, /* !REPLACEMENT-bg/%rR/%rR/fR/retR */
      0, /* !REPLACEMENT-bg/%rR/n0/fR/retR */
      0, /* !REPLACEMENT-bg/%rR/nR/fR/retR */
      0, /* !REPLACEMENT-bg/n0/%rR/fR/retR */
      0, /* !REPLACEMENT-bg/n0/n0/fR/retR */
      0, /* !REPLACEMENT-bg/n0/nR/fR/retR */
      0, /* !REPLACEMENT-bg/nR/%rR/fR/retR */
      0, /* !REPLACEMENT-bg/nR/n0/fR/retR */
      0, /* !REPLACEMENT-bg/nR/nR/fR/retR */
      0, /* !REPLACEMENT-bge/%rR/%rR/fR/retR */
      0, /* !REPLACEMENT-bge/%rR/n0/fR/retR */
      0, /* !REPLACEMENT-bge/%rR/nR/fR/retR */
      0, /* !REPLACEMENT-bge/n0/%rR/fR/retR */
      0, /* !REPLACEMENT-bge/n0/n0/fR/retR */
      0, /* !REPLACEMENT-bge/n0/nR/fR/retR */
      0, /* !REPLACEMENT-bge/nR/%rR/fR/retR */
      0, /* !REPLACEMENT-bge/nR/n0/fR/retR */
      0, /* !REPLACEMENT-bge/nR/nR/fR/retR */
      0, /* !REPLACEMENT-bl/%rR/%rR/fR/retR */
      0, /* !REPLACEMENT-bl/%rR/n0/fR/retR */
      0, /* !REPLACEMENT-bl/%rR/nR/fR/retR */
      0, /* !REPLACEMENT-bl/n0/%rR/fR/retR */
      0, /* !REPLACEMENT-bl/n0/n0/fR/retR */
      0, /* !REPLACEMENT-bl/n0/nR/fR/retR */
      0, /* !REPLACEMENT-bl/nR/%rR/fR/retR */
      0, /* !REPLACEMENT-bl/nR/n0/fR/retR */
      0, /* !REPLACEMENT-bl/nR/nR/fR/retR */
      0, /* !REPLACEMENT-ble/%rR/%rR/fR/retR */
      0, /* !REPLACEMENT-ble/%rR/n0/fR/retR */
      0, /* !REPLACEMENT-ble/%rR/nR/fR/retR */
      0, /* !REPLACEMENT-ble/n0/%rR/fR/retR */
      0, /* !REPLACEMENT-ble/n0/n0/fR/retR */
      0, /* !REPLACEMENT-ble/n0/nR/fR/retR */
      0, /* !REPLACEMENT-ble/nR/%rR/fR/retR */
      0, /* !REPLACEMENT-ble/nR/n0/fR/retR */
      0, /* !REPLACEMENT-ble/nR/nR/fR/retR */
      0, /* !REPLACEMENT-bne/%rR/%rR/fR/retR */
      0, /* !REPLACEMENT-bne/%rR/n0/fR/retR */
      0, /* !REPLACEMENT-bne/%rR/nR/fR/retR */
      0, /* !REPLACEMENT-bne/n0/%rR/fR/retR */
      0, /* !REPLACEMENT-bne/n0/n0/fR/retR */
      0, /* !REPLACEMENT-bne/n0/nR/fR/retR */
      0, /* !REPLACEMENT-bne/nR/%rR/fR/retR */
      0, /* !REPLACEMENT-bne/nR/n0/fR/retR */
      0, /* !REPLACEMENT-bne/nR/nR/fR/retR */
      0, /* !REPLACEMENT-bneqi-stack/n-1/fR/retR */
      0, /* !REPLACEMENT-bneqi-stack/n0/fR/retR */
      0, /* !REPLACEMENT-bneqi-stack/n1/fR/retR */
      0, /* !REPLACEMENT-bneqi-stack/n2/fR/retR */
      0, /* !REPLACEMENT-bneqi-stack/nR/fR/retR */
      0, /* !REPLACEMENT-bt-stack/fR/retR */
      0, /* !REPLACEMENT-call/fR/retR */
      0, /* !REPLACEMENT-exitvm/retR */
      0, /* !REPLACEMENT-procedure-prolog/retR */
      0, /* !REPLACEMENT-return-to-undertop/retR */
      0 /* !REPLACEMENT-unreachable/retR */
    };
#endif // #ifdef JITTER_HAVE_PATCH_IN

// FIXME: I may want to conditionalize this.
const bool
structuredvm_specialized_instruction_relocatables [STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      true, // !INVALID
      true, // !BEGINBASICBLOCK
      true, // !EXITVM
      true, // !DATALOCATIONS
      true, // !NOP
      true, // !UNREACHABLE0
      true, // !UNREACHABLE1
      true, // !PRETENDTOJUMPANYWHERE
      true, // b/fR
      true, // be/%rR/%rR/fR
      true, // be/%rR/n0/fR
      true, // be/%rR/nR/fR
      true, // be/n0/%rR/fR
      true, // be/n0/n0/fR
      true, // be/n0/nR/fR
      true, // be/nR/%rR/fR
      true, // be/nR/n0/fR
      true, // be/nR/nR/fR
      true, // beqi-stack/n-1/fR
      true, // beqi-stack/n0/fR
      true, // beqi-stack/n1/fR
      true, // beqi-stack/n2/fR
      true, // beqi-stack/nR/fR
      true, // bf-stack/fR
      true, // bg/%rR/%rR/fR
      true, // bg/%rR/n0/fR
      true, // bg/%rR/nR/fR
      true, // bg/n0/%rR/fR
      true, // bg/n0/n0/fR
      true, // bg/n0/nR/fR
      true, // bg/nR/%rR/fR
      true, // bg/nR/n0/fR
      true, // bg/nR/nR/fR
      true, // bge/%rR/%rR/fR
      true, // bge/%rR/n0/fR
      true, // bge/%rR/nR/fR
      true, // bge/n0/%rR/fR
      true, // bge/n0/n0/fR
      true, // bge/n0/nR/fR
      true, // bge/nR/%rR/fR
      true, // bge/nR/n0/fR
      true, // bge/nR/nR/fR
      true, // bl/%rR/%rR/fR
      true, // bl/%rR/n0/fR
      true, // bl/%rR/nR/fR
      true, // bl/n0/%rR/fR
      true, // bl/n0/n0/fR
      true, // bl/n0/nR/fR
      true, // bl/nR/%rR/fR
      true, // bl/nR/n0/fR
      true, // bl/nR/nR/fR
      true, // ble/%rR/%rR/fR
      true, // ble/%rR/n0/fR
      true, // ble/%rR/nR/fR
      true, // ble/n0/%rR/fR
      true, // ble/n0/n0/fR
      true, // ble/n0/nR/fR
      true, // ble/nR/%rR/fR
      true, // ble/nR/n0/fR
      true, // ble/nR/nR/fR
      true, // bne/%rR/%rR/fR
      true, // bne/%rR/n0/fR
      true, // bne/%rR/nR/fR
      true, // bne/n0/%rR/fR
      true, // bne/n0/n0/fR
      true, // bne/n0/nR/fR
      true, // bne/nR/%rR/fR
      true, // bne/nR/n0/fR
      true, // bne/nR/nR/fR
      true, // bneqi-stack/n-1/fR
      true, // bneqi-stack/n0/fR
      true, // bneqi-stack/n1/fR
      true, // bneqi-stack/n2/fR
      true, // bneqi-stack/nR/fR
      true, // bt-stack/fR
      true, // call/fR/retR
      true, // copy-to-r-stack/%rR
      true, // different-stack
      false, // divided/%rR/%rR/%rR/retR
      false, // divided/%rR/n2/%rR/retR
      false, // divided/%rR/nR/%rR/retR
      false, // divided/nR/%rR/%rR/retR
      false, // divided/nR/n2/%rR/retR
      false, // divided/nR/nR/%rR/retR
      false, // divided-stack/retR
      true, // drop-stack
      true, // dup-stack
      true, // equal-stack
      true, // equali-stack/n0
      true, // equali-stack/n1
      true, // equali-stack/n2
      true, // equali-stack/nR
      true, // exitvm
      true, // greater-stack
      true, // greaterorequal-stack
      false, // input/%rR/retR
      false, // input-stack/retR
      true, // isnonzero-stack
      true, // less-stack
      true, // lessorequal-stack
      true, // logicaland-stack
      true, // logicalnot-stack
      true, // logicalor-stack
      true, // minus/%rR/%rR/%rR
      true, // minus/%rR/n1/%rR
      true, // minus/%rR/n2/%rR
      true, // minus/%rR/nR/%rR
      true, // minus/nR/%rR/%rR
      true, // minus/nR/n1/%rR
      true, // minus/nR/n2/%rR
      true, // minus/nR/nR/%rR
      true, // minus-stack
      true, // minusi-stack/n1
      true, // minusi-stack/n2
      true, // minusi-stack/nR
      true, // mov/%rR/%rR
      true, // mov/n0/%rR
      true, // mov/n1/%rR
      true, // mov/n-1/%rR
      true, // mov/n2/%rR
      true, // mov/nR/%rR
      true, // plus/%rR/%rR/%rR
      true, // plus/%rR/n1/%rR
      true, // plus/%rR/n2/%rR
      true, // plus/%rR/nR/%rR
      true, // plus/n1/%rR/%rR
      true, // plus/n1/n1/%rR
      true, // plus/n1/n2/%rR
      true, // plus/n1/nR/%rR
      true, // plus/n2/%rR/%rR
      true, // plus/n2/n1/%rR
      true, // plus/n2/n2/%rR
      true, // plus/n2/nR/%rR
      true, // plus/nR/%rR/%rR
      true, // plus/nR/n1/%rR
      true, // plus/nR/n2/%rR
      true, // plus/nR/nR/%rR
      true, // plus-stack
      true, // plusi-stack/n-1
      true, // plusi-stack/n1
      true, // plusi-stack/n2
      true, // plusi-stack/nR
      true, // pop-stack/%rR
      false, // print/%rR/retR
      false, // print/nR/retR
      false, // print-stack/retR
      true, // procedure-prolog
      true, // push-stack/%rR
      true, // push-stack/n0
      true, // push-stack/n1
      true, // push-stack/n-1
      true, // push-stack/n2
      true, // push-stack/nR
      true, // push-stack/lR
      true, // push-unspecified-stack
      false, // remainder/%rR/%rR/%rR/retR
      false, // remainder/%rR/n2/%rR/retR
      false, // remainder/%rR/nR/%rR/retR
      false, // remainder/nR/%rR/%rR/retR
      false, // remainder/nR/n2/%rR/retR
      false, // remainder/nR/nR/%rR/retR
      false, // remainder-stack/retR
      true, // return-to-undertop
      true, // swap-stack
      true, // times/%rR/%rR/%rR
      true, // times/%rR/n2/%rR
      true, // times/%rR/nR/%rR
      true, // times/nR/%rR/%rR
      true, // times/nR/n2/%rR
      true, // times/nR/nR/%rR
      true, // times-stack
      true, // uminus/%rR/%rR
      true, // uminus/nR/%rR
      true, // uminus-stack
      true, // underpop-stack/%rR
      true, // underpush-stack/%rR
      true, // underpush-stack/nR
      true, // unreachable
      false, // !REPLACEMENT-b/fR/retR
      false, // !REPLACEMENT-be/%rR/%rR/fR/retR
      false, // !REPLACEMENT-be/%rR/n0/fR/retR
      false, // !REPLACEMENT-be/%rR/nR/fR/retR
      false, // !REPLACEMENT-be/n0/%rR/fR/retR
      false, // !REPLACEMENT-be/n0/n0/fR/retR
      false, // !REPLACEMENT-be/n0/nR/fR/retR
      false, // !REPLACEMENT-be/nR/%rR/fR/retR
      false, // !REPLACEMENT-be/nR/n0/fR/retR
      false, // !REPLACEMENT-be/nR/nR/fR/retR
      false, // !REPLACEMENT-beqi-stack/n-1/fR/retR
      false, // !REPLACEMENT-beqi-stack/n0/fR/retR
      false, // !REPLACEMENT-beqi-stack/n1/fR/retR
      false, // !REPLACEMENT-beqi-stack/n2/fR/retR
      false, // !REPLACEMENT-beqi-stack/nR/fR/retR
      false, // !REPLACEMENT-bf-stack/fR/retR
      false, // !REPLACEMENT-bg/%rR/%rR/fR/retR
      false, // !REPLACEMENT-bg/%rR/n0/fR/retR
      false, // !REPLACEMENT-bg/%rR/nR/fR/retR
      false, // !REPLACEMENT-bg/n0/%rR/fR/retR
      false, // !REPLACEMENT-bg/n0/n0/fR/retR
      false, // !REPLACEMENT-bg/n0/nR/fR/retR
      false, // !REPLACEMENT-bg/nR/%rR/fR/retR
      false, // !REPLACEMENT-bg/nR/n0/fR/retR
      false, // !REPLACEMENT-bg/nR/nR/fR/retR
      false, // !REPLACEMENT-bge/%rR/%rR/fR/retR
      false, // !REPLACEMENT-bge/%rR/n0/fR/retR
      false, // !REPLACEMENT-bge/%rR/nR/fR/retR
      false, // !REPLACEMENT-bge/n0/%rR/fR/retR
      false, // !REPLACEMENT-bge/n0/n0/fR/retR
      false, // !REPLACEMENT-bge/n0/nR/fR/retR
      false, // !REPLACEMENT-bge/nR/%rR/fR/retR
      false, // !REPLACEMENT-bge/nR/n0/fR/retR
      false, // !REPLACEMENT-bge/nR/nR/fR/retR
      false, // !REPLACEMENT-bl/%rR/%rR/fR/retR
      false, // !REPLACEMENT-bl/%rR/n0/fR/retR
      false, // !REPLACEMENT-bl/%rR/nR/fR/retR
      false, // !REPLACEMENT-bl/n0/%rR/fR/retR
      false, // !REPLACEMENT-bl/n0/n0/fR/retR
      false, // !REPLACEMENT-bl/n0/nR/fR/retR
      false, // !REPLACEMENT-bl/nR/%rR/fR/retR
      false, // !REPLACEMENT-bl/nR/n0/fR/retR
      false, // !REPLACEMENT-bl/nR/nR/fR/retR
      false, // !REPLACEMENT-ble/%rR/%rR/fR/retR
      false, // !REPLACEMENT-ble/%rR/n0/fR/retR
      false, // !REPLACEMENT-ble/%rR/nR/fR/retR
      false, // !REPLACEMENT-ble/n0/%rR/fR/retR
      false, // !REPLACEMENT-ble/n0/n0/fR/retR
      false, // !REPLACEMENT-ble/n0/nR/fR/retR
      false, // !REPLACEMENT-ble/nR/%rR/fR/retR
      false, // !REPLACEMENT-ble/nR/n0/fR/retR
      false, // !REPLACEMENT-ble/nR/nR/fR/retR
      false, // !REPLACEMENT-bne/%rR/%rR/fR/retR
      false, // !REPLACEMENT-bne/%rR/n0/fR/retR
      false, // !REPLACEMENT-bne/%rR/nR/fR/retR
      false, // !REPLACEMENT-bne/n0/%rR/fR/retR
      false, // !REPLACEMENT-bne/n0/n0/fR/retR
      false, // !REPLACEMENT-bne/n0/nR/fR/retR
      false, // !REPLACEMENT-bne/nR/%rR/fR/retR
      false, // !REPLACEMENT-bne/nR/n0/fR/retR
      false, // !REPLACEMENT-bne/nR/nR/fR/retR
      false, // !REPLACEMENT-bneqi-stack/n-1/fR/retR
      false, // !REPLACEMENT-bneqi-stack/n0/fR/retR
      false, // !REPLACEMENT-bneqi-stack/n1/fR/retR
      false, // !REPLACEMENT-bneqi-stack/n2/fR/retR
      false, // !REPLACEMENT-bneqi-stack/nR/fR/retR
      false, // !REPLACEMENT-bt-stack/fR/retR
      false, // !REPLACEMENT-call/fR/retR
      false, // !REPLACEMENT-exitvm/retR
      false, // !REPLACEMENT-procedure-prolog/retR
      false, // !REPLACEMENT-return-to-undertop/retR
      false // !REPLACEMENT-unreachable/retR
    };

// FIXME: this is not currently accessed, and in fact may be useless.
const bool
structuredvm_specialized_instruction_callers [STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      false, // !INVALID
      false, // !BEGINBASICBLOCK
      false, // !EXITVM
      false, // !DATALOCATIONS
      false, // !NOP
      false, // !UNREACHABLE0
      false, // !UNREACHABLE1
      false, // !PRETENDTOJUMPANYWHERE
      false, // b/fR
      false, // be/%rR/%rR/fR
      false, // be/%rR/n0/fR
      false, // be/%rR/nR/fR
      false, // be/n0/%rR/fR
      false, // be/n0/n0/fR
      false, // be/n0/nR/fR
      false, // be/nR/%rR/fR
      false, // be/nR/n0/fR
      false, // be/nR/nR/fR
      false, // beqi-stack/n-1/fR
      false, // beqi-stack/n0/fR
      false, // beqi-stack/n1/fR
      false, // beqi-stack/n2/fR
      false, // beqi-stack/nR/fR
      false, // bf-stack/fR
      false, // bg/%rR/%rR/fR
      false, // bg/%rR/n0/fR
      false, // bg/%rR/nR/fR
      false, // bg/n0/%rR/fR
      false, // bg/n0/n0/fR
      false, // bg/n0/nR/fR
      false, // bg/nR/%rR/fR
      false, // bg/nR/n0/fR
      false, // bg/nR/nR/fR
      false, // bge/%rR/%rR/fR
      false, // bge/%rR/n0/fR
      false, // bge/%rR/nR/fR
      false, // bge/n0/%rR/fR
      false, // bge/n0/n0/fR
      false, // bge/n0/nR/fR
      false, // bge/nR/%rR/fR
      false, // bge/nR/n0/fR
      false, // bge/nR/nR/fR
      false, // bl/%rR/%rR/fR
      false, // bl/%rR/n0/fR
      false, // bl/%rR/nR/fR
      false, // bl/n0/%rR/fR
      false, // bl/n0/n0/fR
      false, // bl/n0/nR/fR
      false, // bl/nR/%rR/fR
      false, // bl/nR/n0/fR
      false, // bl/nR/nR/fR
      false, // ble/%rR/%rR/fR
      false, // ble/%rR/n0/fR
      false, // ble/%rR/nR/fR
      false, // ble/n0/%rR/fR
      false, // ble/n0/n0/fR
      false, // ble/n0/nR/fR
      false, // ble/nR/%rR/fR
      false, // ble/nR/n0/fR
      false, // ble/nR/nR/fR
      false, // bne/%rR/%rR/fR
      false, // bne/%rR/n0/fR
      false, // bne/%rR/nR/fR
      false, // bne/n0/%rR/fR
      false, // bne/n0/n0/fR
      false, // bne/n0/nR/fR
      false, // bne/nR/%rR/fR
      false, // bne/nR/n0/fR
      false, // bne/nR/nR/fR
      false, // bneqi-stack/n-1/fR
      false, // bneqi-stack/n0/fR
      false, // bneqi-stack/n1/fR
      false, // bneqi-stack/n2/fR
      false, // bneqi-stack/nR/fR
      false, // bt-stack/fR
      true, // call/fR/retR
      false, // copy-to-r-stack/%rR
      false, // different-stack
      false, // divided/%rR/%rR/%rR/retR
      false, // divided/%rR/n2/%rR/retR
      false, // divided/%rR/nR/%rR/retR
      false, // divided/nR/%rR/%rR/retR
      false, // divided/nR/n2/%rR/retR
      false, // divided/nR/nR/%rR/retR
      false, // divided-stack/retR
      false, // drop-stack
      false, // dup-stack
      false, // equal-stack
      false, // equali-stack/n0
      false, // equali-stack/n1
      false, // equali-stack/n2
      false, // equali-stack/nR
      false, // exitvm
      false, // greater-stack
      false, // greaterorequal-stack
      false, // input/%rR/retR
      false, // input-stack/retR
      false, // isnonzero-stack
      false, // less-stack
      false, // lessorequal-stack
      false, // logicaland-stack
      false, // logicalnot-stack
      false, // logicalor-stack
      false, // minus/%rR/%rR/%rR
      false, // minus/%rR/n1/%rR
      false, // minus/%rR/n2/%rR
      false, // minus/%rR/nR/%rR
      false, // minus/nR/%rR/%rR
      false, // minus/nR/n1/%rR
      false, // minus/nR/n2/%rR
      false, // minus/nR/nR/%rR
      false, // minus-stack
      false, // minusi-stack/n1
      false, // minusi-stack/n2
      false, // minusi-stack/nR
      false, // mov/%rR/%rR
      false, // mov/n0/%rR
      false, // mov/n1/%rR
      false, // mov/n-1/%rR
      false, // mov/n2/%rR
      false, // mov/nR/%rR
      false, // plus/%rR/%rR/%rR
      false, // plus/%rR/n1/%rR
      false, // plus/%rR/n2/%rR
      false, // plus/%rR/nR/%rR
      false, // plus/n1/%rR/%rR
      false, // plus/n1/n1/%rR
      false, // plus/n1/n2/%rR
      false, // plus/n1/nR/%rR
      false, // plus/n2/%rR/%rR
      false, // plus/n2/n1/%rR
      false, // plus/n2/n2/%rR
      false, // plus/n2/nR/%rR
      false, // plus/nR/%rR/%rR
      false, // plus/nR/n1/%rR
      false, // plus/nR/n2/%rR
      false, // plus/nR/nR/%rR
      false, // plus-stack
      false, // plusi-stack/n-1
      false, // plusi-stack/n1
      false, // plusi-stack/n2
      false, // plusi-stack/nR
      false, // pop-stack/%rR
      false, // print/%rR/retR
      false, // print/nR/retR
      false, // print-stack/retR
      false, // procedure-prolog
      false, // push-stack/%rR
      false, // push-stack/n0
      false, // push-stack/n1
      false, // push-stack/n-1
      false, // push-stack/n2
      false, // push-stack/nR
      false, // push-stack/lR
      false, // push-unspecified-stack
      false, // remainder/%rR/%rR/%rR/retR
      false, // remainder/%rR/n2/%rR/retR
      false, // remainder/%rR/nR/%rR/retR
      false, // remainder/nR/%rR/%rR/retR
      false, // remainder/nR/n2/%rR/retR
      false, // remainder/nR/nR/%rR/retR
      false, // remainder-stack/retR
      false, // return-to-undertop
      false, // swap-stack
      false, // times/%rR/%rR/%rR
      false, // times/%rR/n2/%rR
      false, // times/%rR/nR/%rR
      false, // times/nR/%rR/%rR
      false, // times/nR/n2/%rR
      false, // times/nR/nR/%rR
      false, // times-stack
      false, // uminus/%rR/%rR
      false, // uminus/nR/%rR
      false, // uminus-stack
      false, // underpop-stack/%rR
      false, // underpush-stack/%rR
      false, // underpush-stack/nR
      false, // unreachable
      false, // !REPLACEMENT-b/fR/retR
      false, // !REPLACEMENT-be/%rR/%rR/fR/retR
      false, // !REPLACEMENT-be/%rR/n0/fR/retR
      false, // !REPLACEMENT-be/%rR/nR/fR/retR
      false, // !REPLACEMENT-be/n0/%rR/fR/retR
      false, // !REPLACEMENT-be/n0/n0/fR/retR
      false, // !REPLACEMENT-be/n0/nR/fR/retR
      false, // !REPLACEMENT-be/nR/%rR/fR/retR
      false, // !REPLACEMENT-be/nR/n0/fR/retR
      false, // !REPLACEMENT-be/nR/nR/fR/retR
      false, // !REPLACEMENT-beqi-stack/n-1/fR/retR
      false, // !REPLACEMENT-beqi-stack/n0/fR/retR
      false, // !REPLACEMENT-beqi-stack/n1/fR/retR
      false, // !REPLACEMENT-beqi-stack/n2/fR/retR
      false, // !REPLACEMENT-beqi-stack/nR/fR/retR
      false, // !REPLACEMENT-bf-stack/fR/retR
      false, // !REPLACEMENT-bg/%rR/%rR/fR/retR
      false, // !REPLACEMENT-bg/%rR/n0/fR/retR
      false, // !REPLACEMENT-bg/%rR/nR/fR/retR
      false, // !REPLACEMENT-bg/n0/%rR/fR/retR
      false, // !REPLACEMENT-bg/n0/n0/fR/retR
      false, // !REPLACEMENT-bg/n0/nR/fR/retR
      false, // !REPLACEMENT-bg/nR/%rR/fR/retR
      false, // !REPLACEMENT-bg/nR/n0/fR/retR
      false, // !REPLACEMENT-bg/nR/nR/fR/retR
      false, // !REPLACEMENT-bge/%rR/%rR/fR/retR
      false, // !REPLACEMENT-bge/%rR/n0/fR/retR
      false, // !REPLACEMENT-bge/%rR/nR/fR/retR
      false, // !REPLACEMENT-bge/n0/%rR/fR/retR
      false, // !REPLACEMENT-bge/n0/n0/fR/retR
      false, // !REPLACEMENT-bge/n0/nR/fR/retR
      false, // !REPLACEMENT-bge/nR/%rR/fR/retR
      false, // !REPLACEMENT-bge/nR/n0/fR/retR
      false, // !REPLACEMENT-bge/nR/nR/fR/retR
      false, // !REPLACEMENT-bl/%rR/%rR/fR/retR
      false, // !REPLACEMENT-bl/%rR/n0/fR/retR
      false, // !REPLACEMENT-bl/%rR/nR/fR/retR
      false, // !REPLACEMENT-bl/n0/%rR/fR/retR
      false, // !REPLACEMENT-bl/n0/n0/fR/retR
      false, // !REPLACEMENT-bl/n0/nR/fR/retR
      false, // !REPLACEMENT-bl/nR/%rR/fR/retR
      false, // !REPLACEMENT-bl/nR/n0/fR/retR
      false, // !REPLACEMENT-bl/nR/nR/fR/retR
      false, // !REPLACEMENT-ble/%rR/%rR/fR/retR
      false, // !REPLACEMENT-ble/%rR/n0/fR/retR
      false, // !REPLACEMENT-ble/%rR/nR/fR/retR
      false, // !REPLACEMENT-ble/n0/%rR/fR/retR
      false, // !REPLACEMENT-ble/n0/n0/fR/retR
      false, // !REPLACEMENT-ble/n0/nR/fR/retR
      false, // !REPLACEMENT-ble/nR/%rR/fR/retR
      false, // !REPLACEMENT-ble/nR/n0/fR/retR
      false, // !REPLACEMENT-ble/nR/nR/fR/retR
      false, // !REPLACEMENT-bne/%rR/%rR/fR/retR
      false, // !REPLACEMENT-bne/%rR/n0/fR/retR
      false, // !REPLACEMENT-bne/%rR/nR/fR/retR
      false, // !REPLACEMENT-bne/n0/%rR/fR/retR
      false, // !REPLACEMENT-bne/n0/n0/fR/retR
      false, // !REPLACEMENT-bne/n0/nR/fR/retR
      false, // !REPLACEMENT-bne/nR/%rR/fR/retR
      false, // !REPLACEMENT-bne/nR/n0/fR/retR
      false, // !REPLACEMENT-bne/nR/nR/fR/retR
      false, // !REPLACEMENT-bneqi-stack/n-1/fR/retR
      false, // !REPLACEMENT-bneqi-stack/n0/fR/retR
      false, // !REPLACEMENT-bneqi-stack/n1/fR/retR
      false, // !REPLACEMENT-bneqi-stack/n2/fR/retR
      false, // !REPLACEMENT-bneqi-stack/nR/fR/retR
      false, // !REPLACEMENT-bt-stack/fR/retR
      true, // !REPLACEMENT-call/fR/retR
      false, // !REPLACEMENT-exitvm/retR
      false, // !REPLACEMENT-procedure-prolog/retR
      false, // !REPLACEMENT-return-to-undertop/retR
      false // !REPLACEMENT-unreachable/retR
    };

// FIXME: this is not currently accessed, and in fact may be useless.
const bool
structuredvm_specialized_instruction_callees [STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      false, // !INVALID
      false, // !BEGINBASICBLOCK
      false, // !EXITVM
      false, // !DATALOCATIONS
      false, // !NOP
      false, // !UNREACHABLE0
      false, // !UNREACHABLE1
      false, // !PRETENDTOJUMPANYWHERE
      false, // b/fR
      false, // be/%rR/%rR/fR
      false, // be/%rR/n0/fR
      false, // be/%rR/nR/fR
      false, // be/n0/%rR/fR
      false, // be/n0/n0/fR
      false, // be/n0/nR/fR
      false, // be/nR/%rR/fR
      false, // be/nR/n0/fR
      false, // be/nR/nR/fR
      false, // beqi-stack/n-1/fR
      false, // beqi-stack/n0/fR
      false, // beqi-stack/n1/fR
      false, // beqi-stack/n2/fR
      false, // beqi-stack/nR/fR
      false, // bf-stack/fR
      false, // bg/%rR/%rR/fR
      false, // bg/%rR/n0/fR
      false, // bg/%rR/nR/fR
      false, // bg/n0/%rR/fR
      false, // bg/n0/n0/fR
      false, // bg/n0/nR/fR
      false, // bg/nR/%rR/fR
      false, // bg/nR/n0/fR
      false, // bg/nR/nR/fR
      false, // bge/%rR/%rR/fR
      false, // bge/%rR/n0/fR
      false, // bge/%rR/nR/fR
      false, // bge/n0/%rR/fR
      false, // bge/n0/n0/fR
      false, // bge/n0/nR/fR
      false, // bge/nR/%rR/fR
      false, // bge/nR/n0/fR
      false, // bge/nR/nR/fR
      false, // bl/%rR/%rR/fR
      false, // bl/%rR/n0/fR
      false, // bl/%rR/nR/fR
      false, // bl/n0/%rR/fR
      false, // bl/n0/n0/fR
      false, // bl/n0/nR/fR
      false, // bl/nR/%rR/fR
      false, // bl/nR/n0/fR
      false, // bl/nR/nR/fR
      false, // ble/%rR/%rR/fR
      false, // ble/%rR/n0/fR
      false, // ble/%rR/nR/fR
      false, // ble/n0/%rR/fR
      false, // ble/n0/n0/fR
      false, // ble/n0/nR/fR
      false, // ble/nR/%rR/fR
      false, // ble/nR/n0/fR
      false, // ble/nR/nR/fR
      false, // bne/%rR/%rR/fR
      false, // bne/%rR/n0/fR
      false, // bne/%rR/nR/fR
      false, // bne/n0/%rR/fR
      false, // bne/n0/n0/fR
      false, // bne/n0/nR/fR
      false, // bne/nR/%rR/fR
      false, // bne/nR/n0/fR
      false, // bne/nR/nR/fR
      false, // bneqi-stack/n-1/fR
      false, // bneqi-stack/n0/fR
      false, // bneqi-stack/n1/fR
      false, // bneqi-stack/n2/fR
      false, // bneqi-stack/nR/fR
      false, // bt-stack/fR
      false, // call/fR/retR
      false, // copy-to-r-stack/%rR
      false, // different-stack
      false, // divided/%rR/%rR/%rR/retR
      false, // divided/%rR/n2/%rR/retR
      false, // divided/%rR/nR/%rR/retR
      false, // divided/nR/%rR/%rR/retR
      false, // divided/nR/n2/%rR/retR
      false, // divided/nR/nR/%rR/retR
      false, // divided-stack/retR
      false, // drop-stack
      false, // dup-stack
      false, // equal-stack
      false, // equali-stack/n0
      false, // equali-stack/n1
      false, // equali-stack/n2
      false, // equali-stack/nR
      false, // exitvm
      false, // greater-stack
      false, // greaterorequal-stack
      false, // input/%rR/retR
      false, // input-stack/retR
      false, // isnonzero-stack
      false, // less-stack
      false, // lessorequal-stack
      false, // logicaland-stack
      false, // logicalnot-stack
      false, // logicalor-stack
      false, // minus/%rR/%rR/%rR
      false, // minus/%rR/n1/%rR
      false, // minus/%rR/n2/%rR
      false, // minus/%rR/nR/%rR
      false, // minus/nR/%rR/%rR
      false, // minus/nR/n1/%rR
      false, // minus/nR/n2/%rR
      false, // minus/nR/nR/%rR
      false, // minus-stack
      false, // minusi-stack/n1
      false, // minusi-stack/n2
      false, // minusi-stack/nR
      false, // mov/%rR/%rR
      false, // mov/n0/%rR
      false, // mov/n1/%rR
      false, // mov/n-1/%rR
      false, // mov/n2/%rR
      false, // mov/nR/%rR
      false, // plus/%rR/%rR/%rR
      false, // plus/%rR/n1/%rR
      false, // plus/%rR/n2/%rR
      false, // plus/%rR/nR/%rR
      false, // plus/n1/%rR/%rR
      false, // plus/n1/n1/%rR
      false, // plus/n1/n2/%rR
      false, // plus/n1/nR/%rR
      false, // plus/n2/%rR/%rR
      false, // plus/n2/n1/%rR
      false, // plus/n2/n2/%rR
      false, // plus/n2/nR/%rR
      false, // plus/nR/%rR/%rR
      false, // plus/nR/n1/%rR
      false, // plus/nR/n2/%rR
      false, // plus/nR/nR/%rR
      false, // plus-stack
      false, // plusi-stack/n-1
      false, // plusi-stack/n1
      false, // plusi-stack/n2
      false, // plusi-stack/nR
      false, // pop-stack/%rR
      false, // print/%rR/retR
      false, // print/nR/retR
      false, // print-stack/retR
      true, // procedure-prolog
      false, // push-stack/%rR
      false, // push-stack/n0
      false, // push-stack/n1
      false, // push-stack/n-1
      false, // push-stack/n2
      false, // push-stack/nR
      false, // push-stack/lR
      false, // push-unspecified-stack
      false, // remainder/%rR/%rR/%rR/retR
      false, // remainder/%rR/n2/%rR/retR
      false, // remainder/%rR/nR/%rR/retR
      false, // remainder/nR/%rR/%rR/retR
      false, // remainder/nR/n2/%rR/retR
      false, // remainder/nR/nR/%rR/retR
      false, // remainder-stack/retR
      false, // return-to-undertop
      false, // swap-stack
      false, // times/%rR/%rR/%rR
      false, // times/%rR/n2/%rR
      false, // times/%rR/nR/%rR
      false, // times/nR/%rR/%rR
      false, // times/nR/n2/%rR
      false, // times/nR/nR/%rR
      false, // times-stack
      false, // uminus/%rR/%rR
      false, // uminus/nR/%rR
      false, // uminus-stack
      false, // underpop-stack/%rR
      false, // underpush-stack/%rR
      false, // underpush-stack/nR
      false, // unreachable
      false, // !REPLACEMENT-b/fR/retR
      false, // !REPLACEMENT-be/%rR/%rR/fR/retR
      false, // !REPLACEMENT-be/%rR/n0/fR/retR
      false, // !REPLACEMENT-be/%rR/nR/fR/retR
      false, // !REPLACEMENT-be/n0/%rR/fR/retR
      false, // !REPLACEMENT-be/n0/n0/fR/retR
      false, // !REPLACEMENT-be/n0/nR/fR/retR
      false, // !REPLACEMENT-be/nR/%rR/fR/retR
      false, // !REPLACEMENT-be/nR/n0/fR/retR
      false, // !REPLACEMENT-be/nR/nR/fR/retR
      false, // !REPLACEMENT-beqi-stack/n-1/fR/retR
      false, // !REPLACEMENT-beqi-stack/n0/fR/retR
      false, // !REPLACEMENT-beqi-stack/n1/fR/retR
      false, // !REPLACEMENT-beqi-stack/n2/fR/retR
      false, // !REPLACEMENT-beqi-stack/nR/fR/retR
      false, // !REPLACEMENT-bf-stack/fR/retR
      false, // !REPLACEMENT-bg/%rR/%rR/fR/retR
      false, // !REPLACEMENT-bg/%rR/n0/fR/retR
      false, // !REPLACEMENT-bg/%rR/nR/fR/retR
      false, // !REPLACEMENT-bg/n0/%rR/fR/retR
      false, // !REPLACEMENT-bg/n0/n0/fR/retR
      false, // !REPLACEMENT-bg/n0/nR/fR/retR
      false, // !REPLACEMENT-bg/nR/%rR/fR/retR
      false, // !REPLACEMENT-bg/nR/n0/fR/retR
      false, // !REPLACEMENT-bg/nR/nR/fR/retR
      false, // !REPLACEMENT-bge/%rR/%rR/fR/retR
      false, // !REPLACEMENT-bge/%rR/n0/fR/retR
      false, // !REPLACEMENT-bge/%rR/nR/fR/retR
      false, // !REPLACEMENT-bge/n0/%rR/fR/retR
      false, // !REPLACEMENT-bge/n0/n0/fR/retR
      false, // !REPLACEMENT-bge/n0/nR/fR/retR
      false, // !REPLACEMENT-bge/nR/%rR/fR/retR
      false, // !REPLACEMENT-bge/nR/n0/fR/retR
      false, // !REPLACEMENT-bge/nR/nR/fR/retR
      false, // !REPLACEMENT-bl/%rR/%rR/fR/retR
      false, // !REPLACEMENT-bl/%rR/n0/fR/retR
      false, // !REPLACEMENT-bl/%rR/nR/fR/retR
      false, // !REPLACEMENT-bl/n0/%rR/fR/retR
      false, // !REPLACEMENT-bl/n0/n0/fR/retR
      false, // !REPLACEMENT-bl/n0/nR/fR/retR
      false, // !REPLACEMENT-bl/nR/%rR/fR/retR
      false, // !REPLACEMENT-bl/nR/n0/fR/retR
      false, // !REPLACEMENT-bl/nR/nR/fR/retR
      false, // !REPLACEMENT-ble/%rR/%rR/fR/retR
      false, // !REPLACEMENT-ble/%rR/n0/fR/retR
      false, // !REPLACEMENT-ble/%rR/nR/fR/retR
      false, // !REPLACEMENT-ble/n0/%rR/fR/retR
      false, // !REPLACEMENT-ble/n0/n0/fR/retR
      false, // !REPLACEMENT-ble/n0/nR/fR/retR
      false, // !REPLACEMENT-ble/nR/%rR/fR/retR
      false, // !REPLACEMENT-ble/nR/n0/fR/retR
      false, // !REPLACEMENT-ble/nR/nR/fR/retR
      false, // !REPLACEMENT-bne/%rR/%rR/fR/retR
      false, // !REPLACEMENT-bne/%rR/n0/fR/retR
      false, // !REPLACEMENT-bne/%rR/nR/fR/retR
      false, // !REPLACEMENT-bne/n0/%rR/fR/retR
      false, // !REPLACEMENT-bne/n0/n0/fR/retR
      false, // !REPLACEMENT-bne/n0/nR/fR/retR
      false, // !REPLACEMENT-bne/nR/%rR/fR/retR
      false, // !REPLACEMENT-bne/nR/n0/fR/retR
      false, // !REPLACEMENT-bne/nR/nR/fR/retR
      false, // !REPLACEMENT-bneqi-stack/n-1/fR/retR
      false, // !REPLACEMENT-bneqi-stack/n0/fR/retR
      false, // !REPLACEMENT-bneqi-stack/n1/fR/retR
      false, // !REPLACEMENT-bneqi-stack/n2/fR/retR
      false, // !REPLACEMENT-bneqi-stack/nR/fR/retR
      false, // !REPLACEMENT-bt-stack/fR/retR
      false, // !REPLACEMENT-call/fR/retR
      false, // !REPLACEMENT-exitvm/retR
      true, // !REPLACEMENT-procedure-prolog/retR
      false, // !REPLACEMENT-return-to-undertop/retR
      false // !REPLACEMENT-unreachable/retR
    };

/* An array whose indices are specialised instruction opcodes, and
   whose elements are the corresponding unspecialised instructions
   opcodes -- or -1 when there is no mapping mapping having */
const int
structuredvm_specialized_instruction_to_unspecialized_instruction
   [STRUCTUREDVM_SPECIALIZED_INSTRUCTION_NO]
  = {
    -1, /* !INVALID */
    -1, /* !BEGINBASICBLOCK */
    -1, /* !EXITVM */
    -1, /* !DATALOCATIONS */
    -1, /* !NOP */
    -1, /* !UNREACHABLE0 */
    -1, /* !UNREACHABLE1 */
    -1, /* !PRETENDTOJUMPANYWHERE */
    structuredvm_meta_instruction_id_b, /* b/fR */
    structuredvm_meta_instruction_id_be, /* be/%rR/%rR/fR */
    structuredvm_meta_instruction_id_be, /* be/%rR/n0/fR */
    structuredvm_meta_instruction_id_be, /* be/%rR/nR/fR */
    structuredvm_meta_instruction_id_be, /* be/n0/%rR/fR */
    structuredvm_meta_instruction_id_be, /* be/n0/n0/fR */
    structuredvm_meta_instruction_id_be, /* be/n0/nR/fR */
    structuredvm_meta_instruction_id_be, /* be/nR/%rR/fR */
    structuredvm_meta_instruction_id_be, /* be/nR/n0/fR */
    structuredvm_meta_instruction_id_be, /* be/nR/nR/fR */
    structuredvm_meta_instruction_id_beqi_mstack, /* beqi-stack/n-1/fR */
    structuredvm_meta_instruction_id_beqi_mstack, /* beqi-stack/n0/fR */
    structuredvm_meta_instruction_id_beqi_mstack, /* beqi-stack/n1/fR */
    structuredvm_meta_instruction_id_beqi_mstack, /* beqi-stack/n2/fR */
    structuredvm_meta_instruction_id_beqi_mstack, /* beqi-stack/nR/fR */
    structuredvm_meta_instruction_id_bf_mstack, /* bf-stack/fR */
    structuredvm_meta_instruction_id_bg, /* bg/%rR/%rR/fR */
    structuredvm_meta_instruction_id_bg, /* bg/%rR/n0/fR */
    structuredvm_meta_instruction_id_bg, /* bg/%rR/nR/fR */
    structuredvm_meta_instruction_id_bg, /* bg/n0/%rR/fR */
    structuredvm_meta_instruction_id_bg, /* bg/n0/n0/fR */
    structuredvm_meta_instruction_id_bg, /* bg/n0/nR/fR */
    structuredvm_meta_instruction_id_bg, /* bg/nR/%rR/fR */
    structuredvm_meta_instruction_id_bg, /* bg/nR/n0/fR */
    structuredvm_meta_instruction_id_bg, /* bg/nR/nR/fR */
    structuredvm_meta_instruction_id_bge, /* bge/%rR/%rR/fR */
    structuredvm_meta_instruction_id_bge, /* bge/%rR/n0/fR */
    structuredvm_meta_instruction_id_bge, /* bge/%rR/nR/fR */
    structuredvm_meta_instruction_id_bge, /* bge/n0/%rR/fR */
    structuredvm_meta_instruction_id_bge, /* bge/n0/n0/fR */
    structuredvm_meta_instruction_id_bge, /* bge/n0/nR/fR */
    structuredvm_meta_instruction_id_bge, /* bge/nR/%rR/fR */
    structuredvm_meta_instruction_id_bge, /* bge/nR/n0/fR */
    structuredvm_meta_instruction_id_bge, /* bge/nR/nR/fR */
    structuredvm_meta_instruction_id_bl, /* bl/%rR/%rR/fR */
    structuredvm_meta_instruction_id_bl, /* bl/%rR/n0/fR */
    structuredvm_meta_instruction_id_bl, /* bl/%rR/nR/fR */
    structuredvm_meta_instruction_id_bl, /* bl/n0/%rR/fR */
    structuredvm_meta_instruction_id_bl, /* bl/n0/n0/fR */
    structuredvm_meta_instruction_id_bl, /* bl/n0/nR/fR */
    structuredvm_meta_instruction_id_bl, /* bl/nR/%rR/fR */
    structuredvm_meta_instruction_id_bl, /* bl/nR/n0/fR */
    structuredvm_meta_instruction_id_bl, /* bl/nR/nR/fR */
    structuredvm_meta_instruction_id_ble, /* ble/%rR/%rR/fR */
    structuredvm_meta_instruction_id_ble, /* ble/%rR/n0/fR */
    structuredvm_meta_instruction_id_ble, /* ble/%rR/nR/fR */
    structuredvm_meta_instruction_id_ble, /* ble/n0/%rR/fR */
    structuredvm_meta_instruction_id_ble, /* ble/n0/n0/fR */
    structuredvm_meta_instruction_id_ble, /* ble/n0/nR/fR */
    structuredvm_meta_instruction_id_ble, /* ble/nR/%rR/fR */
    structuredvm_meta_instruction_id_ble, /* ble/nR/n0/fR */
    structuredvm_meta_instruction_id_ble, /* ble/nR/nR/fR */
    structuredvm_meta_instruction_id_bne, /* bne/%rR/%rR/fR */
    structuredvm_meta_instruction_id_bne, /* bne/%rR/n0/fR */
    structuredvm_meta_instruction_id_bne, /* bne/%rR/nR/fR */
    structuredvm_meta_instruction_id_bne, /* bne/n0/%rR/fR */
    structuredvm_meta_instruction_id_bne, /* bne/n0/n0/fR */
    structuredvm_meta_instruction_id_bne, /* bne/n0/nR/fR */
    structuredvm_meta_instruction_id_bne, /* bne/nR/%rR/fR */
    structuredvm_meta_instruction_id_bne, /* bne/nR/n0/fR */
    structuredvm_meta_instruction_id_bne, /* bne/nR/nR/fR */
    structuredvm_meta_instruction_id_bneqi_mstack, /* bneqi-stack/n-1/fR */
    structuredvm_meta_instruction_id_bneqi_mstack, /* bneqi-stack/n0/fR */
    structuredvm_meta_instruction_id_bneqi_mstack, /* bneqi-stack/n1/fR */
    structuredvm_meta_instruction_id_bneqi_mstack, /* bneqi-stack/n2/fR */
    structuredvm_meta_instruction_id_bneqi_mstack, /* bneqi-stack/nR/fR */
    structuredvm_meta_instruction_id_bt_mstack, /* bt-stack/fR */
    structuredvm_meta_instruction_id_call, /* call/fR/retR */
    structuredvm_meta_instruction_id_copy_mto_mr_mstack, /* copy-to-r-stack/%rR */
    structuredvm_meta_instruction_id_different_mstack, /* different-stack */
    structuredvm_meta_instruction_id_divided, /* divided/%rR/%rR/%rR/retR */
    structuredvm_meta_instruction_id_divided, /* divided/%rR/n2/%rR/retR */
    structuredvm_meta_instruction_id_divided, /* divided/%rR/nR/%rR/retR */
    structuredvm_meta_instruction_id_divided, /* divided/nR/%rR/%rR/retR */
    structuredvm_meta_instruction_id_divided, /* divided/nR/n2/%rR/retR */
    structuredvm_meta_instruction_id_divided, /* divided/nR/nR/%rR/retR */
    structuredvm_meta_instruction_id_divided_mstack, /* divided-stack/retR */
    structuredvm_meta_instruction_id_drop_mstack, /* drop-stack */
    structuredvm_meta_instruction_id_dup_mstack, /* dup-stack */
    structuredvm_meta_instruction_id_equal_mstack, /* equal-stack */
    structuredvm_meta_instruction_id_equali_mstack, /* equali-stack/n0 */
    structuredvm_meta_instruction_id_equali_mstack, /* equali-stack/n1 */
    structuredvm_meta_instruction_id_equali_mstack, /* equali-stack/n2 */
    structuredvm_meta_instruction_id_equali_mstack, /* equali-stack/nR */
    structuredvm_meta_instruction_id_exitvm, /* exitvm */
    structuredvm_meta_instruction_id_greater_mstack, /* greater-stack */
    structuredvm_meta_instruction_id_greaterorequal_mstack, /* greaterorequal-stack */
    structuredvm_meta_instruction_id_input, /* input/%rR/retR */
    structuredvm_meta_instruction_id_input_mstack, /* input-stack/retR */
    structuredvm_meta_instruction_id_isnonzero_mstack, /* isnonzero-stack */
    structuredvm_meta_instruction_id_less_mstack, /* less-stack */
    structuredvm_meta_instruction_id_lessorequal_mstack, /* lessorequal-stack */
    structuredvm_meta_instruction_id_logicaland_mstack, /* logicaland-stack */
    structuredvm_meta_instruction_id_logicalnot_mstack, /* logicalnot-stack */
    structuredvm_meta_instruction_id_logicalor_mstack, /* logicalor-stack */
    structuredvm_meta_instruction_id_minus, /* minus/%rR/%rR/%rR */
    structuredvm_meta_instruction_id_minus, /* minus/%rR/n1/%rR */
    structuredvm_meta_instruction_id_minus, /* minus/%rR/n2/%rR */
    structuredvm_meta_instruction_id_minus, /* minus/%rR/nR/%rR */
    structuredvm_meta_instruction_id_minus, /* minus/nR/%rR/%rR */
    structuredvm_meta_instruction_id_minus, /* minus/nR/n1/%rR */
    structuredvm_meta_instruction_id_minus, /* minus/nR/n2/%rR */
    structuredvm_meta_instruction_id_minus, /* minus/nR/nR/%rR */
    structuredvm_meta_instruction_id_minus_mstack, /* minus-stack */
    structuredvm_meta_instruction_id_minusi_mstack, /* minusi-stack/n1 */
    structuredvm_meta_instruction_id_minusi_mstack, /* minusi-stack/n2 */
    structuredvm_meta_instruction_id_minusi_mstack, /* minusi-stack/nR */
    structuredvm_meta_instruction_id_mov, /* mov/%rR/%rR */
    structuredvm_meta_instruction_id_mov, /* mov/n0/%rR */
    structuredvm_meta_instruction_id_mov, /* mov/n1/%rR */
    structuredvm_meta_instruction_id_mov, /* mov/n-1/%rR */
    structuredvm_meta_instruction_id_mov, /* mov/n2/%rR */
    structuredvm_meta_instruction_id_mov, /* mov/nR/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/%rR/%rR/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/%rR/n1/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/%rR/n2/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/%rR/nR/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/n1/%rR/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/n1/n1/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/n1/n2/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/n1/nR/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/n2/%rR/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/n2/n1/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/n2/n2/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/n2/nR/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/nR/%rR/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/nR/n1/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/nR/n2/%rR */
    structuredvm_meta_instruction_id_plus, /* plus/nR/nR/%rR */
    structuredvm_meta_instruction_id_plus_mstack, /* plus-stack */
    structuredvm_meta_instruction_id_plusi_mstack, /* plusi-stack/n-1 */
    structuredvm_meta_instruction_id_plusi_mstack, /* plusi-stack/n1 */
    structuredvm_meta_instruction_id_plusi_mstack, /* plusi-stack/n2 */
    structuredvm_meta_instruction_id_plusi_mstack, /* plusi-stack/nR */
    structuredvm_meta_instruction_id_pop_mstack, /* pop-stack/%rR */
    structuredvm_meta_instruction_id_print, /* print/%rR/retR */
    structuredvm_meta_instruction_id_print, /* print/nR/retR */
    structuredvm_meta_instruction_id_print_mstack, /* print-stack/retR */
    structuredvm_meta_instruction_id_procedure_mprolog, /* procedure-prolog */
    structuredvm_meta_instruction_id_push_mstack, /* push-stack/%rR */
    structuredvm_meta_instruction_id_push_mstack, /* push-stack/n0 */
    structuredvm_meta_instruction_id_push_mstack, /* push-stack/n1 */
    structuredvm_meta_instruction_id_push_mstack, /* push-stack/n-1 */
    structuredvm_meta_instruction_id_push_mstack, /* push-stack/n2 */
    structuredvm_meta_instruction_id_push_mstack, /* push-stack/nR */
    structuredvm_meta_instruction_id_push_mstack, /* push-stack/lR */
    structuredvm_meta_instruction_id_push_munspecified_mstack, /* push-unspecified-stack */
    structuredvm_meta_instruction_id_remainder, /* remainder/%rR/%rR/%rR/retR */
    structuredvm_meta_instruction_id_remainder, /* remainder/%rR/n2/%rR/retR */
    structuredvm_meta_instruction_id_remainder, /* remainder/%rR/nR/%rR/retR */
    structuredvm_meta_instruction_id_remainder, /* remainder/nR/%rR/%rR/retR */
    structuredvm_meta_instruction_id_remainder, /* remainder/nR/n2/%rR/retR */
    structuredvm_meta_instruction_id_remainder, /* remainder/nR/nR/%rR/retR */
    structuredvm_meta_instruction_id_remainder_mstack, /* remainder-stack/retR */
    structuredvm_meta_instruction_id_return_mto_mundertop, /* return-to-undertop */
    structuredvm_meta_instruction_id_swap_mstack, /* swap-stack */
    structuredvm_meta_instruction_id_times, /* times/%rR/%rR/%rR */
    structuredvm_meta_instruction_id_times, /* times/%rR/n2/%rR */
    structuredvm_meta_instruction_id_times, /* times/%rR/nR/%rR */
    structuredvm_meta_instruction_id_times, /* times/nR/%rR/%rR */
    structuredvm_meta_instruction_id_times, /* times/nR/n2/%rR */
    structuredvm_meta_instruction_id_times, /* times/nR/nR/%rR */
    structuredvm_meta_instruction_id_times_mstack, /* times-stack */
    structuredvm_meta_instruction_id_uminus, /* uminus/%rR/%rR */
    structuredvm_meta_instruction_id_uminus, /* uminus/nR/%rR */
    structuredvm_meta_instruction_id_uminus_mstack, /* uminus-stack */
    structuredvm_meta_instruction_id_underpop_mstack, /* underpop-stack/%rR */
    structuredvm_meta_instruction_id_underpush_mstack, /* underpush-stack/%rR */
    structuredvm_meta_instruction_id_underpush_mstack, /* underpush-stack/nR */
    structuredvm_meta_instruction_id_unreachable, /* unreachable */
    structuredvm_meta_instruction_id_b, /* !REPLACEMENT-b/fR/retR */
    structuredvm_meta_instruction_id_be, /* !REPLACEMENT-be/%rR/%rR/fR/retR */
    structuredvm_meta_instruction_id_be, /* !REPLACEMENT-be/%rR/n0/fR/retR */
    structuredvm_meta_instruction_id_be, /* !REPLACEMENT-be/%rR/nR/fR/retR */
    structuredvm_meta_instruction_id_be, /* !REPLACEMENT-be/n0/%rR/fR/retR */
    structuredvm_meta_instruction_id_be, /* !REPLACEMENT-be/n0/n0/fR/retR */
    structuredvm_meta_instruction_id_be, /* !REPLACEMENT-be/n0/nR/fR/retR */
    structuredvm_meta_instruction_id_be, /* !REPLACEMENT-be/nR/%rR/fR/retR */
    structuredvm_meta_instruction_id_be, /* !REPLACEMENT-be/nR/n0/fR/retR */
    structuredvm_meta_instruction_id_be, /* !REPLACEMENT-be/nR/nR/fR/retR */
    structuredvm_meta_instruction_id_beqi_mstack, /* !REPLACEMENT-beqi-stack/n-1/fR/retR */
    structuredvm_meta_instruction_id_beqi_mstack, /* !REPLACEMENT-beqi-stack/n0/fR/retR */
    structuredvm_meta_instruction_id_beqi_mstack, /* !REPLACEMENT-beqi-stack/n1/fR/retR */
    structuredvm_meta_instruction_id_beqi_mstack, /* !REPLACEMENT-beqi-stack/n2/fR/retR */
    structuredvm_meta_instruction_id_beqi_mstack, /* !REPLACEMENT-beqi-stack/nR/fR/retR */
    structuredvm_meta_instruction_id_bf_mstack, /* !REPLACEMENT-bf-stack/fR/retR */
    structuredvm_meta_instruction_id_bg, /* !REPLACEMENT-bg/%rR/%rR/fR/retR */
    structuredvm_meta_instruction_id_bg, /* !REPLACEMENT-bg/%rR/n0/fR/retR */
    structuredvm_meta_instruction_id_bg, /* !REPLACEMENT-bg/%rR/nR/fR/retR */
    structuredvm_meta_instruction_id_bg, /* !REPLACEMENT-bg/n0/%rR/fR/retR */
    structuredvm_meta_instruction_id_bg, /* !REPLACEMENT-bg/n0/n0/fR/retR */
    structuredvm_meta_instruction_id_bg, /* !REPLACEMENT-bg/n0/nR/fR/retR */
    structuredvm_meta_instruction_id_bg, /* !REPLACEMENT-bg/nR/%rR/fR/retR */
    structuredvm_meta_instruction_id_bg, /* !REPLACEMENT-bg/nR/n0/fR/retR */
    structuredvm_meta_instruction_id_bg, /* !REPLACEMENT-bg/nR/nR/fR/retR */
    structuredvm_meta_instruction_id_bge, /* !REPLACEMENT-bge/%rR/%rR/fR/retR */
    structuredvm_meta_instruction_id_bge, /* !REPLACEMENT-bge/%rR/n0/fR/retR */
    structuredvm_meta_instruction_id_bge, /* !REPLACEMENT-bge/%rR/nR/fR/retR */
    structuredvm_meta_instruction_id_bge, /* !REPLACEMENT-bge/n0/%rR/fR/retR */
    structuredvm_meta_instruction_id_bge, /* !REPLACEMENT-bge/n0/n0/fR/retR */
    structuredvm_meta_instruction_id_bge, /* !REPLACEMENT-bge/n0/nR/fR/retR */
    structuredvm_meta_instruction_id_bge, /* !REPLACEMENT-bge/nR/%rR/fR/retR */
    structuredvm_meta_instruction_id_bge, /* !REPLACEMENT-bge/nR/n0/fR/retR */
    structuredvm_meta_instruction_id_bge, /* !REPLACEMENT-bge/nR/nR/fR/retR */
    structuredvm_meta_instruction_id_bl, /* !REPLACEMENT-bl/%rR/%rR/fR/retR */
    structuredvm_meta_instruction_id_bl, /* !REPLACEMENT-bl/%rR/n0/fR/retR */
    structuredvm_meta_instruction_id_bl, /* !REPLACEMENT-bl/%rR/nR/fR/retR */
    structuredvm_meta_instruction_id_bl, /* !REPLACEMENT-bl/n0/%rR/fR/retR */
    structuredvm_meta_instruction_id_bl, /* !REPLACEMENT-bl/n0/n0/fR/retR */
    structuredvm_meta_instruction_id_bl, /* !REPLACEMENT-bl/n0/nR/fR/retR */
    structuredvm_meta_instruction_id_bl, /* !REPLACEMENT-bl/nR/%rR/fR/retR */
    structuredvm_meta_instruction_id_bl, /* !REPLACEMENT-bl/nR/n0/fR/retR */
    structuredvm_meta_instruction_id_bl, /* !REPLACEMENT-bl/nR/nR/fR/retR */
    structuredvm_meta_instruction_id_ble, /* !REPLACEMENT-ble/%rR/%rR/fR/retR */
    structuredvm_meta_instruction_id_ble, /* !REPLACEMENT-ble/%rR/n0/fR/retR */
    structuredvm_meta_instruction_id_ble, /* !REPLACEMENT-ble/%rR/nR/fR/retR */
    structuredvm_meta_instruction_id_ble, /* !REPLACEMENT-ble/n0/%rR/fR/retR */
    structuredvm_meta_instruction_id_ble, /* !REPLACEMENT-ble/n0/n0/fR/retR */
    structuredvm_meta_instruction_id_ble, /* !REPLACEMENT-ble/n0/nR/fR/retR */
    structuredvm_meta_instruction_id_ble, /* !REPLACEMENT-ble/nR/%rR/fR/retR */
    structuredvm_meta_instruction_id_ble, /* !REPLACEMENT-ble/nR/n0/fR/retR */
    structuredvm_meta_instruction_id_ble, /* !REPLACEMENT-ble/nR/nR/fR/retR */
    structuredvm_meta_instruction_id_bne, /* !REPLACEMENT-bne/%rR/%rR/fR/retR */
    structuredvm_meta_instruction_id_bne, /* !REPLACEMENT-bne/%rR/n0/fR/retR */
    structuredvm_meta_instruction_id_bne, /* !REPLACEMENT-bne/%rR/nR/fR/retR */
    structuredvm_meta_instruction_id_bne, /* !REPLACEMENT-bne/n0/%rR/fR/retR */
    structuredvm_meta_instruction_id_bne, /* !REPLACEMENT-bne/n0/n0/fR/retR */
    structuredvm_meta_instruction_id_bne, /* !REPLACEMENT-bne/n0/nR/fR/retR */
    structuredvm_meta_instruction_id_bne, /* !REPLACEMENT-bne/nR/%rR/fR/retR */
    structuredvm_meta_instruction_id_bne, /* !REPLACEMENT-bne/nR/n0/fR/retR */
    structuredvm_meta_instruction_id_bne, /* !REPLACEMENT-bne/nR/nR/fR/retR */
    structuredvm_meta_instruction_id_bneqi_mstack, /* !REPLACEMENT-bneqi-stack/n-1/fR/retR */
    structuredvm_meta_instruction_id_bneqi_mstack, /* !REPLACEMENT-bneqi-stack/n0/fR/retR */
    structuredvm_meta_instruction_id_bneqi_mstack, /* !REPLACEMENT-bneqi-stack/n1/fR/retR */
    structuredvm_meta_instruction_id_bneqi_mstack, /* !REPLACEMENT-bneqi-stack/n2/fR/retR */
    structuredvm_meta_instruction_id_bneqi_mstack, /* !REPLACEMENT-bneqi-stack/nR/fR/retR */
    structuredvm_meta_instruction_id_bt_mstack, /* !REPLACEMENT-bt-stack/fR/retR */
    structuredvm_meta_instruction_id_call, /* !REPLACEMENT-call/fR/retR */
    structuredvm_meta_instruction_id_exitvm, /* !REPLACEMENT-exitvm/retR */
    structuredvm_meta_instruction_id_procedure_mprolog, /* !REPLACEMENT-procedure-prolog/retR */
    structuredvm_meta_instruction_id_return_mto_mundertop, /* !REPLACEMENT-return-to-undertop/retR */
    structuredvm_meta_instruction_id_unreachable /* !REPLACEMENT-unreachable/retR */
    };

#ifdef JITTER_HAVE_DEFECT_REPLACEMENT
/* Worst-case replacement table. */
const jitter_uint
structuredvm_worst_case_replacement_table [] =
  {
    structuredvm_specialized_instruction_opcode__eINVALID, /* !INVALID is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eBEGINBASICBLOCK, /* !BEGINBASICBLOCK is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eEXITVM, /* !EXITVM is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eDATALOCATIONS, /* !DATALOCATIONS is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eNOP, /* !NOP is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eUNREACHABLE0, /* !UNREACHABLE0 is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eUNREACHABLE1, /* !UNREACHABLE1 is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__ePRETENDTOJUMPANYWHERE, /* !PRETENDTOJUMPANYWHERE is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mb__fR__retR, /* b/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-b/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe___rrR___rrR__fR__retR, /* be/%rR/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-be/%rR/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe___rrR__n0__fR__retR, /* be/%rR/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-be/%rR/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe___rrR__nR__fR__retR, /* be/%rR/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-be/%rR/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__n0___rrR__fR__retR, /* be/n0/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-be/n0/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__n0__n0__fR__retR, /* be/n0/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-be/n0/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__n0__nR__fR__retR, /* be/n0/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-be/n0/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__nR___rrR__fR__retR, /* be/nR/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-be/nR/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__nR__n0__fR__retR, /* be/nR/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-be/nR/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__nR__nR__fR__retR, /* be/nR/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-be/nR/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbeqi_mstack__n_m1__fR__retR, /* beqi-stack/n-1/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-beqi-stack/n-1/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbeqi_mstack__n0__fR__retR, /* beqi-stack/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-beqi-stack/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbeqi_mstack__n1__fR__retR, /* beqi-stack/n1/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-beqi-stack/n1/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbeqi_mstack__n2__fR__retR, /* beqi-stack/n2/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-beqi-stack/n2/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbeqi_mstack__nR__fR__retR, /* beqi-stack/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-beqi-stack/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbf_mstack__fR__retR, /* bf-stack/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bf-stack/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg___rrR___rrR__fR__retR, /* bg/%rR/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bg/%rR/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg___rrR__n0__fR__retR, /* bg/%rR/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bg/%rR/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg___rrR__nR__fR__retR, /* bg/%rR/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bg/%rR/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__n0___rrR__fR__retR, /* bg/n0/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bg/n0/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__n0__n0__fR__retR, /* bg/n0/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bg/n0/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__n0__nR__fR__retR, /* bg/n0/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bg/n0/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__nR___rrR__fR__retR, /* bg/nR/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bg/nR/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__nR__n0__fR__retR, /* bg/nR/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bg/nR/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__nR__nR__fR__retR, /* bg/nR/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bg/nR/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge___rrR___rrR__fR__retR, /* bge/%rR/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bge/%rR/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge___rrR__n0__fR__retR, /* bge/%rR/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bge/%rR/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge___rrR__nR__fR__retR, /* bge/%rR/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bge/%rR/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__n0___rrR__fR__retR, /* bge/n0/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bge/n0/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__n0__n0__fR__retR, /* bge/n0/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bge/n0/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__n0__nR__fR__retR, /* bge/n0/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bge/n0/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__nR___rrR__fR__retR, /* bge/nR/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bge/nR/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__nR__n0__fR__retR, /* bge/nR/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bge/nR/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__nR__nR__fR__retR, /* bge/nR/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bge/nR/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl___rrR___rrR__fR__retR, /* bl/%rR/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bl/%rR/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl___rrR__n0__fR__retR, /* bl/%rR/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bl/%rR/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl___rrR__nR__fR__retR, /* bl/%rR/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bl/%rR/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__n0___rrR__fR__retR, /* bl/n0/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bl/n0/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__n0__n0__fR__retR, /* bl/n0/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bl/n0/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__n0__nR__fR__retR, /* bl/n0/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bl/n0/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__nR___rrR__fR__retR, /* bl/nR/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bl/nR/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__nR__n0__fR__retR, /* bl/nR/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bl/nR/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__nR__nR__fR__retR, /* bl/nR/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bl/nR/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble___rrR___rrR__fR__retR, /* ble/%rR/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-ble/%rR/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble___rrR__n0__fR__retR, /* ble/%rR/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-ble/%rR/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble___rrR__nR__fR__retR, /* ble/%rR/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-ble/%rR/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__n0___rrR__fR__retR, /* ble/n0/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-ble/n0/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__n0__n0__fR__retR, /* ble/n0/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-ble/n0/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__n0__nR__fR__retR, /* ble/n0/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-ble/n0/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__nR___rrR__fR__retR, /* ble/nR/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-ble/nR/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__nR__n0__fR__retR, /* ble/nR/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-ble/nR/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__nR__nR__fR__retR, /* ble/nR/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-ble/nR/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne___rrR___rrR__fR__retR, /* bne/%rR/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bne/%rR/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne___rrR__n0__fR__retR, /* bne/%rR/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bne/%rR/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne___rrR__nR__fR__retR, /* bne/%rR/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bne/%rR/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__n0___rrR__fR__retR, /* bne/n0/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bne/n0/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__n0__n0__fR__retR, /* bne/n0/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bne/n0/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__n0__nR__fR__retR, /* bne/n0/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bne/n0/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__nR___rrR__fR__retR, /* bne/nR/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bne/nR/%rR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__nR__n0__fR__retR, /* bne/nR/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bne/nR/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__nR__nR__fR__retR, /* bne/nR/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bne/nR/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbneqi_mstack__n_m1__fR__retR, /* bneqi-stack/n-1/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bneqi-stack/n-1/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbneqi_mstack__n0__fR__retR, /* bneqi-stack/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bneqi-stack/n0/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbneqi_mstack__n1__fR__retR, /* bneqi-stack/n1/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bneqi-stack/n1/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbneqi_mstack__n2__fR__retR, /* bneqi-stack/n2/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bneqi-stack/n2/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbneqi_mstack__nR__fR__retR, /* bneqi-stack/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bneqi-stack/nR/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbt_mstack__fR__retR, /* bt-stack/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bt-stack/fR/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mcall__fR__retR, /* call/fR/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call/fR/retR. */
    structuredvm_specialized_instruction_opcode_copy_mto_mr_mstack___rrR, /* copy-to-r-stack/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_different_mstack, /* different-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_divided___rrR___rrR___rrR__retR, /* divided/%rR/%rR/%rR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_divided___rrR__n2___rrR__retR, /* divided/%rR/n2/%rR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_divided___rrR__nR___rrR__retR, /* divided/%rR/nR/%rR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_divided__nR___rrR___rrR__retR, /* divided/nR/%rR/%rR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_divided__nR__n2___rrR__retR, /* divided/nR/n2/%rR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_divided__nR__nR___rrR__retR, /* divided/nR/nR/%rR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_divided_mstack__retR, /* divided-stack/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_drop_mstack, /* drop-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_dup_mstack, /* dup-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_equal_mstack, /* equal-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_equali_mstack__n0, /* equali-stack/n0 is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_equali_mstack__n1, /* equali-stack/n1 is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_equali_mstack__n2, /* equali-stack/n2 is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_equali_mstack__nR, /* equali-stack/nR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mexitvm__retR, /* exitvm is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-exitvm/retR. */
    structuredvm_specialized_instruction_opcode_greater_mstack, /* greater-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_greaterorequal_mstack, /* greaterorequal-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_input___rrR__retR, /* input/%rR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_input_mstack__retR, /* input-stack/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_isnonzero_mstack, /* isnonzero-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_less_mstack, /* less-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_lessorequal_mstack, /* lessorequal-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_logicaland_mstack, /* logicaland-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_logicalnot_mstack, /* logicalnot-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_logicalor_mstack, /* logicalor-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_minus___rrR___rrR___rrR, /* minus/%rR/%rR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_minus___rrR__n1___rrR, /* minus/%rR/n1/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_minus___rrR__n2___rrR, /* minus/%rR/n2/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_minus___rrR__nR___rrR, /* minus/%rR/nR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_minus__nR___rrR___rrR, /* minus/nR/%rR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_minus__nR__n1___rrR, /* minus/nR/n1/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_minus__nR__n2___rrR, /* minus/nR/n2/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_minus__nR__nR___rrR, /* minus/nR/nR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_minus_mstack, /* minus-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_minusi_mstack__n1, /* minusi-stack/n1 is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_minusi_mstack__n2, /* minusi-stack/n2 is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_minusi_mstack__nR, /* minusi-stack/nR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_mov___rrR___rrR, /* mov/%rR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_mov__n0___rrR, /* mov/n0/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_mov__n1___rrR, /* mov/n1/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_mov__n_m1___rrR, /* mov/n-1/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_mov__n2___rrR, /* mov/n2/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_mov__nR___rrR, /* mov/nR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus___rrR___rrR___rrR, /* plus/%rR/%rR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus___rrR__n1___rrR, /* plus/%rR/n1/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus___rrR__n2___rrR, /* plus/%rR/n2/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus___rrR__nR___rrR, /* plus/%rR/nR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus__n1___rrR___rrR, /* plus/n1/%rR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus__n1__n1___rrR, /* plus/n1/n1/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus__n1__n2___rrR, /* plus/n1/n2/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus__n1__nR___rrR, /* plus/n1/nR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus__n2___rrR___rrR, /* plus/n2/%rR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus__n2__n1___rrR, /* plus/n2/n1/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus__n2__n2___rrR, /* plus/n2/n2/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus__n2__nR___rrR, /* plus/n2/nR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus__nR___rrR___rrR, /* plus/nR/%rR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus__nR__n1___rrR, /* plus/nR/n1/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus__nR__n2___rrR, /* plus/nR/n2/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus__nR__nR___rrR, /* plus/nR/nR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plus_mstack, /* plus-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plusi_mstack__n_m1, /* plusi-stack/n-1 is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plusi_mstack__n1, /* plusi-stack/n1 is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plusi_mstack__n2, /* plusi-stack/n2 is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_plusi_mstack__nR, /* plusi-stack/nR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_pop_mstack___rrR, /* pop-stack/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_print___rrR__retR, /* print/%rR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_print__nR__retR, /* print/nR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_print_mstack__retR, /* print-stack/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mprocedure_mprolog__retR, /* procedure-prolog is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-procedure-prolog/retR. */
    structuredvm_specialized_instruction_opcode_push_mstack___rrR, /* push-stack/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_push_mstack__n0, /* push-stack/n0 is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_push_mstack__n1, /* push-stack/n1 is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_push_mstack__n_m1, /* push-stack/n-1 is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_push_mstack__n2, /* push-stack/n2 is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_push_mstack__nR, /* push-stack/nR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_push_mstack__lR, /* push-stack/lR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_push_munspecified_mstack, /* push-unspecified-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_remainder___rrR___rrR___rrR__retR, /* remainder/%rR/%rR/%rR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_remainder___rrR__n2___rrR__retR, /* remainder/%rR/n2/%rR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_remainder___rrR__nR___rrR__retR, /* remainder/%rR/nR/%rR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_remainder__nR___rrR___rrR__retR, /* remainder/nR/%rR/%rR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_remainder__nR__n2___rrR__retR, /* remainder/nR/n2/%rR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_remainder__nR__nR___rrR__retR, /* remainder/nR/nR/%rR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_remainder_mstack__retR, /* remainder-stack/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mreturn_mto_mundertop__retR, /* return-to-undertop is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-return-to-undertop/retR. */
    structuredvm_specialized_instruction_opcode_swap_mstack, /* swap-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_times___rrR___rrR___rrR, /* times/%rR/%rR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_times___rrR__n2___rrR, /* times/%rR/n2/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_times___rrR__nR___rrR, /* times/%rR/nR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_times__nR___rrR___rrR, /* times/nR/%rR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_times__nR__n2___rrR, /* times/nR/n2/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_times__nR__nR___rrR, /* times/nR/nR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_times_mstack, /* times-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_uminus___rrR___rrR, /* uminus/%rR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_uminus__nR___rrR, /* uminus/nR/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_uminus_mstack, /* uminus-stack is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_underpop_mstack___rrR, /* underpop-stack/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_underpush_mstack___rrR, /* underpush-stack/%rR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode_underpush_mstack__nR, /* underpush-stack/nR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_munreachable__retR, /* unreachable is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-unreachable/retR. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mb__fR__retR, /* !REPLACEMENT-b/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe___rrR___rrR__fR__retR, /* !REPLACEMENT-be/%rR/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe___rrR__n0__fR__retR, /* !REPLACEMENT-be/%rR/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe___rrR__nR__fR__retR, /* !REPLACEMENT-be/%rR/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__n0___rrR__fR__retR, /* !REPLACEMENT-be/n0/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__n0__n0__fR__retR, /* !REPLACEMENT-be/n0/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__n0__nR__fR__retR, /* !REPLACEMENT-be/n0/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__nR___rrR__fR__retR, /* !REPLACEMENT-be/nR/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__nR__n0__fR__retR, /* !REPLACEMENT-be/nR/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__nR__nR__fR__retR, /* !REPLACEMENT-be/nR/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbeqi_mstack__n_m1__fR__retR, /* !REPLACEMENT-beqi-stack/n-1/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbeqi_mstack__n0__fR__retR, /* !REPLACEMENT-beqi-stack/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbeqi_mstack__n1__fR__retR, /* !REPLACEMENT-beqi-stack/n1/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbeqi_mstack__n2__fR__retR, /* !REPLACEMENT-beqi-stack/n2/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbeqi_mstack__nR__fR__retR, /* !REPLACEMENT-beqi-stack/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbf_mstack__fR__retR, /* !REPLACEMENT-bf-stack/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg___rrR___rrR__fR__retR, /* !REPLACEMENT-bg/%rR/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg___rrR__n0__fR__retR, /* !REPLACEMENT-bg/%rR/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg___rrR__nR__fR__retR, /* !REPLACEMENT-bg/%rR/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__n0___rrR__fR__retR, /* !REPLACEMENT-bg/n0/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__n0__n0__fR__retR, /* !REPLACEMENT-bg/n0/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__n0__nR__fR__retR, /* !REPLACEMENT-bg/n0/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__nR___rrR__fR__retR, /* !REPLACEMENT-bg/nR/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__nR__n0__fR__retR, /* !REPLACEMENT-bg/nR/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__nR__nR__fR__retR, /* !REPLACEMENT-bg/nR/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge___rrR___rrR__fR__retR, /* !REPLACEMENT-bge/%rR/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge___rrR__n0__fR__retR, /* !REPLACEMENT-bge/%rR/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge___rrR__nR__fR__retR, /* !REPLACEMENT-bge/%rR/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__n0___rrR__fR__retR, /* !REPLACEMENT-bge/n0/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__n0__n0__fR__retR, /* !REPLACEMENT-bge/n0/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__n0__nR__fR__retR, /* !REPLACEMENT-bge/n0/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__nR___rrR__fR__retR, /* !REPLACEMENT-bge/nR/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__nR__n0__fR__retR, /* !REPLACEMENT-bge/nR/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__nR__nR__fR__retR, /* !REPLACEMENT-bge/nR/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl___rrR___rrR__fR__retR, /* !REPLACEMENT-bl/%rR/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl___rrR__n0__fR__retR, /* !REPLACEMENT-bl/%rR/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl___rrR__nR__fR__retR, /* !REPLACEMENT-bl/%rR/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__n0___rrR__fR__retR, /* !REPLACEMENT-bl/n0/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__n0__n0__fR__retR, /* !REPLACEMENT-bl/n0/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__n0__nR__fR__retR, /* !REPLACEMENT-bl/n0/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__nR___rrR__fR__retR, /* !REPLACEMENT-bl/nR/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__nR__n0__fR__retR, /* !REPLACEMENT-bl/nR/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__nR__nR__fR__retR, /* !REPLACEMENT-bl/nR/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble___rrR___rrR__fR__retR, /* !REPLACEMENT-ble/%rR/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble___rrR__n0__fR__retR, /* !REPLACEMENT-ble/%rR/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble___rrR__nR__fR__retR, /* !REPLACEMENT-ble/%rR/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__n0___rrR__fR__retR, /* !REPLACEMENT-ble/n0/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__n0__n0__fR__retR, /* !REPLACEMENT-ble/n0/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__n0__nR__fR__retR, /* !REPLACEMENT-ble/n0/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__nR___rrR__fR__retR, /* !REPLACEMENT-ble/nR/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__nR__n0__fR__retR, /* !REPLACEMENT-ble/nR/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__nR__nR__fR__retR, /* !REPLACEMENT-ble/nR/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne___rrR___rrR__fR__retR, /* !REPLACEMENT-bne/%rR/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne___rrR__n0__fR__retR, /* !REPLACEMENT-bne/%rR/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne___rrR__nR__fR__retR, /* !REPLACEMENT-bne/%rR/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__n0___rrR__fR__retR, /* !REPLACEMENT-bne/n0/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__n0__n0__fR__retR, /* !REPLACEMENT-bne/n0/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__n0__nR__fR__retR, /* !REPLACEMENT-bne/n0/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__nR___rrR__fR__retR, /* !REPLACEMENT-bne/nR/%rR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__nR__n0__fR__retR, /* !REPLACEMENT-bne/nR/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__nR__nR__fR__retR, /* !REPLACEMENT-bne/nR/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbneqi_mstack__n_m1__fR__retR, /* !REPLACEMENT-bneqi-stack/n-1/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbneqi_mstack__n0__fR__retR, /* !REPLACEMENT-bneqi-stack/n0/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbneqi_mstack__n1__fR__retR, /* !REPLACEMENT-bneqi-stack/n1/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbneqi_mstack__n2__fR__retR, /* !REPLACEMENT-bneqi-stack/n2/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbneqi_mstack__nR__fR__retR, /* !REPLACEMENT-bneqi-stack/nR/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbt_mstack__fR__retR, /* !REPLACEMENT-bt-stack/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mcall__fR__retR, /* !REPLACEMENT-call/fR/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mexitvm__retR, /* !REPLACEMENT-exitvm/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mprocedure_mprolog__retR, /* !REPLACEMENT-procedure-prolog/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_mreturn_mto_mundertop__retR, /* !REPLACEMENT-return-to-undertop/retR is NOT potentially defective. */
    structuredvm_specialized_instruction_opcode__eREPLACEMENT_munreachable__retR /* !REPLACEMENT-unreachable/retR is NOT potentially defective. */
  };
#endif // #ifdef JITTER_HAVE_DEFECT_REPLACEMENT


#ifdef JITTER_HAVE_DEFECT_REPLACEMENT
const jitter_uint
structuredvm_call_related_specialized_instruction_ids []
= {
    structuredvm_specialized_instruction_opcode_call__fR__retR,
    structuredvm_specialized_instruction_opcode_procedure_mprolog,
    structuredvm_specialized_instruction_opcode_return_mto_mundertop
  };

const jitter_uint
structuredvm_call_related_specialized_instruction_id_no
= sizeof (structuredvm_call_related_specialized_instruction_ids) / sizeof (jitter_uint);

const bool
structuredvm_specialized_instruction_call_relateds []
= {
    0, /* !INVALID */
    0, /* !BEGINBASICBLOCK */
    0, /* !EXITVM */
    0, /* !DATALOCATIONS */
    0, /* !NOP */
    0, /* !UNREACHABLE0 */
    0, /* !UNREACHABLE1 */
    0, /* !PRETENDTOJUMPANYWHERE */
    0, /* b/fR */
    0, /* be/%rR/%rR/fR */
    0, /* be/%rR/n0/fR */
    0, /* be/%rR/nR/fR */
    0, /* be/n0/%rR/fR */
    0, /* be/n0/n0/fR */
    0, /* be/n0/nR/fR */
    0, /* be/nR/%rR/fR */
    0, /* be/nR/n0/fR */
    0, /* be/nR/nR/fR */
    0, /* beqi-stack/n-1/fR */
    0, /* beqi-stack/n0/fR */
    0, /* beqi-stack/n1/fR */
    0, /* beqi-stack/n2/fR */
    0, /* beqi-stack/nR/fR */
    0, /* bf-stack/fR */
    0, /* bg/%rR/%rR/fR */
    0, /* bg/%rR/n0/fR */
    0, /* bg/%rR/nR/fR */
    0, /* bg/n0/%rR/fR */
    0, /* bg/n0/n0/fR */
    0, /* bg/n0/nR/fR */
    0, /* bg/nR/%rR/fR */
    0, /* bg/nR/n0/fR */
    0, /* bg/nR/nR/fR */
    0, /* bge/%rR/%rR/fR */
    0, /* bge/%rR/n0/fR */
    0, /* bge/%rR/nR/fR */
    0, /* bge/n0/%rR/fR */
    0, /* bge/n0/n0/fR */
    0, /* bge/n0/nR/fR */
    0, /* bge/nR/%rR/fR */
    0, /* bge/nR/n0/fR */
    0, /* bge/nR/nR/fR */
    0, /* bl/%rR/%rR/fR */
    0, /* bl/%rR/n0/fR */
    0, /* bl/%rR/nR/fR */
    0, /* bl/n0/%rR/fR */
    0, /* bl/n0/n0/fR */
    0, /* bl/n0/nR/fR */
    0, /* bl/nR/%rR/fR */
    0, /* bl/nR/n0/fR */
    0, /* bl/nR/nR/fR */
    0, /* ble/%rR/%rR/fR */
    0, /* ble/%rR/n0/fR */
    0, /* ble/%rR/nR/fR */
    0, /* ble/n0/%rR/fR */
    0, /* ble/n0/n0/fR */
    0, /* ble/n0/nR/fR */
    0, /* ble/nR/%rR/fR */
    0, /* ble/nR/n0/fR */
    0, /* ble/nR/nR/fR */
    0, /* bne/%rR/%rR/fR */
    0, /* bne/%rR/n0/fR */
    0, /* bne/%rR/nR/fR */
    0, /* bne/n0/%rR/fR */
    0, /* bne/n0/n0/fR */
    0, /* bne/n0/nR/fR */
    0, /* bne/nR/%rR/fR */
    0, /* bne/nR/n0/fR */
    0, /* bne/nR/nR/fR */
    0, /* bneqi-stack/n-1/fR */
    0, /* bneqi-stack/n0/fR */
    0, /* bneqi-stack/n1/fR */
    0, /* bneqi-stack/n2/fR */
    0, /* bneqi-stack/nR/fR */
    0, /* bt-stack/fR */
    1, /* call/fR/retR */
    0, /* copy-to-r-stack/%rR */
    0, /* different-stack */
    0, /* divided/%rR/%rR/%rR/retR */
    0, /* divided/%rR/n2/%rR/retR */
    0, /* divided/%rR/nR/%rR/retR */
    0, /* divided/nR/%rR/%rR/retR */
    0, /* divided/nR/n2/%rR/retR */
    0, /* divided/nR/nR/%rR/retR */
    0, /* divided-stack/retR */
    0, /* drop-stack */
    0, /* dup-stack */
    0, /* equal-stack */
    0, /* equali-stack/n0 */
    0, /* equali-stack/n1 */
    0, /* equali-stack/n2 */
    0, /* equali-stack/nR */
    0, /* exitvm */
    0, /* greater-stack */
    0, /* greaterorequal-stack */
    0, /* input/%rR/retR */
    0, /* input-stack/retR */
    0, /* isnonzero-stack */
    0, /* less-stack */
    0, /* lessorequal-stack */
    0, /* logicaland-stack */
    0, /* logicalnot-stack */
    0, /* logicalor-stack */
    0, /* minus/%rR/%rR/%rR */
    0, /* minus/%rR/n1/%rR */
    0, /* minus/%rR/n2/%rR */
    0, /* minus/%rR/nR/%rR */
    0, /* minus/nR/%rR/%rR */
    0, /* minus/nR/n1/%rR */
    0, /* minus/nR/n2/%rR */
    0, /* minus/nR/nR/%rR */
    0, /* minus-stack */
    0, /* minusi-stack/n1 */
    0, /* minusi-stack/n2 */
    0, /* minusi-stack/nR */
    0, /* mov/%rR/%rR */
    0, /* mov/n0/%rR */
    0, /* mov/n1/%rR */
    0, /* mov/n-1/%rR */
    0, /* mov/n2/%rR */
    0, /* mov/nR/%rR */
    0, /* plus/%rR/%rR/%rR */
    0, /* plus/%rR/n1/%rR */
    0, /* plus/%rR/n2/%rR */
    0, /* plus/%rR/nR/%rR */
    0, /* plus/n1/%rR/%rR */
    0, /* plus/n1/n1/%rR */
    0, /* plus/n1/n2/%rR */
    0, /* plus/n1/nR/%rR */
    0, /* plus/n2/%rR/%rR */
    0, /* plus/n2/n1/%rR */
    0, /* plus/n2/n2/%rR */
    0, /* plus/n2/nR/%rR */
    0, /* plus/nR/%rR/%rR */
    0, /* plus/nR/n1/%rR */
    0, /* plus/nR/n2/%rR */
    0, /* plus/nR/nR/%rR */
    0, /* plus-stack */
    0, /* plusi-stack/n-1 */
    0, /* plusi-stack/n1 */
    0, /* plusi-stack/n2 */
    0, /* plusi-stack/nR */
    0, /* pop-stack/%rR */
    0, /* print/%rR/retR */
    0, /* print/nR/retR */
    0, /* print-stack/retR */
    1, /* procedure-prolog */
    0, /* push-stack/%rR */
    0, /* push-stack/n0 */
    0, /* push-stack/n1 */
    0, /* push-stack/n-1 */
    0, /* push-stack/n2 */
    0, /* push-stack/nR */
    0, /* push-stack/lR */
    0, /* push-unspecified-stack */
    0, /* remainder/%rR/%rR/%rR/retR */
    0, /* remainder/%rR/n2/%rR/retR */
    0, /* remainder/%rR/nR/%rR/retR */
    0, /* remainder/nR/%rR/%rR/retR */
    0, /* remainder/nR/n2/%rR/retR */
    0, /* remainder/nR/nR/%rR/retR */
    0, /* remainder-stack/retR */
    1, /* return-to-undertop */
    0, /* swap-stack */
    0, /* times/%rR/%rR/%rR */
    0, /* times/%rR/n2/%rR */
    0, /* times/%rR/nR/%rR */
    0, /* times/nR/%rR/%rR */
    0, /* times/nR/n2/%rR */
    0, /* times/nR/nR/%rR */
    0, /* times-stack */
    0, /* uminus/%rR/%rR */
    0, /* uminus/nR/%rR */
    0, /* uminus-stack */
    0, /* underpop-stack/%rR */
    0, /* underpush-stack/%rR */
    0, /* underpush-stack/nR */
    0, /* unreachable */
    0, /* !REPLACEMENT-b/fR/retR */
    0, /* !REPLACEMENT-be/%rR/%rR/fR/retR */
    0, /* !REPLACEMENT-be/%rR/n0/fR/retR */
    0, /* !REPLACEMENT-be/%rR/nR/fR/retR */
    0, /* !REPLACEMENT-be/n0/%rR/fR/retR */
    0, /* !REPLACEMENT-be/n0/n0/fR/retR */
    0, /* !REPLACEMENT-be/n0/nR/fR/retR */
    0, /* !REPLACEMENT-be/nR/%rR/fR/retR */
    0, /* !REPLACEMENT-be/nR/n0/fR/retR */
    0, /* !REPLACEMENT-be/nR/nR/fR/retR */
    0, /* !REPLACEMENT-beqi-stack/n-1/fR/retR */
    0, /* !REPLACEMENT-beqi-stack/n0/fR/retR */
    0, /* !REPLACEMENT-beqi-stack/n1/fR/retR */
    0, /* !REPLACEMENT-beqi-stack/n2/fR/retR */
    0, /* !REPLACEMENT-beqi-stack/nR/fR/retR */
    0, /* !REPLACEMENT-bf-stack/fR/retR */
    0, /* !REPLACEMENT-bg/%rR/%rR/fR/retR */
    0, /* !REPLACEMENT-bg/%rR/n0/fR/retR */
    0, /* !REPLACEMENT-bg/%rR/nR/fR/retR */
    0, /* !REPLACEMENT-bg/n0/%rR/fR/retR */
    0, /* !REPLACEMENT-bg/n0/n0/fR/retR */
    0, /* !REPLACEMENT-bg/n0/nR/fR/retR */
    0, /* !REPLACEMENT-bg/nR/%rR/fR/retR */
    0, /* !REPLACEMENT-bg/nR/n0/fR/retR */
    0, /* !REPLACEMENT-bg/nR/nR/fR/retR */
    0, /* !REPLACEMENT-bge/%rR/%rR/fR/retR */
    0, /* !REPLACEMENT-bge/%rR/n0/fR/retR */
    0, /* !REPLACEMENT-bge/%rR/nR/fR/retR */
    0, /* !REPLACEMENT-bge/n0/%rR/fR/retR */
    0, /* !REPLACEMENT-bge/n0/n0/fR/retR */
    0, /* !REPLACEMENT-bge/n0/nR/fR/retR */
    0, /* !REPLACEMENT-bge/nR/%rR/fR/retR */
    0, /* !REPLACEMENT-bge/nR/n0/fR/retR */
    0, /* !REPLACEMENT-bge/nR/nR/fR/retR */
    0, /* !REPLACEMENT-bl/%rR/%rR/fR/retR */
    0, /* !REPLACEMENT-bl/%rR/n0/fR/retR */
    0, /* !REPLACEMENT-bl/%rR/nR/fR/retR */
    0, /* !REPLACEMENT-bl/n0/%rR/fR/retR */
    0, /* !REPLACEMENT-bl/n0/n0/fR/retR */
    0, /* !REPLACEMENT-bl/n0/nR/fR/retR */
    0, /* !REPLACEMENT-bl/nR/%rR/fR/retR */
    0, /* !REPLACEMENT-bl/nR/n0/fR/retR */
    0, /* !REPLACEMENT-bl/nR/nR/fR/retR */
    0, /* !REPLACEMENT-ble/%rR/%rR/fR/retR */
    0, /* !REPLACEMENT-ble/%rR/n0/fR/retR */
    0, /* !REPLACEMENT-ble/%rR/nR/fR/retR */
    0, /* !REPLACEMENT-ble/n0/%rR/fR/retR */
    0, /* !REPLACEMENT-ble/n0/n0/fR/retR */
    0, /* !REPLACEMENT-ble/n0/nR/fR/retR */
    0, /* !REPLACEMENT-ble/nR/%rR/fR/retR */
    0, /* !REPLACEMENT-ble/nR/n0/fR/retR */
    0, /* !REPLACEMENT-ble/nR/nR/fR/retR */
    0, /* !REPLACEMENT-bne/%rR/%rR/fR/retR */
    0, /* !REPLACEMENT-bne/%rR/n0/fR/retR */
    0, /* !REPLACEMENT-bne/%rR/nR/fR/retR */
    0, /* !REPLACEMENT-bne/n0/%rR/fR/retR */
    0, /* !REPLACEMENT-bne/n0/n0/fR/retR */
    0, /* !REPLACEMENT-bne/n0/nR/fR/retR */
    0, /* !REPLACEMENT-bne/nR/%rR/fR/retR */
    0, /* !REPLACEMENT-bne/nR/n0/fR/retR */
    0, /* !REPLACEMENT-bne/nR/nR/fR/retR */
    0, /* !REPLACEMENT-bneqi-stack/n-1/fR/retR */
    0, /* !REPLACEMENT-bneqi-stack/n0/fR/retR */
    0, /* !REPLACEMENT-bneqi-stack/n1/fR/retR */
    0, /* !REPLACEMENT-bneqi-stack/n2/fR/retR */
    0, /* !REPLACEMENT-bneqi-stack/nR/fR/retR */
    0, /* !REPLACEMENT-bt-stack/fR/retR */
    0, /* !REPLACEMENT-call/fR/retR */
    0, /* !REPLACEMENT-exitvm/retR */
    0, /* !REPLACEMENT-procedure-prolog/retR */
    0, /* !REPLACEMENT-return-to-undertop/retR */
    0 /* !REPLACEMENT-unreachable/retR */
  };

#endif // #ifdef JITTER_HAVE_DEFECT_REPLACEMENT


void
structuredvm_rewrite (struct jitter_mutable_routine *jitter_mutable_routine_p)
{
  JITTTER_REWRITE_FUNCTION_PROLOG_;

/* User-specified code, rewriter part: beginning. */

/* User-specified code, rewriter part: end */


//asm volatile ("\n# checking not-not--nothing");
//fprintf (stderr, "Trying rule 1 of 25, \"not-not--nothing\" (line 322)\n");
/* Rewrite rule "not-not--nothing" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, logicalnot_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, logicalnot_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule not-not--nothing (line 322) fires...\n");
    //fprintf (stderr, "  ...End of the rule not-not--nothing\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking not-bf--bt");
//fprintf (stderr, "Trying rule 2 of 25, \"not-bf--bt\" (line 323)\n");
/* Rewrite rule "not-bf--bt" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, logicalnot_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, bf_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule not-bf--bt (line 323) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction bt-stack\n");
    JITTER_RULE_APPEND_INSTRUCTION_(bt_mstack);
    //fprintf (stderr, "    instantiating the 0-th argument of bt-stack\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule not-bf--bt\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking not-bt--bf");
//fprintf (stderr, "Trying rule 3 of 25, \"not-bt--bf\" (line 324)\n");
/* Rewrite rule "not-bt--bf" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, logicalnot_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, bt_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule not-bt--bf (line 324) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction bf-stack\n");
    JITTER_RULE_APPEND_INSTRUCTION_(bf_mstack);
    //fprintf (stderr, "    instantiating the 0-th argument of bf-stack\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule not-bt--bf\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking less-not--greaterorequal");
//fprintf (stderr, "Trying rule 4 of 25, \"less-not--greaterorequal\" (line 325)\n");
/* Rewrite rule "less-not--greaterorequal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, less_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, logicalnot_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule less-not--greaterorequal (line 325) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction greaterorequal-stack\n");
    JITTER_RULE_APPEND_INSTRUCTION_(greaterorequal_mstack);
    //fprintf (stderr, "  ...End of the rule less-not--greaterorequal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking lessorequal-not--greater");
//fprintf (stderr, "Trying rule 5 of 25, \"lessorequal-not--greater\" (line 326)\n");
/* Rewrite rule "lessorequal-not--greater" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, lessorequal_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, logicalnot_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule lessorequal-not--greater (line 326) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction greater-stack\n");
    JITTER_RULE_APPEND_INSTRUCTION_(greater_mstack);
    //fprintf (stderr, "  ...End of the rule lessorequal-not--greater\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking greater-not--lessorequal");
//fprintf (stderr, "Trying rule 6 of 25, \"greater-not--lessorequal\" (line 327)\n");
/* Rewrite rule "greater-not--lessorequal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, greater_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, logicalnot_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule greater-not--lessorequal (line 327) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction lessorequal-stack\n");
    JITTER_RULE_APPEND_INSTRUCTION_(lessorequal_mstack);
    //fprintf (stderr, "  ...End of the rule greater-not--lessorequal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking greaterorequal-not--less");
//fprintf (stderr, "Trying rule 7 of 25, \"greaterorequal-not--less\" (line 328)\n");
/* Rewrite rule "greaterorequal-not--less" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, greaterorequal_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, logicalnot_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule greaterorequal-not--less (line 328) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction less-stack\n");
    JITTER_RULE_APPEND_INSTRUCTION_(less_mstack);
    //fprintf (stderr, "  ...End of the rule greaterorequal-not--less\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking pop-push--copytor");
//fprintf (stderr, "Trying rule 8 of 25, \"pop-push--copytor\" (line 329)\n");
/* Rewrite rule "pop-push--copytor" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, pop_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_REGISTER(0, 0)
                         )
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_REGISTER(1, 0)
                         )
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule pop-push--copytor (line 329) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-to-r-stack\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mto_mr_mstack);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-to-r-stack\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule pop-push--copytor\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-pop--");
//fprintf (stderr, "Trying rule 9 of 25, \"push-pop--\" (line 330)\n");
/* Rewrite rule "push-pop--" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, pop_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_REGISTER(0, 0)
                         )
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_REGISTER(1, 0)
                         )
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-pop-- (line 330) fires...\n");
    //fprintf (stderr, "  ...End of the rule push-pop--\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-pop--movr");
//fprintf (stderr, "Trying rule 10 of 25, \"push-pop--movr\" (line 331)\n");
/* Rewrite rule "push-pop--movr" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, pop_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_REGISTER(0, 0)
                         )
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_REGISTER(1, 0)
                         )
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-pop--movr (line 331) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction mov\n");
    JITTER_RULE_APPEND_INSTRUCTION_(mov);
    //fprintf (stderr, "    instantiating the 0-th argument of mov\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of mov\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule push-pop--movr\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-pop--movn");
//fprintf (stderr, "Trying rule 11 of 25, \"push-pop--movn\" (line 332)\n");
/* Rewrite rule "push-pop--movn" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, pop_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_LITERAL(0, 0)
                         )
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_REGISTER(1, 0)
                         )
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-pop--movn (line 332) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction mov\n");
    JITTER_RULE_APPEND_INSTRUCTION_(mov);
    //fprintf (stderr, "    instantiating the 0-th argument of mov\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of mov\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule push-pop--movn\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-plus--plusi");
//fprintf (stderr, "Trying rule 12 of 25, \"push-plus--plusi\" (line 333)\n");
/* Rewrite rule "push-plus--plusi" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, plus_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_LITERAL(0, 0)
                         )
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-plus--plusi (line 333) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction plusi-stack\n");
    JITTER_RULE_APPEND_INSTRUCTION_(plusi_mstack);
    //fprintf (stderr, "    instantiating the 0-th argument of plusi-stack\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule push-plus--plusi\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-minus--minusi");
//fprintf (stderr, "Trying rule 13 of 25, \"push-minus--minusi\" (line 334)\n");
/* Rewrite rule "push-minus--minusi" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, minus_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_LITERAL(0, 0)
                         )
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-minus--minusi (line 334) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction minusi-stack\n");
    JITTER_RULE_APPEND_INSTRUCTION_(minusi_mstack);
    //fprintf (stderr, "    instantiating the 0-th argument of minusi-stack\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule push-minus--minusi\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-equal--equali");
//fprintf (stderr, "Trying rule 14 of 25, \"push-equal--equali\" (line 337)\n");
/* Rewrite rule "push-equal--equali" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, equal_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_LITERAL(0, 0)
                         )
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-equal--equali (line 337) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction equali-stack\n");
    JITTER_RULE_APPEND_INSTRUCTION_(equali_mstack);
    //fprintf (stderr, "    instantiating the 0-th argument of equali-stack\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule push-equal--equali\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking equali-bt--beqi");
//fprintf (stderr, "Trying rule 15 of 25, \"equali-bt--beqi\" (line 338)\n");
/* Rewrite rule "equali-bt--beqi" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, equali_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, bt_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_LITERAL(0, 0)
                         )
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule equali-bt--beqi (line 338) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction beqi-stack\n");
    JITTER_RULE_APPEND_INSTRUCTION_(beqi_mstack);
    //fprintf (stderr, "    instantiating the 0-th argument of beqi-stack\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of beqi-stack\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule equali-bt--beqi\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking equali-bf--bneqi");
//fprintf (stderr, "Trying rule 16 of 25, \"equali-bf--bneqi\" (line 339)\n");
/* Rewrite rule "equali-bf--bneqi" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, equali_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, bf_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_LITERAL(0, 0)
                         )
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule equali-bf--bneqi (line 339) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction bneqi-stack\n");
    JITTER_RULE_APPEND_INSTRUCTION_(bneqi_mstack);
    //fprintf (stderr, "    instantiating the 0-th argument of bneqi-stack\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of bneqi-stack\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule equali-bf--bneqi\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-drop--");
//fprintf (stderr, "Trying rule 17 of 25, \"push-drop--\" (line 341)\n");
/* Rewrite rule "push-drop--" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-drop-- (line 341) fires...\n");
    //fprintf (stderr, "  ...End of the rule push-drop--\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-push--push-dup");
//fprintf (stderr, "Trying rule 18 of 25, \"push-push--push-dup\" (line 342)\n");
/* Rewrite rule "push-push--push-dup" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-push--push-dup (line 342) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction push-stack\n");
    JITTER_RULE_APPEND_INSTRUCTION_(push_mstack);
    //fprintf (stderr, "    instantiating the 0-th argument of push-stack\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction dup-stack\n");
    JITTER_RULE_APPEND_INSTRUCTION_(dup_mstack);
    //fprintf (stderr, "  ...End of the rule push-push--push-dup\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-bf--b");
//fprintf (stderr, "Trying rule 19 of 25, \"push-bf--b\" (line 345)\n");
/* Rewrite rule "push-bf--b" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, bf_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_LITERAL_ARGUMENT(0, 0, 0)
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_LITERAL(0, 0)
                         )
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-bf--b (line 345) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction b\n");
    JITTER_RULE_APPEND_INSTRUCTION_(b);
    //fprintf (stderr, "    instantiating the 0-th argument of b\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule push-bf--b\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-bt--");
//fprintf (stderr, "Trying rule 20 of 25, \"push-bt--\" (line 347)\n");
/* Rewrite rule "push-bt--" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, bt_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_LITERAL_ARGUMENT(0, 0, 0)
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_LITERAL(0, 0)
                         )
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-bt-- (line 347) fires...\n");
    //fprintf (stderr, "  ...End of the rule push-bt--\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking plusi-minusi--");
//fprintf (stderr, "Trying rule 21 of 25, \"plusi-minusi--\" (line 349)\n");
/* Rewrite rule "plusi-minusi--" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, plusi_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, minusi_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule plusi-minusi-- (line 349) fires...\n");
    //fprintf (stderr, "  ...End of the rule plusi-minusi--\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking minusi-plusi--");
//fprintf (stderr, "Trying rule 22 of 25, \"minusi-plusi--\" (line 350)\n");
/* Rewrite rule "minusi-plusi--" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, minusi_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, plusi_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule minusi-plusi-- (line 350) fires...\n");
    //fprintf (stderr, "  ...End of the rule minusi-plusi--\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking plusi--");
//fprintf (stderr, "Trying rule 23 of 25, \"plusi--\" (line 351)\n");
/* Rewrite rule "plusi--" */
JITTER_RULE_BEGIN(1)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, plusi_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_LITERAL_ARGUMENT(0, 0, 0)
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_LITERAL(0, 0)
                         )
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule plusi-- (line 351) fires...\n");
    //fprintf (stderr, "  ...End of the rule plusi--\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking minusi--");
//fprintf (stderr, "Trying rule 24 of 25, \"minusi--\" (line 352)\n");
/* Rewrite rule "minusi--" */
JITTER_RULE_BEGIN(1)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, minusi_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_LITERAL_ARGUMENT(0, 0, 0)
    JITTER_RULE_CONDITION(false
                          || JITTER_RULE_ARGUMENT_IS_A_LITERAL(0, 0)
                         )
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule minusi-- (line 352) fires...\n");
    //fprintf (stderr, "  ...End of the rule minusi--\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-swap--underpush");
//fprintf (stderr, "Trying rule 25 of 25, \"push-swap--underpush\" (line 356)\n");
/* Rewrite rule "push-swap--underpush" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mstack)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, swap_mstack)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-swap--underpush (line 356) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction underpush-stack\n");
    JITTER_RULE_APPEND_INSTRUCTION_(underpush_mstack);
    //fprintf (stderr, "    instantiating the 0-th argument of underpush-stack\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule push-swap--underpush\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//fprintf (stderr, "No more rules to try\n");
}


//#include <jitter/jitter-fatal.h>

//#include <jitter/jitter.h>
//#include <jitter/jitter-instruction.h>
//#include <jitter/jitter-specialize.h>

//#include "structuredvm-vm.h"
//#include "structuredvm-meta-instructions.h"
//#include "structuredvm-specialized-instructions.h"


/* Recognizer function prototypes. */
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_b (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_b__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be___rrR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be___rrR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be___rrR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__n0___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__n0___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__n0__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__n0__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__nR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n_m1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n_m1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bf_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bf_mstack__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg___rrR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg___rrR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg___rrR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__n0___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__n0___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__n0__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__n0__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__nR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge___rrR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge___rrR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge___rrR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__n0___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__n0___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__n0__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__n0__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__nR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl___rrR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl___rrR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl___rrR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__n0___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__n0___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__n0__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__n0__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__nR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble___rrR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble___rrR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble___rrR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__n0___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__n0___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__n0__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__n0__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__nR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne___rrR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne___rrR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne___rrR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__n0___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__n0___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__n0__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__n0__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__nR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n_m1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n_m1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bt_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bt_mstack__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_call (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_call__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_copy_mto_mr_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_copy_mto_mr_mstack___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_different_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided___rrR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided___rrR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided___rrR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided___rrR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided__nR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided__nR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided__nR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_drop_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_dup_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_equal_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_equali_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_equali_mstack__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_equali_mstack__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_equali_mstack__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_equali_mstack__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_exitvm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_greater_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_greaterorequal_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_input (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_input___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_input_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_isnonzero_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_less_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_lessorequal_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_logicaland_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_logicalnot_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_logicalor_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minusi_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minusi_mstack__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minusi_mstack__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minusi_mstack__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n0___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n_m1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n_m1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plusi_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plusi_mstack__n_m1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plusi_mstack__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plusi_mstack__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plusi_mstack__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_pop_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_pop_mstack___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_print (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_print___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_print__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_print_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_procedure_mprolog (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack__n_m1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_munspecified_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder___rrR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder___rrR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder___rrR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder___rrR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder__nR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder__nR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder__nR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_return_mto_mundertop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_swap_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times___rrR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times___rrR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times___rrR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times___rrR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times__nR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times__nR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times__nR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_uminus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_uminus___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_uminus___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_uminus__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_uminus__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_uminus_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_underpop_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_underpop_mstack___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_underpush_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_underpush_mstack___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_underpush_mstack__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_unreachable (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));


/* Recognizer function definitions. */
inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_b (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_b__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_b__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_b__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_be___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_be__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_be__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_be___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_be___rrR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_be___rrR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_be___rrR___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_be___rrR___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be___rrR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_be___rrR__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be___rrR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_be___rrR__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_be___rrR__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be___rrR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_be___rrR__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_be__n0___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_be__n0__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_be__n0__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__n0___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_be__n0___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__n0___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_be__n0___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_be__n0__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__n0__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_be__n0__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_be__n0__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__n0__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_be__n0__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_be__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_be__nR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_be__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_be__nR___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__nR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_be__nR___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_be__nR__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_be__nR__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_be__nR__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_be__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_be__nR__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == -1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_beqi_mstack__n_m1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_beqi_mstack__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_beqi_mstack__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_beqi_mstack__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_beqi_mstack__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n_m1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_beqi_mstack__n_m1__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n_m1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_beqi_mstack__n_m1__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_beqi_mstack__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_beqi_mstack__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_beqi_mstack__n1__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_beqi_mstack__n1__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_beqi_mstack__n2__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_beqi_mstack__n2__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_beqi_mstack__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_beqi_mstack__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_beqi_mstack__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bf_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bf_mstack__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bf_mstack__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bf_mstack__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bg___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bg__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bg__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bg___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bg___rrR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bg___rrR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bg___rrR___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bg___rrR___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg___rrR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bg___rrR__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg___rrR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bg___rrR__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bg___rrR__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg___rrR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bg___rrR__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bg__n0___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bg__n0__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bg__n0__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__n0___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bg__n0___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__n0___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bg__n0___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bg__n0__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__n0__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bg__n0__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bg__n0__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__n0__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bg__n0__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bg__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bg__nR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bg__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bg__nR___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__nR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bg__nR___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bg__nR__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bg__nR__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bg__nR__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bg__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bg__nR__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bge___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bge__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bge__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bge___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bge___rrR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bge___rrR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bge___rrR___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bge___rrR___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge___rrR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bge___rrR__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge___rrR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bge___rrR__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bge___rrR__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge___rrR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bge___rrR__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bge__n0___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bge__n0__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bge__n0__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__n0___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bge__n0___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__n0___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bge__n0___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bge__n0__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__n0__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bge__n0__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bge__n0__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__n0__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bge__n0__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bge__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bge__nR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bge__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bge__nR___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__nR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bge__nR___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bge__nR__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bge__nR__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bge__nR__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bge__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bge__nR__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bl___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bl__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bl__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bl___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bl___rrR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bl___rrR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bl___rrR___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bl___rrR___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl___rrR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bl___rrR__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl___rrR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bl___rrR__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bl___rrR__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl___rrR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bl___rrR__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bl__n0___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bl__n0__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bl__n0__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__n0___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bl__n0___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__n0___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bl__n0___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bl__n0__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__n0__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bl__n0__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bl__n0__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__n0__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bl__n0__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bl__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bl__nR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bl__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bl__nR___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__nR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bl__nR___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bl__nR__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bl__nR__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bl__nR__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bl__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bl__nR__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_ble___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_ble__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_ble__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_ble___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_ble___rrR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_ble___rrR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_ble___rrR___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_ble___rrR___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble___rrR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_ble___rrR__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble___rrR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_ble___rrR__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_ble___rrR__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble___rrR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_ble___rrR__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_ble__n0___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_ble__n0__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_ble__n0__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__n0___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_ble__n0___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__n0___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_ble__n0___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_ble__n0__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__n0__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_ble__n0__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_ble__n0__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__n0__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_ble__n0__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_ble__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_ble__nR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_ble__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_ble__nR___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__nR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_ble__nR___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_ble__nR__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_ble__nR__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_ble__nR__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_ble__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_ble__nR__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bne___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bne__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bne__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bne___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bne___rrR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bne___rrR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bne___rrR___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bne___rrR___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne___rrR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bne___rrR__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne___rrR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bne___rrR__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bne___rrR__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne___rrR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bne___rrR__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bne__n0___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bne__n0__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bne__n0__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__n0___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bne__n0___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__n0___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bne__n0___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bne__n0__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__n0__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bne__n0__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bne__n0__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__n0__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bne__n0__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_bne__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bne__nR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bne__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bne__nR___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__nR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bne__nR___rrR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bne__nR__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bne__nR__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bne__nR__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bne__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bne__nR__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == -1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bneqi_mstack__n_m1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bneqi_mstack__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bneqi_mstack__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_bneqi_mstack__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_bneqi_mstack__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n_m1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bneqi_mstack__n_m1__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n_m1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bneqi_mstack__n_m1__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bneqi_mstack__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bneqi_mstack__n0__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bneqi_mstack__n1__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bneqi_mstack__n1__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bneqi_mstack__n2__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bneqi_mstack__n2__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bneqi_mstack__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bneqi_mstack__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bneqi_mstack__nR__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bt_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_bt_mstack__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_bt_mstack__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_bt_mstack__fR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_call (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_call__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_call__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_call__fR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_copy_mto_mr_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_copy_mto_mr_mstack___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_copy_mto_mr_mstack___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_copy_mto_mr_mstack___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_different_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_different_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_divided___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_divided__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_divided___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_divided___rrR__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_divided___rrR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_divided___rrR___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided___rrR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_divided___rrR___rrR___rrR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided___rrR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_divided___rrR__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided___rrR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_divided___rrR__n2___rrR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_divided___rrR__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided___rrR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_divided___rrR__nR___rrR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_divided__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_divided__nR__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_divided__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_divided__nR___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided__nR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_divided__nR___rrR___rrR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_divided__nR__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided__nR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_divided__nR__n2___rrR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_divided__nR__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided__nR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_divided__nR__nR___rrR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_divided_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_divided_mstack__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_drop_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_drop_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_dup_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_dup_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_equal_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_equal_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_equali_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_equali_mstack__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_equali_mstack__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_equali_mstack__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_equali_mstack__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_equali_mstack__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_equali_mstack__n0;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_equali_mstack__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_equali_mstack__n1;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_equali_mstack__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_equali_mstack__n2;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_equali_mstack__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_equali_mstack__nR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_exitvm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_exitvm;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_greater_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_greater_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_greaterorequal_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_greaterorequal_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_input (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_input___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_input___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_input___rrR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_input_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_input_mstack__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_isnonzero_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_isnonzero_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_less_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_less_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_lessorequal_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_lessorequal_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_logicaland_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_logicaland_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_logicalnot_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_logicalnot_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_logicalor_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_logicalor_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_minus___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_minus__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_minus___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_minus___rrR__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_minus___rrR__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_minus___rrR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_minus___rrR___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_minus___rrR___rrR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_minus___rrR__n1___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_minus___rrR__n1___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_minus___rrR__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_minus___rrR__n2___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_minus___rrR__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus___rrR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_minus___rrR__nR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_minus__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_minus__nR__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_minus__nR__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_minus__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_minus__nR___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_minus__nR___rrR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_minus__nR__n1___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_minus__nR__n1___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_minus__nR__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_minus__nR__n2___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_minus__nR__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus__nR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_minus__nR__nR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minus_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_minus_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minusi_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_minusi_mstack__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_minusi_mstack__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_minusi_mstack__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minusi_mstack__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_minusi_mstack__n1;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minusi_mstack__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_minusi_mstack__n2;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_minusi_mstack__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_minusi_mstack__nR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_mov___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_mov__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_mov__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == -1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_mov__n_m1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_mov__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_mov__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_mov___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_mov___rrR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_mov__n0___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n0___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_mov__n0___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_mov__n1___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_mov__n1___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n_m1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_mov__n_m1___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n_m1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_mov__n_m1___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_mov__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_mov__n2___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_mov__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_mov__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_mov__nR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_plus__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_plus__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_plus__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_plus___rrR__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_plus___rrR__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_plus___rrR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus___rrR___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus___rrR___rrR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus___rrR__n1___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus___rrR__n1___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus___rrR__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus___rrR__n2___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus___rrR__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus___rrR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus___rrR__nR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus__n1___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_plus__n1__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_plus__n1__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_plus__n1__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus__n1___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus__n1___rrR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus__n1__n1___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus__n1__n1___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus__n1__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus__n1__n2___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus__n1__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n1__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus__n1__nR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_plus__n2__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_plus__n2__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_plus__n2__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus__n2___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus__n2___rrR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus__n2__n1___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus__n2__n1___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus__n2__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus__n2__n2___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus__n2__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__n2__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus__n2__nR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_plus__nR__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_plus__nR__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_plus__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus__nR___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus__nR___rrR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus__nR__n1___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus__nR__n1___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus__nR__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus__nR__n2___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_plus__nR__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus__nR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus__nR__nR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plus_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plus_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plusi_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == -1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_plusi_mstack__n_m1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_plusi_mstack__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_plusi_mstack__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_plusi_mstack__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plusi_mstack__n_m1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plusi_mstack__n_m1;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plusi_mstack__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plusi_mstack__n1;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plusi_mstack__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plusi_mstack__n2;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_plusi_mstack__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_plusi_mstack__nR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_pop_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_pop_mstack___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_pop_mstack___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_pop_mstack___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_print (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_print___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_print__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_print___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_print___rrR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_print__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_print__nR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_print_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_print_mstack__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_procedure_mprolog (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_procedure_mprolog;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_push_mstack___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_push_mstack__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_push_mstack__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == -1 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_push_mstack__n_m1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_push_mstack__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_push_mstack__nR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = structuredvm_recognize_specialized_instruction_push_mstack__lR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_push_mstack___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_push_mstack__n0;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_push_mstack__n1;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack__n_m1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_push_mstack__n_m1;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_push_mstack__n2;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_push_mstack__nR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_mstack__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_push_mstack__lR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_push_munspecified_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_push_munspecified_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_remainder___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_remainder__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_remainder___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_remainder___rrR__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_remainder___rrR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_remainder___rrR___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder___rrR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_remainder___rrR___rrR___rrR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder___rrR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_remainder___rrR__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder___rrR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_remainder___rrR__n2___rrR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_remainder___rrR__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder___rrR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_remainder___rrR__nR___rrR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_remainder__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_remainder__nR__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_remainder__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_remainder__nR___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder__nR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_remainder__nR___rrR___rrR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_remainder__nR__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder__nR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_remainder__nR__n2___rrR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_remainder__nR__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder__nR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_remainder__nR__nR___rrR__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_remainder_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_remainder_mstack__retR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_return_mto_mundertop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_return_mto_mundertop;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_swap_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_swap_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_times___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_times__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_times___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_times___rrR__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_times___rrR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_times___rrR___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times___rrR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_times___rrR___rrR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times___rrR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_times___rrR__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times___rrR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_times___rrR__n2___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times___rrR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_times___rrR__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times___rrR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_times___rrR__nR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_times__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = structuredvm_recognize_specialized_instruction_times__nR__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_times__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_times__nR___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times__nR___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_times__nR___rrR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_times__nR__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times__nR__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_times__nR__n2___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_times__nR__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times__nR__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_times__nR__nR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_times_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_times_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_uminus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_uminus___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_uminus__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_uminus___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_uminus___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_uminus___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_uminus___rrR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_uminus__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_uminus__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_uminus__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_uminus__nR___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_uminus_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_uminus_mstack;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_underpop_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_underpop_mstack___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_underpop_mstack___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_underpop_mstack___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_underpush_mstack (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum structuredvm_specialized_instruction_opcode res = structuredvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = structuredvm_recognize_specialized_instruction_underpush_mstack___rrR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = structuredvm_recognize_specialized_instruction_underpush_mstack__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_underpush_mstack___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_underpush_mstack___rrR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_underpush_mstack__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_underpush_mstack__nR;
}

inline static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction_unreachable (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return structuredvm_specialized_instruction_opcode_unreachable;
}



/* Recognizer entry point. */
static enum structuredvm_specialized_instruction_opcode
structuredvm_recognize_specialized_instruction (struct jitter_mutable_routine *p,
                                            const struct jitter_instruction *ins)
{
  bool fl = ! p->options.slow_literals_only;
  const struct jitter_meta_instruction *mi = ins->meta_instruction;
  switch (mi->id)
    {
    case structuredvm_meta_instruction_id_b:
      return structuredvm_recognize_specialized_instruction_b (ins->parameters, fl);
    case structuredvm_meta_instruction_id_be:
      return structuredvm_recognize_specialized_instruction_be (ins->parameters, fl);
    case structuredvm_meta_instruction_id_beqi_mstack:
      return structuredvm_recognize_specialized_instruction_beqi_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_bf_mstack:
      return structuredvm_recognize_specialized_instruction_bf_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_bg:
      return structuredvm_recognize_specialized_instruction_bg (ins->parameters, fl);
    case structuredvm_meta_instruction_id_bge:
      return structuredvm_recognize_specialized_instruction_bge (ins->parameters, fl);
    case structuredvm_meta_instruction_id_bl:
      return structuredvm_recognize_specialized_instruction_bl (ins->parameters, fl);
    case structuredvm_meta_instruction_id_ble:
      return structuredvm_recognize_specialized_instruction_ble (ins->parameters, fl);
    case structuredvm_meta_instruction_id_bne:
      return structuredvm_recognize_specialized_instruction_bne (ins->parameters, fl);
    case structuredvm_meta_instruction_id_bneqi_mstack:
      return structuredvm_recognize_specialized_instruction_bneqi_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_bt_mstack:
      return structuredvm_recognize_specialized_instruction_bt_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_call:
      return structuredvm_recognize_specialized_instruction_call (ins->parameters, fl);
    case structuredvm_meta_instruction_id_copy_mto_mr_mstack:
      return structuredvm_recognize_specialized_instruction_copy_mto_mr_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_different_mstack:
      return structuredvm_recognize_specialized_instruction_different_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_divided:
      return structuredvm_recognize_specialized_instruction_divided (ins->parameters, fl);
    case structuredvm_meta_instruction_id_divided_mstack:
      return structuredvm_recognize_specialized_instruction_divided_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_drop_mstack:
      return structuredvm_recognize_specialized_instruction_drop_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_dup_mstack:
      return structuredvm_recognize_specialized_instruction_dup_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_equal_mstack:
      return structuredvm_recognize_specialized_instruction_equal_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_equali_mstack:
      return structuredvm_recognize_specialized_instruction_equali_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_exitvm:
      return structuredvm_recognize_specialized_instruction_exitvm (ins->parameters, fl);
    case structuredvm_meta_instruction_id_greater_mstack:
      return structuredvm_recognize_specialized_instruction_greater_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_greaterorequal_mstack:
      return structuredvm_recognize_specialized_instruction_greaterorequal_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_input:
      return structuredvm_recognize_specialized_instruction_input (ins->parameters, fl);
    case structuredvm_meta_instruction_id_input_mstack:
      return structuredvm_recognize_specialized_instruction_input_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_isnonzero_mstack:
      return structuredvm_recognize_specialized_instruction_isnonzero_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_less_mstack:
      return structuredvm_recognize_specialized_instruction_less_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_lessorequal_mstack:
      return structuredvm_recognize_specialized_instruction_lessorequal_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_logicaland_mstack:
      return structuredvm_recognize_specialized_instruction_logicaland_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_logicalnot_mstack:
      return structuredvm_recognize_specialized_instruction_logicalnot_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_logicalor_mstack:
      return structuredvm_recognize_specialized_instruction_logicalor_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_minus:
      return structuredvm_recognize_specialized_instruction_minus (ins->parameters, fl);
    case structuredvm_meta_instruction_id_minus_mstack:
      return structuredvm_recognize_specialized_instruction_minus_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_minusi_mstack:
      return structuredvm_recognize_specialized_instruction_minusi_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_mov:
      return structuredvm_recognize_specialized_instruction_mov (ins->parameters, fl);
    case structuredvm_meta_instruction_id_plus:
      return structuredvm_recognize_specialized_instruction_plus (ins->parameters, fl);
    case structuredvm_meta_instruction_id_plus_mstack:
      return structuredvm_recognize_specialized_instruction_plus_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_plusi_mstack:
      return structuredvm_recognize_specialized_instruction_plusi_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_pop_mstack:
      return structuredvm_recognize_specialized_instruction_pop_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_print:
      return structuredvm_recognize_specialized_instruction_print (ins->parameters, fl);
    case structuredvm_meta_instruction_id_print_mstack:
      return structuredvm_recognize_specialized_instruction_print_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_procedure_mprolog:
      return structuredvm_recognize_specialized_instruction_procedure_mprolog (ins->parameters, fl);
    case structuredvm_meta_instruction_id_push_mstack:
      return structuredvm_recognize_specialized_instruction_push_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_push_munspecified_mstack:
      return structuredvm_recognize_specialized_instruction_push_munspecified_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_remainder:
      return structuredvm_recognize_specialized_instruction_remainder (ins->parameters, fl);
    case structuredvm_meta_instruction_id_remainder_mstack:
      return structuredvm_recognize_specialized_instruction_remainder_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_return_mto_mundertop:
      return structuredvm_recognize_specialized_instruction_return_mto_mundertop (ins->parameters, fl);
    case structuredvm_meta_instruction_id_swap_mstack:
      return structuredvm_recognize_specialized_instruction_swap_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_times:
      return structuredvm_recognize_specialized_instruction_times (ins->parameters, fl);
    case structuredvm_meta_instruction_id_times_mstack:
      return structuredvm_recognize_specialized_instruction_times_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_uminus:
      return structuredvm_recognize_specialized_instruction_uminus (ins->parameters, fl);
    case structuredvm_meta_instruction_id_uminus_mstack:
      return structuredvm_recognize_specialized_instruction_uminus_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_underpop_mstack:
      return structuredvm_recognize_specialized_instruction_underpop_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_underpush_mstack:
      return structuredvm_recognize_specialized_instruction_underpush_mstack (ins->parameters, fl);
    case structuredvm_meta_instruction_id_unreachable:
      return structuredvm_recognize_specialized_instruction_unreachable (ins->parameters, fl);
    default:
      jitter_fatal ("invalid meta-instruction id %i", (int)mi->id);
    }
  __builtin_unreachable ();
}

/* Specializer entry point: the only non-static function here. */
int
structuredvm_specialize_instruction (struct jitter_mutable_routine *p,
                                 const struct jitter_instruction *ins)
{
  enum structuredvm_specialized_instruction_opcode opcode
    = structuredvm_recognize_specialized_instruction (p, ins);
  if (opcode == structuredvm_specialized_instruction_opcode__eINVALID)
    jitter_fatal ("specialization failed: %s", ins->meta_instruction->name);

#ifdef JITTER_HAVE_DEFECT_REPLACEMENT
  /* Replace the opcode with its non-defective counterpart. */
  opcode = structuredvm_replacement_table [opcode];
#endif // #ifdef JITTER_HAVE_DEFECT_REPLACEMENT

  jitter_add_specialized_instruction_opcode (p, opcode);


  /* FIXME: in the old shell-based generator I grouped specialized instructions by
     their "residual parameter map", yielding a switch with a lot of different
     specialized instructions mapping to the same case.  I should redo that here. */
  switch (opcode)
    {
    /* !INVALID. */
    case structuredvm_specialized_instruction_opcode__eINVALID:
      break;

    /* !BEGINBASICBLOCK. */
    case structuredvm_specialized_instruction_opcode__eBEGINBASICBLOCK:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* !EXITVM. */
    case structuredvm_specialized_instruction_opcode__eEXITVM:
      break;

    /* !DATALOCATIONS. */
    case structuredvm_specialized_instruction_opcode__eDATALOCATIONS:
      break;

    /* !NOP. */
    case structuredvm_specialized_instruction_opcode__eNOP:
      break;

    /* !UNREACHABLE0. */
    case structuredvm_specialized_instruction_opcode__eUNREACHABLE0:
      break;

    /* !UNREACHABLE1. */
    case structuredvm_specialized_instruction_opcode__eUNREACHABLE1:
      break;

    /* !PRETENDTOJUMPANYWHERE. */
    case structuredvm_specialized_instruction_opcode__ePRETENDTOJUMPANYWHERE:
      break;

    /* b/fR. */
    case structuredvm_specialized_instruction_opcode_b__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* be/%rR/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_be___rrR___rrR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* be/%rR/n0/fR. */
    case structuredvm_specialized_instruction_opcode_be___rrR__n0__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* be/%rR/nR/fR. */
    case structuredvm_specialized_instruction_opcode_be___rrR__nR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* be/n0/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_be__n0___rrR__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* be/n0/n0/fR. */
    case structuredvm_specialized_instruction_opcode_be__n0__n0__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* be/n0/nR/fR. */
    case structuredvm_specialized_instruction_opcode_be__n0__nR__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* be/nR/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_be__nR___rrR__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* be/nR/n0/fR. */
    case structuredvm_specialized_instruction_opcode_be__nR__n0__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* be/nR/nR/fR. */
    case structuredvm_specialized_instruction_opcode_be__nR__nR__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* beqi-stack/n-1/fR. */
    case structuredvm_specialized_instruction_opcode_beqi_mstack__n_m1__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* beqi-stack/n0/fR. */
    case structuredvm_specialized_instruction_opcode_beqi_mstack__n0__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* beqi-stack/n1/fR. */
    case structuredvm_specialized_instruction_opcode_beqi_mstack__n1__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* beqi-stack/n2/fR. */
    case structuredvm_specialized_instruction_opcode_beqi_mstack__n2__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* beqi-stack/nR/fR. */
    case structuredvm_specialized_instruction_opcode_beqi_mstack__nR__fR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* bf-stack/fR. */
    case structuredvm_specialized_instruction_opcode_bf_mstack__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* bg/%rR/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_bg___rrR___rrR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bg/%rR/n0/fR. */
    case structuredvm_specialized_instruction_opcode_bg___rrR__n0__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bg/%rR/nR/fR. */
    case structuredvm_specialized_instruction_opcode_bg___rrR__nR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bg/n0/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_bg__n0___rrR__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bg/n0/n0/fR. */
    case structuredvm_specialized_instruction_opcode_bg__n0__n0__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bg/n0/nR/fR. */
    case structuredvm_specialized_instruction_opcode_bg__n0__nR__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bg/nR/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_bg__nR___rrR__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bg/nR/n0/fR. */
    case structuredvm_specialized_instruction_opcode_bg__nR__n0__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bg/nR/nR/fR. */
    case structuredvm_specialized_instruction_opcode_bg__nR__nR__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bge/%rR/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_bge___rrR___rrR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bge/%rR/n0/fR. */
    case structuredvm_specialized_instruction_opcode_bge___rrR__n0__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bge/%rR/nR/fR. */
    case structuredvm_specialized_instruction_opcode_bge___rrR__nR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bge/n0/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_bge__n0___rrR__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bge/n0/n0/fR. */
    case structuredvm_specialized_instruction_opcode_bge__n0__n0__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bge/n0/nR/fR. */
    case structuredvm_specialized_instruction_opcode_bge__n0__nR__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bge/nR/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_bge__nR___rrR__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bge/nR/n0/fR. */
    case structuredvm_specialized_instruction_opcode_bge__nR__n0__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bge/nR/nR/fR. */
    case structuredvm_specialized_instruction_opcode_bge__nR__nR__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bl/%rR/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_bl___rrR___rrR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bl/%rR/n0/fR. */
    case structuredvm_specialized_instruction_opcode_bl___rrR__n0__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bl/%rR/nR/fR. */
    case structuredvm_specialized_instruction_opcode_bl___rrR__nR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bl/n0/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_bl__n0___rrR__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bl/n0/n0/fR. */
    case structuredvm_specialized_instruction_opcode_bl__n0__n0__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bl/n0/nR/fR. */
    case structuredvm_specialized_instruction_opcode_bl__n0__nR__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bl/nR/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_bl__nR___rrR__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bl/nR/n0/fR. */
    case structuredvm_specialized_instruction_opcode_bl__nR__n0__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bl/nR/nR/fR. */
    case structuredvm_specialized_instruction_opcode_bl__nR__nR__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* ble/%rR/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_ble___rrR___rrR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* ble/%rR/n0/fR. */
    case structuredvm_specialized_instruction_opcode_ble___rrR__n0__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* ble/%rR/nR/fR. */
    case structuredvm_specialized_instruction_opcode_ble___rrR__nR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* ble/n0/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_ble__n0___rrR__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* ble/n0/n0/fR. */
    case structuredvm_specialized_instruction_opcode_ble__n0__n0__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* ble/n0/nR/fR. */
    case structuredvm_specialized_instruction_opcode_ble__n0__nR__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* ble/nR/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_ble__nR___rrR__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* ble/nR/n0/fR. */
    case structuredvm_specialized_instruction_opcode_ble__nR__n0__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* ble/nR/nR/fR. */
    case structuredvm_specialized_instruction_opcode_ble__nR__nR__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bne/%rR/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_bne___rrR___rrR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bne/%rR/n0/fR. */
    case structuredvm_specialized_instruction_opcode_bne___rrR__n0__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bne/%rR/nR/fR. */
    case structuredvm_specialized_instruction_opcode_bne___rrR__nR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bne/n0/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_bne__n0___rrR__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bne/n0/n0/fR. */
    case structuredvm_specialized_instruction_opcode_bne__n0__n0__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bne/n0/nR/fR. */
    case structuredvm_specialized_instruction_opcode_bne__n0__nR__fR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bne/nR/%rR/fR. */
    case structuredvm_specialized_instruction_opcode_bne__nR___rrR__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bne/nR/n0/fR. */
    case structuredvm_specialized_instruction_opcode_bne__nR__n0__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bne/nR/nR/fR. */
    case structuredvm_specialized_instruction_opcode_bne__nR__nR__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* bneqi-stack/n-1/fR. */
    case structuredvm_specialized_instruction_opcode_bneqi_mstack__n_m1__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* bneqi-stack/n0/fR. */
    case structuredvm_specialized_instruction_opcode_bneqi_mstack__n0__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* bneqi-stack/n1/fR. */
    case structuredvm_specialized_instruction_opcode_bneqi_mstack__n1__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* bneqi-stack/n2/fR. */
    case structuredvm_specialized_instruction_opcode_bneqi_mstack__n2__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* bneqi-stack/nR/fR. */
    case structuredvm_specialized_instruction_opcode_bneqi_mstack__nR__fR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* bt-stack/fR. */
    case structuredvm_specialized_instruction_opcode_bt_mstack__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* call/fR/retR. */
    case structuredvm_specialized_instruction_opcode_call__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* copy-to-r-stack/%rR. */
    case structuredvm_specialized_instruction_opcode_copy_mto_mr_mstack___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* different-stack. */
    case structuredvm_specialized_instruction_opcode_different_mstack:
      break;

    /* divided/%rR/%rR/%rR/retR. */
    case structuredvm_specialized_instruction_opcode_divided___rrR___rrR___rrR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* divided/%rR/n2/%rR/retR. */
    case structuredvm_specialized_instruction_opcode_divided___rrR__n2___rrR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* divided/%rR/nR/%rR/retR. */
    case structuredvm_specialized_instruction_opcode_divided___rrR__nR___rrR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* divided/nR/%rR/%rR/retR. */
    case structuredvm_specialized_instruction_opcode_divided__nR___rrR___rrR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* divided/nR/n2/%rR/retR. */
    case structuredvm_specialized_instruction_opcode_divided__nR__n2___rrR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* divided/nR/nR/%rR/retR. */
    case structuredvm_specialized_instruction_opcode_divided__nR__nR___rrR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* divided-stack/retR. */
    case structuredvm_specialized_instruction_opcode_divided_mstack__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* drop-stack. */
    case structuredvm_specialized_instruction_opcode_drop_mstack:
      break;

    /* dup-stack. */
    case structuredvm_specialized_instruction_opcode_dup_mstack:
      break;

    /* equal-stack. */
    case structuredvm_specialized_instruction_opcode_equal_mstack:
      break;

    /* equali-stack/n0. */
    case structuredvm_specialized_instruction_opcode_equali_mstack__n0:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* equali-stack/n1. */
    case structuredvm_specialized_instruction_opcode_equali_mstack__n1:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* equali-stack/n2. */
    case structuredvm_specialized_instruction_opcode_equali_mstack__n2:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* equali-stack/nR. */
    case structuredvm_specialized_instruction_opcode_equali_mstack__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* exitvm. */
    case structuredvm_specialized_instruction_opcode_exitvm:
      break;

    /* greater-stack. */
    case structuredvm_specialized_instruction_opcode_greater_mstack:
      break;

    /* greaterorequal-stack. */
    case structuredvm_specialized_instruction_opcode_greaterorequal_mstack:
      break;

    /* input/%rR/retR. */
    case structuredvm_specialized_instruction_opcode_input___rrR__retR:
      /* j:0 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* input-stack/retR. */
    case structuredvm_specialized_instruction_opcode_input_mstack__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* isnonzero-stack. */
    case structuredvm_specialized_instruction_opcode_isnonzero_mstack:
      break;

    /* less-stack. */
    case structuredvm_specialized_instruction_opcode_less_mstack:
      break;

    /* lessorequal-stack. */
    case structuredvm_specialized_instruction_opcode_lessorequal_mstack:
      break;

    /* logicaland-stack. */
    case structuredvm_specialized_instruction_opcode_logicaland_mstack:
      break;

    /* logicalnot-stack. */
    case structuredvm_specialized_instruction_opcode_logicalnot_mstack:
      break;

    /* logicalor-stack. */
    case structuredvm_specialized_instruction_opcode_logicalor_mstack:
      break;

    /* minus/%rR/%rR/%rR. */
    case structuredvm_specialized_instruction_opcode_minus___rrR___rrR___rrR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* minus/%rR/n1/%rR. */
    case structuredvm_specialized_instruction_opcode_minus___rrR__n1___rrR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* minus/%rR/n2/%rR. */
    case structuredvm_specialized_instruction_opcode_minus___rrR__n2___rrR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* minus/%rR/nR/%rR. */
    case structuredvm_specialized_instruction_opcode_minus___rrR__nR___rrR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* minus/nR/%rR/%rR. */
    case structuredvm_specialized_instruction_opcode_minus__nR___rrR___rrR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* minus/nR/n1/%rR. */
    case structuredvm_specialized_instruction_opcode_minus__nR__n1___rrR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* minus/nR/n2/%rR. */
    case structuredvm_specialized_instruction_opcode_minus__nR__n2___rrR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* minus/nR/nR/%rR. */
    case structuredvm_specialized_instruction_opcode_minus__nR__nR___rrR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* minus-stack. */
    case structuredvm_specialized_instruction_opcode_minus_mstack:
      break;

    /* minusi-stack/n1. */
    case structuredvm_specialized_instruction_opcode_minusi_mstack__n1:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* minusi-stack/n2. */
    case structuredvm_specialized_instruction_opcode_minusi_mstack__n2:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* minusi-stack/nR. */
    case structuredvm_specialized_instruction_opcode_minusi_mstack__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* mov/%rR/%rR. */
    case structuredvm_specialized_instruction_opcode_mov___rrR___rrR:
      /* j:0 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* mov/n0/%rR. */
    case structuredvm_specialized_instruction_opcode_mov__n0___rrR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* mov/n1/%rR. */
    case structuredvm_specialized_instruction_opcode_mov__n1___rrR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* mov/n-1/%rR. */
    case structuredvm_specialized_instruction_opcode_mov__n_m1___rrR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* mov/n2/%rR. */
    case structuredvm_specialized_instruction_opcode_mov__n2___rrR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* mov/nR/%rR. */
    case structuredvm_specialized_instruction_opcode_mov__nR___rrR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* plus/%rR/%rR/%rR. */
    case structuredvm_specialized_instruction_opcode_plus___rrR___rrR___rrR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus/%rR/n1/%rR. */
    case structuredvm_specialized_instruction_opcode_plus___rrR__n1___rrR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus/%rR/n2/%rR. */
    case structuredvm_specialized_instruction_opcode_plus___rrR__n2___rrR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus/%rR/nR/%rR. */
    case structuredvm_specialized_instruction_opcode_plus___rrR__nR___rrR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus/n1/%rR/%rR. */
    case structuredvm_specialized_instruction_opcode_plus__n1___rrR___rrR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus/n1/n1/%rR. */
    case structuredvm_specialized_instruction_opcode_plus__n1__n1___rrR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus/n1/n2/%rR. */
    case structuredvm_specialized_instruction_opcode_plus__n1__n2___rrR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus/n1/nR/%rR. */
    case structuredvm_specialized_instruction_opcode_plus__n1__nR___rrR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus/n2/%rR/%rR. */
    case structuredvm_specialized_instruction_opcode_plus__n2___rrR___rrR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus/n2/n1/%rR. */
    case structuredvm_specialized_instruction_opcode_plus__n2__n1___rrR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus/n2/n2/%rR. */
    case structuredvm_specialized_instruction_opcode_plus__n2__n2___rrR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus/n2/nR/%rR. */
    case structuredvm_specialized_instruction_opcode_plus__n2__nR___rrR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus/nR/%rR/%rR. */
    case structuredvm_specialized_instruction_opcode_plus__nR___rrR___rrR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus/nR/n1/%rR. */
    case structuredvm_specialized_instruction_opcode_plus__nR__n1___rrR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus/nR/n2/%rR. */
    case structuredvm_specialized_instruction_opcode_plus__nR__n2___rrR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus/nR/nR/%rR. */
    case structuredvm_specialized_instruction_opcode_plus__nR__nR___rrR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* plus-stack. */
    case structuredvm_specialized_instruction_opcode_plus_mstack:
      break;

    /* plusi-stack/n-1. */
    case structuredvm_specialized_instruction_opcode_plusi_mstack__n_m1:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* plusi-stack/n1. */
    case structuredvm_specialized_instruction_opcode_plusi_mstack__n1:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* plusi-stack/n2. */
    case structuredvm_specialized_instruction_opcode_plusi_mstack__n2:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* plusi-stack/nR. */
    case structuredvm_specialized_instruction_opcode_plusi_mstack__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* pop-stack/%rR. */
    case structuredvm_specialized_instruction_opcode_pop_mstack___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* print/%rR/retR. */
    case structuredvm_specialized_instruction_opcode_print___rrR__retR:
      /* j:0 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* print/nR/retR. */
    case structuredvm_specialized_instruction_opcode_print__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* print-stack/retR. */
    case structuredvm_specialized_instruction_opcode_print_mstack__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* procedure-prolog. */
    case structuredvm_specialized_instruction_opcode_procedure_mprolog:
      /* This is a callee instruction. */
      break;

    /* push-stack/%rR. */
    case structuredvm_specialized_instruction_opcode_push_mstack___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* push-stack/n0. */
    case structuredvm_specialized_instruction_opcode_push_mstack__n0:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* push-stack/n1. */
    case structuredvm_specialized_instruction_opcode_push_mstack__n1:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* push-stack/n-1. */
    case structuredvm_specialized_instruction_opcode_push_mstack__n_m1:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* push-stack/n2. */
    case structuredvm_specialized_instruction_opcode_push_mstack__n2:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* push-stack/nR. */
    case structuredvm_specialized_instruction_opcode_push_mstack__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* push-stack/lR. */
    case structuredvm_specialized_instruction_opcode_push_mstack__lR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* push-unspecified-stack. */
    case structuredvm_specialized_instruction_opcode_push_munspecified_mstack:
      break;

    /* remainder/%rR/%rR/%rR/retR. */
    case structuredvm_specialized_instruction_opcode_remainder___rrR___rrR___rrR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* remainder/%rR/n2/%rR/retR. */
    case structuredvm_specialized_instruction_opcode_remainder___rrR__n2___rrR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* remainder/%rR/nR/%rR/retR. */
    case structuredvm_specialized_instruction_opcode_remainder___rrR__nR___rrR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* remainder/nR/%rR/%rR/retR. */
    case structuredvm_specialized_instruction_opcode_remainder__nR___rrR___rrR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* remainder/nR/n2/%rR/retR. */
    case structuredvm_specialized_instruction_opcode_remainder__nR__n2___rrR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* remainder/nR/nR/%rR/retR. */
    case structuredvm_specialized_instruction_opcode_remainder__nR__nR___rrR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* remainder-stack/retR. */
    case structuredvm_specialized_instruction_opcode_remainder_mstack__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* return-to-undertop. */
    case structuredvm_specialized_instruction_opcode_return_mto_mundertop:
      break;

    /* swap-stack. */
    case structuredvm_specialized_instruction_opcode_swap_mstack:
      break;

    /* times/%rR/%rR/%rR. */
    case structuredvm_specialized_instruction_opcode_times___rrR___rrR___rrR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* times/%rR/n2/%rR. */
    case structuredvm_specialized_instruction_opcode_times___rrR__n2___rrR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* times/%rR/nR/%rR. */
    case structuredvm_specialized_instruction_opcode_times___rrR__nR___rrR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* times/nR/%rR/%rR. */
    case structuredvm_specialized_instruction_opcode_times__nR___rrR___rrR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* times/nR/n2/%rR. */
    case structuredvm_specialized_instruction_opcode_times__nR__n2___rrR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* times/nR/nR/%rR. */
    case structuredvm_specialized_instruction_opcode_times__nR__nR___rrR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[2]->register_index));
      break;

    /* times-stack. */
    case structuredvm_specialized_instruction_opcode_times_mstack:
      break;

    /* uminus/%rR/%rR. */
    case structuredvm_specialized_instruction_opcode_uminus___rrR___rrR:
      /* j:0 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* uminus/nR/%rR. */
    case structuredvm_specialized_instruction_opcode_uminus__nR___rrR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* uminus-stack. */
    case structuredvm_specialized_instruction_opcode_uminus_mstack:
      break;

    /* underpop-stack/%rR. */
    case structuredvm_specialized_instruction_opcode_underpop_mstack___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* underpush-stack/%rR. */
    case structuredvm_specialized_instruction_opcode_underpush_mstack___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* underpush-stack/nR. */
    case structuredvm_specialized_instruction_opcode_underpush_mstack__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* unreachable. */
    case structuredvm_specialized_instruction_opcode_unreachable:
      break;

    /* !REPLACEMENT-b/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mb__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-be/%rR/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe___rrR___rrR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-be/%rR/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe___rrR__n0__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-be/%rR/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe___rrR__nR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-be/n0/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__n0___rrR__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-be/n0/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__n0__n0__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-be/n0/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__n0__nR__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-be/nR/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__nR___rrR__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-be/nR/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__nR__n0__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-be/nR/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbe__nR__nR__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-beqi-stack/n-1/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbeqi_mstack__n_m1__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-beqi-stack/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbeqi_mstack__n0__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-beqi-stack/n1/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbeqi_mstack__n1__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-beqi-stack/n2/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbeqi_mstack__n2__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-beqi-stack/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbeqi_mstack__nR__fR__retR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bf-stack/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbf_mstack__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bg/%rR/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg___rrR___rrR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bg/%rR/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg___rrR__n0__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bg/%rR/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg___rrR__nR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bg/n0/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__n0___rrR__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bg/n0/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__n0__n0__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bg/n0/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__n0__nR__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bg/nR/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__nR___rrR__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bg/nR/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__nR__n0__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bg/nR/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbg__nR__nR__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bge/%rR/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge___rrR___rrR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bge/%rR/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge___rrR__n0__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bge/%rR/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge___rrR__nR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bge/n0/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__n0___rrR__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bge/n0/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__n0__n0__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bge/n0/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__n0__nR__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bge/nR/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__nR___rrR__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bge/nR/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__nR__n0__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bge/nR/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbge__nR__nR__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bl/%rR/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl___rrR___rrR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bl/%rR/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl___rrR__n0__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bl/%rR/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl___rrR__nR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bl/n0/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__n0___rrR__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bl/n0/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__n0__n0__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bl/n0/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__n0__nR__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bl/nR/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__nR___rrR__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bl/nR/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__nR__n0__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bl/nR/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbl__nR__nR__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-ble/%rR/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble___rrR___rrR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-ble/%rR/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble___rrR__n0__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-ble/%rR/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble___rrR__nR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-ble/n0/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__n0___rrR__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-ble/n0/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__n0__n0__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-ble/n0/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__n0__nR__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-ble/nR/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__nR___rrR__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-ble/nR/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__nR__n0__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-ble/nR/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mble__nR__nR__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bne/%rR/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne___rrR___rrR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bne/%rR/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne___rrR__n0__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bne/%rR/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne___rrR__nR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bne/n0/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__n0___rrR__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bne/n0/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__n0__n0__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bne/n0/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__n0__nR__fR__retR:
      /* j:0 residual_no:4 */
      /* Argument 0 (0-based, of 4) is non-residual */
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bne/nR/%rR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__nR___rrR__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, STRUCTUREDVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bne/nR/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__nR__n0__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bne/nR/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbne__nR__nR__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bneqi-stack/n-1/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbneqi_mstack__n_m1__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bneqi-stack/n0/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbneqi_mstack__n0__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bneqi-stack/n1/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbneqi_mstack__n1__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bneqi-stack/n2/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbneqi_mstack__n2__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bneqi-stack/nR/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbneqi_mstack__nR__fR__retR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bt-stack/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mbt_mstack__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-call/fR/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mcall__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-exitvm/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mexitvm__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-procedure-prolog/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mprocedure_mprolog__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a callee instruction. */
      break;

    /* !REPLACEMENT-return-to-undertop/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_mreturn_mto_mundertop__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-unreachable/retR. */
    case structuredvm_specialized_instruction_opcode__eREPLACEMENT_munreachable__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    default:
      jitter_fatal ("invalid specialized instruction opcode %i", (int)opcode);
    }
  return 1; // FIXME: I should rethink this return value.
}

void
structuredvm_state_initialize_with_slow_registers
   (struct structuredvm_state *jitter_state,
    jitter_uint jitter_slow_register_no_per_class)
{
  struct structuredvm_state_backing * const jitter_state_backing
    __attribute__ ((unused))
    = & jitter_state->structuredvm_state_backing;
  struct structuredvm_state_runtime * const jitter_state_runtime
    __attribute__ ((unused))
    = & jitter_state->_structuredvm_yQcCd7rlyo_state_runtime;

  /* Initialize The Array. */
  jitter_state_backing->jitter_slow_register_no_per_class
    = jitter_slow_register_no_per_class;
  jitter_state_backing->jitter_array
    = jitter_xmalloc (STRUCTUREDVM_ARRAY_SIZE(jitter_state_backing
                         ->jitter_slow_register_no_per_class));

  /* Initialize special-purpose data. */
  structuredvm_initialize_special_purpose_data (STRUCTUREDVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA (jitter_state_backing->jitter_array));

  /* Set the initial program point to an invalid value, for defensiveness. */
  jitter_state_backing->initial_program_point = NULL;

  /* Set the initial exit status. */
  jitter_state_backing->exit_status
    = structuredvm_exit_status_never_executed;

  /* Initialize stack backing and stack runtime data structures, pointing
     to memory from the backings. */
  /* Make the stack backing for mainstack . */
  jitter_stack_initialize_tos_backing(& jitter_state_backing->jitter_stack_mainstack_backing,
                                      sizeof (jitter_int),
                                      8192,
                                      NULL,
                                      1,
                                      1);
  JITTER_STACK_TOS_INITIALIZE(jitter_int, jitter_state_runtime-> ,
                              mainstack, jitter_state_backing->jitter_stack_mainstack_backing);

  /* Initialise the link register. */
  jitter_state_runtime->_jitter_link.label = NULL;

  /* Initialise r-class fast registers. */

  /* Initialise slow registers. */
  structuredvm_initialize_slow_registers
     (jitter_state->structuredvm_state_backing.jitter_array,
      0 /* overwrite any already existing rank */,
      jitter_state_backing->jitter_slow_register_no_per_class);

  /* Link this new state to the list of states. */
  JITTER_LIST_LINK_LAST (structuredvm_state, links, & structuredvm_vm->states, jitter_state);

  /* User code for state initialization. */

  /* End of the user code for state initialization. */

}

void
structuredvm_state_reset
   (struct structuredvm_state *jitter_state)
{
  struct structuredvm_state_backing * const jitter_state_backing
    __attribute__ ((unused))
    = & jitter_state->structuredvm_state_backing;
  struct structuredvm_state_runtime * const jitter_state_runtime
    __attribute__ ((unused))
    = & jitter_state->_structuredvm_yQcCd7rlyo_state_runtime;

  /* No need to touch The Array, which already exists. */
  /* No need to touch special-purpose data, which already exist. */

  /* Set the initial program point to an invalid value, for defensiveness. */
  jitter_state_backing->initial_program_point = NULL;

  /* Set the initial exit status. */
  jitter_state_backing->exit_status
    = structuredvm_exit_status_never_executed;

  /* Initialize stack backing and stack runtime data structures, pointing
     to memory from the backings. */
  /* The stack backing for mainstack already exists, and does
     not require element initialisation. */
  JITTER_STACK_TOS_INITIALIZE(jitter_int, jitter_state_runtime-> ,
                              mainstack, jitter_state_backing->jitter_stack_mainstack_backing);

  /* Initialise the link register. */
  jitter_state_runtime->_jitter_link.label = NULL;

  /* Initialise r-class fast registers. */

  /* Initialise slow registers. */
  structuredvm_initialize_slow_registers
     (jitter_state->structuredvm_state_backing.jitter_array,
      0 /* overwrite any already existing rank */,
      jitter_state_backing->jitter_slow_register_no_per_class);


  /* No need to touch links within the global list of states:
     this state already exists and is already linked. */

  /* The user supplied no explicit code for state reset: use
     finalisation code followed by initialisation code. */
/* User finalisation. */
{

}
/* User Initialisation. */{

}
  /* End of the user code for state finalisation followed by
     initialisation. */
}

void
structuredvm_state_finalize (struct structuredvm_state *jitter_state)
{
  struct structuredvm_state_backing * const jitter_state_backing
    __attribute__ ((unused))
    = & jitter_state->structuredvm_state_backing;
  struct structuredvm_state_runtime * const jitter_state_runtime
    __attribute__ ((unused))
    = & jitter_state->_structuredvm_yQcCd7rlyo_state_runtime;

  /* User code for state finalization. */

  /* End of the user code for state finalization. */

  /* Finalize stack backings -- There is no need to finalize the stack
     runtime data structures, as they hold no heap data of their own. */
  jitter_stack_finalize_backing (& jitter_state_backing->jitter_stack_mainstack_backing);


  /* Unlink this state from the list of states. */
  JITTER_LIST_UNLINK (structuredvm_state, links, & structuredvm_vm->states, jitter_state);

  /* Finalize special-purpose data. */
  structuredvm_finalize_special_purpose_data (STRUCTUREDVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA (jitter_state_backing->jitter_array));

  /* Finalize the Array. */
  free ((void *) jitter_state_backing->jitter_array);

}

