/* This machine-generated file includes source code from GNU Jitter.

   Copyright (C) 2016-2021 Luca Saiu
   Written by Luca Saiu

   GNU Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   GNU Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Jitter.  If not, see <https://www.gnu.org/licenses/>.


  Copyright (C) 2017, 2018, 2020, 2021  Luca Saiu
  Written by Luca Saiu

  This file is part of JitterLisp, distributed as an example along with
  GNU Jitter under the same license.

  JitterLisp is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  JitterLisp is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with JitterLisp.  If not, see <https://www.gnu.org/licenses/>.

*/

/* User-specified code, initial vm1 part: beginning. */

/* User-specified code, initial vm1 part: end */

/* VM library: main VM C file template.

   Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021 Luca Saiu
   Written by Luca Saiu

   This file is part of GNU Jitter.

   GNU Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GNU Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Jitter.  If not, see <https://www.gnu.org/licenses/>. */


/* Generated file warning.
 * ************************************************************************** */

/* Unless this file is named exactly "vm1.c" , without any prefix, you are
   looking at a machine-generated derived file.  The original source is the vm.c
   template from Jitter, with added code implementing the jitterlispvm VM. */




#include <jitter/jitter-early-header.h>

/* When we are using Gnulib the standard header files included below will in
   fact be Gnulib replacements; make sure that the Gnulib macros are
   recognised.
   It is in fact possible that  HAVE_CONFIG_H  is defined even in other
   contexts; it should be harmless to include config.h anyway. */
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif // #ifdef HAVE_CONFIG_H

#include <assert.h>
#include <string.h>

#include <jitter/jitter.h>

#if defined (JITTERLISPVM_PROFILE_SAMPLE)
#include <sys/time.h>
#endif // #if defined (JITTERLISPVM_PROFILE_SAMPLE)

#include <jitter/jitter-hash.h>
#include <jitter/jitter-instruction.h>
#include <jitter/jitter-mmap.h>
#include <jitter/jitter-mutable-routine.h>
#include <jitter/jitter-print.h>
#include <jitter/jitter-rewrite.h>
#include <jitter/jitter-routine.h>
#include <jitter/jitter-routine-parser.h>
#include <jitter/jitter-specialize.h>
#include <jitter/jitter-defect.h>
#include <jitter/jitter-patch-in.h>

/* I don't need to include <jitter/jitter-executor.h> here, nor to define
   JITTER_THIS_CAN_INCLUDE_JITTER_EXECUTOR_H ; doing so carelessly might
   lead to subtle bugs, that it is better to prevent.
   Of course I can reconsider this decision in the future. */

#include <jitter/jitter-data-locations.h>

#include "jitterlispvm-vm.h"
//#include "jitterlispvm-specialized-instructions.h"
//#include "jitterlispvm-meta-instructions.h"
#include <jitter/jitter-fatal.h>




/* Check requirements for particular features.
 * ************************************************************************** */

/* VM sample-profiling is only supported with GCC.  Do not bother activating it
   with other compilers, when the numbers would turn out to be unreliable in the
   end. */
#if  defined (JITTERLISPVM_PROFILE_SAMPLE)        \
     && ! defined (JITTER_HAVE_ACTUAL_GCC)
# error "Sample-profiling is only supported with GCC: it requires (machine-independent)"
# error "GNU C extended asm.  It is not worth supporting other compilers if"
# error "the numbers turn out to be unreliable in the end."
#endif

/* Warn about the unreliability of sample-profiling with simple dispatches
   unless one of the complex dispatches is in use. */
#if  defined (JITTERLISPVM_PROFILE_SAMPLE)                 \
     && ! defined (JITTER_DISPATCH_MINIMAL_THREADING)  \
     && ! defined (JITTER_DISPATCH_NO_THREADING)
# warning "Sample-profiling is unreliable with simple dispatches: the sample"
# warning "incrementation code can be executed at any point in the VM"
# warning "instruction, not necessarily at the same point (the beginning) for"
# warning "every VM instruction."
#endif




/* Machine-generated data structures.
 * ************************************************************************** */

/* Machine-generated data structures defining this VM.  Initializing a static
   struct is problematic, as it requires constant expressions for each field --
   and const pointers don't qualify.  This is why we initialize the struct
   fields below in jitterlispvm_initialize. */
static struct jitter_vm
the_jitterlispvm_vm;

struct jitter_vm * const
jitterlispvm_vm = & the_jitterlispvm_vm;

struct jitter_list_header * const
jitterlispvm_states = & the_jitterlispvm_vm.states;

/* It is convenient to have this initialised at start up, even before calling
   any initialisation function.  This makes it reliable to read this when, for
   example, handling --version . */
static const struct jitter_vm_configuration
jitterlispvm_vm_the_configuration
  = {
      JITTERLISPVM_LOWER_CASE_PREFIX /* lower_case_prefix */,
      JITTERLISPVM_UPPER_CASE_PREFIX /* upper_case_prefix */,
      JITTERLISPVM_HASH_PREFIX /* hash_prefix */,
      JITTERLISPVM_MAX_FAST_REGISTER_NO_PER_CLASS
        /* max_fast_register_no_per_class */,
      JITTERLISPVM_MAX_NONRESIDUAL_LITERAL_NO /* max_nonresidual_literal_no */,
      JITTERLISPVM_DISPATCH_HUMAN_READABLE /* dispatch_human_readable */,
      /* The instrumentation field can be seen as a bit map.  See the comment
         in jitter/jitter-vm.h . */
      (jitter_vm_instrumentation_none
#if defined (JITTERLISPVM_PROFILE_COUNT)
       | jitter_vm_instrumentation_count
#endif
#if defined (JITTERLISPVM_PROFILE_SAMPLE)
       | jitter_vm_instrumentation_sample
#endif
       ) /* instrumentation */
    };

const struct jitter_vm_configuration * const
jitterlispvm_vm_configuration
  = & jitterlispvm_vm_the_configuration;




/* Initialization and finalization: internal functions, not for the user.
 * ************************************************************************** */

/* Initialize threads.  This only needs to be called once at initialization, and
   the user doesn't need to bother with it.  Defined along with the executor. */
void
jitterlispvm_initialize_threads (void);

/* Check that the encodings in enum jitter_specialized_instruction_opcode (as
   used in the specializer) are coherent with machine-generated code.  Making a
   mistake here would introduce subtle bugs, so it's better to be defensive. */
static void
jitterlispvm_check_specialized_instruction_opcode_once (void)
{
  static bool already_checked = false;
  if (already_checked)
    return;

  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__eINVALID)
          == jitter_specialized_instruction_opcode_INVALID);
  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__eBEGINBASICBLOCK)
          == jitter_specialized_instruction_opcode_BEGINBASICBLOCK);
  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__eEXITVM)
          == jitter_specialized_instruction_opcode_EXITVM);
  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__eDATALOCATIONS)
          == jitter_specialized_instruction_opcode_DATALOCATIONS);
  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__eNOP)
          == jitter_specialized_instruction_opcode_NOP);
  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__eUNREACHABLE0)
          == jitter_specialized_instruction_opcode_UNREACHABLE0);
  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__eUNREACHABLE1)
          == jitter_specialized_instruction_opcode_UNREACHABLE1);
  assert (((enum jitter_specialized_instruction_opcode)
           jitterlispvm_specialized_instruction_opcode__ePRETENDTOJUMPANYWHERE)
          == jitter_specialized_instruction_opcode_PRETENDTOJUMPANYWHERE);

  already_checked = true;
}

/* A prototype for a machine-generated function not needing a public
   declaration, only called thru a pointer within struct jitter_vm . */
int
jitterlispvm_specialize_instruction (struct jitter_mutable_routine *p,
                                 const struct jitter_instruction *ins);

/* Initialize the pointed special-purpose data structure. */
static void
jitterlispvm_initialize_special_purpose_data
   (volatile struct jitter_special_purpose_state_data *d)
{
  d->pending_notifications = 0;
  jitter_initialize_pending_signal_notifications
     (& d->pending_signal_notifications);

  /* Initialise profiling fields. */
  jitter_profile_runtime_initialize (jitterlispvm_vm,
                                     (struct jitter_profile_runtime *)
                                     & d->profile_runtime);
}

/* Finalize the pointed special-purpose data structure. */
static void
jitterlispvm_finalize_special_purpose_data
   (volatile struct jitter_special_purpose_state_data *d)
{
  jitter_finalize_pending_signal_notifications
     (d->pending_signal_notifications);

  jitter_profile_runtime_finalize (jitterlispvm_vm,
                                   (struct jitter_profile_runtime *)
                                   & d->profile_runtime);
}




/* Check that we link with the correct Jitter library.
 * ************************************************************************** */

/* It is possible to make a mistake at link time, and link a VM compiled with
   some dispatch with the Jitter runtime for a different dispatch.  That
   would cause crashes that is better to prevent.  This is a way to detect such
   mistakes very early, by causing a link-time failure in case of mismatch. */
extern volatile const bool
JITTER_DISPATCH_DEPENDENT_GLOBAL_NAME;




/* Low-level debugging features relying on assembly: data locations.
 * ************************************************************************** */

#if defined (JITTER_HAVE_KNOWN_BINARY_FORMAT) && ! defined (JITTER_DISPATCH_SWITCH)
/* A declaration for data locations, as visible from C.  The global is defined in
   assembly in its own separate section thru the machinery in
   jitter/jitter-sections.h . */
extern const char
JITTER_DATA_LOCATION_NAME(jitterlispvm) [];
#endif // #if ...

void
jitterlispvm_dump_data_locations (jitter_print_context output)
{
#ifndef JITTER_DISPATCH_SWITCH
  jitter_dump_data_locations (output, & the_jitterlispvm_vm);
#else
  jitter_print_char_star (output,
                          "VM data location information unavailable\n");
#endif // #ifndef JITTER_DISPATCH_SWITCH
}




/* Initialization and finalization.
 * ************************************************************************** */

#if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
JITTER_DEFECT_DESCRIPTOR_DECLARATIONS_(jitterlispvm)

/* This global is defined from C: there is no particular need of doing it in
   assembly.  It is initialised in jitterlispvm_execute_or_initialize , where C
   labels are visible. */ // FIXME: unless it turns out to be simpler in assembly ...
jitter_int
jitterlispvm_defect_descriptors_correct_displacement;
#endif // #if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
#if defined (JITTER_HAVE_PATCH_IN)
JITTER_PATCH_IN_DESCRIPTOR_DECLARATIONS_(jitterlispvm)
#endif // #if defined (JITTER_HAVE_PATCH_IN)

#ifndef JITTER_DISPATCH_SWITCH
/* True iff thread sizes are all non-negative and non-huge.  We refuse to
   disassemble otherwise, and when replication is enabled we refuse to run
   altogether.  See the comment right below. */
static bool
jitterlispvm_threads_validated = false;
#endif // #ifndef JITTER_DISPATCH_SWITCH

/* Omit jitterlispvm_validate_thread_sizes_once for switch-dispatching, as threads
   don't exist at all in that case.*/
#ifndef JITTER_DISPATCH_SWITCH
/* Check that VM instruction sizes are all non-negative, and that no thread
   starts before the end of the previous one.  Even one violation of such
   conditions is a symptom that the code has not been compiled with
   -fno-reorder-blocks , which would have disastrous effects with replication.
   It's better to validate threads at startup and fail immediately than to crash
   at run time.

   If even one thread appears to be wrong then refuse to disassemble when
   replication is disabled, and refuse to run altogether if replication is
   enabled. */
static void
jitterlispvm_validate_threads_once (void)
{
  /* Return if this is not the first time we got here. */
  static bool already_validated = false;
  if (already_validated)
    return;

#ifdef JITTER_REPLICATE
# define JITTER_FAIL(error_text)                                             \
    do                                                                       \
      {                                                                      \
        fprintf (stderr,                                                     \
                 "About specialized instruction %i (%s) at %p, size %liB\n", \
                 i, jitterlispvm_specialized_instruction_names [i],              \
                 jitterlispvm_threads [i],                                       \
                 jitterlispvm_thread_sizes [i]);                                 \
        jitter_fatal ("%s: you are not compiling with -fno-reorder-blocks",  \
                      error_text);                                           \
      }                                                                      \
    while (false)
#else
# define JITTER_FAIL(ignored_error_text)  \
    do                                    \
      {                                   \
        everything_valid = false;         \
        goto out;                         \
      }                                   \
    while (false)
#endif // #ifdef JITTER_REPLICATE

  /* The minimum address the next instruction code has to start at.

     This relies on NULL being zero, or in general lower in magnitude than any
     valid pointer.  It is not worth the trouble to be pedantic, as this will be
     true on every architecture where I can afford low-level tricks. */
  jitter_thread lower_bound = NULL;

  /* Check every thread.  We rely on the order here, following specialized
     instruction opcodes. */
  int i;
  bool everything_valid = true;
  for (i = 0; i < JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO; i ++)
    {
      jitter_thread thread = jitterlispvm_threads [i];
      long size = jitterlispvm_thread_sizes [i];

      /* Check that the current thread has non-negative non-huge size and
         doesn't start before the end of the previous one.  If this is true for
         all threads we can conclude that they are non-overlapping as well. */
      if (__builtin_expect (size < 0, false))
        JITTER_FAIL("a specialized instruction has negative code size");
      if (__builtin_expect (size > (1 << 24), false))
        JITTER_FAIL("a specialized instruction has huge code size");
      if (__builtin_expect (lower_bound > thread, false))
        JITTER_FAIL("non-sequential thread");

      /* The next thread cannot start before the end of the current one. */
      lower_bound = ((char*) thread) + size;
    }

#undef JITTER_FAIL

#ifndef JITTER_REPLICATE
 out:
#endif // #ifndef JITTER_REPLICATE

  /* If we have validated every thread size then disassembling appears safe. */
  if (everything_valid)
    jitterlispvm_threads_validated = true;

  /* We have checked the thread sizes, once and for all.  If this function gets
     called again, thru a second jitterlispvm initialization, it will immediately
     return. */
  already_validated = true;
}
#endif // #ifndef JITTER_DISPATCH_SWITCH

#if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
/* The actual replacement table.  We only need it when defect replacement is in
   use. */
jitter_uint
jitterlispvm_replacement_table [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO];

/* The defective-instruction array for this VM.  The first
   defective_specialized_instruction_no elements of the array contain the
   specialized_instruction_ids of defective instructions; the remaining elements
   are set to -1.  This is initialised by jitter_fill_replacement_table . */
jitter_int
jitterlispvm_defective_specialized_instructions [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO];
#endif // #if defined (JITTER_HAVE_DEFECT_REPLACEMENT)

void
jitterlispvm_initialize (void)
{
  /* Check that the Jitter library we linked is the right one.  This check
     actually only useful to force the global to be used.  I prefer not to use
     an assert, because assertions can be disabled. */
  if (! JITTER_DISPATCH_DEPENDENT_GLOBAL_NAME)
    jitter_fatal ("impossible to reach: the thing should fail at link time");

#ifdef JITTER_REPLICATE
  /* Initialize the executable-memory subsystem. */
  jitter_initialize_executable ();
#endif // #ifdef JITTER_REPLICATE

  /* Initialise the print-context machinery. */
  jitter_print_initialize ();

  /* Perform some sanity checks which only need to be run once. */
  jitterlispvm_check_specialized_instruction_opcode_once ();

  /* We have to initialize threads before jitterlispvm_threads , since the struct
     needs threads. */
  jitterlispvm_initialize_threads ();

#ifndef JITTER_DISPATCH_SWITCH
  /* Validate threads, to make sure the generated code was not compiled with
     incorrect options.  This only needs to be done once. */
  jitterlispvm_validate_threads_once ();
#endif // ifndef JITTER_DISPATCH_SWITCH

  /* Initialize the object pointed by jitterlispvm_vm (see the comment above as to
     why we do it here).  Before actually setting the fields to valid data, fill
     the whole struct with a -- hopefully -- invalid pattern, just to catch
     bugs. */
  static bool vm_struct_initialized = false;
  if (! vm_struct_initialized)
    {
      memset (& the_jitterlispvm_vm, 0xff, sizeof (struct jitter_vm));

      /* Make the configuration struct reachable from the VM struct. */
      the_jitterlispvm_vm.configuration = jitterlispvm_vm_configuration;
      //jitterlispvm_print_vm_configuration (stdout, & the_jitterlispvm_vm.configuration);

      /* Initialize meta-instruction pointers for implicit instructions.
         VM-independent program specialization relies on those, so they have to
         be accessible to the Jitter library, out of generated code.  Since
         meta-instructions are sorted alphabetically in the array, the index
         is not fixed. */
      the_jitterlispvm_vm.exitvm_meta_instruction
        = (jitterlispvm_meta_instructions + jitterlispvm_meta_instruction_id_exitvm);
      the_jitterlispvm_vm.unreachable_meta_instruction
        = (jitterlispvm_meta_instructions
           + jitterlispvm_meta_instruction_id_unreachable);

      /* Threads or pointers to native code blocks of course don't exist with
         switch-dispatching. */
#ifndef JITTER_DISPATCH_SWITCH
      the_jitterlispvm_vm.threads = (jitter_thread *)jitterlispvm_threads;
      the_jitterlispvm_vm.thread_sizes = (long *) jitterlispvm_thread_sizes;
      the_jitterlispvm_vm.threads_validated = jitterlispvm_threads_validated;
#if defined (JITTER_HAVE_KNOWN_BINARY_FORMAT)
      the_jitterlispvm_vm.data_locations = JITTER_DATA_LOCATION_NAME(jitterlispvm);
#else
      the_jitterlispvm_vm.data_locations = NULL;
#endif // #if defined (JITTER_HAVE_KNOWN_BINARY_FORMAT)
#endif // #ifndef JITTER_DISPATCH_SWITCH

      the_jitterlispvm_vm.specialized_instruction_residual_arities
        = jitterlispvm_specialized_instruction_residual_arities;
      the_jitterlispvm_vm.specialized_instruction_label_bitmasks
        = jitterlispvm_specialized_instruction_label_bitmasks;

      /* FIXME: I might want to conditionalize this. */
      the_jitterlispvm_vm.specialized_instruction_relocatables
        = jitterlispvm_specialized_instruction_relocatables;

      the_jitterlispvm_vm.specialized_instruction_callers
        = jitterlispvm_specialized_instruction_callers;
      the_jitterlispvm_vm.specialized_instruction_callees
        = jitterlispvm_specialized_instruction_callees;

      the_jitterlispvm_vm.specialized_instruction_names
        = jitterlispvm_specialized_instruction_names;
      the_jitterlispvm_vm.specialized_instruction_no
        = JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO;

      the_jitterlispvm_vm.meta_instruction_string_hash
        = & jitterlispvm_meta_instruction_hash;
      the_jitterlispvm_vm.meta_instructions
        = (struct jitter_meta_instruction *) jitterlispvm_meta_instructions;
      the_jitterlispvm_vm.meta_instruction_no = JITTERLISPVM_META_INSTRUCTION_NO;
      the_jitterlispvm_vm.max_meta_instruction_name_length
        = JITTERLISPVM_MAX_META_INSTRUCTION_NAME_LENGTH;
      the_jitterlispvm_vm.specialized_instruction_to_unspecialized_instruction
        = jitterlispvm_specialized_instruction_to_unspecialized_instruction;
      the_jitterlispvm_vm.register_class_character_to_register_class
        = jitterlispvm_register_class_character_to_register_class;
      the_jitterlispvm_vm.specialize_instruction = jitterlispvm_specialize_instruction;
      the_jitterlispvm_vm.rewrite = jitterlispvm_rewrite;

#if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
      /* Fill the replacement table.  Since the array in question is a global
         with a fixed size, this needs to be done only once. */
      jitter_fill_replacement_table
         (jitterlispvm_replacement_table,
          jitterlispvm_defective_specialized_instructions,
          & the_jitterlispvm_vm,
          jitterlispvm_worst_case_replacement_table,
          jitterlispvm_call_related_specialized_instruction_ids,
          jitterlispvm_call_related_specialized_instruction_id_no,
          jitterlispvm_specialized_instruction_call_relateds,
          JITTER_DEFECT_DESCRIPTORS_NAME (jitterlispvm),
          (JITTER_DEFECT_DESCRIPTORS_SIZE_IN_BYTES_NAME (jitterlispvm)
           / sizeof (struct jitter_defect_descriptor)),
          JITTER_DEFECT_CORRECT_DISPLACEMENT_NAME (jitterlispvm));
      the_jitterlispvm_vm.replacement_table = jitterlispvm_replacement_table;
      the_jitterlispvm_vm.defective_specialized_instructions
        = jitterlispvm_defective_specialized_instructions;
      the_jitterlispvm_vm.specialized_instruction_call_relateds
        = jitterlispvm_specialized_instruction_call_relateds;
#else /* no defect replacement */
      /* In this configuration it is impossible to have defects: set every
         defect count to zero. */
      the_jitterlispvm_vm.defect_no = 0;
      the_jitterlispvm_vm.defective_specialized_instruction_no = 0;
      the_jitterlispvm_vm.defective_call_related_specialized_instruction_no = 0;
      the_jitterlispvm_vm.replacement_specialized_instruction_no = 0;
      the_jitterlispvm_vm.replacement_table = NULL;
      the_jitterlispvm_vm.defective_specialized_instructions = NULL;
      the_jitterlispvm_vm.specialized_instruction_call_relateds = NULL;
#endif // #if defined (JITTER_HAVE_DEFECT_REPLACEMENT)

      /* Initialize the empty list of states. */
      JITTER_LIST_INITIALIZE_HEADER (& the_jitterlispvm_vm.states);

      vm_struct_initialized = true;
    }

#ifdef JITTER_HAVE_PATCH_IN
    /* Since the patch-in table is destroyed at finalisation time we have to
       rebuild it at every initialisation, out of the previous conditional. */
    the_jitterlispvm_vm.specialized_instruction_fast_label_bitmasks
      = jitterlispvm_specialized_instruction_fast_label_bitmasks;
    the_jitterlispvm_vm.patch_in_descriptors =
      JITTER_PATCH_IN_DESCRIPTORS_NAME(jitterlispvm);
    const size_t patch_in_descriptor_size
      = sizeof (struct jitter_patch_in_descriptor);
    the_jitterlispvm_vm.patch_in_descriptor_no
      = (JITTER_PATCH_IN_DESCRIPTORS_SIZE_IN_BYTES_NAME(jitterlispvm)
         / patch_in_descriptor_size);
    /* Cheap sanity check: if the size in bytes is not a multiple of
       the element size, we are doing something very wrong. */
    if (JITTER_PATCH_IN_DESCRIPTORS_SIZE_IN_BYTES_NAME(jitterlispvm)
        % patch_in_descriptor_size != 0)
      jitter_fatal ("patch-in descriptors total size %li not a multiple "
                    "of the element size %li",
                    (long) (JITTER_PATCH_IN_DESCRIPTORS_SIZE_IN_BYTES_NAME
                            (jitterlispvm)),
                    (long) patch_in_descriptor_size);
    /* Initialize the patch-in table for this VM. */
    the_jitterlispvm_vm.patch_in_table
      = jitter_make_patch_in_table (the_jitterlispvm_vm.patch_in_descriptors,
                                    the_jitterlispvm_vm.patch_in_descriptor_no,
                                    JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO);
#else
    the_jitterlispvm_vm.specialized_instruction_fast_label_bitmasks = NULL;
#endif // #ifdef JITTER_HAVE_PATCH_IN

  jitter_initialize_meta_instructions (& jitterlispvm_meta_instruction_hash,
                                         jitterlispvm_meta_instructions,
                                         JITTERLISPVM_META_INSTRUCTION_NO);

#if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
# if 0
  jitter_dump_replacement_table (stderr, jitterlispvm_replacement_table,
                                 & the_jitterlispvm_vm);
  jitter_dump_defects (stderr, jitterlispvm_defective_specialized_instructions,
                       & the_jitterlispvm_vm,
                       jitterlispvm_specialized_instruction_call_relateds);
# endif
#endif // #if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
#if defined (JITTER_HAVE_PATCH_IN)
  //printf ("======================= Patch-in descriptors: BEGIN\n");
  //JITTER_DUMP_PATCH_IN_DESCRIPTORS(jitterlispvm);
  //printf ("======================= Patch-in descriptors: END\n");
#endif // #if defined (JITTER_HAVE_PATCH_IN)
}

void
jitterlispvm_finalize (void)
{
  /* There's no need to touch the_jitterlispvm_vm ; we can keep it as it is, as it
     contains no dynamically-allocated fields. */
  /* Threads need no finalization. */
  jitter_finalize_meta_instructions (& jitterlispvm_meta_instruction_hash);

#ifdef JITTER_HAVE_PATCH_IN
  /* Destroy the patch-in table for this VM. */
  jitter_destroy_patch_in_table (the_jitterlispvm_vm.patch_in_table,
                                 JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO);
#endif // #ifdef JITTER_HAVE_PATCH_IN

#ifdef JITTER_REPLICATE
  /* Finalize the executable-memory subsystem. */
  jitter_finalize_executable ();
#endif // #ifdef JITTER_REPLICATE

  /* Finalize the state list.  If it is not empty then something has gone
     wrong earlier. */
  if (the_jitterlispvm_vm.states.first != NULL
      || the_jitterlispvm_vm.states.last != NULL)
    jitter_fatal ("not every state structure was destroyed before JITTERLISPVM "
                  "finalisation.");
}




/* VM-dependant mutable routine initialization.
 * ************************************************************************** */

struct jitter_mutable_routine*
jitterlispvm_make_mutable_routine (void)
{
  return jitter_make_mutable_routine (jitterlispvm_vm);
}




/* Sample profiling: internal API.
 * ************************************************************************** */

#if defined (JITTERLISPVM_PROFILE_SAMPLE)

/* Sample profiling depends on some system features: fail immediately if they
   are not available */
#if ! defined (JITTER_HAVE_SIGACTION) || ! defined (JITTER_HAVE_SETITIMER)
# jitter_fatal "sample-profiling depends on sigaction and setitimer"
#endif

static struct itimerval
jitterlispvm_timer_interval;

static struct itimerval
jitterlispvm_timer_disabled_interval;

/* The sampling data, currently global.  The current implementation does not
   play well with threads, but it can be changed later keeping the same user
   API. */
struct jitterlispvm_sample_profile_state
{
  /* The state currently sample-profiling.  Since such a state can be only one
     right now this field is useful for printing error messages in case the user
     sets up sample-profiling from two states at the same time by mistake.
     This field is also useful for temporarily suspending and then reenabling
     sampling, when The Array is being resized: if the signal handler sees that
     this field is NULL it will not touch the fields. */
  struct jitterlispvm_state *state_p;

  /* A pointer to the counts field within the sample_profile_runtime struct. */
  uint32_t *counts;

  /* A pointer to the current specialised instruction opcode within the
     sample_profile_runtime struct. */
  volatile jitter_int * specialized_opcode_p;

  /* A pointer to the field counting the number of samples, again within the
     sample_profile_runtime struct. */
  unsigned int *sample_no_p;
};

/* The (currently) one and only global state for sample-profiling. */
static struct jitterlispvm_sample_profile_state
jitterlispvm_sample_profile_state;

static void
jitterlispvm_sigprof_handler (int signal)
{
#if 0
  assert (jitterlispvm_sample_profile_state.state_p != NULL);
#endif

  jitter_int specialized_opcode
    = * jitterlispvm_sample_profile_state.specialized_opcode_p;
  if (__builtin_expect ((specialized_opcode >= 0
                         && (specialized_opcode
                             < JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO)),
                        true))
    jitterlispvm_sample_profile_state.counts [specialized_opcode] ++;

  (* jitterlispvm_sample_profile_state.sample_no_p) ++;
}

void
jitterlispvm_profile_sample_initialize (void)
{
  /* Perform a sanity check over the sampling period. */
  if (JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS <= 0 ||
      JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS >= 1000)
    jitter_fatal ("invalid JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS: %f",
                  (double) JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS);
  struct sigaction action;
  sigaction (SIGPROF, NULL, & action);
  action.sa_handler = jitterlispvm_sigprof_handler;
  sigaction (SIGPROF, & action, NULL);

  long microseconds
    = (long) (JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS * 1000);
  jitterlispvm_timer_interval.it_interval.tv_sec = 0;
  jitterlispvm_timer_interval.it_interval.tv_usec = microseconds;
  jitterlispvm_timer_interval.it_value = jitterlispvm_timer_interval.it_interval;

  jitterlispvm_sample_profile_state.state_p = NULL;
  jitterlispvm_timer_disabled_interval.it_interval.tv_sec = 0;
  jitterlispvm_timer_disabled_interval.it_interval.tv_usec = 0;
  jitterlispvm_timer_disabled_interval.it_value
    = jitterlispvm_timer_disabled_interval.it_interval;
}

void
jitterlispvm_profile_sample_start (struct jitterlispvm_state *state_p)
{
  struct jitter_sample_profile_runtime *spr
    = ((struct jitter_sample_profile_runtime *)
       & JITTERLISPVM_STATE_TO_SPECIAL_PURPOSE_STATE_DATA (state_p)
           ->profile_runtime.sample_profile_runtime);

  if (jitterlispvm_sample_profile_state.state_p != NULL)
    {
      if (state_p != jitterlispvm_sample_profile_state.state_p)
        jitter_fatal ("currently it is only possible to sample-profile from "
                      "one state at the time: trying to sample-profile from "
                      "the state %p when already sample-profiling from the "
                      "state %p",
                      state_p, jitterlispvm_sample_profile_state.state_p);
      else
        {
          /* This situation is a symptom of a bug, but does not need to lead
             to a fatal error. */
          printf ("WARNING: starting profile on the state %p when profiling "
                  "was already active in the same state.\n"
                  "Did you call longjmp from VM code?", state_p);
          fflush (stdout);
        }
    }
  jitterlispvm_sample_profile_state.state_p = state_p;
  jitterlispvm_sample_profile_state.sample_no_p = & spr->sample_no;
  jitterlispvm_sample_profile_state.counts = spr->counts;
  jitterlispvm_sample_profile_state.specialized_opcode_p
    = & spr->current_specialized_instruction_opcode;
  //fprintf (stderr, "SAMPLE START\n"); fflush (NULL);
  if (setitimer (ITIMER_PROF, & jitterlispvm_timer_interval, NULL) != 0)
    jitter_fatal ("setitimer failed when establishing a timer");
}

void
jitterlispvm_profile_sample_stop (void)
{
  if (setitimer (ITIMER_PROF, & jitterlispvm_timer_disabled_interval, NULL) != 0)
    jitter_fatal ("setitimer failed when disabling a timer");

  jitterlispvm_sample_profile_state.state_p = NULL;

  /* The rest is just for defenisveness' sake. */
  * jitterlispvm_sample_profile_state.specialized_opcode_p = -1;
  jitterlispvm_sample_profile_state.sample_no_p = NULL;
  jitterlispvm_sample_profile_state.counts = NULL;
  jitterlispvm_sample_profile_state.specialized_opcode_p = NULL;
}
#endif // #if defined (JITTERLISPVM_PROFILE_SAMPLE)




/* Slow register initialisation.
 * ************************************************************************** */

/* Initialise slow registers (for register classes defining an initial value) in
   a given Array, starting from a given rank up to another given rank.  The
   argument old_slow_register_no_per_class indicates the number of already
   initialised ranks, which this functions does not touch;
   new_slow_register_no_per_class indicates the new number of ranks.  Every rank
   from old_slow_register_no_per_class + 1 to new_slow_register_no_per_class,
   both included, will be initialised. */
static void
jitterlispvm_initialize_slow_registers (char *initial_array_pointer,
                                    jitter_int old_slow_register_no_per_class,
                                    jitter_int new_slow_register_no_per_class)
{
  /* Compute the address of the first slow registers, which is the beginning
     of the first rank. */
  union jitterlispvm_any_register *first_slow_register
    = ((union jitterlispvm_any_register *)
       ((char *) initial_array_pointer
        + JITTERLISPVM_FIRST_SLOW_REGISTER_UNBIASED_OFFSET));

  /* Initialise every *new* rank, without touching the old ones. */
  jitter_int i;
  for (i = old_slow_register_no_per_class;
       i < new_slow_register_no_per_class;
       i ++)
    {
      /* A pointer to the i-th rank of slow registers.  Every register
         in the rank is new and in general (according to its class) may
         need initialisation. */
      union jitterlispvm_any_register *rank
        = first_slow_register + (i * JITTERLISPVM_REGISTER_CLASS_NO);
      JITTERLISPVM_INITIALIZE_SLOW_REGISTER_RANK (rank);
    }
#if 0
      fprintf (stderr, "initialised %li (up from %li) slow registers per class, array at %p\n",
               (long) new_slow_register_no_per_class,
               (long) old_slow_register_no_per_class,
               initial_array_pointer);
#endif
}




/* Array re-allocation.
 * ************************************************************************** */

char *
jitterlispvm_make_place_for_slow_registers (struct jitterlispvm_state *s,
                                        jitter_int new_slow_register_no_per_class)
{
  if (new_slow_register_no_per_class < 0)
    jitter_fatal ("jitterlispvm_make_place_for_slow_registers: negative slow "
                  "register number");
  jitter_int old_slow_register_no_per_class
    = s->jitterlispvm_state_backing.jitter_slow_register_no_per_class;
  /* Change nothing if we already have enough space for the required number of
     slow registers.  The no-change case will be the most common one, and
     this function might be worth optimizing. */
  if (__builtin_expect (new_slow_register_no_per_class
                        > old_slow_register_no_per_class,
                        false))
    {
#if defined (JITTERLISPVM_PROFILE_SAMPLE)
      /* If sample-profiling is currently in progress on this state suspend it
         temporarily. */
      bool suspending_sample_profiling
        = (jitterlispvm_sample_profile_state.state_p == s);
      if (suspending_sample_profiling)
        jitterlispvm_profile_sample_stop ();
#endif // #if defined (JITTERLISPVM_PROFILE_SAMPLE)

#if 0
      printf ("Increasing slow register-no (per class) from %li to %li\n", (long) old_slow_register_no_per_class, (long)new_slow_register_no_per_class);
      printf ("Array size %li -> %li\n", (long) JITTERLISPVM_ARRAY_SIZE(old_slow_register_no_per_class), (long) JITTERLISPVM_ARRAY_SIZE(new_slow_register_no_per_class));
#endif
      /* Save the new value for new_slow_register_no_per_class in the state
         structure; reallocate The Array. */
      s->jitterlispvm_state_backing.jitter_slow_register_no_per_class
        = new_slow_register_no_per_class;
      s->jitterlispvm_state_backing.jitter_array
        = jitter_xrealloc ((void *) s->jitterlispvm_state_backing.jitter_array,
                           JITTERLISPVM_ARRAY_SIZE(new_slow_register_no_per_class));

     /* Initialise the slow registers we have just added, for every class. */
     jitterlispvm_initialize_slow_registers (s->jitterlispvm_state_backing.jitter_array,
                                         old_slow_register_no_per_class,
                                         new_slow_register_no_per_class);

#if defined (JITTERLISPVM_PROFILE_SAMPLE)
      /* Now we can resume sample-profiling on this state if we suspended it. */
      if (suspending_sample_profiling)
        jitterlispvm_profile_sample_start (s);
#endif // #if defined (JITTERLISPVM_PROFILE_SAMPLE)
#if 0
      fprintf (stderr, "slow registers are now %li per class, Array at %p (biased %p)\n",
               ((long)
                s->jitterlispvm_state_backing.jitter_slow_register_no_per_class),
               s->jitterlispvm_state_backing.jitter_array,
               s->jitterlispvm_state_backing.jitter_array + JITTER_ARRAY_BIAS);
#endif
    }

  /* Return the new (or unchanged) base, by simply adding the bias to the
     Array as it is now. */
  return s->jitterlispvm_state_backing.jitter_array + JITTER_ARRAY_BIAS;
}

void
jitterlispvm_ensure_enough_slow_registers_for_executable_routine
   (const struct jitter_executable_routine *er, struct jitterlispvm_state *s)
{
  jitterlispvm_make_place_for_slow_registers (s, er->slow_register_per_class_no);
}




/* Program text frontend.
 * ************************************************************************** */

struct jitterlispvm_routine_parse_error *
jitterlispvm_parse_mutable_routine_from_file_star (FILE *input_file,
                                               struct jitter_mutable_routine *p)
{
  return jitter_parse_mutable_routine_from_file_star (input_file, p,
                                                      jitterlispvm_vm);
}

struct jitterlispvm_routine_parse_error *
jitterlispvm_parse_mutable_routine_from_file (const char *input_file_name,
                                          struct jitter_mutable_routine *p)
{
  return jitter_parse_mutable_routine_from_file (input_file_name, p,
                                                 jitterlispvm_vm);
}

struct jitterlispvm_routine_parse_error *
jitterlispvm_parse_mutable_routine_from_string (const char *string,
                                            struct jitter_mutable_routine *p)
{
  return jitter_parse_mutable_routine_from_string (string, p, jitterlispvm_vm);
}




/* State making and destroying.
 * ************************************************************************** */

/* State initialisation (with a given number of slow registers), reset and
   finalisation are machine-generated. */

void
jitterlispvm_state_initialize (struct jitterlispvm_state *sp)
{
  jitterlispvm_state_initialize_with_slow_registers (sp, 0);
}

struct jitterlispvm_state *
jitterlispvm_state_make_with_slow_registers (jitter_uint slow_register_no_per_class)
{
  struct jitterlispvm_state *res = jitter_xmalloc (sizeof (struct jitterlispvm_state));
  jitterlispvm_state_initialize_with_slow_registers (res,
                                                 slow_register_no_per_class);
  return res;
}

struct jitterlispvm_state *
jitterlispvm_state_make (void)
{
  return jitterlispvm_state_make_with_slow_registers (0);
}

void
jitterlispvm_state_destroy (struct jitterlispvm_state *state)
{
  jitterlispvm_state_finalize (state);
  free (state);
}




/* Executing code: unified routine API.
 * ************************************************************************** */

void
jitterlispvm_ensure_enough_slow_registers_for_routine
   (jitter_routine r, struct jitterlispvm_state *s)
{
  struct jitter_executable_routine *e
    = jitter_routine_make_executable_if_needed (r);
  jitterlispvm_ensure_enough_slow_registers_for_executable_routine (e, s);
}

enum jitterlispvm_exit_status
jitterlispvm_execute_routine (jitter_routine r,
                          struct jitterlispvm_state *s)
{
  struct jitter_executable_routine *e
    = jitter_routine_make_executable_if_needed (r);
  return jitterlispvm_execute_executable_routine (e, s);
}





/* Defects and replacements: user API.
 * ************************************************************************** */

/* These functions are all trivial wrappers around the functionality declared
   in jitter/jitter-defect.h, hiding the VM pointer. */

void
jitterlispvm_defect_print_summary (jitter_print_context cx)
{
  jitter_defect_print_summary (cx, jitterlispvm_vm);
}

void
jitterlispvm_defect_print (jitter_print_context cx,
                       unsigned indentation_column_no)
{
  jitter_defect_print (cx, jitterlispvm_vm, indentation_column_no);
}

void
jitterlispvm_defect_print_replacement_table (jitter_print_context cx,
                                         unsigned indentation_column_no)
{
  jitter_defect_print_replacement_table (cx, jitterlispvm_vm, indentation_column_no);
}




/* Profiling: user API.
 * ************************************************************************** */

/* These functions are all trivial wrappers around the functionality declared
   in jitter/jitter-profile.h, hiding the VM pointer. */

struct jitterlispvm_profile_runtime *
jitterlispvm_state_profile_runtime (struct jitterlispvm_state *s)
{
  volatile struct jitter_special_purpose_state_data *spd
    = JITTERLISPVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA
        (s->jitterlispvm_state_backing.jitter_array);
  return (struct jitterlispvm_profile_runtime *) & spd->profile_runtime;
}

struct jitterlispvm_profile_runtime *
jitterlispvm_profile_runtime_make (void)
{
  return jitter_profile_runtime_make (jitterlispvm_vm);
}

void
jitterlispvm_profile_runtime_destroy (struct jitterlispvm_profile_runtime *p)
{
  jitter_profile_runtime_destroy (jitterlispvm_vm, p);
}

void
jitterlispvm_profile_runtime_clear (struct jitterlispvm_profile_runtime * p)
{
  jitter_profile_runtime_clear (jitterlispvm_vm, p);
}

void
jitterlispvm_profile_runtime_merge_from (struct jitterlispvm_profile_runtime *to,
                                     const struct jitterlispvm_profile_runtime *from)
{
  jitter_profile_runtime_merge_from (jitterlispvm_vm, to, from);
}

void
jitterlispvm_profile_runtime_merge_from_state (struct jitterlispvm_profile_runtime *to,
                                           const struct jitterlispvm_state *from_state)
{
  const struct jitterlispvm_profile_runtime* from
    = jitterlispvm_state_profile_runtime ((struct jitterlispvm_state *) from_state);
  jitter_profile_runtime_merge_from (jitterlispvm_vm, to, from);
}

void
jitterlispvm_profile_runtime_print_unspecialized
   (jitter_print_context ct,
    const struct jitterlispvm_profile_runtime *p)
{
  jitter_profile_runtime_print_unspecialized (ct, jitterlispvm_vm, p);
}

void
jitterlispvm_profile_runtime_print_specialized (jitter_print_context ct,
                                            const struct jitterlispvm_profile_runtime
                                            *p)
{
  jitter_profile_runtime_print_specialized (ct, jitterlispvm_vm, p);
}

struct jitterlispvm_profile *
jitterlispvm_profile_unspecialized_from_runtime
   (const struct jitterlispvm_profile_runtime *p)
{
  return jitter_profile_unspecialized_from_runtime (jitterlispvm_vm, p);
}

struct jitterlispvm_profile *
jitterlispvm_profile_specialized_from_runtime (const struct jitterlispvm_profile_runtime
                                           *p)
{
  return jitter_profile_specialized_from_runtime (jitterlispvm_vm, p);
}




/* Evrything following this point is machine-generated.
 * ************************************************************************** */

/* What follows could be conceptually split into several generated C files, but
   having too many of them would be inconvenient for the user to compile and
   link.  For this reason we currently generate just three files: one is this,
   which also contains the specializer, another is for the executor, and then a
   header -- a main module is optional.  The executor will be potentially very
   large, so it is best compiled separately.  The specializer might be large as
   well at this stage, even if its compilation is usually much less
   expensive. */
/* These two macros are convenient for making VM-specific identifiers
   using VM-independent macros from a public header, without polluting
   the global namespace. */
#define JITTER_VM_PREFIX_LOWER_CASE jitterlispvm
#define JITTER_VM_PREFIX_UPPER_CASE JITTERLISPVM

/* User-specified code, printer part: beginning. */

    /* Not really needed.  The printer is jitterlisp_print. */
  
/* User-specified code, printer part: end */

//#include <stdbool.h>

//#include <jitter/jitter.h>
//#include <jitter/jitter-instruction.h>

//#include "jitterlispvm-meta-instructions.h"

// FIXME: comment.
struct jitter_hash_table
jitterlispvm_meta_instruction_hash;


static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_at_mdepth_mto_mregister_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mfalse_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mnon_mpositive_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mnot_mless_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mnot_mnull_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mnull_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mpositive_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mregister_mnon_mpositive_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mregister_mnon_mzero_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mregister_mnot_mnull_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mregister_mnull_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mregister_mpositive_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_branch_mif_mtrue_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_call_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_call_mcompiled_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_cdr_mregister_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_check_mclosure_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_check_mglobal_mdefined_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitterlisp_print }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_check_min_marity_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_check_min_marity_m_malt_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_copy_mfrom_mliteral_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitterlisp_print } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_copy_mfrom_mregister_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_copy_mto_mregister_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_gc_mif_mneeded_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_heap_mallocate_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_literal_mto_mregister_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitterlisp_print }, { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_nip_mpush_mliteral_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_nip_mpush_mregister_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_one_mminus_mregister_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_one_mplus_mregister_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_pop_mto_mglobal_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitterlisp_print }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_pop_mto_mglobal_mdefined_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitterlisp_print }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_pop_mto_mregister_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mbox_mget_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mbox_msetb_mspecial_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mcar_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mcdr_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mfixnum_meqp_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mfixnum_mnot_meqp_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mgreaterp_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mlessp_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mnegate_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mnegativep_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mnon_mnegativep_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mnon_mpositivep_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mnon_mzerop_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mnot_mgreaterp_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mnot_mlessp_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mone_mminus_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mone_mplus_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mpositivep_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mprimordial_mdivided_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mprimordial_mdivided_munsafe_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mprimordial_mminus_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mprimordial_mplus_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mprimordial_mtimes_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mquotient_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mquotient_munsafe_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mremainder_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mremainder_munsafe_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mset_mcarb_mspecial_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mset_mcdrb_mspecial_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mtwo_mdivided_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mtwo_mquotient_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mtwo_mremainder_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mtwo_mtimes_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_primitive_mzerop_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_push_mglobal_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitterlisp_print }, { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_push_mliteral_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitterlisp_print } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_push_mregister_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_register_mto_mregister_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_restore_mregister_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_save_mregister_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & jitterlispvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_tail_mcall_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type jitterlispvm_tail_mcall_mcompiled_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };


const struct jitter_meta_instruction
jitterlispvm_meta_instructions [JITTERLISPVM_META_INSTRUCTION_NO]
  = {
      { 0, "at-depth-to-register", 2, false, false, false, true /* this ignores replacements */, jitterlispvm_at_mdepth_mto_mregister_meta_instruction_parameter_types },
      { 1, "branch", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_branch_meta_instruction_parameter_types },
      { 2, "branch-if-false", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_branch_mif_mfalse_meta_instruction_parameter_types },
      { 3, "branch-if-non-positive", 2, true, false, false, true /* this ignores replacements */, jitterlispvm_branch_mif_mnon_mpositive_meta_instruction_parameter_types },
      { 4, "branch-if-not-less", 2, true, false, false, true /* this ignores replacements */, jitterlispvm_branch_mif_mnot_mless_meta_instruction_parameter_types },
      { 5, "branch-if-not-null", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_branch_mif_mnot_mnull_meta_instruction_parameter_types },
      { 6, "branch-if-null", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_branch_mif_mnull_meta_instruction_parameter_types },
      { 7, "branch-if-positive", 2, true, false, false, true /* this ignores replacements */, jitterlispvm_branch_mif_mpositive_meta_instruction_parameter_types },
      { 8, "branch-if-register-non-positive", 3, true, false, false, true /* this ignores replacements */, jitterlispvm_branch_mif_mregister_mnon_mpositive_meta_instruction_parameter_types },
      { 9, "branch-if-register-non-zero", 3, true, false, false, true /* this ignores replacements */, jitterlispvm_branch_mif_mregister_mnon_mzero_meta_instruction_parameter_types },
      { 10, "branch-if-register-not-null", 2, true, false, false, true /* this ignores replacements */, jitterlispvm_branch_mif_mregister_mnot_mnull_meta_instruction_parameter_types },
      { 11, "branch-if-register-null", 2, true, false, false, true /* this ignores replacements */, jitterlispvm_branch_mif_mregister_mnull_meta_instruction_parameter_types },
      { 12, "branch-if-register-positive", 3, true, false, false, true /* this ignores replacements */, jitterlispvm_branch_mif_mregister_mpositive_meta_instruction_parameter_types },
      { 13, "branch-if-true", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_branch_mif_mtrue_meta_instruction_parameter_types },
      { 14, "call", 1, true, true, false, true /* this ignores replacements */, jitterlispvm_call_meta_instruction_parameter_types },
      { 15, "call-compiled", 1, true, true, false, true /* this ignores replacements */, jitterlispvm_call_mcompiled_meta_instruction_parameter_types },
      { 16, "call-from-c", 0, true, true, false, true /* this ignores replacements */, NULL },
      { 17, "canonicalize-boolean", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 18, "cdr-register", 3, true, false, false, true /* this ignores replacements */, jitterlispvm_cdr_mregister_meta_instruction_parameter_types },
      { 19, "check-closure", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_check_mclosure_meta_instruction_parameter_types },
      { 20, "check-global-defined", 2, true, false, false, true /* this ignores replacements */, jitterlispvm_check_mglobal_mdefined_meta_instruction_parameter_types },
      { 21, "check-in-arity", 2, true, false, false, true /* this ignores replacements */, jitterlispvm_check_min_marity_meta_instruction_parameter_types },
      { 22, "check-in-arity--alt", 2, true, false, false, true /* this ignores replacements */, jitterlispvm_check_min_marity_m_malt_meta_instruction_parameter_types },
      { 23, "copy-from-literal", 1, false, false, false, true /* this ignores replacements */, jitterlispvm_copy_mfrom_mliteral_meta_instruction_parameter_types },
      { 24, "copy-from-register", 1, false, false, false, true /* this ignores replacements */, jitterlispvm_copy_mfrom_mregister_meta_instruction_parameter_types },
      { 25, "copy-to-register", 1, false, false, false, true /* this ignores replacements */, jitterlispvm_copy_mto_mregister_meta_instruction_parameter_types },
      { 26, "drop", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 27, "drop-nip", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 28, "dup", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 29, "exitvm", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 30, "fail", 0, false, false, false, false /* this ignores replacements */, NULL },
      { 31, "gc-if-needed", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_gc_mif_mneeded_meta_instruction_parameter_types },
      { 32, "heap-allocate", 1, false, false, false, true /* this ignores replacements */, jitterlispvm_heap_mallocate_meta_instruction_parameter_types },
      { 33, "literal-to-register", 2, false, false, false, true /* this ignores replacements */, jitterlispvm_literal_mto_mregister_meta_instruction_parameter_types },
      { 34, "nip", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 35, "nip-drop", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 36, "nip-five", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 37, "nip-five-drop", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 38, "nip-four", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 39, "nip-four-drop", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 40, "nip-push-literal", 1, false, false, false, true /* this ignores replacements */, jitterlispvm_nip_mpush_mliteral_meta_instruction_parameter_types },
      { 41, "nip-push-register", 1, false, false, false, true /* this ignores replacements */, jitterlispvm_nip_mpush_mregister_meta_instruction_parameter_types },
      { 42, "nip-six", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 43, "nip-six-drop", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 44, "nip-three", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 45, "nip-three-drop", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 46, "nip-two", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 47, "nip-two-drop", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 48, "nop", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 49, "one-minus-register", 3, true, false, false, true /* this ignores replacements */, jitterlispvm_one_mminus_mregister_meta_instruction_parameter_types },
      { 50, "one-plus-register", 3, true, false, false, true /* this ignores replacements */, jitterlispvm_one_mplus_mregister_meta_instruction_parameter_types },
      { 51, "pop-to-global", 2, true, false, false, true /* this ignores replacements */, jitterlispvm_pop_mto_mglobal_meta_instruction_parameter_types },
      { 52, "pop-to-global-defined", 2, true, false, false, true /* this ignores replacements */, jitterlispvm_pop_mto_mglobal_mdefined_meta_instruction_parameter_types },
      { 53, "pop-to-register", 1, false, false, false, true /* this ignores replacements */, jitterlispvm_pop_mto_mregister_meta_instruction_parameter_types },
      { 54, "primitive", 3, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_meta_instruction_parameter_types },
      { 55, "primitive-boolean-canonicalize", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 56, "primitive-box", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 57, "primitive-box-get", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mbox_mget_meta_instruction_parameter_types },
      { 58, "primitive-box-setb-special", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mbox_msetb_mspecial_meta_instruction_parameter_types },
      { 59, "primitive-car", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mcar_meta_instruction_parameter_types },
      { 60, "primitive-cdr", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mcdr_meta_instruction_parameter_types },
      { 61, "primitive-characterp", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 62, "primitive-cons-special", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 63, "primitive-consp", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 64, "primitive-eqp", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 65, "primitive-fixnum-eqp", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mfixnum_meqp_meta_instruction_parameter_types },
      { 66, "primitive-fixnum-not-eqp", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mfixnum_mnot_meqp_meta_instruction_parameter_types },
      { 67, "primitive-fixnump", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 68, "primitive-greaterp", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mgreaterp_meta_instruction_parameter_types },
      { 69, "primitive-lessp", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mlessp_meta_instruction_parameter_types },
      { 70, "primitive-negate", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mnegate_meta_instruction_parameter_types },
      { 71, "primitive-negativep", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mnegativep_meta_instruction_parameter_types },
      { 72, "primitive-non-consp", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 73, "primitive-non-negativep", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mnon_mnegativep_meta_instruction_parameter_types },
      { 74, "primitive-non-nullp", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 75, "primitive-non-positivep", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mnon_mpositivep_meta_instruction_parameter_types },
      { 76, "primitive-non-symbolp", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 77, "primitive-non-zerop", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mnon_mzerop_meta_instruction_parameter_types },
      { 78, "primitive-not", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 79, "primitive-not-eqp", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 80, "primitive-not-greaterp", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mnot_mgreaterp_meta_instruction_parameter_types },
      { 81, "primitive-not-lessp", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mnot_mlessp_meta_instruction_parameter_types },
      { 82, "primitive-nothingp", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 83, "primitive-nullp", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 84, "primitive-one-minus", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mone_mminus_meta_instruction_parameter_types },
      { 85, "primitive-one-plus", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mone_mplus_meta_instruction_parameter_types },
      { 86, "primitive-positivep", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mpositivep_meta_instruction_parameter_types },
      { 87, "primitive-primordial-divided", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mprimordial_mdivided_meta_instruction_parameter_types },
      { 88, "primitive-primordial-divided-unsafe", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mprimordial_mdivided_munsafe_meta_instruction_parameter_types },
      { 89, "primitive-primordial-minus", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mprimordial_mminus_meta_instruction_parameter_types },
      { 90, "primitive-primordial-plus", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mprimordial_mplus_meta_instruction_parameter_types },
      { 91, "primitive-primordial-times", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mprimordial_mtimes_meta_instruction_parameter_types },
      { 92, "primitive-quotient", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mquotient_meta_instruction_parameter_types },
      { 93, "primitive-quotient-unsafe", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mquotient_munsafe_meta_instruction_parameter_types },
      { 94, "primitive-remainder", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mremainder_meta_instruction_parameter_types },
      { 95, "primitive-remainder-unsafe", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mremainder_munsafe_meta_instruction_parameter_types },
      { 96, "primitive-set-carb-special", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mset_mcarb_mspecial_meta_instruction_parameter_types },
      { 97, "primitive-set-cdrb-special", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mset_mcdrb_mspecial_meta_instruction_parameter_types },
      { 98, "primitive-symbolp", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 99, "primitive-two-divided", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mtwo_mdivided_meta_instruction_parameter_types },
      { 100, "primitive-two-quotient", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mtwo_mquotient_meta_instruction_parameter_types },
      { 101, "primitive-two-remainder", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mtwo_mremainder_meta_instruction_parameter_types },
      { 102, "primitive-two-times", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mtwo_mtimes_meta_instruction_parameter_types },
      { 103, "primitive-uniquep", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 104, "primitive-zerop", 1, true, false, false, true /* this ignores replacements */, jitterlispvm_primitive_mzerop_meta_instruction_parameter_types },
      { 105, "procedure-prolog", 0, true, false, true, true /* this ignores replacements */, NULL },
      { 106, "push-false", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 107, "push-global", 2, true, false, false, true /* this ignores replacements */, jitterlispvm_push_mglobal_meta_instruction_parameter_types },
      { 108, "push-literal", 1, false, false, false, true /* this ignores replacements */, jitterlispvm_push_mliteral_meta_instruction_parameter_types },
      { 109, "push-nil", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 110, "push-nothing", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 111, "push-one", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 112, "push-register", 1, false, false, false, true /* this ignores replacements */, jitterlispvm_push_mregister_meta_instruction_parameter_types },
      { 113, "push-unspecified", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 114, "push-zero", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 115, "register-to-register", 2, false, false, false, true /* this ignores replacements */, jitterlispvm_register_mto_mregister_meta_instruction_parameter_types },
      { 116, "restore-register", 1, false, false, false, true /* this ignores replacements */, jitterlispvm_restore_mregister_meta_instruction_parameter_types },
      { 117, "return", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 118, "save-register", 1, false, false, false, true /* this ignores replacements */, jitterlispvm_save_mregister_meta_instruction_parameter_types },
      { 119, "tail-call", 1, true, true, false, true /* this ignores replacements */, jitterlispvm_tail_mcall_meta_instruction_parameter_types },
      { 120, "tail-call-compiled", 1, true, true, false, true /* this ignores replacements */, jitterlispvm_tail_mcall_mcompiled_meta_instruction_parameter_types },
      { 121, "unreachable", 0, true, false, false, true /* this ignores replacements */, NULL }
    };

/* The register class descriptor for r registers. */
const struct jitter_register_class
jitterlispvm_register_class_r
  = {
      jitterlispvm_register_class_id_r,
      'r',
      "register_class_r",
      "REGISTER_CLASS_R",
      JITTERLISPVM_REGISTER_r_FAST_REGISTER_NO,
      1 /* Use slow registers */
    };


/* A pointer to every existing register class descriptor. */
const struct jitter_register_class * const
jitterlispvm_regiter_classes []
  = {
      & jitterlispvm_register_class_r
    };

const struct jitter_register_class *
jitterlispvm_register_class_character_to_register_class (char c)
{
  switch (c)
    {
    case 'r': return & jitterlispvm_register_class_r;
    default:  return NULL;
    }
}

//#include "jitterlispvm-specialized-instructions.h"

const char * const
jitterlispvm_specialized_instruction_names [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      "!INVALID",
      "!BEGINBASICBLOCK",
      "!EXITVM",
      "!DATALOCATIONS",
      "!NOP",
      "!UNREACHABLE0",
      "!UNREACHABLE1",
      "!PRETENDTOJUMPANYWHERE",
      "at-depth-to-register/n1/%rR",
      "at-depth-to-register/n2/%rR",
      "at-depth-to-register/n3/%rR",
      "at-depth-to-register/n4/%rR",
      "at-depth-to-register/n5/%rR",
      "at-depth-to-register/n6/%rR",
      "at-depth-to-register/n7/%rR",
      "at-depth-to-register/n8/%rR",
      "at-depth-to-register/n9/%rR",
      "at-depth-to-register/n10/%rR",
      "at-depth-to-register/nR/%rR",
      "branch/fR",
      "branch-if-false/fR",
      "branch-if-non-positive/fR/fR",
      "branch-if-not-less/fR/fR",
      "branch-if-not-null/fR",
      "branch-if-null/fR",
      "branch-if-positive/fR/fR",
      "branch-if-register-non-positive/%rR/fR/fR",
      "branch-if-register-non-zero/%rR/fR/fR",
      "branch-if-register-not-null/%rR/fR",
      "branch-if-register-null/%rR/fR",
      "branch-if-register-positive/%rR/fR/fR",
      "branch-if-true/fR",
      "call/n0/retR",
      "call/n1/retR",
      "call/n2/retR",
      "call/n3/retR",
      "call/n4/retR",
      "call/n5/retR",
      "call/n6/retR",
      "call/n7/retR",
      "call/n8/retR",
      "call/n9/retR",
      "call/n10/retR",
      "call/nR/retR",
      "call-compiled/n0/retR",
      "call-compiled/n1/retR",
      "call-compiled/n2/retR",
      "call-compiled/n3/retR",
      "call-compiled/n4/retR",
      "call-compiled/n5/retR",
      "call-compiled/n6/retR",
      "call-compiled/n7/retR",
      "call-compiled/n8/retR",
      "call-compiled/n9/retR",
      "call-compiled/n10/retR",
      "call-compiled/nR/retR",
      "call-from-c/retR",
      "canonicalize-boolean",
      "cdr-register/%rR/%rR/fR",
      "check-closure/fR",
      "check-global-defined/nR/fR",
      "check-in-arity/n0/fR",
      "check-in-arity/n1/fR",
      "check-in-arity/n2/fR",
      "check-in-arity/n3/fR",
      "check-in-arity/n4/fR",
      "check-in-arity/n5/fR",
      "check-in-arity/n6/fR",
      "check-in-arity/n7/fR",
      "check-in-arity/n8/fR",
      "check-in-arity/n9/fR",
      "check-in-arity/n10/fR",
      "check-in-arity/nR/fR",
      "check-in-arity--alt/n0/fR",
      "check-in-arity--alt/n1/fR",
      "check-in-arity--alt/n2/fR",
      "check-in-arity--alt/n3/fR",
      "check-in-arity--alt/n4/fR",
      "check-in-arity--alt/n5/fR",
      "check-in-arity--alt/n6/fR",
      "check-in-arity--alt/n7/fR",
      "check-in-arity--alt/n8/fR",
      "check-in-arity--alt/n9/fR",
      "check-in-arity--alt/n10/fR",
      "check-in-arity--alt/nR/fR",
      "copy-from-literal/nR",
      "copy-from-register/%rR",
      "copy-to-register/%rR",
      "drop",
      "drop-nip",
      "dup",
      "exitvm",
      "fail/retR",
      "gc-if-needed/fR",
      "heap-allocate/n4",
      "heap-allocate/n8",
      "heap-allocate/n12",
      "heap-allocate/n16",
      "heap-allocate/n24",
      "heap-allocate/n32",
      "heap-allocate/n36",
      "heap-allocate/n48",
      "heap-allocate/n52",
      "heap-allocate/n64",
      "heap-allocate/nR",
      "literal-to-register/nR/%rR",
      "nip",
      "nip-drop",
      "nip-five",
      "nip-five-drop",
      "nip-four",
      "nip-four-drop",
      "nip-push-literal/nR",
      "nip-push-register/%rR",
      "nip-six",
      "nip-six-drop",
      "nip-three",
      "nip-three-drop",
      "nip-two",
      "nip-two-drop",
      "nop",
      "one-minus-register/%rR/%rR/fR",
      "one-plus-register/%rR/%rR/fR",
      "pop-to-global/nR/fR",
      "pop-to-global-defined/nR/fR",
      "pop-to-register/%rR",
      "primitive/nR/n0/fR",
      "primitive/nR/n1/fR",
      "primitive/nR/n2/fR",
      "primitive/nR/n3/fR",
      "primitive/nR/n4/fR",
      "primitive/nR/nR/fR",
      "primitive-boolean-canonicalize",
      "primitive-box",
      "primitive-box-get/fR",
      "primitive-box-setb-special/fR",
      "primitive-car/fR",
      "primitive-cdr/fR",
      "primitive-characterp",
      "primitive-cons-special",
      "primitive-consp",
      "primitive-eqp",
      "primitive-fixnum-eqp/fR",
      "primitive-fixnum-not-eqp/fR",
      "primitive-fixnump",
      "primitive-greaterp/fR",
      "primitive-lessp/fR",
      "primitive-negate/fR",
      "primitive-negativep/fR",
      "primitive-non-consp",
      "primitive-non-negativep/fR",
      "primitive-non-nullp",
      "primitive-non-positivep/fR",
      "primitive-non-symbolp",
      "primitive-non-zerop/fR",
      "primitive-not",
      "primitive-not-eqp",
      "primitive-not-greaterp/fR",
      "primitive-not-lessp/fR",
      "primitive-nothingp",
      "primitive-nullp",
      "primitive-one-minus/fR",
      "primitive-one-plus/fR",
      "primitive-positivep/fR",
      "primitive-primordial-divided/fR",
      "primitive-primordial-divided-unsafe/fR",
      "primitive-primordial-minus/fR",
      "primitive-primordial-plus/fR",
      "primitive-primordial-times/fR",
      "primitive-quotient/fR",
      "primitive-quotient-unsafe/fR",
      "primitive-remainder/fR",
      "primitive-remainder-unsafe/fR",
      "primitive-set-carb-special/fR",
      "primitive-set-cdrb-special/fR",
      "primitive-symbolp",
      "primitive-two-divided/fR",
      "primitive-two-quotient/fR",
      "primitive-two-remainder/fR",
      "primitive-two-times/fR",
      "primitive-uniquep",
      "primitive-zerop/fR",
      "procedure-prolog",
      "push-false",
      "push-global/nR/fR",
      "push-literal/nR",
      "push-nil",
      "push-nothing",
      "push-one",
      "push-register/%rR",
      "push-unspecified",
      "push-zero",
      "register-to-register/%rR/%rR",
      "restore-register/%rR",
      "return",
      "save-register/%rR",
      "tail-call/n0/retR",
      "tail-call/n1/retR",
      "tail-call/n2/retR",
      "tail-call/n3/retR",
      "tail-call/n4/retR",
      "tail-call/n5/retR",
      "tail-call/n6/retR",
      "tail-call/n7/retR",
      "tail-call/n8/retR",
      "tail-call/n9/retR",
      "tail-call/n10/retR",
      "tail-call/nR/retR",
      "tail-call-compiled/n0/retR",
      "tail-call-compiled/n1/retR",
      "tail-call-compiled/n2/retR",
      "tail-call-compiled/n3/retR",
      "tail-call-compiled/n4/retR",
      "tail-call-compiled/n5/retR",
      "tail-call-compiled/n6/retR",
      "tail-call-compiled/n7/retR",
      "tail-call-compiled/n8/retR",
      "tail-call-compiled/n9/retR",
      "tail-call-compiled/n10/retR",
      "tail-call-compiled/nR/retR",
      "unreachable",
      "!REPLACEMENT-branch/fR/retR",
      "!REPLACEMENT-branch-if-false/fR/retR",
      "!REPLACEMENT-branch-if-non-positive/fR/fR/retR",
      "!REPLACEMENT-branch-if-not-less/fR/fR/retR",
      "!REPLACEMENT-branch-if-not-null/fR/retR",
      "!REPLACEMENT-branch-if-null/fR/retR",
      "!REPLACEMENT-branch-if-positive/fR/fR/retR",
      "!REPLACEMENT-branch-if-register-non-positive/%rR/fR/fR/retR",
      "!REPLACEMENT-branch-if-register-non-zero/%rR/fR/fR/retR",
      "!REPLACEMENT-branch-if-register-not-null/%rR/fR/retR",
      "!REPLACEMENT-branch-if-register-null/%rR/fR/retR",
      "!REPLACEMENT-branch-if-register-positive/%rR/fR/fR/retR",
      "!REPLACEMENT-branch-if-true/fR/retR",
      "!REPLACEMENT-call/n0/retR",
      "!REPLACEMENT-call/n1/retR",
      "!REPLACEMENT-call/n2/retR",
      "!REPLACEMENT-call/n3/retR",
      "!REPLACEMENT-call/n4/retR",
      "!REPLACEMENT-call/n5/retR",
      "!REPLACEMENT-call/n6/retR",
      "!REPLACEMENT-call/n7/retR",
      "!REPLACEMENT-call/n8/retR",
      "!REPLACEMENT-call/n9/retR",
      "!REPLACEMENT-call/n10/retR",
      "!REPLACEMENT-call/nR/retR",
      "!REPLACEMENT-call-compiled/n0/retR",
      "!REPLACEMENT-call-compiled/n1/retR",
      "!REPLACEMENT-call-compiled/n2/retR",
      "!REPLACEMENT-call-compiled/n3/retR",
      "!REPLACEMENT-call-compiled/n4/retR",
      "!REPLACEMENT-call-compiled/n5/retR",
      "!REPLACEMENT-call-compiled/n6/retR",
      "!REPLACEMENT-call-compiled/n7/retR",
      "!REPLACEMENT-call-compiled/n8/retR",
      "!REPLACEMENT-call-compiled/n9/retR",
      "!REPLACEMENT-call-compiled/n10/retR",
      "!REPLACEMENT-call-compiled/nR/retR",
      "!REPLACEMENT-call-from-c/retR",
      "!REPLACEMENT-cdr-register/%rR/%rR/fR/retR",
      "!REPLACEMENT-check-closure/fR/retR",
      "!REPLACEMENT-check-global-defined/nR/fR/retR",
      "!REPLACEMENT-check-in-arity/n0/fR/retR",
      "!REPLACEMENT-check-in-arity/n1/fR/retR",
      "!REPLACEMENT-check-in-arity/n2/fR/retR",
      "!REPLACEMENT-check-in-arity/n3/fR/retR",
      "!REPLACEMENT-check-in-arity/n4/fR/retR",
      "!REPLACEMENT-check-in-arity/n5/fR/retR",
      "!REPLACEMENT-check-in-arity/n6/fR/retR",
      "!REPLACEMENT-check-in-arity/n7/fR/retR",
      "!REPLACEMENT-check-in-arity/n8/fR/retR",
      "!REPLACEMENT-check-in-arity/n9/fR/retR",
      "!REPLACEMENT-check-in-arity/n10/fR/retR",
      "!REPLACEMENT-check-in-arity/nR/fR/retR",
      "!REPLACEMENT-check-in-arity--alt/n0/fR/retR",
      "!REPLACEMENT-check-in-arity--alt/n1/fR/retR",
      "!REPLACEMENT-check-in-arity--alt/n2/fR/retR",
      "!REPLACEMENT-check-in-arity--alt/n3/fR/retR",
      "!REPLACEMENT-check-in-arity--alt/n4/fR/retR",
      "!REPLACEMENT-check-in-arity--alt/n5/fR/retR",
      "!REPLACEMENT-check-in-arity--alt/n6/fR/retR",
      "!REPLACEMENT-check-in-arity--alt/n7/fR/retR",
      "!REPLACEMENT-check-in-arity--alt/n8/fR/retR",
      "!REPLACEMENT-check-in-arity--alt/n9/fR/retR",
      "!REPLACEMENT-check-in-arity--alt/n10/fR/retR",
      "!REPLACEMENT-check-in-arity--alt/nR/fR/retR",
      "!REPLACEMENT-exitvm/retR",
      "!REPLACEMENT-gc-if-needed/fR/retR",
      "!REPLACEMENT-one-minus-register/%rR/%rR/fR/retR",
      "!REPLACEMENT-one-plus-register/%rR/%rR/fR/retR",
      "!REPLACEMENT-pop-to-global/nR/fR/retR",
      "!REPLACEMENT-pop-to-global-defined/nR/fR/retR",
      "!REPLACEMENT-primitive/nR/n0/fR/retR",
      "!REPLACEMENT-primitive/nR/n1/fR/retR",
      "!REPLACEMENT-primitive/nR/n2/fR/retR",
      "!REPLACEMENT-primitive/nR/n3/fR/retR",
      "!REPLACEMENT-primitive/nR/n4/fR/retR",
      "!REPLACEMENT-primitive/nR/nR/fR/retR",
      "!REPLACEMENT-primitive-box-get/fR/retR",
      "!REPLACEMENT-primitive-box-setb-special/fR/retR",
      "!REPLACEMENT-primitive-car/fR/retR",
      "!REPLACEMENT-primitive-cdr/fR/retR",
      "!REPLACEMENT-primitive-fixnum-eqp/fR/retR",
      "!REPLACEMENT-primitive-fixnum-not-eqp/fR/retR",
      "!REPLACEMENT-primitive-greaterp/fR/retR",
      "!REPLACEMENT-primitive-lessp/fR/retR",
      "!REPLACEMENT-primitive-negate/fR/retR",
      "!REPLACEMENT-primitive-negativep/fR/retR",
      "!REPLACEMENT-primitive-non-negativep/fR/retR",
      "!REPLACEMENT-primitive-non-positivep/fR/retR",
      "!REPLACEMENT-primitive-non-zerop/fR/retR",
      "!REPLACEMENT-primitive-not-greaterp/fR/retR",
      "!REPLACEMENT-primitive-not-lessp/fR/retR",
      "!REPLACEMENT-primitive-one-minus/fR/retR",
      "!REPLACEMENT-primitive-one-plus/fR/retR",
      "!REPLACEMENT-primitive-positivep/fR/retR",
      "!REPLACEMENT-primitive-primordial-divided/fR/retR",
      "!REPLACEMENT-primitive-primordial-divided-unsafe/fR/retR",
      "!REPLACEMENT-primitive-primordial-minus/fR/retR",
      "!REPLACEMENT-primitive-primordial-plus/fR/retR",
      "!REPLACEMENT-primitive-primordial-times/fR/retR",
      "!REPLACEMENT-primitive-quotient/fR/retR",
      "!REPLACEMENT-primitive-quotient-unsafe/fR/retR",
      "!REPLACEMENT-primitive-remainder/fR/retR",
      "!REPLACEMENT-primitive-remainder-unsafe/fR/retR",
      "!REPLACEMENT-primitive-set-carb-special/fR/retR",
      "!REPLACEMENT-primitive-set-cdrb-special/fR/retR",
      "!REPLACEMENT-primitive-two-divided/fR/retR",
      "!REPLACEMENT-primitive-two-quotient/fR/retR",
      "!REPLACEMENT-primitive-two-remainder/fR/retR",
      "!REPLACEMENT-primitive-two-times/fR/retR",
      "!REPLACEMENT-primitive-zerop/fR/retR",
      "!REPLACEMENT-procedure-prolog/retR",
      "!REPLACEMENT-push-global/nR/fR/retR",
      "!REPLACEMENT-return/retR",
      "!REPLACEMENT-tail-call/n0/retR",
      "!REPLACEMENT-tail-call/n1/retR",
      "!REPLACEMENT-tail-call/n2/retR",
      "!REPLACEMENT-tail-call/n3/retR",
      "!REPLACEMENT-tail-call/n4/retR",
      "!REPLACEMENT-tail-call/n5/retR",
      "!REPLACEMENT-tail-call/n6/retR",
      "!REPLACEMENT-tail-call/n7/retR",
      "!REPLACEMENT-tail-call/n8/retR",
      "!REPLACEMENT-tail-call/n9/retR",
      "!REPLACEMENT-tail-call/n10/retR",
      "!REPLACEMENT-tail-call/nR/retR",
      "!REPLACEMENT-tail-call-compiled/n0/retR",
      "!REPLACEMENT-tail-call-compiled/n1/retR",
      "!REPLACEMENT-tail-call-compiled/n2/retR",
      "!REPLACEMENT-tail-call-compiled/n3/retR",
      "!REPLACEMENT-tail-call-compiled/n4/retR",
      "!REPLACEMENT-tail-call-compiled/n5/retR",
      "!REPLACEMENT-tail-call-compiled/n6/retR",
      "!REPLACEMENT-tail-call-compiled/n7/retR",
      "!REPLACEMENT-tail-call-compiled/n8/retR",
      "!REPLACEMENT-tail-call-compiled/n9/retR",
      "!REPLACEMENT-tail-call-compiled/n10/retR",
      "!REPLACEMENT-tail-call-compiled/nR/retR",
      "!REPLACEMENT-unreachable/retR"
    };
// #include <stdlib.h>

// #include "jitterlispvm-specialized-instructions.h"
const size_t
jitterlispvm_specialized_instruction_residual_arities [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      0, /* !INVALID */
      1, /* !BEGINBASICBLOCK */
      0, /* !EXITVM */
      0, /* !DATALOCATIONS */
      0, /* !NOP */
      0, /* !UNREACHABLE0 */
      0, /* !UNREACHABLE1 */
      0, /* !PRETENDTOJUMPANYWHERE */
      1, /* at-depth-to-register/n1/%rR */
      1, /* at-depth-to-register/n2/%rR */
      1, /* at-depth-to-register/n3/%rR */
      1, /* at-depth-to-register/n4/%rR */
      1, /* at-depth-to-register/n5/%rR */
      1, /* at-depth-to-register/n6/%rR */
      1, /* at-depth-to-register/n7/%rR */
      1, /* at-depth-to-register/n8/%rR */
      1, /* at-depth-to-register/n9/%rR */
      1, /* at-depth-to-register/n10/%rR */
      2, /* at-depth-to-register/nR/%rR */
      1, /* branch/fR */
      1, /* branch-if-false/fR */
      2, /* branch-if-non-positive/fR/fR */
      2, /* branch-if-not-less/fR/fR */
      1, /* branch-if-not-null/fR */
      1, /* branch-if-null/fR */
      2, /* branch-if-positive/fR/fR */
      3, /* branch-if-register-non-positive/%rR/fR/fR */
      3, /* branch-if-register-non-zero/%rR/fR/fR */
      2, /* branch-if-register-not-null/%rR/fR */
      2, /* branch-if-register-null/%rR/fR */
      3, /* branch-if-register-positive/%rR/fR/fR */
      1, /* branch-if-true/fR */
      1, /* call/n0/retR */
      1, /* call/n1/retR */
      1, /* call/n2/retR */
      1, /* call/n3/retR */
      1, /* call/n4/retR */
      1, /* call/n5/retR */
      1, /* call/n6/retR */
      1, /* call/n7/retR */
      1, /* call/n8/retR */
      1, /* call/n9/retR */
      1, /* call/n10/retR */
      2, /* call/nR/retR */
      1, /* call-compiled/n0/retR */
      1, /* call-compiled/n1/retR */
      1, /* call-compiled/n2/retR */
      1, /* call-compiled/n3/retR */
      1, /* call-compiled/n4/retR */
      1, /* call-compiled/n5/retR */
      1, /* call-compiled/n6/retR */
      1, /* call-compiled/n7/retR */
      1, /* call-compiled/n8/retR */
      1, /* call-compiled/n9/retR */
      1, /* call-compiled/n10/retR */
      2, /* call-compiled/nR/retR */
      1, /* call-from-c/retR */
      0, /* canonicalize-boolean */
      3, /* cdr-register/%rR/%rR/fR */
      1, /* check-closure/fR */
      2, /* check-global-defined/nR/fR */
      1, /* check-in-arity/n0/fR */
      1, /* check-in-arity/n1/fR */
      1, /* check-in-arity/n2/fR */
      1, /* check-in-arity/n3/fR */
      1, /* check-in-arity/n4/fR */
      1, /* check-in-arity/n5/fR */
      1, /* check-in-arity/n6/fR */
      1, /* check-in-arity/n7/fR */
      1, /* check-in-arity/n8/fR */
      1, /* check-in-arity/n9/fR */
      1, /* check-in-arity/n10/fR */
      2, /* check-in-arity/nR/fR */
      1, /* check-in-arity--alt/n0/fR */
      1, /* check-in-arity--alt/n1/fR */
      1, /* check-in-arity--alt/n2/fR */
      1, /* check-in-arity--alt/n3/fR */
      1, /* check-in-arity--alt/n4/fR */
      1, /* check-in-arity--alt/n5/fR */
      1, /* check-in-arity--alt/n6/fR */
      1, /* check-in-arity--alt/n7/fR */
      1, /* check-in-arity--alt/n8/fR */
      1, /* check-in-arity--alt/n9/fR */
      1, /* check-in-arity--alt/n10/fR */
      2, /* check-in-arity--alt/nR/fR */
      1, /* copy-from-literal/nR */
      1, /* copy-from-register/%rR */
      1, /* copy-to-register/%rR */
      0, /* drop */
      0, /* drop-nip */
      0, /* dup */
      0, /* exitvm */
      1, /* fail/retR */
      1, /* gc-if-needed/fR */
      0, /* heap-allocate/n4 */
      0, /* heap-allocate/n8 */
      0, /* heap-allocate/n12 */
      0, /* heap-allocate/n16 */
      0, /* heap-allocate/n24 */
      0, /* heap-allocate/n32 */
      0, /* heap-allocate/n36 */
      0, /* heap-allocate/n48 */
      0, /* heap-allocate/n52 */
      0, /* heap-allocate/n64 */
      1, /* heap-allocate/nR */
      2, /* literal-to-register/nR/%rR */
      0, /* nip */
      0, /* nip-drop */
      0, /* nip-five */
      0, /* nip-five-drop */
      0, /* nip-four */
      0, /* nip-four-drop */
      1, /* nip-push-literal/nR */
      1, /* nip-push-register/%rR */
      0, /* nip-six */
      0, /* nip-six-drop */
      0, /* nip-three */
      0, /* nip-three-drop */
      0, /* nip-two */
      0, /* nip-two-drop */
      0, /* nop */
      3, /* one-minus-register/%rR/%rR/fR */
      3, /* one-plus-register/%rR/%rR/fR */
      2, /* pop-to-global/nR/fR */
      2, /* pop-to-global-defined/nR/fR */
      1, /* pop-to-register/%rR */
      2, /* primitive/nR/n0/fR */
      2, /* primitive/nR/n1/fR */
      2, /* primitive/nR/n2/fR */
      2, /* primitive/nR/n3/fR */
      2, /* primitive/nR/n4/fR */
      3, /* primitive/nR/nR/fR */
      0, /* primitive-boolean-canonicalize */
      0, /* primitive-box */
      1, /* primitive-box-get/fR */
      1, /* primitive-box-setb-special/fR */
      1, /* primitive-car/fR */
      1, /* primitive-cdr/fR */
      0, /* primitive-characterp */
      0, /* primitive-cons-special */
      0, /* primitive-consp */
      0, /* primitive-eqp */
      1, /* primitive-fixnum-eqp/fR */
      1, /* primitive-fixnum-not-eqp/fR */
      0, /* primitive-fixnump */
      1, /* primitive-greaterp/fR */
      1, /* primitive-lessp/fR */
      1, /* primitive-negate/fR */
      1, /* primitive-negativep/fR */
      0, /* primitive-non-consp */
      1, /* primitive-non-negativep/fR */
      0, /* primitive-non-nullp */
      1, /* primitive-non-positivep/fR */
      0, /* primitive-non-symbolp */
      1, /* primitive-non-zerop/fR */
      0, /* primitive-not */
      0, /* primitive-not-eqp */
      1, /* primitive-not-greaterp/fR */
      1, /* primitive-not-lessp/fR */
      0, /* primitive-nothingp */
      0, /* primitive-nullp */
      1, /* primitive-one-minus/fR */
      1, /* primitive-one-plus/fR */
      1, /* primitive-positivep/fR */
      1, /* primitive-primordial-divided/fR */
      1, /* primitive-primordial-divided-unsafe/fR */
      1, /* primitive-primordial-minus/fR */
      1, /* primitive-primordial-plus/fR */
      1, /* primitive-primordial-times/fR */
      1, /* primitive-quotient/fR */
      1, /* primitive-quotient-unsafe/fR */
      1, /* primitive-remainder/fR */
      1, /* primitive-remainder-unsafe/fR */
      1, /* primitive-set-carb-special/fR */
      1, /* primitive-set-cdrb-special/fR */
      0, /* primitive-symbolp */
      1, /* primitive-two-divided/fR */
      1, /* primitive-two-quotient/fR */
      1, /* primitive-two-remainder/fR */
      1, /* primitive-two-times/fR */
      0, /* primitive-uniquep */
      1, /* primitive-zerop/fR */
      0, /* procedure-prolog */
      0, /* push-false */
      2, /* push-global/nR/fR */
      1, /* push-literal/nR */
      0, /* push-nil */
      0, /* push-nothing */
      0, /* push-one */
      1, /* push-register/%rR */
      0, /* push-unspecified */
      0, /* push-zero */
      2, /* register-to-register/%rR/%rR */
      1, /* restore-register/%rR */
      0, /* return */
      1, /* save-register/%rR */
      1, /* tail-call/n0/retR */
      1, /* tail-call/n1/retR */
      1, /* tail-call/n2/retR */
      1, /* tail-call/n3/retR */
      1, /* tail-call/n4/retR */
      1, /* tail-call/n5/retR */
      1, /* tail-call/n6/retR */
      1, /* tail-call/n7/retR */
      1, /* tail-call/n8/retR */
      1, /* tail-call/n9/retR */
      1, /* tail-call/n10/retR */
      2, /* tail-call/nR/retR */
      1, /* tail-call-compiled/n0/retR */
      1, /* tail-call-compiled/n1/retR */
      1, /* tail-call-compiled/n2/retR */
      1, /* tail-call-compiled/n3/retR */
      1, /* tail-call-compiled/n4/retR */
      1, /* tail-call-compiled/n5/retR */
      1, /* tail-call-compiled/n6/retR */
      1, /* tail-call-compiled/n7/retR */
      1, /* tail-call-compiled/n8/retR */
      1, /* tail-call-compiled/n9/retR */
      1, /* tail-call-compiled/n10/retR */
      2, /* tail-call-compiled/nR/retR */
      0, /* unreachable */
      2, /* !REPLACEMENT-branch/fR/retR */
      2, /* !REPLACEMENT-branch-if-false/fR/retR */
      3, /* !REPLACEMENT-branch-if-non-positive/fR/fR/retR */
      3, /* !REPLACEMENT-branch-if-not-less/fR/fR/retR */
      2, /* !REPLACEMENT-branch-if-not-null/fR/retR */
      2, /* !REPLACEMENT-branch-if-null/fR/retR */
      3, /* !REPLACEMENT-branch-if-positive/fR/fR/retR */
      4, /* !REPLACEMENT-branch-if-register-non-positive/%rR/fR/fR/retR */
      4, /* !REPLACEMENT-branch-if-register-non-zero/%rR/fR/fR/retR */
      3, /* !REPLACEMENT-branch-if-register-not-null/%rR/fR/retR */
      3, /* !REPLACEMENT-branch-if-register-null/%rR/fR/retR */
      4, /* !REPLACEMENT-branch-if-register-positive/%rR/fR/fR/retR */
      2, /* !REPLACEMENT-branch-if-true/fR/retR */
      1, /* !REPLACEMENT-call/n0/retR */
      1, /* !REPLACEMENT-call/n1/retR */
      1, /* !REPLACEMENT-call/n2/retR */
      1, /* !REPLACEMENT-call/n3/retR */
      1, /* !REPLACEMENT-call/n4/retR */
      1, /* !REPLACEMENT-call/n5/retR */
      1, /* !REPLACEMENT-call/n6/retR */
      1, /* !REPLACEMENT-call/n7/retR */
      1, /* !REPLACEMENT-call/n8/retR */
      1, /* !REPLACEMENT-call/n9/retR */
      1, /* !REPLACEMENT-call/n10/retR */
      2, /* !REPLACEMENT-call/nR/retR */
      1, /* !REPLACEMENT-call-compiled/n0/retR */
      1, /* !REPLACEMENT-call-compiled/n1/retR */
      1, /* !REPLACEMENT-call-compiled/n2/retR */
      1, /* !REPLACEMENT-call-compiled/n3/retR */
      1, /* !REPLACEMENT-call-compiled/n4/retR */
      1, /* !REPLACEMENT-call-compiled/n5/retR */
      1, /* !REPLACEMENT-call-compiled/n6/retR */
      1, /* !REPLACEMENT-call-compiled/n7/retR */
      1, /* !REPLACEMENT-call-compiled/n8/retR */
      1, /* !REPLACEMENT-call-compiled/n9/retR */
      1, /* !REPLACEMENT-call-compiled/n10/retR */
      2, /* !REPLACEMENT-call-compiled/nR/retR */
      1, /* !REPLACEMENT-call-from-c/retR */
      4, /* !REPLACEMENT-cdr-register/%rR/%rR/fR/retR */
      2, /* !REPLACEMENT-check-closure/fR/retR */
      3, /* !REPLACEMENT-check-global-defined/nR/fR/retR */
      2, /* !REPLACEMENT-check-in-arity/n0/fR/retR */
      2, /* !REPLACEMENT-check-in-arity/n1/fR/retR */
      2, /* !REPLACEMENT-check-in-arity/n2/fR/retR */
      2, /* !REPLACEMENT-check-in-arity/n3/fR/retR */
      2, /* !REPLACEMENT-check-in-arity/n4/fR/retR */
      2, /* !REPLACEMENT-check-in-arity/n5/fR/retR */
      2, /* !REPLACEMENT-check-in-arity/n6/fR/retR */
      2, /* !REPLACEMENT-check-in-arity/n7/fR/retR */
      2, /* !REPLACEMENT-check-in-arity/n8/fR/retR */
      2, /* !REPLACEMENT-check-in-arity/n9/fR/retR */
      2, /* !REPLACEMENT-check-in-arity/n10/fR/retR */
      3, /* !REPLACEMENT-check-in-arity/nR/fR/retR */
      2, /* !REPLACEMENT-check-in-arity--alt/n0/fR/retR */
      2, /* !REPLACEMENT-check-in-arity--alt/n1/fR/retR */
      2, /* !REPLACEMENT-check-in-arity--alt/n2/fR/retR */
      2, /* !REPLACEMENT-check-in-arity--alt/n3/fR/retR */
      2, /* !REPLACEMENT-check-in-arity--alt/n4/fR/retR */
      2, /* !REPLACEMENT-check-in-arity--alt/n5/fR/retR */
      2, /* !REPLACEMENT-check-in-arity--alt/n6/fR/retR */
      2, /* !REPLACEMENT-check-in-arity--alt/n7/fR/retR */
      2, /* !REPLACEMENT-check-in-arity--alt/n8/fR/retR */
      2, /* !REPLACEMENT-check-in-arity--alt/n9/fR/retR */
      2, /* !REPLACEMENT-check-in-arity--alt/n10/fR/retR */
      3, /* !REPLACEMENT-check-in-arity--alt/nR/fR/retR */
      1, /* !REPLACEMENT-exitvm/retR */
      2, /* !REPLACEMENT-gc-if-needed/fR/retR */
      4, /* !REPLACEMENT-one-minus-register/%rR/%rR/fR/retR */
      4, /* !REPLACEMENT-one-plus-register/%rR/%rR/fR/retR */
      3, /* !REPLACEMENT-pop-to-global/nR/fR/retR */
      3, /* !REPLACEMENT-pop-to-global-defined/nR/fR/retR */
      3, /* !REPLACEMENT-primitive/nR/n0/fR/retR */
      3, /* !REPLACEMENT-primitive/nR/n1/fR/retR */
      3, /* !REPLACEMENT-primitive/nR/n2/fR/retR */
      3, /* !REPLACEMENT-primitive/nR/n3/fR/retR */
      3, /* !REPLACEMENT-primitive/nR/n4/fR/retR */
      4, /* !REPLACEMENT-primitive/nR/nR/fR/retR */
      2, /* !REPLACEMENT-primitive-box-get/fR/retR */
      2, /* !REPLACEMENT-primitive-box-setb-special/fR/retR */
      2, /* !REPLACEMENT-primitive-car/fR/retR */
      2, /* !REPLACEMENT-primitive-cdr/fR/retR */
      2, /* !REPLACEMENT-primitive-fixnum-eqp/fR/retR */
      2, /* !REPLACEMENT-primitive-fixnum-not-eqp/fR/retR */
      2, /* !REPLACEMENT-primitive-greaterp/fR/retR */
      2, /* !REPLACEMENT-primitive-lessp/fR/retR */
      2, /* !REPLACEMENT-primitive-negate/fR/retR */
      2, /* !REPLACEMENT-primitive-negativep/fR/retR */
      2, /* !REPLACEMENT-primitive-non-negativep/fR/retR */
      2, /* !REPLACEMENT-primitive-non-positivep/fR/retR */
      2, /* !REPLACEMENT-primitive-non-zerop/fR/retR */
      2, /* !REPLACEMENT-primitive-not-greaterp/fR/retR */
      2, /* !REPLACEMENT-primitive-not-lessp/fR/retR */
      2, /* !REPLACEMENT-primitive-one-minus/fR/retR */
      2, /* !REPLACEMENT-primitive-one-plus/fR/retR */
      2, /* !REPLACEMENT-primitive-positivep/fR/retR */
      2, /* !REPLACEMENT-primitive-primordial-divided/fR/retR */
      2, /* !REPLACEMENT-primitive-primordial-divided-unsafe/fR/retR */
      2, /* !REPLACEMENT-primitive-primordial-minus/fR/retR */
      2, /* !REPLACEMENT-primitive-primordial-plus/fR/retR */
      2, /* !REPLACEMENT-primitive-primordial-times/fR/retR */
      2, /* !REPLACEMENT-primitive-quotient/fR/retR */
      2, /* !REPLACEMENT-primitive-quotient-unsafe/fR/retR */
      2, /* !REPLACEMENT-primitive-remainder/fR/retR */
      2, /* !REPLACEMENT-primitive-remainder-unsafe/fR/retR */
      2, /* !REPLACEMENT-primitive-set-carb-special/fR/retR */
      2, /* !REPLACEMENT-primitive-set-cdrb-special/fR/retR */
      2, /* !REPLACEMENT-primitive-two-divided/fR/retR */
      2, /* !REPLACEMENT-primitive-two-quotient/fR/retR */
      2, /* !REPLACEMENT-primitive-two-remainder/fR/retR */
      2, /* !REPLACEMENT-primitive-two-times/fR/retR */
      2, /* !REPLACEMENT-primitive-zerop/fR/retR */
      1, /* !REPLACEMENT-procedure-prolog/retR */
      3, /* !REPLACEMENT-push-global/nR/fR/retR */
      1, /* !REPLACEMENT-return/retR */
      1, /* !REPLACEMENT-tail-call/n0/retR */
      1, /* !REPLACEMENT-tail-call/n1/retR */
      1, /* !REPLACEMENT-tail-call/n2/retR */
      1, /* !REPLACEMENT-tail-call/n3/retR */
      1, /* !REPLACEMENT-tail-call/n4/retR */
      1, /* !REPLACEMENT-tail-call/n5/retR */
      1, /* !REPLACEMENT-tail-call/n6/retR */
      1, /* !REPLACEMENT-tail-call/n7/retR */
      1, /* !REPLACEMENT-tail-call/n8/retR */
      1, /* !REPLACEMENT-tail-call/n9/retR */
      1, /* !REPLACEMENT-tail-call/n10/retR */
      2, /* !REPLACEMENT-tail-call/nR/retR */
      1, /* !REPLACEMENT-tail-call-compiled/n0/retR */
      1, /* !REPLACEMENT-tail-call-compiled/n1/retR */
      1, /* !REPLACEMENT-tail-call-compiled/n2/retR */
      1, /* !REPLACEMENT-tail-call-compiled/n3/retR */
      1, /* !REPLACEMENT-tail-call-compiled/n4/retR */
      1, /* !REPLACEMENT-tail-call-compiled/n5/retR */
      1, /* !REPLACEMENT-tail-call-compiled/n6/retR */
      1, /* !REPLACEMENT-tail-call-compiled/n7/retR */
      1, /* !REPLACEMENT-tail-call-compiled/n8/retR */
      1, /* !REPLACEMENT-tail-call-compiled/n9/retR */
      1, /* !REPLACEMENT-tail-call-compiled/n10/retR */
      2, /* !REPLACEMENT-tail-call-compiled/nR/retR */
      1 /* !REPLACEMENT-unreachable/retR */
    };
const unsigned long // FIXME: shall I use a shorter type when possible?
jitterlispvm_specialized_instruction_label_bitmasks [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      /* It's important that !BEGINBASICBLOCK has a zero here: it does not need residual patching. */
      0, /* !INVALID */
      0, /* !BEGINBASICBLOCK */
      0, /* !EXITVM */
      0, /* !DATALOCATIONS */
      0, /* !NOP */
      0, /* !UNREACHABLE0 */
      0, /* !UNREACHABLE1 */
      0, /* !PRETENDTOJUMPANYWHERE */
      0, /* at-depth-to-register/n1/%rR */
      0, /* at-depth-to-register/n2/%rR */
      0, /* at-depth-to-register/n3/%rR */
      0, /* at-depth-to-register/n4/%rR */
      0, /* at-depth-to-register/n5/%rR */
      0, /* at-depth-to-register/n6/%rR */
      0, /* at-depth-to-register/n7/%rR */
      0, /* at-depth-to-register/n8/%rR */
      0, /* at-depth-to-register/n9/%rR */
      0, /* at-depth-to-register/n10/%rR */
      0, /* at-depth-to-register/nR/%rR */
      0 | (1UL << 0), /* branch/fR */
      0 | (1UL << 0), /* branch-if-false/fR */
      0 | (1UL << 0) | (1UL << 1), /* branch-if-non-positive/fR/fR */
      0 | (1UL << 0) | (1UL << 1), /* branch-if-not-less/fR/fR */
      0 | (1UL << 0), /* branch-if-not-null/fR */
      0 | (1UL << 0), /* branch-if-null/fR */
      0 | (1UL << 0) | (1UL << 1), /* branch-if-positive/fR/fR */
      0 | (1UL << 1) | (1UL << 2), /* branch-if-register-non-positive/%rR/fR/fR */
      0 | (1UL << 1) | (1UL << 2), /* branch-if-register-non-zero/%rR/fR/fR */
      0 | (1UL << 1), /* branch-if-register-not-null/%rR/fR */
      0 | (1UL << 1), /* branch-if-register-null/%rR/fR */
      0 | (1UL << 1) | (1UL << 2), /* branch-if-register-positive/%rR/fR/fR */
      0 | (1UL << 0), /* branch-if-true/fR */
      0, /* call/n0/retR */
      0, /* call/n1/retR */
      0, /* call/n2/retR */
      0, /* call/n3/retR */
      0, /* call/n4/retR */
      0, /* call/n5/retR */
      0, /* call/n6/retR */
      0, /* call/n7/retR */
      0, /* call/n8/retR */
      0, /* call/n9/retR */
      0, /* call/n10/retR */
      0, /* call/nR/retR */
      0, /* call-compiled/n0/retR */
      0, /* call-compiled/n1/retR */
      0, /* call-compiled/n2/retR */
      0, /* call-compiled/n3/retR */
      0, /* call-compiled/n4/retR */
      0, /* call-compiled/n5/retR */
      0, /* call-compiled/n6/retR */
      0, /* call-compiled/n7/retR */
      0, /* call-compiled/n8/retR */
      0, /* call-compiled/n9/retR */
      0, /* call-compiled/n10/retR */
      0, /* call-compiled/nR/retR */
      0, /* call-from-c/retR */
      0, /* canonicalize-boolean */
      0 | (1UL << 2), /* cdr-register/%rR/%rR/fR */
      0 | (1UL << 0), /* check-closure/fR */
      0 | (1UL << 1), /* check-global-defined/nR/fR */
      0 | (1UL << 0), /* check-in-arity/n0/fR */
      0 | (1UL << 0), /* check-in-arity/n1/fR */
      0 | (1UL << 0), /* check-in-arity/n2/fR */
      0 | (1UL << 0), /* check-in-arity/n3/fR */
      0 | (1UL << 0), /* check-in-arity/n4/fR */
      0 | (1UL << 0), /* check-in-arity/n5/fR */
      0 | (1UL << 0), /* check-in-arity/n6/fR */
      0 | (1UL << 0), /* check-in-arity/n7/fR */
      0 | (1UL << 0), /* check-in-arity/n8/fR */
      0 | (1UL << 0), /* check-in-arity/n9/fR */
      0 | (1UL << 0), /* check-in-arity/n10/fR */
      0 | (1UL << 1), /* check-in-arity/nR/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n0/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n1/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n2/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n3/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n4/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n5/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n6/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n7/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n8/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n9/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n10/fR */
      0 | (1UL << 1), /* check-in-arity--alt/nR/fR */
      0, /* copy-from-literal/nR */
      0, /* copy-from-register/%rR */
      0, /* copy-to-register/%rR */
      0, /* drop */
      0, /* drop-nip */
      0, /* dup */
      0, /* exitvm */
      0, /* fail/retR */
      0 | (1UL << 0), /* gc-if-needed/fR */
      0, /* heap-allocate/n4 */
      0, /* heap-allocate/n8 */
      0, /* heap-allocate/n12 */
      0, /* heap-allocate/n16 */
      0, /* heap-allocate/n24 */
      0, /* heap-allocate/n32 */
      0, /* heap-allocate/n36 */
      0, /* heap-allocate/n48 */
      0, /* heap-allocate/n52 */
      0, /* heap-allocate/n64 */
      0, /* heap-allocate/nR */
      0, /* literal-to-register/nR/%rR */
      0, /* nip */
      0, /* nip-drop */
      0, /* nip-five */
      0, /* nip-five-drop */
      0, /* nip-four */
      0, /* nip-four-drop */
      0, /* nip-push-literal/nR */
      0, /* nip-push-register/%rR */
      0, /* nip-six */
      0, /* nip-six-drop */
      0, /* nip-three */
      0, /* nip-three-drop */
      0, /* nip-two */
      0, /* nip-two-drop */
      0, /* nop */
      0 | (1UL << 2), /* one-minus-register/%rR/%rR/fR */
      0 | (1UL << 2), /* one-plus-register/%rR/%rR/fR */
      0 | (1UL << 1), /* pop-to-global/nR/fR */
      0 | (1UL << 1), /* pop-to-global-defined/nR/fR */
      0, /* pop-to-register/%rR */
      0 | (1UL << 1), /* primitive/nR/n0/fR */
      0 | (1UL << 1), /* primitive/nR/n1/fR */
      0 | (1UL << 1), /* primitive/nR/n2/fR */
      0 | (1UL << 1), /* primitive/nR/n3/fR */
      0 | (1UL << 1), /* primitive/nR/n4/fR */
      0 | (1UL << 2), /* primitive/nR/nR/fR */
      0, /* primitive-boolean-canonicalize */
      0, /* primitive-box */
      0 | (1UL << 0), /* primitive-box-get/fR */
      0 | (1UL << 0), /* primitive-box-setb-special/fR */
      0 | (1UL << 0), /* primitive-car/fR */
      0 | (1UL << 0), /* primitive-cdr/fR */
      0, /* primitive-characterp */
      0, /* primitive-cons-special */
      0, /* primitive-consp */
      0, /* primitive-eqp */
      0 | (1UL << 0), /* primitive-fixnum-eqp/fR */
      0 | (1UL << 0), /* primitive-fixnum-not-eqp/fR */
      0, /* primitive-fixnump */
      0 | (1UL << 0), /* primitive-greaterp/fR */
      0 | (1UL << 0), /* primitive-lessp/fR */
      0 | (1UL << 0), /* primitive-negate/fR */
      0 | (1UL << 0), /* primitive-negativep/fR */
      0, /* primitive-non-consp */
      0 | (1UL << 0), /* primitive-non-negativep/fR */
      0, /* primitive-non-nullp */
      0 | (1UL << 0), /* primitive-non-positivep/fR */
      0, /* primitive-non-symbolp */
      0 | (1UL << 0), /* primitive-non-zerop/fR */
      0, /* primitive-not */
      0, /* primitive-not-eqp */
      0 | (1UL << 0), /* primitive-not-greaterp/fR */
      0 | (1UL << 0), /* primitive-not-lessp/fR */
      0, /* primitive-nothingp */
      0, /* primitive-nullp */
      0 | (1UL << 0), /* primitive-one-minus/fR */
      0 | (1UL << 0), /* primitive-one-plus/fR */
      0 | (1UL << 0), /* primitive-positivep/fR */
      0 | (1UL << 0), /* primitive-primordial-divided/fR */
      0 | (1UL << 0), /* primitive-primordial-divided-unsafe/fR */
      0 | (1UL << 0), /* primitive-primordial-minus/fR */
      0 | (1UL << 0), /* primitive-primordial-plus/fR */
      0 | (1UL << 0), /* primitive-primordial-times/fR */
      0 | (1UL << 0), /* primitive-quotient/fR */
      0 | (1UL << 0), /* primitive-quotient-unsafe/fR */
      0 | (1UL << 0), /* primitive-remainder/fR */
      0 | (1UL << 0), /* primitive-remainder-unsafe/fR */
      0 | (1UL << 0), /* primitive-set-carb-special/fR */
      0 | (1UL << 0), /* primitive-set-cdrb-special/fR */
      0, /* primitive-symbolp */
      0 | (1UL << 0), /* primitive-two-divided/fR */
      0 | (1UL << 0), /* primitive-two-quotient/fR */
      0 | (1UL << 0), /* primitive-two-remainder/fR */
      0 | (1UL << 0), /* primitive-two-times/fR */
      0, /* primitive-uniquep */
      0 | (1UL << 0), /* primitive-zerop/fR */
      0, /* procedure-prolog */
      0, /* push-false */
      0 | (1UL << 1), /* push-global/nR/fR */
      0, /* push-literal/nR */
      0, /* push-nil */
      0, /* push-nothing */
      0, /* push-one */
      0, /* push-register/%rR */
      0, /* push-unspecified */
      0, /* push-zero */
      0, /* register-to-register/%rR/%rR */
      0, /* restore-register/%rR */
      0, /* return */
      0, /* save-register/%rR */
      0, /* tail-call/n0/retR */
      0, /* tail-call/n1/retR */
      0, /* tail-call/n2/retR */
      0, /* tail-call/n3/retR */
      0, /* tail-call/n4/retR */
      0, /* tail-call/n5/retR */
      0, /* tail-call/n6/retR */
      0, /* tail-call/n7/retR */
      0, /* tail-call/n8/retR */
      0, /* tail-call/n9/retR */
      0, /* tail-call/n10/retR */
      0, /* tail-call/nR/retR */
      0, /* tail-call-compiled/n0/retR */
      0, /* tail-call-compiled/n1/retR */
      0, /* tail-call-compiled/n2/retR */
      0, /* tail-call-compiled/n3/retR */
      0, /* tail-call-compiled/n4/retR */
      0, /* tail-call-compiled/n5/retR */
      0, /* tail-call-compiled/n6/retR */
      0, /* tail-call-compiled/n7/retR */
      0, /* tail-call-compiled/n8/retR */
      0, /* tail-call-compiled/n9/retR */
      0, /* tail-call-compiled/n10/retR */
      0, /* tail-call-compiled/nR/retR */
      0, /* unreachable */
      0 | (1UL << 0), /* !REPLACEMENT-branch/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-branch-if-false/fR/retR */
      0 | (1UL << 0) | (1UL << 1), /* !REPLACEMENT-branch-if-non-positive/fR/fR/retR */
      0 | (1UL << 0) | (1UL << 1), /* !REPLACEMENT-branch-if-not-less/fR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-branch-if-not-null/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-branch-if-null/fR/retR */
      0 | (1UL << 0) | (1UL << 1), /* !REPLACEMENT-branch-if-positive/fR/fR/retR */
      0 | (1UL << 1) | (1UL << 2), /* !REPLACEMENT-branch-if-register-non-positive/%rR/fR/fR/retR */
      0 | (1UL << 1) | (1UL << 2), /* !REPLACEMENT-branch-if-register-non-zero/%rR/fR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-branch-if-register-not-null/%rR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-branch-if-register-null/%rR/fR/retR */
      0 | (1UL << 1) | (1UL << 2), /* !REPLACEMENT-branch-if-register-positive/%rR/fR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-branch-if-true/fR/retR */
      0, /* !REPLACEMENT-call/n0/retR */
      0, /* !REPLACEMENT-call/n1/retR */
      0, /* !REPLACEMENT-call/n2/retR */
      0, /* !REPLACEMENT-call/n3/retR */
      0, /* !REPLACEMENT-call/n4/retR */
      0, /* !REPLACEMENT-call/n5/retR */
      0, /* !REPLACEMENT-call/n6/retR */
      0, /* !REPLACEMENT-call/n7/retR */
      0, /* !REPLACEMENT-call/n8/retR */
      0, /* !REPLACEMENT-call/n9/retR */
      0, /* !REPLACEMENT-call/n10/retR */
      0, /* !REPLACEMENT-call/nR/retR */
      0, /* !REPLACEMENT-call-compiled/n0/retR */
      0, /* !REPLACEMENT-call-compiled/n1/retR */
      0, /* !REPLACEMENT-call-compiled/n2/retR */
      0, /* !REPLACEMENT-call-compiled/n3/retR */
      0, /* !REPLACEMENT-call-compiled/n4/retR */
      0, /* !REPLACEMENT-call-compiled/n5/retR */
      0, /* !REPLACEMENT-call-compiled/n6/retR */
      0, /* !REPLACEMENT-call-compiled/n7/retR */
      0, /* !REPLACEMENT-call-compiled/n8/retR */
      0, /* !REPLACEMENT-call-compiled/n9/retR */
      0, /* !REPLACEMENT-call-compiled/n10/retR */
      0, /* !REPLACEMENT-call-compiled/nR/retR */
      0, /* !REPLACEMENT-call-from-c/retR */
      0 | (1UL << 2), /* !REPLACEMENT-cdr-register/%rR/%rR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-closure/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-check-global-defined/nR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity/n0/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity/n1/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity/n2/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity/n3/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity/n4/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity/n5/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity/n6/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity/n7/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity/n8/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity/n9/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity/n10/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-check-in-arity/nR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity--alt/n0/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity--alt/n1/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity--alt/n2/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity--alt/n3/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity--alt/n4/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity--alt/n5/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity--alt/n6/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity--alt/n7/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity--alt/n8/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity--alt/n9/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-check-in-arity--alt/n10/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-check-in-arity--alt/nR/fR/retR */
      0, /* !REPLACEMENT-exitvm/retR */
      0 | (1UL << 0), /* !REPLACEMENT-gc-if-needed/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-one-minus-register/%rR/%rR/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-one-plus-register/%rR/%rR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-pop-to-global/nR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-pop-to-global-defined/nR/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-primitive/nR/n0/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-primitive/nR/n1/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-primitive/nR/n2/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-primitive/nR/n3/fR/retR */
      0 | (1UL << 1), /* !REPLACEMENT-primitive/nR/n4/fR/retR */
      0 | (1UL << 2), /* !REPLACEMENT-primitive/nR/nR/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-box-get/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-box-setb-special/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-car/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-cdr/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-fixnum-eqp/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-fixnum-not-eqp/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-greaterp/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-lessp/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-negate/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-negativep/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-non-negativep/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-non-positivep/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-non-zerop/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-not-greaterp/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-not-lessp/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-one-minus/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-one-plus/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-positivep/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-primordial-divided/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-primordial-divided-unsafe/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-primordial-minus/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-primordial-plus/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-primordial-times/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-quotient/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-quotient-unsafe/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-remainder/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-remainder-unsafe/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-set-carb-special/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-set-cdrb-special/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-two-divided/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-two-quotient/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-two-remainder/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-two-times/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-primitive-zerop/fR/retR */
      0, /* !REPLACEMENT-procedure-prolog/retR */
      0 | (1UL << 1), /* !REPLACEMENT-push-global/nR/fR/retR */
      0, /* !REPLACEMENT-return/retR */
      0, /* !REPLACEMENT-tail-call/n0/retR */
      0, /* !REPLACEMENT-tail-call/n1/retR */
      0, /* !REPLACEMENT-tail-call/n2/retR */
      0, /* !REPLACEMENT-tail-call/n3/retR */
      0, /* !REPLACEMENT-tail-call/n4/retR */
      0, /* !REPLACEMENT-tail-call/n5/retR */
      0, /* !REPLACEMENT-tail-call/n6/retR */
      0, /* !REPLACEMENT-tail-call/n7/retR */
      0, /* !REPLACEMENT-tail-call/n8/retR */
      0, /* !REPLACEMENT-tail-call/n9/retR */
      0, /* !REPLACEMENT-tail-call/n10/retR */
      0, /* !REPLACEMENT-tail-call/nR/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n0/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n1/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n2/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n3/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n4/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n5/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n6/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n7/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n8/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n9/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n10/retR */
      0, /* !REPLACEMENT-tail-call-compiled/nR/retR */
      0 /* !REPLACEMENT-unreachable/retR */
    };
#ifdef JITTER_HAVE_PATCH_IN
const unsigned long // FIXME: shall I use a shorter type when possible?
jitterlispvm_specialized_instruction_fast_label_bitmasks [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      /* It's important that !BEGINBASICBLOCK has a zero here: it does not need residual patching. */
      0, /* !INVALID */
      0, /* !BEGINBASICBLOCK */
      0, /* !EXITVM */
      0, /* !DATALOCATIONS */
      0, /* !NOP */
      0, /* !UNREACHABLE0 */
      0, /* !UNREACHABLE1 */
      0, /* !PRETENDTOJUMPANYWHERE */
      0, /* at-depth-to-register/n1/%rR */
      0, /* at-depth-to-register/n2/%rR */
      0, /* at-depth-to-register/n3/%rR */
      0, /* at-depth-to-register/n4/%rR */
      0, /* at-depth-to-register/n5/%rR */
      0, /* at-depth-to-register/n6/%rR */
      0, /* at-depth-to-register/n7/%rR */
      0, /* at-depth-to-register/n8/%rR */
      0, /* at-depth-to-register/n9/%rR */
      0, /* at-depth-to-register/n10/%rR */
      0, /* at-depth-to-register/nR/%rR */
      0 | (1UL << 0), /* branch/fR */
      0 | (1UL << 0), /* branch-if-false/fR */
      0 | (1UL << 0) | (1UL << 1), /* branch-if-non-positive/fR/fR */
      0 | (1UL << 0) | (1UL << 1), /* branch-if-not-less/fR/fR */
      0 | (1UL << 0), /* branch-if-not-null/fR */
      0 | (1UL << 0), /* branch-if-null/fR */
      0 | (1UL << 0) | (1UL << 1), /* branch-if-positive/fR/fR */
      0 | (1UL << 1) | (1UL << 2), /* branch-if-register-non-positive/%rR/fR/fR */
      0 | (1UL << 1) | (1UL << 2), /* branch-if-register-non-zero/%rR/fR/fR */
      0 | (1UL << 1), /* branch-if-register-not-null/%rR/fR */
      0 | (1UL << 1), /* branch-if-register-null/%rR/fR */
      0 | (1UL << 1) | (1UL << 2), /* branch-if-register-positive/%rR/fR/fR */
      0 | (1UL << 0), /* branch-if-true/fR */
      0, /* call/n0/retR */
      0, /* call/n1/retR */
      0, /* call/n2/retR */
      0, /* call/n3/retR */
      0, /* call/n4/retR */
      0, /* call/n5/retR */
      0, /* call/n6/retR */
      0, /* call/n7/retR */
      0, /* call/n8/retR */
      0, /* call/n9/retR */
      0, /* call/n10/retR */
      0, /* call/nR/retR */
      0, /* call-compiled/n0/retR */
      0, /* call-compiled/n1/retR */
      0, /* call-compiled/n2/retR */
      0, /* call-compiled/n3/retR */
      0, /* call-compiled/n4/retR */
      0, /* call-compiled/n5/retR */
      0, /* call-compiled/n6/retR */
      0, /* call-compiled/n7/retR */
      0, /* call-compiled/n8/retR */
      0, /* call-compiled/n9/retR */
      0, /* call-compiled/n10/retR */
      0, /* call-compiled/nR/retR */
      0, /* call-from-c/retR */
      0, /* canonicalize-boolean */
      0 | (1UL << 2), /* cdr-register/%rR/%rR/fR */
      0 | (1UL << 0), /* check-closure/fR */
      0 | (1UL << 1), /* check-global-defined/nR/fR */
      0 | (1UL << 0), /* check-in-arity/n0/fR */
      0 | (1UL << 0), /* check-in-arity/n1/fR */
      0 | (1UL << 0), /* check-in-arity/n2/fR */
      0 | (1UL << 0), /* check-in-arity/n3/fR */
      0 | (1UL << 0), /* check-in-arity/n4/fR */
      0 | (1UL << 0), /* check-in-arity/n5/fR */
      0 | (1UL << 0), /* check-in-arity/n6/fR */
      0 | (1UL << 0), /* check-in-arity/n7/fR */
      0 | (1UL << 0), /* check-in-arity/n8/fR */
      0 | (1UL << 0), /* check-in-arity/n9/fR */
      0 | (1UL << 0), /* check-in-arity/n10/fR */
      0 | (1UL << 1), /* check-in-arity/nR/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n0/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n1/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n2/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n3/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n4/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n5/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n6/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n7/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n8/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n9/fR */
      0 | (1UL << 0), /* check-in-arity--alt/n10/fR */
      0 | (1UL << 1), /* check-in-arity--alt/nR/fR */
      0, /* copy-from-literal/nR */
      0, /* copy-from-register/%rR */
      0, /* copy-to-register/%rR */
      0, /* drop */
      0, /* drop-nip */
      0, /* dup */
      0, /* exitvm */
      0, /* fail/retR */
      0 | (1UL << 0), /* gc-if-needed/fR */
      0, /* heap-allocate/n4 */
      0, /* heap-allocate/n8 */
      0, /* heap-allocate/n12 */
      0, /* heap-allocate/n16 */
      0, /* heap-allocate/n24 */
      0, /* heap-allocate/n32 */
      0, /* heap-allocate/n36 */
      0, /* heap-allocate/n48 */
      0, /* heap-allocate/n52 */
      0, /* heap-allocate/n64 */
      0, /* heap-allocate/nR */
      0, /* literal-to-register/nR/%rR */
      0, /* nip */
      0, /* nip-drop */
      0, /* nip-five */
      0, /* nip-five-drop */
      0, /* nip-four */
      0, /* nip-four-drop */
      0, /* nip-push-literal/nR */
      0, /* nip-push-register/%rR */
      0, /* nip-six */
      0, /* nip-six-drop */
      0, /* nip-three */
      0, /* nip-three-drop */
      0, /* nip-two */
      0, /* nip-two-drop */
      0, /* nop */
      0 | (1UL << 2), /* one-minus-register/%rR/%rR/fR */
      0 | (1UL << 2), /* one-plus-register/%rR/%rR/fR */
      0 | (1UL << 1), /* pop-to-global/nR/fR */
      0 | (1UL << 1), /* pop-to-global-defined/nR/fR */
      0, /* pop-to-register/%rR */
      0 | (1UL << 1), /* primitive/nR/n0/fR */
      0 | (1UL << 1), /* primitive/nR/n1/fR */
      0 | (1UL << 1), /* primitive/nR/n2/fR */
      0 | (1UL << 1), /* primitive/nR/n3/fR */
      0 | (1UL << 1), /* primitive/nR/n4/fR */
      0 | (1UL << 2), /* primitive/nR/nR/fR */
      0, /* primitive-boolean-canonicalize */
      0, /* primitive-box */
      0 | (1UL << 0), /* primitive-box-get/fR */
      0 | (1UL << 0), /* primitive-box-setb-special/fR */
      0 | (1UL << 0), /* primitive-car/fR */
      0 | (1UL << 0), /* primitive-cdr/fR */
      0, /* primitive-characterp */
      0, /* primitive-cons-special */
      0, /* primitive-consp */
      0, /* primitive-eqp */
      0 | (1UL << 0), /* primitive-fixnum-eqp/fR */
      0 | (1UL << 0), /* primitive-fixnum-not-eqp/fR */
      0, /* primitive-fixnump */
      0 | (1UL << 0), /* primitive-greaterp/fR */
      0 | (1UL << 0), /* primitive-lessp/fR */
      0 | (1UL << 0), /* primitive-negate/fR */
      0 | (1UL << 0), /* primitive-negativep/fR */
      0, /* primitive-non-consp */
      0 | (1UL << 0), /* primitive-non-negativep/fR */
      0, /* primitive-non-nullp */
      0 | (1UL << 0), /* primitive-non-positivep/fR */
      0, /* primitive-non-symbolp */
      0 | (1UL << 0), /* primitive-non-zerop/fR */
      0, /* primitive-not */
      0, /* primitive-not-eqp */
      0 | (1UL << 0), /* primitive-not-greaterp/fR */
      0 | (1UL << 0), /* primitive-not-lessp/fR */
      0, /* primitive-nothingp */
      0, /* primitive-nullp */
      0 | (1UL << 0), /* primitive-one-minus/fR */
      0 | (1UL << 0), /* primitive-one-plus/fR */
      0 | (1UL << 0), /* primitive-positivep/fR */
      0 | (1UL << 0), /* primitive-primordial-divided/fR */
      0 | (1UL << 0), /* primitive-primordial-divided-unsafe/fR */
      0 | (1UL << 0), /* primitive-primordial-minus/fR */
      0 | (1UL << 0), /* primitive-primordial-plus/fR */
      0 | (1UL << 0), /* primitive-primordial-times/fR */
      0 | (1UL << 0), /* primitive-quotient/fR */
      0 | (1UL << 0), /* primitive-quotient-unsafe/fR */
      0 | (1UL << 0), /* primitive-remainder/fR */
      0 | (1UL << 0), /* primitive-remainder-unsafe/fR */
      0 | (1UL << 0), /* primitive-set-carb-special/fR */
      0 | (1UL << 0), /* primitive-set-cdrb-special/fR */
      0, /* primitive-symbolp */
      0 | (1UL << 0), /* primitive-two-divided/fR */
      0 | (1UL << 0), /* primitive-two-quotient/fR */
      0 | (1UL << 0), /* primitive-two-remainder/fR */
      0 | (1UL << 0), /* primitive-two-times/fR */
      0, /* primitive-uniquep */
      0 | (1UL << 0), /* primitive-zerop/fR */
      0, /* procedure-prolog */
      0, /* push-false */
      0 | (1UL << 1), /* push-global/nR/fR */
      0, /* push-literal/nR */
      0, /* push-nil */
      0, /* push-nothing */
      0, /* push-one */
      0, /* push-register/%rR */
      0, /* push-unspecified */
      0, /* push-zero */
      0, /* register-to-register/%rR/%rR */
      0, /* restore-register/%rR */
      0, /* return */
      0, /* save-register/%rR */
      0, /* tail-call/n0/retR */
      0, /* tail-call/n1/retR */
      0, /* tail-call/n2/retR */
      0, /* tail-call/n3/retR */
      0, /* tail-call/n4/retR */
      0, /* tail-call/n5/retR */
      0, /* tail-call/n6/retR */
      0, /* tail-call/n7/retR */
      0, /* tail-call/n8/retR */
      0, /* tail-call/n9/retR */
      0, /* tail-call/n10/retR */
      0, /* tail-call/nR/retR */
      0, /* tail-call-compiled/n0/retR */
      0, /* tail-call-compiled/n1/retR */
      0, /* tail-call-compiled/n2/retR */
      0, /* tail-call-compiled/n3/retR */
      0, /* tail-call-compiled/n4/retR */
      0, /* tail-call-compiled/n5/retR */
      0, /* tail-call-compiled/n6/retR */
      0, /* tail-call-compiled/n7/retR */
      0, /* tail-call-compiled/n8/retR */
      0, /* tail-call-compiled/n9/retR */
      0, /* tail-call-compiled/n10/retR */
      0, /* tail-call-compiled/nR/retR */
      0, /* unreachable */
      0, /* !REPLACEMENT-branch/fR/retR */
      0, /* !REPLACEMENT-branch-if-false/fR/retR */
      0, /* !REPLACEMENT-branch-if-non-positive/fR/fR/retR */
      0, /* !REPLACEMENT-branch-if-not-less/fR/fR/retR */
      0, /* !REPLACEMENT-branch-if-not-null/fR/retR */
      0, /* !REPLACEMENT-branch-if-null/fR/retR */
      0, /* !REPLACEMENT-branch-if-positive/fR/fR/retR */
      0, /* !REPLACEMENT-branch-if-register-non-positive/%rR/fR/fR/retR */
      0, /* !REPLACEMENT-branch-if-register-non-zero/%rR/fR/fR/retR */
      0, /* !REPLACEMENT-branch-if-register-not-null/%rR/fR/retR */
      0, /* !REPLACEMENT-branch-if-register-null/%rR/fR/retR */
      0, /* !REPLACEMENT-branch-if-register-positive/%rR/fR/fR/retR */
      0, /* !REPLACEMENT-branch-if-true/fR/retR */
      0, /* !REPLACEMENT-call/n0/retR */
      0, /* !REPLACEMENT-call/n1/retR */
      0, /* !REPLACEMENT-call/n2/retR */
      0, /* !REPLACEMENT-call/n3/retR */
      0, /* !REPLACEMENT-call/n4/retR */
      0, /* !REPLACEMENT-call/n5/retR */
      0, /* !REPLACEMENT-call/n6/retR */
      0, /* !REPLACEMENT-call/n7/retR */
      0, /* !REPLACEMENT-call/n8/retR */
      0, /* !REPLACEMENT-call/n9/retR */
      0, /* !REPLACEMENT-call/n10/retR */
      0, /* !REPLACEMENT-call/nR/retR */
      0, /* !REPLACEMENT-call-compiled/n0/retR */
      0, /* !REPLACEMENT-call-compiled/n1/retR */
      0, /* !REPLACEMENT-call-compiled/n2/retR */
      0, /* !REPLACEMENT-call-compiled/n3/retR */
      0, /* !REPLACEMENT-call-compiled/n4/retR */
      0, /* !REPLACEMENT-call-compiled/n5/retR */
      0, /* !REPLACEMENT-call-compiled/n6/retR */
      0, /* !REPLACEMENT-call-compiled/n7/retR */
      0, /* !REPLACEMENT-call-compiled/n8/retR */
      0, /* !REPLACEMENT-call-compiled/n9/retR */
      0, /* !REPLACEMENT-call-compiled/n10/retR */
      0, /* !REPLACEMENT-call-compiled/nR/retR */
      0, /* !REPLACEMENT-call-from-c/retR */
      0, /* !REPLACEMENT-cdr-register/%rR/%rR/fR/retR */
      0, /* !REPLACEMENT-check-closure/fR/retR */
      0, /* !REPLACEMENT-check-global-defined/nR/fR/retR */
      0, /* !REPLACEMENT-check-in-arity/n0/fR/retR */
      0, /* !REPLACEMENT-check-in-arity/n1/fR/retR */
      0, /* !REPLACEMENT-check-in-arity/n2/fR/retR */
      0, /* !REPLACEMENT-check-in-arity/n3/fR/retR */
      0, /* !REPLACEMENT-check-in-arity/n4/fR/retR */
      0, /* !REPLACEMENT-check-in-arity/n5/fR/retR */
      0, /* !REPLACEMENT-check-in-arity/n6/fR/retR */
      0, /* !REPLACEMENT-check-in-arity/n7/fR/retR */
      0, /* !REPLACEMENT-check-in-arity/n8/fR/retR */
      0, /* !REPLACEMENT-check-in-arity/n9/fR/retR */
      0, /* !REPLACEMENT-check-in-arity/n10/fR/retR */
      0, /* !REPLACEMENT-check-in-arity/nR/fR/retR */
      0, /* !REPLACEMENT-check-in-arity--alt/n0/fR/retR */
      0, /* !REPLACEMENT-check-in-arity--alt/n1/fR/retR */
      0, /* !REPLACEMENT-check-in-arity--alt/n2/fR/retR */
      0, /* !REPLACEMENT-check-in-arity--alt/n3/fR/retR */
      0, /* !REPLACEMENT-check-in-arity--alt/n4/fR/retR */
      0, /* !REPLACEMENT-check-in-arity--alt/n5/fR/retR */
      0, /* !REPLACEMENT-check-in-arity--alt/n6/fR/retR */
      0, /* !REPLACEMENT-check-in-arity--alt/n7/fR/retR */
      0, /* !REPLACEMENT-check-in-arity--alt/n8/fR/retR */
      0, /* !REPLACEMENT-check-in-arity--alt/n9/fR/retR */
      0, /* !REPLACEMENT-check-in-arity--alt/n10/fR/retR */
      0, /* !REPLACEMENT-check-in-arity--alt/nR/fR/retR */
      0, /* !REPLACEMENT-exitvm/retR */
      0, /* !REPLACEMENT-gc-if-needed/fR/retR */
      0, /* !REPLACEMENT-one-minus-register/%rR/%rR/fR/retR */
      0, /* !REPLACEMENT-one-plus-register/%rR/%rR/fR/retR */
      0, /* !REPLACEMENT-pop-to-global/nR/fR/retR */
      0, /* !REPLACEMENT-pop-to-global-defined/nR/fR/retR */
      0, /* !REPLACEMENT-primitive/nR/n0/fR/retR */
      0, /* !REPLACEMENT-primitive/nR/n1/fR/retR */
      0, /* !REPLACEMENT-primitive/nR/n2/fR/retR */
      0, /* !REPLACEMENT-primitive/nR/n3/fR/retR */
      0, /* !REPLACEMENT-primitive/nR/n4/fR/retR */
      0, /* !REPLACEMENT-primitive/nR/nR/fR/retR */
      0, /* !REPLACEMENT-primitive-box-get/fR/retR */
      0, /* !REPLACEMENT-primitive-box-setb-special/fR/retR */
      0, /* !REPLACEMENT-primitive-car/fR/retR */
      0, /* !REPLACEMENT-primitive-cdr/fR/retR */
      0, /* !REPLACEMENT-primitive-fixnum-eqp/fR/retR */
      0, /* !REPLACEMENT-primitive-fixnum-not-eqp/fR/retR */
      0, /* !REPLACEMENT-primitive-greaterp/fR/retR */
      0, /* !REPLACEMENT-primitive-lessp/fR/retR */
      0, /* !REPLACEMENT-primitive-negate/fR/retR */
      0, /* !REPLACEMENT-primitive-negativep/fR/retR */
      0, /* !REPLACEMENT-primitive-non-negativep/fR/retR */
      0, /* !REPLACEMENT-primitive-non-positivep/fR/retR */
      0, /* !REPLACEMENT-primitive-non-zerop/fR/retR */
      0, /* !REPLACEMENT-primitive-not-greaterp/fR/retR */
      0, /* !REPLACEMENT-primitive-not-lessp/fR/retR */
      0, /* !REPLACEMENT-primitive-one-minus/fR/retR */
      0, /* !REPLACEMENT-primitive-one-plus/fR/retR */
      0, /* !REPLACEMENT-primitive-positivep/fR/retR */
      0, /* !REPLACEMENT-primitive-primordial-divided/fR/retR */
      0, /* !REPLACEMENT-primitive-primordial-divided-unsafe/fR/retR */
      0, /* !REPLACEMENT-primitive-primordial-minus/fR/retR */
      0, /* !REPLACEMENT-primitive-primordial-plus/fR/retR */
      0, /* !REPLACEMENT-primitive-primordial-times/fR/retR */
      0, /* !REPLACEMENT-primitive-quotient/fR/retR */
      0, /* !REPLACEMENT-primitive-quotient-unsafe/fR/retR */
      0, /* !REPLACEMENT-primitive-remainder/fR/retR */
      0, /* !REPLACEMENT-primitive-remainder-unsafe/fR/retR */
      0, /* !REPLACEMENT-primitive-set-carb-special/fR/retR */
      0, /* !REPLACEMENT-primitive-set-cdrb-special/fR/retR */
      0, /* !REPLACEMENT-primitive-two-divided/fR/retR */
      0, /* !REPLACEMENT-primitive-two-quotient/fR/retR */
      0, /* !REPLACEMENT-primitive-two-remainder/fR/retR */
      0, /* !REPLACEMENT-primitive-two-times/fR/retR */
      0, /* !REPLACEMENT-primitive-zerop/fR/retR */
      0, /* !REPLACEMENT-procedure-prolog/retR */
      0, /* !REPLACEMENT-push-global/nR/fR/retR */
      0, /* !REPLACEMENT-return/retR */
      0, /* !REPLACEMENT-tail-call/n0/retR */
      0, /* !REPLACEMENT-tail-call/n1/retR */
      0, /* !REPLACEMENT-tail-call/n2/retR */
      0, /* !REPLACEMENT-tail-call/n3/retR */
      0, /* !REPLACEMENT-tail-call/n4/retR */
      0, /* !REPLACEMENT-tail-call/n5/retR */
      0, /* !REPLACEMENT-tail-call/n6/retR */
      0, /* !REPLACEMENT-tail-call/n7/retR */
      0, /* !REPLACEMENT-tail-call/n8/retR */
      0, /* !REPLACEMENT-tail-call/n9/retR */
      0, /* !REPLACEMENT-tail-call/n10/retR */
      0, /* !REPLACEMENT-tail-call/nR/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n0/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n1/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n2/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n3/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n4/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n5/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n6/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n7/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n8/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n9/retR */
      0, /* !REPLACEMENT-tail-call-compiled/n10/retR */
      0, /* !REPLACEMENT-tail-call-compiled/nR/retR */
      0 /* !REPLACEMENT-unreachable/retR */
    };
#endif // #ifdef JITTER_HAVE_PATCH_IN

// FIXME: I may want to conditionalize this.
const bool
jitterlispvm_specialized_instruction_relocatables [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      true, // !INVALID
      true, // !BEGINBASICBLOCK
      true, // !EXITVM
      true, // !DATALOCATIONS
      true, // !NOP
      true, // !UNREACHABLE0
      true, // !UNREACHABLE1
      true, // !PRETENDTOJUMPANYWHERE
      true, // at-depth-to-register/n1/%rR
      true, // at-depth-to-register/n2/%rR
      true, // at-depth-to-register/n3/%rR
      true, // at-depth-to-register/n4/%rR
      true, // at-depth-to-register/n5/%rR
      true, // at-depth-to-register/n6/%rR
      true, // at-depth-to-register/n7/%rR
      true, // at-depth-to-register/n8/%rR
      true, // at-depth-to-register/n9/%rR
      true, // at-depth-to-register/n10/%rR
      true, // at-depth-to-register/nR/%rR
      true, // branch/fR
      true, // branch-if-false/fR
      true, // branch-if-non-positive/fR/fR
      true, // branch-if-not-less/fR/fR
      true, // branch-if-not-null/fR
      true, // branch-if-null/fR
      true, // branch-if-positive/fR/fR
      true, // branch-if-register-non-positive/%rR/fR/fR
      true, // branch-if-register-non-zero/%rR/fR/fR
      true, // branch-if-register-not-null/%rR/fR
      true, // branch-if-register-null/%rR/fR
      true, // branch-if-register-positive/%rR/fR/fR
      true, // branch-if-true/fR
      true, // call/n0/retR
      true, // call/n1/retR
      true, // call/n2/retR
      true, // call/n3/retR
      true, // call/n4/retR
      true, // call/n5/retR
      true, // call/n6/retR
      true, // call/n7/retR
      true, // call/n8/retR
      true, // call/n9/retR
      true, // call/n10/retR
      true, // call/nR/retR
      true, // call-compiled/n0/retR
      true, // call-compiled/n1/retR
      true, // call-compiled/n2/retR
      true, // call-compiled/n3/retR
      true, // call-compiled/n4/retR
      true, // call-compiled/n5/retR
      true, // call-compiled/n6/retR
      true, // call-compiled/n7/retR
      true, // call-compiled/n8/retR
      true, // call-compiled/n9/retR
      true, // call-compiled/n10/retR
      true, // call-compiled/nR/retR
      true, // call-from-c/retR
      true, // canonicalize-boolean
      true, // cdr-register/%rR/%rR/fR
      true, // check-closure/fR
      true, // check-global-defined/nR/fR
      true, // check-in-arity/n0/fR
      true, // check-in-arity/n1/fR
      true, // check-in-arity/n2/fR
      true, // check-in-arity/n3/fR
      true, // check-in-arity/n4/fR
      true, // check-in-arity/n5/fR
      true, // check-in-arity/n6/fR
      true, // check-in-arity/n7/fR
      true, // check-in-arity/n8/fR
      true, // check-in-arity/n9/fR
      true, // check-in-arity/n10/fR
      true, // check-in-arity/nR/fR
      true, // check-in-arity--alt/n0/fR
      true, // check-in-arity--alt/n1/fR
      true, // check-in-arity--alt/n2/fR
      true, // check-in-arity--alt/n3/fR
      true, // check-in-arity--alt/n4/fR
      true, // check-in-arity--alt/n5/fR
      true, // check-in-arity--alt/n6/fR
      true, // check-in-arity--alt/n7/fR
      true, // check-in-arity--alt/n8/fR
      true, // check-in-arity--alt/n9/fR
      true, // check-in-arity--alt/n10/fR
      true, // check-in-arity--alt/nR/fR
      true, // copy-from-literal/nR
      true, // copy-from-register/%rR
      true, // copy-to-register/%rR
      true, // drop
      true, // drop-nip
      true, // dup
      true, // exitvm
      false, // fail/retR
      true, // gc-if-needed/fR
      true, // heap-allocate/n4
      true, // heap-allocate/n8
      true, // heap-allocate/n12
      true, // heap-allocate/n16
      true, // heap-allocate/n24
      true, // heap-allocate/n32
      true, // heap-allocate/n36
      true, // heap-allocate/n48
      true, // heap-allocate/n52
      true, // heap-allocate/n64
      true, // heap-allocate/nR
      true, // literal-to-register/nR/%rR
      true, // nip
      true, // nip-drop
      true, // nip-five
      true, // nip-five-drop
      true, // nip-four
      true, // nip-four-drop
      true, // nip-push-literal/nR
      true, // nip-push-register/%rR
      true, // nip-six
      true, // nip-six-drop
      true, // nip-three
      true, // nip-three-drop
      true, // nip-two
      true, // nip-two-drop
      true, // nop
      true, // one-minus-register/%rR/%rR/fR
      true, // one-plus-register/%rR/%rR/fR
      true, // pop-to-global/nR/fR
      true, // pop-to-global-defined/nR/fR
      true, // pop-to-register/%rR
      true, // primitive/nR/n0/fR
      true, // primitive/nR/n1/fR
      true, // primitive/nR/n2/fR
      true, // primitive/nR/n3/fR
      true, // primitive/nR/n4/fR
      true, // primitive/nR/nR/fR
      true, // primitive-boolean-canonicalize
      true, // primitive-box
      true, // primitive-box-get/fR
      true, // primitive-box-setb-special/fR
      true, // primitive-car/fR
      true, // primitive-cdr/fR
      true, // primitive-characterp
      true, // primitive-cons-special
      true, // primitive-consp
      true, // primitive-eqp
      true, // primitive-fixnum-eqp/fR
      true, // primitive-fixnum-not-eqp/fR
      true, // primitive-fixnump
      true, // primitive-greaterp/fR
      true, // primitive-lessp/fR
      true, // primitive-negate/fR
      true, // primitive-negativep/fR
      true, // primitive-non-consp
      true, // primitive-non-negativep/fR
      true, // primitive-non-nullp
      true, // primitive-non-positivep/fR
      true, // primitive-non-symbolp
      true, // primitive-non-zerop/fR
      true, // primitive-not
      true, // primitive-not-eqp
      true, // primitive-not-greaterp/fR
      true, // primitive-not-lessp/fR
      true, // primitive-nothingp
      true, // primitive-nullp
      true, // primitive-one-minus/fR
      true, // primitive-one-plus/fR
      true, // primitive-positivep/fR
      true, // primitive-primordial-divided/fR
      true, // primitive-primordial-divided-unsafe/fR
      true, // primitive-primordial-minus/fR
      true, // primitive-primordial-plus/fR
      true, // primitive-primordial-times/fR
      true, // primitive-quotient/fR
      true, // primitive-quotient-unsafe/fR
      true, // primitive-remainder/fR
      true, // primitive-remainder-unsafe/fR
      true, // primitive-set-carb-special/fR
      true, // primitive-set-cdrb-special/fR
      true, // primitive-symbolp
      true, // primitive-two-divided/fR
      true, // primitive-two-quotient/fR
      true, // primitive-two-remainder/fR
      true, // primitive-two-times/fR
      true, // primitive-uniquep
      true, // primitive-zerop/fR
      true, // procedure-prolog
      true, // push-false
      true, // push-global/nR/fR
      true, // push-literal/nR
      true, // push-nil
      true, // push-nothing
      true, // push-one
      true, // push-register/%rR
      true, // push-unspecified
      true, // push-zero
      true, // register-to-register/%rR/%rR
      true, // restore-register/%rR
      true, // return
      true, // save-register/%rR
      true, // tail-call/n0/retR
      true, // tail-call/n1/retR
      true, // tail-call/n2/retR
      true, // tail-call/n3/retR
      true, // tail-call/n4/retR
      true, // tail-call/n5/retR
      true, // tail-call/n6/retR
      true, // tail-call/n7/retR
      true, // tail-call/n8/retR
      true, // tail-call/n9/retR
      true, // tail-call/n10/retR
      true, // tail-call/nR/retR
      true, // tail-call-compiled/n0/retR
      true, // tail-call-compiled/n1/retR
      true, // tail-call-compiled/n2/retR
      true, // tail-call-compiled/n3/retR
      true, // tail-call-compiled/n4/retR
      true, // tail-call-compiled/n5/retR
      true, // tail-call-compiled/n6/retR
      true, // tail-call-compiled/n7/retR
      true, // tail-call-compiled/n8/retR
      true, // tail-call-compiled/n9/retR
      true, // tail-call-compiled/n10/retR
      true, // tail-call-compiled/nR/retR
      true, // unreachable
      false, // !REPLACEMENT-branch/fR/retR
      false, // !REPLACEMENT-branch-if-false/fR/retR
      false, // !REPLACEMENT-branch-if-non-positive/fR/fR/retR
      false, // !REPLACEMENT-branch-if-not-less/fR/fR/retR
      false, // !REPLACEMENT-branch-if-not-null/fR/retR
      false, // !REPLACEMENT-branch-if-null/fR/retR
      false, // !REPLACEMENT-branch-if-positive/fR/fR/retR
      false, // !REPLACEMENT-branch-if-register-non-positive/%rR/fR/fR/retR
      false, // !REPLACEMENT-branch-if-register-non-zero/%rR/fR/fR/retR
      false, // !REPLACEMENT-branch-if-register-not-null/%rR/fR/retR
      false, // !REPLACEMENT-branch-if-register-null/%rR/fR/retR
      false, // !REPLACEMENT-branch-if-register-positive/%rR/fR/fR/retR
      false, // !REPLACEMENT-branch-if-true/fR/retR
      false, // !REPLACEMENT-call/n0/retR
      false, // !REPLACEMENT-call/n1/retR
      false, // !REPLACEMENT-call/n2/retR
      false, // !REPLACEMENT-call/n3/retR
      false, // !REPLACEMENT-call/n4/retR
      false, // !REPLACEMENT-call/n5/retR
      false, // !REPLACEMENT-call/n6/retR
      false, // !REPLACEMENT-call/n7/retR
      false, // !REPLACEMENT-call/n8/retR
      false, // !REPLACEMENT-call/n9/retR
      false, // !REPLACEMENT-call/n10/retR
      false, // !REPLACEMENT-call/nR/retR
      false, // !REPLACEMENT-call-compiled/n0/retR
      false, // !REPLACEMENT-call-compiled/n1/retR
      false, // !REPLACEMENT-call-compiled/n2/retR
      false, // !REPLACEMENT-call-compiled/n3/retR
      false, // !REPLACEMENT-call-compiled/n4/retR
      false, // !REPLACEMENT-call-compiled/n5/retR
      false, // !REPLACEMENT-call-compiled/n6/retR
      false, // !REPLACEMENT-call-compiled/n7/retR
      false, // !REPLACEMENT-call-compiled/n8/retR
      false, // !REPLACEMENT-call-compiled/n9/retR
      false, // !REPLACEMENT-call-compiled/n10/retR
      false, // !REPLACEMENT-call-compiled/nR/retR
      false, // !REPLACEMENT-call-from-c/retR
      false, // !REPLACEMENT-cdr-register/%rR/%rR/fR/retR
      false, // !REPLACEMENT-check-closure/fR/retR
      false, // !REPLACEMENT-check-global-defined/nR/fR/retR
      false, // !REPLACEMENT-check-in-arity/n0/fR/retR
      false, // !REPLACEMENT-check-in-arity/n1/fR/retR
      false, // !REPLACEMENT-check-in-arity/n2/fR/retR
      false, // !REPLACEMENT-check-in-arity/n3/fR/retR
      false, // !REPLACEMENT-check-in-arity/n4/fR/retR
      false, // !REPLACEMENT-check-in-arity/n5/fR/retR
      false, // !REPLACEMENT-check-in-arity/n6/fR/retR
      false, // !REPLACEMENT-check-in-arity/n7/fR/retR
      false, // !REPLACEMENT-check-in-arity/n8/fR/retR
      false, // !REPLACEMENT-check-in-arity/n9/fR/retR
      false, // !REPLACEMENT-check-in-arity/n10/fR/retR
      false, // !REPLACEMENT-check-in-arity/nR/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n0/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n1/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n2/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n3/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n4/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n5/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n6/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n7/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n8/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n9/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n10/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/nR/fR/retR
      false, // !REPLACEMENT-exitvm/retR
      false, // !REPLACEMENT-gc-if-needed/fR/retR
      false, // !REPLACEMENT-one-minus-register/%rR/%rR/fR/retR
      false, // !REPLACEMENT-one-plus-register/%rR/%rR/fR/retR
      false, // !REPLACEMENT-pop-to-global/nR/fR/retR
      false, // !REPLACEMENT-pop-to-global-defined/nR/fR/retR
      false, // !REPLACEMENT-primitive/nR/n0/fR/retR
      false, // !REPLACEMENT-primitive/nR/n1/fR/retR
      false, // !REPLACEMENT-primitive/nR/n2/fR/retR
      false, // !REPLACEMENT-primitive/nR/n3/fR/retR
      false, // !REPLACEMENT-primitive/nR/n4/fR/retR
      false, // !REPLACEMENT-primitive/nR/nR/fR/retR
      false, // !REPLACEMENT-primitive-box-get/fR/retR
      false, // !REPLACEMENT-primitive-box-setb-special/fR/retR
      false, // !REPLACEMENT-primitive-car/fR/retR
      false, // !REPLACEMENT-primitive-cdr/fR/retR
      false, // !REPLACEMENT-primitive-fixnum-eqp/fR/retR
      false, // !REPLACEMENT-primitive-fixnum-not-eqp/fR/retR
      false, // !REPLACEMENT-primitive-greaterp/fR/retR
      false, // !REPLACEMENT-primitive-lessp/fR/retR
      false, // !REPLACEMENT-primitive-negate/fR/retR
      false, // !REPLACEMENT-primitive-negativep/fR/retR
      false, // !REPLACEMENT-primitive-non-negativep/fR/retR
      false, // !REPLACEMENT-primitive-non-positivep/fR/retR
      false, // !REPLACEMENT-primitive-non-zerop/fR/retR
      false, // !REPLACEMENT-primitive-not-greaterp/fR/retR
      false, // !REPLACEMENT-primitive-not-lessp/fR/retR
      false, // !REPLACEMENT-primitive-one-minus/fR/retR
      false, // !REPLACEMENT-primitive-one-plus/fR/retR
      false, // !REPLACEMENT-primitive-positivep/fR/retR
      false, // !REPLACEMENT-primitive-primordial-divided/fR/retR
      false, // !REPLACEMENT-primitive-primordial-divided-unsafe/fR/retR
      false, // !REPLACEMENT-primitive-primordial-minus/fR/retR
      false, // !REPLACEMENT-primitive-primordial-plus/fR/retR
      false, // !REPLACEMENT-primitive-primordial-times/fR/retR
      false, // !REPLACEMENT-primitive-quotient/fR/retR
      false, // !REPLACEMENT-primitive-quotient-unsafe/fR/retR
      false, // !REPLACEMENT-primitive-remainder/fR/retR
      false, // !REPLACEMENT-primitive-remainder-unsafe/fR/retR
      false, // !REPLACEMENT-primitive-set-carb-special/fR/retR
      false, // !REPLACEMENT-primitive-set-cdrb-special/fR/retR
      false, // !REPLACEMENT-primitive-two-divided/fR/retR
      false, // !REPLACEMENT-primitive-two-quotient/fR/retR
      false, // !REPLACEMENT-primitive-two-remainder/fR/retR
      false, // !REPLACEMENT-primitive-two-times/fR/retR
      false, // !REPLACEMENT-primitive-zerop/fR/retR
      false, // !REPLACEMENT-procedure-prolog/retR
      false, // !REPLACEMENT-push-global/nR/fR/retR
      false, // !REPLACEMENT-return/retR
      false, // !REPLACEMENT-tail-call/n0/retR
      false, // !REPLACEMENT-tail-call/n1/retR
      false, // !REPLACEMENT-tail-call/n2/retR
      false, // !REPLACEMENT-tail-call/n3/retR
      false, // !REPLACEMENT-tail-call/n4/retR
      false, // !REPLACEMENT-tail-call/n5/retR
      false, // !REPLACEMENT-tail-call/n6/retR
      false, // !REPLACEMENT-tail-call/n7/retR
      false, // !REPLACEMENT-tail-call/n8/retR
      false, // !REPLACEMENT-tail-call/n9/retR
      false, // !REPLACEMENT-tail-call/n10/retR
      false, // !REPLACEMENT-tail-call/nR/retR
      false, // !REPLACEMENT-tail-call-compiled/n0/retR
      false, // !REPLACEMENT-tail-call-compiled/n1/retR
      false, // !REPLACEMENT-tail-call-compiled/n2/retR
      false, // !REPLACEMENT-tail-call-compiled/n3/retR
      false, // !REPLACEMENT-tail-call-compiled/n4/retR
      false, // !REPLACEMENT-tail-call-compiled/n5/retR
      false, // !REPLACEMENT-tail-call-compiled/n6/retR
      false, // !REPLACEMENT-tail-call-compiled/n7/retR
      false, // !REPLACEMENT-tail-call-compiled/n8/retR
      false, // !REPLACEMENT-tail-call-compiled/n9/retR
      false, // !REPLACEMENT-tail-call-compiled/n10/retR
      false, // !REPLACEMENT-tail-call-compiled/nR/retR
      false // !REPLACEMENT-unreachable/retR
    };

// FIXME: this is not currently accessed, and in fact may be useless.
const bool
jitterlispvm_specialized_instruction_callers [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      false, // !INVALID
      false, // !BEGINBASICBLOCK
      false, // !EXITVM
      false, // !DATALOCATIONS
      false, // !NOP
      false, // !UNREACHABLE0
      false, // !UNREACHABLE1
      false, // !PRETENDTOJUMPANYWHERE
      false, // at-depth-to-register/n1/%rR
      false, // at-depth-to-register/n2/%rR
      false, // at-depth-to-register/n3/%rR
      false, // at-depth-to-register/n4/%rR
      false, // at-depth-to-register/n5/%rR
      false, // at-depth-to-register/n6/%rR
      false, // at-depth-to-register/n7/%rR
      false, // at-depth-to-register/n8/%rR
      false, // at-depth-to-register/n9/%rR
      false, // at-depth-to-register/n10/%rR
      false, // at-depth-to-register/nR/%rR
      false, // branch/fR
      false, // branch-if-false/fR
      false, // branch-if-non-positive/fR/fR
      false, // branch-if-not-less/fR/fR
      false, // branch-if-not-null/fR
      false, // branch-if-null/fR
      false, // branch-if-positive/fR/fR
      false, // branch-if-register-non-positive/%rR/fR/fR
      false, // branch-if-register-non-zero/%rR/fR/fR
      false, // branch-if-register-not-null/%rR/fR
      false, // branch-if-register-null/%rR/fR
      false, // branch-if-register-positive/%rR/fR/fR
      false, // branch-if-true/fR
      true, // call/n0/retR
      true, // call/n1/retR
      true, // call/n2/retR
      true, // call/n3/retR
      true, // call/n4/retR
      true, // call/n5/retR
      true, // call/n6/retR
      true, // call/n7/retR
      true, // call/n8/retR
      true, // call/n9/retR
      true, // call/n10/retR
      true, // call/nR/retR
      true, // call-compiled/n0/retR
      true, // call-compiled/n1/retR
      true, // call-compiled/n2/retR
      true, // call-compiled/n3/retR
      true, // call-compiled/n4/retR
      true, // call-compiled/n5/retR
      true, // call-compiled/n6/retR
      true, // call-compiled/n7/retR
      true, // call-compiled/n8/retR
      true, // call-compiled/n9/retR
      true, // call-compiled/n10/retR
      true, // call-compiled/nR/retR
      true, // call-from-c/retR
      false, // canonicalize-boolean
      false, // cdr-register/%rR/%rR/fR
      false, // check-closure/fR
      false, // check-global-defined/nR/fR
      false, // check-in-arity/n0/fR
      false, // check-in-arity/n1/fR
      false, // check-in-arity/n2/fR
      false, // check-in-arity/n3/fR
      false, // check-in-arity/n4/fR
      false, // check-in-arity/n5/fR
      false, // check-in-arity/n6/fR
      false, // check-in-arity/n7/fR
      false, // check-in-arity/n8/fR
      false, // check-in-arity/n9/fR
      false, // check-in-arity/n10/fR
      false, // check-in-arity/nR/fR
      false, // check-in-arity--alt/n0/fR
      false, // check-in-arity--alt/n1/fR
      false, // check-in-arity--alt/n2/fR
      false, // check-in-arity--alt/n3/fR
      false, // check-in-arity--alt/n4/fR
      false, // check-in-arity--alt/n5/fR
      false, // check-in-arity--alt/n6/fR
      false, // check-in-arity--alt/n7/fR
      false, // check-in-arity--alt/n8/fR
      false, // check-in-arity--alt/n9/fR
      false, // check-in-arity--alt/n10/fR
      false, // check-in-arity--alt/nR/fR
      false, // copy-from-literal/nR
      false, // copy-from-register/%rR
      false, // copy-to-register/%rR
      false, // drop
      false, // drop-nip
      false, // dup
      false, // exitvm
      false, // fail/retR
      false, // gc-if-needed/fR
      false, // heap-allocate/n4
      false, // heap-allocate/n8
      false, // heap-allocate/n12
      false, // heap-allocate/n16
      false, // heap-allocate/n24
      false, // heap-allocate/n32
      false, // heap-allocate/n36
      false, // heap-allocate/n48
      false, // heap-allocate/n52
      false, // heap-allocate/n64
      false, // heap-allocate/nR
      false, // literal-to-register/nR/%rR
      false, // nip
      false, // nip-drop
      false, // nip-five
      false, // nip-five-drop
      false, // nip-four
      false, // nip-four-drop
      false, // nip-push-literal/nR
      false, // nip-push-register/%rR
      false, // nip-six
      false, // nip-six-drop
      false, // nip-three
      false, // nip-three-drop
      false, // nip-two
      false, // nip-two-drop
      false, // nop
      false, // one-minus-register/%rR/%rR/fR
      false, // one-plus-register/%rR/%rR/fR
      false, // pop-to-global/nR/fR
      false, // pop-to-global-defined/nR/fR
      false, // pop-to-register/%rR
      false, // primitive/nR/n0/fR
      false, // primitive/nR/n1/fR
      false, // primitive/nR/n2/fR
      false, // primitive/nR/n3/fR
      false, // primitive/nR/n4/fR
      false, // primitive/nR/nR/fR
      false, // primitive-boolean-canonicalize
      false, // primitive-box
      false, // primitive-box-get/fR
      false, // primitive-box-setb-special/fR
      false, // primitive-car/fR
      false, // primitive-cdr/fR
      false, // primitive-characterp
      false, // primitive-cons-special
      false, // primitive-consp
      false, // primitive-eqp
      false, // primitive-fixnum-eqp/fR
      false, // primitive-fixnum-not-eqp/fR
      false, // primitive-fixnump
      false, // primitive-greaterp/fR
      false, // primitive-lessp/fR
      false, // primitive-negate/fR
      false, // primitive-negativep/fR
      false, // primitive-non-consp
      false, // primitive-non-negativep/fR
      false, // primitive-non-nullp
      false, // primitive-non-positivep/fR
      false, // primitive-non-symbolp
      false, // primitive-non-zerop/fR
      false, // primitive-not
      false, // primitive-not-eqp
      false, // primitive-not-greaterp/fR
      false, // primitive-not-lessp/fR
      false, // primitive-nothingp
      false, // primitive-nullp
      false, // primitive-one-minus/fR
      false, // primitive-one-plus/fR
      false, // primitive-positivep/fR
      false, // primitive-primordial-divided/fR
      false, // primitive-primordial-divided-unsafe/fR
      false, // primitive-primordial-minus/fR
      false, // primitive-primordial-plus/fR
      false, // primitive-primordial-times/fR
      false, // primitive-quotient/fR
      false, // primitive-quotient-unsafe/fR
      false, // primitive-remainder/fR
      false, // primitive-remainder-unsafe/fR
      false, // primitive-set-carb-special/fR
      false, // primitive-set-cdrb-special/fR
      false, // primitive-symbolp
      false, // primitive-two-divided/fR
      false, // primitive-two-quotient/fR
      false, // primitive-two-remainder/fR
      false, // primitive-two-times/fR
      false, // primitive-uniquep
      false, // primitive-zerop/fR
      false, // procedure-prolog
      false, // push-false
      false, // push-global/nR/fR
      false, // push-literal/nR
      false, // push-nil
      false, // push-nothing
      false, // push-one
      false, // push-register/%rR
      false, // push-unspecified
      false, // push-zero
      false, // register-to-register/%rR/%rR
      false, // restore-register/%rR
      false, // return
      false, // save-register/%rR
      true, // tail-call/n0/retR
      true, // tail-call/n1/retR
      true, // tail-call/n2/retR
      true, // tail-call/n3/retR
      true, // tail-call/n4/retR
      true, // tail-call/n5/retR
      true, // tail-call/n6/retR
      true, // tail-call/n7/retR
      true, // tail-call/n8/retR
      true, // tail-call/n9/retR
      true, // tail-call/n10/retR
      true, // tail-call/nR/retR
      true, // tail-call-compiled/n0/retR
      true, // tail-call-compiled/n1/retR
      true, // tail-call-compiled/n2/retR
      true, // tail-call-compiled/n3/retR
      true, // tail-call-compiled/n4/retR
      true, // tail-call-compiled/n5/retR
      true, // tail-call-compiled/n6/retR
      true, // tail-call-compiled/n7/retR
      true, // tail-call-compiled/n8/retR
      true, // tail-call-compiled/n9/retR
      true, // tail-call-compiled/n10/retR
      true, // tail-call-compiled/nR/retR
      false, // unreachable
      false, // !REPLACEMENT-branch/fR/retR
      false, // !REPLACEMENT-branch-if-false/fR/retR
      false, // !REPLACEMENT-branch-if-non-positive/fR/fR/retR
      false, // !REPLACEMENT-branch-if-not-less/fR/fR/retR
      false, // !REPLACEMENT-branch-if-not-null/fR/retR
      false, // !REPLACEMENT-branch-if-null/fR/retR
      false, // !REPLACEMENT-branch-if-positive/fR/fR/retR
      false, // !REPLACEMENT-branch-if-register-non-positive/%rR/fR/fR/retR
      false, // !REPLACEMENT-branch-if-register-non-zero/%rR/fR/fR/retR
      false, // !REPLACEMENT-branch-if-register-not-null/%rR/fR/retR
      false, // !REPLACEMENT-branch-if-register-null/%rR/fR/retR
      false, // !REPLACEMENT-branch-if-register-positive/%rR/fR/fR/retR
      false, // !REPLACEMENT-branch-if-true/fR/retR
      true, // !REPLACEMENT-call/n0/retR
      true, // !REPLACEMENT-call/n1/retR
      true, // !REPLACEMENT-call/n2/retR
      true, // !REPLACEMENT-call/n3/retR
      true, // !REPLACEMENT-call/n4/retR
      true, // !REPLACEMENT-call/n5/retR
      true, // !REPLACEMENT-call/n6/retR
      true, // !REPLACEMENT-call/n7/retR
      true, // !REPLACEMENT-call/n8/retR
      true, // !REPLACEMENT-call/n9/retR
      true, // !REPLACEMENT-call/n10/retR
      true, // !REPLACEMENT-call/nR/retR
      true, // !REPLACEMENT-call-compiled/n0/retR
      true, // !REPLACEMENT-call-compiled/n1/retR
      true, // !REPLACEMENT-call-compiled/n2/retR
      true, // !REPLACEMENT-call-compiled/n3/retR
      true, // !REPLACEMENT-call-compiled/n4/retR
      true, // !REPLACEMENT-call-compiled/n5/retR
      true, // !REPLACEMENT-call-compiled/n6/retR
      true, // !REPLACEMENT-call-compiled/n7/retR
      true, // !REPLACEMENT-call-compiled/n8/retR
      true, // !REPLACEMENT-call-compiled/n9/retR
      true, // !REPLACEMENT-call-compiled/n10/retR
      true, // !REPLACEMENT-call-compiled/nR/retR
      true, // !REPLACEMENT-call-from-c/retR
      false, // !REPLACEMENT-cdr-register/%rR/%rR/fR/retR
      false, // !REPLACEMENT-check-closure/fR/retR
      false, // !REPLACEMENT-check-global-defined/nR/fR/retR
      false, // !REPLACEMENT-check-in-arity/n0/fR/retR
      false, // !REPLACEMENT-check-in-arity/n1/fR/retR
      false, // !REPLACEMENT-check-in-arity/n2/fR/retR
      false, // !REPLACEMENT-check-in-arity/n3/fR/retR
      false, // !REPLACEMENT-check-in-arity/n4/fR/retR
      false, // !REPLACEMENT-check-in-arity/n5/fR/retR
      false, // !REPLACEMENT-check-in-arity/n6/fR/retR
      false, // !REPLACEMENT-check-in-arity/n7/fR/retR
      false, // !REPLACEMENT-check-in-arity/n8/fR/retR
      false, // !REPLACEMENT-check-in-arity/n9/fR/retR
      false, // !REPLACEMENT-check-in-arity/n10/fR/retR
      false, // !REPLACEMENT-check-in-arity/nR/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n0/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n1/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n2/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n3/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n4/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n5/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n6/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n7/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n8/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n9/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n10/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/nR/fR/retR
      false, // !REPLACEMENT-exitvm/retR
      false, // !REPLACEMENT-gc-if-needed/fR/retR
      false, // !REPLACEMENT-one-minus-register/%rR/%rR/fR/retR
      false, // !REPLACEMENT-one-plus-register/%rR/%rR/fR/retR
      false, // !REPLACEMENT-pop-to-global/nR/fR/retR
      false, // !REPLACEMENT-pop-to-global-defined/nR/fR/retR
      false, // !REPLACEMENT-primitive/nR/n0/fR/retR
      false, // !REPLACEMENT-primitive/nR/n1/fR/retR
      false, // !REPLACEMENT-primitive/nR/n2/fR/retR
      false, // !REPLACEMENT-primitive/nR/n3/fR/retR
      false, // !REPLACEMENT-primitive/nR/n4/fR/retR
      false, // !REPLACEMENT-primitive/nR/nR/fR/retR
      false, // !REPLACEMENT-primitive-box-get/fR/retR
      false, // !REPLACEMENT-primitive-box-setb-special/fR/retR
      false, // !REPLACEMENT-primitive-car/fR/retR
      false, // !REPLACEMENT-primitive-cdr/fR/retR
      false, // !REPLACEMENT-primitive-fixnum-eqp/fR/retR
      false, // !REPLACEMENT-primitive-fixnum-not-eqp/fR/retR
      false, // !REPLACEMENT-primitive-greaterp/fR/retR
      false, // !REPLACEMENT-primitive-lessp/fR/retR
      false, // !REPLACEMENT-primitive-negate/fR/retR
      false, // !REPLACEMENT-primitive-negativep/fR/retR
      false, // !REPLACEMENT-primitive-non-negativep/fR/retR
      false, // !REPLACEMENT-primitive-non-positivep/fR/retR
      false, // !REPLACEMENT-primitive-non-zerop/fR/retR
      false, // !REPLACEMENT-primitive-not-greaterp/fR/retR
      false, // !REPLACEMENT-primitive-not-lessp/fR/retR
      false, // !REPLACEMENT-primitive-one-minus/fR/retR
      false, // !REPLACEMENT-primitive-one-plus/fR/retR
      false, // !REPLACEMENT-primitive-positivep/fR/retR
      false, // !REPLACEMENT-primitive-primordial-divided/fR/retR
      false, // !REPLACEMENT-primitive-primordial-divided-unsafe/fR/retR
      false, // !REPLACEMENT-primitive-primordial-minus/fR/retR
      false, // !REPLACEMENT-primitive-primordial-plus/fR/retR
      false, // !REPLACEMENT-primitive-primordial-times/fR/retR
      false, // !REPLACEMENT-primitive-quotient/fR/retR
      false, // !REPLACEMENT-primitive-quotient-unsafe/fR/retR
      false, // !REPLACEMENT-primitive-remainder/fR/retR
      false, // !REPLACEMENT-primitive-remainder-unsafe/fR/retR
      false, // !REPLACEMENT-primitive-set-carb-special/fR/retR
      false, // !REPLACEMENT-primitive-set-cdrb-special/fR/retR
      false, // !REPLACEMENT-primitive-two-divided/fR/retR
      false, // !REPLACEMENT-primitive-two-quotient/fR/retR
      false, // !REPLACEMENT-primitive-two-remainder/fR/retR
      false, // !REPLACEMENT-primitive-two-times/fR/retR
      false, // !REPLACEMENT-primitive-zerop/fR/retR
      false, // !REPLACEMENT-procedure-prolog/retR
      false, // !REPLACEMENT-push-global/nR/fR/retR
      false, // !REPLACEMENT-return/retR
      true, // !REPLACEMENT-tail-call/n0/retR
      true, // !REPLACEMENT-tail-call/n1/retR
      true, // !REPLACEMENT-tail-call/n2/retR
      true, // !REPLACEMENT-tail-call/n3/retR
      true, // !REPLACEMENT-tail-call/n4/retR
      true, // !REPLACEMENT-tail-call/n5/retR
      true, // !REPLACEMENT-tail-call/n6/retR
      true, // !REPLACEMENT-tail-call/n7/retR
      true, // !REPLACEMENT-tail-call/n8/retR
      true, // !REPLACEMENT-tail-call/n9/retR
      true, // !REPLACEMENT-tail-call/n10/retR
      true, // !REPLACEMENT-tail-call/nR/retR
      true, // !REPLACEMENT-tail-call-compiled/n0/retR
      true, // !REPLACEMENT-tail-call-compiled/n1/retR
      true, // !REPLACEMENT-tail-call-compiled/n2/retR
      true, // !REPLACEMENT-tail-call-compiled/n3/retR
      true, // !REPLACEMENT-tail-call-compiled/n4/retR
      true, // !REPLACEMENT-tail-call-compiled/n5/retR
      true, // !REPLACEMENT-tail-call-compiled/n6/retR
      true, // !REPLACEMENT-tail-call-compiled/n7/retR
      true, // !REPLACEMENT-tail-call-compiled/n8/retR
      true, // !REPLACEMENT-tail-call-compiled/n9/retR
      true, // !REPLACEMENT-tail-call-compiled/n10/retR
      true, // !REPLACEMENT-tail-call-compiled/nR/retR
      false // !REPLACEMENT-unreachable/retR
    };

// FIXME: this is not currently accessed, and in fact may be useless.
const bool
jitterlispvm_specialized_instruction_callees [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      false, // !INVALID
      false, // !BEGINBASICBLOCK
      false, // !EXITVM
      false, // !DATALOCATIONS
      false, // !NOP
      false, // !UNREACHABLE0
      false, // !UNREACHABLE1
      false, // !PRETENDTOJUMPANYWHERE
      false, // at-depth-to-register/n1/%rR
      false, // at-depth-to-register/n2/%rR
      false, // at-depth-to-register/n3/%rR
      false, // at-depth-to-register/n4/%rR
      false, // at-depth-to-register/n5/%rR
      false, // at-depth-to-register/n6/%rR
      false, // at-depth-to-register/n7/%rR
      false, // at-depth-to-register/n8/%rR
      false, // at-depth-to-register/n9/%rR
      false, // at-depth-to-register/n10/%rR
      false, // at-depth-to-register/nR/%rR
      false, // branch/fR
      false, // branch-if-false/fR
      false, // branch-if-non-positive/fR/fR
      false, // branch-if-not-less/fR/fR
      false, // branch-if-not-null/fR
      false, // branch-if-null/fR
      false, // branch-if-positive/fR/fR
      false, // branch-if-register-non-positive/%rR/fR/fR
      false, // branch-if-register-non-zero/%rR/fR/fR
      false, // branch-if-register-not-null/%rR/fR
      false, // branch-if-register-null/%rR/fR
      false, // branch-if-register-positive/%rR/fR/fR
      false, // branch-if-true/fR
      false, // call/n0/retR
      false, // call/n1/retR
      false, // call/n2/retR
      false, // call/n3/retR
      false, // call/n4/retR
      false, // call/n5/retR
      false, // call/n6/retR
      false, // call/n7/retR
      false, // call/n8/retR
      false, // call/n9/retR
      false, // call/n10/retR
      false, // call/nR/retR
      false, // call-compiled/n0/retR
      false, // call-compiled/n1/retR
      false, // call-compiled/n2/retR
      false, // call-compiled/n3/retR
      false, // call-compiled/n4/retR
      false, // call-compiled/n5/retR
      false, // call-compiled/n6/retR
      false, // call-compiled/n7/retR
      false, // call-compiled/n8/retR
      false, // call-compiled/n9/retR
      false, // call-compiled/n10/retR
      false, // call-compiled/nR/retR
      false, // call-from-c/retR
      false, // canonicalize-boolean
      false, // cdr-register/%rR/%rR/fR
      false, // check-closure/fR
      false, // check-global-defined/nR/fR
      false, // check-in-arity/n0/fR
      false, // check-in-arity/n1/fR
      false, // check-in-arity/n2/fR
      false, // check-in-arity/n3/fR
      false, // check-in-arity/n4/fR
      false, // check-in-arity/n5/fR
      false, // check-in-arity/n6/fR
      false, // check-in-arity/n7/fR
      false, // check-in-arity/n8/fR
      false, // check-in-arity/n9/fR
      false, // check-in-arity/n10/fR
      false, // check-in-arity/nR/fR
      false, // check-in-arity--alt/n0/fR
      false, // check-in-arity--alt/n1/fR
      false, // check-in-arity--alt/n2/fR
      false, // check-in-arity--alt/n3/fR
      false, // check-in-arity--alt/n4/fR
      false, // check-in-arity--alt/n5/fR
      false, // check-in-arity--alt/n6/fR
      false, // check-in-arity--alt/n7/fR
      false, // check-in-arity--alt/n8/fR
      false, // check-in-arity--alt/n9/fR
      false, // check-in-arity--alt/n10/fR
      false, // check-in-arity--alt/nR/fR
      false, // copy-from-literal/nR
      false, // copy-from-register/%rR
      false, // copy-to-register/%rR
      false, // drop
      false, // drop-nip
      false, // dup
      false, // exitvm
      false, // fail/retR
      false, // gc-if-needed/fR
      false, // heap-allocate/n4
      false, // heap-allocate/n8
      false, // heap-allocate/n12
      false, // heap-allocate/n16
      false, // heap-allocate/n24
      false, // heap-allocate/n32
      false, // heap-allocate/n36
      false, // heap-allocate/n48
      false, // heap-allocate/n52
      false, // heap-allocate/n64
      false, // heap-allocate/nR
      false, // literal-to-register/nR/%rR
      false, // nip
      false, // nip-drop
      false, // nip-five
      false, // nip-five-drop
      false, // nip-four
      false, // nip-four-drop
      false, // nip-push-literal/nR
      false, // nip-push-register/%rR
      false, // nip-six
      false, // nip-six-drop
      false, // nip-three
      false, // nip-three-drop
      false, // nip-two
      false, // nip-two-drop
      false, // nop
      false, // one-minus-register/%rR/%rR/fR
      false, // one-plus-register/%rR/%rR/fR
      false, // pop-to-global/nR/fR
      false, // pop-to-global-defined/nR/fR
      false, // pop-to-register/%rR
      false, // primitive/nR/n0/fR
      false, // primitive/nR/n1/fR
      false, // primitive/nR/n2/fR
      false, // primitive/nR/n3/fR
      false, // primitive/nR/n4/fR
      false, // primitive/nR/nR/fR
      false, // primitive-boolean-canonicalize
      false, // primitive-box
      false, // primitive-box-get/fR
      false, // primitive-box-setb-special/fR
      false, // primitive-car/fR
      false, // primitive-cdr/fR
      false, // primitive-characterp
      false, // primitive-cons-special
      false, // primitive-consp
      false, // primitive-eqp
      false, // primitive-fixnum-eqp/fR
      false, // primitive-fixnum-not-eqp/fR
      false, // primitive-fixnump
      false, // primitive-greaterp/fR
      false, // primitive-lessp/fR
      false, // primitive-negate/fR
      false, // primitive-negativep/fR
      false, // primitive-non-consp
      false, // primitive-non-negativep/fR
      false, // primitive-non-nullp
      false, // primitive-non-positivep/fR
      false, // primitive-non-symbolp
      false, // primitive-non-zerop/fR
      false, // primitive-not
      false, // primitive-not-eqp
      false, // primitive-not-greaterp/fR
      false, // primitive-not-lessp/fR
      false, // primitive-nothingp
      false, // primitive-nullp
      false, // primitive-one-minus/fR
      false, // primitive-one-plus/fR
      false, // primitive-positivep/fR
      false, // primitive-primordial-divided/fR
      false, // primitive-primordial-divided-unsafe/fR
      false, // primitive-primordial-minus/fR
      false, // primitive-primordial-plus/fR
      false, // primitive-primordial-times/fR
      false, // primitive-quotient/fR
      false, // primitive-quotient-unsafe/fR
      false, // primitive-remainder/fR
      false, // primitive-remainder-unsafe/fR
      false, // primitive-set-carb-special/fR
      false, // primitive-set-cdrb-special/fR
      false, // primitive-symbolp
      false, // primitive-two-divided/fR
      false, // primitive-two-quotient/fR
      false, // primitive-two-remainder/fR
      false, // primitive-two-times/fR
      false, // primitive-uniquep
      false, // primitive-zerop/fR
      true, // procedure-prolog
      false, // push-false
      false, // push-global/nR/fR
      false, // push-literal/nR
      false, // push-nil
      false, // push-nothing
      false, // push-one
      false, // push-register/%rR
      false, // push-unspecified
      false, // push-zero
      false, // register-to-register/%rR/%rR
      false, // restore-register/%rR
      false, // return
      false, // save-register/%rR
      false, // tail-call/n0/retR
      false, // tail-call/n1/retR
      false, // tail-call/n2/retR
      false, // tail-call/n3/retR
      false, // tail-call/n4/retR
      false, // tail-call/n5/retR
      false, // tail-call/n6/retR
      false, // tail-call/n7/retR
      false, // tail-call/n8/retR
      false, // tail-call/n9/retR
      false, // tail-call/n10/retR
      false, // tail-call/nR/retR
      false, // tail-call-compiled/n0/retR
      false, // tail-call-compiled/n1/retR
      false, // tail-call-compiled/n2/retR
      false, // tail-call-compiled/n3/retR
      false, // tail-call-compiled/n4/retR
      false, // tail-call-compiled/n5/retR
      false, // tail-call-compiled/n6/retR
      false, // tail-call-compiled/n7/retR
      false, // tail-call-compiled/n8/retR
      false, // tail-call-compiled/n9/retR
      false, // tail-call-compiled/n10/retR
      false, // tail-call-compiled/nR/retR
      false, // unreachable
      false, // !REPLACEMENT-branch/fR/retR
      false, // !REPLACEMENT-branch-if-false/fR/retR
      false, // !REPLACEMENT-branch-if-non-positive/fR/fR/retR
      false, // !REPLACEMENT-branch-if-not-less/fR/fR/retR
      false, // !REPLACEMENT-branch-if-not-null/fR/retR
      false, // !REPLACEMENT-branch-if-null/fR/retR
      false, // !REPLACEMENT-branch-if-positive/fR/fR/retR
      false, // !REPLACEMENT-branch-if-register-non-positive/%rR/fR/fR/retR
      false, // !REPLACEMENT-branch-if-register-non-zero/%rR/fR/fR/retR
      false, // !REPLACEMENT-branch-if-register-not-null/%rR/fR/retR
      false, // !REPLACEMENT-branch-if-register-null/%rR/fR/retR
      false, // !REPLACEMENT-branch-if-register-positive/%rR/fR/fR/retR
      false, // !REPLACEMENT-branch-if-true/fR/retR
      false, // !REPLACEMENT-call/n0/retR
      false, // !REPLACEMENT-call/n1/retR
      false, // !REPLACEMENT-call/n2/retR
      false, // !REPLACEMENT-call/n3/retR
      false, // !REPLACEMENT-call/n4/retR
      false, // !REPLACEMENT-call/n5/retR
      false, // !REPLACEMENT-call/n6/retR
      false, // !REPLACEMENT-call/n7/retR
      false, // !REPLACEMENT-call/n8/retR
      false, // !REPLACEMENT-call/n9/retR
      false, // !REPLACEMENT-call/n10/retR
      false, // !REPLACEMENT-call/nR/retR
      false, // !REPLACEMENT-call-compiled/n0/retR
      false, // !REPLACEMENT-call-compiled/n1/retR
      false, // !REPLACEMENT-call-compiled/n2/retR
      false, // !REPLACEMENT-call-compiled/n3/retR
      false, // !REPLACEMENT-call-compiled/n4/retR
      false, // !REPLACEMENT-call-compiled/n5/retR
      false, // !REPLACEMENT-call-compiled/n6/retR
      false, // !REPLACEMENT-call-compiled/n7/retR
      false, // !REPLACEMENT-call-compiled/n8/retR
      false, // !REPLACEMENT-call-compiled/n9/retR
      false, // !REPLACEMENT-call-compiled/n10/retR
      false, // !REPLACEMENT-call-compiled/nR/retR
      false, // !REPLACEMENT-call-from-c/retR
      false, // !REPLACEMENT-cdr-register/%rR/%rR/fR/retR
      false, // !REPLACEMENT-check-closure/fR/retR
      false, // !REPLACEMENT-check-global-defined/nR/fR/retR
      false, // !REPLACEMENT-check-in-arity/n0/fR/retR
      false, // !REPLACEMENT-check-in-arity/n1/fR/retR
      false, // !REPLACEMENT-check-in-arity/n2/fR/retR
      false, // !REPLACEMENT-check-in-arity/n3/fR/retR
      false, // !REPLACEMENT-check-in-arity/n4/fR/retR
      false, // !REPLACEMENT-check-in-arity/n5/fR/retR
      false, // !REPLACEMENT-check-in-arity/n6/fR/retR
      false, // !REPLACEMENT-check-in-arity/n7/fR/retR
      false, // !REPLACEMENT-check-in-arity/n8/fR/retR
      false, // !REPLACEMENT-check-in-arity/n9/fR/retR
      false, // !REPLACEMENT-check-in-arity/n10/fR/retR
      false, // !REPLACEMENT-check-in-arity/nR/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n0/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n1/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n2/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n3/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n4/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n5/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n6/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n7/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n8/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n9/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/n10/fR/retR
      false, // !REPLACEMENT-check-in-arity--alt/nR/fR/retR
      false, // !REPLACEMENT-exitvm/retR
      false, // !REPLACEMENT-gc-if-needed/fR/retR
      false, // !REPLACEMENT-one-minus-register/%rR/%rR/fR/retR
      false, // !REPLACEMENT-one-plus-register/%rR/%rR/fR/retR
      false, // !REPLACEMENT-pop-to-global/nR/fR/retR
      false, // !REPLACEMENT-pop-to-global-defined/nR/fR/retR
      false, // !REPLACEMENT-primitive/nR/n0/fR/retR
      false, // !REPLACEMENT-primitive/nR/n1/fR/retR
      false, // !REPLACEMENT-primitive/nR/n2/fR/retR
      false, // !REPLACEMENT-primitive/nR/n3/fR/retR
      false, // !REPLACEMENT-primitive/nR/n4/fR/retR
      false, // !REPLACEMENT-primitive/nR/nR/fR/retR
      false, // !REPLACEMENT-primitive-box-get/fR/retR
      false, // !REPLACEMENT-primitive-box-setb-special/fR/retR
      false, // !REPLACEMENT-primitive-car/fR/retR
      false, // !REPLACEMENT-primitive-cdr/fR/retR
      false, // !REPLACEMENT-primitive-fixnum-eqp/fR/retR
      false, // !REPLACEMENT-primitive-fixnum-not-eqp/fR/retR
      false, // !REPLACEMENT-primitive-greaterp/fR/retR
      false, // !REPLACEMENT-primitive-lessp/fR/retR
      false, // !REPLACEMENT-primitive-negate/fR/retR
      false, // !REPLACEMENT-primitive-negativep/fR/retR
      false, // !REPLACEMENT-primitive-non-negativep/fR/retR
      false, // !REPLACEMENT-primitive-non-positivep/fR/retR
      false, // !REPLACEMENT-primitive-non-zerop/fR/retR
      false, // !REPLACEMENT-primitive-not-greaterp/fR/retR
      false, // !REPLACEMENT-primitive-not-lessp/fR/retR
      false, // !REPLACEMENT-primitive-one-minus/fR/retR
      false, // !REPLACEMENT-primitive-one-plus/fR/retR
      false, // !REPLACEMENT-primitive-positivep/fR/retR
      false, // !REPLACEMENT-primitive-primordial-divided/fR/retR
      false, // !REPLACEMENT-primitive-primordial-divided-unsafe/fR/retR
      false, // !REPLACEMENT-primitive-primordial-minus/fR/retR
      false, // !REPLACEMENT-primitive-primordial-plus/fR/retR
      false, // !REPLACEMENT-primitive-primordial-times/fR/retR
      false, // !REPLACEMENT-primitive-quotient/fR/retR
      false, // !REPLACEMENT-primitive-quotient-unsafe/fR/retR
      false, // !REPLACEMENT-primitive-remainder/fR/retR
      false, // !REPLACEMENT-primitive-remainder-unsafe/fR/retR
      false, // !REPLACEMENT-primitive-set-carb-special/fR/retR
      false, // !REPLACEMENT-primitive-set-cdrb-special/fR/retR
      false, // !REPLACEMENT-primitive-two-divided/fR/retR
      false, // !REPLACEMENT-primitive-two-quotient/fR/retR
      false, // !REPLACEMENT-primitive-two-remainder/fR/retR
      false, // !REPLACEMENT-primitive-two-times/fR/retR
      false, // !REPLACEMENT-primitive-zerop/fR/retR
      true, // !REPLACEMENT-procedure-prolog/retR
      false, // !REPLACEMENT-push-global/nR/fR/retR
      false, // !REPLACEMENT-return/retR
      false, // !REPLACEMENT-tail-call/n0/retR
      false, // !REPLACEMENT-tail-call/n1/retR
      false, // !REPLACEMENT-tail-call/n2/retR
      false, // !REPLACEMENT-tail-call/n3/retR
      false, // !REPLACEMENT-tail-call/n4/retR
      false, // !REPLACEMENT-tail-call/n5/retR
      false, // !REPLACEMENT-tail-call/n6/retR
      false, // !REPLACEMENT-tail-call/n7/retR
      false, // !REPLACEMENT-tail-call/n8/retR
      false, // !REPLACEMENT-tail-call/n9/retR
      false, // !REPLACEMENT-tail-call/n10/retR
      false, // !REPLACEMENT-tail-call/nR/retR
      false, // !REPLACEMENT-tail-call-compiled/n0/retR
      false, // !REPLACEMENT-tail-call-compiled/n1/retR
      false, // !REPLACEMENT-tail-call-compiled/n2/retR
      false, // !REPLACEMENT-tail-call-compiled/n3/retR
      false, // !REPLACEMENT-tail-call-compiled/n4/retR
      false, // !REPLACEMENT-tail-call-compiled/n5/retR
      false, // !REPLACEMENT-tail-call-compiled/n6/retR
      false, // !REPLACEMENT-tail-call-compiled/n7/retR
      false, // !REPLACEMENT-tail-call-compiled/n8/retR
      false, // !REPLACEMENT-tail-call-compiled/n9/retR
      false, // !REPLACEMENT-tail-call-compiled/n10/retR
      false, // !REPLACEMENT-tail-call-compiled/nR/retR
      false // !REPLACEMENT-unreachable/retR
    };

/* An array whose indices are specialised instruction opcodes, and
   whose elements are the corresponding unspecialised instructions
   opcodes -- or -1 when there is no mapping mapping having */
const int
jitterlispvm_specialized_instruction_to_unspecialized_instruction
   [JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO]
  = {
    -1, /* !INVALID */
    -1, /* !BEGINBASICBLOCK */
    -1, /* !EXITVM */
    -1, /* !DATALOCATIONS */
    -1, /* !NOP */
    -1, /* !UNREACHABLE0 */
    -1, /* !UNREACHABLE1 */
    -1, /* !PRETENDTOJUMPANYWHERE */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n1/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n2/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n3/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n4/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n5/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n6/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n7/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n8/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n9/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/n10/%rR */
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister, /* at-depth-to-register/nR/%rR */
    jitterlispvm_meta_instruction_id_branch, /* branch/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mfalse, /* branch-if-false/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mnon_mpositive, /* branch-if-non-positive/fR/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mnot_mless, /* branch-if-not-less/fR/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mnot_mnull, /* branch-if-not-null/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mnull, /* branch-if-null/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mpositive, /* branch-if-positive/fR/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mnon_mpositive, /* branch-if-register-non-positive/%rR/fR/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mnon_mzero, /* branch-if-register-non-zero/%rR/fR/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mnot_mnull, /* branch-if-register-not-null/%rR/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mnull, /* branch-if-register-null/%rR/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mpositive, /* branch-if-register-positive/%rR/fR/fR */
    jitterlispvm_meta_instruction_id_branch_mif_mtrue, /* branch-if-true/fR */
    jitterlispvm_meta_instruction_id_call, /* call/n0/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n1/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n2/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n3/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n4/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n5/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n6/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n7/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n8/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n9/retR */
    jitterlispvm_meta_instruction_id_call, /* call/n10/retR */
    jitterlispvm_meta_instruction_id_call, /* call/nR/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n0/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n1/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n2/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n3/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n4/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n5/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n6/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n7/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n8/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n9/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/n10/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* call-compiled/nR/retR */
    jitterlispvm_meta_instruction_id_call_mfrom_mc, /* call-from-c/retR */
    jitterlispvm_meta_instruction_id_canonicalize_mboolean, /* canonicalize-boolean */
    jitterlispvm_meta_instruction_id_cdr_mregister, /* cdr-register/%rR/%rR/fR */
    jitterlispvm_meta_instruction_id_check_mclosure, /* check-closure/fR */
    jitterlispvm_meta_instruction_id_check_mglobal_mdefined, /* check-global-defined/nR/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n0/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n1/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n2/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n3/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n4/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n5/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n6/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n7/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n8/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n9/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/n10/fR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* check-in-arity/nR/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n0/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n1/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n2/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n3/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n4/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n5/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n6/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n7/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n8/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n9/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/n10/fR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* check-in-arity--alt/nR/fR */
    jitterlispvm_meta_instruction_id_copy_mfrom_mliteral, /* copy-from-literal/nR */
    jitterlispvm_meta_instruction_id_copy_mfrom_mregister, /* copy-from-register/%rR */
    jitterlispvm_meta_instruction_id_copy_mto_mregister, /* copy-to-register/%rR */
    jitterlispvm_meta_instruction_id_drop, /* drop */
    jitterlispvm_meta_instruction_id_drop_mnip, /* drop-nip */
    jitterlispvm_meta_instruction_id_dup, /* dup */
    jitterlispvm_meta_instruction_id_exitvm, /* exitvm */
    jitterlispvm_meta_instruction_id_fail, /* fail/retR */
    jitterlispvm_meta_instruction_id_gc_mif_mneeded, /* gc-if-needed/fR */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n4 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n8 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n12 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n16 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n24 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n32 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n36 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n48 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n52 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/n64 */
    jitterlispvm_meta_instruction_id_heap_mallocate, /* heap-allocate/nR */
    jitterlispvm_meta_instruction_id_literal_mto_mregister, /* literal-to-register/nR/%rR */
    jitterlispvm_meta_instruction_id_nip, /* nip */
    jitterlispvm_meta_instruction_id_nip_mdrop, /* nip-drop */
    jitterlispvm_meta_instruction_id_nip_mfive, /* nip-five */
    jitterlispvm_meta_instruction_id_nip_mfive_mdrop, /* nip-five-drop */
    jitterlispvm_meta_instruction_id_nip_mfour, /* nip-four */
    jitterlispvm_meta_instruction_id_nip_mfour_mdrop, /* nip-four-drop */
    jitterlispvm_meta_instruction_id_nip_mpush_mliteral, /* nip-push-literal/nR */
    jitterlispvm_meta_instruction_id_nip_mpush_mregister, /* nip-push-register/%rR */
    jitterlispvm_meta_instruction_id_nip_msix, /* nip-six */
    jitterlispvm_meta_instruction_id_nip_msix_mdrop, /* nip-six-drop */
    jitterlispvm_meta_instruction_id_nip_mthree, /* nip-three */
    jitterlispvm_meta_instruction_id_nip_mthree_mdrop, /* nip-three-drop */
    jitterlispvm_meta_instruction_id_nip_mtwo, /* nip-two */
    jitterlispvm_meta_instruction_id_nip_mtwo_mdrop, /* nip-two-drop */
    jitterlispvm_meta_instruction_id_nop, /* nop */
    jitterlispvm_meta_instruction_id_one_mminus_mregister, /* one-minus-register/%rR/%rR/fR */
    jitterlispvm_meta_instruction_id_one_mplus_mregister, /* one-plus-register/%rR/%rR/fR */
    jitterlispvm_meta_instruction_id_pop_mto_mglobal, /* pop-to-global/nR/fR */
    jitterlispvm_meta_instruction_id_pop_mto_mglobal_mdefined, /* pop-to-global-defined/nR/fR */
    jitterlispvm_meta_instruction_id_pop_mto_mregister, /* pop-to-register/%rR */
    jitterlispvm_meta_instruction_id_primitive, /* primitive/nR/n0/fR */
    jitterlispvm_meta_instruction_id_primitive, /* primitive/nR/n1/fR */
    jitterlispvm_meta_instruction_id_primitive, /* primitive/nR/n2/fR */
    jitterlispvm_meta_instruction_id_primitive, /* primitive/nR/n3/fR */
    jitterlispvm_meta_instruction_id_primitive, /* primitive/nR/n4/fR */
    jitterlispvm_meta_instruction_id_primitive, /* primitive/nR/nR/fR */
    jitterlispvm_meta_instruction_id_primitive_mboolean_mcanonicalize, /* primitive-boolean-canonicalize */
    jitterlispvm_meta_instruction_id_primitive_mbox, /* primitive-box */
    jitterlispvm_meta_instruction_id_primitive_mbox_mget, /* primitive-box-get/fR */
    jitterlispvm_meta_instruction_id_primitive_mbox_msetb_mspecial, /* primitive-box-setb-special/fR */
    jitterlispvm_meta_instruction_id_primitive_mcar, /* primitive-car/fR */
    jitterlispvm_meta_instruction_id_primitive_mcdr, /* primitive-cdr/fR */
    jitterlispvm_meta_instruction_id_primitive_mcharacterp, /* primitive-characterp */
    jitterlispvm_meta_instruction_id_primitive_mcons_mspecial, /* primitive-cons-special */
    jitterlispvm_meta_instruction_id_primitive_mconsp, /* primitive-consp */
    jitterlispvm_meta_instruction_id_primitive_meqp, /* primitive-eqp */
    jitterlispvm_meta_instruction_id_primitive_mfixnum_meqp, /* primitive-fixnum-eqp/fR */
    jitterlispvm_meta_instruction_id_primitive_mfixnum_mnot_meqp, /* primitive-fixnum-not-eqp/fR */
    jitterlispvm_meta_instruction_id_primitive_mfixnump, /* primitive-fixnump */
    jitterlispvm_meta_instruction_id_primitive_mgreaterp, /* primitive-greaterp/fR */
    jitterlispvm_meta_instruction_id_primitive_mlessp, /* primitive-lessp/fR */
    jitterlispvm_meta_instruction_id_primitive_mnegate, /* primitive-negate/fR */
    jitterlispvm_meta_instruction_id_primitive_mnegativep, /* primitive-negativep/fR */
    jitterlispvm_meta_instruction_id_primitive_mnon_mconsp, /* primitive-non-consp */
    jitterlispvm_meta_instruction_id_primitive_mnon_mnegativep, /* primitive-non-negativep/fR */
    jitterlispvm_meta_instruction_id_primitive_mnon_mnullp, /* primitive-non-nullp */
    jitterlispvm_meta_instruction_id_primitive_mnon_mpositivep, /* primitive-non-positivep/fR */
    jitterlispvm_meta_instruction_id_primitive_mnon_msymbolp, /* primitive-non-symbolp */
    jitterlispvm_meta_instruction_id_primitive_mnon_mzerop, /* primitive-non-zerop/fR */
    jitterlispvm_meta_instruction_id_primitive_mnot, /* primitive-not */
    jitterlispvm_meta_instruction_id_primitive_mnot_meqp, /* primitive-not-eqp */
    jitterlispvm_meta_instruction_id_primitive_mnot_mgreaterp, /* primitive-not-greaterp/fR */
    jitterlispvm_meta_instruction_id_primitive_mnot_mlessp, /* primitive-not-lessp/fR */
    jitterlispvm_meta_instruction_id_primitive_mnothingp, /* primitive-nothingp */
    jitterlispvm_meta_instruction_id_primitive_mnullp, /* primitive-nullp */
    jitterlispvm_meta_instruction_id_primitive_mone_mminus, /* primitive-one-minus/fR */
    jitterlispvm_meta_instruction_id_primitive_mone_mplus, /* primitive-one-plus/fR */
    jitterlispvm_meta_instruction_id_primitive_mpositivep, /* primitive-positivep/fR */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mdivided, /* primitive-primordial-divided/fR */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mdivided_munsafe, /* primitive-primordial-divided-unsafe/fR */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mminus, /* primitive-primordial-minus/fR */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mplus, /* primitive-primordial-plus/fR */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mtimes, /* primitive-primordial-times/fR */
    jitterlispvm_meta_instruction_id_primitive_mquotient, /* primitive-quotient/fR */
    jitterlispvm_meta_instruction_id_primitive_mquotient_munsafe, /* primitive-quotient-unsafe/fR */
    jitterlispvm_meta_instruction_id_primitive_mremainder, /* primitive-remainder/fR */
    jitterlispvm_meta_instruction_id_primitive_mremainder_munsafe, /* primitive-remainder-unsafe/fR */
    jitterlispvm_meta_instruction_id_primitive_mset_mcarb_mspecial, /* primitive-set-carb-special/fR */
    jitterlispvm_meta_instruction_id_primitive_mset_mcdrb_mspecial, /* primitive-set-cdrb-special/fR */
    jitterlispvm_meta_instruction_id_primitive_msymbolp, /* primitive-symbolp */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mdivided, /* primitive-two-divided/fR */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mquotient, /* primitive-two-quotient/fR */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mremainder, /* primitive-two-remainder/fR */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mtimes, /* primitive-two-times/fR */
    jitterlispvm_meta_instruction_id_primitive_muniquep, /* primitive-uniquep */
    jitterlispvm_meta_instruction_id_primitive_mzerop, /* primitive-zerop/fR */
    jitterlispvm_meta_instruction_id_procedure_mprolog, /* procedure-prolog */
    jitterlispvm_meta_instruction_id_push_mfalse, /* push-false */
    jitterlispvm_meta_instruction_id_push_mglobal, /* push-global/nR/fR */
    jitterlispvm_meta_instruction_id_push_mliteral, /* push-literal/nR */
    jitterlispvm_meta_instruction_id_push_mnil, /* push-nil */
    jitterlispvm_meta_instruction_id_push_mnothing, /* push-nothing */
    jitterlispvm_meta_instruction_id_push_mone, /* push-one */
    jitterlispvm_meta_instruction_id_push_mregister, /* push-register/%rR */
    jitterlispvm_meta_instruction_id_push_munspecified, /* push-unspecified */
    jitterlispvm_meta_instruction_id_push_mzero, /* push-zero */
    jitterlispvm_meta_instruction_id_register_mto_mregister, /* register-to-register/%rR/%rR */
    jitterlispvm_meta_instruction_id_restore_mregister, /* restore-register/%rR */
    jitterlispvm_meta_instruction_id_return, /* return */
    jitterlispvm_meta_instruction_id_save_mregister, /* save-register/%rR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n0/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n1/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n2/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n3/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n4/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n5/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n6/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n7/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n8/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n9/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/n10/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* tail-call/nR/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n0/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n1/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n2/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n3/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n4/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n5/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n6/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n7/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n8/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n9/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/n10/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* tail-call-compiled/nR/retR */
    jitterlispvm_meta_instruction_id_unreachable, /* unreachable */
    jitterlispvm_meta_instruction_id_branch, /* !REPLACEMENT-branch/fR/retR */
    jitterlispvm_meta_instruction_id_branch_mif_mfalse, /* !REPLACEMENT-branch-if-false/fR/retR */
    jitterlispvm_meta_instruction_id_branch_mif_mnon_mpositive, /* !REPLACEMENT-branch-if-non-positive/fR/fR/retR */
    jitterlispvm_meta_instruction_id_branch_mif_mnot_mless, /* !REPLACEMENT-branch-if-not-less/fR/fR/retR */
    jitterlispvm_meta_instruction_id_branch_mif_mnot_mnull, /* !REPLACEMENT-branch-if-not-null/fR/retR */
    jitterlispvm_meta_instruction_id_branch_mif_mnull, /* !REPLACEMENT-branch-if-null/fR/retR */
    jitterlispvm_meta_instruction_id_branch_mif_mpositive, /* !REPLACEMENT-branch-if-positive/fR/fR/retR */
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mnon_mpositive, /* !REPLACEMENT-branch-if-register-non-positive/%rR/fR/fR/retR */
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mnon_mzero, /* !REPLACEMENT-branch-if-register-non-zero/%rR/fR/fR/retR */
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mnot_mnull, /* !REPLACEMENT-branch-if-register-not-null/%rR/fR/retR */
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mnull, /* !REPLACEMENT-branch-if-register-null/%rR/fR/retR */
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mpositive, /* !REPLACEMENT-branch-if-register-positive/%rR/fR/fR/retR */
    jitterlispvm_meta_instruction_id_branch_mif_mtrue, /* !REPLACEMENT-branch-if-true/fR/retR */
    jitterlispvm_meta_instruction_id_call, /* !REPLACEMENT-call/n0/retR */
    jitterlispvm_meta_instruction_id_call, /* !REPLACEMENT-call/n1/retR */
    jitterlispvm_meta_instruction_id_call, /* !REPLACEMENT-call/n2/retR */
    jitterlispvm_meta_instruction_id_call, /* !REPLACEMENT-call/n3/retR */
    jitterlispvm_meta_instruction_id_call, /* !REPLACEMENT-call/n4/retR */
    jitterlispvm_meta_instruction_id_call, /* !REPLACEMENT-call/n5/retR */
    jitterlispvm_meta_instruction_id_call, /* !REPLACEMENT-call/n6/retR */
    jitterlispvm_meta_instruction_id_call, /* !REPLACEMENT-call/n7/retR */
    jitterlispvm_meta_instruction_id_call, /* !REPLACEMENT-call/n8/retR */
    jitterlispvm_meta_instruction_id_call, /* !REPLACEMENT-call/n9/retR */
    jitterlispvm_meta_instruction_id_call, /* !REPLACEMENT-call/n10/retR */
    jitterlispvm_meta_instruction_id_call, /* !REPLACEMENT-call/nR/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* !REPLACEMENT-call-compiled/n0/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* !REPLACEMENT-call-compiled/n1/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* !REPLACEMENT-call-compiled/n2/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* !REPLACEMENT-call-compiled/n3/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* !REPLACEMENT-call-compiled/n4/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* !REPLACEMENT-call-compiled/n5/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* !REPLACEMENT-call-compiled/n6/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* !REPLACEMENT-call-compiled/n7/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* !REPLACEMENT-call-compiled/n8/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* !REPLACEMENT-call-compiled/n9/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* !REPLACEMENT-call-compiled/n10/retR */
    jitterlispvm_meta_instruction_id_call_mcompiled, /* !REPLACEMENT-call-compiled/nR/retR */
    jitterlispvm_meta_instruction_id_call_mfrom_mc, /* !REPLACEMENT-call-from-c/retR */
    jitterlispvm_meta_instruction_id_cdr_mregister, /* !REPLACEMENT-cdr-register/%rR/%rR/fR/retR */
    jitterlispvm_meta_instruction_id_check_mclosure, /* !REPLACEMENT-check-closure/fR/retR */
    jitterlispvm_meta_instruction_id_check_mglobal_mdefined, /* !REPLACEMENT-check-global-defined/nR/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* !REPLACEMENT-check-in-arity/n0/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* !REPLACEMENT-check-in-arity/n1/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* !REPLACEMENT-check-in-arity/n2/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* !REPLACEMENT-check-in-arity/n3/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* !REPLACEMENT-check-in-arity/n4/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* !REPLACEMENT-check-in-arity/n5/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* !REPLACEMENT-check-in-arity/n6/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* !REPLACEMENT-check-in-arity/n7/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* !REPLACEMENT-check-in-arity/n8/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* !REPLACEMENT-check-in-arity/n9/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* !REPLACEMENT-check-in-arity/n10/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity, /* !REPLACEMENT-check-in-arity/nR/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* !REPLACEMENT-check-in-arity--alt/n0/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* !REPLACEMENT-check-in-arity--alt/n1/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* !REPLACEMENT-check-in-arity--alt/n2/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* !REPLACEMENT-check-in-arity--alt/n3/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* !REPLACEMENT-check-in-arity--alt/n4/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* !REPLACEMENT-check-in-arity--alt/n5/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* !REPLACEMENT-check-in-arity--alt/n6/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* !REPLACEMENT-check-in-arity--alt/n7/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* !REPLACEMENT-check-in-arity--alt/n8/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* !REPLACEMENT-check-in-arity--alt/n9/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* !REPLACEMENT-check-in-arity--alt/n10/fR/retR */
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt, /* !REPLACEMENT-check-in-arity--alt/nR/fR/retR */
    jitterlispvm_meta_instruction_id_exitvm, /* !REPLACEMENT-exitvm/retR */
    jitterlispvm_meta_instruction_id_gc_mif_mneeded, /* !REPLACEMENT-gc-if-needed/fR/retR */
    jitterlispvm_meta_instruction_id_one_mminus_mregister, /* !REPLACEMENT-one-minus-register/%rR/%rR/fR/retR */
    jitterlispvm_meta_instruction_id_one_mplus_mregister, /* !REPLACEMENT-one-plus-register/%rR/%rR/fR/retR */
    jitterlispvm_meta_instruction_id_pop_mto_mglobal, /* !REPLACEMENT-pop-to-global/nR/fR/retR */
    jitterlispvm_meta_instruction_id_pop_mto_mglobal_mdefined, /* !REPLACEMENT-pop-to-global-defined/nR/fR/retR */
    jitterlispvm_meta_instruction_id_primitive, /* !REPLACEMENT-primitive/nR/n0/fR/retR */
    jitterlispvm_meta_instruction_id_primitive, /* !REPLACEMENT-primitive/nR/n1/fR/retR */
    jitterlispvm_meta_instruction_id_primitive, /* !REPLACEMENT-primitive/nR/n2/fR/retR */
    jitterlispvm_meta_instruction_id_primitive, /* !REPLACEMENT-primitive/nR/n3/fR/retR */
    jitterlispvm_meta_instruction_id_primitive, /* !REPLACEMENT-primitive/nR/n4/fR/retR */
    jitterlispvm_meta_instruction_id_primitive, /* !REPLACEMENT-primitive/nR/nR/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mbox_mget, /* !REPLACEMENT-primitive-box-get/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mbox_msetb_mspecial, /* !REPLACEMENT-primitive-box-setb-special/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mcar, /* !REPLACEMENT-primitive-car/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mcdr, /* !REPLACEMENT-primitive-cdr/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mfixnum_meqp, /* !REPLACEMENT-primitive-fixnum-eqp/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mfixnum_mnot_meqp, /* !REPLACEMENT-primitive-fixnum-not-eqp/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mgreaterp, /* !REPLACEMENT-primitive-greaterp/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mlessp, /* !REPLACEMENT-primitive-lessp/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mnegate, /* !REPLACEMENT-primitive-negate/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mnegativep, /* !REPLACEMENT-primitive-negativep/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mnon_mnegativep, /* !REPLACEMENT-primitive-non-negativep/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mnon_mpositivep, /* !REPLACEMENT-primitive-non-positivep/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mnon_mzerop, /* !REPLACEMENT-primitive-non-zerop/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mnot_mgreaterp, /* !REPLACEMENT-primitive-not-greaterp/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mnot_mlessp, /* !REPLACEMENT-primitive-not-lessp/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mone_mminus, /* !REPLACEMENT-primitive-one-minus/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mone_mplus, /* !REPLACEMENT-primitive-one-plus/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mpositivep, /* !REPLACEMENT-primitive-positivep/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mdivided, /* !REPLACEMENT-primitive-primordial-divided/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mdivided_munsafe, /* !REPLACEMENT-primitive-primordial-divided-unsafe/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mminus, /* !REPLACEMENT-primitive-primordial-minus/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mplus, /* !REPLACEMENT-primitive-primordial-plus/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mtimes, /* !REPLACEMENT-primitive-primordial-times/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mquotient, /* !REPLACEMENT-primitive-quotient/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mquotient_munsafe, /* !REPLACEMENT-primitive-quotient-unsafe/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mremainder, /* !REPLACEMENT-primitive-remainder/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mremainder_munsafe, /* !REPLACEMENT-primitive-remainder-unsafe/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mset_mcarb_mspecial, /* !REPLACEMENT-primitive-set-carb-special/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mset_mcdrb_mspecial, /* !REPLACEMENT-primitive-set-cdrb-special/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mdivided, /* !REPLACEMENT-primitive-two-divided/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mquotient, /* !REPLACEMENT-primitive-two-quotient/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mremainder, /* !REPLACEMENT-primitive-two-remainder/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mtwo_mtimes, /* !REPLACEMENT-primitive-two-times/fR/retR */
    jitterlispvm_meta_instruction_id_primitive_mzerop, /* !REPLACEMENT-primitive-zerop/fR/retR */
    jitterlispvm_meta_instruction_id_procedure_mprolog, /* !REPLACEMENT-procedure-prolog/retR */
    jitterlispvm_meta_instruction_id_push_mglobal, /* !REPLACEMENT-push-global/nR/fR/retR */
    jitterlispvm_meta_instruction_id_return, /* !REPLACEMENT-return/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* !REPLACEMENT-tail-call/n0/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* !REPLACEMENT-tail-call/n1/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* !REPLACEMENT-tail-call/n2/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* !REPLACEMENT-tail-call/n3/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* !REPLACEMENT-tail-call/n4/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* !REPLACEMENT-tail-call/n5/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* !REPLACEMENT-tail-call/n6/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* !REPLACEMENT-tail-call/n7/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* !REPLACEMENT-tail-call/n8/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* !REPLACEMENT-tail-call/n9/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* !REPLACEMENT-tail-call/n10/retR */
    jitterlispvm_meta_instruction_id_tail_mcall, /* !REPLACEMENT-tail-call/nR/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* !REPLACEMENT-tail-call-compiled/n0/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* !REPLACEMENT-tail-call-compiled/n1/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* !REPLACEMENT-tail-call-compiled/n2/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* !REPLACEMENT-tail-call-compiled/n3/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* !REPLACEMENT-tail-call-compiled/n4/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* !REPLACEMENT-tail-call-compiled/n5/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* !REPLACEMENT-tail-call-compiled/n6/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* !REPLACEMENT-tail-call-compiled/n7/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* !REPLACEMENT-tail-call-compiled/n8/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* !REPLACEMENT-tail-call-compiled/n9/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* !REPLACEMENT-tail-call-compiled/n10/retR */
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled, /* !REPLACEMENT-tail-call-compiled/nR/retR */
    jitterlispvm_meta_instruction_id_unreachable /* !REPLACEMENT-unreachable/retR */
    };

#ifdef JITTER_HAVE_DEFECT_REPLACEMENT
/* Worst-case replacement table. */
const jitter_uint
jitterlispvm_worst_case_replacement_table [] =
  {
    jitterlispvm_specialized_instruction_opcode__eINVALID, /* !INVALID is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eBEGINBASICBLOCK, /* !BEGINBASICBLOCK is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eEXITVM, /* !EXITVM is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eDATALOCATIONS, /* !DATALOCATIONS is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eNOP, /* !NOP is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eUNREACHABLE0, /* !UNREACHABLE0 is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eUNREACHABLE1, /* !UNREACHABLE1 is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__ePRETENDTOJUMPANYWHERE, /* !PRETENDTOJUMPANYWHERE is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n1___rrR, /* at-depth-to-register/n1/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n2___rrR, /* at-depth-to-register/n2/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n3___rrR, /* at-depth-to-register/n3/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n4___rrR, /* at-depth-to-register/n4/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n5___rrR, /* at-depth-to-register/n5/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n6___rrR, /* at-depth-to-register/n6/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n7___rrR, /* at-depth-to-register/n7/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n8___rrR, /* at-depth-to-register/n8/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n9___rrR, /* at-depth-to-register/n9/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n10___rrR, /* at-depth-to-register/n10/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__nR___rrR, /* at-depth-to-register/nR/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch__fR__retR, /* branch/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-branch/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mfalse__fR__retR, /* branch-if-false/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-branch-if-false/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnon_mpositive__fR__fR__retR, /* branch-if-non-positive/fR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-branch-if-non-positive/fR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnot_mless__fR__fR__retR, /* branch-if-not-less/fR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-branch-if-not-less/fR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnot_mnull__fR__retR, /* branch-if-not-null/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-branch-if-not-null/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnull__fR__retR, /* branch-if-null/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-branch-if-null/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mpositive__fR__fR__retR, /* branch-if-positive/fR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-branch-if-positive/fR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnon_mpositive___rrR__fR__fR__retR, /* branch-if-register-non-positive/%rR/fR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-branch-if-register-non-positive/%rR/fR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnon_mzero___rrR__fR__fR__retR, /* branch-if-register-non-zero/%rR/fR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-branch-if-register-non-zero/%rR/fR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnot_mnull___rrR__fR__retR, /* branch-if-register-not-null/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-branch-if-register-not-null/%rR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnull___rrR__fR__retR, /* branch-if-register-null/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-branch-if-register-null/%rR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mpositive___rrR__fR__fR__retR, /* branch-if-register-positive/%rR/fR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-branch-if-register-positive/%rR/fR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mtrue__fR__retR, /* branch-if-true/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-branch-if-true/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n0__retR, /* call/n0/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call/n0/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n1__retR, /* call/n1/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call/n1/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n2__retR, /* call/n2/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call/n2/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n3__retR, /* call/n3/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call/n3/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n4__retR, /* call/n4/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call/n4/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n5__retR, /* call/n5/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call/n5/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n6__retR, /* call/n6/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call/n6/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n7__retR, /* call/n7/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call/n7/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n8__retR, /* call/n8/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call/n8/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n9__retR, /* call/n9/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call/n9/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n10__retR, /* call/n10/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call/n10/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__nR__retR, /* call/nR/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call/nR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n0__retR, /* call-compiled/n0/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call-compiled/n0/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n1__retR, /* call-compiled/n1/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call-compiled/n1/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n2__retR, /* call-compiled/n2/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call-compiled/n2/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n3__retR, /* call-compiled/n3/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call-compiled/n3/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n4__retR, /* call-compiled/n4/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call-compiled/n4/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n5__retR, /* call-compiled/n5/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call-compiled/n5/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n6__retR, /* call-compiled/n6/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call-compiled/n6/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n7__retR, /* call-compiled/n7/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call-compiled/n7/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n8__retR, /* call-compiled/n8/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call-compiled/n8/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n9__retR, /* call-compiled/n9/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call-compiled/n9/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n10__retR, /* call-compiled/n10/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call-compiled/n10/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__nR__retR, /* call-compiled/nR/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call-compiled/nR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mfrom_mc__retR, /* call-from-c/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call-from-c/retR. */
    jitterlispvm_specialized_instruction_opcode_canonicalize_mboolean, /* canonicalize-boolean is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcdr_mregister___rrR___rrR__fR__retR, /* cdr-register/%rR/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-cdr-register/%rR/%rR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_mclosure__fR__retR, /* check-closure/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-closure/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_mglobal_mdefined__nR__fR__retR, /* check-global-defined/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-global-defined/nR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n0__fR__retR, /* check-in-arity/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity/n0/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n1__fR__retR, /* check-in-arity/n1/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity/n1/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n2__fR__retR, /* check-in-arity/n2/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity/n2/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n3__fR__retR, /* check-in-arity/n3/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity/n3/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n4__fR__retR, /* check-in-arity/n4/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity/n4/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n5__fR__retR, /* check-in-arity/n5/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity/n5/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n6__fR__retR, /* check-in-arity/n6/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity/n6/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n7__fR__retR, /* check-in-arity/n7/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity/n7/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n8__fR__retR, /* check-in-arity/n8/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity/n8/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n9__fR__retR, /* check-in-arity/n9/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity/n9/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n10__fR__retR, /* check-in-arity/n10/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity/n10/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__nR__fR__retR, /* check-in-arity/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity/nR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n0__fR__retR, /* check-in-arity--alt/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity--alt/n0/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n1__fR__retR, /* check-in-arity--alt/n1/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity--alt/n1/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n2__fR__retR, /* check-in-arity--alt/n2/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity--alt/n2/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n3__fR__retR, /* check-in-arity--alt/n3/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity--alt/n3/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n4__fR__retR, /* check-in-arity--alt/n4/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity--alt/n4/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n5__fR__retR, /* check-in-arity--alt/n5/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity--alt/n5/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n6__fR__retR, /* check-in-arity--alt/n6/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity--alt/n6/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n7__fR__retR, /* check-in-arity--alt/n7/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity--alt/n7/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n8__fR__retR, /* check-in-arity--alt/n8/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity--alt/n8/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n9__fR__retR, /* check-in-arity--alt/n9/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity--alt/n9/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n10__fR__retR, /* check-in-arity--alt/n10/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity--alt/n10/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__nR__fR__retR, /* check-in-arity--alt/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-check-in-arity--alt/nR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode_copy_mfrom_mliteral__nR, /* copy-from-literal/nR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_copy_mfrom_mregister___rrR, /* copy-from-register/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_copy_mto_mregister___rrR, /* copy-to-register/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_drop, /* drop is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_drop_mnip, /* drop-nip is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_dup, /* dup is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mexitvm__retR, /* exitvm is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-exitvm/retR. */
    jitterlispvm_specialized_instruction_opcode_fail__retR, /* fail/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mgc_mif_mneeded__fR__retR, /* gc-if-needed/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-gc-if-needed/fR/retR. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n4, /* heap-allocate/n4 is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n8, /* heap-allocate/n8 is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n12, /* heap-allocate/n12 is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n16, /* heap-allocate/n16 is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n24, /* heap-allocate/n24 is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n32, /* heap-allocate/n32 is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n36, /* heap-allocate/n36 is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n48, /* heap-allocate/n48 is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n52, /* heap-allocate/n52 is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n64, /* heap-allocate/n64 is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__nR, /* heap-allocate/nR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_literal_mto_mregister__nR___rrR, /* literal-to-register/nR/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip, /* nip is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mdrop, /* nip-drop is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mfive, /* nip-five is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mfive_mdrop, /* nip-five-drop is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mfour, /* nip-four is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mfour_mdrop, /* nip-four-drop is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mpush_mliteral__nR, /* nip-push-literal/nR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mpush_mregister___rrR, /* nip-push-register/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_msix, /* nip-six is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_msix_mdrop, /* nip-six-drop is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mthree, /* nip-three is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mthree_mdrop, /* nip-three-drop is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mtwo, /* nip-two is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nip_mtwo_mdrop, /* nip-two-drop is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_nop, /* nop is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mone_mminus_mregister___rrR___rrR__fR__retR, /* one-minus-register/%rR/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-one-minus-register/%rR/%rR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mone_mplus_mregister___rrR___rrR__fR__retR, /* one-plus-register/%rR/%rR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-one-plus-register/%rR/%rR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mpop_mto_mglobal__nR__fR__retR, /* pop-to-global/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-pop-to-global/nR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mpop_mto_mglobal_mdefined__nR__fR__retR, /* pop-to-global-defined/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-pop-to-global-defined/nR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode_pop_mto_mregister___rrR, /* pop-to-register/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n0__fR__retR, /* primitive/nR/n0/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive/nR/n0/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n1__fR__retR, /* primitive/nR/n1/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive/nR/n1/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n2__fR__retR, /* primitive/nR/n2/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive/nR/n2/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n3__fR__retR, /* primitive/nR/n3/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive/nR/n3/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n4__fR__retR, /* primitive/nR/n4/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive/nR/n4/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__nR__fR__retR, /* primitive/nR/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive/nR/nR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode_primitive_mboolean_mcanonicalize, /* primitive-boolean-canonicalize is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_primitive_mbox, /* primitive-box is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mbox_mget__fR__retR, /* primitive-box-get/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-box-get/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mbox_msetb_mspecial__fR__retR, /* primitive-box-setb-special/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-box-setb-special/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mcar__fR__retR, /* primitive-car/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-car/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mcdr__fR__retR, /* primitive-cdr/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-cdr/fR/retR. */
    jitterlispvm_specialized_instruction_opcode_primitive_mcharacterp, /* primitive-characterp is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_primitive_mcons_mspecial, /* primitive-cons-special is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_primitive_mconsp, /* primitive-consp is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_primitive_meqp, /* primitive-eqp is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mfixnum_meqp__fR__retR, /* primitive-fixnum-eqp/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-fixnum-eqp/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mfixnum_mnot_meqp__fR__retR, /* primitive-fixnum-not-eqp/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-fixnum-not-eqp/fR/retR. */
    jitterlispvm_specialized_instruction_opcode_primitive_mfixnump, /* primitive-fixnump is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mgreaterp__fR__retR, /* primitive-greaterp/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-greaterp/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mlessp__fR__retR, /* primitive-lessp/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-lessp/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnegate__fR__retR, /* primitive-negate/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-negate/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnegativep__fR__retR, /* primitive-negativep/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-negativep/fR/retR. */
    jitterlispvm_specialized_instruction_opcode_primitive_mnon_mconsp, /* primitive-non-consp is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnon_mnegativep__fR__retR, /* primitive-non-negativep/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-non-negativep/fR/retR. */
    jitterlispvm_specialized_instruction_opcode_primitive_mnon_mnullp, /* primitive-non-nullp is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnon_mpositivep__fR__retR, /* primitive-non-positivep/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-non-positivep/fR/retR. */
    jitterlispvm_specialized_instruction_opcode_primitive_mnon_msymbolp, /* primitive-non-symbolp is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnon_mzerop__fR__retR, /* primitive-non-zerop/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-non-zerop/fR/retR. */
    jitterlispvm_specialized_instruction_opcode_primitive_mnot, /* primitive-not is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_primitive_mnot_meqp, /* primitive-not-eqp is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnot_mgreaterp__fR__retR, /* primitive-not-greaterp/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-not-greaterp/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnot_mlessp__fR__retR, /* primitive-not-lessp/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-not-lessp/fR/retR. */
    jitterlispvm_specialized_instruction_opcode_primitive_mnothingp, /* primitive-nothingp is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_primitive_mnullp, /* primitive-nullp is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mone_mminus__fR__retR, /* primitive-one-minus/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-one-minus/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mone_mplus__fR__retR, /* primitive-one-plus/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-one-plus/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mpositivep__fR__retR, /* primitive-positivep/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-positivep/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mdivided__fR__retR, /* primitive-primordial-divided/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-primordial-divided/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mdivided_munsafe__fR__retR, /* primitive-primordial-divided-unsafe/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-primordial-divided-unsafe/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mminus__fR__retR, /* primitive-primordial-minus/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-primordial-minus/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mplus__fR__retR, /* primitive-primordial-plus/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-primordial-plus/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mtimes__fR__retR, /* primitive-primordial-times/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-primordial-times/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mquotient__fR__retR, /* primitive-quotient/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-quotient/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mquotient_munsafe__fR__retR, /* primitive-quotient-unsafe/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-quotient-unsafe/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mremainder__fR__retR, /* primitive-remainder/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-remainder/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mremainder_munsafe__fR__retR, /* primitive-remainder-unsafe/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-remainder-unsafe/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mset_mcarb_mspecial__fR__retR, /* primitive-set-carb-special/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-set-carb-special/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mset_mcdrb_mspecial__fR__retR, /* primitive-set-cdrb-special/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-set-cdrb-special/fR/retR. */
    jitterlispvm_specialized_instruction_opcode_primitive_msymbolp, /* primitive-symbolp is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mdivided__fR__retR, /* primitive-two-divided/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-two-divided/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mquotient__fR__retR, /* primitive-two-quotient/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-two-quotient/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mremainder__fR__retR, /* primitive-two-remainder/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-two-remainder/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mtimes__fR__retR, /* primitive-two-times/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-two-times/fR/retR. */
    jitterlispvm_specialized_instruction_opcode_primitive_muniquep, /* primitive-uniquep is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mzerop__fR__retR, /* primitive-zerop/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-primitive-zerop/fR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprocedure_mprolog__retR, /* procedure-prolog is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-procedure-prolog/retR. */
    jitterlispvm_specialized_instruction_opcode_push_mfalse, /* push-false is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mpush_mglobal__nR__fR__retR, /* push-global/nR/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-push-global/nR/fR/retR. */
    jitterlispvm_specialized_instruction_opcode_push_mliteral__nR, /* push-literal/nR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_push_mnil, /* push-nil is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_push_mnothing, /* push-nothing is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_push_mone, /* push-one is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_push_mregister___rrR, /* push-register/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_push_munspecified, /* push-unspecified is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_push_mzero, /* push-zero is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_register_mto_mregister___rrR___rrR, /* register-to-register/%rR/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode_restore_mregister___rrR, /* restore-register/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mreturn__retR, /* return is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-return/retR. */
    jitterlispvm_specialized_instruction_opcode_save_mregister___rrR, /* save-register/%rR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n0__retR, /* tail-call/n0/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call/n0/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n1__retR, /* tail-call/n1/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call/n1/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n2__retR, /* tail-call/n2/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call/n2/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n3__retR, /* tail-call/n3/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call/n3/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n4__retR, /* tail-call/n4/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call/n4/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n5__retR, /* tail-call/n5/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call/n5/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n6__retR, /* tail-call/n6/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call/n6/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n7__retR, /* tail-call/n7/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call/n7/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n8__retR, /* tail-call/n8/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call/n8/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n9__retR, /* tail-call/n9/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call/n9/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n10__retR, /* tail-call/n10/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call/n10/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__nR__retR, /* tail-call/nR/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call/nR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n0__retR, /* tail-call-compiled/n0/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call-compiled/n0/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n1__retR, /* tail-call-compiled/n1/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call-compiled/n1/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n2__retR, /* tail-call-compiled/n2/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call-compiled/n2/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n3__retR, /* tail-call-compiled/n3/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call-compiled/n3/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n4__retR, /* tail-call-compiled/n4/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call-compiled/n4/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n5__retR, /* tail-call-compiled/n5/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call-compiled/n5/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n6__retR, /* tail-call-compiled/n6/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call-compiled/n6/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n7__retR, /* tail-call-compiled/n7/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call-compiled/n7/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n8__retR, /* tail-call-compiled/n8/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call-compiled/n8/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n9__retR, /* tail-call-compiled/n9/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call-compiled/n9/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n10__retR, /* tail-call-compiled/n10/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call-compiled/n10/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__nR__retR, /* tail-call-compiled/nR/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-tail-call-compiled/nR/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_munreachable__retR, /* unreachable is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-unreachable/retR. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch__fR__retR, /* !REPLACEMENT-branch/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mfalse__fR__retR, /* !REPLACEMENT-branch-if-false/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnon_mpositive__fR__fR__retR, /* !REPLACEMENT-branch-if-non-positive/fR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnot_mless__fR__fR__retR, /* !REPLACEMENT-branch-if-not-less/fR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnot_mnull__fR__retR, /* !REPLACEMENT-branch-if-not-null/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnull__fR__retR, /* !REPLACEMENT-branch-if-null/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mpositive__fR__fR__retR, /* !REPLACEMENT-branch-if-positive/fR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnon_mpositive___rrR__fR__fR__retR, /* !REPLACEMENT-branch-if-register-non-positive/%rR/fR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnon_mzero___rrR__fR__fR__retR, /* !REPLACEMENT-branch-if-register-non-zero/%rR/fR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnot_mnull___rrR__fR__retR, /* !REPLACEMENT-branch-if-register-not-null/%rR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnull___rrR__fR__retR, /* !REPLACEMENT-branch-if-register-null/%rR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mpositive___rrR__fR__fR__retR, /* !REPLACEMENT-branch-if-register-positive/%rR/fR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mtrue__fR__retR, /* !REPLACEMENT-branch-if-true/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n0__retR, /* !REPLACEMENT-call/n0/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n1__retR, /* !REPLACEMENT-call/n1/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n2__retR, /* !REPLACEMENT-call/n2/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n3__retR, /* !REPLACEMENT-call/n3/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n4__retR, /* !REPLACEMENT-call/n4/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n5__retR, /* !REPLACEMENT-call/n5/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n6__retR, /* !REPLACEMENT-call/n6/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n7__retR, /* !REPLACEMENT-call/n7/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n8__retR, /* !REPLACEMENT-call/n8/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n9__retR, /* !REPLACEMENT-call/n9/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n10__retR, /* !REPLACEMENT-call/n10/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__nR__retR, /* !REPLACEMENT-call/nR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n0__retR, /* !REPLACEMENT-call-compiled/n0/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n1__retR, /* !REPLACEMENT-call-compiled/n1/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n2__retR, /* !REPLACEMENT-call-compiled/n2/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n3__retR, /* !REPLACEMENT-call-compiled/n3/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n4__retR, /* !REPLACEMENT-call-compiled/n4/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n5__retR, /* !REPLACEMENT-call-compiled/n5/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n6__retR, /* !REPLACEMENT-call-compiled/n6/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n7__retR, /* !REPLACEMENT-call-compiled/n7/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n8__retR, /* !REPLACEMENT-call-compiled/n8/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n9__retR, /* !REPLACEMENT-call-compiled/n9/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n10__retR, /* !REPLACEMENT-call-compiled/n10/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__nR__retR, /* !REPLACEMENT-call-compiled/nR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mfrom_mc__retR, /* !REPLACEMENT-call-from-c/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcdr_mregister___rrR___rrR__fR__retR, /* !REPLACEMENT-cdr-register/%rR/%rR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_mclosure__fR__retR, /* !REPLACEMENT-check-closure/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_mglobal_mdefined__nR__fR__retR, /* !REPLACEMENT-check-global-defined/nR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n0__fR__retR, /* !REPLACEMENT-check-in-arity/n0/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n1__fR__retR, /* !REPLACEMENT-check-in-arity/n1/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n2__fR__retR, /* !REPLACEMENT-check-in-arity/n2/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n3__fR__retR, /* !REPLACEMENT-check-in-arity/n3/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n4__fR__retR, /* !REPLACEMENT-check-in-arity/n4/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n5__fR__retR, /* !REPLACEMENT-check-in-arity/n5/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n6__fR__retR, /* !REPLACEMENT-check-in-arity/n6/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n7__fR__retR, /* !REPLACEMENT-check-in-arity/n7/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n8__fR__retR, /* !REPLACEMENT-check-in-arity/n8/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n9__fR__retR, /* !REPLACEMENT-check-in-arity/n9/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n10__fR__retR, /* !REPLACEMENT-check-in-arity/n10/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__nR__fR__retR, /* !REPLACEMENT-check-in-arity/nR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n0__fR__retR, /* !REPLACEMENT-check-in-arity--alt/n0/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n1__fR__retR, /* !REPLACEMENT-check-in-arity--alt/n1/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n2__fR__retR, /* !REPLACEMENT-check-in-arity--alt/n2/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n3__fR__retR, /* !REPLACEMENT-check-in-arity--alt/n3/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n4__fR__retR, /* !REPLACEMENT-check-in-arity--alt/n4/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n5__fR__retR, /* !REPLACEMENT-check-in-arity--alt/n5/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n6__fR__retR, /* !REPLACEMENT-check-in-arity--alt/n6/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n7__fR__retR, /* !REPLACEMENT-check-in-arity--alt/n7/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n8__fR__retR, /* !REPLACEMENT-check-in-arity--alt/n8/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n9__fR__retR, /* !REPLACEMENT-check-in-arity--alt/n9/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n10__fR__retR, /* !REPLACEMENT-check-in-arity--alt/n10/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__nR__fR__retR, /* !REPLACEMENT-check-in-arity--alt/nR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mexitvm__retR, /* !REPLACEMENT-exitvm/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mgc_mif_mneeded__fR__retR, /* !REPLACEMENT-gc-if-needed/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mone_mminus_mregister___rrR___rrR__fR__retR, /* !REPLACEMENT-one-minus-register/%rR/%rR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mone_mplus_mregister___rrR___rrR__fR__retR, /* !REPLACEMENT-one-plus-register/%rR/%rR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mpop_mto_mglobal__nR__fR__retR, /* !REPLACEMENT-pop-to-global/nR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mpop_mto_mglobal_mdefined__nR__fR__retR, /* !REPLACEMENT-pop-to-global-defined/nR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n0__fR__retR, /* !REPLACEMENT-primitive/nR/n0/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n1__fR__retR, /* !REPLACEMENT-primitive/nR/n1/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n2__fR__retR, /* !REPLACEMENT-primitive/nR/n2/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n3__fR__retR, /* !REPLACEMENT-primitive/nR/n3/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n4__fR__retR, /* !REPLACEMENT-primitive/nR/n4/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__nR__fR__retR, /* !REPLACEMENT-primitive/nR/nR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mbox_mget__fR__retR, /* !REPLACEMENT-primitive-box-get/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mbox_msetb_mspecial__fR__retR, /* !REPLACEMENT-primitive-box-setb-special/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mcar__fR__retR, /* !REPLACEMENT-primitive-car/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mcdr__fR__retR, /* !REPLACEMENT-primitive-cdr/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mfixnum_meqp__fR__retR, /* !REPLACEMENT-primitive-fixnum-eqp/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mfixnum_mnot_meqp__fR__retR, /* !REPLACEMENT-primitive-fixnum-not-eqp/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mgreaterp__fR__retR, /* !REPLACEMENT-primitive-greaterp/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mlessp__fR__retR, /* !REPLACEMENT-primitive-lessp/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnegate__fR__retR, /* !REPLACEMENT-primitive-negate/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnegativep__fR__retR, /* !REPLACEMENT-primitive-negativep/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnon_mnegativep__fR__retR, /* !REPLACEMENT-primitive-non-negativep/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnon_mpositivep__fR__retR, /* !REPLACEMENT-primitive-non-positivep/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnon_mzerop__fR__retR, /* !REPLACEMENT-primitive-non-zerop/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnot_mgreaterp__fR__retR, /* !REPLACEMENT-primitive-not-greaterp/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnot_mlessp__fR__retR, /* !REPLACEMENT-primitive-not-lessp/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mone_mminus__fR__retR, /* !REPLACEMENT-primitive-one-minus/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mone_mplus__fR__retR, /* !REPLACEMENT-primitive-one-plus/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mpositivep__fR__retR, /* !REPLACEMENT-primitive-positivep/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mdivided__fR__retR, /* !REPLACEMENT-primitive-primordial-divided/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mdivided_munsafe__fR__retR, /* !REPLACEMENT-primitive-primordial-divided-unsafe/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mminus__fR__retR, /* !REPLACEMENT-primitive-primordial-minus/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mplus__fR__retR, /* !REPLACEMENT-primitive-primordial-plus/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mtimes__fR__retR, /* !REPLACEMENT-primitive-primordial-times/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mquotient__fR__retR, /* !REPLACEMENT-primitive-quotient/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mquotient_munsafe__fR__retR, /* !REPLACEMENT-primitive-quotient-unsafe/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mremainder__fR__retR, /* !REPLACEMENT-primitive-remainder/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mremainder_munsafe__fR__retR, /* !REPLACEMENT-primitive-remainder-unsafe/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mset_mcarb_mspecial__fR__retR, /* !REPLACEMENT-primitive-set-carb-special/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mset_mcdrb_mspecial__fR__retR, /* !REPLACEMENT-primitive-set-cdrb-special/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mdivided__fR__retR, /* !REPLACEMENT-primitive-two-divided/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mquotient__fR__retR, /* !REPLACEMENT-primitive-two-quotient/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mremainder__fR__retR, /* !REPLACEMENT-primitive-two-remainder/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mtimes__fR__retR, /* !REPLACEMENT-primitive-two-times/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mzerop__fR__retR, /* !REPLACEMENT-primitive-zerop/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprocedure_mprolog__retR, /* !REPLACEMENT-procedure-prolog/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mpush_mglobal__nR__fR__retR, /* !REPLACEMENT-push-global/nR/fR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mreturn__retR, /* !REPLACEMENT-return/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n0__retR, /* !REPLACEMENT-tail-call/n0/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n1__retR, /* !REPLACEMENT-tail-call/n1/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n2__retR, /* !REPLACEMENT-tail-call/n2/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n3__retR, /* !REPLACEMENT-tail-call/n3/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n4__retR, /* !REPLACEMENT-tail-call/n4/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n5__retR, /* !REPLACEMENT-tail-call/n5/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n6__retR, /* !REPLACEMENT-tail-call/n6/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n7__retR, /* !REPLACEMENT-tail-call/n7/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n8__retR, /* !REPLACEMENT-tail-call/n8/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n9__retR, /* !REPLACEMENT-tail-call/n9/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n10__retR, /* !REPLACEMENT-tail-call/n10/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__nR__retR, /* !REPLACEMENT-tail-call/nR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n0__retR, /* !REPLACEMENT-tail-call-compiled/n0/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n1__retR, /* !REPLACEMENT-tail-call-compiled/n1/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n2__retR, /* !REPLACEMENT-tail-call-compiled/n2/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n3__retR, /* !REPLACEMENT-tail-call-compiled/n3/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n4__retR, /* !REPLACEMENT-tail-call-compiled/n4/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n5__retR, /* !REPLACEMENT-tail-call-compiled/n5/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n6__retR, /* !REPLACEMENT-tail-call-compiled/n6/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n7__retR, /* !REPLACEMENT-tail-call-compiled/n7/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n8__retR, /* !REPLACEMENT-tail-call-compiled/n8/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n9__retR, /* !REPLACEMENT-tail-call-compiled/n9/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n10__retR, /* !REPLACEMENT-tail-call-compiled/n10/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__nR__retR, /* !REPLACEMENT-tail-call-compiled/nR/retR is NOT potentially defective. */
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_munreachable__retR /* !REPLACEMENT-unreachable/retR is NOT potentially defective. */
  };
#endif // #ifdef JITTER_HAVE_DEFECT_REPLACEMENT


#ifdef JITTER_HAVE_DEFECT_REPLACEMENT
const jitter_uint
jitterlispvm_call_related_specialized_instruction_ids []
= {
    jitterlispvm_specialized_instruction_opcode_call__n0__retR,
    jitterlispvm_specialized_instruction_opcode_call__n1__retR,
    jitterlispvm_specialized_instruction_opcode_call__n2__retR,
    jitterlispvm_specialized_instruction_opcode_call__n3__retR,
    jitterlispvm_specialized_instruction_opcode_call__n4__retR,
    jitterlispvm_specialized_instruction_opcode_call__n5__retR,
    jitterlispvm_specialized_instruction_opcode_call__n6__retR,
    jitterlispvm_specialized_instruction_opcode_call__n7__retR,
    jitterlispvm_specialized_instruction_opcode_call__n8__retR,
    jitterlispvm_specialized_instruction_opcode_call__n9__retR,
    jitterlispvm_specialized_instruction_opcode_call__n10__retR,
    jitterlispvm_specialized_instruction_opcode_call__nR__retR,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n0__retR,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n1__retR,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n2__retR,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n3__retR,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n4__retR,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n5__retR,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n6__retR,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n7__retR,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n8__retR,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n9__retR,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n10__retR,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__nR__retR,
    jitterlispvm_specialized_instruction_opcode_call_mfrom_mc__retR,
    jitterlispvm_specialized_instruction_opcode_procedure_mprolog,
    jitterlispvm_specialized_instruction_opcode_return,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n0__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n1__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n2__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n3__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n4__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n5__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n6__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n7__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n8__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n9__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n10__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__nR__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n0__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n1__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n2__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n3__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n4__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n5__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n6__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n7__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n8__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n9__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n10__retR,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__nR__retR
  };

const jitter_uint
jitterlispvm_call_related_specialized_instruction_id_no
= sizeof (jitterlispvm_call_related_specialized_instruction_ids) / sizeof (jitter_uint);

const bool
jitterlispvm_specialized_instruction_call_relateds []
= {
    0, /* !INVALID */
    0, /* !BEGINBASICBLOCK */
    0, /* !EXITVM */
    0, /* !DATALOCATIONS */
    0, /* !NOP */
    0, /* !UNREACHABLE0 */
    0, /* !UNREACHABLE1 */
    0, /* !PRETENDTOJUMPANYWHERE */
    0, /* at-depth-to-register/n1/%rR */
    0, /* at-depth-to-register/n2/%rR */
    0, /* at-depth-to-register/n3/%rR */
    0, /* at-depth-to-register/n4/%rR */
    0, /* at-depth-to-register/n5/%rR */
    0, /* at-depth-to-register/n6/%rR */
    0, /* at-depth-to-register/n7/%rR */
    0, /* at-depth-to-register/n8/%rR */
    0, /* at-depth-to-register/n9/%rR */
    0, /* at-depth-to-register/n10/%rR */
    0, /* at-depth-to-register/nR/%rR */
    0, /* branch/fR */
    0, /* branch-if-false/fR */
    0, /* branch-if-non-positive/fR/fR */
    0, /* branch-if-not-less/fR/fR */
    0, /* branch-if-not-null/fR */
    0, /* branch-if-null/fR */
    0, /* branch-if-positive/fR/fR */
    0, /* branch-if-register-non-positive/%rR/fR/fR */
    0, /* branch-if-register-non-zero/%rR/fR/fR */
    0, /* branch-if-register-not-null/%rR/fR */
    0, /* branch-if-register-null/%rR/fR */
    0, /* branch-if-register-positive/%rR/fR/fR */
    0, /* branch-if-true/fR */
    1, /* call/n0/retR */
    1, /* call/n1/retR */
    1, /* call/n2/retR */
    1, /* call/n3/retR */
    1, /* call/n4/retR */
    1, /* call/n5/retR */
    1, /* call/n6/retR */
    1, /* call/n7/retR */
    1, /* call/n8/retR */
    1, /* call/n9/retR */
    1, /* call/n10/retR */
    1, /* call/nR/retR */
    1, /* call-compiled/n0/retR */
    1, /* call-compiled/n1/retR */
    1, /* call-compiled/n2/retR */
    1, /* call-compiled/n3/retR */
    1, /* call-compiled/n4/retR */
    1, /* call-compiled/n5/retR */
    1, /* call-compiled/n6/retR */
    1, /* call-compiled/n7/retR */
    1, /* call-compiled/n8/retR */
    1, /* call-compiled/n9/retR */
    1, /* call-compiled/n10/retR */
    1, /* call-compiled/nR/retR */
    1, /* call-from-c/retR */
    0, /* canonicalize-boolean */
    0, /* cdr-register/%rR/%rR/fR */
    0, /* check-closure/fR */
    0, /* check-global-defined/nR/fR */
    0, /* check-in-arity/n0/fR */
    0, /* check-in-arity/n1/fR */
    0, /* check-in-arity/n2/fR */
    0, /* check-in-arity/n3/fR */
    0, /* check-in-arity/n4/fR */
    0, /* check-in-arity/n5/fR */
    0, /* check-in-arity/n6/fR */
    0, /* check-in-arity/n7/fR */
    0, /* check-in-arity/n8/fR */
    0, /* check-in-arity/n9/fR */
    0, /* check-in-arity/n10/fR */
    0, /* check-in-arity/nR/fR */
    0, /* check-in-arity--alt/n0/fR */
    0, /* check-in-arity--alt/n1/fR */
    0, /* check-in-arity--alt/n2/fR */
    0, /* check-in-arity--alt/n3/fR */
    0, /* check-in-arity--alt/n4/fR */
    0, /* check-in-arity--alt/n5/fR */
    0, /* check-in-arity--alt/n6/fR */
    0, /* check-in-arity--alt/n7/fR */
    0, /* check-in-arity--alt/n8/fR */
    0, /* check-in-arity--alt/n9/fR */
    0, /* check-in-arity--alt/n10/fR */
    0, /* check-in-arity--alt/nR/fR */
    0, /* copy-from-literal/nR */
    0, /* copy-from-register/%rR */
    0, /* copy-to-register/%rR */
    0, /* drop */
    0, /* drop-nip */
    0, /* dup */
    0, /* exitvm */
    0, /* fail/retR */
    0, /* gc-if-needed/fR */
    0, /* heap-allocate/n4 */
    0, /* heap-allocate/n8 */
    0, /* heap-allocate/n12 */
    0, /* heap-allocate/n16 */
    0, /* heap-allocate/n24 */
    0, /* heap-allocate/n32 */
    0, /* heap-allocate/n36 */
    0, /* heap-allocate/n48 */
    0, /* heap-allocate/n52 */
    0, /* heap-allocate/n64 */
    0, /* heap-allocate/nR */
    0, /* literal-to-register/nR/%rR */
    0, /* nip */
    0, /* nip-drop */
    0, /* nip-five */
    0, /* nip-five-drop */
    0, /* nip-four */
    0, /* nip-four-drop */
    0, /* nip-push-literal/nR */
    0, /* nip-push-register/%rR */
    0, /* nip-six */
    0, /* nip-six-drop */
    0, /* nip-three */
    0, /* nip-three-drop */
    0, /* nip-two */
    0, /* nip-two-drop */
    0, /* nop */
    0, /* one-minus-register/%rR/%rR/fR */
    0, /* one-plus-register/%rR/%rR/fR */
    0, /* pop-to-global/nR/fR */
    0, /* pop-to-global-defined/nR/fR */
    0, /* pop-to-register/%rR */
    0, /* primitive/nR/n0/fR */
    0, /* primitive/nR/n1/fR */
    0, /* primitive/nR/n2/fR */
    0, /* primitive/nR/n3/fR */
    0, /* primitive/nR/n4/fR */
    0, /* primitive/nR/nR/fR */
    0, /* primitive-boolean-canonicalize */
    0, /* primitive-box */
    0, /* primitive-box-get/fR */
    0, /* primitive-box-setb-special/fR */
    0, /* primitive-car/fR */
    0, /* primitive-cdr/fR */
    0, /* primitive-characterp */
    0, /* primitive-cons-special */
    0, /* primitive-consp */
    0, /* primitive-eqp */
    0, /* primitive-fixnum-eqp/fR */
    0, /* primitive-fixnum-not-eqp/fR */
    0, /* primitive-fixnump */
    0, /* primitive-greaterp/fR */
    0, /* primitive-lessp/fR */
    0, /* primitive-negate/fR */
    0, /* primitive-negativep/fR */
    0, /* primitive-non-consp */
    0, /* primitive-non-negativep/fR */
    0, /* primitive-non-nullp */
    0, /* primitive-non-positivep/fR */
    0, /* primitive-non-symbolp */
    0, /* primitive-non-zerop/fR */
    0, /* primitive-not */
    0, /* primitive-not-eqp */
    0, /* primitive-not-greaterp/fR */
    0, /* primitive-not-lessp/fR */
    0, /* primitive-nothingp */
    0, /* primitive-nullp */
    0, /* primitive-one-minus/fR */
    0, /* primitive-one-plus/fR */
    0, /* primitive-positivep/fR */
    0, /* primitive-primordial-divided/fR */
    0, /* primitive-primordial-divided-unsafe/fR */
    0, /* primitive-primordial-minus/fR */
    0, /* primitive-primordial-plus/fR */
    0, /* primitive-primordial-times/fR */
    0, /* primitive-quotient/fR */
    0, /* primitive-quotient-unsafe/fR */
    0, /* primitive-remainder/fR */
    0, /* primitive-remainder-unsafe/fR */
    0, /* primitive-set-carb-special/fR */
    0, /* primitive-set-cdrb-special/fR */
    0, /* primitive-symbolp */
    0, /* primitive-two-divided/fR */
    0, /* primitive-two-quotient/fR */
    0, /* primitive-two-remainder/fR */
    0, /* primitive-two-times/fR */
    0, /* primitive-uniquep */
    0, /* primitive-zerop/fR */
    1, /* procedure-prolog */
    0, /* push-false */
    0, /* push-global/nR/fR */
    0, /* push-literal/nR */
    0, /* push-nil */
    0, /* push-nothing */
    0, /* push-one */
    0, /* push-register/%rR */
    0, /* push-unspecified */
    0, /* push-zero */
    0, /* register-to-register/%rR/%rR */
    0, /* restore-register/%rR */
    1, /* return */
    0, /* save-register/%rR */
    1, /* tail-call/n0/retR */
    1, /* tail-call/n1/retR */
    1, /* tail-call/n2/retR */
    1, /* tail-call/n3/retR */
    1, /* tail-call/n4/retR */
    1, /* tail-call/n5/retR */
    1, /* tail-call/n6/retR */
    1, /* tail-call/n7/retR */
    1, /* tail-call/n8/retR */
    1, /* tail-call/n9/retR */
    1, /* tail-call/n10/retR */
    1, /* tail-call/nR/retR */
    1, /* tail-call-compiled/n0/retR */
    1, /* tail-call-compiled/n1/retR */
    1, /* tail-call-compiled/n2/retR */
    1, /* tail-call-compiled/n3/retR */
    1, /* tail-call-compiled/n4/retR */
    1, /* tail-call-compiled/n5/retR */
    1, /* tail-call-compiled/n6/retR */
    1, /* tail-call-compiled/n7/retR */
    1, /* tail-call-compiled/n8/retR */
    1, /* tail-call-compiled/n9/retR */
    1, /* tail-call-compiled/n10/retR */
    1, /* tail-call-compiled/nR/retR */
    0, /* unreachable */
    0, /* !REPLACEMENT-branch/fR/retR */
    0, /* !REPLACEMENT-branch-if-false/fR/retR */
    0, /* !REPLACEMENT-branch-if-non-positive/fR/fR/retR */
    0, /* !REPLACEMENT-branch-if-not-less/fR/fR/retR */
    0, /* !REPLACEMENT-branch-if-not-null/fR/retR */
    0, /* !REPLACEMENT-branch-if-null/fR/retR */
    0, /* !REPLACEMENT-branch-if-positive/fR/fR/retR */
    0, /* !REPLACEMENT-branch-if-register-non-positive/%rR/fR/fR/retR */
    0, /* !REPLACEMENT-branch-if-register-non-zero/%rR/fR/fR/retR */
    0, /* !REPLACEMENT-branch-if-register-not-null/%rR/fR/retR */
    0, /* !REPLACEMENT-branch-if-register-null/%rR/fR/retR */
    0, /* !REPLACEMENT-branch-if-register-positive/%rR/fR/fR/retR */
    0, /* !REPLACEMENT-branch-if-true/fR/retR */
    0, /* !REPLACEMENT-call/n0/retR */
    0, /* !REPLACEMENT-call/n1/retR */
    0, /* !REPLACEMENT-call/n2/retR */
    0, /* !REPLACEMENT-call/n3/retR */
    0, /* !REPLACEMENT-call/n4/retR */
    0, /* !REPLACEMENT-call/n5/retR */
    0, /* !REPLACEMENT-call/n6/retR */
    0, /* !REPLACEMENT-call/n7/retR */
    0, /* !REPLACEMENT-call/n8/retR */
    0, /* !REPLACEMENT-call/n9/retR */
    0, /* !REPLACEMENT-call/n10/retR */
    0, /* !REPLACEMENT-call/nR/retR */
    0, /* !REPLACEMENT-call-compiled/n0/retR */
    0, /* !REPLACEMENT-call-compiled/n1/retR */
    0, /* !REPLACEMENT-call-compiled/n2/retR */
    0, /* !REPLACEMENT-call-compiled/n3/retR */
    0, /* !REPLACEMENT-call-compiled/n4/retR */
    0, /* !REPLACEMENT-call-compiled/n5/retR */
    0, /* !REPLACEMENT-call-compiled/n6/retR */
    0, /* !REPLACEMENT-call-compiled/n7/retR */
    0, /* !REPLACEMENT-call-compiled/n8/retR */
    0, /* !REPLACEMENT-call-compiled/n9/retR */
    0, /* !REPLACEMENT-call-compiled/n10/retR */
    0, /* !REPLACEMENT-call-compiled/nR/retR */
    0, /* !REPLACEMENT-call-from-c/retR */
    0, /* !REPLACEMENT-cdr-register/%rR/%rR/fR/retR */
    0, /* !REPLACEMENT-check-closure/fR/retR */
    0, /* !REPLACEMENT-check-global-defined/nR/fR/retR */
    0, /* !REPLACEMENT-check-in-arity/n0/fR/retR */
    0, /* !REPLACEMENT-check-in-arity/n1/fR/retR */
    0, /* !REPLACEMENT-check-in-arity/n2/fR/retR */
    0, /* !REPLACEMENT-check-in-arity/n3/fR/retR */
    0, /* !REPLACEMENT-check-in-arity/n4/fR/retR */
    0, /* !REPLACEMENT-check-in-arity/n5/fR/retR */
    0, /* !REPLACEMENT-check-in-arity/n6/fR/retR */
    0, /* !REPLACEMENT-check-in-arity/n7/fR/retR */
    0, /* !REPLACEMENT-check-in-arity/n8/fR/retR */
    0, /* !REPLACEMENT-check-in-arity/n9/fR/retR */
    0, /* !REPLACEMENT-check-in-arity/n10/fR/retR */
    0, /* !REPLACEMENT-check-in-arity/nR/fR/retR */
    0, /* !REPLACEMENT-check-in-arity--alt/n0/fR/retR */
    0, /* !REPLACEMENT-check-in-arity--alt/n1/fR/retR */
    0, /* !REPLACEMENT-check-in-arity--alt/n2/fR/retR */
    0, /* !REPLACEMENT-check-in-arity--alt/n3/fR/retR */
    0, /* !REPLACEMENT-check-in-arity--alt/n4/fR/retR */
    0, /* !REPLACEMENT-check-in-arity--alt/n5/fR/retR */
    0, /* !REPLACEMENT-check-in-arity--alt/n6/fR/retR */
    0, /* !REPLACEMENT-check-in-arity--alt/n7/fR/retR */
    0, /* !REPLACEMENT-check-in-arity--alt/n8/fR/retR */
    0, /* !REPLACEMENT-check-in-arity--alt/n9/fR/retR */
    0, /* !REPLACEMENT-check-in-arity--alt/n10/fR/retR */
    0, /* !REPLACEMENT-check-in-arity--alt/nR/fR/retR */
    0, /* !REPLACEMENT-exitvm/retR */
    0, /* !REPLACEMENT-gc-if-needed/fR/retR */
    0, /* !REPLACEMENT-one-minus-register/%rR/%rR/fR/retR */
    0, /* !REPLACEMENT-one-plus-register/%rR/%rR/fR/retR */
    0, /* !REPLACEMENT-pop-to-global/nR/fR/retR */
    0, /* !REPLACEMENT-pop-to-global-defined/nR/fR/retR */
    0, /* !REPLACEMENT-primitive/nR/n0/fR/retR */
    0, /* !REPLACEMENT-primitive/nR/n1/fR/retR */
    0, /* !REPLACEMENT-primitive/nR/n2/fR/retR */
    0, /* !REPLACEMENT-primitive/nR/n3/fR/retR */
    0, /* !REPLACEMENT-primitive/nR/n4/fR/retR */
    0, /* !REPLACEMENT-primitive/nR/nR/fR/retR */
    0, /* !REPLACEMENT-primitive-box-get/fR/retR */
    0, /* !REPLACEMENT-primitive-box-setb-special/fR/retR */
    0, /* !REPLACEMENT-primitive-car/fR/retR */
    0, /* !REPLACEMENT-primitive-cdr/fR/retR */
    0, /* !REPLACEMENT-primitive-fixnum-eqp/fR/retR */
    0, /* !REPLACEMENT-primitive-fixnum-not-eqp/fR/retR */
    0, /* !REPLACEMENT-primitive-greaterp/fR/retR */
    0, /* !REPLACEMENT-primitive-lessp/fR/retR */
    0, /* !REPLACEMENT-primitive-negate/fR/retR */
    0, /* !REPLACEMENT-primitive-negativep/fR/retR */
    0, /* !REPLACEMENT-primitive-non-negativep/fR/retR */
    0, /* !REPLACEMENT-primitive-non-positivep/fR/retR */
    0, /* !REPLACEMENT-primitive-non-zerop/fR/retR */
    0, /* !REPLACEMENT-primitive-not-greaterp/fR/retR */
    0, /* !REPLACEMENT-primitive-not-lessp/fR/retR */
    0, /* !REPLACEMENT-primitive-one-minus/fR/retR */
    0, /* !REPLACEMENT-primitive-one-plus/fR/retR */
    0, /* !REPLACEMENT-primitive-positivep/fR/retR */
    0, /* !REPLACEMENT-primitive-primordial-divided/fR/retR */
    0, /* !REPLACEMENT-primitive-primordial-divided-unsafe/fR/retR */
    0, /* !REPLACEMENT-primitive-primordial-minus/fR/retR */
    0, /* !REPLACEMENT-primitive-primordial-plus/fR/retR */
    0, /* !REPLACEMENT-primitive-primordial-times/fR/retR */
    0, /* !REPLACEMENT-primitive-quotient/fR/retR */
    0, /* !REPLACEMENT-primitive-quotient-unsafe/fR/retR */
    0, /* !REPLACEMENT-primitive-remainder/fR/retR */
    0, /* !REPLACEMENT-primitive-remainder-unsafe/fR/retR */
    0, /* !REPLACEMENT-primitive-set-carb-special/fR/retR */
    0, /* !REPLACEMENT-primitive-set-cdrb-special/fR/retR */
    0, /* !REPLACEMENT-primitive-two-divided/fR/retR */
    0, /* !REPLACEMENT-primitive-two-quotient/fR/retR */
    0, /* !REPLACEMENT-primitive-two-remainder/fR/retR */
    0, /* !REPLACEMENT-primitive-two-times/fR/retR */
    0, /* !REPLACEMENT-primitive-zerop/fR/retR */
    0, /* !REPLACEMENT-procedure-prolog/retR */
    0, /* !REPLACEMENT-push-global/nR/fR/retR */
    0, /* !REPLACEMENT-return/retR */
    0, /* !REPLACEMENT-tail-call/n0/retR */
    0, /* !REPLACEMENT-tail-call/n1/retR */
    0, /* !REPLACEMENT-tail-call/n2/retR */
    0, /* !REPLACEMENT-tail-call/n3/retR */
    0, /* !REPLACEMENT-tail-call/n4/retR */
    0, /* !REPLACEMENT-tail-call/n5/retR */
    0, /* !REPLACEMENT-tail-call/n6/retR */
    0, /* !REPLACEMENT-tail-call/n7/retR */
    0, /* !REPLACEMENT-tail-call/n8/retR */
    0, /* !REPLACEMENT-tail-call/n9/retR */
    0, /* !REPLACEMENT-tail-call/n10/retR */
    0, /* !REPLACEMENT-tail-call/nR/retR */
    0, /* !REPLACEMENT-tail-call-compiled/n0/retR */
    0, /* !REPLACEMENT-tail-call-compiled/n1/retR */
    0, /* !REPLACEMENT-tail-call-compiled/n2/retR */
    0, /* !REPLACEMENT-tail-call-compiled/n3/retR */
    0, /* !REPLACEMENT-tail-call-compiled/n4/retR */
    0, /* !REPLACEMENT-tail-call-compiled/n5/retR */
    0, /* !REPLACEMENT-tail-call-compiled/n6/retR */
    0, /* !REPLACEMENT-tail-call-compiled/n7/retR */
    0, /* !REPLACEMENT-tail-call-compiled/n8/retR */
    0, /* !REPLACEMENT-tail-call-compiled/n9/retR */
    0, /* !REPLACEMENT-tail-call-compiled/n10/retR */
    0, /* !REPLACEMENT-tail-call-compiled/nR/retR */
    0 /* !REPLACEMENT-unreachable/retR */
  };

#endif // #ifdef JITTER_HAVE_DEFECT_REPLACEMENT


void
jitterlispvm_rewrite (struct jitter_mutable_routine *jitter_mutable_routine_p)
{
  JITTTER_REWRITE_FUNCTION_PROLOG_;

/* User-specified code, rewriter part: beginning. */

/* User-specified code, rewriter part: end */


//asm volatile ("\n# checking pop-to-register-push-register");
//fprintf (stderr, "Trying rule 1 of 75, \"pop-to-register-push-register\" (line 1675)\n");
/* Rewrite rule "pop-to-register-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, pop_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule pop-to-register-push-register (line 1675) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule pop-to-register-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking pop-to-register-push-literal");
//fprintf (stderr, "Trying rule 2 of 75, \"pop-to-register-push-literal\" (line 1680)\n");
/* Rewrite rule "pop-to-register-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, pop_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule pop-to-register-push-literal (line 1680) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule pop-to-register-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-literal-pop-to-register");
//fprintf (stderr, "Trying rule 3 of 75, \"push-literal-pop-to-register\" (line 1686)\n");
/* Rewrite rule "push-literal-pop-to-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, pop_mto_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-literal-pop-to-register (line 1686) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction literal-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(literal_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of literal-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of literal-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule push-literal-pop-to-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop-push-register");
//fprintf (stderr, "Trying rule 4 of 75, \"drop-push-register\" (line 1692)\n");
/* Rewrite rule "drop-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop-push-register (line 1692) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule drop-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop-push-literal");
//fprintf (stderr, "Trying rule 5 of 75, \"drop-push-literal\" (line 1697)\n");
/* Rewrite rule "drop-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop-push-literal (line 1697) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule drop-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop-push-unspecified");
//fprintf (stderr, "Trying rule 6 of 75, \"drop-push-unspecified\" (line 1702)\n");
/* Rewrite rule "drop-push-unspecified" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_munspecified)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop-push-unspecified (line 1702) fires...\n");
    //fprintf (stderr, "  ...End of the rule drop-push-unspecified\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-register-pop-to-register");
//fprintf (stderr, "Trying rule 7 of 75, \"push-register-pop-to-register\" (line 1708)\n");
/* Rewrite rule "push-register-pop-to-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, pop_mto_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-register-pop-to-register (line 1708) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction register-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(register_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of register-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of register-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule push-register-pop-to-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-register-pop-to-register");
//fprintf (stderr, "Trying rule 8 of 75, \"copy-from-register-pop-to-register\" (line 1714)\n");
/* Rewrite rule "copy-from-register-pop-to-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, pop_mto_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-register-pop-to-register (line 1714) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction register-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(register_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of register-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of register-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop);
    //fprintf (stderr, "  ...End of the rule copy-from-register-pop-to-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-literal-pop-to-register");
//fprintf (stderr, "Trying rule 9 of 75, \"copy-from-literal-pop-to-register\" (line 1720)\n");
/* Rewrite rule "copy-from-literal-pop-to-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, pop_mto_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-literal-pop-to-register (line 1720) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction literal-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(literal_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of literal-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of literal-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop);
    //fprintf (stderr, "  ...End of the rule copy-from-literal-pop-to-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-register-push-register");
//fprintf (stderr, "Trying rule 10 of 75, \"push-register-push-register\" (line 1726)\n");
/* Rewrite rule "push-register-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-register-push-register (line 1726) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction push-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(push_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of push-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction dup\n");
    JITTER_RULE_APPEND_INSTRUCTION_(dup);
    //fprintf (stderr, "  ...End of the rule push-register-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-register-push-register");
//fprintf (stderr, "Trying rule 11 of 75, \"copy-from-register-push-register\" (line 1732)\n");
/* Rewrite rule "copy-from-register-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-register-push-register (line 1732) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction dup\n");
    JITTER_RULE_APPEND_INSTRUCTION_(dup);
    //fprintf (stderr, "  ...End of the rule copy-from-register-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-to-register-and-push-the-same-register");
//fprintf (stderr, "Trying rule 12 of 75, \"copy-to-register-and-push-the-same-register\" (line 1738)\n");
/* Rewrite rule "copy-to-register-and-push-the-same-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-to-register-and-push-the-same-register (line 1738) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction dup\n");
    JITTER_RULE_APPEND_INSTRUCTION_(dup);
    //fprintf (stderr, "  ...End of the rule copy-to-register-and-push-the-same-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-to-and-from-the-same-register");
//fprintf (stderr, "Trying rule 13 of 75, \"copy-to-and-from-the-same-register\" (line 1744)\n");
/* Rewrite rule "copy-to-and-from-the-same-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, copy_mfrom_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-to-and-from-the-same-register (line 1744) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule copy-to-and-from-the-same-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-the-same-literal-twice");
//fprintf (stderr, "Trying rule 14 of 75, \"push-the-same-literal-twice\" (line 1750)\n");
/* Rewrite rule "push-the-same-literal-twice" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-the-same-literal-twice (line 1750) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction push-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(push_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of push-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction dup\n");
    JITTER_RULE_APPEND_INSTRUCTION_(dup);
    //fprintf (stderr, "  ...End of the rule push-the-same-literal-twice\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-literal-and-push-the-same-literal");
//fprintf (stderr, "Trying rule 15 of 75, \"copy-from-literal-and-push-the-same-literal\" (line 1756)\n");
/* Rewrite rule "copy-from-literal-and-push-the-same-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-literal-and-push-the-same-literal (line 1756) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction dup\n");
    JITTER_RULE_APPEND_INSTRUCTION_(dup);
    //fprintf (stderr, "  ...End of the rule copy-from-literal-and-push-the-same-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-literal-then-from-another-literal");
//fprintf (stderr, "Trying rule 16 of 75, \"copy-from-literal-then-from-another-literal\" (line 1764)\n");
/* Rewrite rule "copy-from-literal-then-from-another-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, copy_mfrom_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-literal-then-from-another-literal (line 1764) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule copy-from-literal-then-from-another-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-literal-then-from-a-register");
//fprintf (stderr, "Trying rule 17 of 75, \"copy-from-literal-then-from-a-register\" (line 1769)\n");
/* Rewrite rule "copy-from-literal-then-from-a-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, copy_mfrom_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-literal-then-from-a-register (line 1769) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule copy-from-literal-then-from-a-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-a-register-then-from-literal");
//fprintf (stderr, "Trying rule 18 of 75, \"copy-from-a-register-then-from-literal\" (line 1774)\n");
/* Rewrite rule "copy-from-a-register-then-from-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, copy_mfrom_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-a-register-then-from-literal (line 1774) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule copy-from-a-register-then-from-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-from-a-register-then-from-another-register");
//fprintf (stderr, "Trying rule 19 of 75, \"copy-from-a-register-then-from-another-register\" (line 1779)\n");
/* Rewrite rule "copy-from-a-register-then-from-another-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, copy_mfrom_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-from-a-register-then-from-another-register (line 1779) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule copy-from-a-register-then-from-another-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking useless-copy-from-literal-elimination");
//fprintf (stderr, "Trying rule 20 of 75, \"useless-copy-from-literal-elimination\" (line 1786)\n");
/* Rewrite rule "useless-copy-from-literal-elimination" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule useless-copy-from-literal-elimination (line 1786) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop);
    //fprintf (stderr, "  ...End of the rule useless-copy-from-literal-elimination\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking useless-copy-from-register-elimination");
//fprintf (stderr, "Trying rule 21 of 75, \"useless-copy-from-register-elimination\" (line 1791)\n");
/* Rewrite rule "useless-copy-from-register-elimination" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule useless-copy-from-register-elimination (line 1791) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop);
    //fprintf (stderr, "  ...End of the rule useless-copy-from-register-elimination\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-register-drop");
//fprintf (stderr, "Trying rule 22 of 75, \"push-register-drop\" (line 1797)\n");
/* Rewrite rule "push-register-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-register-drop (line 1797) fires...\n");
    //fprintf (stderr, "  ...End of the rule push-register-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-literal-drop");
//fprintf (stderr, "Trying rule 23 of 75, \"push-literal-drop\" (line 1802)\n");
/* Rewrite rule "push-literal-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-literal-drop (line 1802) fires...\n");
    //fprintf (stderr, "  ...End of the rule push-literal-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking pop-to-register-copy-from-register");
//fprintf (stderr, "Trying rule 24 of 75, \"pop-to-register-copy-from-register\" (line 1808)\n");
/* Rewrite rule "pop-to-register-copy-from-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, pop_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, copy_mfrom_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule pop-to-register-copy-from-register (line 1808) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "  ...End of the rule pop-to-register-copy-from-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking pop-to-register-drop");
//fprintf (stderr, "Trying rule 25 of 75, \"pop-to-register-drop\" (line 1813)\n");
/* Rewrite rule "pop-to-register-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, pop_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule pop-to-register-drop (line 1813) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "    rewrite: adding instruction pop-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(pop_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of pop-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule pop-to-register-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking pop-to-register-nip");
//fprintf (stderr, "Trying rule 26 of 75, \"pop-to-register-nip\" (line 1819)\n");
/* Rewrite rule "pop-to-register-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, pop_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule pop-to-register-nip (line 1819) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction copy-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    rewrite: adding instruction drop-nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop_mnip);
    //fprintf (stderr, "  ...End of the rule pop-to-register-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop-nip");
//fprintf (stderr, "Trying rule 27 of 75, \"drop-nip\" (line 1825)\n");
/* Rewrite rule "drop-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop-nip (line 1825) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction drop-nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop_mnip);
    //fprintf (stderr, "  ...End of the rule drop-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-drop");
//fprintf (stderr, "Trying rule 28 of 75, \"nip-drop\" (line 1831)\n");
/* Rewrite rule "nip-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-drop (line 1831) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mdrop);
    //fprintf (stderr, "  ...End of the rule nip-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop-drop");
//fprintf (stderr, "Trying rule 29 of 75, \"drop-drop\" (line 1837)\n");
/* Rewrite rule "drop-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop-drop (line 1837) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mdrop);
    //fprintf (stderr, "  ...End of the rule drop-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-drop-drop");
//fprintf (stderr, "Trying rule 30 of 75, \"nip-drop-drop\" (line 1843)\n");
/* Rewrite rule "nip-drop-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-drop-drop (line 1843) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-two-drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mtwo_mdrop);
    //fprintf (stderr, "  ...End of the rule nip-drop-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-two-drop-drop");
//fprintf (stderr, "Trying rule 31 of 75, \"nip-two-drop-drop\" (line 1848)\n");
/* Rewrite rule "nip-two-drop-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mtwo_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-two-drop-drop (line 1848) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-three-drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mthree_mdrop);
    //fprintf (stderr, "  ...End of the rule nip-two-drop-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-three-drop-drop");
//fprintf (stderr, "Trying rule 32 of 75, \"nip-three-drop-drop\" (line 1853)\n");
/* Rewrite rule "nip-three-drop-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mthree_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-three-drop-drop (line 1853) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-four-drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfour_mdrop);
    //fprintf (stderr, "  ...End of the rule nip-three-drop-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-four-drop-drop");
//fprintf (stderr, "Trying rule 33 of 75, \"nip-four-drop-drop\" (line 1858)\n");
/* Rewrite rule "nip-four-drop-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfour_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-four-drop-drop (line 1858) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-five-drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfive_mdrop);
    //fprintf (stderr, "  ...End of the rule nip-four-drop-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-five-drop-drop");
//fprintf (stderr, "Trying rule 34 of 75, \"nip-five-drop-drop\" (line 1863)\n");
/* Rewrite rule "nip-five-drop-drop" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfive_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-five-drop-drop (line 1863) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-six-drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_msix_mdrop);
    //fprintf (stderr, "  ...End of the rule nip-five-drop-drop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-nip");
//fprintf (stderr, "Trying rule 35 of 75, \"nip-nip\" (line 1872)\n");
/* Rewrite rule "nip-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-nip (line 1872) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-two\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mtwo);
    //fprintf (stderr, "  ...End of the rule nip-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-two-nip");
//fprintf (stderr, "Trying rule 36 of 75, \"nip-two-nip\" (line 1877)\n");
/* Rewrite rule "nip-two-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mtwo)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-two-nip (line 1877) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-three\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mthree);
    //fprintf (stderr, "  ...End of the rule nip-two-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-three-nip");
//fprintf (stderr, "Trying rule 37 of 75, \"nip-three-nip\" (line 1882)\n");
/* Rewrite rule "nip-three-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mthree)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-three-nip (line 1882) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-four\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfour);
    //fprintf (stderr, "  ...End of the rule nip-three-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-four-nip");
//fprintf (stderr, "Trying rule 38 of 75, \"nip-four-nip\" (line 1887)\n");
/* Rewrite rule "nip-four-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfour)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-four-nip (line 1887) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-five\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfive);
    //fprintf (stderr, "  ...End of the rule nip-four-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-five-nip");
//fprintf (stderr, "Trying rule 39 of 75, \"nip-five-nip\" (line 1892)\n");
/* Rewrite rule "nip-five-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfive)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-five-nip (line 1892) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-six\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_msix);
    //fprintf (stderr, "  ...End of the rule nip-five-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-before-copy-from-register");
//fprintf (stderr, "Trying rule 40 of 75, \"nip-before-copy-from-register\" (line 1900)\n");
/* Rewrite rule "nip-before-copy-from-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-before-copy-from-register (line 1900) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-before-copy-from-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-before-copy-from-literal");
//fprintf (stderr, "Trying rule 41 of 75, \"nip-before-copy-from-literal\" (line 1905)\n");
/* Rewrite rule "nip-before-copy-from-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mfrom_mliteral)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-before-copy-from-literal (line 1905) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-before-copy-from-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-to-register-nip");
//fprintf (stderr, "Trying rule 42 of 75, \"copy-to-register-nip\" (line 1914)\n");
/* Rewrite rule "copy-to-register-nip" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-to-register-nip (line 1914) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "    rewrite: adding instruction copy-to-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mto_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-to-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule copy-to-register-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking pop-to-register-return");
//fprintf (stderr, "Trying rule 43 of 75, \"pop-to-register-return\" (line 1920)\n");
/* Rewrite rule "pop-to-register-return" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, pop_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, return)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule pop-to-register-return (line 1920) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop);
    //fprintf (stderr, "    rewrite: adding instruction return\n");
    JITTER_RULE_APPEND_INSTRUCTION_(return);
    //fprintf (stderr, "  ...End of the rule pop-to-register-return\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking copy-to-register-return");
//fprintf (stderr, "Trying rule 44 of 75, \"copy-to-register-return\" (line 1926)\n");
/* Rewrite rule "copy-to-register-return" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, copy_mto_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, return)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule copy-to-register-return (line 1926) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction return\n");
    JITTER_RULE_APPEND_INSTRUCTION_(return);
    //fprintf (stderr, "  ...End of the rule copy-to-register-return\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking not-branch-if-true");
//fprintf (stderr, "Trying rule 45 of 75, \"not-branch-if-true\" (line 1939)\n");
/* Rewrite rule "not-branch-if-true" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, primitive_mnot)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mtrue)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule not-branch-if-true (line 1939) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-false\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mfalse);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-false\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule not-branch-if-true\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nullp-branch-if-true");
//fprintf (stderr, "Trying rule 46 of 75, \"nullp-branch-if-true\" (line 1945)\n");
/* Rewrite rule "nullp-branch-if-true" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, primitive_mnullp)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mtrue)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nullp-branch-if-true (line 1945) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-null\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mnull);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-null\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nullp-branch-if-true\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nullp-branch-if-false");
//fprintf (stderr, "Trying rule 47 of 75, \"nullp-branch-if-false\" (line 1950)\n");
/* Rewrite rule "nullp-branch-if-false" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, primitive_mnullp)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mfalse)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nullp-branch-if-false (line 1950) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-not-null\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mnot_mnull);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-not-null\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nullp-branch-if-false\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking non-nullp-branch-if-true");
//fprintf (stderr, "Trying rule 48 of 75, \"non-nullp-branch-if-true\" (line 1956)\n");
/* Rewrite rule "non-nullp-branch-if-true" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, primitive_mnon_mnullp)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mtrue)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule non-nullp-branch-if-true (line 1956) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-not-null\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mnot_mnull);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-not-null\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule non-nullp-branch-if-true\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking non-nullp-branch-if-false");
//fprintf (stderr, "Trying rule 49 of 75, \"non-nullp-branch-if-false\" (line 1961)\n");
/* Rewrite rule "non-nullp-branch-if-false" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, primitive_mnon_mnullp)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mfalse)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule non-nullp-branch-if-false (line 1961) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-null\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mnull);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-null\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule non-nullp-branch-if-false\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking positivep-branch-if-true");
//fprintf (stderr, "Trying rule 50 of 75, \"positivep-branch-if-true\" (line 1967)\n");
/* Rewrite rule "positivep-branch-if-true" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, primitive_mpositivep)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mtrue)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule positivep-branch-if-true (line 1967) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-positive\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mpositive);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-positive\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of branch-if-positive\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule positivep-branch-if-true\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking positivep-branch-if-false");
//fprintf (stderr, "Trying rule 51 of 75, \"positivep-branch-if-false\" (line 1972)\n");
/* Rewrite rule "positivep-branch-if-false" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, primitive_mpositivep)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mfalse)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule positivep-branch-if-false (line 1972) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-non-positive\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mnon_mpositive);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-non-positive\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of branch-if-non-positive\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule positivep-branch-if-false\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-register-branch-if-positive");
//fprintf (stderr, "Trying rule 52 of 75, \"push-register-branch-if-positive\" (line 1978)\n");
/* Rewrite rule "push-register-branch-if-positive" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
    JITTER_RULE_DECLARE_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mpositive)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 1, c)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
    JITTER_RULE_CLONE_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-register-branch-if-positive (line 1978) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-register-positive\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mregister_mpositive);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-register-positive\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of branch-if-register-positive\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "    instantiating the 2-th argument of branch-if-register-positive\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(c)
                                 );
    //fprintf (stderr, "  ...End of the rule push-register-branch-if-positive\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
    JITTER_RULE_DESTROY_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-register-branch-if-non-positive");
//fprintf (stderr, "Trying rule 53 of 75, \"push-register-branch-if-non-positive\" (line 1983)\n");
/* Rewrite rule "push-register-branch-if-non-positive" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
    JITTER_RULE_DECLARE_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mnon_mpositive)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 1, c)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
    JITTER_RULE_CLONE_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-register-branch-if-non-positive (line 1983) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-register-non-positive\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mregister_mnon_mpositive);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-register-non-positive\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of branch-if-register-non-positive\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "    instantiating the 2-th argument of branch-if-register-non-positive\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(c)
                                 );
    //fprintf (stderr, "  ...End of the rule push-register-branch-if-non-positive\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
    JITTER_RULE_DESTROY_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-register-branch-if-null");
//fprintf (stderr, "Trying rule 54 of 75, \"push-register-branch-if-null\" (line 1989)\n");
/* Rewrite rule "push-register-branch-if-null" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mnull)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-register-branch-if-null (line 1989) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-register-null\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mregister_mnull);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-register-null\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of branch-if-register-null\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule push-register-branch-if-null\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-register-branch-if-non-null");
//fprintf (stderr, "Trying rule 55 of 75, \"push-register-branch-if-non-null\" (line 1994)\n");
/* Rewrite rule "push-register-branch-if-non-null" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, branch_mif_mnot_mnull)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-register-branch-if-non-null (line 1994) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-register-not-null\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mregister_mnot_mnull);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-register-not-null\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of branch-if-register-not-null\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule push-register-branch-if-non-null\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-register-one-minus-pop-to-register");
//fprintf (stderr, "Trying rule 56 of 75, \"push-register-one-minus-pop-to-register\" (line 2003)\n");
/* Rewrite rule "push-register-one-minus-pop-to-register" */
JITTER_RULE_BEGIN(3)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
    JITTER_RULE_DECLARE_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, primitive_mone_mminus)
    JITTER_RULE_CONDITION_MATCH_OPCODE(2, pop_mto_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(2, 0, c)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
    JITTER_RULE_CLONE_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-register-one-minus-pop-to-register (line 2003) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction one-minus-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(one_mminus_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of one-minus-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of one-minus-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(c)
                                 );
    //fprintf (stderr, "    instantiating the 2-th argument of one-minus-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule push-register-one-minus-pop-to-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
    JITTER_RULE_DESTROY_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-register-one-plus-pop-to-register");
//fprintf (stderr, "Trying rule 57 of 75, \"push-register-one-plus-pop-to-register\" (line 2008)\n");
/* Rewrite rule "push-register-one-plus-pop-to-register" */
JITTER_RULE_BEGIN(3)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
    JITTER_RULE_DECLARE_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, primitive_mone_mplus)
    JITTER_RULE_CONDITION_MATCH_OPCODE(2, pop_mto_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(2, 0, c)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
    JITTER_RULE_CLONE_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-register-one-plus-pop-to-register (line 2008) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction one-plus-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(one_mplus_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of one-plus-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of one-plus-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(c)
                                 );
    //fprintf (stderr, "    instantiating the 2-th argument of one-plus-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule push-register-one-plus-pop-to-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
    JITTER_RULE_DESTROY_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-register-cdr-pop-to-register");
//fprintf (stderr, "Trying rule 58 of 75, \"push-register-cdr-pop-to-register\" (line 2013)\n");
/* Rewrite rule "push-register-cdr-pop-to-register" */
JITTER_RULE_BEGIN(3)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
    JITTER_RULE_DECLARE_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, primitive_mcdr)
    JITTER_RULE_CONDITION_MATCH_OPCODE(2, pop_mto_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(2, 0, c)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
    JITTER_RULE_CLONE_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-register-cdr-pop-to-register (line 2013) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction cdr-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(cdr_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of cdr-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of cdr-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(c)
                                 );
    //fprintf (stderr, "    instantiating the 2-th argument of cdr-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule push-register-cdr-pop-to-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
    JITTER_RULE_DESTROY_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking restore-register-then-save-the-same-register");
//fprintf (stderr, "Trying rule 59 of 75, \"restore-register-then-save-the-same-register\" (line 2033)\n");
/* Rewrite rule "restore-register-then-save-the-same-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, restore_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, save_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule restore-register-then-save-the-same-register (line 2033) fires...\n");
    //fprintf (stderr, "  ...End of the rule restore-register-then-save-the-same-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking scratch");
//fprintf (stderr, "Trying rule 60 of 75, \"scratch\" (line 2069)\n");
/* Rewrite rule "scratch" */
JITTER_RULE_BEGIN(3)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(b);
    JITTER_RULE_DECLARE_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push_mregister)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, primitive_mnon_mzerop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(2, branch_mif_mtrue)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, b)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(2, 0, c)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(b);
    JITTER_RULE_CLONE_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule scratch (line 2069) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction branch-if-register-non-zero\n");
    JITTER_RULE_APPEND_INSTRUCTION_(branch_mif_mregister_mnon_mzero);
    //fprintf (stderr, "    instantiating the 0-th argument of branch-if-register-non-zero\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "    instantiating the 1-th argument of branch-if-register-non-zero\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(c)
                                 );
    //fprintf (stderr, "    instantiating the 2-th argument of branch-if-register-non-zero\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(b)
                                 );
    //fprintf (stderr, "  ...End of the rule scratch\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(b);
    JITTER_RULE_DESTROY_PLACEHOLDER_(c);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-drop-push-literal");
//fprintf (stderr, "Trying rule 61 of 75, \"nip-drop-push-literal\" (line 2092)\n");
/* Rewrite rule "nip-drop-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-drop-push-literal (line 2092) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-drop-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-drop-push-register");
//fprintf (stderr, "Trying rule 62 of 75, \"nip-drop-push-register\" (line 2097)\n");
/* Rewrite rule "nip-drop-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-drop-push-register (line 2097) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-drop-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-two-drop-push-literal");
//fprintf (stderr, "Trying rule 63 of 75, \"nip-two-drop-push-literal\" (line 2102)\n");
/* Rewrite rule "nip-two-drop-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mtwo_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-two-drop-push-literal (line 2102) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-two\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mtwo);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-two-drop-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-two-drop-push-register");
//fprintf (stderr, "Trying rule 64 of 75, \"nip-two-drop-push-register\" (line 2107)\n");
/* Rewrite rule "nip-two-drop-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mtwo_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-two-drop-push-register (line 2107) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-two\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mtwo);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-two-drop-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-three-drop-push-literal");
//fprintf (stderr, "Trying rule 65 of 75, \"nip-three-drop-push-literal\" (line 2112)\n");
/* Rewrite rule "nip-three-drop-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mthree_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-three-drop-push-literal (line 2112) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-three\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mthree);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-three-drop-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-three-drop-push-register");
//fprintf (stderr, "Trying rule 66 of 75, \"nip-three-drop-push-register\" (line 2117)\n");
/* Rewrite rule "nip-three-drop-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mthree_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-three-drop-push-register (line 2117) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-three\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mthree);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-three-drop-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-four-drop-push-literal");
//fprintf (stderr, "Trying rule 67 of 75, \"nip-four-drop-push-literal\" (line 2122)\n");
/* Rewrite rule "nip-four-drop-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfour_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-four-drop-push-literal (line 2122) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-four\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfour);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-four-drop-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-four-drop-push-register");
//fprintf (stderr, "Trying rule 68 of 75, \"nip-four-drop-push-register\" (line 2127)\n");
/* Rewrite rule "nip-four-drop-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfour_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-four-drop-push-register (line 2127) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-four\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfour);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-four-drop-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-five-drop-push-literal");
//fprintf (stderr, "Trying rule 69 of 75, \"nip-five-drop-push-literal\" (line 2132)\n");
/* Rewrite rule "nip-five-drop-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfive_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-five-drop-push-literal (line 2132) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-five\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfive);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-five-drop-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-five-drop-push-register");
//fprintf (stderr, "Trying rule 70 of 75, \"nip-five-drop-push-register\" (line 2137)\n");
/* Rewrite rule "nip-five-drop-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_mfive_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-five-drop-push-register (line 2137) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-five\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mfive);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-five-drop-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-six-drop-push-literal");
//fprintf (stderr, "Trying rule 71 of 75, \"nip-six-drop-push-literal\" (line 2142)\n");
/* Rewrite rule "nip-six-drop-push-literal" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_msix_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-six-drop-push-literal (line 2142) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-six\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_msix);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-six-drop-push-literal\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-six-drop-push-register");
//fprintf (stderr, "Trying rule 72 of 75, \"nip-six-drop-push-register\" (line 2147)\n");
/* Rewrite rule "nip-six-drop-push-register" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip_msix_mdrop)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-six-drop-push-register (line 2147) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-six\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_msix);
    //fprintf (stderr, "    rewrite: adding instruction copy-from-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(copy_mfrom_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of copy-from-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule nip-six-drop-push-register\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking unnamed");
//fprintf (stderr, "Trying rule 73 of 75, \"unnamed\" (line 2169)\n");
/* Rewrite rule "unnamed" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mliteral)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule unnamed (line 2169) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-push-literal\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mpush_mliteral);
    //fprintf (stderr, "    instantiating the 0-th argument of nip-push-literal\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule unnamed\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking unnamed");
//fprintf (stderr, "Trying rule 74 of 75, \"unnamed\" (line 2174)\n");
/* Rewrite rule "unnamed" */
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, push_mregister)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule unnamed (line 2174) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction nip-push-register\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip_mpush_mregister);
    //fprintf (stderr, "    instantiating the 0-th argument of nip-push-register\n");
    jitter_mutable_routine_append_parameter_copy (jitter_mutable_routine_p,
      JITTER_PLACEHOLDER_NAME(a)
                                 );
    //fprintf (stderr, "  ...End of the rule unnamed\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking useless-cons-elimination");
//fprintf (stderr, "Trying rule 75 of 75, \"useless-cons-elimination\" (line 2185)\n");
/* Rewrite rule "useless-cons-elimination" */
JITTER_RULE_BEGIN(4)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
    JITTER_RULE_DECLARE_PLACEHOLDER_(f);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, heap_mallocate)
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, gc_mif_mneeded)
    JITTER_RULE_CONDITION_MATCH_OPCODE(2, primitive_mcons_mspecial)
    JITTER_RULE_CONDITION_MATCH_OPCODE(3, nip_mdrop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(1, 0, f)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
    JITTER_RULE_CLONE_PLACEHOLDER_(f);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule useless-cons-elimination (line 2185) fires...\n");
    //fprintf (stderr, "    rewrite: adding instruction drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop);
    //fprintf (stderr, "    rewrite: adding instruction drop\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop);
    //fprintf (stderr, "  ...End of the rule useless-cons-elimination\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
    JITTER_RULE_DESTROY_PLACEHOLDER_(f);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//fprintf (stderr, "No more rules to try\n");
}


//#include <jitter/jitter-fatal.h>

//#include <jitter/jitter.h>
//#include <jitter/jitter-instruction.h>
//#include <jitter/jitter-specialize.h>

//#include "jitterlispvm-vm.h"
//#include "jitterlispvm-meta-instructions.h"
//#include "jitterlispvm-specialized-instructions.h"


/* Recognizer function prototypes. */
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n3___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n4___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n5___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n6___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n7___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n8___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n9___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n10___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mfalse (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mfalse__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnon_mpositive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnon_mpositive__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnon_mpositive__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mnull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mnull__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnull__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mpositive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mpositive__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mpositive__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mpositive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mpositive___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mpositive___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mpositive___rrR__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnot_mnull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnot_mnull___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnot_mnull___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnull___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnull___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mpositive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mpositive___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mpositive___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mpositive___rrR__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mtrue (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mtrue__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mfrom_mc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_canonicalize_mboolean (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_cdr_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_cdr_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_cdr_mregister___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_cdr_mregister___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mclosure (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mclosure__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n3__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n4__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n5__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n6__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n7__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n8__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n9__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n10__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n3__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n4__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n5__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n6__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n7__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n8__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n9__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n10__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mliteral (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mliteral__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mto_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_drop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_drop_mnip (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_dup (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_exitvm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_fail (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_gc_mif_mneeded (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_gc_mif_mneeded__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n12 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n16 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n24 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n32 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n36 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n48 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n52 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n64 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_literal_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_literal_mto_mregister__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_literal_mto_mregister__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfive_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfour (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfour_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mliteral (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mliteral__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_msix (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_msix_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mthree (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mthree_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mtwo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mtwo_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mminus_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mminus_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mminus_mregister___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mminus_mregister___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mplus_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mplus_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mplus_mregister___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mplus_mregister___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n3__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n4__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mboolean_mcanonicalize (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_mget (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_mget__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_msetb_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_msetb_mspecial__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcar__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcdr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcdr__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcharacterp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcons_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mconsp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_meqp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_mnot_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_mnot_meqp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnump (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mgreaterp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mgreaterp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mlessp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mlessp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegate (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegate__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegativep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegativep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mconsp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnegativep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnegativep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnullp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mpositivep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mpositivep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_msymbolp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mzerop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mzerop__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mgreaterp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mgreaterp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mlessp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mlessp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnothingp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnullp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mminus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mminus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mplus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mplus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mpositivep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mpositivep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided_munsafe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided_munsafe__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mminus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mminus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mplus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mplus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mtimes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mtimes__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient_munsafe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient_munsafe__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder_munsafe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder_munsafe__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcarb_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcarb_mspecial__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcdrb_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcdrb_mspecial__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_msymbolp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mdivided (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mdivided__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mquotient (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mquotient__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mremainder (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mremainder__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mtimes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mtimes__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_muniquep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mzerop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mzerop__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_procedure_mprolog (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mfalse (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mglobal (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mglobal__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mglobal__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mliteral (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mliteral__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mnil (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mnothing (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mone (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_munspecified (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mzero (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_register_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_register_mto_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_register_mto_mregister___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_restore_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_restore_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_return (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_save_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_save_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_unreachable (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));


/* Recognizer function definitions. */
inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 6 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n6 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 7 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n7 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 9 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n9 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 10 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n10 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n1___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n1___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n1___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n2___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n2___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n2___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n3___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n3___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n3___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n4___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n4___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n4___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n5___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n5___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n5___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n6___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n6___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n6___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n7___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n7___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n7___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n8___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n8___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n8___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n9___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n9___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n9___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n10___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__n10___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n10___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__nR___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mfalse (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mfalse__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mfalse__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mfalse__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnon_mpositive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mnon_mpositive__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnon_mpositive__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mnon_mpositive__fR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnon_mpositive__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mnon_mpositive__fR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless__fR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mnot_mless__fR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mnull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mnull__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mnull__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mnot_mnull__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mnull__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mnull__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mnull__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mpositive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mpositive__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mpositive__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mpositive__fR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mpositive__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mpositive__fR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mpositive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mpositive___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mpositive___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mpositive___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mpositive___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mpositive___rrR__fR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mpositive___rrR__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mnon_mpositive___rrR__fR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR__fR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero___rrR__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mnon_mzero___rrR__fR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnot_mnull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnot_mnull___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnot_mnull___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnot_mnull___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnot_mnull___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mnot_mnull___rrR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnull___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnull___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnull___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnull___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mnull___rrR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mpositive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mpositive___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mpositive___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mpositive___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mpositive___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mpositive___rrR__fR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mpositive___rrR__fR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mpositive___rrR__fR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mtrue (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_branch_mif_mtrue__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_branch_mif_mtrue__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_branch_mif_mtrue__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 6 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n6 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 7 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n7 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 9 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n9 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 10 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call__n10 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_call__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n0__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n1__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n2__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n3__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n4__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n5__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n6__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n7__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n8__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n9__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__n10__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call__nR__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 6 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n6 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 7 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n7 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 9 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n9 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 10 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__n10 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_call_mcompiled__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n0__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n1__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n2__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n3__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n4__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n5__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n6__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n7__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n8__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n9__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__n10__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mcompiled__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mcompiled__nR__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_call_mfrom_mc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_call_mfrom_mc__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_canonicalize_mboolean (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_canonicalize_mboolean;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_cdr_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_cdr_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_cdr_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_cdr_mregister___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_cdr_mregister___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_cdr_mregister___rrR___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_cdr_mregister___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_cdr_mregister___rrR___rrR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mclosure (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_mclosure__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mclosure__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_mclosure__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_mglobal_mdefined__nR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 6 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n6 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 7 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n7 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 9 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n9 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 10 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n10 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n0__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n1__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n1__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n2__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n2__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n3__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n3__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n3__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n4__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n4__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n4__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n5__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n5__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n5__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n6__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n6__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n6__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n7__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n7__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n7__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n8__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n8__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n8__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n9__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n9__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n9__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__n10__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__n10__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__n10__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity__nR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 6 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n6 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 7 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n7 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 9 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n9 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 10 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n10 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n0__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n1__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n1__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n2__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n2__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n3__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n3__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n3__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n4__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n4__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n4__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n5__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n5__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n5__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n6__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n6__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n6__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n7__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n7__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n7__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n8__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n8__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n8__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n9__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n9__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n9__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n10__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__n10__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n10__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__nR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mliteral (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_copy_mfrom_mliteral__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mliteral__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_copy_mfrom_mliteral__nR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_copy_mfrom_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mfrom_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_copy_mfrom_mregister___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_copy_mto_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_copy_mto_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_copy_mto_mregister___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_drop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_drop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_drop_mnip (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_drop_mnip;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_dup (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_dup;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_exitvm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_exitvm;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_fail (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_fail__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_gc_mif_mneeded (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_gc_mif_mneeded__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_gc_mif_mneeded__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_gc_mif_mneeded__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 12 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n12 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 16 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n16 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 24 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n24 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 32 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n32 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 36 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n36 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 48 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n48 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 52 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n52 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 64 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__n64 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_heap_mallocate__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n4;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n8;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n12 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n12;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n16 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n16;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n24 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n24;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n32 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n32;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n36 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n36;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n48 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n48;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n52 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n52;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__n64 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__n64;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_heap_mallocate__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_heap_mallocate__nR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_literal_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_literal_mto_mregister__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_literal_mto_mregister__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_literal_mto_mregister__nR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_literal_mto_mregister__nR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_literal_mto_mregister__nR___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mdrop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mfive;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfive_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mfive_mdrop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfour (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mfour;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mfour_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mfour_mdrop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mliteral (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_nip_mpush_mliteral__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mliteral__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mpush_mliteral__nR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_nip_mpush_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mpush_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mpush_mregister___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_msix (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_msix;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_msix_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_msix_mdrop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mthree (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mthree;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mthree_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mthree_mdrop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mtwo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mtwo;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nip_mtwo_mdrop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nip_mtwo_mdrop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_nop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_nop;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mminus_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_one_mminus_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mminus_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_one_mminus_mregister___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mminus_mregister___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_one_mminus_mregister___rrR___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mminus_mregister___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_one_mminus_mregister___rrR___rrR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mplus_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_one_mplus_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mplus_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_one_mplus_mregister___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mplus_mregister___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_one_mplus_mregister___rrR___rrR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_one_mplus_mregister___rrR___rrR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_one_mplus_mregister___rrR___rrR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_pop_mto_mglobal__nR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_pop_mto_mglobal_mdefined__nR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_pop_mto_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_pop_mto_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_pop_mto_mregister___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n0__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n0__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive__nR__n0__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n1__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n1__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive__nR__n1__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n2__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n2__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive__nR__n2__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n3__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n3__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive__nR__n3__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__n4__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__n4__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive__nR__n4__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive__nR__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive__nR__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive__nR__nR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mboolean_mcanonicalize (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mboolean_mcanonicalize;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mbox;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_mget (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mbox_mget__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_mget__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mbox_mget__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_msetb_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mbox_msetb_mspecial__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mbox_msetb_mspecial__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mbox_msetb_mspecial__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mcar__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcar__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mcar__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcdr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mcdr__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcdr__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mcdr__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcharacterp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mcharacterp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mcons_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mcons_mspecial;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mconsp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mconsp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_meqp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_meqp__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_meqp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mfixnum_meqp__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_mnot_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_mnot_meqp__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_mnot_meqp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mfixnum_mnot_meqp__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mfixnump (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mfixnump;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mgreaterp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mgreaterp__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mgreaterp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mgreaterp__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mlessp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mlessp__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mlessp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mlessp__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegate (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mnegate__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegate__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnegate__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegativep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mnegativep__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnegativep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnegativep__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mconsp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnon_mconsp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnegativep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnegativep__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnegativep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnon_mnegativep__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnullp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnon_mnullp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mpositivep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mnon_mpositivep__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mpositivep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnon_mpositivep__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_msymbolp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnon_msymbolp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mzerop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mnon_mzerop__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnon_mzerop__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnon_mzerop__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnot;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_meqp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnot_meqp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mgreaterp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mnot_mgreaterp__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mgreaterp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnot_mgreaterp__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mlessp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mnot_mlessp__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnot_mlessp__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnot_mlessp__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnothingp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnothingp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mnullp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mnullp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mminus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mone_mminus__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mminus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mone_mminus__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mplus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mone_mplus__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mone_mplus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mone_mplus__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mpositivep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mpositivep__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mpositivep__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mpositivep__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mdivided__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided_munsafe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided_munsafe__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided_munsafe__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mdivided_munsafe__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mminus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mminus__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mminus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mminus__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mplus (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mplus__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mplus__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mplus__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mtimes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mtimes__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mtimes__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mtimes__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mquotient__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mquotient__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient_munsafe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mquotient_munsafe__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mquotient_munsafe__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mquotient_munsafe__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mremainder__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mremainder__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder_munsafe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mremainder_munsafe__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mremainder_munsafe__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mremainder_munsafe__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcarb_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mset_mcarb_mspecial__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcarb_mspecial__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mset_mcarb_mspecial__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcdrb_mspecial (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mset_mcdrb_mspecial__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mset_mcdrb_mspecial__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mset_mcdrb_mspecial__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_msymbolp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_msymbolp;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mdivided (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mdivided__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mdivided__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mdivided__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mquotient (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mquotient__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mquotient__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mquotient__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mremainder (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mremainder__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mremainder__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mremainder__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mtimes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mtimes__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mtimes__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mtimes__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_muniquep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_muniquep;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mzerop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_primitive_mzerop__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_primitive_mzerop__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_primitive_mzerop__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_procedure_mprolog (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_procedure_mprolog;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mfalse (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mfalse;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mglobal (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_push_mglobal__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mglobal__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = jitterlispvm_recognize_specialized_instruction_push_mglobal__nR__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mglobal__nR__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mglobal__nR__fR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mliteral (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_push_mliteral__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mliteral__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mliteral__nR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mnil (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mnil;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mnothing (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mnothing;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mone (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mone;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_push_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mregister___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_munspecified (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_munspecified;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_push_mzero (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_push_mzero;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_register_mto_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_register_mto_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_register_mto_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_register_mto_mregister___rrR___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_register_mto_mregister___rrR___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_register_mto_mregister___rrR___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_restore_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_restore_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_restore_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_restore_mregister___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_return (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_return;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_save_mregister (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = jitterlispvm_recognize_specialized_instruction_save_mregister___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_save_mregister___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_save_mregister___rrR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 6 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n6 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 7 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n7 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 9 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n9 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 10 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__n10 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n0__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n1__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n2__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n3__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n4__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n5__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n6__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n7__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n8__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n9__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__n10__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall__nR__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum jitterlispvm_specialized_instruction_opcode res = jitterlispvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 6 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n6 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 7 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n7 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 8 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n8 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 9 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n9 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 10 && enable_fast_literals)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n10 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n0__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n1__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n2__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n3__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n4__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n5__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n6 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n6__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n7 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n7__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n8 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n8__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n9 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n9__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__n10 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n10__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__nR__retR;
}

inline static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction_unreachable (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return jitterlispvm_specialized_instruction_opcode_unreachable;
}



/* Recognizer entry point. */
static enum jitterlispvm_specialized_instruction_opcode
jitterlispvm_recognize_specialized_instruction (struct jitter_mutable_routine *p,
                                            const struct jitter_instruction *ins)
{
  bool fl = ! p->options.slow_literals_only;
  const struct jitter_meta_instruction *mi = ins->meta_instruction;
  switch (mi->id)
    {
    case jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister:
      return jitterlispvm_recognize_specialized_instruction_at_mdepth_mto_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch:
      return jitterlispvm_recognize_specialized_instruction_branch (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mfalse:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mfalse (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mnon_mpositive:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mnon_mpositive (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mnot_mless:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mless (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mnot_mnull:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mnot_mnull (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mnull:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mnull (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mpositive:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mpositive (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mregister_mnon_mpositive:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mpositive (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mregister_mnon_mzero:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnon_mzero (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mregister_mnot_mnull:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnot_mnull (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mregister_mnull:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mnull (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mregister_mpositive:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mregister_mpositive (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_branch_mif_mtrue:
      return jitterlispvm_recognize_specialized_instruction_branch_mif_mtrue (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_call:
      return jitterlispvm_recognize_specialized_instruction_call (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_call_mcompiled:
      return jitterlispvm_recognize_specialized_instruction_call_mcompiled (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_call_mfrom_mc:
      return jitterlispvm_recognize_specialized_instruction_call_mfrom_mc (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_canonicalize_mboolean:
      return jitterlispvm_recognize_specialized_instruction_canonicalize_mboolean (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_cdr_mregister:
      return jitterlispvm_recognize_specialized_instruction_cdr_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_check_mclosure:
      return jitterlispvm_recognize_specialized_instruction_check_mclosure (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_check_mglobal_mdefined:
      return jitterlispvm_recognize_specialized_instruction_check_mglobal_mdefined (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_check_min_marity:
      return jitterlispvm_recognize_specialized_instruction_check_min_marity (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_check_min_marity_m_malt:
      return jitterlispvm_recognize_specialized_instruction_check_min_marity_m_malt (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_copy_mfrom_mliteral:
      return jitterlispvm_recognize_specialized_instruction_copy_mfrom_mliteral (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_copy_mfrom_mregister:
      return jitterlispvm_recognize_specialized_instruction_copy_mfrom_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_copy_mto_mregister:
      return jitterlispvm_recognize_specialized_instruction_copy_mto_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_drop:
      return jitterlispvm_recognize_specialized_instruction_drop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_drop_mnip:
      return jitterlispvm_recognize_specialized_instruction_drop_mnip (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_dup:
      return jitterlispvm_recognize_specialized_instruction_dup (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_exitvm:
      return jitterlispvm_recognize_specialized_instruction_exitvm (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_fail:
      return jitterlispvm_recognize_specialized_instruction_fail (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_gc_mif_mneeded:
      return jitterlispvm_recognize_specialized_instruction_gc_mif_mneeded (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_heap_mallocate:
      return jitterlispvm_recognize_specialized_instruction_heap_mallocate (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_literal_mto_mregister:
      return jitterlispvm_recognize_specialized_instruction_literal_mto_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip:
      return jitterlispvm_recognize_specialized_instruction_nip (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mdrop:
      return jitterlispvm_recognize_specialized_instruction_nip_mdrop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mfive:
      return jitterlispvm_recognize_specialized_instruction_nip_mfive (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mfive_mdrop:
      return jitterlispvm_recognize_specialized_instruction_nip_mfive_mdrop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mfour:
      return jitterlispvm_recognize_specialized_instruction_nip_mfour (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mfour_mdrop:
      return jitterlispvm_recognize_specialized_instruction_nip_mfour_mdrop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mpush_mliteral:
      return jitterlispvm_recognize_specialized_instruction_nip_mpush_mliteral (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mpush_mregister:
      return jitterlispvm_recognize_specialized_instruction_nip_mpush_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_msix:
      return jitterlispvm_recognize_specialized_instruction_nip_msix (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_msix_mdrop:
      return jitterlispvm_recognize_specialized_instruction_nip_msix_mdrop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mthree:
      return jitterlispvm_recognize_specialized_instruction_nip_mthree (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mthree_mdrop:
      return jitterlispvm_recognize_specialized_instruction_nip_mthree_mdrop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mtwo:
      return jitterlispvm_recognize_specialized_instruction_nip_mtwo (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nip_mtwo_mdrop:
      return jitterlispvm_recognize_specialized_instruction_nip_mtwo_mdrop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_nop:
      return jitterlispvm_recognize_specialized_instruction_nop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_one_mminus_mregister:
      return jitterlispvm_recognize_specialized_instruction_one_mminus_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_one_mplus_mregister:
      return jitterlispvm_recognize_specialized_instruction_one_mplus_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_pop_mto_mglobal:
      return jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_pop_mto_mglobal_mdefined:
      return jitterlispvm_recognize_specialized_instruction_pop_mto_mglobal_mdefined (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_pop_mto_mregister:
      return jitterlispvm_recognize_specialized_instruction_pop_mto_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive:
      return jitterlispvm_recognize_specialized_instruction_primitive (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mboolean_mcanonicalize:
      return jitterlispvm_recognize_specialized_instruction_primitive_mboolean_mcanonicalize (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mbox:
      return jitterlispvm_recognize_specialized_instruction_primitive_mbox (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mbox_mget:
      return jitterlispvm_recognize_specialized_instruction_primitive_mbox_mget (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mbox_msetb_mspecial:
      return jitterlispvm_recognize_specialized_instruction_primitive_mbox_msetb_mspecial (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mcar:
      return jitterlispvm_recognize_specialized_instruction_primitive_mcar (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mcdr:
      return jitterlispvm_recognize_specialized_instruction_primitive_mcdr (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mcharacterp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mcharacterp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mcons_mspecial:
      return jitterlispvm_recognize_specialized_instruction_primitive_mcons_mspecial (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mconsp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mconsp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_meqp:
      return jitterlispvm_recognize_specialized_instruction_primitive_meqp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mfixnum_meqp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_meqp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mfixnum_mnot_meqp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mfixnum_mnot_meqp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mfixnump:
      return jitterlispvm_recognize_specialized_instruction_primitive_mfixnump (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mgreaterp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mgreaterp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mlessp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mlessp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnegate:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnegate (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnegativep:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnegativep (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnon_mconsp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnon_mconsp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnon_mnegativep:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnegativep (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnon_mnullp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnon_mnullp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnon_mpositivep:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnon_mpositivep (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnon_msymbolp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnon_msymbolp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnon_mzerop:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnon_mzerop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnot:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnot (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnot_meqp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnot_meqp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnot_mgreaterp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnot_mgreaterp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnot_mlessp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnot_mlessp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnothingp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnothingp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mnullp:
      return jitterlispvm_recognize_specialized_instruction_primitive_mnullp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mone_mminus:
      return jitterlispvm_recognize_specialized_instruction_primitive_mone_mminus (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mone_mplus:
      return jitterlispvm_recognize_specialized_instruction_primitive_mone_mplus (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mpositivep:
      return jitterlispvm_recognize_specialized_instruction_primitive_mpositivep (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mprimordial_mdivided:
      return jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mprimordial_mdivided_munsafe:
      return jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mdivided_munsafe (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mprimordial_mminus:
      return jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mminus (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mprimordial_mplus:
      return jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mplus (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mprimordial_mtimes:
      return jitterlispvm_recognize_specialized_instruction_primitive_mprimordial_mtimes (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mquotient:
      return jitterlispvm_recognize_specialized_instruction_primitive_mquotient (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mquotient_munsafe:
      return jitterlispvm_recognize_specialized_instruction_primitive_mquotient_munsafe (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mremainder:
      return jitterlispvm_recognize_specialized_instruction_primitive_mremainder (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mremainder_munsafe:
      return jitterlispvm_recognize_specialized_instruction_primitive_mremainder_munsafe (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mset_mcarb_mspecial:
      return jitterlispvm_recognize_specialized_instruction_primitive_mset_mcarb_mspecial (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mset_mcdrb_mspecial:
      return jitterlispvm_recognize_specialized_instruction_primitive_mset_mcdrb_mspecial (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_msymbolp:
      return jitterlispvm_recognize_specialized_instruction_primitive_msymbolp (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mtwo_mdivided:
      return jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mdivided (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mtwo_mquotient:
      return jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mquotient (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mtwo_mremainder:
      return jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mremainder (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mtwo_mtimes:
      return jitterlispvm_recognize_specialized_instruction_primitive_mtwo_mtimes (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_muniquep:
      return jitterlispvm_recognize_specialized_instruction_primitive_muniquep (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_primitive_mzerop:
      return jitterlispvm_recognize_specialized_instruction_primitive_mzerop (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_procedure_mprolog:
      return jitterlispvm_recognize_specialized_instruction_procedure_mprolog (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mfalse:
      return jitterlispvm_recognize_specialized_instruction_push_mfalse (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mglobal:
      return jitterlispvm_recognize_specialized_instruction_push_mglobal (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mliteral:
      return jitterlispvm_recognize_specialized_instruction_push_mliteral (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mnil:
      return jitterlispvm_recognize_specialized_instruction_push_mnil (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mnothing:
      return jitterlispvm_recognize_specialized_instruction_push_mnothing (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mone:
      return jitterlispvm_recognize_specialized_instruction_push_mone (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mregister:
      return jitterlispvm_recognize_specialized_instruction_push_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_munspecified:
      return jitterlispvm_recognize_specialized_instruction_push_munspecified (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_push_mzero:
      return jitterlispvm_recognize_specialized_instruction_push_mzero (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_register_mto_mregister:
      return jitterlispvm_recognize_specialized_instruction_register_mto_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_restore_mregister:
      return jitterlispvm_recognize_specialized_instruction_restore_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_return:
      return jitterlispvm_recognize_specialized_instruction_return (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_save_mregister:
      return jitterlispvm_recognize_specialized_instruction_save_mregister (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_tail_mcall:
      return jitterlispvm_recognize_specialized_instruction_tail_mcall (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_tail_mcall_mcompiled:
      return jitterlispvm_recognize_specialized_instruction_tail_mcall_mcompiled (ins->parameters, fl);
    case jitterlispvm_meta_instruction_id_unreachable:
      return jitterlispvm_recognize_specialized_instruction_unreachable (ins->parameters, fl);
    default:
      jitter_fatal ("invalid meta-instruction id %i", (int)mi->id);
    }
  __builtin_unreachable ();
}

/* Specializer entry point: the only non-static function here. */
int
jitterlispvm_specialize_instruction (struct jitter_mutable_routine *p,
                                 const struct jitter_instruction *ins)
{
  enum jitterlispvm_specialized_instruction_opcode opcode
    = jitterlispvm_recognize_specialized_instruction (p, ins);
  if (opcode == jitterlispvm_specialized_instruction_opcode__eINVALID)
    jitter_fatal ("specialization failed: %s", ins->meta_instruction->name);

#ifdef JITTER_HAVE_DEFECT_REPLACEMENT
  /* Replace the opcode with its non-defective counterpart. */
  opcode = jitterlispvm_replacement_table [opcode];
#endif // #ifdef JITTER_HAVE_DEFECT_REPLACEMENT

  jitter_add_specialized_instruction_opcode (p, opcode);


  /* FIXME: in the old shell-based generator I grouped specialized instructions by
     their "residual parameter map", yielding a switch with a lot of different
     specialized instructions mapping to the same case.  I should redo that here. */
  switch (opcode)
    {
    /* !INVALID. */
    case jitterlispvm_specialized_instruction_opcode__eINVALID:
      break;

    /* !BEGINBASICBLOCK. */
    case jitterlispvm_specialized_instruction_opcode__eBEGINBASICBLOCK:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* !EXITVM. */
    case jitterlispvm_specialized_instruction_opcode__eEXITVM:
      break;

    /* !DATALOCATIONS. */
    case jitterlispvm_specialized_instruction_opcode__eDATALOCATIONS:
      break;

    /* !NOP. */
    case jitterlispvm_specialized_instruction_opcode__eNOP:
      break;

    /* !UNREACHABLE0. */
    case jitterlispvm_specialized_instruction_opcode__eUNREACHABLE0:
      break;

    /* !UNREACHABLE1. */
    case jitterlispvm_specialized_instruction_opcode__eUNREACHABLE1:
      break;

    /* !PRETENDTOJUMPANYWHERE. */
    case jitterlispvm_specialized_instruction_opcode__ePRETENDTOJUMPANYWHERE:
      break;

    /* at-depth-to-register/n1/%rR. */
    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n1___rrR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* at-depth-to-register/n2/%rR. */
    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n2___rrR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* at-depth-to-register/n3/%rR. */
    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n3___rrR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* at-depth-to-register/n4/%rR. */
    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n4___rrR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* at-depth-to-register/n5/%rR. */
    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n5___rrR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* at-depth-to-register/n6/%rR. */
    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n6___rrR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* at-depth-to-register/n7/%rR. */
    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n7___rrR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* at-depth-to-register/n8/%rR. */
    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n8___rrR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* at-depth-to-register/n9/%rR. */
    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n9___rrR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* at-depth-to-register/n10/%rR. */
    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n10___rrR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* at-depth-to-register/nR/%rR. */
    case jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__nR___rrR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* branch/fR. */
    case jitterlispvm_specialized_instruction_opcode_branch__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* branch-if-false/fR. */
    case jitterlispvm_specialized_instruction_opcode_branch_mif_mfalse__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* branch-if-non-positive/fR/fR. */
    case jitterlispvm_specialized_instruction_opcode_branch_mif_mnon_mpositive__fR__fR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* branch-if-not-less/fR/fR. */
    case jitterlispvm_specialized_instruction_opcode_branch_mif_mnot_mless__fR__fR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* branch-if-not-null/fR. */
    case jitterlispvm_specialized_instruction_opcode_branch_mif_mnot_mnull__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* branch-if-null/fR. */
    case jitterlispvm_specialized_instruction_opcode_branch_mif_mnull__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* branch-if-positive/fR/fR. */
    case jitterlispvm_specialized_instruction_opcode_branch_mif_mpositive__fR__fR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* branch-if-register-non-positive/%rR/fR/fR. */
    case jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mnon_mpositive___rrR__fR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* branch-if-register-non-zero/%rR/fR/fR. */
    case jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mnon_mzero___rrR__fR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* branch-if-register-not-null/%rR/fR. */
    case jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mnot_mnull___rrR__fR:
      /* j:0 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* branch-if-register-null/%rR/fR. */
    case jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mnull___rrR__fR:
      /* j:0 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* branch-if-register-positive/%rR/fR/fR. */
    case jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mpositive___rrR__fR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* branch-if-true/fR. */
    case jitterlispvm_specialized_instruction_opcode_branch_mif_mtrue__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* call/n0/retR. */
    case jitterlispvm_specialized_instruction_opcode_call__n0__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call/n1/retR. */
    case jitterlispvm_specialized_instruction_opcode_call__n1__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call/n2/retR. */
    case jitterlispvm_specialized_instruction_opcode_call__n2__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call/n3/retR. */
    case jitterlispvm_specialized_instruction_opcode_call__n3__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call/n4/retR. */
    case jitterlispvm_specialized_instruction_opcode_call__n4__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call/n5/retR. */
    case jitterlispvm_specialized_instruction_opcode_call__n5__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call/n6/retR. */
    case jitterlispvm_specialized_instruction_opcode_call__n6__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call/n7/retR. */
    case jitterlispvm_specialized_instruction_opcode_call__n7__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call/n8/retR. */
    case jitterlispvm_specialized_instruction_opcode_call__n8__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call/n9/retR. */
    case jitterlispvm_specialized_instruction_opcode_call__n9__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call/n10/retR. */
    case jitterlispvm_specialized_instruction_opcode_call__n10__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call/nR/retR. */
    case jitterlispvm_specialized_instruction_opcode_call__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call-compiled/n0/retR. */
    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n0__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call-compiled/n1/retR. */
    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n1__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call-compiled/n2/retR. */
    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n2__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call-compiled/n3/retR. */
    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n3__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call-compiled/n4/retR. */
    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n4__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call-compiled/n5/retR. */
    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n5__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call-compiled/n6/retR. */
    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n6__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call-compiled/n7/retR. */
    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n7__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call-compiled/n8/retR. */
    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n8__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call-compiled/n9/retR. */
    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n9__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call-compiled/n10/retR. */
    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__n10__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call-compiled/nR/retR. */
    case jitterlispvm_specialized_instruction_opcode_call_mcompiled__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* call-from-c/retR. */
    case jitterlispvm_specialized_instruction_opcode_call_mfrom_mc__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* canonicalize-boolean. */
    case jitterlispvm_specialized_instruction_opcode_canonicalize_mboolean:
      break;

    /* cdr-register/%rR/%rR/fR. */
    case jitterlispvm_specialized_instruction_opcode_cdr_mregister___rrR___rrR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* check-closure/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_mclosure__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* check-global-defined/nR/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_mglobal_mdefined__nR__fR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity/n0/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n0__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity/n1/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n1__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity/n2/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n2__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity/n3/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n3__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity/n4/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n4__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity/n5/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n5__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity/n6/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n6__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity/n7/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n7__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity/n8/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n8__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity/n9/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n9__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity/n10/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity__n10__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity/nR/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity__nR__fR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity--alt/n0/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n0__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity--alt/n1/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n1__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity--alt/n2/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n2__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity--alt/n3/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n3__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity--alt/n4/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n4__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity--alt/n5/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n5__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity--alt/n6/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n6__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity--alt/n7/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n7__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity--alt/n8/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n8__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity--alt/n9/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n9__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity--alt/n10/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n10__fR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* check-in-arity--alt/nR/fR. */
    case jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__nR__fR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* copy-from-literal/nR. */
    case jitterlispvm_specialized_instruction_opcode_copy_mfrom_mliteral__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* copy-from-register/%rR. */
    case jitterlispvm_specialized_instruction_opcode_copy_mfrom_mregister___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* copy-to-register/%rR. */
    case jitterlispvm_specialized_instruction_opcode_copy_mto_mregister___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* drop. */
    case jitterlispvm_specialized_instruction_opcode_drop:
      break;

    /* drop-nip. */
    case jitterlispvm_specialized_instruction_opcode_drop_mnip:
      break;

    /* dup. */
    case jitterlispvm_specialized_instruction_opcode_dup:
      break;

    /* exitvm. */
    case jitterlispvm_specialized_instruction_opcode_exitvm:
      break;

    /* fail/retR. */
    case jitterlispvm_specialized_instruction_opcode_fail__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* gc-if-needed/fR. */
    case jitterlispvm_specialized_instruction_opcode_gc_mif_mneeded__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* heap-allocate/n4. */
    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n4:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* heap-allocate/n8. */
    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n8:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* heap-allocate/n12. */
    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n12:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* heap-allocate/n16. */
    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n16:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* heap-allocate/n24. */
    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n24:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* heap-allocate/n32. */
    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n32:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* heap-allocate/n36. */
    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n36:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* heap-allocate/n48. */
    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n48:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* heap-allocate/n52. */
    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n52:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* heap-allocate/n64. */
    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__n64:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* heap-allocate/nR. */
    case jitterlispvm_specialized_instruction_opcode_heap_mallocate__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* literal-to-register/nR/%rR. */
    case jitterlispvm_specialized_instruction_opcode_literal_mto_mregister__nR___rrR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* nip. */
    case jitterlispvm_specialized_instruction_opcode_nip:
      break;

    /* nip-drop. */
    case jitterlispvm_specialized_instruction_opcode_nip_mdrop:
      break;

    /* nip-five. */
    case jitterlispvm_specialized_instruction_opcode_nip_mfive:
      break;

    /* nip-five-drop. */
    case jitterlispvm_specialized_instruction_opcode_nip_mfive_mdrop:
      break;

    /* nip-four. */
    case jitterlispvm_specialized_instruction_opcode_nip_mfour:
      break;

    /* nip-four-drop. */
    case jitterlispvm_specialized_instruction_opcode_nip_mfour_mdrop:
      break;

    /* nip-push-literal/nR. */
    case jitterlispvm_specialized_instruction_opcode_nip_mpush_mliteral__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* nip-push-register/%rR. */
    case jitterlispvm_specialized_instruction_opcode_nip_mpush_mregister___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* nip-six. */
    case jitterlispvm_specialized_instruction_opcode_nip_msix:
      break;

    /* nip-six-drop. */
    case jitterlispvm_specialized_instruction_opcode_nip_msix_mdrop:
      break;

    /* nip-three. */
    case jitterlispvm_specialized_instruction_opcode_nip_mthree:
      break;

    /* nip-three-drop. */
    case jitterlispvm_specialized_instruction_opcode_nip_mthree_mdrop:
      break;

    /* nip-two. */
    case jitterlispvm_specialized_instruction_opcode_nip_mtwo:
      break;

    /* nip-two-drop. */
    case jitterlispvm_specialized_instruction_opcode_nip_mtwo_mdrop:
      break;

    /* nop. */
    case jitterlispvm_specialized_instruction_opcode_nop:
      break;

    /* one-minus-register/%rR/%rR/fR. */
    case jitterlispvm_specialized_instruction_opcode_one_mminus_mregister___rrR___rrR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* one-plus-register/%rR/%rR/fR. */
    case jitterlispvm_specialized_instruction_opcode_one_mplus_mregister___rrR___rrR__fR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* pop-to-global/nR/fR. */
    case jitterlispvm_specialized_instruction_opcode_pop_mto_mglobal__nR__fR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* pop-to-global-defined/nR/fR. */
    case jitterlispvm_specialized_instruction_opcode_pop_mto_mglobal_mdefined__nR__fR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* pop-to-register/%rR. */
    case jitterlispvm_specialized_instruction_opcode_pop_mto_mregister___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* primitive/nR/n0/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive__nR__n0__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* primitive/nR/n1/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive__nR__n1__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* primitive/nR/n2/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive__nR__n2__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* primitive/nR/n3/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive__nR__n3__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* primitive/nR/n4/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive__nR__n4__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      /* Argument 1 (0-based, of 3) is non-residual */
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* primitive/nR/nR/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive__nR__nR__fR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      break;

    /* primitive-boolean-canonicalize. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mboolean_mcanonicalize:
      break;

    /* primitive-box. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mbox:
      break;

    /* primitive-box-get/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mbox_mget__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-box-setb-special/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mbox_msetb_mspecial__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-car/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mcar__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-cdr/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mcdr__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-characterp. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mcharacterp:
      break;

    /* primitive-cons-special. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mcons_mspecial:
      break;

    /* primitive-consp. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mconsp:
      break;

    /* primitive-eqp. */
    case jitterlispvm_specialized_instruction_opcode_primitive_meqp:
      break;

    /* primitive-fixnum-eqp/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mfixnum_meqp__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-fixnum-not-eqp/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mfixnum_mnot_meqp__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-fixnump. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mfixnump:
      break;

    /* primitive-greaterp/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mgreaterp__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-lessp/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mlessp__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-negate/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mnegate__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-negativep/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mnegativep__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-non-consp. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mnon_mconsp:
      break;

    /* primitive-non-negativep/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mnon_mnegativep__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-non-nullp. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mnon_mnullp:
      break;

    /* primitive-non-positivep/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mnon_mpositivep__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-non-symbolp. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mnon_msymbolp:
      break;

    /* primitive-non-zerop/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mnon_mzerop__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-not. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mnot:
      break;

    /* primitive-not-eqp. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mnot_meqp:
      break;

    /* primitive-not-greaterp/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mnot_mgreaterp__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-not-lessp/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mnot_mlessp__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-nothingp. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mnothingp:
      break;

    /* primitive-nullp. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mnullp:
      break;

    /* primitive-one-minus/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mone_mminus__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-one-plus/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mone_mplus__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-positivep/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mpositivep__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-primordial-divided/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mdivided__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-primordial-divided-unsafe/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mdivided_munsafe__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-primordial-minus/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mminus__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-primordial-plus/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mplus__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-primordial-times/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mtimes__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-quotient/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mquotient__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-quotient-unsafe/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mquotient_munsafe__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-remainder/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mremainder__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-remainder-unsafe/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mremainder_munsafe__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-set-carb-special/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mset_mcarb_mspecial__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-set-cdrb-special/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mset_mcdrb_mspecial__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-symbolp. */
    case jitterlispvm_specialized_instruction_opcode_primitive_msymbolp:
      break;

    /* primitive-two-divided/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mdivided__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-two-quotient/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mquotient__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-two-remainder/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mremainder__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-two-times/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mtimes__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* primitive-uniquep. */
    case jitterlispvm_specialized_instruction_opcode_primitive_muniquep:
      break;

    /* primitive-zerop/fR. */
    case jitterlispvm_specialized_instruction_opcode_primitive_mzerop__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* procedure-prolog. */
    case jitterlispvm_specialized_instruction_opcode_procedure_mprolog:
      /* This is a callee instruction. */
      break;

    /* push-false. */
    case jitterlispvm_specialized_instruction_opcode_push_mfalse:
      break;

    /* push-global/nR/fR. */
    case jitterlispvm_specialized_instruction_opcode_push_mglobal__nR__fR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      break;

    /* push-literal/nR. */
    case jitterlispvm_specialized_instruction_opcode_push_mliteral__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* push-nil. */
    case jitterlispvm_specialized_instruction_opcode_push_mnil:
      break;

    /* push-nothing. */
    case jitterlispvm_specialized_instruction_opcode_push_mnothing:
      break;

    /* push-one. */
    case jitterlispvm_specialized_instruction_opcode_push_mone:
      break;

    /* push-register/%rR. */
    case jitterlispvm_specialized_instruction_opcode_push_mregister___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* push-unspecified. */
    case jitterlispvm_specialized_instruction_opcode_push_munspecified:
      break;

    /* push-zero. */
    case jitterlispvm_specialized_instruction_opcode_push_mzero:
      break;

    /* register-to-register/%rR/%rR. */
    case jitterlispvm_specialized_instruction_opcode_register_mto_mregister___rrR___rrR:
      /* j:0 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:2 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      break;

    /* restore-register/%rR. */
    case jitterlispvm_specialized_instruction_opcode_restore_mregister___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* return. */
    case jitterlispvm_specialized_instruction_opcode_return:
      break;

    /* save-register/%rR. */
    case jitterlispvm_specialized_instruction_opcode_save_mregister___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* tail-call/n0/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n0__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call/n1/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n1__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call/n2/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n2__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call/n3/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n3__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call/n4/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n4__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call/n5/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n5__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call/n6/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n6__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call/n7/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n7__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call/n8/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n8__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call/n9/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n9__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call/n10/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall__n10__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call/nR/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call-compiled/n0/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n0__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call-compiled/n1/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n1__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call-compiled/n2/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n2__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call-compiled/n3/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n3__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call-compiled/n4/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n4__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call-compiled/n5/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n5__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call-compiled/n6/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n6__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call-compiled/n7/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n7__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call-compiled/n8/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n8__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call-compiled/n9/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n9__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call-compiled/n10/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n10__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* tail-call-compiled/nR/retR. */
    case jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* unreachable. */
    case jitterlispvm_specialized_instruction_opcode_unreachable:
      break;

    /* !REPLACEMENT-branch/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-branch-if-false/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mfalse__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-branch-if-non-positive/fR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnon_mpositive__fR__fR__retR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-branch-if-not-less/fR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnot_mless__fR__fR__retR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-branch-if-not-null/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnot_mnull__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-branch-if-null/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnull__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-branch-if-positive/fR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mpositive__fR__fR__retR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-branch-if-register-non-positive/%rR/fR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnon_mpositive___rrR__fR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-branch-if-register-non-zero/%rR/fR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnon_mzero___rrR__fR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-branch-if-register-not-null/%rR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnot_mnull___rrR__fR__retR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-branch-if-register-null/%rR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnull___rrR__fR__retR:
      /* j:0 residual_no:3 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-branch-if-register-positive/%rR/fR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mpositive___rrR__fR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-branch-if-true/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mtrue__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-call/n0/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n0__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call/n1/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n1__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call/n2/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n2__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call/n3/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n3__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call/n4/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n4__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call/n5/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n5__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call/n6/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n6__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call/n7/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n7__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call/n8/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n8__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call/n9/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n9__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call/n10/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n10__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call/nR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call-compiled/n0/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n0__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call-compiled/n1/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n1__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call-compiled/n2/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n2__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call-compiled/n3/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n3__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call-compiled/n4/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n4__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call-compiled/n5/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n5__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call-compiled/n6/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n6__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call-compiled/n7/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n7__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call-compiled/n8/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n8__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call-compiled/n9/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n9__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call-compiled/n10/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n10__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call-compiled/nR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-call-from-c/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mfrom_mc__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-cdr-register/%rR/%rR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcdr_mregister___rrR___rrR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-closure/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_mclosure__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-global-defined/nR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_mglobal_mdefined__nR__fR__retR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity/n0/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n0__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity/n1/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n1__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity/n2/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n2__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity/n3/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n3__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity/n4/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n4__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity/n5/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n5__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity/n6/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n6__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity/n7/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n7__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity/n8/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n8__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity/n9/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n9__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity/n10/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n10__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity/nR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__nR__fR__retR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity--alt/n0/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n0__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity--alt/n1/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n1__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity--alt/n2/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n2__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity--alt/n3/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n3__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity--alt/n4/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n4__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity--alt/n5/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n5__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity--alt/n6/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n6__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity--alt/n7/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n7__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity--alt/n8/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n8__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity--alt/n9/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n9__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity--alt/n10/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n10__fR__retR:
      /* j:0 residual_no:3 */
      /* Argument 0 (0-based, of 3) is non-residual */
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-check-in-arity--alt/nR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__nR__fR__retR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-exitvm/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mexitvm__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-gc-if-needed/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mgc_mif_mneeded__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-one-minus-register/%rR/%rR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mone_mminus_mregister___rrR___rrR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-one-plus-register/%rR/%rR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mone_mplus_mregister___rrR___rrR__fR__retR:
      /* j:0 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      /* j:1 residual_no:4 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, JITTERLISPVM_SLOW_REGISTER_OFFSET(r, ins->parameters[1]->register_index));
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-pop-to-global/nR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mpop_mto_mglobal__nR__fR__retR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-pop-to-global-defined/nR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mpop_mto_mglobal_mdefined__nR__fR__retR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive/nR/n0/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n0__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive/nR/n1/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n1__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive/nR/n2/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n2__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive/nR/n3/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n3__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive/nR/n4/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n4__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      /* Argument 1 (0-based, of 4) is non-residual */
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive/nR/nR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__nR__fR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[2]->label_as_index);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-box-get/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mbox_mget__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-box-setb-special/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mbox_msetb_mspecial__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-car/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mcar__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-cdr/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mcdr__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-fixnum-eqp/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mfixnum_meqp__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-fixnum-not-eqp/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mfixnum_mnot_meqp__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-greaterp/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mgreaterp__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-lessp/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mlessp__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-negate/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnegate__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-negativep/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnegativep__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-non-negativep/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnon_mnegativep__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-non-positivep/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnon_mpositivep__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-non-zerop/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnon_mzerop__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-not-greaterp/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnot_mgreaterp__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-not-lessp/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnot_mlessp__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-one-minus/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mone_mminus__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-one-plus/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mone_mplus__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-positivep/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mpositivep__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-primordial-divided/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mdivided__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-primordial-divided-unsafe/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mdivided_munsafe__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-primordial-minus/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mminus__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-primordial-plus/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mplus__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-primordial-times/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mtimes__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-quotient/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mquotient__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-quotient-unsafe/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mquotient_munsafe__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-remainder/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mremainder__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-remainder-unsafe/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mremainder_munsafe__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-set-carb-special/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mset_mcarb_mspecial__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-set-cdrb-special/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mset_mcdrb_mspecial__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-two-divided/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mdivided__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-two-quotient/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mquotient__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-two-remainder/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mremainder__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-two-times/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mtimes__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-primitive-zerop/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mzerop__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-procedure-prolog/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprocedure_mprolog__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a callee instruction. */
      break;

    /* !REPLACEMENT-push-global/nR/fR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mpush_mglobal__nR__fR__retR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[1]->label_as_index);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-return/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mreturn__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-tail-call/n0/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n0__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call/n1/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n1__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call/n2/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n2__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call/n3/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n3__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call/n4/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n4__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call/n5/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n5__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call/n6/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n6__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call/n7/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n7__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call/n8/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n8__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call/n9/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n9__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call/n10/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n10__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call/nR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call-compiled/n0/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n0__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call-compiled/n1/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n1__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call-compiled/n2/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n2__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call-compiled/n3/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n3__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call-compiled/n4/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n4__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call-compiled/n5/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n5__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call-compiled/n6/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n6__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call-compiled/n7/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n7__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call-compiled/n8/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n8__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call-compiled/n9/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n9__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call-compiled/n10/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n10__retR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-tail-call-compiled/nR/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-unreachable/retR. */
    case jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_munreachable__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    default:
      jitter_fatal ("invalid specialized instruction opcode %i", (int)opcode);
    }
  return 1; // FIXME: I should rethink this return value.
}

void
jitterlispvm_state_initialize_with_slow_registers
   (struct jitterlispvm_state *jitter_state,
    jitter_uint jitter_slow_register_no_per_class)
{
  struct jitterlispvm_state_backing * const jitter_state_backing
    __attribute__ ((unused))
    = & jitter_state->jitterlispvm_state_backing;
  struct jitterlispvm_state_runtime * const jitter_state_runtime
    __attribute__ ((unused))
    = & jitter_state->_jitterlispvm_1TbNcL3hel_state_runtime;

  /* Initialize The Array. */
  jitter_state_backing->jitter_slow_register_no_per_class
    = jitter_slow_register_no_per_class;
  jitter_state_backing->jitter_array
    = jitter_xmalloc (JITTERLISPVM_ARRAY_SIZE(jitter_state_backing
                         ->jitter_slow_register_no_per_class));

  /* Initialize special-purpose data. */
  jitterlispvm_initialize_special_purpose_data (JITTERLISPVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA (jitter_state_backing->jitter_array));

  /* Set the initial program point to an invalid value, for defensiveness. */
  jitter_state_backing->initial_program_point = NULL;

  /* Set the initial exit status. */
  jitter_state_backing->exit_status
    = jitterlispvm_exit_status_never_executed;

  /* Initialize stack backing and stack runtime data structures, pointing
     to memory from the backings. */
  /* Make the stack backing for mainstack . */
  jitter_stack_initialize_tos_backing(& jitter_state_backing->jitter_stack_mainstack_backing,
                                      sizeof (jitterlisp_object),
                                      10240,
                                      NULL,
                                      0,
                                      0);
  JITTER_STACK_TOS_INITIALIZE(jitterlisp_object, jitter_state_runtime-> ,
                              mainstack, jitter_state_backing->jitter_stack_mainstack_backing);
  /* Make the stack backing for returnstack . */
  jitter_stack_initialize_ntos_backing(& jitter_state_backing->jitter_stack_returnstack_backing,
                                      sizeof (jitterlisp_object),
                                      10240,
                                      NULL,
                                      0,
                                      0);
  JITTER_STACK_NTOS_INITIALIZE(jitterlisp_object, jitter_state_runtime-> ,
                              returnstack, jitter_state_backing->jitter_stack_returnstack_backing);

  /* Initialise the link register. */
  jitter_state_runtime->_jitter_link.label = NULL;

  /* No need to initialise r-class fast registers. */

  /* Initialise slow registers. */
  jitterlispvm_initialize_slow_registers
     (jitter_state->jitterlispvm_state_backing.jitter_array,
      0 /* overwrite any already existing rank */,
      jitter_state_backing->jitter_slow_register_no_per_class);

  /* Link this new state to the list of states. */
  JITTER_LIST_LINK_LAST (jitterlispvm_state, links, & jitterlispvm_vm->states, jitter_state);

  /* User code for state initialization. */

#ifdef JITTER_GC_STUB
  /* Initialize the next pointer and the limit pointer to refer to a fixed-size
     nursery.  There is no real GC yet, so when next hits limit there will be
     a failure; still, allocation should work up to that point. */
  size_t nursery_size = 1024 * 1024 * 10;
  char *nursery = jitter_xmalloc (nursery_size);
  jitter_state_runtime->allocation_next = nursery;
  jitter_state_runtime->allocation_limit = nursery + nursery_size;
#endif // #ifdef JITTER_GC_STUB

  /* Register the two stack backings as GC roots. */
  jitterlisp_push_stack_backing_as_gc_root
     (& jitter_state_backing->jitter_stack_mainstack_backing);
  jitterlisp_push_stack_backing_as_gc_root
     (& jitter_state_backing->jitter_stack_returnstack_backing);
  
  /* End of the user code for state initialization. */

}

void
jitterlispvm_state_reset
   (struct jitterlispvm_state *jitter_state)
{
  struct jitterlispvm_state_backing * const jitter_state_backing
    __attribute__ ((unused))
    = & jitter_state->jitterlispvm_state_backing;
  struct jitterlispvm_state_runtime * const jitter_state_runtime
    __attribute__ ((unused))
    = & jitter_state->_jitterlispvm_1TbNcL3hel_state_runtime;

  /* No need to touch The Array, which already exists. */
  /* No need to touch special-purpose data, which already exist. */

  /* Set the initial program point to an invalid value, for defensiveness. */
  jitter_state_backing->initial_program_point = NULL;

  /* Set the initial exit status. */
  jitter_state_backing->exit_status
    = jitterlispvm_exit_status_never_executed;

  /* Initialize stack backing and stack runtime data structures, pointing
     to memory from the backings. */
  /* The stack backing for mainstack already exists, and does
     not require element initialisation. */
  JITTER_STACK_TOS_INITIALIZE(jitterlisp_object, jitter_state_runtime-> ,
                              mainstack, jitter_state_backing->jitter_stack_mainstack_backing);
  /* The stack backing for returnstack already exists, and does
     not require element initialisation. */
  JITTER_STACK_NTOS_INITIALIZE(jitterlisp_object, jitter_state_runtime-> ,
                              returnstack, jitter_state_backing->jitter_stack_returnstack_backing);

  /* Initialise the link register. */
  jitter_state_runtime->_jitter_link.label = NULL;

  /* No need to initialise r-class fast registers. */

  /* Initialise slow registers. */
  jitterlispvm_initialize_slow_registers
     (jitter_state->jitterlispvm_state_backing.jitter_array,
      0 /* overwrite any already existing rank */,
      jitter_state_backing->jitter_slow_register_no_per_class);


  /* No need to touch links within the global list of states:
     this state already exists and is already linked. */

  /* User code for state reset. */

    /* If this explicit code block were not provided a state would be reset
       by first executing the user finalisation code, and then the user
       initialisation code.  That would first unregister some roots, and
       then re-register them.
       The best solution here consists in explicitly doing nothing. */
  
  /* End of the user code for state reset. */
}

void
jitterlispvm_state_finalize (struct jitterlispvm_state *jitter_state)
{
  struct jitterlispvm_state_backing * const jitter_state_backing
    __attribute__ ((unused))
    = & jitter_state->jitterlispvm_state_backing;
  struct jitterlispvm_state_runtime * const jitter_state_runtime
    __attribute__ ((unused))
    = & jitter_state->_jitterlispvm_1TbNcL3hel_state_runtime;

  /* User code for state finalization. */

    /* This assumes that states are made and destroyed in LIFO order, which
       is true in JitterLisp but still crude.  I will define a better API
       when switching to the new Jitter GC (in Boehm's defense, it could
       easily be done with his GC as well). */
    jitterlisp_pop_gc_roots (2);
  
  /* End of the user code for state finalization. */

  /* Finalize stack backings -- There is no need to finalize the stack
     runtime data structures, as they hold no heap data of their own. */
  jitter_stack_finalize_backing (& jitter_state_backing->jitter_stack_mainstack_backing);
  jitter_stack_finalize_backing (& jitter_state_backing->jitter_stack_returnstack_backing);


  /* Unlink this state from the list of states. */
  JITTER_LIST_UNLINK (jitterlispvm_state, links, & jitterlispvm_vm->states, jitter_state);

  /* Finalize special-purpose data. */
  jitterlispvm_finalize_special_purpose_data (JITTERLISPVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA (jitter_state_backing->jitter_array));

  /* Finalize the Array. */
  free ((void *) jitter_state_backing->jitter_array);

}

