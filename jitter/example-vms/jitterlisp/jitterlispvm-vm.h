/* This machine-generated file includes source code from GNU Jitter.

   Copyright (C) 2016-2021 Luca Saiu
   Written by Luca Saiu

   GNU Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   GNU Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Jitter.  If not, see <https://www.gnu.org/licenses/>.


  Copyright (C) 2017, 2018, 2020, 2021  Luca Saiu
  Written by Luca Saiu

  This file is part of JitterLisp, distributed as an example along with
  GNU Jitter under the same license.

  JitterLisp is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  JitterLisp is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with JitterLisp.  If not, see <https://www.gnu.org/licenses/>.

*/

/* User-specified code, initial header part: beginning. */

/* User-specified code, initial header part: end */

/* VM library: main header file.

   Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021 Luca Saiu
   Written by Luca Saiu

   This file is part of GNU Jitter.

   GNU Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GNU Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Jitter.  If not, see <https://www.gnu.org/licenses/>. */


/* Generated file warning.
 * ************************************************************************** */

/* Unless this file is named exactly "vm.h" , without any prefix, you are
   looking at a machine-generated derived file.  The original source is the vm.h
   template from Jitter, with added code implementing the jitterlispvm VM. */




/* This multiple-inclusion guard is opened here in the template, and will be
   closed at the end of the generated code.  It is normal to find no matching
   #endif in the template file.  */
#ifndef JITTERLISPVM_VM_H_
#define JITTERLISPVM_VM_H_


/* This is the main VM header to use from hand-written code.
 * ************************************************************************** */

#include <jitter/jitter-early-header.h>

#include <stdio.h>
#include <stdbool.h>

#include <jitter/jitter.h>
#include <jitter/jitter-hash.h>
#include <jitter/jitter-stack.h>
#include <jitter/jitter-instruction.h>
#include <jitter/jitter-mutable-routine.h>
#include <jitter/jitter-print.h>
#include <jitter/jitter-routine.h>
//#include <jitter/jitter-specialize.h> // FIXME: what about only declaring jitter_specialize in another header, and not including this?
#include <jitter/jitter-disassemble.h>
#include <jitter/jitter-vm.h>
#include <jitter/jitter-profile.h>
#include <jitter/jitter-data-locations.h>
#include <jitter/jitter-arithmetic.h>
#include <jitter/jitter-bitwise.h>
#include <jitter/jitter-signals.h>
#include <jitter/jitter-list.h>




/* Initialization and finalization.
 * ************************************************************************** */

/* Initialize the runtime state for the jitterlispvm VM.  This needs to be called
   before using VM routines or VM states in any way. */
void
jitterlispvm_initialize (void);

/* Finalize the runtime state, freeing some resources.  After calling this no
   use of VM routines or states is allowed.  It is possible to re-initialize
   after finalizing; these later re-initializations might be more efficient than
   the first initialization. */
void
jitterlispvm_finalize (void);




/* State data structure initialization and finalization.
 * ************************************************************************** */

/* The machine state is separated into the backing and the more compact runtime
   data structures, to be allocated in registers as far as possible.  These are
   just a forward-declarations: the actual definitions are machine-generated. */
struct jitterlispvm_state_backing;
struct jitterlispvm_state_runtime;

/* A data structure containing both the backing and the runtime state.  This is
   a forward-declaration: the actual definition will come after both are
   defined. */
struct jitterlispvm_state;

/* Initialize the pointed VM state data structure, or fail fatally.  The
   function definition is machine-generated, even if it may include user code.
   The state backing and runtime are initialized at the same time, and in fact
   the distinction between them is invisible to the VM user.
   The version not specifying a given number of slow registers per class
   sets slow registers to be initially zero. */
void
jitterlispvm_state_initialize (struct jitterlispvm_state *state)
  __attribute__ ((nonnull (1)));
void
jitterlispvm_state_initialize_with_slow_registers (struct jitterlispvm_state *state,
                                               jitter_uint
                                               slow_register_no_per_class)
  __attribute__ ((nonnull (1)));

/* Finalize the pointed VM state data structure, or fail fatally.  The function
   definition is machine-generated, even if it may include user code.  The state
   backing and runtime are finalized at the same time. */
void
jitterlispvm_state_finalize (struct jitterlispvm_state *state)
  __attribute__ ((nonnull (1)));

/* The make/destroy counterparts of the initialize/finalize functions above. */
struct jitterlispvm_state *
jitterlispvm_state_make (void)
  __attribute__ ((returns_nonnull));
struct jitterlispvm_state *
jitterlispvm_state_make_with_slow_registers (jitter_uint slow_register_no_per_class)
  __attribute__ ((returns_nonnull));
void
jitterlispvm_state_destroy (struct jitterlispvm_state *state)
  __attribute__ ((nonnull (1)));

/* Reset the pointed VM state, restoring its initial content.  This is cheaper
   than finalising and re-initialising a state. */
void
jitterlispvm_state_reset (struct jitterlispvm_state *state)
  __attribute__ ((nonnull (1)));




/* State data structure: iteration.
 * ************************************************************************** */

/* The header of a doubly-linked list linking every state for the jitterlispvm VM
   together.  This global is automatically wrapped, and therefore also
   accessible from VM instruction code. */
extern struct jitter_list_header * const
jitterlispvm_states;

/* A pointer to the current state, only accessible from VM code.  This is usable
   for pointer comparison when iterating over states. */
#define JITTERLISPVM_OWN_STATE                           \
  ((struct jitterlispvm_state *) jitter_original_state)

/* Given an l-value of type struct jitterlispvm_state * (usually a variable name)
   expand to a for loop statement iterating over every existing jitterlispvm state
   using the l-value as iteration variable.  The expansion will execute the
   statement immediately following the macro call with the l-value in scope;
   in order words the loop body is not a macro argument, but follows the macro
   use.
   The l-value may be evaluated an unspecified number of times.
   This macro is safe to use within VM instruction code.
   For example:
     struct jitterlispvm_state *s;
     JITTERLISPVM_FOR_EACH_STATE (s)
       printf ("This is a state: %p\n", s); // (but printf unsafe in VM code) */
#define JITTERLISPVM_FOR_EACH_STATE(jitter_state_iteration_lvalue)     \
  for ((jitter_state_iteration_lvalue)                             \
          = jitterlispvm_states->first;                                \
       (jitter_state_iteration_lvalue)                             \
          != NULL;                                                 \
       (jitter_state_iteration_lvalue)                             \
         = (jitter_state_iteration_lvalue)->links.next)            \
    /* Here comes the body supplied by the user: no semicolon. */




/* Mutable routine initialization.
 * ************************************************************************** */

/* Return a freshly-allocated empty mutable routine for the jitterlispvm VM. */
struct jitter_mutable_routine*
jitterlispvm_make_mutable_routine (void)
  __attribute__ ((returns_nonnull));

/* Mutable routine finalization is actually VM-independent, but a definition of
   jitterlispvm_destroy_mutable_routine is provided below as a macro, for cosmetic
   reasons. */


/* Mutable routines: code generation C API.
 * ************************************************************************** */

/* This is the preferred way of adding a new VM instruction to a pointed
   routine, more efficient than jitterlispvm_mutable_routine_append_instruction_name
   even if only usable when the VM instruction opcode is known at compile time.
   The unspecialized instruction name must be explicitly mangled by the user as
   per the rules in jitterc_mangle.c .  For example an instruction named foo_bar
   can be added to the routine pointed by p with any one of
     jitterlispvm_mutable_routine_append_instruction_name (p, "foo_bar");
   ,
     JITTERLISPVM_MUTABLE_ROUTINE_APPEND_INSTRUCTION (p, foo_ubar);
   , and
     JITTERLISPVM_MUTABLE_ROUTINE_APPEND_INSTRUCTION_ID 
        (p, jitterlispvm_meta_instruction_id_foo_ubar);
   .
   The string "foo_bar" is not mangled, but the token foo_ubar is. */
#define JITTERLISPVM_MUTABLE_ROUTINE_APPEND_INSTRUCTION(                 \
          routine_p, instruction_mangled_name_root)                  \
  do                                                                 \
    {                                                                \
      jitter_mutable_routine_append_meta_instruction                 \
         ((routine_p),                                               \
          jitterlispvm_meta_instructions                                 \
          + JITTER_CONCATENATE_TWO(jitterlispvm_meta_instruction_id_,    \
                                   instruction_mangled_name_root));  \
    }                                                                \
  while (false)

/* Append the unspecialized instruction whose id is given to the pointed routine.
   The id must be a case of enum jitterlispvm_meta_instruction_id ; such cases have
   a name starting with jitterlispvm_meta_instruction_id_ .
   This is slightly less convenient to use than JITTERLISPVM_MUTABLE_ROUTINE_APPEND_INSTRUCTION
   but more general, as the instruction id is allowed to be a non-constant C
   expression. */
#define JITTERLISPVM_MUTABLE_ROUTINE_APPEND_INSTRUCTION_ID(_jitter_routine_p,       \
                                                       _jitter_instruction_id)  \
  do                                                                            \
    {                                                                           \
      jitter_mutable_routine_append_instruction_id                              \
         ((_jitter_routine_p),                                                  \
          jitterlispvm_meta_instructions,                                           \
          JITTERLISPVM_META_INSTRUCTION_NO,                                         \
          (_jitter_instruction_id));                                            \
    }                                                                           \
  while (false)

/* This is the preferred way of appending a register argument to the instruction
   being added to the pointed routine, more convenient than directly using
   jitterlispvm_mutable_routine_append_register_id_parameter , even if only usable
   when the register class is known at compile time.  Here the register class is
   only provided as a letter, but both the routine pointer and the register
   index are arbitrary C expressions.
   For example, in
     JITTERLISPVM_MUTABLE_ROUTINE_APPEND_REGISTER_PARAMETER (p, r,
                                                         variable_to_index (x));
   the second macro argument "r" represents the register class named "r", and
   not the value of a variable named r. */
#define JITTERLISPVM_MUTABLE_ROUTINE_APPEND_REGISTER_PARAMETER(routine_p,     \
                                                           class_letter,  \
                                                           index)         \
  do                                                                      \
    {                                                                     \
      jitterlispvm_mutable_routine_append_register_parameter                  \
         ((routine_p),                                                    \
          & JITTER_CONCATENATE_TWO(jitterlispvm_register_class_,              \
                                   class_letter),                         \
          (index));                                                       \
    }                                                                     \
  while (false)




/* Routine unified API: initialization.
 * ************************************************************************** */

/* See the comments above in "Mutable routines: initialization", and the
   implementation of the unified routine API in <jitter/jitter-routine.h> . */

#define jitterlispvm_make_routine jitterlispvm_make_mutable_routine




/* Routine unified API: code generation C API.
 * ************************************************************************** */

/* See the comments above in "Mutable routines: code generation C API". */

#define JITTERLISPVM_ROUTINE_APPEND_INSTRUCTION  \
  JITTERLISPVM_MUTABLE_ROUTINE_APPEND_INSTRUCTION
#define JITTERLISPVM_ROUTINE_APPEND_INSTRUCTION_ID  \
  JITTERLISPVM_MUTABLE_ROUTINE_APPEND_INSTRUCTION_ID
#define JITTERLISPVM_ROUTINE_APPEND_REGISTER_PARAMETER  \
  JITTERLISPVM_MUTABLE_ROUTINE_APPEND_REGISTER_PARAMETER




/* Array: special-purpose data.
 * ************************************************************************** */

/* The Array is a convenient place to store special-purpose data, accessible in
   an efficient way from a VM routine.
   Every item in special-purpose data is thread-local. */

/* The special-purpose data struct.  Every Array contains one of these at unbiased
   offset JITTERLISPVM_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET from the unbiased
   beginning of the array.
   This entire struct is aligned to at least sizeof (jitter_int) bytes.  The
   entire struct is meant to be always accessed through a pointer-to-volatile,
   as its content may be altered from signal handlers and from different
   threads.  In particualar the user should use the macro
     JITTERLISPVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA
   defined below and the macros defined from it as accessors.
   VM code accessing special-purpose data for its own state should use
     JITTERLISPVM_SPECIAL_PURPOSE_STATE_DATA
   and the macros defined from it. */
struct jitter_special_purpose_state_data
{
  /* Notification fields.
   * ***************************************************************** */

  /* This is a Boolean flag, held as a word-sized datum so as to ensure
     atomicity in access.  It is also aligned to at least sizeof (jitter_int)
     bytes.
     Non-zero means that there is at least one notification pending, zero means
     that there are no notifications.  The flag specifies no other details: it
     is meant to be fast to check, with detailed information about each pending
     notification available elsewhere.
     It is the receiver's responsibility to periodically poll for notifications
     in application-specific "safe-points":
     A check can be inserted, for example, in all of these program points:
     a) at every backward branch;
     b) at every procedure entry;
     c) right after a call to each blocking primitive (as long as primitives
       can be interrupted).
     Safe-point checks are designed to be short and fast in the common case.  In
     the common case no action is required, and the VM routine should simply
     fall through.  If an action is required then control should branch off to a
     handler, where the user may implement the required behavior.
     It is mandatory that, as long as notifications can arrive, this field
     is reset to zero (when handling pending notifications) only by a thread
     running VM code in the state containing this struct.
     Other threads are allowed to set this to non-zero, in order to send a
     notification.  */
  jitter_int pending_notifications;

  /* Information about pending signal notifications.  If any signal is pending
     then pending_notifications must also be set, so that a notification check
     can always just quickly check pending_notifications, and then look at more
     details (including in pending_signal_notifications) only in the rare case
     of pending_notifications being true. */
  struct jitter_signal_notification *pending_signal_notifications;


  /* Profiling instrumentation fields.
   * ***************************************************************** */
  struct jitter_profile_runtime profile_runtime;
};




/* The Array and volatility.
 * ************************************************************************** */

/* Some fields of The Array, seen from VM code, are meant to be volatile, since
   they can be set by signal handlers or by other threads.  However it is
   acceptable to not see such changes immediately after they occur (notifications
   will get delayed, but not lost) and always accessing such data through a
   volatile struct is suboptimal.

   Non-VM code does need a volatile qualifier.

   Advanced dispatches already need a trick using inline assembly to make the
   base pointer (a biased pointer to The Array beginning) appear to
   spontaneously change beween instruction.  That is sufficient to express the
   degree of volatility required for this purpose.
   Simple dispatches, on targets where inline assembly may not be available at
   all, will use an actual volatile qualifier. */
#if defined (JITTER_DISPATCH_SWITCH)               \
    || defined (JITTER_DISPATCH_DIRECT_THREADING)
# define JITTERLISPVM_ARRAY_VOLATILE_QUALIFIER volatile
#elif defined (JITTER_DISPATCH_MINIMAL_THREADING)  \
      || defined (JITTER_DISPATCH_NO_THREADING)
# define JITTERLISPVM_ARRAY_VOLATILE_QUALIFIER /* nothing */
#else
# error "unknown dispatch: this should not happen"
#endif /* dispatch conditional */




/* Array element access: residuals, transfers, slow registers, and more.
 * ************************************************************************** */

/* In order to cover a wider range of addresses with simple base + register
   addressing the base does not necessarily point to the beginning of the Array;
   instead the base points to the beginning of the Array plus JITTER_ARRAY_BIAS
   bytes.
   FIXME: define the bias as a value appropriate to each architecture.  I think
   I should just move the definition to jitter-machine.h and provide a default
   here, in case the definition is missing on some architecture. */

/* FIXME: Horrible, horrible, horrible temporary workaround!

   This is a temporary workaround, very ugly and fragile, to compensate
   a limitation in jitter-specialize.c , which I will need to rewrite anyway.
   The problem is that jitter-specialize.c patches snippets to load non-label
   residuals in a VM-independent way based only on slow-register/memory residual
   indices, which is incorrect.  By using this particular bias I am cancelling
   that error.
   Test case, on a machine having only one register residual and a VM having just
     one fast register:
     [luca@moore ~/repos/jitter/_build/native-gcc-9]$ Q=bin/uninspired--no-threading; make $Q && echo 'mov 2, %r1' | libtool --mode=execute valgrind $Q --disassemble - --print-locations
   If this bias is wrong the slow-register accesses in mov/nR/%rR will use two
   different offsets, one for reading and another for writing.  With this
   workaround they will be the same.
   Good, with workadound (biased offset 0x0 from the base in %rbx):
    # 0x4a43d38: mov/nR/%rR 0x2, 0x20 (21 bytes):
        0x0000000004effb30 41 bc 02 00 00 00    	movl   $0x2,%r12d
        0x0000000004effb36 48 c7 43 00 20 00 00 00 	movq   $0x20,0x0(%rbx)
        0x0000000004effb3e 48 8b 13             	movq   (%rbx),%rdx
        0x0000000004effb41 4c 89 24 13          	movq   %r12,(%rbx,%rdx,1)
   Bad, with JITTER_ARRAY_BIAS defined as zero: first write at 0x0(%rbx)
                                                then read at 0x10(%rbx):
    # 0x4a43d38: mov/nR/%rR 0x2, 0x30 (22 bytes):
        0x0000000004effb30 41 bc 02 00 00 00    	movl   $0x2,%r12d
        0x0000000004effb36 48 c7 43 00 30 00 00 00 	movq   $0x30,0x0(%rbx)
        0x0000000004effb3e 48 8b 53 10          	movq   0x10(%rbx),%rdx
        0x0000000004effb42 4c 89 24 13          	movq   %r12,(%rbx,%rdx,1) */
#define JITTER_ARRAY_BIAS \
  (sizeof (struct jitter_special_purpose_state_data))
//#define JITTER_ARRAY_BIAS //0//(((jitter_int) 1 << 15))//(((jitter_int) 1 << 31))//0//0//16//0

/* Array-based globals are not implemented yet.  For the purpose of computing
   Array offsets I will say they are zero. */
#define JITTERLISPVM_GLOBAL_NO 0

/* Transfer registers are not implemented yet.  For the purpose of computing
   Array offsets I will say they are zero. */
#define JITTERLISPVM_TRANSFER_REGISTER_NO 0

/* Define macros holding offsets in bytes for the first global, memory residual
   and transfer register, from an initial Array pointer.
   In general we have to keep into account:
   - globals (word-sized);
   - special-purpose state data;
   - memory residuals (word-sized);
   - transfer registers (word-sized);
   - slow registers (jitterlispvm_any_register-sized and aligned).
   Notice that memory
   residuals (meaning residuals stored in The Array) are zero on dispatching
   modes different from no-threading.  This relies on
   JITTERLISPVM_MAX_MEMORY_RESIDUAL_ARITY , defined below, which in its turn depends
   on JITTERLISPVM_MAX_RESIDUAL_ARITY, which is machine-generated. */
#define JITTERLISPVM_FIRST_GLOBAL_UNBIASED_OFFSET  \
  0
#define JITTERLISPVM_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET  \
  (JITTERLISPVM_FIRST_GLOBAL_UNBIASED_OFFSET                     \
   + sizeof (jitter_int) * JITTERLISPVM_GLOBAL_NO)
#define JITTERLISPVM_FIRST_MEMORY_RESIDUAL_UNBIASED_OFFSET   \
  (JITTERLISPVM_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET   \
   + sizeof (struct jitter_special_purpose_state_data))
#define JITTERLISPVM_FIRST_TRANSFER_REGISTER_UNBIASED_OFFSET        \
  (JITTERLISPVM_FIRST_MEMORY_RESIDUAL_UNBIASED_OFFSET               \
   + sizeof (jitter_int) * JITTERLISPVM_MAX_MEMORY_RESIDUAL_ARITY)
#define JITTERLISPVM_FIRST_SLOW_REGISTER_UNBIASED_OFFSET          \
  JITTER_NEXT_MULTIPLE_OF_POSITIVE                            \
     (JITTERLISPVM_FIRST_TRANSFER_REGISTER_UNBIASED_OFFSET        \
      + sizeof (jitter_int) * JITTERLISPVM_TRANSFER_REGISTER_NO,  \
      sizeof (union jitterlispvm_any_register))

/* Expand to the offset of the special-purpose data struct from the Array
   biased beginning. */
#define JITTERLISPVM_SPECIAL_PURPOSE_STATE_DATA_OFFSET       \
  (JITTERLISPVM_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET   \
   - JITTER_ARRAY_BIAS)

/* Given an expression evaluating to the Array unbiased beginning, expand to
   an expression evaluating to a pointer to its special-purpose data.
   This is convenient for accessing special-purpose data from outside the
   state -- for example, to set the pending notification flag for another
   thread.
   There are two versions of this feature:
     JITTERLISPVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA
   is meant to be used to access state data for some other thread, or in
   general out of VM code.
     JITTERLISPVM_OWN_SPECIAL_PURPOSE_STATE_DATA
   is for VM code accessing its own special-purpose data. */
#define JITTERLISPVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA_PRIVATE(qualifier,      \
                                                             array_address)  \
  ((qualifier struct jitter_special_purpose_state_data *)                    \
   (((char *) (array_address))                                               \
    + JITTERLISPVM_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET))
#define JITTERLISPVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA(array_address)       \
  JITTERLISPVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA_PRIVATE (volatile,         \
                                                        (array_address))
#define JITTERLISPVM_OWN_SPECIAL_PURPOSE_STATE_DATA          \
  JITTERLISPVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA_PRIVATE   \
     (JITTERLISPVM_ARRAY_VOLATILE_QUALIFIER,                 \
      ((char *) jitter_array_base) - JITTER_ARRAY_BIAS)

/* Given a state pointer, expand to an expression evaluating to a pointer to
   the state's special-purpose data.  This is meant for threads accessing
   other threads' special-purpose data, typically to set notifications. */
#define JITTERLISPVM_STATE_TO_SPECIAL_PURPOSE_STATE_DATA(state_p)  \
  (JITTERLISPVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA                \
     ((state_p)->jitterlispvm_state_backing.jitter_array))

/* Given a state pointer, expand to an expression evaluating to the
   pending_notification field for the state as an l-value.  This is meant for
   threads sending notifications to other threads. */
#define JITTERLISPVM_STATE_TO_PENDING_NOTIFICATIONS(state_p)   \
  (JITTERLISPVM_STATE_TO_SPECIAL_PURPOSE_STATE_DATA (state_p)  \
     ->pending_notifications)

/* Given a state pointer and a signal, expand to an l-value evaluating to a the
   pending field of the struct jitter_signal_notification element for the given
   signal in the pointed state.  This is meant for threads sending signal
   notifications to other threads and for C handler function. */
#define JITTERLISPVM_STATE_AND_SIGNAL_TO_PENDING_SIGNAL_NOTIFICATION(state_p,    \
                                                                 signal_id)  \
  (((JITTERLISPVM_STATE_TO_SPECIAL_PURPOSE_STATE_DATA (state_p)                   \
       ->pending_signal_notifications)                                        \
    + (signal_id))->pending)


/* Expand to the offset of the i-th register of class c in bytes from the Array
   beginning.
   The c argument must be a literal C (one-character) identifier.
   The i argument should always be a compile-time constant for performance, and
   it is in generated code.
   The i-th c-class register must be slow, otherwise the offset will be
   incorrect -- in fact fast registers are, hopefully, not in memory at all.

   Slow registers come in the Array ordered first by index, then by class.  For
   example if there are three classes "r" with 4 fast registers, "f" with 7 fast
   registers and "q" with 2 fast registers, slow registers can be accessed in
   this order:
     r4, f7, q2, r5, r8, q3, r6, r9, q4, and so on.
   Each contiguous group of slow registers spanning every class and starting
   from the first class (here for example <r5, r6, q3>) is called a "rank".
   This organization is convenient since changing the number of slow registers
   doesn't invalidate any offset computed in the past: the Array can simply be
   resized and its base pointer updated, without changing the code accessing it.

   This relies on macro such as JITTERLISPVM_REGISTER_CLASS_NO and
   JITTERLISPVM_REGISTER_?_FAST_REGISTER_NO and , defined below in machine-generated
   code. */
#define JITTERLISPVM_SLOW_REGISTER_UNBIASED_OFFSET(c, i)                     \
  (JITTERLISPVM_FIRST_SLOW_REGISTER_UNBIASED_OFFSET                          \
   + (sizeof (union jitterlispvm_any_register)                               \
      * (JITTERLISPVM_REGISTER_CLASS_NO                                      \
         * ((i) - JITTER_CONCATENATE_THREE(JITTERLISPVM_REGISTER_, c,        \
                                           _FAST_REGISTER_NO))           \
         + JITTER_CONCATENATE_THREE(JITTERLISPVM_REGISTER_, c, _CLASS_ID))))

/* Expand to the offset of the i-th register of class c in bytes from the base,
   keeping the bias into account. */
#define JITTERLISPVM_SLOW_REGISTER_OFFSET(c, i)                              \
  (JITTERLISPVM_SLOW_REGISTER_UNBIASED_OFFSET(c, i) - JITTER_ARRAY_BIAS)

/* Expand to the Array size in bytes, assuming the given number of slow
   registers per class.  This is an allocation size, ignoring the bias. */
#define JITTERLISPVM_ARRAY_SIZE(slow_register_per_class_no)                  \
  (JITTERLISPVM_FIRST_SLOW_REGISTER_UNBIASED_OFFSET                          \
   + (sizeof (union jitterlispvm_any_register)                               \
      * JITTERLISPVM_REGISTER_CLASS_NO                                       \
      * (slow_register_per_class_no)))




/* Residual access.
 * ************************************************************************** */

/* How many residuals we can have at most in memory, which is to say,
   without counting residuals kept in reserved registers.

   Implementation note: it would be wrong here to use a CPP conditional based on
   the value of JITTERLISPVM_MAX_RESIDUAL_ARITY , as I was doing in a preliminary
   version.  That lead to a tricky bug, since JITTERLISPVM_MAX_RESIDUAL_ARITY ,
   which is defined below but is not yet available here, simply counted as 0
   for the purposes of evaluating the CPP condititional. */
#ifdef JITTER_DISPATCH_NO_THREADING
  /* We are using no-threading dispatch.  If there are no more residuals
     than reserved residual registers then we never need to keep any in
     memory.  Otherwise we need to keep as many residuals in memory as the
     total number of residuals minus how many registers are reserved for
     them. */
# define JITTERLISPVM_MAX_MEMORY_RESIDUAL_ARITY                          \
    ((JITTERLISPVM_MAX_RESIDUAL_ARITY <= JITTER_RESIDUAL_REGISTER_NO)    \
     ? 0                                                             \
     : (JITTERLISPVM_MAX_RESIDUAL_ARITY - JITTER_RESIDUAL_REGISTER_NO))
#else // Not no-threading.
  /* No registers are reserved for residuals in this dispatch; even if
     in fact all residuals are memory residuals they don't count here, since
     residuals are not held in The Array in this dispatch. */
# define JITTERLISPVM_MAX_MEMORY_RESIDUAL_ARITY  \
  0
#endif // #ifdef JITTER_DISPATCH_NO_THREADING

#ifdef JITTER_DISPATCH_NO_THREADING
/* Expand to the offset from the base, in bytes, of the i-th residual.  The
   given index must be greater than or equal to JITTER_RESIDUAL_REGISTER_NO;
   residuals with indices lower than that number are not stored in The Array
   at all.
   This is not useful with any of the other dispatches, where residuals
   directly follow each VM instruction opcode or thread.  For good performance i
   should always be a compile-time constant, as it is in machine-generated
   code.
   Residuals always have the size of a jitter word, even if some register class
   may be wider. */
/* FIXME: if later I use a different policy than simply checking
   JITTER_RESIDUAL_REGISTER_NO to decide how many residuals to keep in
   registers, then I have to change this or meet very nasty bugs. */
# define JITTERLISPVM_RESIDUAL_UNBIASED_OFFSET(i)                      \
    (JITTERLISPVM_FIRST_MEMORY_RESIDUAL_UNBIASED_OFFSET                \
     + (sizeof (jitter_int) * (i - JITTER_RESIDUAL_REGISTER_NO)))
# define JITTERLISPVM_RESIDUAL_OFFSET(i)  \
    (JITTERLISPVM_RESIDUAL_UNBIASED_OFFSET(i) - JITTER_ARRAY_BIAS)
#endif // #ifdef JITTER_DISPATCH_NO_THREADING



/* Mutable routine text frontend.
 * ************************************************************************** */

/* An enum type. */
#define jitterlispvm_routine_edit_status  \
  jitter_routine_edit_status
/* Cases for an enum type. */
#define jitterlispvm_routine_edit_status_success  \
  jitter_routine_edit_status_success
#define jitterlispvm_routine_edit_status_label_defined_twice  \
  jitter_routine_edit_status_label_defined_twice
#define jitterlispvm_routine_edit_status_invalid_instruction  \
  jitter_routine_edit_status_invalid_instruction
#define jitterlispvm_routine_edit_status_invalid_register  \
  jitter_routine_edit_status_invalid_register
#define jitterlispvm_routine_edit_status_register_class_mismatch  \
  jitter_routine_edit_status_register_class_mismatch
#define jitterlispvm_routine_edit_status_nonexisting_register_class  \
  jitter_routine_edit_status_nonexisting_register_class
#define jitterlispvm_routine_edit_status_invalid_parameter_kind  \
  jitter_routine_edit_status_invalid_parameter_kind
#define jitterlispvm_routine_edit_status_too_many_parameters  \
  jitter_routine_edit_status_too_many_parameters
#define jitterlispvm_routine_edit_status_last_instruction_incomplete  \
  jitter_routine_edit_status_last_instruction_incomplete
#define jitterlispvm_routine_edit_status_other_parse_error  \
  jitter_routine_edit_status_other_parse_error

/* Given a parse status of type enum jitter_routine_edit_status return its
   written C representation. */
#define jitterlispvm_routine_edit_status_to_string  \
  jitter_routine_edit_status_to_string

/* The name of the struct returned by parsers. */
#define jitterlispvm_routine_parse_error  \
  jitter_routine_parse_error

/* The name of the function destroying a pointer to jitterlispvm_routine_parse_error
   , returned by routine parsers in case of error. */
#define jitterlispvm_routine_parse_error_destroy  \
  jitter_routine_parse_error_destroy

/* Parse VM code from the given file or string into the pointed VM routine,
   which is allowed but not required to be empty.
   These are simple wrappers around functions implemented in the Bison file. */
struct jitterlispvm_routine_parse_error *
jitterlispvm_parse_mutable_routine_from_file_star (FILE *input_file,
                                               struct jitter_mutable_routine *p)
  __attribute__ ((nonnull (1, 2)));
struct jitterlispvm_routine_parse_error *
jitterlispvm_parse_mutable_routine_from_file (const char *input_file_name,
                                          struct jitter_mutable_routine *p)
  __attribute__ ((nonnull (1, 2)));
struct jitterlispvm_routine_parse_error *
jitterlispvm_parse_mutable_routine_from_string (const char *string,
                                            struct jitter_mutable_routine *p)
  __attribute__ ((nonnull (1, 2)));




/* Unified routine text frontend.
 * ************************************************************************** */

/* The C wrappers for the ordinary API can be reused for the unified API, since
   it internally works with mutable routines. */
#define jitterlispvm_parse_routine_from_file_star  \
  jitterlispvm_parse_mutable_routine_from_file_star
#define jitterlispvm_parse_routine_from_file  \
  jitterlispvm_parse_mutable_routine_from_file
#define jitterlispvm_parse_routine_from_string  \
  jitterlispvm_parse_mutable_routine_from_string




/* Machine-generated data structures.
 * ************************************************************************** */

/* Declare a few machine-generated data structures, which together define a VM. */

/* Threads or pointers to native code blocks of course don't exist with
   switch-dispatching. */
#ifndef JITTER_DISPATCH_SWITCH
/* Every possible thread, indexed by enum jitter_specialized_instruction_opcode .
   This is used at specialization time, and the user shouldn't need to touch
   it. */
extern const jitter_thread *
jitterlispvm_threads;

/* VM instruction end label.  These are not all reachable at run time, but
   having them in a global array might prevent older GCCs from being too clever
   in reordering blocks. */
extern const jitter_thread *
jitterlispvm_thread_ends;

/* The size, in chars, of each thread's native code.  The elements are in the
   same order of jitterlispvm_threads.  Sizes could conceptually be of type size_t ,
   but in order to be defensive I'm storing pointer differences as signed
   values, so that we may catch compilation problems: if any VM instruction end
   *precedes* its VM instruction beginning, then the compiler has reordered
   labels, which would have disastrous effects with replicated code. */
extern const long *
jitterlispvm_thread_sizes;
#endif // #ifndef JITTER_DISPATCH_SWITCH

/* This is defined in the machine-generated vm/meta-instructions.c . */
extern struct jitter_hash_table
jitterlispvm_meta_instruction_hash;

/* An array specifying every existing meta-instruction, defined in the order of
   enum jitterlispvm_meta_instruction_id .  This is defined in vm/meta-instructions.c ,
   which is machine-generated. */
extern const struct jitter_meta_instruction
jitterlispvm_meta_instructions [];

/* An array whose indices are specialised instruction opcodes, and
   whose elements are the corresponding unspecialised instructions
   opcodes -- or -1 when there is no mapping mapping having */
extern const int
jitterlispvm_specialized_instruction_to_unspecialized_instruction [];

/* How many residual parameters each specialized instruction has.  The
   actual array definition is machine-generated. */
extern const size_t
jitterlispvm_specialized_instruction_residual_arities [];

/* An array of bitmasks, one per specialized instruction.  Each bitmask holds
   one bit per residual argument, counting from the least significant (the first
   residual arg maps to element & (1 << 0), the second to element & (1 << 1),
   and so on).
   Each bit is 1 if and only if the corresponding residual argument is a label
   or a fast label.
   Only residual arguments are counted: for example a specialized instruction
   foo_n1_lR_r2 would have a mask with the *first* bit set. */
extern const unsigned long // FIXME: possibly use a shorter type when possible
jitterlispvm_specialized_instruction_label_bitmasks [];

/* Like jitterlispvm_specialized_instruction_label_bitmasks , but for fast labels
   only.
   The actual definition is conditionalized so as to appear only when
   needed according to the dispatch. */
extern const unsigned long // FIXME: possibly use a shorter type when possible
jitterlispvm_specialized_instruction_fast_label_bitmasks [];

/* An array of booleans in which each element is true iff the specialized
   instruction whose opcode is the index is relocatable. */
extern const bool
jitterlispvm_specialized_instruction_relocatables [];

/* An array of booleans in which each element is true iff the specialized
   instruction whose opcode is the index is a caller. */
extern const bool
jitterlispvm_specialized_instruction_callers [];

/* An array of booleans in which each element is true iff the specialized
   instruction whose opcode is the index is a callee. */
extern const bool
jitterlispvm_specialized_instruction_callees [];

/* This big array of strings contains the name of each specialized instruction,
   in the order of enum jitterlispvm_specialized_instruction_opcode . */
extern const char* const
jitterlispvm_specialized_instruction_names [];


/* A pointer to a struct containing const pointers to the structures above, plus
   sizes; there will be only one instance of this per VM, machine-generated.
   Each program data structure contains a pointer to that instance, so that
   VM-independent functions, given a program, will have everything needed to
   work.  The one instance of struct jitter_vm for the jitterlispvm VM. */
extern struct jitter_vm * const
jitterlispvm_vm;

/* A pointer to a struct containing VM-specific parameters set in part when
   calling jitterc and in part when compiling the generated C code, such as the
   dispatch and the number of fast registers.  The data is fully
   initialized only after a call to jitterlispvm_initialize . */
extern const
struct jitter_vm_configuration * const
jitterlispvm_vm_configuration;




/* Compatibility macros.
 * ************************************************************************** */

/* It is convenient, for future extensibility, to expose an interface in which
   some VM-independent functions and data structures actually look as if they
   were specific to the user VM. */

/* What the user refers to as struct jitterlispvm_mutable_routine is actually a
   struct jitter_mutable_routine , whose definition is VM-independent. */
#define jitterlispvm_mutable_routine jitter_mutable_routine

/* Same for executable routines. */
#define jitterlispvm_executable_routine jitter_executable_routine

/* Same for unified routines. */
#define jitterlispvm_routine jitter_routine

/* Destroy a non-executable routine (routine initialization is actually
   VM-specific). */
#define jitterlispvm_destroy_mutable_routine jitter_destroy_mutable_routine

/* Destroy a unified routine. */
#define jitterlispvm_destroy_routine jitter_destroy_routine

/* Pin a unified routine. */
#define jitterlispvm_pin_routine jitter_pin_routine

/* Unpin a unified routine. */
#define jitterlispvm_unpin_routine jitter_unpin_routine

/* Print VM configuration. */
#define jitterlispvm_print_vm_configuration jitter_print_vm_configuration

/* Generic routine construction API. */
#define jitterlispvm_label \
  jitter_label
#define jitterlispvm_fresh_label \
  jitter_fresh_label

/* Mutable routine option API. */
#define jitterlispvm_set_mutable_routine_option_slow_literals_only \
  jitter_set_mutable_routine_option_slow_literals_only
#define jitterlispvm_set_mutable_routine_option_slow_registers_only \
  jitter_set_mutable_routine_option_slow_registers_only
#define jitterlispvm_set_mutable_routine_option_slow_literals_and_registers_only \
  jitter_set_mutable_routine_option_slow_literals_and_registers_only
#define jitterlispvm_set_mutable_routine_option_add_final_exitvm \
  jitter_set_mutable_routine_option_add_final_exitvm
#define jitterlispvm_set_mutable_routine_option_optimization_rewriting \
  jitter_set_mutable_routine_option_optimization_rewriting

/* Printing and disassembling: ordinary API. */
#define jitterlispvm_mutable_routine_print \
  jitter_mutable_routine_print
#define jitterlispvm_executable_routine_disassemble \
  jitter_executable_routine_disassemble

/* Mutable routine construction API: unsafe API */
#define jitterlispvm_mutable_routine_append_instruction_name \
  jitter_mutable_routine_append_instruction_name
#define jitterlispvm_mutable_routine_append_meta_instruction \
  jitter_mutable_routine_append_meta_instruction
#define jitterlispvm_mutable_routine_append_label \
  jitter_mutable_routine_append_label
#define jitterlispvm_mutable_routine_append_symbolic_label \
  jitter_mutable_routine_append_symbolic_label
#define jitterlispvm_mutable_routine_append_register_parameter \
  jitter_mutable_routine_append_register_parameter
#define jitterlispvm_mutable_routine_append_symbolic_register_parameter \
  jitter_mutable_routine_append_symbolic_register_parameter
#define jitterlispvm_mutable_routine_append_literal_parameter \
  jitter_mutable_routine_append_literal_parameter
#define jitterlispvm_mutable_routine_append_signed_literal_parameter \
  jitter_mutable_routine_append_signed_literal_parameter
#define jitterlispvm_mutable_routine_append_unsigned_literal_parameter \
  jitter_mutable_routine_append_unsigned_literal_parameter
#define jitterlispvm_mutable_routine_append_pointer_literal_parameter \
  jitter_mutable_routine_append_pointer_literal_parameter
#define jitterlispvm_mutable_routine_append_label_parameter \
  jitter_mutable_routine_append_label_parameter
#define jitterlispvm_mutable_routine_append_symbolic_label_parameter \
  jitter_mutable_routine_append_symbolic_label_parameter

/* Mutable routine construction API: safe API */
#define jitterlispvm_mutable_routine_append_label_safe  \
  jitter_mutable_routine_append_label_safe
#define jitterlispvm_mutable_routine_append_symbolic_label_safe  \
  jitter_mutable_routine_append_symbolic_label_safe
#define jitterlispvm_mutable_routine_append_instruction_name_safe  \
  jitter_mutable_routine_append_instruction_name_safe
#define jitterlispvm_mutable_routine_append_instruction_id_safe  \
  jitter_mutable_routine_append_instruction_id_safe
#define jitterlispvm_mutable_routine_append_meta_instruction_safe  \
  jitter_mutable_routine_append_meta_instruction_safe
#define jitterlispvm_mutable_routine_append_literal_parameter_safe  \
  jitter_mutable_routine_append_literal_parameter_safe
#define jitterlispvm_mutable_routine_append_signed_literal_parameter_safe  \
  jitter_mutable_routine_append_signed_literal_parameter_safe
#define jitterlispvm_mutable_routine_append_unsigned_literal_parameter_safe  \
  jitter_mutable_routine_append_unsigned_literal_parameter_safe
#define jitterlispvm_mutable_routine_append_pointer_literal_parameter_safe  \
  jitter_mutable_routine_append_pointer_literal_parameter_safe
#define jitterlispvm_mutable_routine_append_register_parameter_safe  \
  jitter_mutable_routine_append_register_parameter_safe
#define jitterlispvm_mutable_routine_append_symbolic_register_parameter_safe  \
  jitter_mutable_routine_append_symbolic_register_parameter_safe
#define jitterlispvm_mutable_routine_append_symbolic_label_parameter_safe  \
  jitter_mutable_routine_append_symbolic_label_parameter_safe
#define jitterlispvm_mutable_routine_append_label_parameter_safe  \
  jitter_mutable_routine_append_label_parameter_safe

/* Mutable routine destruction. */
#define jitterlispvm_destroy_executable_routine \
  jitter_destroy_executable_routine

/* Making executable routines from mutable routines. */
#define jitterlispvm_make_executable_routine \
  jitter_make_executable_routine

/* Unified routine option API. */
#define jitterlispvm_set_routine_option_slow_literals_only \
  jitter_set_mutable_routine_option_slow_literals_only
#define jitterlispvm_set_routine_option_slow_registers_only \
  jitter_set_mutable_routine_option_slow_registers_only
#define jitterlispvm_set_routine_option_slow_literals_and_registers_only \
  jitter_set_mutable_routine_option_slow_literals_and_registers_only
#define jitterlispvm_set_routine_option_add_final_exitvm \
  jitter_set_mutable_routine_option_add_final_exitvm
#define jitterlispvm_set_routine_option_optimization_rewriting \
  jitter_set_mutable_routine_option_optimization_rewriting

/* Printing and disassembling: unified API.  These do not follow the pattern of
   the rest: wrapped identifiers here are the names of C functions specific to
   the unified API */
#define jitterlispvm_routine_print \
  jitter_routine_print
#define jitterlispvm_routine_disassemble \
  jitter_routine_disassemble

/* Unified routine construction API. */
#define jitterlispvm_routine_append_instruction_name \
  jitter_mutable_routine_append_instruction_name
#define jitterlispvm_routine_append_meta_instruction \
  jitter_mutable_routine_append_meta_instruction
#define jitterlispvm_routine_append_label \
  jitter_mutable_routine_append_label
#define jitterlispvm_routine_append_symbolic_label \
  jitter_mutable_routine_append_symbolic_label
#define jitterlispvm_routine_append_register_parameter \
  jitter_mutable_routine_append_register_parameter
#define jitterlispvm_routine_append_literal_parameter \
  jitter_mutable_routine_append_literal_parameter
#define jitterlispvm_routine_append_signed_literal_parameter \
  jitter_mutable_routine_append_signed_literal_parameter
#define jitterlispvm_routine_append_unsigned_literal_parameter \
  jitter_mutable_routine_append_unsigned_literal_parameter
#define jitterlispvm_routine_append_pointer_literal_parameter \
  jitter_mutable_routine_append_pointer_literal_parameter
#define jitterlispvm_routine_append_label_parameter \
  jitter_mutable_routine_append_label_parameter
#define jitterlispvm_routine_append_symbolic_label_parameter \
  jitter_mutable_routine_append_symbolic_label_parameter

/* Mutable routine destruction. */
#define jitterlispvm_destroy_routine                                           \
  /* This does not follow the pattern of the rest: the wrapped identifier  \
     here is the name of a C function specific to the unified API. */      \
  jitter_destroy_routine

/* The unified API has no facility to explicitly make executable routines: their
   very existence is hidden.  For this reason some of the macros above, such
   jitterlispvm_make_executable_routine, have no unified counterpart here. */

/* Defects and replacements. */
void
jitterlispvm_defect_print_summary (jitter_print_context cx)
  __attribute__ ((nonnull (1)));
void
jitterlispvm_defect_print (jitter_print_context cx,
                       unsigned indentation_column_no)
  __attribute__ ((nonnull (1)));
void
jitterlispvm_defect_print_replacement_table (jitter_print_context cx,
                                         unsigned indentation_column_no)
  __attribute__ ((nonnull (1)));

/* Profiling.  Apart from jitterlispvm_state_profile, which returns a pointer to
   the profile within a pointed state structure, everything else here has the
   same API as the functionality in jitter/jitter-profile.h , without the VM
   pointer.
   Notice that this API does nothing useful onless at least one of the CPP
   macros JITTERLISPVM_PROFILE_COUNT or JITTERLISPVM_PROFILE_SAMPLE is defined. */
#define jitterlispvm_profile_runtime  \
  jitter_profile_runtime /* the struct name */
#define jitterlispvm_profile  \
  jitter_profile /* the struct name */
// FIXME: no: distinguish between struct jitter_profile_runtime and its user-friendly variant
struct jitter_profile_runtime *
jitterlispvm_state_profile_runtime (struct jitterlispvm_state *s)
  __attribute__ ((returns_nonnull, nonnull (1)));
struct jitterlispvm_profile_runtime*
jitterlispvm_profile_runtime_make (void)
  __attribute__ ((returns_nonnull));
void
jitterlispvm_profile_runtime_destroy (struct jitterlispvm_profile_runtime *p)
  __attribute__ ((nonnull (1)));
#define jitterlispvm_profile_destroy jitter_profile_destroy
void
jitterlispvm_profile_runtime_clear (struct jitterlispvm_profile_runtime *p)
  __attribute__ ((nonnull (1)));
void
jitterlispvm_profile_runtime_merge_from (struct jitterlispvm_profile_runtime *to,
                                     const struct jitterlispvm_profile_runtime *from)
  __attribute__ ((nonnull (1, 2)));
void
jitterlispvm_profile_runtime_merge_from_state (struct jitterlispvm_profile_runtime *to,
                                   const struct jitterlispvm_state *from_state)
  __attribute__ ((nonnull (1, 2)));
struct jitterlispvm_profile *
jitterlispvm_profile_unspecialized_from_runtime
   (const struct jitterlispvm_profile_runtime *p)
  __attribute__ ((returns_nonnull, nonnull (1)));
struct jitterlispvm_profile *
jitterlispvm_profile_specialized_from_runtime (const struct jitterlispvm_profile_runtime
                                           *p)
  __attribute__ ((returns_nonnull, nonnull (1)));
void
jitterlispvm_profile_runtime_print_unspecialized
   (jitter_print_context ct,
    const struct jitterlispvm_profile_runtime *p)
  __attribute__ ((nonnull (1, 2)));
void
jitterlispvm_profile_runtime_print_specialized (jitter_print_context ct,
                                            const struct jitterlispvm_profile_runtime
                                            *p)
  __attribute__ ((nonnull (1, 2)));




/* Register class types.
 * ************************************************************************** */

/* Return a pointer to a statically allocated register class descriptor, given
   the register class character, or NULL if the character does not represent a
   valid register class.

   A constant array indexed by a character would have been more efficient, but
   relying on character ordering is not portable, at least in theory.  A
   non-constant array could be initialized in a portable way, but that would
   probably not be worth the trouble. */
const struct jitter_register_class *
jitterlispvm_register_class_character_to_register_class (char c)
  __attribute__ ((pure));


/* A constant array of constant pointers to every existing register class
   descriptor, ordered by class id; each pointer within the array refers the
   only existing class descriptor for its class.  The number of elements is
   JITTERLISPVM_REGISTER_CLASS_NO , but that is not declared because the definition
   of JITTERLISPVM_REGISTER_CLASS_NO comes later in generated code.

   This is useful when the user code enumerates every existing register class,
   particularly for debugging. */
extern const struct jitter_register_class * const
jitterlispvm_regiter_classes [];




/* Array re-allocation.
 * ************************************************************************** */

/* Make the Array in the pointed state large enough to accommodate the given
   number of slow reigsters per class, adjusting the Array pointer as needed
   and recording information about the new size in the state; change nothing
   if the array is already large enough.  Return the new base.
   For example passing 3 as the value of slow_register_no would make
   place for three slow registers per register class: if the current VM had two
   classes 'r' and 'f' than the function would ensure that the Array can hold
   three 'r' and three 'f' slow registers, independently from the number
   of fast 'r' or 'f' registers.
   Any new elements allocated in the Array are left uninitialized, but its old
   content remains valid. */
char *
jitterlispvm_make_place_for_slow_registers (struct jitterlispvm_state *s,
                                        jitter_int slow_register_no_per_class)
  __attribute__ ((noinline));




/* **************************************************************************
 * Evrything following this point is for internal use only.
 * ************************************************************************** */




/* Replacement tables.
 * ************************************************************************** */

/* It is harmless to declare these unconditionally, even if they only used when
   defect replacement is available.  See jitter/jitter-defect.h . */

/* The worst-case replacement table.  This is a global constant array, having
   one element per specialised instruction. */
extern const jitter_uint
jitterlispvm_worst_case_replacement_table [];

/* An array whose first defective_specialized_instruction_no elements contain
   the specialized_instruction_ids of defective instructions; the remaining
   elements are set to -1.  This array is initialised by
   jitter_fill_replacement_table . */
extern jitter_int
jitterlispvm_defective_specialized_instructions [];

/* The actual replacement table, to be filled at initialization time. */
extern jitter_uint
jitterlispvm_replacement_table [];

/* An array of specialized instruction ids, holding the specialized instruction
   ids of any non-replacement specialized instruction which is "call-related",
   which is to say any of caller, callee or returning.  These are useful for
   defect replacements, since any defect in even just one of them requires
   replacing them all. */
extern const jitter_uint
jitterlispvm_call_related_specialized_instruction_ids [];

/* The size of jitterlispvm_call_related_specialized_instruction_ids in elements. */
extern const jitter_uint
jitterlispvm_call_related_specialized_instruction_id_no;

/* An array of Booleans in which each element is true iff the specialized
   instruction whose opcode is the index is call-related. */
extern const bool
jitterlispvm_specialized_instruction_call_relateds [];




/* Instruction rewriter.
 * ************************************************************************** */

/* Try to apply each rewrite rule in order and run the first one that matches,
   if any, on the pointed program.  When a rule fires the following ones are not
   checked but if a rule, after removing the last few instructions, adds another
   one, the addition will trigger another rewrite in its turn, and so on until
   no more rewriting is possible.  The rewriting process is inherently
   recursive.

   The implementation of this function is machine-generated, but the user can
   add her own code in the rewriter-c block, which ends up near the beginning of
   this function body, right after JITTTER_REWRITE_FUNCTION_PROLOG_ .  The
   formal argument seen from the body is named jitter_mutable_routine_p .

   Rationale: the argument is named differently in the body in order to keep
   the namespace conventions and, more importantly, to encourage the user to
   read this comment.

   The user must *not* append labels to the VM routines during rewriting: that
   would break it.  The user is responsible for destroying any instruction she
   removes, including their arguments.  The user can assume that
   jitter_rewritable_instruction_no is strictly greater than zero. */
void
jitterlispvm_rewrite (struct jitter_mutable_routine *jitter_mutable_routine_p);




/* Program points at run time in executable routines.
 * ************************************************************************** */

/* Provide a nice name for a program point type which looks VM-dependent. */
typedef jitter_program_point
jitterlispvm_program_point;

/* Again, provide a VM-dependent alias for an actually VM-independent macro. */
#define JITTERLISPVM_EXECUTABLE_ROUTINE_BEGINNING(_jitter_executable_routine_ptr)  \
  JITTER_EXECUTABLE_ROUTINE_BEGINNING(_jitter_executable_routine_ptr)




/* Program points at run time in routines: unified routine API.
 * ************************************************************************** */

/* Like JITTERLISPVM_EXECUTABLE_ROUTINE_BEGINNING for the unified routine API. */
#define JITTERLISPVM_ROUTINE_BEGINNING(_jitter_routine)                \
  JITTER_EXECUTABLE_ROUTINE_BEGINNING                              \
     (jitter_routine_make_executable_if_needed (_jitter_routine))



/* Executing code from an executable routine.
 * ************************************************************************** */

/* Make sure that the pointed state has enough slow registers to run the pointed
   executable routine; if that is not the case, allocate more slow registers. */
void
jitterlispvm_ensure_enough_slow_registers_for_executable_routine
   (const struct jitter_executable_routine *er, struct jitterlispvm_state *s)
  __attribute__ ((nonnull (1, 2)));

/* Run VM code starting from the given program point (which must belong to some
   executable routine), in the pointed VM state.
   Return the VM execution state at the end.

   Since no executable routine is given this cannot automatically guarantee that
   the slow registers in the pointed state are in sufficient number; it is the
   user's responsibility to check, if needed.

   This function is also usable with the unified routine API. */
enum jitterlispvm_exit_status
jitterlispvm_branch_to_program_point (jitterlispvm_program_point p,
                                  struct jitterlispvm_state *s)
  __attribute__ ((nonnull (1, 2)));

/* Run VM code starting from the beginning of the pointed executable routine,
   in the pointed VM state.
   Return the VM execution state at the end.
   This does ensure that the slow registers in the pointed state are in
   sufficient number, by calling jitterlispvm_ensure_enough_slow_registers_for .
   This function is slightly less efficient than
   jitterlispvm_branch_to_program_point , and jitterlispvm_branch_to_program_point
   should be preferred in contexts where C code repeatedly calls VM code. */
enum jitterlispvm_exit_status
jitterlispvm_execute_executable_routine (const struct jitter_executable_routine *er,
                                     struct jitterlispvm_state *s)
  __attribute__ ((nonnull (1, 2)));




/* Executing code: unified routine API.
 * ************************************************************************** */

/* Like jitterlispvm_ensure_enough_slow_registers_for_executable_routine , with the
   unified API. */
void
jitterlispvm_ensure_enough_slow_registers_for_routine
   (jitter_routine r, struct jitterlispvm_state *s)
  __attribute__ ((nonnull (1, 2)));

/* jitterlispvm_branch_to_program_point , declared above, is also usable with the
   unified routine API. */

/* Like jitterlispvm_execute_executable_routine, for a unified routine. */
enum jitterlispvm_exit_status
jitterlispvm_execute_routine (jitter_routine r,
                          struct jitterlispvm_state *s)
  __attribute__ ((nonnull (1, 2)));




/* VM exit status.
 * ************************************************************************** */

/* A value of this type is returned by a VM after execution, and the last
   returned value is also held in the VM state in order to make consistency
   checks. */
enum jitterlispvm_exit_status
  {
    /* This state has never been used for execution.  This is the initial value
       within the state, and is never returned after execution. */
    jitterlispvm_exit_status_never_executed = 0,

    /* The state is being used in execution right now; this is never returned by
       the executor.  It is an error (checked for) to execute code with a VM
       state containing this exit status, which shows that there has been a
       problem -- likely VM code was exited via longjmp, skipping the proper
       cleanup. */
    jitterlispvm_exit_status_being_executed = 1,

    /* Some VM code has been executed.  It is now possible to execute more code
       (including the same code again) in the same state. */
    jitterlispvm_exit_status_exited = 2,

    /* Code execution has been interrupted for debugging, but can be resumed. */
    jitterlispvm_exit_status_debug = 3,
  };




/* Low-level debugging features relying on assembly: data locations.
 * ************************************************************************** */

/* Dump human-readable information about data locations to the given print
   context.
   This is a trivial VM-dependent wrapper around jitter_dump_data_locations,
   which does not require a struct jitter_vm pointer as input. */
void
jitterlispvm_dump_data_locations (jitter_print_context output)
  __attribute__ ((nonnull (1)));




/* Sample profiling: internal API.
 * ************************************************************************** */

/* The functions in this sections are used internally by vm2.c, only when
   sample-profiling is enabled.  In fact these functions are not defined at all
   otherwise. */

/* Initialise global sampling-related structures. */
// FIXME: no: distinguish struct jitter_profile_runtime and struct jitter_profile
void
jitterlispvm_profile_sample_initialize (void);

/* Begin sampling. */
void
jitterlispvm_profile_sample_start (struct jitterlispvm_state *state)
  __attribute__ ((nonnull (1)));

/* Stop sampling. */
void
jitterlispvm_profile_sample_stop (void);




/* User macros to access VM state data structures (out of VM instructions).
 * ************************************************************************** */

/* Notice that these macros are to be used out of VM instruction code blocks:
   for use within instructions see the alternative definitions of
   _JITTER_STATE_RUNTIME_FIELD and _JITTER_STATE_BACKING_FIELD in
   jitter-executor.h .  The alternative definitions are *not* compatible: the
   macros defined here have one more argument, the VM state structure. */

/* Given a VM state pointer and a state runtime field name expand to an l-value
   referring the named field in the given VM state runtime.
   This macro is not usable within VM instruction code blocks: see the commmnt
   above. */
#define JITTERLISPVM_STATE_RUNTIME_FIELD(state_p /* see the comment above */,  \
                                     field_name)                           \
  ((state_p)->_jitterlispvm_1TbNcL3hel_state_runtime.field_name)

/* Given a VM state pointer and a state backing field name expand to an l-value
   referring the named field in the given VM state backing.
   This macro is not usable within VM instruction code blocks: see the commmnt
   above. */
#define JITTERLISPVM_STATE_BACKING_FIELD(state_p /* see the comment above */,  \
                                     field_name)                           \
  ((state_p)->jitterlispvm_state_backing.field_name)




/* Machine-generated code.
 * ************************************************************************** */

/* What follows could be conceptually split into several generated header files,
   but having too many files would be inconvenient for the user to compile and
   link.  For this reason we generate a single header. */

/* User-specified code, early header part: beginning. */

#   include "jitterlisp.h"
  
/* If enabled, use my nonworking heap allocation stub.  This is useful to me,
   for playing with the still non-existent Jitter garbage collector and reason
   about its API. */
//#define JITTER_GC_STUB
  
/* User-specified code, early header part: end */

/* Configuration data for struct jitter_vm_configuration. */
#define JITTERLISPVM_VM_NAME JITTER_STRINGIFY(Jitterlispvm)
#define JITTERLISPVM_LOWER_CASE_PREFIX "jitterlispvm"
#define JITTERLISPVM_UPPER_CASE_PREFIX "JITTERLISPVM"
#define JITTERLISPVM_HASH_PREFIX "_jitterlispvm_1TbNcL3hel"
#define JITTERLISPVM_DISPATCH_HUMAN_READABLE \
  JITTER_DISPATCH_NAME_STRING
#define JITTERLISPVM_MAX_FAST_REGISTER_NO_PER_CLASS -1
#define JITTERLISPVM_MAX_NONRESIDUAL_LITERAL_NO -1


/* For each register class define the register type, a unique index, and the
   number of fast registers.  Indices are useful for computing slow register
   offsets.  For each register class declare a global register class
   descriptor, convenient to use when generating unspecialized instructions
   from the C API.*/
typedef
jitterlisp_object jitterlispvm_register_r;
#define JITTERLISPVM_REGISTER_r_CLASS_ID 0
#define JITTERLISPVM_REGISTER_r_FAST_REGISTER_NO 0
extern const struct jitter_register_class
jitterlispvm_register_class_r;

/* How many register classes we have. */
#define JITTERLISPVM_REGISTER_CLASS_NO  1

/* A union large enough to hold a register of any class, or a machine word. */
union jitterlispvm_any_register
{
  /* In any case the union must be at least as large as a machine word. */
  jitter_int jitter_unused_field;

  jitterlispvm_register_r r /* A r-class register */;
};

/* An enumeration of all jitterlispvm register classes. */
enum jitterlispvm_register_class_id
  {
    jitterlispvm_register_class_id_r = JITTERLISPVM_REGISTER_r_CLASS_ID,

    /* The number of register class ids, not valid as a class id itself. */
    jitterlispvm_register_class_id_no = JITTERLISPVM_REGISTER_CLASS_NO
  };

/* A macro expanding to a statement initialising a rank of slow
   registers.  The argument has type union jitterlispvm_any_register *
   and points to the first register in a rank. */
#define JITTERLISPVM_INITIALIZE_SLOW_REGISTER_RANK(rank) \
  do \
    { \
      union jitterlispvm_any_register *_jitter_rank __attribute__ ((unused)) \
        = (rank); \
      /* r-class registers need no initialisation. */ \
    } \
  while (false)


#ifndef JITTERLISPVM_STATE_H_
#define JITTERLISPVM_STATE_H_

//#include <jitter/jitter.h>

/* Early C code from the user for the state definition. */
/* End of the early C code from the user for the state definition. */

/* The VM state backing. */
struct jitterlispvm_state_backing
{
  /* The Array.  This initial pointer is kept in the backing, since it is
     not normally needed at run time.  By subtracting JITTER_ARRAY_BIAS from
     it (as a pointer to char) we get the base pointer. */
  char *jitter_array;

  /* How many slow registers per class the Array can hold, without being
     reallocated.  This number is always the same for evey class. */
  jitter_int jitter_slow_register_no_per_class;

  /* The initial VM program point.  This is not part of the runtime,
     in fact with no-threading dispatch there is not even a copy of this
     datum being kept up to date during execution, anywhere; this field
     serves to keep track of where execution should *continue* from at the
     next execution.  It will become more useful when debubbing is
     implemented. */
  jitterlispvm_program_point initial_program_point;

  /* The exit status. */
  enum jitterlispvm_exit_status exit_status;

  /* Stack backing data structures. */
  struct jitter_stack_backing jitter_stack_mainstack_backing;
  struct jitter_stack_backing jitter_stack_returnstack_backing;

  /* State backing fields added in C by the user. */

    /* Every VM state which is currently in use is linked within the list
       jitterlisp_used_states; every VM state which is *not* currently in use is
       linked within the list jitterlisp_unused_states.  Since each state is at
       any time only in one of the two lists, we can reuse the same pointer
       fields.
       See the "State pool" section in jitterlisp-eval-vm.h .
       The code for linking and unlinking states within the list is in
       jitterlisp-eval-vm.c ; it is not part of state initialisation or
       finalisation. */
    struct jitter_list_links pool_links;
  
  /* End of the state backing fields added in C by the user. */
};

/* The VM state runtime data structure, using memory from the VM state backing. */
struct jitterlispvm_state_runtime
{
  /* A link register for branch-and-link operations.  This field must *not*
     be accessed from user code, as it may not exist on all dispatches.
     It is only used internally for JITTER_PROCEDURE_PROLOG .

     With no-threading on arthitectures supporting procedures some
     hardware-dependent resource such as a designed register (general-
     purpose or not, reserved or not) or a stack location will be used
     instead of this, normally; however even with no-threading we need
     this for defect replacement: if any call-related instruction turns
     out to be defective they will all be replaced in order to keep their
     calling conventions compatible, and the replacement will use
     this. */
  union jitter_word _jitter_link;

  /* With recent GCC versions (as of Summer 2017) the *last* declared fields
     are the most likely to be allocated in registers; this is why VM registers
     are in reverse order here.  The first few fast registers will be the "fastest"
     ones, allocated in hardware registers; they may be followed by other fast
     fast allocated on the stack at known offsets, with intermediate performance; then
     come the slow registers.  In critical code the users should prefer a register with as
     small an index as possible for best performance. */

  /* Stack runtime data structures. */
  JITTER_STACK_TOS_DECLARATION(jitterlisp_object, mainstack);
  JITTER_STACK_NTOS_DECLARATION(jitterlisp_object, returnstack);

  /* State runtime fields added in C by the user. */

#ifdef JITTER_GC_STUB
    /* A pointer to the next free byte in the nursery.  Untagged. */
    char *allocation_next;

    /* The nursery allocation limit, untagged -- which is to say, the maximum
       valid address in the nursery plus 1.
       Notice that it is correct, if slightly conservative, to compare even a
       tagged pointer against this in an expression like
          new_tagged_pointer < allocation_limit
       , since a tagged pointer is always greater than or equal to its untagged
       counterpart. */
    char *allocation_limit;
#endif // #ifdef JITTER_GC_STUB
  
  /* End of the state runtime fields added in C by the user. */
};

/* A struct holding both the backing and the runtime part of the VM state. */
struct jitterlispvm_state
{
  /* Pointers to the previous and next VM state for this VM. */
  struct jitter_list_links links;

  /* Each state data structure contains its backing. */
  struct jitterlispvm_state_backing jitterlispvm_state_backing;

  /* Each state data structure contains its runtime data structures,
     which the compiler will try to keep in registers as far as
     possible.  Runtime structures are allowed to point to memory
     from the backing (which is particularly important for stacks),
     but the backing itself is not copied into registers at
     execution time.
     It is important for this identifier not to be directly used in
     user code, since at some points during execution the data stored
     struct field may be out of date.  In order to prevent this kind
     of mistakes this field has a hard-to-predict name. */
  struct jitterlispvm_state_runtime _jitterlispvm_1TbNcL3hel_state_runtime;
};
#endif // #ifndef JITTERLISPVM_STATE_H_
#ifndef JITTERLISPVM_META_INSTRUCTIONS_H_
#define JITTERLISPVM_META_INSTRUCTIONS_H_

enum jitterlispvm_meta_instruction_id
  {
    jitterlispvm_meta_instruction_id_at_mdepth_mto_mregister = 0,
    jitterlispvm_meta_instruction_id_branch = 1,
    jitterlispvm_meta_instruction_id_branch_mif_mfalse = 2,
    jitterlispvm_meta_instruction_id_branch_mif_mnon_mpositive = 3,
    jitterlispvm_meta_instruction_id_branch_mif_mnot_mless = 4,
    jitterlispvm_meta_instruction_id_branch_mif_mnot_mnull = 5,
    jitterlispvm_meta_instruction_id_branch_mif_mnull = 6,
    jitterlispvm_meta_instruction_id_branch_mif_mpositive = 7,
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mnon_mpositive = 8,
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mnon_mzero = 9,
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mnot_mnull = 10,
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mnull = 11,
    jitterlispvm_meta_instruction_id_branch_mif_mregister_mpositive = 12,
    jitterlispvm_meta_instruction_id_branch_mif_mtrue = 13,
    jitterlispvm_meta_instruction_id_call = 14,
    jitterlispvm_meta_instruction_id_call_mcompiled = 15,
    jitterlispvm_meta_instruction_id_call_mfrom_mc = 16,
    jitterlispvm_meta_instruction_id_canonicalize_mboolean = 17,
    jitterlispvm_meta_instruction_id_cdr_mregister = 18,
    jitterlispvm_meta_instruction_id_check_mclosure = 19,
    jitterlispvm_meta_instruction_id_check_mglobal_mdefined = 20,
    jitterlispvm_meta_instruction_id_check_min_marity = 21,
    jitterlispvm_meta_instruction_id_check_min_marity_m_malt = 22,
    jitterlispvm_meta_instruction_id_copy_mfrom_mliteral = 23,
    jitterlispvm_meta_instruction_id_copy_mfrom_mregister = 24,
    jitterlispvm_meta_instruction_id_copy_mto_mregister = 25,
    jitterlispvm_meta_instruction_id_drop = 26,
    jitterlispvm_meta_instruction_id_drop_mnip = 27,
    jitterlispvm_meta_instruction_id_dup = 28,
    jitterlispvm_meta_instruction_id_exitvm = 29,
    jitterlispvm_meta_instruction_id_fail = 30,
    jitterlispvm_meta_instruction_id_gc_mif_mneeded = 31,
    jitterlispvm_meta_instruction_id_heap_mallocate = 32,
    jitterlispvm_meta_instruction_id_literal_mto_mregister = 33,
    jitterlispvm_meta_instruction_id_nip = 34,
    jitterlispvm_meta_instruction_id_nip_mdrop = 35,
    jitterlispvm_meta_instruction_id_nip_mfive = 36,
    jitterlispvm_meta_instruction_id_nip_mfive_mdrop = 37,
    jitterlispvm_meta_instruction_id_nip_mfour = 38,
    jitterlispvm_meta_instruction_id_nip_mfour_mdrop = 39,
    jitterlispvm_meta_instruction_id_nip_mpush_mliteral = 40,
    jitterlispvm_meta_instruction_id_nip_mpush_mregister = 41,
    jitterlispvm_meta_instruction_id_nip_msix = 42,
    jitterlispvm_meta_instruction_id_nip_msix_mdrop = 43,
    jitterlispvm_meta_instruction_id_nip_mthree = 44,
    jitterlispvm_meta_instruction_id_nip_mthree_mdrop = 45,
    jitterlispvm_meta_instruction_id_nip_mtwo = 46,
    jitterlispvm_meta_instruction_id_nip_mtwo_mdrop = 47,
    jitterlispvm_meta_instruction_id_nop = 48,
    jitterlispvm_meta_instruction_id_one_mminus_mregister = 49,
    jitterlispvm_meta_instruction_id_one_mplus_mregister = 50,
    jitterlispvm_meta_instruction_id_pop_mto_mglobal = 51,
    jitterlispvm_meta_instruction_id_pop_mto_mglobal_mdefined = 52,
    jitterlispvm_meta_instruction_id_pop_mto_mregister = 53,
    jitterlispvm_meta_instruction_id_primitive = 54,
    jitterlispvm_meta_instruction_id_primitive_mboolean_mcanonicalize = 55,
    jitterlispvm_meta_instruction_id_primitive_mbox = 56,
    jitterlispvm_meta_instruction_id_primitive_mbox_mget = 57,
    jitterlispvm_meta_instruction_id_primitive_mbox_msetb_mspecial = 58,
    jitterlispvm_meta_instruction_id_primitive_mcar = 59,
    jitterlispvm_meta_instruction_id_primitive_mcdr = 60,
    jitterlispvm_meta_instruction_id_primitive_mcharacterp = 61,
    jitterlispvm_meta_instruction_id_primitive_mcons_mspecial = 62,
    jitterlispvm_meta_instruction_id_primitive_mconsp = 63,
    jitterlispvm_meta_instruction_id_primitive_meqp = 64,
    jitterlispvm_meta_instruction_id_primitive_mfixnum_meqp = 65,
    jitterlispvm_meta_instruction_id_primitive_mfixnum_mnot_meqp = 66,
    jitterlispvm_meta_instruction_id_primitive_mfixnump = 67,
    jitterlispvm_meta_instruction_id_primitive_mgreaterp = 68,
    jitterlispvm_meta_instruction_id_primitive_mlessp = 69,
    jitterlispvm_meta_instruction_id_primitive_mnegate = 70,
    jitterlispvm_meta_instruction_id_primitive_mnegativep = 71,
    jitterlispvm_meta_instruction_id_primitive_mnon_mconsp = 72,
    jitterlispvm_meta_instruction_id_primitive_mnon_mnegativep = 73,
    jitterlispvm_meta_instruction_id_primitive_mnon_mnullp = 74,
    jitterlispvm_meta_instruction_id_primitive_mnon_mpositivep = 75,
    jitterlispvm_meta_instruction_id_primitive_mnon_msymbolp = 76,
    jitterlispvm_meta_instruction_id_primitive_mnon_mzerop = 77,
    jitterlispvm_meta_instruction_id_primitive_mnot = 78,
    jitterlispvm_meta_instruction_id_primitive_mnot_meqp = 79,
    jitterlispvm_meta_instruction_id_primitive_mnot_mgreaterp = 80,
    jitterlispvm_meta_instruction_id_primitive_mnot_mlessp = 81,
    jitterlispvm_meta_instruction_id_primitive_mnothingp = 82,
    jitterlispvm_meta_instruction_id_primitive_mnullp = 83,
    jitterlispvm_meta_instruction_id_primitive_mone_mminus = 84,
    jitterlispvm_meta_instruction_id_primitive_mone_mplus = 85,
    jitterlispvm_meta_instruction_id_primitive_mpositivep = 86,
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mdivided = 87,
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mdivided_munsafe = 88,
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mminus = 89,
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mplus = 90,
    jitterlispvm_meta_instruction_id_primitive_mprimordial_mtimes = 91,
    jitterlispvm_meta_instruction_id_primitive_mquotient = 92,
    jitterlispvm_meta_instruction_id_primitive_mquotient_munsafe = 93,
    jitterlispvm_meta_instruction_id_primitive_mremainder = 94,
    jitterlispvm_meta_instruction_id_primitive_mremainder_munsafe = 95,
    jitterlispvm_meta_instruction_id_primitive_mset_mcarb_mspecial = 96,
    jitterlispvm_meta_instruction_id_primitive_mset_mcdrb_mspecial = 97,
    jitterlispvm_meta_instruction_id_primitive_msymbolp = 98,
    jitterlispvm_meta_instruction_id_primitive_mtwo_mdivided = 99,
    jitterlispvm_meta_instruction_id_primitive_mtwo_mquotient = 100,
    jitterlispvm_meta_instruction_id_primitive_mtwo_mremainder = 101,
    jitterlispvm_meta_instruction_id_primitive_mtwo_mtimes = 102,
    jitterlispvm_meta_instruction_id_primitive_muniquep = 103,
    jitterlispvm_meta_instruction_id_primitive_mzerop = 104,
    jitterlispvm_meta_instruction_id_procedure_mprolog = 105,
    jitterlispvm_meta_instruction_id_push_mfalse = 106,
    jitterlispvm_meta_instruction_id_push_mglobal = 107,
    jitterlispvm_meta_instruction_id_push_mliteral = 108,
    jitterlispvm_meta_instruction_id_push_mnil = 109,
    jitterlispvm_meta_instruction_id_push_mnothing = 110,
    jitterlispvm_meta_instruction_id_push_mone = 111,
    jitterlispvm_meta_instruction_id_push_mregister = 112,
    jitterlispvm_meta_instruction_id_push_munspecified = 113,
    jitterlispvm_meta_instruction_id_push_mzero = 114,
    jitterlispvm_meta_instruction_id_register_mto_mregister = 115,
    jitterlispvm_meta_instruction_id_restore_mregister = 116,
    jitterlispvm_meta_instruction_id_return = 117,
    jitterlispvm_meta_instruction_id_save_mregister = 118,
    jitterlispvm_meta_instruction_id_tail_mcall = 119,
    jitterlispvm_meta_instruction_id_tail_mcall_mcompiled = 120,
    jitterlispvm_meta_instruction_id_unreachable = 121
  };

#define JITTERLISPVM_META_INSTRUCTION_NO 122

/* The longest meta-instruction name length, not mangled, without
   counting the final '\0' character. */
#define JITTERLISPVM_MAX_META_INSTRUCTION_NAME_LENGTH 35

#endif // #ifndef JITTERLISPVM_META_INSTRUCTIONS_H_
#ifndef JITTERLISPVM_SPECIALIZED_INSTRUCTIONS_H_
#define JITTERLISPVM_SPECIALIZED_INSTRUCTIONS_H_

enum jitterlispvm_specialized_instruction_opcode
  {
    jitterlispvm_specialized_instruction_opcode__eINVALID = 0,
    jitterlispvm_specialized_instruction_opcode__eBEGINBASICBLOCK = 1,
    jitterlispvm_specialized_instruction_opcode__eEXITVM = 2,
    jitterlispvm_specialized_instruction_opcode__eDATALOCATIONS = 3,
    jitterlispvm_specialized_instruction_opcode__eNOP = 4,
    jitterlispvm_specialized_instruction_opcode__eUNREACHABLE0 = 5,
    jitterlispvm_specialized_instruction_opcode__eUNREACHABLE1 = 6,
    jitterlispvm_specialized_instruction_opcode__ePRETENDTOJUMPANYWHERE = 7,
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n1___rrR = 8,
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n2___rrR = 9,
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n3___rrR = 10,
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n4___rrR = 11,
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n5___rrR = 12,
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n6___rrR = 13,
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n7___rrR = 14,
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n8___rrR = 15,
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n9___rrR = 16,
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__n10___rrR = 17,
    jitterlispvm_specialized_instruction_opcode_at_mdepth_mto_mregister__nR___rrR = 18,
    jitterlispvm_specialized_instruction_opcode_branch__fR = 19,
    jitterlispvm_specialized_instruction_opcode_branch_mif_mfalse__fR = 20,
    jitterlispvm_specialized_instruction_opcode_branch_mif_mnon_mpositive__fR__fR = 21,
    jitterlispvm_specialized_instruction_opcode_branch_mif_mnot_mless__fR__fR = 22,
    jitterlispvm_specialized_instruction_opcode_branch_mif_mnot_mnull__fR = 23,
    jitterlispvm_specialized_instruction_opcode_branch_mif_mnull__fR = 24,
    jitterlispvm_specialized_instruction_opcode_branch_mif_mpositive__fR__fR = 25,
    jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mnon_mpositive___rrR__fR__fR = 26,
    jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mnon_mzero___rrR__fR__fR = 27,
    jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mnot_mnull___rrR__fR = 28,
    jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mnull___rrR__fR = 29,
    jitterlispvm_specialized_instruction_opcode_branch_mif_mregister_mpositive___rrR__fR__fR = 30,
    jitterlispvm_specialized_instruction_opcode_branch_mif_mtrue__fR = 31,
    jitterlispvm_specialized_instruction_opcode_call__n0__retR = 32,
    jitterlispvm_specialized_instruction_opcode_call__n1__retR = 33,
    jitterlispvm_specialized_instruction_opcode_call__n2__retR = 34,
    jitterlispvm_specialized_instruction_opcode_call__n3__retR = 35,
    jitterlispvm_specialized_instruction_opcode_call__n4__retR = 36,
    jitterlispvm_specialized_instruction_opcode_call__n5__retR = 37,
    jitterlispvm_specialized_instruction_opcode_call__n6__retR = 38,
    jitterlispvm_specialized_instruction_opcode_call__n7__retR = 39,
    jitterlispvm_specialized_instruction_opcode_call__n8__retR = 40,
    jitterlispvm_specialized_instruction_opcode_call__n9__retR = 41,
    jitterlispvm_specialized_instruction_opcode_call__n10__retR = 42,
    jitterlispvm_specialized_instruction_opcode_call__nR__retR = 43,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n0__retR = 44,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n1__retR = 45,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n2__retR = 46,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n3__retR = 47,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n4__retR = 48,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n5__retR = 49,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n6__retR = 50,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n7__retR = 51,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n8__retR = 52,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n9__retR = 53,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__n10__retR = 54,
    jitterlispvm_specialized_instruction_opcode_call_mcompiled__nR__retR = 55,
    jitterlispvm_specialized_instruction_opcode_call_mfrom_mc__retR = 56,
    jitterlispvm_specialized_instruction_opcode_canonicalize_mboolean = 57,
    jitterlispvm_specialized_instruction_opcode_cdr_mregister___rrR___rrR__fR = 58,
    jitterlispvm_specialized_instruction_opcode_check_mclosure__fR = 59,
    jitterlispvm_specialized_instruction_opcode_check_mglobal_mdefined__nR__fR = 60,
    jitterlispvm_specialized_instruction_opcode_check_min_marity__n0__fR = 61,
    jitterlispvm_specialized_instruction_opcode_check_min_marity__n1__fR = 62,
    jitterlispvm_specialized_instruction_opcode_check_min_marity__n2__fR = 63,
    jitterlispvm_specialized_instruction_opcode_check_min_marity__n3__fR = 64,
    jitterlispvm_specialized_instruction_opcode_check_min_marity__n4__fR = 65,
    jitterlispvm_specialized_instruction_opcode_check_min_marity__n5__fR = 66,
    jitterlispvm_specialized_instruction_opcode_check_min_marity__n6__fR = 67,
    jitterlispvm_specialized_instruction_opcode_check_min_marity__n7__fR = 68,
    jitterlispvm_specialized_instruction_opcode_check_min_marity__n8__fR = 69,
    jitterlispvm_specialized_instruction_opcode_check_min_marity__n9__fR = 70,
    jitterlispvm_specialized_instruction_opcode_check_min_marity__n10__fR = 71,
    jitterlispvm_specialized_instruction_opcode_check_min_marity__nR__fR = 72,
    jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n0__fR = 73,
    jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n1__fR = 74,
    jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n2__fR = 75,
    jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n3__fR = 76,
    jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n4__fR = 77,
    jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n5__fR = 78,
    jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n6__fR = 79,
    jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n7__fR = 80,
    jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n8__fR = 81,
    jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n9__fR = 82,
    jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__n10__fR = 83,
    jitterlispvm_specialized_instruction_opcode_check_min_marity_m_malt__nR__fR = 84,
    jitterlispvm_specialized_instruction_opcode_copy_mfrom_mliteral__nR = 85,
    jitterlispvm_specialized_instruction_opcode_copy_mfrom_mregister___rrR = 86,
    jitterlispvm_specialized_instruction_opcode_copy_mto_mregister___rrR = 87,
    jitterlispvm_specialized_instruction_opcode_drop = 88,
    jitterlispvm_specialized_instruction_opcode_drop_mnip = 89,
    jitterlispvm_specialized_instruction_opcode_dup = 90,
    jitterlispvm_specialized_instruction_opcode_exitvm = 91,
    jitterlispvm_specialized_instruction_opcode_fail__retR = 92,
    jitterlispvm_specialized_instruction_opcode_gc_mif_mneeded__fR = 93,
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n4 = 94,
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n8 = 95,
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n12 = 96,
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n16 = 97,
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n24 = 98,
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n32 = 99,
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n36 = 100,
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n48 = 101,
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n52 = 102,
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__n64 = 103,
    jitterlispvm_specialized_instruction_opcode_heap_mallocate__nR = 104,
    jitterlispvm_specialized_instruction_opcode_literal_mto_mregister__nR___rrR = 105,
    jitterlispvm_specialized_instruction_opcode_nip = 106,
    jitterlispvm_specialized_instruction_opcode_nip_mdrop = 107,
    jitterlispvm_specialized_instruction_opcode_nip_mfive = 108,
    jitterlispvm_specialized_instruction_opcode_nip_mfive_mdrop = 109,
    jitterlispvm_specialized_instruction_opcode_nip_mfour = 110,
    jitterlispvm_specialized_instruction_opcode_nip_mfour_mdrop = 111,
    jitterlispvm_specialized_instruction_opcode_nip_mpush_mliteral__nR = 112,
    jitterlispvm_specialized_instruction_opcode_nip_mpush_mregister___rrR = 113,
    jitterlispvm_specialized_instruction_opcode_nip_msix = 114,
    jitterlispvm_specialized_instruction_opcode_nip_msix_mdrop = 115,
    jitterlispvm_specialized_instruction_opcode_nip_mthree = 116,
    jitterlispvm_specialized_instruction_opcode_nip_mthree_mdrop = 117,
    jitterlispvm_specialized_instruction_opcode_nip_mtwo = 118,
    jitterlispvm_specialized_instruction_opcode_nip_mtwo_mdrop = 119,
    jitterlispvm_specialized_instruction_opcode_nop = 120,
    jitterlispvm_specialized_instruction_opcode_one_mminus_mregister___rrR___rrR__fR = 121,
    jitterlispvm_specialized_instruction_opcode_one_mplus_mregister___rrR___rrR__fR = 122,
    jitterlispvm_specialized_instruction_opcode_pop_mto_mglobal__nR__fR = 123,
    jitterlispvm_specialized_instruction_opcode_pop_mto_mglobal_mdefined__nR__fR = 124,
    jitterlispvm_specialized_instruction_opcode_pop_mto_mregister___rrR = 125,
    jitterlispvm_specialized_instruction_opcode_primitive__nR__n0__fR = 126,
    jitterlispvm_specialized_instruction_opcode_primitive__nR__n1__fR = 127,
    jitterlispvm_specialized_instruction_opcode_primitive__nR__n2__fR = 128,
    jitterlispvm_specialized_instruction_opcode_primitive__nR__n3__fR = 129,
    jitterlispvm_specialized_instruction_opcode_primitive__nR__n4__fR = 130,
    jitterlispvm_specialized_instruction_opcode_primitive__nR__nR__fR = 131,
    jitterlispvm_specialized_instruction_opcode_primitive_mboolean_mcanonicalize = 132,
    jitterlispvm_specialized_instruction_opcode_primitive_mbox = 133,
    jitterlispvm_specialized_instruction_opcode_primitive_mbox_mget__fR = 134,
    jitterlispvm_specialized_instruction_opcode_primitive_mbox_msetb_mspecial__fR = 135,
    jitterlispvm_specialized_instruction_opcode_primitive_mcar__fR = 136,
    jitterlispvm_specialized_instruction_opcode_primitive_mcdr__fR = 137,
    jitterlispvm_specialized_instruction_opcode_primitive_mcharacterp = 138,
    jitterlispvm_specialized_instruction_opcode_primitive_mcons_mspecial = 139,
    jitterlispvm_specialized_instruction_opcode_primitive_mconsp = 140,
    jitterlispvm_specialized_instruction_opcode_primitive_meqp = 141,
    jitterlispvm_specialized_instruction_opcode_primitive_mfixnum_meqp__fR = 142,
    jitterlispvm_specialized_instruction_opcode_primitive_mfixnum_mnot_meqp__fR = 143,
    jitterlispvm_specialized_instruction_opcode_primitive_mfixnump = 144,
    jitterlispvm_specialized_instruction_opcode_primitive_mgreaterp__fR = 145,
    jitterlispvm_specialized_instruction_opcode_primitive_mlessp__fR = 146,
    jitterlispvm_specialized_instruction_opcode_primitive_mnegate__fR = 147,
    jitterlispvm_specialized_instruction_opcode_primitive_mnegativep__fR = 148,
    jitterlispvm_specialized_instruction_opcode_primitive_mnon_mconsp = 149,
    jitterlispvm_specialized_instruction_opcode_primitive_mnon_mnegativep__fR = 150,
    jitterlispvm_specialized_instruction_opcode_primitive_mnon_mnullp = 151,
    jitterlispvm_specialized_instruction_opcode_primitive_mnon_mpositivep__fR = 152,
    jitterlispvm_specialized_instruction_opcode_primitive_mnon_msymbolp = 153,
    jitterlispvm_specialized_instruction_opcode_primitive_mnon_mzerop__fR = 154,
    jitterlispvm_specialized_instruction_opcode_primitive_mnot = 155,
    jitterlispvm_specialized_instruction_opcode_primitive_mnot_meqp = 156,
    jitterlispvm_specialized_instruction_opcode_primitive_mnot_mgreaterp__fR = 157,
    jitterlispvm_specialized_instruction_opcode_primitive_mnot_mlessp__fR = 158,
    jitterlispvm_specialized_instruction_opcode_primitive_mnothingp = 159,
    jitterlispvm_specialized_instruction_opcode_primitive_mnullp = 160,
    jitterlispvm_specialized_instruction_opcode_primitive_mone_mminus__fR = 161,
    jitterlispvm_specialized_instruction_opcode_primitive_mone_mplus__fR = 162,
    jitterlispvm_specialized_instruction_opcode_primitive_mpositivep__fR = 163,
    jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mdivided__fR = 164,
    jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mdivided_munsafe__fR = 165,
    jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mminus__fR = 166,
    jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mplus__fR = 167,
    jitterlispvm_specialized_instruction_opcode_primitive_mprimordial_mtimes__fR = 168,
    jitterlispvm_specialized_instruction_opcode_primitive_mquotient__fR = 169,
    jitterlispvm_specialized_instruction_opcode_primitive_mquotient_munsafe__fR = 170,
    jitterlispvm_specialized_instruction_opcode_primitive_mremainder__fR = 171,
    jitterlispvm_specialized_instruction_opcode_primitive_mremainder_munsafe__fR = 172,
    jitterlispvm_specialized_instruction_opcode_primitive_mset_mcarb_mspecial__fR = 173,
    jitterlispvm_specialized_instruction_opcode_primitive_mset_mcdrb_mspecial__fR = 174,
    jitterlispvm_specialized_instruction_opcode_primitive_msymbolp = 175,
    jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mdivided__fR = 176,
    jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mquotient__fR = 177,
    jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mremainder__fR = 178,
    jitterlispvm_specialized_instruction_opcode_primitive_mtwo_mtimes__fR = 179,
    jitterlispvm_specialized_instruction_opcode_primitive_muniquep = 180,
    jitterlispvm_specialized_instruction_opcode_primitive_mzerop__fR = 181,
    jitterlispvm_specialized_instruction_opcode_procedure_mprolog = 182,
    jitterlispvm_specialized_instruction_opcode_push_mfalse = 183,
    jitterlispvm_specialized_instruction_opcode_push_mglobal__nR__fR = 184,
    jitterlispvm_specialized_instruction_opcode_push_mliteral__nR = 185,
    jitterlispvm_specialized_instruction_opcode_push_mnil = 186,
    jitterlispvm_specialized_instruction_opcode_push_mnothing = 187,
    jitterlispvm_specialized_instruction_opcode_push_mone = 188,
    jitterlispvm_specialized_instruction_opcode_push_mregister___rrR = 189,
    jitterlispvm_specialized_instruction_opcode_push_munspecified = 190,
    jitterlispvm_specialized_instruction_opcode_push_mzero = 191,
    jitterlispvm_specialized_instruction_opcode_register_mto_mregister___rrR___rrR = 192,
    jitterlispvm_specialized_instruction_opcode_restore_mregister___rrR = 193,
    jitterlispvm_specialized_instruction_opcode_return = 194,
    jitterlispvm_specialized_instruction_opcode_save_mregister___rrR = 195,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n0__retR = 196,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n1__retR = 197,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n2__retR = 198,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n3__retR = 199,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n4__retR = 200,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n5__retR = 201,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n6__retR = 202,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n7__retR = 203,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n8__retR = 204,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n9__retR = 205,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__n10__retR = 206,
    jitterlispvm_specialized_instruction_opcode_tail_mcall__nR__retR = 207,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n0__retR = 208,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n1__retR = 209,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n2__retR = 210,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n3__retR = 211,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n4__retR = 212,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n5__retR = 213,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n6__retR = 214,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n7__retR = 215,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n8__retR = 216,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n9__retR = 217,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__n10__retR = 218,
    jitterlispvm_specialized_instruction_opcode_tail_mcall_mcompiled__nR__retR = 219,
    jitterlispvm_specialized_instruction_opcode_unreachable = 220,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch__fR__retR = 221,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mfalse__fR__retR = 222,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnon_mpositive__fR__fR__retR = 223,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnot_mless__fR__fR__retR = 224,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnot_mnull__fR__retR = 225,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mnull__fR__retR = 226,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mpositive__fR__fR__retR = 227,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnon_mpositive___rrR__fR__fR__retR = 228,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnon_mzero___rrR__fR__fR__retR = 229,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnot_mnull___rrR__fR__retR = 230,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mnull___rrR__fR__retR = 231,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mregister_mpositive___rrR__fR__fR__retR = 232,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mbranch_mif_mtrue__fR__retR = 233,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n0__retR = 234,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n1__retR = 235,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n2__retR = 236,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n3__retR = 237,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n4__retR = 238,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n5__retR = 239,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n6__retR = 240,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n7__retR = 241,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n8__retR = 242,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n9__retR = 243,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__n10__retR = 244,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall__nR__retR = 245,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n0__retR = 246,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n1__retR = 247,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n2__retR = 248,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n3__retR = 249,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n4__retR = 250,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n5__retR = 251,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n6__retR = 252,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n7__retR = 253,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n8__retR = 254,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n9__retR = 255,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__n10__retR = 256,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mcompiled__nR__retR = 257,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcall_mfrom_mc__retR = 258,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcdr_mregister___rrR___rrR__fR__retR = 259,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_mclosure__fR__retR = 260,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_mglobal_mdefined__nR__fR__retR = 261,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n0__fR__retR = 262,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n1__fR__retR = 263,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n2__fR__retR = 264,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n3__fR__retR = 265,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n4__fR__retR = 266,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n5__fR__retR = 267,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n6__fR__retR = 268,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n7__fR__retR = 269,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n8__fR__retR = 270,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n9__fR__retR = 271,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__n10__fR__retR = 272,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity__nR__fR__retR = 273,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n0__fR__retR = 274,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n1__fR__retR = 275,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n2__fR__retR = 276,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n3__fR__retR = 277,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n4__fR__retR = 278,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n5__fR__retR = 279,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n6__fR__retR = 280,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n7__fR__retR = 281,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n8__fR__retR = 282,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n9__fR__retR = 283,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__n10__fR__retR = 284,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mcheck_min_marity_m_malt__nR__fR__retR = 285,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mexitvm__retR = 286,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mgc_mif_mneeded__fR__retR = 287,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mone_mminus_mregister___rrR___rrR__fR__retR = 288,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mone_mplus_mregister___rrR___rrR__fR__retR = 289,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mpop_mto_mglobal__nR__fR__retR = 290,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mpop_mto_mglobal_mdefined__nR__fR__retR = 291,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n0__fR__retR = 292,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n1__fR__retR = 293,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n2__fR__retR = 294,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n3__fR__retR = 295,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__n4__fR__retR = 296,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive__nR__nR__fR__retR = 297,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mbox_mget__fR__retR = 298,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mbox_msetb_mspecial__fR__retR = 299,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mcar__fR__retR = 300,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mcdr__fR__retR = 301,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mfixnum_meqp__fR__retR = 302,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mfixnum_mnot_meqp__fR__retR = 303,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mgreaterp__fR__retR = 304,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mlessp__fR__retR = 305,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnegate__fR__retR = 306,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnegativep__fR__retR = 307,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnon_mnegativep__fR__retR = 308,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnon_mpositivep__fR__retR = 309,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnon_mzerop__fR__retR = 310,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnot_mgreaterp__fR__retR = 311,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mnot_mlessp__fR__retR = 312,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mone_mminus__fR__retR = 313,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mone_mplus__fR__retR = 314,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mpositivep__fR__retR = 315,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mdivided__fR__retR = 316,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mdivided_munsafe__fR__retR = 317,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mminus__fR__retR = 318,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mplus__fR__retR = 319,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mprimordial_mtimes__fR__retR = 320,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mquotient__fR__retR = 321,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mquotient_munsafe__fR__retR = 322,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mremainder__fR__retR = 323,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mremainder_munsafe__fR__retR = 324,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mset_mcarb_mspecial__fR__retR = 325,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mset_mcdrb_mspecial__fR__retR = 326,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mdivided__fR__retR = 327,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mquotient__fR__retR = 328,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mremainder__fR__retR = 329,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mtwo_mtimes__fR__retR = 330,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprimitive_mzerop__fR__retR = 331,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mprocedure_mprolog__retR = 332,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mpush_mglobal__nR__fR__retR = 333,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mreturn__retR = 334,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n0__retR = 335,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n1__retR = 336,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n2__retR = 337,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n3__retR = 338,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n4__retR = 339,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n5__retR = 340,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n6__retR = 341,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n7__retR = 342,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n8__retR = 343,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n9__retR = 344,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__n10__retR = 345,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall__nR__retR = 346,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n0__retR = 347,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n1__retR = 348,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n2__retR = 349,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n3__retR = 350,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n4__retR = 351,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n5__retR = 352,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n6__retR = 353,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n7__retR = 354,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n8__retR = 355,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n9__retR = 356,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__n10__retR = 357,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_mtail_mcall_mcompiled__nR__retR = 358,
    jitterlispvm_specialized_instruction_opcode__eREPLACEMENT_munreachable__retR = 359
  };

#define JITTERLISPVM_SPECIALIZED_INSTRUCTION_NO 360

#endif // #ifndef JITTERLISPVM_SPECIALIZED_INSTRUCTIONS_H_
/* How many residuals we can have at most.  This, with some dispatches,
   is needed to compute a slow register offset from the base. */
#define JITTERLISPVM_MAX_RESIDUAL_ARITY  4

/* Stack operations.
 * ************************************************************************** */

/* The following stack operations (with the initial state
   pointer argument) can be used *out* of instruction code
   blocks, in non-VM code.
   Macros with the same names are available from instruction
   code blocks, but those alternative definitions lack the first
   argument: the state they operate on is always the current
   state -- in particular, its runtime. */

/* Wrapper definition of the top operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_TOP_MAINSTACK(state_p)  \
  JITTER_STACK_TOS_TOP (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    )
/* Wrapper definition of the under_top operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_UNDER_TOP_MAINSTACK(state_p)  \
  JITTER_STACK_TOS_UNDER_TOP (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    )
/* Wrapper definition of the at_depth operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_AT_DEPTH_MAINSTACK(state_p, x0)  \
  JITTER_STACK_TOS_AT_DEPTH (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    , x0)
/* Wrapper definition of the at_nonzero_depth operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_AT_NONZERO_DEPTH_MAINSTACK(state_p, x0)  \
  JITTER_STACK_TOS_AT_NONZERO_DEPTH (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    , x0)
/* Wrapper definition of the set_at_depth operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_SET_AT_DEPTH_MAINSTACK(state_p, x0, x1)  \
  JITTER_STACK_TOS_SET_AT_DEPTH (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    , x0, x1)
/* Wrapper definition of the set_at_nonzero_depth operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_SET_AT_NONZERO_DEPTH_MAINSTACK(state_p, x0, x1)  \
  JITTER_STACK_TOS_SET_AT_NONZERO_DEPTH (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    , x0, x1)
/* Wrapper definition of the push_unspecified operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_PUSH_UNSPECIFIED_MAINSTACK(state_p)  \
  JITTER_STACK_TOS_PUSH_UNSPECIFIED (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    )
/* Wrapper definition of the push operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_PUSH_MAINSTACK(state_p, x0)  \
  JITTER_STACK_TOS_PUSH (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    , x0)
/* Wrapper definition of the under_push_unspecified operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_UNDER_PUSH_UNSPECIFIED_MAINSTACK(state_p)  \
  JITTER_STACK_TOS_UNDER_PUSH_UNSPECIFIED (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    )
/* Wrapper definition of the under_push operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_UNDER_PUSH_MAINSTACK(state_p, x0)  \
  JITTER_STACK_TOS_UNDER_PUSH (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    , x0)
/* Wrapper definition of the drop operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_DROP_MAINSTACK(state_p)  \
  JITTER_STACK_TOS_DROP (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    )
/* Wrapper definition of the dup operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_DUP_MAINSTACK(state_p)  \
  JITTER_STACK_TOS_DUP (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    )
/* Wrapper definition of the swap operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_SWAP_MAINSTACK(state_p)  \
  JITTER_STACK_TOS_SWAP (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    )
/* Wrapper definition of the quake operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_QUAKE_MAINSTACK(state_p)  \
  JITTER_STACK_TOS_QUAKE (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    )
/* Wrapper definition of the over operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_OVER_MAINSTACK(state_p)  \
  JITTER_STACK_TOS_OVER (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    )
/* Wrapper definition of the tuck operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_TUCK_MAINSTACK(state_p)  \
  JITTER_STACK_TOS_TUCK (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    )
/* Wrapper definition of the nip operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_NIP_MAINSTACK(state_p)  \
  JITTER_STACK_TOS_NIP (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    )
/* Wrapper definition of the rot operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_ROT_MAINSTACK(state_p)  \
  JITTER_STACK_TOS_ROT (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    )
/* Wrapper definition of the mrot operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_MROT_MAINSTACK(state_p)  \
  JITTER_STACK_TOS_MROT (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    )
/* Wrapper definition of the roll operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_ROLL_MAINSTACK(state_p, x0)  \
  JITTER_STACK_TOS_ROLL (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    , x0)
/* Wrapper definition of the mroll operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_MROLL_MAINSTACK(state_p, x0)  \
  JITTER_STACK_TOS_MROLL (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    , x0)
/* Wrapper definition of the slide operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_SLIDE_MAINSTACK(state_p, x0, x1)  \
  JITTER_STACK_TOS_SLIDE (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    , x0, x1)
/* Wrapper definition of the whirl operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_WHIRL_MAINSTACK(state_p, x0)  \
  JITTER_STACK_TOS_WHIRL (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    , x0)
/* Wrapper definition of the bulge operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_BULGE_MAINSTACK(state_p, x0)  \
  JITTER_STACK_TOS_BULGE (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    , x0)
/* Wrapper definition of the height operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_HEIGHT_MAINSTACK(state_p)  \
  JITTER_STACK_TOS_HEIGHT (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    )
/* Wrapper definition of the set_height operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_SET_HEIGHT_MAINSTACK(state_p, x0)  \
  JITTER_STACK_TOS_SET_HEIGHT (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    , x0)
/* Wrapper definition of the reverse operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_REVERSE_MAINSTACK(state_p, x0)  \
  JITTER_STACK_TOS_REVERSE (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    , x0)
/* Wrapper definition of the unary operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_UNARY_MAINSTACK(state_p, x0)  \
  JITTER_STACK_TOS_UNARY (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    , x0)
/* Wrapper definition of the binary operation for the
   TOS-optimized stack "mainstack". */
#define JITTERLISPVM_BINARY_MAINSTACK(state_p, x0)  \
  JITTER_STACK_TOS_BINARY (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    mainstack  \
    , x0)
/* Wrapper definition of the top operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_TOP_RETURNSTACK(state_p)  \
  JITTER_STACK_NTOS_TOP (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    )
/* Wrapper definition of the under_top operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_UNDER_TOP_RETURNSTACK(state_p)  \
  JITTER_STACK_NTOS_UNDER_TOP (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    )
/* Wrapper definition of the at_depth operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_AT_DEPTH_RETURNSTACK(state_p, x0)  \
  JITTER_STACK_NTOS_AT_DEPTH (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    , x0)
/* Wrapper definition of the at_nonzero_depth operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_AT_NONZERO_DEPTH_RETURNSTACK(state_p, x0)  \
  JITTER_STACK_NTOS_AT_NONZERO_DEPTH (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    , x0)
/* Wrapper definition of the set_at_depth operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_SET_AT_DEPTH_RETURNSTACK(state_p, x0, x1)  \
  JITTER_STACK_NTOS_SET_AT_DEPTH (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    , x0, x1)
/* Wrapper definition of the set_at_nonzero_depth operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_SET_AT_NONZERO_DEPTH_RETURNSTACK(state_p, x0, x1)  \
  JITTER_STACK_NTOS_SET_AT_NONZERO_DEPTH (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    , x0, x1)
/* Wrapper definition of the push_unspecified operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_PUSH_UNSPECIFIED_RETURNSTACK(state_p)  \
  JITTER_STACK_NTOS_PUSH_UNSPECIFIED (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    )
/* Wrapper definition of the push operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_PUSH_RETURNSTACK(state_p, x0)  \
  JITTER_STACK_NTOS_PUSH (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    , x0)
/* Wrapper definition of the under_push_unspecified operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_UNDER_PUSH_UNSPECIFIED_RETURNSTACK(state_p)  \
  JITTER_STACK_NTOS_UNDER_PUSH_UNSPECIFIED (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    )
/* Wrapper definition of the under_push operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_UNDER_PUSH_RETURNSTACK(state_p, x0)  \
  JITTER_STACK_NTOS_UNDER_PUSH (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    , x0)
/* Wrapper definition of the drop operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_DROP_RETURNSTACK(state_p)  \
  JITTER_STACK_NTOS_DROP (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    )
/* Wrapper definition of the dup operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_DUP_RETURNSTACK(state_p)  \
  JITTER_STACK_NTOS_DUP (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    )
/* Wrapper definition of the swap operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_SWAP_RETURNSTACK(state_p)  \
  JITTER_STACK_NTOS_SWAP (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    )
/* Wrapper definition of the quake operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_QUAKE_RETURNSTACK(state_p)  \
  JITTER_STACK_NTOS_QUAKE (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    )
/* Wrapper definition of the over operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_OVER_RETURNSTACK(state_p)  \
  JITTER_STACK_NTOS_OVER (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    )
/* Wrapper definition of the tuck operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_TUCK_RETURNSTACK(state_p)  \
  JITTER_STACK_NTOS_TUCK (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    )
/* Wrapper definition of the nip operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_NIP_RETURNSTACK(state_p)  \
  JITTER_STACK_NTOS_NIP (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    )
/* Wrapper definition of the rot operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_ROT_RETURNSTACK(state_p)  \
  JITTER_STACK_NTOS_ROT (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    )
/* Wrapper definition of the mrot operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_MROT_RETURNSTACK(state_p)  \
  JITTER_STACK_NTOS_MROT (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    )
/* Wrapper definition of the roll operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_ROLL_RETURNSTACK(state_p, x0)  \
  JITTER_STACK_NTOS_ROLL (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    , x0)
/* Wrapper definition of the mroll operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_MROLL_RETURNSTACK(state_p, x0)  \
  JITTER_STACK_NTOS_MROLL (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    , x0)
/* Wrapper definition of the slide operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_SLIDE_RETURNSTACK(state_p, x0, x1)  \
  JITTER_STACK_NTOS_SLIDE (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    , x0, x1)
/* Wrapper definition of the whirl operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_WHIRL_RETURNSTACK(state_p, x0)  \
  JITTER_STACK_NTOS_WHIRL (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    , x0)
/* Wrapper definition of the bulge operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_BULGE_RETURNSTACK(state_p, x0)  \
  JITTER_STACK_NTOS_BULGE (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    , x0)
/* Wrapper definition of the height operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_HEIGHT_RETURNSTACK(state_p)  \
  JITTER_STACK_NTOS_HEIGHT (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    )
/* Wrapper definition of the set_height operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_SET_HEIGHT_RETURNSTACK(state_p, x0)  \
  JITTER_STACK_NTOS_SET_HEIGHT (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    , x0)
/* Wrapper definition of the reverse operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_REVERSE_RETURNSTACK(state_p, x0)  \
  JITTER_STACK_NTOS_REVERSE (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    , x0)
/* Wrapper definition of the unary operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_UNARY_RETURNSTACK(state_p, x0)  \
  JITTER_STACK_NTOS_UNARY (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    , x0)
/* Wrapper definition of the binary operation for the
   non-TOS-optimized stack "returnstack". */
#define JITTERLISPVM_BINARY_RETURNSTACK(state_p, x0)  \
  JITTER_STACK_NTOS_BINARY (jitterlisp_object,  \
    (state_p)->_jitterlispvm_1TbNcL3hel_state_runtime. /* not an error */,  \
    returnstack  \
    , x0)

/* User-specified code, late header part: beginning. */

  
/* User-specified code, late header part: end */


/* Close the multiple-inclusion guard opened in the template. */
#endif // #ifndef JITTERLISPVM_VM_H_
