/* JitterLisp: constant string header.

   Copyright (C) 2017, 2018 Luca Saiu
   Written by Luca Saiu

   This file is part of the JitterLisp language implementation, distributed as
   an example along with GNU Jitter under the same license.

   GNU Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GNU Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Jitter.  If not, see <https://www.gnu.org/licenses/>. */


#ifndef JITTERLISP_CONSTANT_STRINGS_H_
#define JITTERLISP_CONSTANT_STRINGS_H_


/* Global machine-generated strings.
 * ************************************************************************** */

/* The variables declared here are defined in a machine-generated C file,
   obtained by running text files thru a simple sed script. */

/* The JitterLisp library in Lisp, as a constant C string containing the text to
   be parsed. */
extern const char *
jitterlisp_library_string;

/* The GNU GPL text as a C string. */
extern const char *
jitterlisp_gpl;

/* An excerpt from the GNU GPL explaining that there is no warranty, as a C
   string. */
extern const char *
jitterlisp_no_warranty;

#endif // #ifndef JITTERLISP_CONSTANT_STRINGS_H_
