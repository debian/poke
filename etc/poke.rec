# POKE -- The GNU poke database -*- mode: rec -*-
#
# Copyright (C) 2020, 2021, 2023, 2024 The poke authors.
#
# This file is part of GNU poke.
#
# GNU poke is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GNU poke is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU poke.  If not, see <https://www.gnu.org/licenses/>.

# This recfile (see http://www.gnu.org/software/recutils) contains
# information relative to the development of GNU poke, and it is used
# by the poke hackers and maintainers.
#
# The information is gathered among several record sets:
#
# - `Task' records describe stuff that we would like to see realized
#   somehow.
#
# - `Release' records describe poke releases.
#
# - `Hacker' records describe members of the poke gang.
#
# See the corresponding record descriptors for more information.

# If you start working on a complex task, please let us know in the
# development mailing list so we can be aware of your efforts, and
# probably help you.
#
# If the task is marked with RFC, please send a proposal of a design
# to poke-devel so we can discuss about it.  Do not start working on
# it straight away, because whatever design to use must be agreed
# among the maintainers.
#
# If you complete a task, please include a patch for this file in your
# patch submission that removes the task from this file.
#
# If you want to add a comment to any of the entries in this file,
# please do so adding a note field like:
#
#   Note: This would be soooooo cool :) - jemarch
#
# IMPORTANT: please make sure to run `recfix' on this file to make
#            sure your modifications do not break the integrity of the
#            data stored in it!

%rec: Task
%key: Summary
%type: Summary line
%type: Kind enum
+ ENH (Enhancement) OPT (Optimization) BUG (Work on a bug fix)
%type: Component enum
+ Build         (The build system)
+ Language      (Poke, the language)
+ Compiler      (The PKL compiler)
+ RAS           (Our dear Retarded Assembler)
+ PVM           (The Poke Virtual Machine)
+ Runtime       (Poke standard library and other runtime stuff)
+ Library       (Libpoke)
+ MI            (The Machine Interface)
+ Emacs         (Emacs related stuff, poke.el, poke-mode.el, etc)
+ IO            (The IO subsystem)
+ POKE          (poke, the program)
+ CLI           (Commands and interface)
+ Pickles       (Everything under pickles/)
+ Documentation (The manual and other docs)
+ Testsuite     (Everything under testsuite/)
+ GUI           (Everything under gui/)
+ Other         (Kitchen sink)
+ Someday       (One can dream... lalala)
%mandatory: Component Kind
# A task can be related to other tasks in several ways.
#
# `Requires' is used for hard requirements, i.e. some task
# depends on the resolution of another task.
#
# `SeeAlso' is used when some task is relevant to some other
# task.
%type: Requires line
%type: SeeAlso line
%type: Assignee rec Hacker
# A task that fixes a bug should refer to the corresponding bug in the
# sourceware bugzilla.
%type: BZ int
%constraint: (Kind = "BUG") => #BZ
# What version this task is scheduled for.
%type: Target rec Release
# Whether this task requires discussion in the mailing list.
%type: RFC bool
%summary: Kind,Component,Summary

Summary: Array deintegrators
Component: PVM
Component: Compiler
Kind: ENH
Description:
+ Integrators are already done.  Deintegrators are pending.
Target: 3.0

Summary: Documentation for poked
Component: Documentation
Kind: ENH
Description:
+ New chapter in poke.texi explaining how to write interfaces
+ to poke using poked.
Target: 3.0

Summary: Tests for poked
Kind: ENH
Component: Testsuite
Description:
+ In testsuite/poke.poked.
+ But it needs Tcl machinery.

Summary: Avoid mid-instruction PVM_RAISE
Component: PVM
Kind: OPT
Description: Generate better code, branching to raise instructions
+ instead of using complex instructions containing PVM_RAISE inside.
+ This will improve code size and locality, also reducing the number
+ of taken branches.
+ A promising way to achieve this is with Jitter "subsections": ask
+ Luca Saiu.
+
+ So instead of having:
+
+ instruction sleep ()
+ begin
+  ...
+  if (status == ERROR)
+    PVM_RAISE (E_ERROR);
+ end
+
+ We have:
+
+ instruction sleep ()
+ begin
+  ...
+  PVM_PUSH_STACK (status);
+ end
+
+ And then the compiler generates:
+
+   push 100
+   push 0
+   sleep
+   bnz .noerror
+   raise E_ERROR
+ .noerror:

Summary: STRING ~ REGEXP operator
Component: Language
Kind: ENH
Description:
+ How to express regular expressions?  Using strings for them is
+ cumbersome because of the escaping of special characters.
+
+ But this could be a special case of the more general match
+ operator.
+
+   regexp ("...") -> (any)int
+ or
+   /.../ -> (any)int
+ or
+   raw strings.

Summary: struct patterns and matching operators
Component: Language
Kind: ENH
Description:
+    Foo {{ a == 2 }}
+
+ Only valid as second argument of the match operator ~.  NO: valid
+ everywhere, so the resulting closure can be passed to
+ commands/functions.  The pattern compiles to a closure, that is then
+ called.
+
+    (poke) Foo { a = 2} ~ Foo {{ a == 2 }}
+    1
+
+ Or:
+
+     Foo {{a ==2}} (Foo { a = 2 })
+
+    TYPE {{ EXP, EXP, ... }}
+
+ Where EXP is an expression that evaluates to an int<32>.  All the
+ expressions should evaluate to 1 when applied to the given struct
+ value, for the pattern operator to succeed.
+
+ ~ can also be used with: STRING ~ STRING where the second string
+ contains a regular expression.
+
+ This will lead to write the `search' command.
+
+ 18:27 <bhaible> so does it return a Packet, or does it return a
+                 boolean, or throw an exception?
+ 18:28 <jemarch> Packet {{ i > 0 }} (mypacket)
+ 18:28 <jemarch> the function returns a boolean
+ 18:28 <jemarch> if (Packet {{ i > 0 }} (mypacket)) ...
+ 18:28 <jemarch> Then I am considering adding a "match" operator ~
+ 18:29 <jemarch> which gets as operands a struct value, and a
+                 function with signature (TYPE_NAME s) INT
+ 18:29 <jemarch> so you can write:
+ 18:29 <jemarch> if (mypacket ~ Packet {{ i > 0 }}) ...
+ 18:29 <jemarch> which looks more natural
+ 18:29 <jemarch> still having the patterns to evaluate to a function
+                 is useful, because you can pass it around without
+                 having to define a new value class "pattern"
+ 18:30 <bhaible> and you will want cascaded matches to be useable in
+                 a 'switch' statement too, is that it?
+ 18:31 <jemarch> right :)
+ 18:31 <jemarch> case Packet {{ ... }}:
+ 18:31 <jemarch> case Packet {{ ... }}:
+
+ 18:37 <bhaible> I mean    ID3V1_Tag {{ get_year() == 2018 }}
+ 18:37 <jemarch> you cant
+ 18:37 <jemarch> but you don't need a pattern for that
+ 18:38 <jemarch> astruct ~ ID3V1_Tag {{ ...other expr... }} &&
+                 astruct.get_year () == 2018
+ 18:38 <bhaible> But that's where the pattern as a synonym for "type
+                 with certain conditions" would be useful
+ 18:38 <jemarch> yes but the methods are not fields
+ 18:39 <bhaible> If inside the {{ ... }} I can access the fields
+                 without a prefix, I should also be able to invoke
+                 the methods without a prefix.
+ 18:39 <jemarch> that can be arranged
+
+ Or using lambdas:
+
+ lambda (TYPE s) int: { return EXP && ...; }
+
+ NO.
+
+ This better, which is not ambiguous:
+
+         | pushlevel '{' TYPENAME '|' expression_list '}'
+                 {
+                   $$ = NULL;
+                 }
+         ;
+
+ expression_list:
+           %empty { $$ = NULL; }
+          | expression
+          | expression_list ',' expression  { $$ = NULL; }
+          ;
+
+ i.e.
+
+   { Packet | i == 0, j > 10 }

Summary: struct/union flattening
Component: Language
Kind: ENH
Description:
+ Can happen both at compile and run-time.
+
+ At compile-time, the struct/union type is modified.  Also, run-time
+ flattening is considered when emitting emit compilation errors in
+ struct references.
+
+ At run-time, sref simulates the flattening, which is not reflected in
+ the structure/union value itself.

Summary: Size attribute in struct types
Component: Language
Kind: ENH
Description:
+ Also size (which is also an implicit constraint):
+ type Bar = struct { byte hdr_sz; byte[hdr_sz] hdr; byte sz; byte[sz] data; } size (128#B)

Summary: Alignment attribute in struct types
Component: Language
Kind: ENH
Description:
+ In order for searching to be more efficient, alignment constraints
+ should be used whenever possible.  For both the struct itself, and
+ also fields.
+
+ Possible syntax:
+
+   struct (arg1, arg2)
+   {
+     int a;
+     long b;
+   }
+   alignment (8)

Summary: Support for more than one constraint per struct field
Component: Language
Kind: ENH
Description:
+ struct
+ {
+    int i : i > 1,
+            i % 2 == 0;
+ };

Summary: Named annotations in struct types
Component: Language
Kind: ENH
Description:
+   int foo : foo > 10;      /* Error constraint.  */
+   int foo :fail foo > 10; /* Equivalent to the above.  */
+   int foo :last foo > 10; /* Last/final constraint.  */
+   int foo :warning foo > 10; /* Warning.  */
+   int foo :aligned 8;  /* Alignment.  */
+
+ These constraints can be combined:
+
+   int foo : foo < 10, :warning foo == 8, :final foo == 5;
+
+ The warnings are emitted by the struct mapper function with `print',
+ and also registered in the IOS as "problematic" areas, to be displayed
+ (and listed) as such.

Summary: Support recursive definitions
Component: Language
Kind: ENH
Description:
+ This is to support recursive definitions, like it happens in the BSON
+ format:
+
+   type BSON_Code_Scope =
+   struct
+   {
+     int32 size;
+     BSON_String code;
+     BSON_Doc doc;
+     int doc;
+   };
+
+   type BSON_Doc =
+   struct
+   {
+     ...
+     struct
+     {
+       byte tag : tag == 0x0f;
+       string name;
+       BSON_Code_Scope value;
+     } js_scope;
+   }

Summary: Implement {beginning,end}-of-defun-function functions in poke-mode
Component: Emacs
Kind: ENH
Description:
+ Having functions in poke-mode capable of locating the beginning and
+ end of functions and methods will allow us to navigate thru them, and
+ also to implement support for add-log-current-defun-function.

Summary: Implement add-log-current-defun-function in poke-mode
Component: Emacs
Kind: ENH
Requires: Implement {beginning,end}-of-defun-function functions in poke-mode

Summary: Support for offline method definitions
Component: Language
Kind: ENH
Description:
+ type Foo =
+  struct
+  {
+    [...]
+  };
+
+ method Foo.bar = void:
+ {
+   [...]
+ }
+
+ Alternative syntax: Foo::bar

Summary: struct _equal methods
Component: Language
Kind: ENH
Description:
+ The default equal compares all fields, but the user may want to use a
+ different notion of equality.
+
+ This can also return 1 for > and -1 for <, but then the default
+ predicate should return < by convention to mean "not equal".
+
+   type Exception =
+     struct
+     {
+       int<32> code;
+       string msg;
+
+       defun _equal = int: (Exception b)
+       {
+         return code == b.code;
+       }
+     };
Requires: Support recursive definitions
Requires: Support for offline method definitions

Summary: Support a pipe IOS
Component: Language
Kind: ENH
Description:
+ Something like:
+
+   var ios = open ("|cmd")
+
+ This could return an array of two IO descriptors, one read-only stream
+ connected to the standard output of |cmd.  And one write-only stream
+ connected to the standard input of |cmd.
+
+ That runs CMD in a subprocess.  Reading from IOS results in accessing
+ the process' standard output, and writing to IOS results in writing to
+ the process' standard input.
+
+ This should leverage the existing Stream IOD.
+ Modeled after the Tcl open(3tcl)
RFC: yes

Summary: Make endianness a type quality
Component: Language
Kind: ENH
Description:
+ To: Bruno Haible <bruno@clisp.org>
+ Cc: poke-devel@gnu.org
+ Subject: Re: Generalizing the big/little attribute
+ From: "Jose E. Marchesi" <jemarch@gnu.org>
+ Gcc: nnfolder+archive:sent.2020-07
+ --text follows this line--
+
+ Hi Bruno!
+ Sorry for the delay in replying to this.
+
+ First of all, thank you very much for your suggestions.  There is a lot
+ to improve/complete in Poke's design!  Much appreciated :)
+
+     Currently, big/little applies only integral struct fields:
+
+     deftype test =
+       struct
+       {
+         little uint32 a;                        // OK
+         little uint8[3] b;                      // ERROR
+         little struct { uint8 x; uint8 y; } c;  // ERROR
+       };
+
+     A simple generalization to make it apply to integral types, then
+
+     deftype test =
+       struct
+       {
+         little uint32 a;                        // OK
+         little uint8[3] b;                      // would be OK
+         little struct { uint8 x; uint8 y; } c;  // ERROR
+       };
+
+     However, another generalization would be more powerful:
+     [...]
+
+ The big/little attribute, as you mention, is currently associated with
+ struct fields.  It is not associated with integral types.  Implementing
+ the "simple generalization" would involve adding a new attribute to
+ struct types, with the endianness to use.  This would be easy.
+
+ However, at this point I would generalize the endianness in a way it
+ becomes an attribute of certain types, not struct fields.  This way, you
+ could write:
+
+ deftype MSBInt32 = big int<32>;
+ deftype Foo = little struct { MSBInt32 magic_in_big; ... };
+
+ This would involve changes in the type system (like, two integer types
+ with different endianness are different types) but I don't think it will
+ require a lot of work.
+
+     So, in any place where you can use a type name, you could also add a
+     little/big attribute, and it has a recursive effect.
+
+ Using the approach of associating endianness to types, it would be
+ natural to support a new type specifier `{little,big} typename', that
+ constructs a proper (derived) type itself.
+
+ Then we could use `{little,big} typename' anywhere a type specifier is
+ expected, like casts, function formal arguments, etc.
+
+     Method invocations would not only have an implicit SELF argument, but
+     maybe also an implicit ENDIANNESS_OVERRIDE argument?
+
+ Good idea
RFC: yes

Summary: Support for sets (enums and bitmasks)
Component: Language
Kind: ENH
Description:
+ set NAME = enum { ... };
+ set NAME = bitmask { ... };
+
+ NAME<int<32>> field;
+
+ enums should be visible in PVM
+ tagged integrals: tag is a reference to an enum.
+ pvm_print_val knows this, and prints: DW_const_type (0x23)
+ Setting checks for valid value.
RFC: yes

Summary: Variable-length integers
Component: Language
Kind: ENH
Description:
+ - Similar syntax than fixed-width integers:
+   int<EXP>, uint<EXP>, where EXP are non constant expressions.
+ - Type specifiers: int<*> and uint<*>.
+ - Built on top of long<64> and ulong<64> PVM values?
+ - Casts: u?int<*> -> u?int<N>, u?int<N> -> u?int<*>
RFC: yes

Summary: Support for big numbers
Component: Language
Kind: ENH
Description: Currently the PVM supports the following kind of integer values:
+
+    Integers up to 32-bit, signed and unsigned
+      These values are unboxed.
+    Long integers, up to 64-bit, signed and unsigned
+      These values are boxed.
+
+ The task is to introduce an additional kind of integer:
+
+    Big integers, up to infinite bits, signed and unsigned
+     These values are boxed.
+
+ This new integer shall be implemented as a boxed value, using the GNU
+ MultiPrecision library ``libgmp``.  This task involves:
+
+ - Adding the support to ``src/pvm-val.[ch]``.
+ - Add new instruction to ``src/pvm.jitter``, i.e. itob, btol, etc.
+ - Adapt the compiler to support big integer literals.
+ - Adapt the code generator to use the new instructions.
RFC: yes

Summary: Auto-complete $<...> in the prompt
Component: CLI
Kind: ENH
Description:
+ $<...> should auto-complete to the handler of some of the
+ open IO spaces.

Summary: Auto-complete fields foo->bar
Component: Library
Kind: ENH
Description:
+ libpoke.c:complete_struct shall be adapted in order
+ to auto-complete foo->bar in a similar way it auto-completes
+ foo.bar.

Summary: Support arguments in type constructors
Component: Language
Kind: ENH
Description:
+ struct Packet (t1 f1, ..., tn fn) { ... }
+
+   Packet (a1, ..., an) packet;
+
+ or
+
+   Packet<a1,...,an> packet;
+
+ The second option is more coherent with the constructors like int<16>.
+ But it will require a lexical trick TYPE<.
RFC: yes

Summary: Add error recovery support to the compiler
Component: Compiler
Kind: ENH
Description:
+ - Ability to emit more than one error message.
+ - Passes and phases can replace trees with ERROR_MARK nodes in the
+   AST.
+ - Further passes and phases ignore ERROR_MARK nodes.
+ - At some points a pass is invoked that checks for ERROR_MARK nodes
+   and emit error messages and then abort the compilation.


Summary: Constant-fold array trims
Component: Compiler
Kind: OPT

Summary: Constant-fold array casts
Component: Compiler
Kind: OPT

Summary: Constant-fold `isa' expressions
Component: Compiler
Kind: OPT

Summary: Constant-fold array concatenation
Component: Compiler
Kind: OPT

Summary: Constant-fold struct references
Component: Compiler
Kind: OPT

Summary: Constant-fold struct integrations
Component: Compiler
Kind: OPT

Summary: Constant-fold operations with integral structs
Component: Compiler
Kind: OPT
Description: such as +, -, etc

Summary: Avoid subpasses by introducing additional AST nodes
Component: Compiler
Kind: ENH
Description:
+ The presence of a subpass in a compiler phase handler sometimes
+ indicates a problem in the design of the AST.  These subpasses can be
+ eliminated by introducing extra nodes in the AST.
+
+ This task is about identifying these cases and to remove the
+ corresponding subpasses.
RFC: yes

Summary: Move checks from anal2 to anal1 whenever feasible
Component: Compiler
Kind: ENH
Description:
+ In general checks in anal2 that rely on attributes of children nodes
+ can be moved to anal1.
+
+ This task is about identifying these cases, and to move the
+ corresponding check to anal1.

Summary: Do tail recursion optimization in the compiler
Component: Compiler
Kind: ENH

Summary: Support lazy mapping
Component: PVM
Kind: ENH
Description:
+ In complete arrays, the size of the elements is known at compile-time.
+ Then, it is not required to calculate it by peeking the element from
+ IO.  Lazy mapping is therefore possible.
+
+ Size annotations will be needed in structs in cases where it is not
+ possible for the compiler to determine the size by:
+
+   struct { ... } size(24#B)
+
+ Because of data integrity, lazy mapping can only be done when mapping
+ non-strict values or for array types whose elements do not have
+ constraints in them.
RFC: yes

Summary: Save a nton instruction in pkl_asm_while
Component: Compiler
Kind: OPT
Description:
+ - This also requires removing the promo for loop conditions
+ - Ditto for `where' conditions
+ - This requires changes in ras! :/
+
+ diff --git a/ChangeLog b/ChangeLog
+ index a37da464..5a592409 100644
+ --- a/ChangeLog
+ +++ b/ChangeLog
+ @@ -1,3 +1,11 @@
+ +2020-07-24  Jose E. Marchesi  <jemarch@gnu.org>
+ +
+ +	* libpoke/pkl-asm.h (pkl_asm_while): Get a new argument
+ +	`condition'.
+ +	* libpoke/pkl-asm.c (pkl_asm_while): Likewise.
+ +	(pkl_asm_while_loop): Generate BZ instead of BZI.
+ +	(pkl_asm_while_endloop): Cleanup node1.
+ +
+  2020-07-24  Jose E. Marchesi  <jemarch@gnu.org>
+
+  	* libpoke/pvm-val.h (PVM_VAL_TYP_I_SIGNED_P): Renamed from
+ diff --git a/libpoke/pkl-asm.c b/libpoke/pkl-asm.c
+ index 4d2f08dc..048782d7 100644
+ --- a/libpoke/pkl-asm.c
+ +++ b/libpoke/pkl-asm.c
+ @@ -1712,10 +1712,11 @@ pkl_asm_endloop (pkl_asm pasm)
+     Thus, loops use two labels.  */
+
+  void
+ -pkl_asm_while (pkl_asm pasm)
+ +pkl_asm_while (pkl_asm pasm, pkl_ast_node condition)
+  {
+    pkl_asm_pushlevel (pasm, PKL_ASM_ENV_LOOP);
+
+ +  pasm->level->node1 = ASTREF (condition);
+    pasm->level->label1 = pvm_program_fresh_label (pasm->program);
+    pasm->level->label2 = pvm_program_fresh_label (pasm->program);
+    pasm->level->break_label = pvm_program_fresh_label (pasm->program);
+ @@ -1726,7 +1727,9 @@ pkl_asm_while (pkl_asm pasm)
+  void
+  pkl_asm_while_loop (pkl_asm pasm)
+  {
+ -  pkl_asm_insn (pasm, PKL_INSN_BZI, pasm->level->label2);
+ +  pkl_asm_insn (pasm, PKL_INSN_BZ,
+ +                PKL_AST_TYPE (pasm->level->node1),
+ +                pasm->level->label2);
+    /* Pop the loop condition from the stack.  */
+    pkl_asm_insn (pasm, PKL_INSN_DROP);
+  }
+ @@ -1743,6 +1746,7 @@ pkl_asm_while_endloop (pkl_asm pasm)
+    pvm_program_append_label (pasm->program, pasm->level->break_label);
+
+    /* Cleanup and pop the current level.  */
+ +  pkl_ast_node_free (pasm->level->node1);
+    pkl_asm_poplevel (pasm);
+  }
+
+ diff --git a/libpoke/pkl-asm.h b/libpoke/pkl-asm.h
+ index b217b850..1968734c 100644
+ --- a/libpoke/pkl-asm.h
+ +++ b/libpoke/pkl-asm.h
+ @@ -141,7 +141,7 @@ void pkl_asm_endloop (pkl_asm pasm)
+
+  /* While loops.
+   *
+ - * pkl_asm_while (pasm);
+ + * pkl_asm_while (pasm, condition);
+   *
+   *   ... condition ...
+   *
+ @@ -151,11 +151,12 @@ void pkl_asm_endloop (pkl_asm pasm)
+   *
+   * pkl_asm_while_endloop (pasm);
+   *
+ - * Note that the code in `... condition ...' should result in an
+ - * int<32> value, and this is assumed by pkl_asm_while_loop.
+ + * Note that the CONDITION expression can be of any integral type.
+ + * The macro-assembler will generate the right code for the specific
+ + * type.
+   */
+
+ -void pkl_asm_while (pkl_asm pasm)
+ +void pkl_asm_while (pkl_asm pasm, pkl_ast_node condition)
+    __attribute__ ((visibility ("hidden")));
+
+  void pkl_asm_while_loop (pkl_asm pasm)

Summary: Adopt GCC's ChangeLog generation machinery
Component: Build
Kind: ENH

Summary: Union fields should not refer to other fields
Component: Compiler
Kind: BUG
BZ: 26884
Description:
+ A field in an union type cannot refer to other union alternatives, in
+ any case.  This applies to constraints, array bounds, etc.
+
+ For example: type Foo = union { int i; int j : i != 10; }

Summary: Remove call to `abort' in pkl-lex.l
Component: Compiler
Kind: BUG
BZ: 26893
Description:
+ Code in libpoke should not call `abort' nor `exit'.


Summary: Complete C and Poke printers for function types
Component: PVM
Kind: BUG
BZ: 26895
Description:
+ pvm_print_val should print valid type descriptors for function types,
+ including optional arguments, varargs, etc.
+
+ Ditto for the PVM printer and formatter.


Summary: Function types are generated with its arguments reversed
Component: Compiler
Kind: BUG
BZ: 26896
Description:
+ Either mktyc should get its arguments reversed (ugly) or the compiler
+ should reverse them before calling the instruction.


Summary: Validate u?int and u?long args in RAS
Component: RAS
Kind: ENH
Description:
+ This should be reported as invalid by RAS: int<40>

Summary: Support for string properties
Component: PVM
Kind: ENH
Description:
+ In areas of a string:
+
+ Styling classes.
+ Hyperlinks/actions.
+
+ This should be visible at the PVM level.  Instructions to set string
+ properties:
+
+    push "Age: 10"
+    push 5LU
+    push 2LU
+    push "integer"
+    sprop ; String propertize  ( STR IDX LENGTH CLASS -- STR )
RFC: yes


Summary: pvm_print_val should emit hyperlinks in collapsed structs
Component: PVM
Kind: ENH
Description:
+     > What about:
+     >
+     >   item_hdr=St_Item_Hdr {...}
+     >
+     > That makes it more explicit that St_Item_Hdr is a struct, and also makes
+     > it easier to glimpse that it can be further expanded.  WDYT?
+
+     No objections and it is indeed a nice improvement.
+
+ I just had an idea.
+
+ It would be awesome if you would generate a terminal hyperlink on the
+ `TypeName {...}' part, that would generate an `e' command to evaluate
+ the field.  This would make using the tree shallow mode very comfy!
+
+ Example:
+
+ (poke) .file foo.o
+ (poke) load elf
+ (poke) .set tree-print shallow
+ (poke) Elf64_Ehdr @ 0#B
+ Elf64_Ehdr {
+    e_ident=struct {...},
+            ------------
+    e_type=0x1UH,
+    e_machine=0x3eUH,
+    e_version=0x1U,
+    e_entry=0x0UL#B,
+    e_phoff=0x0UL#B,
+    e_shoff=0x208UL#B,
+    e_flags=0x0U,
+    e_ehsize=0x40UH#B,
+    e_phentsize=0x0UH#B,
+    e_phnum=0x0UH,
+    e_shentsize=0x40UH#B,
+    e_shnum=0xbUH,
+    e_shstrndx=0xaUH
+  }
+
+ Above, the ---- underlines the hyperlink that would be generated in the
+ terminal.  Then, when the user clicks it, it will execute:
+
+ (poke) (Elf64_Ehdr @ 0#B).e_ident
+ struct {
+    ei_mag=[0x7fUB,0x45UB,0x4cUB,0x46UB],
+    ei_class=0x2UB,
+    ei_data=0x1UB,
+    ei_version=0x1UB,
+    ei_osabi=0x0UB,
+    ei_abiversion=0x0UB,
+    ei_pad=[0x0UB,0x0UB,0x0UB,0x0UB,0x0UB,0x0UB],
+    ei_nident=0x0UB#B
+  }

Summary: Location tracking in PVM
Component: PVM
Kind: ENH
Description:
+ The PVM shall be expanded with new instructions for location tracking.
+ Something like::
+
+   pushloc file,line,column
+   setloc line,column
+   poploc
+
+ If you want to work in this, please start a discussion in
+ ``poke-devel`` so we can design a suitable set of instructions.

Summary: Make the PVM aware of units
Component: PVM
Kind: ENH
Description:
+ The poke compiler allows the user to define her own units using the
+ ``unit`` construct.  The PVM, however, is oblivious of this, and it
+ only knows how to print the names of the standard units.  This is
+ achieved by replicating the logic of the units in the
+ ``print_unit_name`` function.
+
+ We really want to make the PVM aware of units.  This would allow us to:
+
+ 1. Print the name of user-defined units when printing offsets.
+
+ 2. Avoid logic replication in ``print_unit_name`` and the set of
+    ``PVM_VAL_OFF_UNIT_*`` constants defined in ``pvm-val.h``.
+
+ A _better_ way to do this is without impacting the PVM:
+
+   _pkl_unit_names -> array of [ULONG STR] elements...
+
+  Compiler generates:
+   defunit: add new entry to the beginning of _pkl_unit_names.
+   End of scope where unit is defined: remove first matching entry from
+     _pkl_unit_names.  Or mark as invalid.
+
+  Run-time option to switch this on and off. ounames -> output unit names.



Summary: Unbox small strings in the PVM
Component: PVM
Kind: OPT
Description:
+ String values are boxed in the PVM.  A cool optimization would be to,
+ somehow, treat small strings less than 7 or 8 characters as unboxed
+ values.
RFC: yes

Summary: Add `strtok' to the standard library
Component: Runtime
Kind: ENH
Description:
+ defun strok = (string s, string sep) char[]:
+ Returns an array of tokens.
RFC: yes


Summary: Support IO transactions
Component: IO
Kind: ENH
Description:
+ (poke) ... poke something ...
+ (poke*) ... poke something more ...
+ (poke*) .changes
+ - 000043: 0034 aabb
+ + 000043: ffff ffff
+ (poke*) .commit
+ Written 4 #B to IO foo.o
+
+ (poke*) .rollback

Summary: Generalized endianness support
Component: IO
Kind: ENH
Description:
+ .set word-size N (defaults to 1 byte)
+ .set group-size N (defaults to 0 with means infinite)
+ .set endian {big,ittle} (sets the word endianness)
+ set group-endian {big,little} (sets the group endianness)
+ normal users will just use .set endian {big,little}

Summary: Support Poke IOS Translators
Component: IO
Kind: ENH
Description:
+ This requires support for specifying method implementations in struct
+ constructors.
+
+ The goal of this project is to being able to "translate" an IO space
+ using Poke code.  Example:
+
+   var my_translator
+     = IOS_Translate { readp = my_readp, writep = my_writep ... };
+
+   var outer = open ("disk.img");
+   var inner = translate (outer, my_translator);
+
+ In this scenario, the methods in ``my_translator`` are invoked when IO
+ mapping is performed in ``inner``::
+
+   int @ inner : 0#B -> calls my_read
+
+ When the methods in ``my_translator`` run, the current IOS is ``outer``.
+
+ The ``IOS_Translate`` type shall be defined in ``std.pk`` and
+ basically implements the IOD interface in ``ios-dev.h`:
+
+   type IOS_Translate =
+    struct
+    {
+      type IOS_ReadFunc = ...;
+      type IOS_WriteFunc = ...;
+
+      IOS_ReadFunc readp = ...;
+      IOS_WriteFunc writep = ...;
+    };
+
+ This of course depends on support for setting methods in struct
+ constructors.

Summary: Modeline and toolbar
Component: CLI
Kind: ENH
Description:
+ -- ./foo.o [elf]
+ (poke)
+ -- ./foo.o [elf]
+ (poke)
+
+ Customizable:
+
+ %m -> comma-separated list of maps.
+ %i -> IO space name
+
+ mode_line_format = "-- %i [%m]"
+
+ Styling classes:
+
+ mode-line
+ mode-line-map-list
+ mode-line-ios
+
+ Can have a toolbar with hyperlinks, associated with Poke functions!
RFC: yes

Summary: pk-term-query
Component: CLI
Kind: ENH
Description:
+ Query:
+
+    char *pk_term_query (const char *prompt,
+                         int (*valid_p) (const char *input),
+                         complete_fn)
+
+ Based on readline.
+ Ctrl-C cancels query, and makes pk_term_query return NULL.
+ Language-level support.
+
+ Predefined functions:
+
+    char *pk_term_query_yesno (const char *prompt);
+
+ Integration in cmd:
+
+    cmd subcmd1 subcmd2 arg1, arg2
+
+    Interactive selection of subcommands and input of command arguments:
+
+    (poke) cmd
+    a) subcmd1a
+    b) subcmd1b
+    c) subcmd1c
+    subcommand? _
+    (poke) cmd subcmd1b _
+    a) subcmd2a
+    b) subcmd2b
+    subcommand? _
+    (poke) cmd subcmd1b subcmd2a _
+    arg1? _
+    arg2? _
+    ... execute command ...
+
+ Real example:
+
+    (poke) .map entry add
+    map? foobarbaz
+    map? mp3
+    variable? foo
+    ios (optional)? 2
+    [map: mp3, variable: foo, ios: 2]  Input correct? yes
+    (poke) .map show mp3
+    Offset        Entry
+    ...           mp3::foo
+    (poke)
+
+ Note how above foobarbaz is an invalid entry, i.e. a map that doesn't
+ exist.
+
+ The confirmation query is optional and configurable by the user.
+
+ Two alternatives:
+ 1) Use the information in struct pk_cmd entries to guide the
+    interactive process.
+ 2) To add an entry in struct pk_cmd with a function that guides the
+    interactive process.
+
+ We can use 2) if the function is specified, and fallback to 1) if the
+ function is NULL.
RFC: yes

Summary: Auto-complete map names in .map
Component: CLI
Kind: ENH

Summary: Routine to get an unique memory IO space name
Component: IO
Kind: ENH
Description: uniquify.el
+
+     forward
+     reverse
+     post-forward
+     post-forward-angle-brackets
+
+ Support all three styles:
+ .set uniquify-style (forward|reverse|post-forward|post-forward-angle-brackets)
+ This is for recognizing names in #"...." constructs.
+
+    #"foo.o"
+
+ For this we need auto-completion to work well.
RFC: yes

Summary: .vm time
Component: CLI
Kind: ENH
Description:
+ This command will measure and show the time spent to execute some
+ given expression/statement.  The computed time shall be decomposed
+ into several components:
+
+ - Time spent in the compiler.
+ - Time spent in the PVM.
+  - Total amount of time.

Summary: Write RFC 8794 (ebml) pickle
Component: Pickles
Kind: ENH

Summary: Write pickles for unicode
Component: Pickles
Kind: ENH
Description: unicode.pk  (also handling ucs encodings)
+ utf8.pk
+ utf16.pk
+
+ Manual for libunistring:
+ http://www.gnu.org/software/libunistring/manual/libunistring.html
+
+ - unicode_character_name (UCS4) string:
+ - display width (for printing)
+ - We don't need explicit check functions because that logic shall be
+   implemented in the UTF8 type definitions (constraints.)
+ - Conversion functions (utf* -> utf*)
+ - mblen functions are not needed because the logic is implemented as part
+   of the mapping.
+ - ditto for the *cpy functions.
+ - ditto for the *move functions.
+ - ditto for the *mbsnlen functions.
+ - ditto for *next and *prev.
+ - ditto for *strlen.
+ - Comparison functions are useful (for sorting for example.)
RFC: yes

Summary: Write pickle for mseed (geological formats)
Component: Pickles
Kind: ENH
Description:
+ And data encodings for the payloads: GEOSCOPE, STEIM-1 and STEIM-2
+ encoded integers, etc.

Summary: Write pickle for tar files (PAX)
Component: Pickles
Kind: ENH

Summary: parallelize the dg testsuite
Component: Testsuite
Kind: ENH
Description:
+ The dg testsuites are big.  We need to be able to run the tests in
+ parallel.  A good place where to look for inspiration for this task is
+ the GCC testing infrastructure.


Summary: Document poke CSS classes in the manual
Component: Documentation
Kind: ENH

Summary: Complete PVM values and PKL nodes support in poke-gdb.scm
Component: Other
Kind: ENH

Summary: Document "default values" in the poke manual
Component: Documentation
Kind: ENH
Description:
+ These are the values generated by the constructors of the several
+ types (integers, offsets, strings, arrays, structs).  Refer to this
+ section from array and struct constructor sections.
Target: 3.0

Summary: add mutex to avoid concurrency crash with hyperlinks
Component: POKE
Kind: ENH
Description:
+ Until we figure out how to implement concurrency in Poke, we have to
+ avoid running nested PVMs, which happens easily with hyperlinks:
+
+   hserver_print_hl ('i', "crashme", "", lambda void: { print "die\n"; });
+   print "\n";
+
+   while (1)
+     {
+       print ("looping peacefully...");
+       sleep (1);
+     }
+
+ Jitter detects this and gives an error.  We need a mutex to protect
+ the PVM, and make the hserver and the prompt to use it.


Summary: support casts from `any' to function types
Kind: ENH
Component: Compiler
Component: PVM
Description:
+ Right now we are not allowing casting from `any' to a function type.
+ Supporting this requires to tag the PVM closure values with their
+ corresponding type.


%rec: Release
%key: Version
%type: Version regexp /^[0-9]+\.[0-9]+$/
%type: ReleaseDate date

Version: 1.0
ReleaseDate: 2021-02-26

Version: 1.1
ReleaseDate: 2021-03-21

Version: 1.2
ReleaseDate: 2021-04-18

Version: 1.3
ReleaseDate: 2021-06-05

Version: 1.4
ReleaseDate: 2021-12-03

Version: 2.0
ReleaseDate: 2022-01-27

Version: 2.1
ReleaseDate: 2022-02-07

Version: 2.2
ReleaseDate: 2022-03-29

Version: 2.3
ReleaseDate: 2022-03-31

Version: 2.4
ReleaseDate: 2022-07-25

Version: 3.0
ReleaseDate: 2023-01-25

Version: 3.1
ReleaseDate: 2023-04-17

%rec: Hacker
%key: Email

Name: Jose E. Marchesi
Email: jemarch@gnu.org

Name: Egeyar Bagcioglu
Email: egeyar@gmail.com

Name: John Darrington
Email: jmd@gnu.org

Name: Luca Saiu
Email: positron@gnu.org

Name: Darshit Shah
Email: darnir@gnu.org

Name: Dan Čermák
Email: dan.cermak@cgc-instruments.com

Name: Carlo Caione
Email: ccaione@baylibre.com

Name: Eric Blake
Email: eblake@redhat.com

Name: Tim Ruehsen
Email: tim.ruehsen@gmx.de

Name: Aurélien Aptel
Email: aaptel@suse.com

Name: Mohammad-Reza Nabipoor
Email: mnabipoor@gnu.org

Name: David Faust
Email: david.faust@oracle.com

Name: Indu Bhagat
Email: indu.bhagat@oracle.com

Name: Matt Ihlenfield
Email: mtihlenfield@protonmail.com

Name: Arsen Arsenović
Email: arsen@aarsen.me
