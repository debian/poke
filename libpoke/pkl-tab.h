/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_PKL_TAB_PKL_TAB_TAB_H_INCLUDED
# define YY_PKL_TAB_PKL_TAB_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef PKL_TAB_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define PKL_TAB_DEBUG 1
#  else
#   define PKL_TAB_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define PKL_TAB_DEBUG 1
# endif /* ! defined YYDEBUG */
#endif  /* ! defined PKL_TAB_DEBUG */
#if PKL_TAB_DEBUG
extern int pkl_tab_debug;
#endif

/* Token kinds.  */
#ifndef PKL_TAB_TOKENTYPE
# define PKL_TAB_TOKENTYPE
  enum pkl_tab_tokentype
  {
    PKL_TAB_EMPTY = -2,
    PKL_TAB_EOF = 0,               /* "end of file"  */
    PKL_TAB_error = 256,           /* error  */
    PKL_TAB_UNDEF = 257,           /* "invalid token"  */
    INTEGER = 258,                 /* "integer literal"  */
    LEXER_EXCEPTION = 259,         /* LEXER_EXCEPTION  */
    CHAR = 260,                    /* "character literal"  */
    STR = 261,                     /* "string"  */
    IDENTIFIER = 262,              /* "identifier"  */
    TYPENAME = 263,                /* "type name"  */
    UNIT = 264,                    /* "offset unit"  */
    OFFSET = 265,                  /* "offset"  */
    ASM = 266,                     /* "keyword `asm'"  */
    ENUM = 267,                    /* "keyword `enum'"  */
    PINNED = 268,                  /* "keyword `pinned'"  */
    STRUCT = 269,                  /* "keyword `struct'"  */
    UNION = 270,                   /* "keyword `union'"  */
    CONST = 271,                   /* "keyword `const'"  */
    CONTINUE = 272,                /* "keyword `continue'"  */
    ELSE = 273,                    /* "keyword `else'"  */
    IF = 274,                      /* "keyword `if'"  */
    WHILE = 275,                   /* "keyword `while'"  */
    UNTIL = 276,                   /* "keyword `until'"  */
    FOR = 277,                     /* "keyword `for'"  */
    IN = 278,                      /* "keyword `in'"  */
    WHERE = 279,                   /* "keyword `where'"  */
    SIZEOF = 280,                  /* "keyword `sizeof'"  */
    TYPEOF = 281,                  /* "keyword `typeof'"  */
    ASSERT = 282,                  /* "keyword `assert'"  */
    APUSH = 283,                   /* "keyword `apush'"  */
    APOP = 284,                    /* "keyword `apop'"  */
    ERR = 285,                     /* "token"  */
    ALIEN = 286,                   /* ALIEN  */
    INTCONSTR = 287,               /* "int type constructor"  */
    UINTCONSTR = 288,              /* "uint type constructor"  */
    OFFSETCONSTR = 289,            /* "offset type constructor"  */
    DEFUN = 290,                   /* "keyword `fun'"  */
    DEFSET = 291,                  /* "keyword `defset'"  */
    DEFTYPE = 292,                 /* "keyword `type'"  */
    DEFVAR = 293,                  /* "keyword `var'"  */
    DEFUNIT = 294,                 /* "keyword `unit'"  */
    METHOD = 295,                  /* "keyword `method'"  */
    RETURN = 296,                  /* "keyword `return'"  */
    BREAK = 297,                   /* "keyword `break'"  */
    STRING = 298,                  /* "string type specifier"  */
    TRY = 299,                     /* "keyword `try'"  */
    CATCH = 300,                   /* "keyword `catch'"  */
    RAISE = 301,                   /* "keyword `raise'"  */
    VOID = 302,                    /* "void type specifier"  */
    ANY = 303,                     /* "any type specifier"  */
    PRINT = 304,                   /* "keyword `print'"  */
    PRINTF = 305,                  /* "keyword `printf'"  */
    LOAD = 306,                    /* "keyword `load'"  */
    LAMBDA = 307,                  /* "keyword `lambda'"  */
    FORMAT = 308,                  /* "keyword `format'"  */
    COMPUTED = 309,                /* "keyword `computed'"  */
    IMMUTABLE = 310,               /* IMMUTABLE  */
    POWA = 311,                    /* "power-and-assign operator"  */
    MULA = 312,                    /* "multiply-and-assign operator"  */
    DIVA = 313,                    /* "divide-and-assing operator"  */
    MODA = 314,                    /* "modulus-and-assign operator"  */
    ADDA = 315,                    /* "add-and-assing operator"  */
    SUBA = 316,                    /* "subtract-and-assign operator"  */
    SLA = 317,                     /* "shift-left-and-assign operator"  */
    SRA = 318,                     /* "shift-right-and-assign operator"  */
    BANDA = 319,                   /* "bit-and-and-assign operator"  */
    XORA = 320,                    /* "bit-xor-and-assign operator"  */
    IORA = 321,                    /* "bit-or-and-assign operator"  */
    RANGEA = 322,                  /* "range separator"  */
    OR = 323,                      /* "logical or operator"  */
    AND = 324,                     /* "logical and operator"  */
    IMPL = 325,                    /* "logical implication operator"  */
    EQ = 326,                      /* "equality operator"  */
    NE = 327,                      /* "inequality operator"  */
    LE = 328,                      /* "less-or-equal operator"  */
    GE = 329,                      /* "bigger-or-equal-than operator"  */
    SL = 330,                      /* "left shift operator"  */
    SR = 331,                      /* "right shift operator"  */
    CEILDIV = 332,                 /* "ceiling division operator"  */
    POW = 333,                     /* "power operator"  */
    BCONC = 334,                   /* "bit-concatenation operator"  */
    NSMAP = 335,                   /* "non-strict map operator"  */
    INC = 336,                     /* "increment operator"  */
    DEC = 337,                     /* "decrement operator"  */
    AS = 338,                      /* "cast operator"  */
    ISA = 339,                     /* "type identification operator"  */
    IND = 340,                     /* "indirection operator"  */
    ATTR = 341,                    /* "attribute"  */
    UNMAP = 342,                   /* "unmap operator"  */
    REMAP = 343,                   /* "remap operator"  */
    EXCOND = 344,                  /* "conditional on exception operator"  */
    BIG = 345,                     /* "keyword `big'"  */
    LITTLE = 346,                  /* "keyword `little'"  */
    SIGNED = 347,                  /* "keyword `signed'"  */
    UNSIGNED = 348,                /* "keyword `unsigned'"  */
    THREEDOTS = 349,               /* "varargs indicator"  */
    THEN = 350,                    /* THEN  */
    UNARY = 351,                   /* UNARY  */
    HYPERUNARY = 352,              /* HYPERUNARY  */
    START_EXP = 353,               /* START_EXP  */
    START_DECL = 354,              /* START_DECL  */
    START_STMT = 355,              /* START_STMT  */
    START_PROGRAM = 356            /* START_PROGRAM  */
  };
  typedef enum pkl_tab_tokentype pkl_tab_token_kind_t;
#endif

/* Value type.  */
#if ! defined PKL_TAB_STYPE && ! defined PKL_TAB_STYPE_IS_DECLARED
union PKL_TAB_STYPE
{
#line 370 "pkl-tab.y"

  pkl_ast_node ast;
  struct
  {
    pkl_ast_node constraint;
    pkl_ast_node initializer;
    int impl_constraint_p;
  } field_const_init;
  enum pkl_ast_op opcode;
  int integer;
  char *exception_msg;

#line 186 "pkl-tab.h"

};
typedef union PKL_TAB_STYPE PKL_TAB_STYPE;
# define PKL_TAB_STYPE_IS_TRIVIAL 1
# define PKL_TAB_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined PKL_TAB_LTYPE && ! defined PKL_TAB_LTYPE_IS_DECLARED
typedef struct PKL_TAB_LTYPE PKL_TAB_LTYPE;
struct PKL_TAB_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define PKL_TAB_LTYPE_IS_DECLARED 1
# define PKL_TAB_LTYPE_IS_TRIVIAL 1
#endif




int pkl_tab_parse (struct pkl_parser *pkl_parser);


#endif /* !YY_PKL_TAB_PKL_TAB_TAB_H_INCLUDED  */
