/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Substitute the type names.  */
#define YYSTYPE         PKL_TAB_STYPE
#define YYLTYPE         PKL_TAB_LTYPE
/* Substitute the variable and function names.  */
#define yyparse         pkl_tab_parse
#define yylex           pkl_tab_lex
#define yyerror         pkl_tab_error
#define yydebug         pkl_tab_debug
#define yynerrs         pkl_tab_nerrs

/* First part of user prologue.  */
#line 35 "pkl-tab.y"

#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <xalloc.h>
#include <assert.h>
#include <string.h>
#include <gettext.h>
#define _(str) gettext (str)

#include "pk-utils.h"

#include "pkl.h"
#include "pkl-diag.h"
#include "pkl-ast.h"
#include "pkl-parser.h" /* For struct pkl_parser.  */

#include "pvm.h"

#define PKL_TAB_LTYPE pkl_ast_loc
#define YYDEBUG 1
#include "pkl-tab.h"
#include "pkl-lex.h"

#ifdef PKL_DEBUG
# include "pkl-gen.h"
#endif

#define scanner (pkl_parser->scanner)

/* YYLLOC_DEFAULT -> default code for computing locations.  */

#define PKL_AST_CHILDREN_STEP 12


/* Emit an error.  */

static void
pkl_tab_error (YYLTYPE *llocp,
               struct pkl_parser *pkl_parser,
               char const *err)
{
    pkl_error (pkl_parser->compiler, pkl_parser->ast, *llocp, "%s", err);
}

/* These are used in the defun_or_method rule.  */

#define IS_DEFUN 0
#define IS_METHOD 1

/* Register an argument in the compile-time environment.  This is used
   by function specifiers and try-catch statements.

   Return 0 if there was an error registering, 1 otherwise.  */

static int
pkl_register_arg (struct pkl_parser *parser, pkl_ast_node arg)
{
  pkl_ast_node arg_decl;
  pkl_ast_node arg_identifier = PKL_AST_FUNC_ARG_IDENTIFIER (arg);

  pkl_ast_node dummy
    = pkl_ast_make_integer (parser->ast, 0);
  PKL_AST_TYPE (dummy) = ASTREF (PKL_AST_FUNC_ARG_TYPE (arg));

  arg_decl = pkl_ast_make_decl (parser->ast,
                                PKL_AST_DECL_KIND_VAR,
                                arg_identifier,
                                dummy,
                                NULL /* source */);
  PKL_AST_LOC (arg_decl) = PKL_AST_LOC (arg);

  if (!pkl_env_register (parser->env,
                         PKL_ENV_NS_MAIN,
                         PKL_AST_IDENTIFIER_POINTER (arg_identifier),
                         arg_decl))
    {
      pkl_error (parser->compiler, parser->ast,PKL_AST_LOC (arg_identifier),
                 "duplicated argument name `%s' in function declaration",
                 PKL_AST_IDENTIFIER_POINTER (arg_identifier));
      /* Make sure to pop the function frame.  */
      parser->env = pkl_env_pop_frame (parser->env);
      return 0;
    }

  return 1;
}

/* Assert statement is a syntactic sugar that transforms to invocation
   of _pkl_assert function with appropriate arguments.

   This function accepts AST nodes corresponding to the condition and
   optional message of the assert statement, and also the location info
   of the statement.

   Returns NULL on failure, and expression statement AST node on success.  */

static pkl_ast_node
pkl_make_assertion (struct pkl_parser *parser, pkl_ast_node cond, pkl_ast_node msg,
                    struct pkl_ast_loc stmt_loc)
{
  pkl_ast_node vfunc, call, call_arg;
  /* _pkl_assert args */
  pkl_ast_node arg_cond, arg_msg, arg_fname, arg_line, arg_col;

  /* Make variable for `_pkl_assert` function */
  {
    const char *name = "_pkl_assert";
    pkl_ast_node vfunc_init;
    int back, over;

    vfunc_init = pkl_env_lookup (parser->env, PKL_ENV_NS_MAIN, name, &back, &over);
    if (!vfunc_init
        || (PKL_AST_DECL_KIND (vfunc_init) != PKL_AST_DECL_KIND_FUNC))
      {
        pkl_error (parser->compiler, parser->ast, stmt_loc, "undefined function '%s'",
                   name);
        return NULL;
      }
    vfunc = pkl_ast_make_var (parser->ast,
                              pkl_ast_make_identifier (parser->ast, name),
                              vfunc_init, back, over);
  }

  /* First argument of _pkl_assert: condition */
  arg_cond = pkl_ast_make_funcall_arg (parser->ast, cond, NULL);
  PKL_AST_LOC (arg_cond) = PKL_AST_LOC (cond);

  /* Second argument of _pkl_assert: user message */
  if (msg == NULL)
    {
      /* Use the source code of the condition as the user message.  */
      struct pkl_ast_loc exp_loc = PKL_AST_LOC (cond);
      char *loc_source = pkl_loc_to_source (parser, exp_loc, 80);
      char *escaped_loc_source = NULL;

      /* Escape backslash characters in loc_source, since we are using
         it in a Poke string literal.  */
      {
        size_t escaped_size = 0, i, j;

        for (i = 0; i < strlen (loc_source); ++i)
          escaped_size += (loc_source[i] == '\\' ? 2 : 1);

        escaped_loc_source = malloc (escaped_size + 1);

        for (i = 0, j = 0; i < strlen (loc_source); ++i)
          {
            if (loc_source[i] == '\\')
              {
                escaped_loc_source[j++] = '\\';
                escaped_loc_source[j++] = '\\';
              }
            else
              escaped_loc_source[j++] = loc_source[i];
          }
        escaped_loc_source[j] = '\0';
      }

      msg = pkl_ast_make_string (parser->ast, escaped_loc_source);
      free (loc_source);
      free (escaped_loc_source);
      PKL_AST_TYPE (msg) = ASTREF (pkl_ast_make_string_type (parser->ast));
    }
  arg_msg = pkl_ast_make_funcall_arg (parser->ast, msg, NULL);
  arg_msg = ASTREF (arg_msg);
  PKL_AST_LOC (arg_msg) = PKL_AST_LOC (msg);

  /* Third argument of _pkl_assert: file name */
  {
    pkl_ast_node fname
        = pkl_ast_make_string (parser->ast,
                               parser->filename ? parser->filename : "<stdin>");

    PKL_AST_TYPE (fname) = ASTREF (pkl_ast_make_string_type (parser->ast));
    arg_fname = pkl_ast_make_funcall_arg (parser->ast, fname, NULL);
    arg_fname = ASTREF (arg_fname);
  }

  /* Fourth argument of _pkl_assert: line */
  {
    pkl_ast_node line = pkl_ast_make_integer (parser->ast, stmt_loc.first_line);

    PKL_AST_TYPE (line) = ASTREF (pkl_ast_make_integral_type (parser->ast, 64, 0));
    arg_line = pkl_ast_make_funcall_arg (parser->ast, line, NULL);
    arg_line = ASTREF (arg_line);
  }

  /* Fifth argument of _pkl_assert: column */
  {
    pkl_ast_node col = pkl_ast_make_integer (parser->ast, stmt_loc.first_column);

    PKL_AST_TYPE (col) = ASTREF (pkl_ast_make_integral_type (parser->ast, 64, 0));
    arg_col = pkl_ast_make_funcall_arg (parser->ast, col, NULL);
    arg_col = ASTREF (arg_col);
  }

  call_arg = pkl_ast_chainon (arg_line, arg_col);
  call_arg = pkl_ast_chainon (arg_fname, call_arg);
  call_arg = pkl_ast_chainon (arg_msg, call_arg);
  call_arg = pkl_ast_chainon (arg_cond, call_arg);
  call = pkl_ast_make_funcall (parser->ast, vfunc, call_arg);
  return pkl_ast_make_exp_stmt (parser->ast, call);
}

#if 0
/* Register a list of arguments in the compile-time environment.  This
   is used by function specifiers and try-catch statements.

   Return 0 if there was an error registering, 1 otherwise.  */

static int
pkl_register_args (struct pkl_parser *parser, pkl_ast_node arg_list)
{
  pkl_ast_node arg;

  for (arg = arg_list; arg; arg = PKL_AST_CHAIN (arg))
    {
      pkl_ast_node arg_decl;
      pkl_ast_node arg_identifier = PKL_AST_FUNC_ARG_IDENTIFIER (arg);

      pkl_ast_node dummy
        = pkl_ast_make_integer (parser->ast, 0);
      PKL_AST_TYPE (dummy) = ASTREF (PKL_AST_FUNC_ARG_TYPE (arg));

      arg_decl = pkl_ast_make_decl (parser->ast,
                                    PKL_AST_DECL_KIND_VAR,
                                    arg_identifier,
                                    dummy,
                                    NULL /* source */);
      PKL_AST_LOC (arg_decl) = PKL_AST_LOC (arg);

      if (!pkl_env_register (parser->env,
                             PKL_ENV_NS_MAIN,
                             PKL_AST_IDENTIFIER_POINTER (arg_identifier),
                             arg_decl))
        {
          pkl_error (parser->compiler, parser->ast, PKL_AST_LOC (arg_identifier),
                     "duplicated argument name `%s' in function declaration",
                     PKL_AST_IDENTIFIER_POINTER (arg_identifier));
          /* Make sure to pop the function frame.  */
          parser->env = pkl_env_pop_frame (parser->env);
          return 0;
        }
    }

  return 1;
}
#endif

/* Register N dummy entries in the compilation environment.  */

static void
pkl_register_dummies (struct pkl_parser *parser, int n)
{
  int i;
  for (i = 0; i < n; ++i)
    {
      char *name;
      pkl_ast_node id;
      pkl_ast_node decl;
      int r;

      asprintf (&name, "@*UNUSABLE_OFF_%d*@", i);
      id = pkl_ast_make_identifier (parser->ast, name);
      decl = pkl_ast_make_decl (parser->ast,
                                PKL_AST_DECL_KIND_VAR,
                                id, NULL /* initial */,
                                NULL /* source */);

      r = pkl_env_register (parser->env, PKL_ENV_NS_MAIN, name, decl);
      assert (r);
    }
}

/* Load a module, given its name.
   If the module file cannot be read, return 1.
   If there is a parse error loading the module, return 2.
   Otherwise, return 0.  */

static int
load_module (struct pkl_parser *parser,
             const char *module, pkl_ast_node *node,
             int filename_p, char **filename)
{
  char *module_filename = NULL;
  pkl_ast ast;
  FILE *fp;

  module_filename = pkl_resolve_module (parser->compiler,
                                        module,
                                        filename_p);
  if (module_filename == NULL)
    /* No file found.  */
    return 1;

  fp = fopen (module_filename, "rb");
  if (!fp)
    {
      free (module_filename);
      return 1;
    }

  /* Parse the file, using the given environment.  The declarations
     found in the parsed file are appended to that environment, so
     nothing extra should be done about that.  */
  if (pkl_parse_file (parser->compiler, &parser->env, &ast, fp,
                      module_filename)
      != 0)
    {
      fclose (fp);
      free (module_filename);
      return 2;
    }

  /* However, the AST nodes shall be appended explicitly, which is
     achieved by returning them to the caller in the NODE
     argument.  */
  *node = PKL_AST_PROGRAM_ELEMS (ast->ast);

  /* Dirty hack is dirty, but it works.  */
  PKL_AST_PROGRAM_ELEMS (ast->ast) = NULL;
  pkl_ast_free (ast);

  /* Set the `filename' output argument if needed.  */
  if (filename)
    *filename = strdup (module_filename);

  fclose (fp);
  free (module_filename);
  return 0;
}


#line 413 "pkl-tab.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "pkl-tab.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_INTEGER = 3,                    /* "integer literal"  */
  YYSYMBOL_LEXER_EXCEPTION = 4,            /* LEXER_EXCEPTION  */
  YYSYMBOL_CHAR = 5,                       /* "character literal"  */
  YYSYMBOL_STR = 6,                        /* "string"  */
  YYSYMBOL_IDENTIFIER = 7,                 /* "identifier"  */
  YYSYMBOL_TYPENAME = 8,                   /* "type name"  */
  YYSYMBOL_UNIT = 9,                       /* "offset unit"  */
  YYSYMBOL_OFFSET = 10,                    /* "offset"  */
  YYSYMBOL_ASM = 11,                       /* "keyword `asm'"  */
  YYSYMBOL_ENUM = 12,                      /* "keyword `enum'"  */
  YYSYMBOL_PINNED = 13,                    /* "keyword `pinned'"  */
  YYSYMBOL_STRUCT = 14,                    /* "keyword `struct'"  */
  YYSYMBOL_UNION = 15,                     /* "keyword `union'"  */
  YYSYMBOL_CONST = 16,                     /* "keyword `const'"  */
  YYSYMBOL_CONTINUE = 17,                  /* "keyword `continue'"  */
  YYSYMBOL_ELSE = 18,                      /* "keyword `else'"  */
  YYSYMBOL_IF = 19,                        /* "keyword `if'"  */
  YYSYMBOL_WHILE = 20,                     /* "keyword `while'"  */
  YYSYMBOL_UNTIL = 21,                     /* "keyword `until'"  */
  YYSYMBOL_FOR = 22,                       /* "keyword `for'"  */
  YYSYMBOL_IN = 23,                        /* "keyword `in'"  */
  YYSYMBOL_WHERE = 24,                     /* "keyword `where'"  */
  YYSYMBOL_SIZEOF = 25,                    /* "keyword `sizeof'"  */
  YYSYMBOL_TYPEOF = 26,                    /* "keyword `typeof'"  */
  YYSYMBOL_ASSERT = 27,                    /* "keyword `assert'"  */
  YYSYMBOL_APUSH = 28,                     /* "keyword `apush'"  */
  YYSYMBOL_APOP = 29,                      /* "keyword `apop'"  */
  YYSYMBOL_ERR = 30,                       /* "token"  */
  YYSYMBOL_ALIEN = 31,                     /* ALIEN  */
  YYSYMBOL_INTCONSTR = 32,                 /* "int type constructor"  */
  YYSYMBOL_UINTCONSTR = 33,                /* "uint type constructor"  */
  YYSYMBOL_OFFSETCONSTR = 34,              /* "offset type constructor"  */
  YYSYMBOL_DEFUN = 35,                     /* "keyword `fun'"  */
  YYSYMBOL_DEFSET = 36,                    /* "keyword `defset'"  */
  YYSYMBOL_DEFTYPE = 37,                   /* "keyword `type'"  */
  YYSYMBOL_DEFVAR = 38,                    /* "keyword `var'"  */
  YYSYMBOL_DEFUNIT = 39,                   /* "keyword `unit'"  */
  YYSYMBOL_METHOD = 40,                    /* "keyword `method'"  */
  YYSYMBOL_RETURN = 41,                    /* "keyword `return'"  */
  YYSYMBOL_BREAK = 42,                     /* "keyword `break'"  */
  YYSYMBOL_STRING = 43,                    /* "string type specifier"  */
  YYSYMBOL_TRY = 44,                       /* "keyword `try'"  */
  YYSYMBOL_CATCH = 45,                     /* "keyword `catch'"  */
  YYSYMBOL_RAISE = 46,                     /* "keyword `raise'"  */
  YYSYMBOL_VOID = 47,                      /* "void type specifier"  */
  YYSYMBOL_ANY = 48,                       /* "any type specifier"  */
  YYSYMBOL_PRINT = 49,                     /* "keyword `print'"  */
  YYSYMBOL_PRINTF = 50,                    /* "keyword `printf'"  */
  YYSYMBOL_LOAD = 51,                      /* "keyword `load'"  */
  YYSYMBOL_LAMBDA = 52,                    /* "keyword `lambda'"  */
  YYSYMBOL_FORMAT = 53,                    /* "keyword `format'"  */
  YYSYMBOL_COMPUTED = 54,                  /* "keyword `computed'"  */
  YYSYMBOL_IMMUTABLE = 55,                 /* IMMUTABLE  */
  YYSYMBOL_POWA = 56,                      /* "power-and-assign operator"  */
  YYSYMBOL_MULA = 57,                      /* "multiply-and-assign operator"  */
  YYSYMBOL_DIVA = 58,                      /* "divide-and-assing operator"  */
  YYSYMBOL_MODA = 59,                      /* "modulus-and-assign operator"  */
  YYSYMBOL_ADDA = 60,                      /* "add-and-assing operator"  */
  YYSYMBOL_SUBA = 61,                      /* "subtract-and-assign operator"  */
  YYSYMBOL_SLA = 62,                       /* "shift-left-and-assign operator"  */
  YYSYMBOL_SRA = 63,                       /* "shift-right-and-assign operator"  */
  YYSYMBOL_BANDA = 64,                     /* "bit-and-and-assign operator"  */
  YYSYMBOL_XORA = 65,                      /* "bit-xor-and-assign operator"  */
  YYSYMBOL_IORA = 66,                      /* "bit-or-and-assign operator"  */
  YYSYMBOL_RANGEA = 67,                    /* "range separator"  */
  YYSYMBOL_OR = 68,                        /* "logical or operator"  */
  YYSYMBOL_AND = 69,                       /* "logical and operator"  */
  YYSYMBOL_IMPL = 70,                      /* "logical implication operator"  */
  YYSYMBOL_71_bit_wise_or_operator_ = 71,  /* "bit-wise or operator"  */
  YYSYMBOL_72_bit_wise_xor_operator_ = 72, /* "bit-wise xor operator"  */
  YYSYMBOL_73_bit_wise_and_operator_ = 73, /* "bit-wise and operator"  */
  YYSYMBOL_EQ = 74,                        /* "equality operator"  */
  YYSYMBOL_NE = 75,                        /* "inequality operator"  */
  YYSYMBOL_LE = 76,                        /* "less-or-equal operator"  */
  YYSYMBOL_GE = 77,                        /* "bigger-or-equal-than operator"  */
  YYSYMBOL_78_less_than_operator_ = 78,    /* "less-than operator"  */
  YYSYMBOL_79_bigger_than_operator_ = 79,  /* "bigger-than operator"  */
  YYSYMBOL_SL = 80,                        /* "left shift operator"  */
  YYSYMBOL_SR = 81,                        /* "right shift operator"  */
  YYSYMBOL_82_addition_operator_ = 82,     /* "addition operator"  */
  YYSYMBOL_83_subtraction_operator_ = 83,  /* "subtraction operator"  */
  YYSYMBOL_84_multiplication_operator_ = 84, /* "multiplication operator"  */
  YYSYMBOL_85_division_operator_ = 85,     /* "division operator"  */
  YYSYMBOL_CEILDIV = 86,                   /* "ceiling division operator"  */
  YYSYMBOL_87_modulus_operator_ = 87,      /* "modulus operator"  */
  YYSYMBOL_POW = 88,                       /* "power operator"  */
  YYSYMBOL_BCONC = 89,                     /* "bit-concatenation operator"  */
  YYSYMBOL_90_map_operator_ = 90,          /* "map operator"  */
  YYSYMBOL_NSMAP = 91,                     /* "non-strict map operator"  */
  YYSYMBOL_INC = 92,                       /* "increment operator"  */
  YYSYMBOL_DEC = 93,                       /* "decrement operator"  */
  YYSYMBOL_AS = 94,                        /* "cast operator"  */
  YYSYMBOL_ISA = 95,                       /* "type identification operator"  */
  YYSYMBOL_96_dot_operator_ = 96,          /* "dot operator"  */
  YYSYMBOL_IND = 97,                       /* "indirection operator"  */
  YYSYMBOL_ATTR = 98,                      /* "attribute"  */
  YYSYMBOL_UNMAP = 99,                     /* "unmap operator"  */
  YYSYMBOL_REMAP = 100,                    /* "remap operator"  */
  YYSYMBOL_EXCOND = 101,                   /* "conditional on exception operator"  */
  YYSYMBOL_BIG = 102,                      /* "keyword `big'"  */
  YYSYMBOL_LITTLE = 103,                   /* "keyword `little'"  */
  YYSYMBOL_SIGNED = 104,                   /* "keyword `signed'"  */
  YYSYMBOL_UNSIGNED = 105,                 /* "keyword `unsigned'"  */
  YYSYMBOL_THREEDOTS = 106,                /* "varargs indicator"  */
  YYSYMBOL_THEN = 107,                     /* THEN  */
  YYSYMBOL_108_ = 108,                     /* '?'  */
  YYSYMBOL_109_ = 109,                     /* ':'  */
  YYSYMBOL_UNARY = 110,                    /* UNARY  */
  YYSYMBOL_HYPERUNARY = 111,               /* HYPERUNARY  */
  YYSYMBOL_START_EXP = 112,                /* START_EXP  */
  YYSYMBOL_START_DECL = 113,               /* START_DECL  */
  YYSYMBOL_START_STMT = 114,               /* START_STMT  */
  YYSYMBOL_START_PROGRAM = 115,            /* START_PROGRAM  */
  YYSYMBOL_116_ = 116,                     /* ','  */
  YYSYMBOL_117_ = 117,                     /* ';'  */
  YYSYMBOL_118_ = 118,                     /* '('  */
  YYSYMBOL_119_ = 119,                     /* ')'  */
  YYSYMBOL_120_ = 120,                     /* '~'  */
  YYSYMBOL_121_ = 121,                     /* '!'  */
  YYSYMBOL_122_ = 122,                     /* ".>"  */
  YYSYMBOL_123_ = 123,                     /* '['  */
  YYSYMBOL_124_ = 124,                     /* ']'  */
  YYSYMBOL_125_ = 125,                     /* '{'  */
  YYSYMBOL_126_ = 126,                     /* '}'  */
  YYSYMBOL_127_ = 127,                     /* '='  */
  YYSYMBOL_YYACCEPT = 128,                 /* $accept  */
  YYSYMBOL_pushlevel = 129,                /* pushlevel  */
  YYSYMBOL_start = 130,                    /* start  */
  YYSYMBOL_program = 131,                  /* program  */
  YYSYMBOL_program_elem_list = 132,        /* program_elem_list  */
  YYSYMBOL_program_elem = 133,             /* program_elem  */
  YYSYMBOL_load = 134,                     /* load  */
  YYSYMBOL_integer = 135,                  /* integer  */
  YYSYMBOL_identifier = 136,               /* identifier  */
  YYSYMBOL_expression_list = 137,          /* expression_list  */
  YYSYMBOL_expression_opt = 138,           /* expression_opt  */
  YYSYMBOL_expression = 139,               /* expression  */
  YYSYMBOL_bconc = 140,                    /* bconc  */
  YYSYMBOL_mapop = 141,                    /* mapop  */
  YYSYMBOL_map = 142,                      /* map  */
  YYSYMBOL_unary_operator = 143,           /* unary_operator  */
  YYSYMBOL_primary = 144,                  /* primary  */
  YYSYMBOL_145_1 = 145,                    /* $@1  */
  YYSYMBOL_funcall = 146,                  /* funcall  */
  YYSYMBOL_funcall_arg_list = 147,         /* funcall_arg_list  */
  YYSYMBOL_funcall_arg = 148,              /* funcall_arg  */
  YYSYMBOL_format_arg_list = 149,          /* format_arg_list  */
  YYSYMBOL_format_arg = 150,               /* format_arg  */
  YYSYMBOL_opt_comma = 151,                /* opt_comma  */
  YYSYMBOL_struct_field_list = 152,        /* struct_field_list  */
  YYSYMBOL_struct_field = 153,             /* struct_field  */
  YYSYMBOL_array = 154,                    /* array  */
  YYSYMBOL_array_initializer_list = 155,   /* array_initializer_list  */
  YYSYMBOL_array_initializer = 156,        /* array_initializer  */
  YYSYMBOL_pushlevel_args = 157,           /* pushlevel_args  */
  YYSYMBOL_function_specifier = 158,       /* function_specifier  */
  YYSYMBOL_function_arg_list = 159,        /* function_arg_list  */
  YYSYMBOL_function_arg = 160,             /* function_arg  */
  YYSYMBOL_function_arg_initial = 161,     /* function_arg_initial  */
  YYSYMBOL_type_specifier = 162,           /* type_specifier  */
  YYSYMBOL_typename = 163,                 /* typename  */
  YYSYMBOL_string_type_specifier = 164,    /* string_type_specifier  */
  YYSYMBOL_simple_type_specifier = 165,    /* simple_type_specifier  */
  YYSYMBOL_cons_type_specifier = 166,      /* cons_type_specifier  */
  YYSYMBOL_integral_type_specifier = 167,  /* integral_type_specifier  */
  YYSYMBOL_dynamic_integral_type_specifier = 168, /* dynamic_integral_type_specifier  */
  YYSYMBOL_integral_type_sign = 169,       /* integral_type_sign  */
  YYSYMBOL_ref_type = 170,                 /* ref_type  */
  YYSYMBOL_offset_type_specifier = 171,    /* offset_type_specifier  */
  YYSYMBOL_array_type_specifier = 172,     /* array_type_specifier  */
  YYSYMBOL_function_type_specifier = 173,  /* function_type_specifier  */
  YYSYMBOL_function_type_arg_list = 174,   /* function_type_arg_list  */
  YYSYMBOL_function_type_arg = 175,        /* function_type_arg  */
  YYSYMBOL_struct_type_specifier = 176,    /* struct_type_specifier  */
  YYSYMBOL_177_2 = 177,                    /* $@2  */
  YYSYMBOL_struct_or_union = 178,          /* struct_or_union  */
  YYSYMBOL_struct_type_pinned = 179,       /* struct_type_pinned  */
  YYSYMBOL_integral_struct = 180,          /* integral_struct  */
  YYSYMBOL_struct_type_elem_list = 181,    /* struct_type_elem_list  */
  YYSYMBOL_endianness = 182,               /* endianness  */
  YYSYMBOL_struct_type_computed_field = 183, /* struct_type_computed_field  */
  YYSYMBOL_struct_type_field = 184,        /* struct_type_field  */
  YYSYMBOL_185_3 = 185,                    /* $@3  */
  YYSYMBOL_struct_type_field_identifier = 186, /* struct_type_field_identifier  */
  YYSYMBOL_struct_type_field_label = 187,  /* struct_type_field_label  */
  YYSYMBOL_struct_type_field_constraint_and_init = 188, /* struct_type_field_constraint_and_init  */
  YYSYMBOL_struct_type_field_optcond_pre = 189, /* struct_type_field_optcond_pre  */
  YYSYMBOL_struct_type_field_optcond_post = 190, /* struct_type_field_optcond_post  */
  YYSYMBOL_simple_declaration = 191,       /* simple_declaration  */
  YYSYMBOL_declaration = 192,              /* declaration  */
  YYSYMBOL_193_4 = 193,                    /* @4  */
  YYSYMBOL_defun_or_method = 194,          /* defun_or_method  */
  YYSYMBOL_defvar_list = 195,              /* defvar_list  */
  YYSYMBOL_defvar = 196,                   /* defvar  */
  YYSYMBOL_deftype_list = 197,             /* deftype_list  */
  YYSYMBOL_deftype = 198,                  /* deftype  */
  YYSYMBOL_defunit_list = 199,             /* defunit_list  */
  YYSYMBOL_defunit = 200,                  /* defunit  */
  YYSYMBOL_comp_stmt = 201,                /* comp_stmt  */
  YYSYMBOL_stmt_decl_list = 202,           /* stmt_decl_list  */
  YYSYMBOL_ass_exp_op = 203,               /* ass_exp_op  */
  YYSYMBOL_simple_stmt_list = 204,         /* simple_stmt_list  */
  YYSYMBOL_simple_stmt = 205,              /* simple_stmt  */
  YYSYMBOL_stmt = 206,                     /* stmt  */
  YYSYMBOL_207_5 = 207,                    /* @5  */
  YYSYMBOL_208_6 = 208,                    /* @6  */
  YYSYMBOL_funcall_stmt = 209,             /* funcall_stmt  */
  YYSYMBOL_funcall_stmt_arg_list = 210,    /* funcall_stmt_arg_list  */
  YYSYMBOL_funcall_stmt_arg = 211          /* funcall_stmt_arg  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef N_
# define N_(Msgid) Msgid
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if 1

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
# define YYCOPY_NEEDED 1
#endif /* 1 */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined PKL_TAB_LTYPE_IS_TRIVIAL && PKL_TAB_LTYPE_IS_TRIVIAL \
             && defined PKL_TAB_STYPE_IS_TRIVIAL && PKL_TAB_STYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  93
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   5002

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  128
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  84
/* YYNRULES -- Number of rules.  */
#define YYNRULES  285
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  540

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   357


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   121,     2,     2,     2,    87,    73,     2,
     118,   119,    84,    82,   116,    83,    96,    85,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   109,   117,
      78,   127,    79,   108,    90,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   123,     2,   124,    72,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   125,    71,   126,   120,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    74,    75,    76,    77,
      80,    81,    86,    88,    89,    91,    92,    93,    94,    95,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   110,   111,   112,   113,   114,   115,   122
};

#if PKL_TAB_DEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   594,   594,   609,   615,   622,   628,   634,   640,   650,
     656,   662,   671,   675,   679,   680,   690,   691,   696,   697,
     701,   735,   775,   776,   791,   792,   800,   802,   803,   810,
     811,   815,   816,   822,   829,   836,   842,   848,   854,   860,
     865,   870,   876,   882,   888,   894,   900,   906,   912,   918,
     924,   930,   936,   942,   948,   954,   960,   966,   971,   976,
     982,   988,   994,  1009,  1014,  1029,  1035,  1041,  1042,  1046,
    1055,  1056,  1060,  1066,  1075,  1076,  1077,  1078,  1079,  1080,
    1084,  1113,  1119,  1125,  1131,  1139,  1140,  1146,  1152,  1159,
    1164,  1170,  1176,  1182,  1188,  1194,  1195,  1200,  1199,  1211,
    1216,  1221,  1229,  1235,  1241,  1246,  1251,  1256,  1262,  1268,
    1282,  1305,  1314,  1316,  1317,  1324,  1333,  1335,  1336,  1343,
    1351,  1352,  1356,  1358,  1359,  1366,  1373,  1384,  1395,  1396,
    1403,  1409,  1422,  1451,  1461,  1474,  1475,  1482,  1492,  1518,
    1519,  1527,  1528,  1529,  1533,  1548,  1556,  1561,  1566,  1567,
    1568,  1569,  1570,  1574,  1575,  1576,  1580,  1591,  1600,  1601,
    1605,  1606,  1610,  1642,  1653,  1659,  1667,  1674,  1684,  1685,
    1692,  1698,  1705,  1724,  1744,  1742,  1805,  1806,  1810,  1811,
    1815,  1816,  1817,  1821,  1822,  1823,  1824,  1826,  1828,  1833,
    1834,  1835,  1839,  1856,  1854,  1982,  1983,  1987,  1991,  1999,
    2005,  2012,  2019,  2027,  2035,  2045,  2049,  2057,  2061,  2073,
    2074,  2075,  2080,  2079,  2133,  2137,  2138,  2142,  2143,  2148,
    2170,  2171,  2176,  2200,  2201,  2206,  2245,  2253,  2264,  2265,
    2267,  2268,  2273,  2274,  2275,  2276,  2277,  2278,  2279,  2280,
    2281,  2282,  2283,  2287,  2288,  2289,  2294,  2300,  2311,  2317,
    2323,  2329,  2342,  2349,  2356,  2362,  2368,  2374,  2383,  2384,
    2389,  2393,  2399,  2405,  2416,  2431,  2444,  2443,  2492,  2491,
    2539,  2545,  2551,  2557,  2563,  2577,  2591,  2609,  2621,  2627,
    2633,  2639,  2655,  2664,  2665,  2672
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if 1
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  static const char *const yy_sname[] =
  {
  N_("end of file"), N_("error"), N_("invalid token"),
  N_("integer literal"), "LEXER_EXCEPTION", N_("character literal"),
  N_("string"), N_("identifier"), N_("type name"), N_("offset unit"),
  N_("offset"), N_("keyword `asm'"), N_("keyword `enum'"),
  N_("keyword `pinned'"), N_("keyword `struct'"), N_("keyword `union'"),
  N_("keyword `const'"), N_("keyword `continue'"), N_("keyword `else'"),
  N_("keyword `if'"), N_("keyword `while'"), N_("keyword `until'"),
  N_("keyword `for'"), N_("keyword `in'"), N_("keyword `where'"),
  N_("keyword `sizeof'"), N_("keyword `typeof'"), N_("keyword `assert'"),
  N_("keyword `apush'"), N_("keyword `apop'"), N_("token"), "ALIEN",
  N_("int type constructor"), N_("uint type constructor"),
  N_("offset type constructor"), N_("keyword `fun'"),
  N_("keyword `defset'"), N_("keyword `type'"), N_("keyword `var'"),
  N_("keyword `unit'"), N_("keyword `method'"), N_("keyword `return'"),
  N_("keyword `break'"), N_("string type specifier"), N_("keyword `try'"),
  N_("keyword `catch'"), N_("keyword `raise'"), N_("void type specifier"),
  N_("any type specifier"), N_("keyword `print'"), N_("keyword `printf'"),
  N_("keyword `load'"), N_("keyword `lambda'"), N_("keyword `format'"),
  N_("keyword `computed'"), "IMMUTABLE", N_("power-and-assign operator"),
  N_("multiply-and-assign operator"), N_("divide-and-assing operator"),
  N_("modulus-and-assign operator"), N_("add-and-assing operator"),
  N_("subtract-and-assign operator"), N_("shift-left-and-assign operator"),
  N_("shift-right-and-assign operator"), N_("bit-and-and-assign operator"),
  N_("bit-xor-and-assign operator"), N_("bit-or-and-assign operator"),
  N_("range separator"), N_("logical or operator"),
  N_("logical and operator"), N_("logical implication operator"),
  N_("bit-wise or operator"), N_("bit-wise xor operator"),
  N_("bit-wise and operator"), N_("equality operator"),
  N_("inequality operator"), N_("less-or-equal operator"),
  N_("bigger-or-equal-than operator"), N_("less-than operator"),
  N_("bigger-than operator"), N_("left shift operator"),
  N_("right shift operator"), N_("addition operator"),
  N_("subtraction operator"), N_("multiplication operator"),
  N_("division operator"), N_("ceiling division operator"),
  N_("modulus operator"), N_("power operator"),
  N_("bit-concatenation operator"), N_("map operator"),
  N_("non-strict map operator"), N_("increment operator"),
  N_("decrement operator"), N_("cast operator"),
  N_("type identification operator"), N_("dot operator"),
  N_("indirection operator"), N_("attribute"), N_("unmap operator"),
  N_("remap operator"), N_("conditional on exception operator"),
  N_("keyword `big'"), N_("keyword `little'"), N_("keyword `signed'"),
  N_("keyword `unsigned'"), N_("varargs indicator"), "THEN", "'?'", "':'",
  "UNARY", "HYPERUNARY", "START_EXP", "START_DECL", "START_STMT",
  "START_PROGRAM", "','", "';'", "'('", "')'", "'~'", "'!'", ".>", "'['",
  "']'", "'{'", "'}'", "'='", "$accept", "pushlevel", "start", "program",
  "program_elem_list", "program_elem", "load", "integer", "identifier",
  "expression_list", "expression_opt", "expression", "bconc", "mapop",
  "map", "unary_operator", "primary", "$@1", "funcall", "funcall_arg_list",
  "funcall_arg", "format_arg_list", "format_arg", "opt_comma",
  "struct_field_list", "struct_field", "array", "array_initializer_list",
  "array_initializer", "pushlevel_args", "function_specifier",
  "function_arg_list", "function_arg", "function_arg_initial",
  "type_specifier", "typename", "string_type_specifier",
  "simple_type_specifier", "cons_type_specifier",
  "integral_type_specifier", "dynamic_integral_type_specifier",
  "integral_type_sign", "ref_type", "offset_type_specifier",
  "array_type_specifier", "function_type_specifier",
  "function_type_arg_list", "function_type_arg", "struct_type_specifier",
  "$@2", "struct_or_union", "struct_type_pinned", "integral_struct",
  "struct_type_elem_list", "endianness", "struct_type_computed_field",
  "struct_type_field", "$@3", "struct_type_field_identifier",
  "struct_type_field_label", "struct_type_field_constraint_and_init",
  "struct_type_field_optcond_pre", "struct_type_field_optcond_post",
  "simple_declaration", "declaration", "@4", "defun_or_method",
  "defvar_list", "defvar", "deftype_list", "deftype", "defunit_list",
  "defunit", "comp_stmt", "stmt_decl_list", "ass_exp_op",
  "simple_stmt_list", "simple_stmt", "stmt", "@5", "@6", "funcall_stmt",
  "funcall_stmt_arg_list", "funcall_stmt_arg", YY_NULLPTR
  };
  /* YYTRANSLATABLE[SYMBOL-NUM] -- Whether YY_SNAME[SYMBOL-NUM] is
     internationalizable.  */
  static yytype_int8 yytranslatable[] =
  {
       1,     1,     1,     1,     0,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     0,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     0,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0
  };
  return (yysymbol < YYNTOKENS && yytranslatable[yysymbol]
          ? _(yy_sname[yysymbol])
          : yy_sname[yysymbol]);
}
#endif

#define YYPACT_NINF (-488)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-269)

#define yytable_value_is_error(Yyn) \
  ((Yyn) == YYTABLE_NINF)

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     104,  2453,   235,  1747,   776,    17,  -488,  -488,  -488,  -488,
    -488,  -488,  -488,  -488,   510,   -67,   -65,   -41,   -35,  -488,
    -488,   510,  -488,  -488,  -488,  -488,   -33,  -488,  -488,  2453,
    2453,  -488,  -488,  2453,  -488,  -488,  1976,   -16,  -488,  4453,
    -488,  -488,  2453,   -42,  -488,  -488,   -99,   -23,   -84,    -7,
    -488,   150,  -488,    43,    45,  -488,   197,   197,   197,  -488,
      46,    52,   197,   189,    73,    76,    78,    80,    95,  2075,
     103,  1852,  2137,  2453,    21,   219,  -488,   126,  4780,   123,
     129,  2411,    45,   141,   152,  -488,   235,  -488,   881,  -488,
    -488,  -488,  -488,  -488,  -488,  -488,   -53,  -488,   510,  2453,
    2453,  2453,   -10,   206,   261,   316,   316,  3319,   -76,   143,
     155,  4780,   170,  -488,   986,  -488,  2453,  2453,  2453,  2453,
    2453,  2453,  2453,  2453,  2453,  2453,  2453,  2453,  2453,  2453,
    2453,  2453,  2453,  2453,  2453,  2453,  2453,  2453,  2453,  -488,
    -488,   510,   510,   173,  2453,  2453,  -488,   203,   197,   197,
    1277,   197,  2199,  1339,  -488,  -488,  1501,  2453,  1277,   223,
    2453,  -488,  -488,   179,   193,  -488,   184,   196,  -488,   186,
     200,  -488,  -488,  -488,  -488,  2453,  -488,  2453,  2453,    31,
    2453,  -488,  4129,  -488,   -11,  -488,  4210,  4291,  1277,   311,
     202,   204,  -488,  2453,  2453,  -488,  -488,  -488,  -488,  -488,
    -488,  -488,  -488,  -488,  -488,  -488,   319,  2453,  2453,   220,
    -488,  -488,  -488,  -488,  -488,   210,  -105,  3400,   -75,  4534,
    3481,   163,  -488,  -488,   -30,  1277,  -488,  -488,  2453,  1401,
     211,  -488,  -488,  1091,  -488,   969,  4820,  4901,  4780,  1730,
     864,  2545,  1617,  1617,  1387,  1387,  1387,  1387,  1956,  1956,
     577,   577,   153,   153,   153,   153,   286,   391,   207,   207,
    2453,  1617,  4615,  -488,  -488,  4780,    -9,  -488,  -488,  1563,
    2590,   209,   -61,   216,  4780,   231,  -488,  -488,  2671,  4657,
     232,  4780,  -488,  1617,   256,   197,  2453,   197,  2453,   197,
     222,  3076,  3562,  3643,   327,  2320,   172,  3157,  -488,  2453,
     -15,  -488,  -488,  4780,   118,  -488,  1277,  -488,  -488,  4780,
    4780,  2453,  4780,  4780,  -488,  2453,  -488,  -488,  -488,  2453,
    -488,   236,   236,   503,  -488,    62,  2752,  -488,  -488,  -488,
    -488,  -488,  3724,  2453,  2453,  -488,  -488,  2833,  2453,  1625,
    -488,  2453,  1153,   225,  -488,  2453,  2332,   237,    41,   341,
    -488,   207,  -488,  -488,  -488,  4780,  -488,  4780,  -488,   206,
    1277,  -488,  1852,  1852,  2453,   241,  4780,   245,  2453,  -488,
    4372,  2453,  -488,  -488,  2453,  -488,    72,  4780,  3238,  3805,
     510,   285,   287,   259,   252,   260,    15,  -488,  -488,   258,
    -488,  4861,  -488,  -488,  2914,  -488,  2995,  4780,  -488,  -488,
    4780,  4780,  -488,  -488,   510,   -71,   253,   267,  -488,   251,
    -488,   -59,   350,  -488,  4780,  1215,  2320,  3886,  -488,  4780,
     503,  -488,  -488,  1277,  -488,  -488,   207,  -488,  -488,  -488,
     510,   503,   262,  -488,  2453,  -488,  -488,   207,  -488,   510,
     249,  -488,  -488,   510,  1277,  2453,  -488,  1852,   362,   272,
      84,  -488,   274,  -488,  -488,   275,   114,   -27,  -488,  2453,
    -488,  4780,   207,  -488,   207,  -488,     8,   268,   136,  -488,
     276,   372,  1914,  1852,  1215,  -488,  -488,  -488,  4780,   320,
     278,  -488,  1852,  2453,  -488,  -488,   144,  -488,  -488,  -488,
    -488,   145,  -488,  3967,  1852,   280,   256,    24,  -488,  -488,
     174,  -488,  1852,  -488,  2453,   197,  -488,  -488,  -488,  -488,
    -488,  -488,   256,  -488,  4048,   288,   197,  -488,  -488,  -488,
    -488,   -69,  2453,  2453,  2453,   322,  4780,  2509,  4738,  2453,
     387,  2453,  2453,  4780,  2453,   299,  4780,  4780,  4780,  -488
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int16 yydefact[] =
{
       0,     2,     0,     2,     2,     0,    22,    23,    82,    83,
      80,   144,    62,    63,     0,     0,     0,     0,     0,   158,
     159,     0,   145,   147,   146,    97,     0,    75,    74,     2,
       2,    78,    79,     2,    76,    77,     2,     0,    81,     3,
      67,    68,     2,    31,    95,    85,   148,   152,     0,     0,
     149,     0,   150,   151,     0,   215,     0,     0,     0,   216,
       0,     5,     0,     0,     0,     0,     0,     0,     0,     2,
       0,     2,     2,     2,     0,     0,   259,     9,   250,    67,
      68,    31,   258,     0,     7,   254,     0,    11,     2,    14,
      19,    16,    18,     1,   148,   152,     0,   151,     0,     2,
       2,     2,     0,     0,     0,    65,    66,     0,    31,     0,
       0,   130,   120,   128,     2,    64,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,   102,
     103,     0,     0,    33,     2,     2,     4,    32,     0,     0,
     112,     0,     2,   122,    70,    71,     2,     2,    26,     0,
       2,    25,    24,     0,   210,   220,     0,   209,   217,     0,
     211,   223,   214,     6,   212,     2,   271,     2,     2,     2,
       2,   272,     0,   270,     0,   278,     0,     0,   116,     0,
       0,     0,    10,     2,     2,   232,   233,   234,   235,   236,
     237,   238,   239,   240,   242,   241,     0,     2,     2,   282,
     283,   260,     8,    17,    15,     0,     0,     0,     0,     0,
       0,     0,   132,    98,     0,   116,    84,    96,     2,     2,
       0,   226,   230,     2,   228,    56,    54,    53,    55,    50,
      51,    52,    44,    45,    48,    49,    46,    47,    42,    43,
      35,    36,    37,    38,    39,    41,    40,    69,    57,    58,
       2,    61,     0,    87,    88,   115,     0,   113,    86,     2,
       0,    80,   144,     0,   125,   120,   123,   164,     0,    72,
     120,    27,   156,    60,     2,     0,     2,     0,     2,     0,
       0,     0,     0,     0,     0,     2,     0,     0,   273,     2,
       2,   279,   280,   119,     0,   117,   116,    21,    20,   248,
     249,     2,   246,   247,   284,     2,   106,   104,   105,     2,
     108,   160,   160,     0,   132,     0,     0,   129,   127,   227,
     231,   229,     0,     2,     2,   111,    92,     0,     2,     2,
      89,     2,     2,     0,   165,     2,     2,     0,     0,   178,
     222,   141,   143,   142,   221,   219,   218,   225,   224,     0,
      26,   255,     2,     2,     2,     0,    30,     0,     2,   252,
       0,     2,     2,   274,     2,   281,     0,   285,     0,     0,
       0,     0,     0,     0,     0,   135,     0,     2,   101,     0,
      34,    59,   114,    93,     0,    94,     0,   126,   124,   110,
      73,    28,   109,   172,     0,   170,     0,   168,   179,     0,
     213,     0,   261,   263,     2,   243,     2,     0,   277,     2,
       0,   118,   251,    26,    99,   107,   161,   163,   162,   138,
       0,     0,   139,   134,     2,    90,    91,   167,   171,     0,
       0,   176,   177,   180,    26,     2,   256,     2,   266,     0,
       0,   244,     0,   253,   275,     0,     0,     0,   136,     2,
     137,   131,   166,   169,   181,   182,     0,     0,     0,   262,
       0,     0,     2,     2,   243,     2,   100,     2,   140,     0,
     174,   257,     2,     2,   245,   265,     0,   276,   133,   157,
     173,   205,   267,     0,     2,     0,     2,   205,   184,   183,
     189,   185,     2,   264,     2,     0,   175,   188,   187,   186,
     191,   190,     2,   269,     0,     0,   195,   206,   192,   196,
     193,   199,     2,     2,     2,   197,   204,   200,   201,     2,
     207,     2,     2,   198,     2,     0,   203,   202,   208,   194
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -488,  -143,  -488,  -488,  -488,   329,   416,   -48,   345,  -336,
       9,    -1,    98,  -488,   101,  -488,    -2,  -488,  -488,  -488,
      90,  -212,    53,  -135,  -488,    88,  -488,  -488,   205,   102,
      70,     0,    12,  -488,  -487,   279,   294,   181,  -488,  -488,
    -488,    -6,   116,  -488,   325,  -488,     1,  -488,  -488,  -488,
    -488,  -488,  -488,  -488,  -488,   -58,   -57,  -488,  -488,  -488,
    -488,  -488,  -488,   146,    28,  -488,  -488,  -488,   159,  -488,
     162,  -488,   164,    87,  -488,  -488,   -26,  -407,    94,  -488,
    -488,   418,  -488,   243
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
       0,    37,     5,    87,    88,    89,    90,    38,   383,   280,
     365,    78,    40,   157,    41,    42,    43,   103,    44,   266,
     267,   304,   305,   230,   275,   276,    45,   112,   113,   323,
     223,   384,   385,   460,   350,    46,    47,    48,    49,    50,
     465,    51,   381,    52,    53,   352,   406,   407,   353,   491,
     443,   409,   467,   497,   512,   498,   499,   521,   520,   530,
     525,   500,   535,    60,    91,   290,    62,   167,   168,   164,
     165,   170,   171,    54,   233,   208,   450,    83,    92,   470,
     471,    85,   209,   210
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      39,    81,    81,   159,   371,   522,   154,   155,   451,   505,
     299,     6,     7,   325,   316,   154,   155,    93,   156,  -153,
     148,   149,   161,   162,   411,   516,   153,   188,   105,   106,
      61,   108,   107,   206,   300,   111,   296,   438,   294,   156,
     523,   147,   150,   495,   318,   -24,   151,   152,   156,    11,
     444,    98,   156,    99,   148,   149,   215,   445,   524,    55,
     446,    56,    57,    58,    59,   484,   -24,   451,   182,    81,
     156,   186,   187,    19,    20,    21,   150,   100,   496,   324,
     151,   152,   477,   101,    22,   104,    81,   456,    23,    24,
      82,    82,   479,   156,   376,  -155,   156,    84,   217,   219,
     220,    79,    79,   372,    80,    80,   221,   334,   468,   114,
     335,   158,    81,   156,   213,   235,   236,   237,   238,   239,
     240,   241,   242,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,   254,   255,   256,   257,   156,   189,
     343,   349,   232,   261,   262,   347,   160,   403,   295,   265,
     506,   270,   274,     6,     7,   278,   279,   281,    82,   283,
     404,  -154,   115,   172,   495,   184,     6,     7,   173,    79,
     161,   162,    80,   321,   291,    82,   292,   293,   374,   297,
      55,   388,    56,    57,    58,    59,    79,   303,   374,    80,
     176,   422,   309,   310,   177,    96,   178,    11,   179,   496,
     472,    82,   102,   473,   161,   162,   312,   313,   234,    56,
      57,    58,    79,   180,    11,    80,     1,     2,     3,     4,
     183,    19,    20,    21,   303,   190,   191,   326,   111,   420,
     445,    81,    22,   476,   374,   375,    23,    24,    19,    20,
      21,   137,   138,   192,    96,   139,   140,   141,   142,    22,
     193,   143,   445,    23,    24,   481,   194,    11,   211,   332,
     472,   330,   227,   494,    11,   441,   442,   225,   337,   212,
      55,   448,    56,    57,    58,    59,   510,   511,   228,   216,
     218,    19,    20,    21,   224,   355,   229,   357,    19,    20,
      21,   260,    22,    94,   366,   115,    23,    24,   370,    22,
      94,   143,   282,    23,    24,   303,   284,   175,    95,   285,
     377,   286,   287,   288,   378,    95,   289,   306,   379,   307,
      82,   308,   258,   259,   222,  -269,   311,   331,   315,   206,
     156,    79,   391,   265,    80,   328,   -25,   394,   396,    97,
     397,   274,    94,   341,   400,   401,    97,   342,   346,   359,
     364,   399,   380,   349,   408,   403,   402,    95,   415,   281,
      81,    81,   416,   414,   427,   429,   428,   417,   447,   349,
     419,   430,   439,   303,   348,   138,   431,    94,   139,   140,
     141,   142,    94,   440,   143,   434,  -268,   373,    97,   459,
     189,   474,    95,   480,   475,   482,   483,    95,   504,   489,
     115,   163,   166,   169,   490,   518,   534,   174,  -269,  -269,
     141,   142,   529,    81,   143,   366,   539,   214,   159,    77,
      94,    94,   281,    97,   392,   452,   387,   421,    97,   410,
     398,   458,   455,   461,   327,    95,    95,   466,   382,   507,
     508,   463,   367,   281,   401,    81,   356,   354,   486,    82,
      82,   109,   314,   358,     0,     0,   412,   413,   478,     0,
      79,    79,     0,    80,    80,   351,    97,    97,     0,     0,
      81,    81,    81,     0,   433,     0,     0,     0,     0,     0,
      81,     0,   493,   139,   140,   141,   142,     0,     0,   143,
       0,     0,    81,   263,   264,     0,   268,     0,   273,     0,
      81,     0,     0,   514,   386,     0,   454,     0,     0,     0,
     161,   272,     0,    79,     0,     0,    80,     0,    11,   501,
       0,   526,   527,   528,     0,   509,     0,     0,   533,   405,
     536,   537,     0,   538,    82,    19,    20,    21,     0,     0,
     224,   469,    19,    20,    21,    79,    22,     0,    80,     0,
      23,    24,     0,    22,     0,     0,     0,    23,    24,     0,
      82,   426,   487,    94,   488,     0,   322,   485,     0,    82,
      79,    79,    79,    80,    80,    80,   492,     0,    95,     0,
      79,    82,     0,    80,     0,   437,   115,     0,   503,    82,
       0,     0,    79,     0,     0,    80,   513,     0,     0,     0,
      79,   386,    94,    80,     0,     0,     0,     0,     0,    97,
       0,   457,   386,     0,     0,     0,     0,    95,     0,     0,
     462,   405,     0,     0,   464,     0,     0,    94,     0,     0,
     163,     0,   166,     0,   169,     0,     0,     0,    94,     0,
       0,     0,    95,     0,     0,     0,     0,     0,    97,     0,
       0,     0,     0,    95,     0,     0,     0,     0,     0,    94,
       0,   133,   134,   135,   136,   137,   138,     0,     0,   139,
     140,   141,   142,    97,    95,   143,     0,   351,     0,     0,
       0,     0,     0,    94,    97,     0,     0,   273,     0,     0,
       0,     0,     0,   351,     0,     0,     0,     0,    95,    94,
       0,     0,     0,     0,     0,    97,     0,     0,     0,    94,
      94,     0,     0,     0,    95,     0,     0,     0,    94,    94,
       0,     0,    94,     0,    95,    95,     0,     0,     0,    97,
       0,   432,     0,    95,    95,     0,     0,    95,     0,     0,
       0,     0,     0,     0,     0,    97,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    97,    97,     0,     0,     0,
       0,     0,     0,     0,    97,    97,     0,     0,    97,     0,
       0,     0,     0,     0,     0,    94,   -12,     0,     0,     6,
       7,     8,     9,    10,    11,    12,    13,    63,     0,     0,
      95,    94,     0,    64,     0,    65,    66,     0,    67,     0,
       0,    15,    16,    68,    17,    18,    95,     0,    19,    20,
      21,    55,     0,    56,    57,    58,    59,    69,    70,    22,
      71,    97,    72,    23,    24,    73,    74,    75,    25,    26,
       0,    86,     0,     0,     0,     0,     0,    97,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     515,     0,     0,     0,     0,     0,     0,     0,    27,    28,
       0,   519,     0,     0,     0,     0,     0,     0,    29,    30,
       0,     0,     0,   115,     0,    31,    32,     0,     0,     0,
       0,   -13,     0,     0,     6,     7,     8,     9,    10,    11,
      12,    13,    63,    76,    33,     0,    34,    35,    64,    36,
      65,    66,     0,    67,     0,     0,    15,    16,    68,    17,
      18,     0,     0,    19,    20,    21,    55,     0,    56,    57,
      58,    59,    69,    70,    22,    71,     0,    72,    23,    24,
      73,    74,    75,    25,    26,     0,    86,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,     0,     0,   139,   140,   141,   142,
       0,     0,   143,    27,    28,   144,     0,     0,     0,     0,
       0,     0,     0,    29,    30,     0,     0,     0,   115,     0,
      31,    32,     0,     0,     0,     0,     0,     0,     0,     6,
       7,     8,     9,    10,    11,    12,    13,    63,    76,    33,
       0,    34,    35,    64,    36,    65,    66,     0,    67,     0,
       0,    15,    16,    68,    17,    18,     0,     0,    19,    20,
      21,    55,     0,    56,    57,    58,    59,    69,    70,    22,
      71,     0,    72,    23,    24,    73,    74,     0,    25,    26,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,     0,
       0,   139,   140,   141,   142,     0,     0,   143,    27,    28,
     144,     0,     0,     0,     0,     0,     0,     0,    29,    30,
       0,     0,     0,     0,     0,    31,    32,     0,     0,     0,
       0,     0,     0,     0,     6,     7,     8,     9,    10,    11,
      12,    13,    63,    76,    33,     0,    34,    35,    64,    36,
      65,    66,   231,    67,     0,     0,    15,    16,    68,    17,
      18,     0,     0,    19,    20,    21,    55,     0,    56,    57,
      58,    59,    69,    70,    22,    71,     0,    72,    23,    24,
      73,    74,     0,    25,    26,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     6,     7,     8,     9,
     271,   272,    12,    13,    14,     0,     0,     0,     0,     0,
       0,     0,     0,    27,    28,     0,     0,     0,    15,    16,
       0,    17,    18,    29,    30,    19,    20,    21,     0,     0,
      31,    32,     0,     0,     0,     0,    22,     0,     0,     0,
      23,    24,     0,     0,     0,    25,    26,     0,    76,    33,
       0,    34,    35,     0,    36,     0,     0,   329,     6,     7,
       8,     9,    10,    11,    12,    13,    63,     0,     0,     0,
       0,     0,     0,     0,     0,    27,    28,     0,     0,     0,
      15,    16,    68,    17,    18,    29,    30,    19,    20,    21,
       0,     0,    31,    32,     0,     0,     0,     0,    22,     0,
       0,     0,    23,    24,     0,   449,     0,    25,    26,     0,
       0,    33,     0,    34,    35,     0,    36,     0,     0,  -121,
       6,     7,     8,     9,    10,    11,    12,    13,    14,     0,
       0,     0,     0,     0,     0,     0,     0,    27,    28,     0,
       0,     0,    15,    16,     0,    17,    18,    29,    30,    19,
      20,    21,     0,     0,    31,    32,     0,     0,     0,     0,
      22,     0,     0,     0,    23,    24,     0,     0,     0,    25,
      26,     0,     0,    33,     0,    34,    35,     0,    36,     0,
      -2,     0,     6,     7,     8,     9,   271,   272,    12,    13,
      14,     0,     0,     0,     0,     0,     0,     0,     0,    27,
      28,     0,     0,     0,    15,    16,     0,    17,    18,    29,
      30,    19,    20,    21,     0,     0,    31,    32,     0,     0,
       0,     0,    22,     0,     0,     0,    23,    24,     0,     0,
       0,    25,    26,     0,     0,    33,   115,    34,    35,     0,
      36,     0,    -2,     0,     6,     7,     8,     9,    10,    11,
      12,    13,    14,     0,     0,     0,     0,     0,     0,     0,
       0,    27,    28,     0,     0,     0,    15,    16,     0,    17,
      18,    29,    30,    19,    20,    21,     0,     0,    31,    32,
       0,     0,     0,     0,    22,     0,     0,     0,    23,    24,
       0,     0,     0,    25,    26,     0,     0,    33,     0,    34,
      35,     0,    36,     0,    -2,     0,     0,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,     0,     0,   139,
     140,   141,   142,    27,    28,   143,     0,     0,     0,     0,
       0,     0,     0,    29,    30,     0,     0,   110,     0,     0,
      31,    32,     0,     0,     6,     7,     8,     9,    10,    11,
      12,    13,    14,     0,     0,     0,     0,     0,     0,    33,
       0,    34,    35,     0,    36,  -121,    15,    16,     0,    17,
      18,     0,     0,    19,    20,    21,     0,     0,     0,     0,
       0,     0,     0,     0,    22,     0,     0,     0,    23,    24,
       0,     0,     0,    25,    26,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     6,     7,     8,     9,
      10,    11,    12,    13,    14,     0,     0,     0,     0,     0,
       0,     0,     0,    27,    28,     0,     0,     0,    15,    16,
       0,    17,    18,    29,    30,    19,    20,    21,     0,     0,
      31,    32,     0,     0,     0,     0,    22,     0,     0,     0,
      23,    24,     0,     0,     0,    25,    26,     0,     0,    33,
       0,    34,    35,     0,    36,   277,   115,     0,     6,     7,
       8,     9,    10,    11,    12,    13,    14,     0,     0,     0,
       0,     0,     0,     0,     0,    27,    28,     0,     0,     0,
      15,    16,     0,    17,    18,    29,    30,    19,    20,    21,
       0,     0,    31,    32,     0,     0,     0,     0,    22,     0,
       0,     0,    23,    24,     0,     0,     0,    25,    26,     0,
       0,    33,     0,    34,    35,     0,    36,   336,     0,     0,
       0,     0,     0,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,    27,    28,   139,
     140,   141,   142,     0,     0,   143,     0,    29,    30,     0,
       0,     0,     0,     0,    31,    32,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   115,
       0,     0,     0,    33,     0,    34,    35,     0,    36,   395,
       6,     7,     8,     9,    10,    11,    12,    13,    63,     0,
       0,     0,     0,     0,    64,     0,    65,    66,     0,    67,
       0,     0,    15,    16,    68,    17,    18,     0,     0,    19,
      20,    21,     0,     0,     0,     0,     0,     0,    69,    70,
      22,    71,     0,    72,    23,    24,    73,    74,    75,    25,
      26,     0,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
       0,     0,   139,   140,   141,   142,     0,     0,   143,    27,
      28,   144,     0,     0,     0,     0,     0,     0,     0,    29,
      30,     0,     0,     0,     0,     0,    31,    32,     0,     0,
       0,     0,     0,     0,     0,     6,     7,     8,     9,    10,
      11,    12,    13,    63,    76,    33,     0,    34,    35,    64,
      36,    65,    66,     0,    67,     0,     0,    15,    16,    68,
      17,    18,     0,     0,    19,    20,    21,     0,     0,     0,
       0,     0,     0,    69,    70,    22,    71,     0,    72,    23,
      24,    73,    74,     0,    25,    26,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     6,     7,     8,
       9,    10,    11,    12,    13,    63,     0,     0,     0,     0,
       0,     0,     0,     0,    27,    28,     0,     0,     0,    15,
      16,    68,    17,    18,    29,    30,    19,    20,    21,     0,
       0,    31,    32,     0,     0,     0,     0,    22,     0,     0,
       0,    23,    24,     0,   449,   115,    25,    26,     0,    76,
      33,     0,    34,    35,     0,    36,     0,     0,     0,     6,
       7,     8,     9,    10,    11,    12,    13,    14,     0,     0,
       0,     0,     0,     0,     0,     0,    27,    28,     0,     0,
       0,    15,    16,     0,    17,    18,    29,    30,    19,    20,
      21,     0,     0,    31,    32,     0,     0,     0,     0,    22,
       0,     0,     0,    23,    24,     0,     0,     0,    25,    26,
       0,     0,    33,     0,    34,    35,     0,    36,   131,   132,
     133,   134,   135,   136,   137,   138,     0,     0,   139,   140,
     141,   142,     0,     0,   143,     0,     0,     0,    27,    28,
       0,     0,     0,     0,     0,     0,     0,     0,    29,    30,
       0,     0,   110,     0,     0,    31,    32,     0,     6,     7,
       8,     9,    10,    11,    12,    13,    14,     0,     0,     0,
       0,     0,     0,     0,    33,     0,    34,    35,     0,    36,
      15,    16,     0,    17,    18,     0,     0,    19,    20,    21,
       0,     0,     0,     0,     0,     0,     0,     0,    22,     0,
       0,     0,    23,    24,     0,     0,     0,    25,    26,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       6,     7,     8,     9,    10,    11,    12,    13,    14,     0,
       0,     0,     0,     0,     0,     0,     0,    27,    28,     0,
       0,     0,    15,    16,     0,    17,    18,    29,    30,    19,
      20,    21,     0,     0,    31,    32,     0,     0,     0,     0,
      22,     0,     0,     0,    23,    24,     0,     0,     0,    25,
      26,     0,   181,    33,     0,    34,    35,     0,    36,     0,
       0,     0,     6,     7,     8,     9,    10,    11,    12,    13,
      14,     0,     0,     0,     0,     0,     0,     0,     0,    27,
      28,     0,     0,     0,    15,    16,     0,    17,    18,    29,
      30,    19,    20,    21,     0,     0,    31,    32,     0,     0,
       0,     0,    22,     0,     0,     0,    23,    24,     0,     0,
       0,    25,    26,     0,   185,    33,     0,    34,    35,     0,
      36,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    27,    28,     0,     0,     0,     0,     0,     0,     0,
       0,    29,    30,     0,     0,     0,     0,     0,    31,    32,
       0,     0,     0,     0,     0,     0,     0,     0,   269,     0,
       0,     0,     0,     0,     0,     0,     0,    33,     0,    34,
      35,     0,    36,     6,     7,     8,     9,    10,    11,    12,
      13,    14,     0,     0,     0,     6,     7,     8,     9,    10,
      11,    12,    13,    14,     0,    15,    16,     0,    17,    18,
       0,     0,    19,    20,    21,     0,     0,    15,    16,     0,
      17,    18,     0,    22,    19,    20,    21,    23,    24,     0,
       0,     0,    25,    26,     0,    22,     0,     0,     0,    23,
      24,     0,     0,     0,    25,    26,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    27,    28,     0,     0,     0,     0,     0,     0,
       0,     0,    29,    30,    27,    28,     0,     0,     0,    31,
      32,     0,     0,     0,    29,    30,     0,     0,     0,     0,
       0,    31,    32,     0,     0,     0,     0,   -29,    33,     0,
      34,    35,     0,    36,     0,     0,     0,     0,     0,     0,
      33,  -121,    34,    35,     0,    36,     6,     7,     8,     9,
      10,    11,    12,    13,    14,     0,     0,   195,   196,   197,
     198,   199,   200,   201,   202,   203,   204,   205,    15,    16,
       0,    17,    18,     0,     0,    19,    20,    21,     0,     0,
       0,     0,     0,     0,     0,     0,    22,     0,     0,     0,
      23,    24,     0,     0,     0,    25,    26,   148,   149,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   115,     0,
     206,     0,     0,     0,     0,     0,     0,     0,     0,   150,
       0,     0,   116,   151,   152,    27,    28,     0,   207,     0,
       0,     0,     0,     0,     0,    29,    30,     0,     0,     0,
       0,     0,    31,    32,   115,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    33,     0,    34,    35,     0,    36,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,   115,
       0,   139,   140,   141,   142,     0,     0,   143,     0,     0,
     144,     0,     0,   116,     0,     0,     0,   145,     0,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,     0,   531,   139,   140,   141,
     142,     0,     0,   143,     0,     0,   144,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   338,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
     115,     0,   139,   140,   141,   142,     0,     0,   143,     0,
       0,   144,     0,     0,   116,     0,     0,     0,   145,   339,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   340,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   115,     0,   139,   140,   141,   142,     0,     0,   143,
       0,     0,   144,     0,     0,   116,     0,     0,     0,   145,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   344,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   115,     0,   139,   140,   141,   142,     0,     0,
     143,     0,     0,   144,     0,     0,   116,     0,     0,     0,
     145,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   389,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   115,     0,   139,   140,   141,   142,     0,
       0,   143,     0,     0,   144,     0,     0,   116,     0,     0,
       0,   145,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   393,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   115,     0,   139,   140,   141,   142,
       0,     0,   143,     0,     0,   144,     0,     0,   116,     0,
       0,     0,   145,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   435,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   115,     0,   139,   140,   141,
     142,     0,     0,   143,     0,     0,   144,     0,     0,   116,
       0,     0,     0,   145,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   436,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   115,     0,   139,   140,
     141,   142,     0,     0,   143,     0,     0,   144,     0,     0,
     116,     0,     0,     0,   145,   360,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   361,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   115,     0,   139,
     140,   141,   142,     0,     0,   143,     0,     0,   144,     0,
       0,   116,     0,     0,     0,   145,     0,     0,     0,     0,
       0,     0,     0,   368,     0,     0,   369,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   115,     0,
     139,   140,   141,   142,     0,     0,   143,     0,     0,   144,
       0,     0,   116,     0,     0,     0,   145,   423,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   424,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,   115,
       0,   139,   140,   141,   142,     0,     0,   143,     0,     0,
     144,     0,     0,   116,     0,     0,     0,   145,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   226,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
     115,     0,   139,   140,   141,   142,     0,     0,   143,     0,
       0,   144,     0,     0,   116,     0,     0,     0,   145,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   317,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   115,     0,   139,   140,   141,   142,     0,     0,   143,
       0,     0,   144,     0,     0,   116,     0,     0,     0,   145,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     320,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   115,     0,   139,   140,   141,   142,     0,     0,
     143,     0,     0,   144,     0,     0,   116,     0,     0,     0,
     145,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   362,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   115,     0,   139,   140,   141,   142,     0,
       0,   143,     0,     0,   144,     0,     0,   116,     0,     0,
       0,   145,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   363,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   115,     0,   139,   140,   141,   142,
       0,     0,   143,     0,     0,   144,     0,     0,   116,     0,
       0,     0,   145,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   390,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   115,     0,   139,   140,   141,
     142,     0,     0,   143,     0,     0,   144,     0,     0,   116,
       0,     0,     0,   145,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   425,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   115,     0,   139,   140,
     141,   142,     0,     0,   143,     0,     0,   144,     0,     0,
     116,     0,     0,     0,   145,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   453,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   115,     0,   139,
     140,   141,   142,     0,     0,   143,     0,     0,   144,     0,
       0,   116,     0,     0,     0,   145,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   502,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   115,     0,
     139,   140,   141,   142,     0,     0,   143,     0,     0,   144,
       0,     0,   116,     0,     0,     0,   145,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   517,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,   115,
       0,   139,   140,   141,   142,     0,     0,   143,     0,     0,
     144,     0,     0,   116,     0,     0,     0,   145,     0,     0,
       0,     0,     0,     0,     0,     0,   298,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
     115,     0,   139,   140,   141,   142,     0,     0,   143,     0,
       0,   144,     0,     0,   116,     0,     0,     0,   145,     0,
       0,     0,     0,     0,     0,     0,     0,   301,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   115,     0,   139,   140,   141,   142,     0,     0,   143,
       0,     0,   144,     0,     0,   116,     0,     0,     0,   145,
       0,     0,     0,     0,     0,     0,     0,     0,   302,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   115,     0,   139,   140,   141,   142,     0,     0,
     143,     0,     0,   144,     0,     0,   116,     0,     0,     0,
     145,     0,     0,     0,     0,     0,     0,     0,     0,   418,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   115,     0,   139,   140,   141,   142,     0,
       0,   143,     0,     0,   144,     0,     0,   116,     0,     0,
       0,   145,     0,     0,     0,     0,     0,     0,     0,   146,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   115,     0,   139,   140,   141,   142,
       0,     0,   143,     0,     0,   144,     0,     0,   116,     0,
       0,     0,   145,     0,     0,     0,     0,     0,     0,     0,
     319,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   115,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     116,     0,     0,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,     0,     0,   139,   140,   141,
     142,     0,     0,   143,     0,     0,   144,     0,     0,     0,
       0,     0,     0,   145,   333,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   115,     0,   139,
     140,   141,   142,     0,     0,   143,     0,     0,   144,     0,
       0,   116,     0,     0,     0,   145,   345,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   115,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   116,     0,     0,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,     0,   115,
     139,   140,   141,   142,     0,     0,   143,     0,     0,   144,
       0,     0,     0,   116,     0,     0,   145,   532,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
     115,     0,   139,   140,   141,   142,     0,     0,   143,     0,
       0,   144,     0,     0,   116,     0,     0,     0,   145,   118,
       0,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
     115,     0,   139,   140,   141,   142,     0,     0,   143,     0,
       0,   144,     0,     0,   116,     0,     0,     0,     0,   117,
     118,     0,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,     0,     0,   139,   140,   141,   142,     0,     0,   143,
       0,     0,   144,     0,     0,     0,     0,     0,     0,   145,
       0,     0,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,     0,     0,   139,   140,   141,   142,     0,     0,   143,
       0,     0,   144
};

static const yytype_int16 yycheck[] =
{
       1,     3,     4,    51,    19,    74,    90,    91,   415,   496,
      21,     3,     4,   225,   119,    90,    91,     0,   123,   118,
      96,    97,     7,     8,   360,   512,   125,     6,    29,    30,
       2,    33,    33,   109,    45,    36,   179,   108,     7,   123,
     109,    42,   118,    19,   119,   106,   122,   123,   123,     8,
     109,   118,   123,   118,    96,    97,   109,   116,   127,    35,
     119,    37,    38,    39,    40,   472,   127,   474,    69,    71,
     123,    72,    73,    32,    33,    34,   118,   118,    54,   109,
     122,   123,   109,   118,    43,   118,    88,   423,    47,    48,
       3,     4,    84,   123,   306,   118,   123,     3,    99,   100,
     101,     3,     4,   118,     3,     4,   116,   116,   444,   125,
     119,   118,   114,   123,    86,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   123,   118,
     275,   284,   114,   144,   145,   280,   101,   106,   117,   150,
     126,   152,   153,     3,     4,   156,   157,   158,    71,   160,
     119,   118,     9,   117,    19,    71,     3,     4,   116,    71,
       7,     8,    71,   221,   175,    88,   177,   178,   116,   180,
      35,   119,    37,    38,    39,    40,    88,   188,   116,    88,
     117,   119,   193,   194,   118,    14,   118,     8,   118,    54,
     116,   114,    21,   119,     7,     8,   207,   208,   114,    37,
      38,    39,   114,   118,     8,   114,   112,   113,   114,   115,
     117,    32,    33,    34,   225,     6,     7,   228,   229,   372,
     116,   233,    43,   119,   116,   117,    47,    48,    32,    33,
      34,    88,    89,   117,    63,    92,    93,    94,    95,    43,
     127,    98,   116,    47,    48,   119,   127,     8,   117,   260,
     116,   233,   119,   119,     8,    14,    15,     6,   269,   117,
      35,   414,    37,    38,    39,    40,   102,   103,   123,    98,
      99,    32,    33,    34,   103,   286,   116,   288,    32,    33,
      34,   118,    43,    14,   295,     9,    47,    48,   299,    43,
      21,    98,    79,    47,    48,   306,   127,   118,    14,   116,
     311,   127,   116,   127,   315,    21,   116,     6,   319,   117,
     233,   117,   141,   142,   118,     9,     7,   233,   118,   109,
     123,   233,   333,   334,   233,   124,   127,   338,   339,    14,
     341,   342,    63,   127,   345,   346,    21,   116,   116,   127,
      23,   126,   116,   496,    13,   106,   119,    63,   117,   360,
     362,   363,   117,   364,    79,   106,    79,   368,    18,   512,
     371,   119,   119,   374,   118,    89,   116,    98,    92,    93,
      94,    95,   103,   116,    98,   127,    24,   300,    63,   127,
     118,   117,    98,   125,   119,   119,    24,   103,   118,    79,
       9,    56,    57,    58,   126,   117,    19,    62,    92,    93,
      94,    95,    90,   415,    98,   416,   117,    88,   466,     3,
     141,   142,   423,    98,   334,   416,   324,   374,   103,   359,
     342,   431,   420,   434,   229,   141,   142,   443,   322,   497,
     497,   440,   296,   444,   445,   447,   287,   285,   474,   362,
     363,    33,   209,   289,    -1,    -1,   362,   363,   459,    -1,
     362,   363,    -1,   362,   363,   284,   141,   142,    -1,    -1,
     472,   473,   474,    -1,   387,    -1,    -1,    -1,    -1,    -1,
     482,    -1,   483,    92,    93,    94,    95,    -1,    -1,    98,
      -1,    -1,   494,   148,   149,    -1,   151,    -1,   153,    -1,
     502,    -1,    -1,   504,   323,    -1,   419,    -1,    -1,    -1,
       7,     8,    -1,   415,    -1,    -1,   415,    -1,     8,   491,
      -1,   522,   523,   524,    -1,   497,    -1,    -1,   529,   348,
     531,   532,    -1,   534,   447,    32,    33,    34,    -1,    -1,
     359,   447,    32,    33,    34,   447,    43,    -1,   447,    -1,
      47,    48,    -1,    43,    -1,    -1,    -1,    47,    48,    -1,
     473,   380,   475,   284,   477,    -1,   221,   473,    -1,   482,
     472,   473,   474,   472,   473,   474,   482,    -1,   284,    -1,
     482,   494,    -1,   482,    -1,   404,     9,    -1,   494,   502,
      -1,    -1,   494,    -1,    -1,   494,   502,    -1,    -1,    -1,
     502,   420,   323,   502,    -1,    -1,    -1,    -1,    -1,   284,
      -1,   430,   431,    -1,    -1,    -1,    -1,   323,    -1,    -1,
     439,   440,    -1,    -1,   443,    -1,    -1,   348,    -1,    -1,
     285,    -1,   287,    -1,   289,    -1,    -1,    -1,   359,    -1,
      -1,    -1,   348,    -1,    -1,    -1,    -1,    -1,   323,    -1,
      -1,    -1,    -1,   359,    -1,    -1,    -1,    -1,    -1,   380,
      -1,    84,    85,    86,    87,    88,    89,    -1,    -1,    92,
      93,    94,    95,   348,   380,    98,    -1,   496,    -1,    -1,
      -1,    -1,    -1,   404,   359,    -1,    -1,   342,    -1,    -1,
      -1,    -1,    -1,   512,    -1,    -1,    -1,    -1,   404,   420,
      -1,    -1,    -1,    -1,    -1,   380,    -1,    -1,    -1,   430,
     431,    -1,    -1,    -1,   420,    -1,    -1,    -1,   439,   440,
      -1,    -1,   443,    -1,   430,   431,    -1,    -1,    -1,   404,
      -1,   386,    -1,   439,   440,    -1,    -1,   443,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   420,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   430,   431,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   439,   440,    -1,    -1,   443,    -1,
      -1,    -1,    -1,    -1,    -1,   496,     0,    -1,    -1,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    -1,    -1,
     496,   512,    -1,    17,    -1,    19,    20,    -1,    22,    -1,
      -1,    25,    26,    27,    28,    29,   512,    -1,    32,    33,
      34,    35,    -1,    37,    38,    39,    40,    41,    42,    43,
      44,   496,    46,    47,    48,    49,    50,    51,    52,    53,
      -1,    55,    -1,    -1,    -1,    -1,    -1,   512,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     505,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    82,    83,
      -1,   516,    -1,    -1,    -1,    -1,    -1,    -1,    92,    93,
      -1,    -1,    -1,     9,    -1,    99,   100,    -1,    -1,    -1,
      -1,     0,    -1,    -1,     3,     4,     5,     6,     7,     8,
       9,    10,    11,   117,   118,    -1,   120,   121,    17,   123,
      19,    20,    -1,    22,    -1,    -1,    25,    26,    27,    28,
      29,    -1,    -1,    32,    33,    34,    35,    -1,    37,    38,
      39,    40,    41,    42,    43,    44,    -1,    46,    47,    48,
      49,    50,    51,    52,    53,    -1,    55,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    -1,    -1,    92,    93,    94,    95,
      -1,    -1,    98,    82,    83,   101,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    92,    93,    -1,    -1,    -1,     9,    -1,
      99,   100,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     3,
       4,     5,     6,     7,     8,     9,    10,    11,   117,   118,
      -1,   120,   121,    17,   123,    19,    20,    -1,    22,    -1,
      -1,    25,    26,    27,    28,    29,    -1,    -1,    32,    33,
      34,    35,    -1,    37,    38,    39,    40,    41,    42,    43,
      44,    -1,    46,    47,    48,    49,    50,    -1,    52,    53,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    -1,
      -1,    92,    93,    94,    95,    -1,    -1,    98,    82,    83,
     101,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    92,    93,
      -1,    -1,    -1,    -1,    -1,    99,   100,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     3,     4,     5,     6,     7,     8,
       9,    10,    11,   117,   118,    -1,   120,   121,    17,   123,
      19,    20,   126,    22,    -1,    -1,    25,    26,    27,    28,
      29,    -1,    -1,    32,    33,    34,    35,    -1,    37,    38,
      39,    40,    41,    42,    43,    44,    -1,    46,    47,    48,
      49,    50,    -1,    52,    53,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    82,    83,    -1,    -1,    -1,    25,    26,
      -1,    28,    29,    92,    93,    32,    33,    34,    -1,    -1,
      99,   100,    -1,    -1,    -1,    -1,    43,    -1,    -1,    -1,
      47,    48,    -1,    -1,    -1,    52,    53,    -1,   117,   118,
      -1,   120,   121,    -1,   123,    -1,    -1,   126,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    82,    83,    -1,    -1,    -1,
      25,    26,    27,    28,    29,    92,    93,    32,    33,    34,
      -1,    -1,    99,   100,    -1,    -1,    -1,    -1,    43,    -1,
      -1,    -1,    47,    48,    -1,    50,    -1,    52,    53,    -1,
      -1,   118,    -1,   120,   121,    -1,   123,    -1,    -1,   126,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    82,    83,    -1,
      -1,    -1,    25,    26,    -1,    28,    29,    92,    93,    32,
      33,    34,    -1,    -1,    99,   100,    -1,    -1,    -1,    -1,
      43,    -1,    -1,    -1,    47,    48,    -1,    -1,    -1,    52,
      53,    -1,    -1,   118,    -1,   120,   121,    -1,   123,    -1,
     125,    -1,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    82,
      83,    -1,    -1,    -1,    25,    26,    -1,    28,    29,    92,
      93,    32,    33,    34,    -1,    -1,    99,   100,    -1,    -1,
      -1,    -1,    43,    -1,    -1,    -1,    47,    48,    -1,    -1,
      -1,    52,    53,    -1,    -1,   118,     9,   120,   121,    -1,
     123,    -1,   125,    -1,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    82,    83,    -1,    -1,    -1,    25,    26,    -1,    28,
      29,    92,    93,    32,    33,    34,    -1,    -1,    99,   100,
      -1,    -1,    -1,    -1,    43,    -1,    -1,    -1,    47,    48,
      -1,    -1,    -1,    52,    53,    -1,    -1,   118,    -1,   120,
     121,    -1,   123,    -1,   125,    -1,    -1,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    -1,    -1,    92,
      93,    94,    95,    82,    83,    98,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    92,    93,    -1,    -1,    96,    -1,    -1,
      99,   100,    -1,    -1,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    -1,    -1,    -1,    -1,    -1,    -1,   118,
      -1,   120,   121,    -1,   123,   124,    25,    26,    -1,    28,
      29,    -1,    -1,    32,    33,    34,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    43,    -1,    -1,    -1,    47,    48,
      -1,    -1,    -1,    52,    53,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    82,    83,    -1,    -1,    -1,    25,    26,
      -1,    28,    29,    92,    93,    32,    33,    34,    -1,    -1,
      99,   100,    -1,    -1,    -1,    -1,    43,    -1,    -1,    -1,
      47,    48,    -1,    -1,    -1,    52,    53,    -1,    -1,   118,
      -1,   120,   121,    -1,   123,   124,     9,    -1,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    82,    83,    -1,    -1,    -1,
      25,    26,    -1,    28,    29,    92,    93,    32,    33,    34,
      -1,    -1,    99,   100,    -1,    -1,    -1,    -1,    43,    -1,
      -1,    -1,    47,    48,    -1,    -1,    -1,    52,    53,    -1,
      -1,   118,    -1,   120,   121,    -1,   123,   124,    -1,    -1,
      -1,    -1,    -1,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    82,    83,    92,
      93,    94,    95,    -1,    -1,    98,    -1,    92,    93,    -1,
      -1,    -1,    -1,    -1,    99,   100,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     9,
      -1,    -1,    -1,   118,    -1,   120,   121,    -1,   123,   124,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    -1,
      -1,    -1,    -1,    -1,    17,    -1,    19,    20,    -1,    22,
      -1,    -1,    25,    26,    27,    28,    29,    -1,    -1,    32,
      33,    34,    -1,    -1,    -1,    -1,    -1,    -1,    41,    42,
      43,    44,    -1,    46,    47,    48,    49,    50,    51,    52,
      53,    -1,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      -1,    -1,    92,    93,    94,    95,    -1,    -1,    98,    82,
      83,   101,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    92,
      93,    -1,    -1,    -1,    -1,    -1,    99,   100,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,     3,     4,     5,     6,     7,
       8,     9,    10,    11,   117,   118,    -1,   120,   121,    17,
     123,    19,    20,    -1,    22,    -1,    -1,    25,    26,    27,
      28,    29,    -1,    -1,    32,    33,    34,    -1,    -1,    -1,
      -1,    -1,    -1,    41,    42,    43,    44,    -1,    46,    47,
      48,    49,    50,    -1,    52,    53,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    82,    83,    -1,    -1,    -1,    25,
      26,    27,    28,    29,    92,    93,    32,    33,    34,    -1,
      -1,    99,   100,    -1,    -1,    -1,    -1,    43,    -1,    -1,
      -1,    47,    48,    -1,    50,     9,    52,    53,    -1,   117,
     118,    -1,   120,   121,    -1,   123,    -1,    -1,    -1,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    82,    83,    -1,    -1,
      -1,    25,    26,    -1,    28,    29,    92,    93,    32,    33,
      34,    -1,    -1,    99,   100,    -1,    -1,    -1,    -1,    43,
      -1,    -1,    -1,    47,    48,    -1,    -1,    -1,    52,    53,
      -1,    -1,   118,    -1,   120,   121,    -1,   123,    82,    83,
      84,    85,    86,    87,    88,    89,    -1,    -1,    92,    93,
      94,    95,    -1,    -1,    98,    -1,    -1,    -1,    82,    83,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    92,    93,
      -1,    -1,    96,    -1,    -1,    99,   100,    -1,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   118,    -1,   120,   121,    -1,   123,
      25,    26,    -1,    28,    29,    -1,    -1,    32,    33,    34,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    43,    -1,
      -1,    -1,    47,    48,    -1,    -1,    -1,    52,    53,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    82,    83,    -1,
      -1,    -1,    25,    26,    -1,    28,    29,    92,    93,    32,
      33,    34,    -1,    -1,    99,   100,    -1,    -1,    -1,    -1,
      43,    -1,    -1,    -1,    47,    48,    -1,    -1,    -1,    52,
      53,    -1,   117,   118,    -1,   120,   121,    -1,   123,    -1,
      -1,    -1,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    82,
      83,    -1,    -1,    -1,    25,    26,    -1,    28,    29,    92,
      93,    32,    33,    34,    -1,    -1,    99,   100,    -1,    -1,
      -1,    -1,    43,    -1,    -1,    -1,    47,    48,    -1,    -1,
      -1,    52,    53,    -1,   117,   118,    -1,   120,   121,    -1,
     123,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    82,    83,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    92,    93,    -1,    -1,    -1,    -1,    -1,    99,   100,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   109,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   118,    -1,   120,
     121,    -1,   123,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    -1,    -1,    -1,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    -1,    25,    26,    -1,    28,    29,
      -1,    -1,    32,    33,    34,    -1,    -1,    25,    26,    -1,
      28,    29,    -1,    43,    32,    33,    34,    47,    48,    -1,
      -1,    -1,    52,    53,    -1,    43,    -1,    -1,    -1,    47,
      48,    -1,    -1,    -1,    52,    53,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    83,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    92,    93,    82,    83,    -1,    -1,    -1,    99,
     100,    -1,    -1,    -1,    92,    93,    -1,    -1,    -1,    -1,
      -1,    99,   100,    -1,    -1,    -1,    -1,   117,   118,    -1,
     120,   121,    -1,   123,    -1,    -1,    -1,    -1,    -1,    -1,
     118,   119,   120,   121,    -1,   123,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    -1,    -1,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    25,    26,
      -1,    28,    29,    -1,    -1,    32,    33,    34,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    43,    -1,    -1,    -1,
      47,    48,    -1,    -1,    -1,    52,    53,    96,    97,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     9,    -1,
     109,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   118,
      -1,    -1,    23,   122,   123,    82,    83,    -1,   127,    -1,
      -1,    -1,    -1,    -1,    -1,    92,    93,    -1,    -1,    -1,
      -1,    -1,    99,   100,     9,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   118,    -1,   120,   121,    -1,   123,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,     9,
      -1,    92,    93,    94,    95,    -1,    -1,    98,    -1,    -1,
     101,    -1,    -1,    23,    -1,    -1,    -1,   108,    -1,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    -1,   127,    92,    93,    94,
      95,    -1,    -1,    98,    -1,    -1,   101,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
       9,    -1,    92,    93,    94,    95,    -1,    -1,    98,    -1,
      -1,   101,    -1,    -1,    23,    -1,    -1,    -1,   108,   109,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   124,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,     9,    -1,    92,    93,    94,    95,    -1,    -1,    98,
      -1,    -1,   101,    -1,    -1,    23,    -1,    -1,    -1,   108,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   124,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,     9,    -1,    92,    93,    94,    95,    -1,    -1,
      98,    -1,    -1,   101,    -1,    -1,    23,    -1,    -1,    -1,
     108,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   124,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,     9,    -1,    92,    93,    94,    95,    -1,
      -1,    98,    -1,    -1,   101,    -1,    -1,    23,    -1,    -1,
      -1,   108,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   124,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,     9,    -1,    92,    93,    94,    95,
      -1,    -1,    98,    -1,    -1,   101,    -1,    -1,    23,    -1,
      -1,    -1,   108,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   124,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,     9,    -1,    92,    93,    94,
      95,    -1,    -1,    98,    -1,    -1,   101,    -1,    -1,    23,
      -1,    -1,    -1,   108,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   124,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,     9,    -1,    92,    93,
      94,    95,    -1,    -1,    98,    -1,    -1,   101,    -1,    -1,
      23,    -1,    -1,    -1,   108,   109,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   119,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,     9,    -1,    92,
      93,    94,    95,    -1,    -1,    98,    -1,    -1,   101,    -1,
      -1,    23,    -1,    -1,    -1,   108,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   116,    -1,    -1,   119,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,     9,    -1,
      92,    93,    94,    95,    -1,    -1,    98,    -1,    -1,   101,
      -1,    -1,    23,    -1,    -1,    -1,   108,   109,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   119,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,     9,
      -1,    92,    93,    94,    95,    -1,    -1,    98,    -1,    -1,
     101,    -1,    -1,    23,    -1,    -1,    -1,   108,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   119,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
       9,    -1,    92,    93,    94,    95,    -1,    -1,    98,    -1,
      -1,   101,    -1,    -1,    23,    -1,    -1,    -1,   108,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   119,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,     9,    -1,    92,    93,    94,    95,    -1,    -1,    98,
      -1,    -1,   101,    -1,    -1,    23,    -1,    -1,    -1,   108,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     119,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,     9,    -1,    92,    93,    94,    95,    -1,    -1,
      98,    -1,    -1,   101,    -1,    -1,    23,    -1,    -1,    -1,
     108,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   119,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,     9,    -1,    92,    93,    94,    95,    -1,
      -1,    98,    -1,    -1,   101,    -1,    -1,    23,    -1,    -1,
      -1,   108,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   119,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,     9,    -1,    92,    93,    94,    95,
      -1,    -1,    98,    -1,    -1,   101,    -1,    -1,    23,    -1,
      -1,    -1,   108,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   119,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,     9,    -1,    92,    93,    94,
      95,    -1,    -1,    98,    -1,    -1,   101,    -1,    -1,    23,
      -1,    -1,    -1,   108,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   119,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,     9,    -1,    92,    93,
      94,    95,    -1,    -1,    98,    -1,    -1,   101,    -1,    -1,
      23,    -1,    -1,    -1,   108,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   119,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,     9,    -1,    92,
      93,    94,    95,    -1,    -1,    98,    -1,    -1,   101,    -1,
      -1,    23,    -1,    -1,    -1,   108,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   119,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,     9,    -1,
      92,    93,    94,    95,    -1,    -1,    98,    -1,    -1,   101,
      -1,    -1,    23,    -1,    -1,    -1,   108,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   119,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,     9,
      -1,    92,    93,    94,    95,    -1,    -1,    98,    -1,    -1,
     101,    -1,    -1,    23,    -1,    -1,    -1,   108,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   117,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
       9,    -1,    92,    93,    94,    95,    -1,    -1,    98,    -1,
      -1,   101,    -1,    -1,    23,    -1,    -1,    -1,   108,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   117,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,     9,    -1,    92,    93,    94,    95,    -1,    -1,    98,
      -1,    -1,   101,    -1,    -1,    23,    -1,    -1,    -1,   108,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   117,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,     9,    -1,    92,    93,    94,    95,    -1,    -1,
      98,    -1,    -1,   101,    -1,    -1,    23,    -1,    -1,    -1,
     108,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   117,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,     9,    -1,    92,    93,    94,    95,    -1,
      -1,    98,    -1,    -1,   101,    -1,    -1,    23,    -1,    -1,
      -1,   108,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   116,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,     9,    -1,    92,    93,    94,    95,
      -1,    -1,    98,    -1,    -1,   101,    -1,    -1,    23,    -1,
      -1,    -1,   108,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     116,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     9,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      23,    -1,    -1,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    -1,    -1,    92,    93,    94,
      95,    -1,    -1,    98,    -1,    -1,   101,    -1,    -1,    -1,
      -1,    -1,    -1,   108,   109,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,     9,    -1,    92,
      93,    94,    95,    -1,    -1,    98,    -1,    -1,   101,    -1,
      -1,    23,    -1,    -1,    -1,   108,   109,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     9,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    23,    -1,    -1,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    -1,     9,
      92,    93,    94,    95,    -1,    -1,    98,    -1,    -1,   101,
      -1,    -1,    -1,    23,    -1,    -1,   108,   109,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
       9,    -1,    92,    93,    94,    95,    -1,    -1,    98,    -1,
      -1,   101,    -1,    -1,    23,    -1,    -1,    -1,   108,    69,
      -1,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
       9,    -1,    92,    93,    94,    95,    -1,    -1,    98,    -1,
      -1,   101,    -1,    -1,    23,    -1,    -1,    -1,    -1,    68,
      69,    -1,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    -1,    -1,    92,    93,    94,    95,    -1,    -1,    98,
      -1,    -1,   101,    -1,    -1,    -1,    -1,    -1,    -1,   108,
      -1,    -1,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    -1,    -1,    92,    93,    94,    95,    -1,    -1,    98,
      -1,    -1,   101
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,   112,   113,   114,   115,   130,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    25,    26,    28,    29,    32,
      33,    34,    43,    47,    48,    52,    53,    82,    83,    92,
      93,    99,   100,   118,   120,   121,   123,   129,   135,   139,
     140,   142,   143,   144,   146,   154,   163,   164,   165,   166,
     167,   169,   171,   172,   201,    35,    37,    38,    39,    40,
     191,   192,   194,    11,    17,    19,    20,    22,    27,    41,
      42,    44,    46,    49,    50,    51,   117,   134,   139,   140,
     142,   144,   201,   205,   206,   209,    55,   131,   132,   133,
     134,   192,   206,     0,   163,   164,   165,   172,   118,   118,
     118,   118,   165,   145,   118,   139,   139,   139,   144,   209,
      96,   139,   155,   156,   125,     9,    23,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    92,
      93,    94,    95,    98,   101,   108,   116,   139,    96,    97,
     118,   122,   123,   125,    90,    91,   123,   141,   118,   135,
     101,     7,     8,   136,   197,   198,   136,   195,   196,   136,
     199,   200,   117,   116,   136,   118,   117,   118,   118,   118,
     118,   117,   139,   117,   206,   117,   139,   139,     6,   118,
       6,     7,   117,   127,   127,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,   109,   127,   203,   210,
     211,   117,   117,   192,   133,   109,   165,   139,   165,   139,
     139,   116,   118,   158,   165,     6,   119,   119,   123,   116,
     151,   126,   192,   202,   206,   139,   139,   139,   139,   139,
     139,   139,   139,   139,   139,   139,   139,   139,   139,   139,
     139,   139,   139,   139,   139,   139,   139,   139,   165,   165,
     118,   139,   139,   136,   136,   139,   147,   148,   136,   109,
     139,     7,     8,   136,   139,   152,   153,   124,   139,   139,
     137,   139,    79,   139,   127,   116,   127,   116,   127,   116,
     193,   139,   139,   139,     7,   117,   129,   139,   117,    21,
      45,   117,   117,   139,   149,   150,     6,   117,   117,   139,
     139,     7,   139,   139,   211,   118,   119,   119,   119,   116,
     119,   135,   136,   157,   109,   149,   139,   156,   124,   126,
     192,   206,   139,   109,   116,   119,   124,   139,    67,   109,
     124,   127,   116,   151,   124,   109,   116,   151,   118,   129,
     162,   165,   173,   176,   198,   139,   196,   139,   200,   127,
     109,   119,   119,   119,    23,   138,   139,   191,   116,   119,
     139,    19,   118,   201,   116,   117,   149,   139,   139,   139,
     116,   170,   170,   136,   159,   160,   165,   157,   119,   124,
     119,   139,   148,   124,   139,   124,   139,   139,   153,   126,
     139,   139,   119,   106,   119,   165,   174,   175,    13,   179,
     158,   137,   206,   206,   139,   117,   117,   139,   117,   139,
     129,   150,   119,   109,   119,   119,   165,    79,    79,   106,
     119,   116,   136,   201,   127,   124,   124,   165,   108,   119,
     116,    14,    15,   178,   109,   116,   119,    18,   129,    50,
     204,   205,   138,   119,   201,   160,   137,   165,   159,   127,
     161,   139,   165,   174,   165,   168,   169,   180,   137,   206,
     207,   208,   116,   119,   117,   119,   119,   109,   139,    84,
     125,   119,   119,    24,   205,   206,   204,   201,   201,    79,
     126,   177,   206,   139,   119,    19,    54,   181,   183,   184,
     189,   192,   119,   206,   118,   162,   126,   183,   184,   192,
     102,   103,   182,   206,   139,   136,   162,   119,   117,   136,
     186,   185,    74,   109,   127,   188,   139,   139,   139,    90,
     187,   127,   109,   139,    19,   190,   139,   139,   139,   117
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_uint8 yyr1[] =
{
       0,   128,   129,   130,   130,   130,   130,   130,   130,   130,
     130,   130,   131,   131,   132,   132,   133,   133,   133,   133,
     134,   134,   135,   135,   136,   136,   137,   137,   137,   138,
     138,   139,   139,   139,   139,   139,   139,   139,   139,   139,
     139,   139,   139,   139,   139,   139,   139,   139,   139,   139,
     139,   139,   139,   139,   139,   139,   139,   139,   139,   139,
     139,   139,   139,   139,   139,   139,   139,   139,   139,   140,
     141,   141,   142,   142,   143,   143,   143,   143,   143,   143,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   145,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   146,   147,   147,   147,   148,   149,   149,   149,   150,
     151,   151,   152,   152,   152,   153,   153,   154,   155,   155,
     156,   156,   157,   158,   158,   159,   159,   160,   160,   161,
     161,   162,   162,   162,   163,   164,   165,   165,   165,   165,
     165,   165,   165,   166,   166,   166,   167,   168,   169,   169,
     170,   170,   171,   171,   172,   172,   173,   173,   174,   174,
     175,   175,   175,   176,   177,   176,   178,   178,   179,   179,
     180,   180,   180,   181,   181,   181,   181,   181,   181,   182,
     182,   182,   183,   185,   184,   186,   186,   187,   187,   188,
     188,   188,   188,   188,   188,   189,   189,   190,   190,   191,
     191,   191,   193,   192,   192,   194,   194,   195,   195,   196,
     197,   197,   198,   199,   199,   200,   201,   201,   202,   202,
     202,   202,   203,   203,   203,   203,   203,   203,   203,   203,
     203,   203,   203,   204,   204,   204,   205,   205,   205,   205,
     205,   205,   205,   205,   205,   205,   205,   205,   206,   206,
     206,   206,   206,   206,   206,   206,   207,   206,   208,   206,
     206,   206,   206,   206,   206,   206,   206,   206,   206,   206,
     206,   206,   209,   210,   210,   211
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     2,     3,     2,     3,     2,     3,     2,
       3,     2,     0,     1,     1,     2,     1,     2,     1,     1,
       3,     3,     1,     1,     1,     1,     0,     1,     3,     0,
       1,     1,     2,     2,     5,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     5,
       3,     3,     1,     1,     2,     2,     2,     1,     1,     3,
       1,     1,     3,     5,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     3,     1,     3,     3,     3,     4,
       6,     6,     4,     5,     5,     1,     3,     0,     3,     6,
       8,     5,     2,     2,     4,     4,     4,     6,     4,     5,
       5,     4,     0,     1,     3,     1,     0,     1,     3,     1,
       0,     1,     0,     1,     3,     1,     3,     4,     1,     3,
       1,     6,     0,     7,     4,     1,     3,     3,     2,     0,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     3,     3,     1,     1,
       0,     2,     6,     6,     3,     4,     4,     3,     1,     3,
       1,     2,     1,     6,     0,     8,     1,     1,     0,     1,
       0,     1,     1,     1,     1,     1,     2,     2,     2,     0,
       1,     1,     4,     0,     9,     0,     1,     0,     2,     0,
       2,     2,     4,     4,     2,     0,     4,     0,     2,     2,
       2,     2,     0,     5,     2,     1,     1,     1,     3,     3,
       1,     3,     3,     1,     3,     3,     3,     4,     1,     2,
       1,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     0,     1,     3,     3,     3,     3,     3,
       1,     5,     4,     6,     1,     4,     6,     8,     1,     1,
       2,     5,     7,     5,    10,     8,     0,     9,     0,    11,
       2,     2,     2,     3,     4,     6,     8,     5,     2,     3,
       3,     4,     2,     1,     2,     3
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = PKL_TAB_EMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == PKL_TAB_EMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        YY_LAC_DISCARD ("YYBACKUP");                              \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (&yylloc, pkl_parser, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use PKL_TAB_error or PKL_TAB_UNDEF. */
#define YYERRCODE PKL_TAB_UNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if PKL_TAB_DEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YYLOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YYLOCATION_PRINT

#  if defined YY_LOCATION_PRINT

   /* Temporary convenience wrapper in case some people defined the
      undocumented and private YY_LOCATION_PRINT macros.  */
#   define YYLOCATION_PRINT(File, Loc)  YY_LOCATION_PRINT(File, *(Loc))

#  elif defined PKL_TAB_LTYPE_IS_TRIVIAL && PKL_TAB_LTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  int res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
}

#   define YYLOCATION_PRINT  yy_location_print_

    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT(File, Loc)  YYLOCATION_PRINT(File, &(Loc))

#  else

#   define YYLOCATION_PRINT(File, Loc) ((void) 0)
    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT  YYLOCATION_PRINT

#  endif
# endif /* !defined YYLOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, Location, pkl_parser); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct pkl_parser *pkl_parser)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  YY_USE (yylocationp);
  YY_USE (pkl_parser);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct pkl_parser *pkl_parser)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  YYLOCATION_PRINT (yyo, yylocationp);
  YYFPRINTF (yyo, ": ");
  yy_symbol_value_print (yyo, yykind, yyvaluep, yylocationp, pkl_parser);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp,
                 int yyrule, struct pkl_parser *pkl_parser)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)],
                       &(yylsp[(yyi + 1) - (yynrhs)]), pkl_parser);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, pkl_parser); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !PKL_TAB_DEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !PKL_TAB_DEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


/* Given a state stack such that *YYBOTTOM is its bottom, such that
   *YYTOP is either its top or is YYTOP_EMPTY to indicate an empty
   stack, and such that *YYCAPACITY is the maximum number of elements it
   can hold without a reallocation, make sure there is enough room to
   store YYADD more elements.  If not, allocate a new stack using
   YYSTACK_ALLOC, copy the existing elements, and adjust *YYBOTTOM,
   *YYTOP, and *YYCAPACITY to reflect the new capacity and memory
   location.  If *YYBOTTOM != YYBOTTOM_NO_FREE, then free the old stack
   using YYSTACK_FREE.  Return 0 if successful or if no reallocation is
   required.  Return YYENOMEM if memory is exhausted.  */
static int
yy_lac_stack_realloc (YYPTRDIFF_T *yycapacity, YYPTRDIFF_T yyadd,
#if PKL_TAB_DEBUG
                      char const *yydebug_prefix,
                      char const *yydebug_suffix,
#endif
                      yy_state_t **yybottom,
                      yy_state_t *yybottom_no_free,
                      yy_state_t **yytop, yy_state_t *yytop_empty)
{
  YYPTRDIFF_T yysize_old =
    *yytop == yytop_empty ? 0 : *yytop - *yybottom + 1;
  YYPTRDIFF_T yysize_new = yysize_old + yyadd;
  if (*yycapacity < yysize_new)
    {
      YYPTRDIFF_T yyalloc = 2 * yysize_new;
      yy_state_t *yybottom_new;
      /* Use YYMAXDEPTH for maximum stack size given that the stack
         should never need to grow larger than the main state stack
         needs to grow without LAC.  */
      if (YYMAXDEPTH < yysize_new)
        {
          YYDPRINTF ((stderr, "%smax size exceeded%s", yydebug_prefix,
                      yydebug_suffix));
          return YYENOMEM;
        }
      if (YYMAXDEPTH < yyalloc)
        yyalloc = YYMAXDEPTH;
      yybottom_new =
        YY_CAST (yy_state_t *,
                 YYSTACK_ALLOC (YY_CAST (YYSIZE_T,
                                         yyalloc * YYSIZEOF (*yybottom_new))));
      if (!yybottom_new)
        {
          YYDPRINTF ((stderr, "%srealloc failed%s", yydebug_prefix,
                      yydebug_suffix));
          return YYENOMEM;
        }
      if (*yytop != yytop_empty)
        {
          YYCOPY (yybottom_new, *yybottom, yysize_old);
          *yytop = yybottom_new + (yysize_old - 1);
        }
      if (*yybottom != yybottom_no_free)
        YYSTACK_FREE (*yybottom);
      *yybottom = yybottom_new;
      *yycapacity = yyalloc;
    }
  return 0;
}

/* Establish the initial context for the current lookahead if no initial
   context is currently established.

   We define a context as a snapshot of the parser stacks.  We define
   the initial context for a lookahead as the context in which the
   parser initially examines that lookahead in order to select a
   syntactic action.  Thus, if the lookahead eventually proves
   syntactically unacceptable (possibly in a later context reached via a
   series of reductions), the initial context can be used to determine
   the exact set of tokens that would be syntactically acceptable in the
   lookahead's place.  Moreover, it is the context after which any
   further semantic actions would be erroneous because they would be
   determined by a syntactically unacceptable token.

   YY_LAC_ESTABLISH should be invoked when a reduction is about to be
   performed in an inconsistent state (which, for the purposes of LAC,
   includes consistent states that don't know they're consistent because
   their default reductions have been disabled).  Iff there is a
   lookahead token, it should also be invoked before reporting a syntax
   error.  This latter case is for the sake of the debugging output.

   For parse.lac=full, the implementation of YY_LAC_ESTABLISH is as
   follows.  If no initial context is currently established for the
   current lookahead, then check if that lookahead can eventually be
   shifted if syntactic actions continue from the current context.
   Report a syntax error if it cannot.  */
#define YY_LAC_ESTABLISH                                                \
do {                                                                    \
  if (!yy_lac_established)                                              \
    {                                                                   \
      YYDPRINTF ((stderr,                                               \
                  "LAC: initial context established for %s\n",          \
                  yysymbol_name (yytoken)));                            \
      yy_lac_established = 1;                                           \
      switch (yy_lac (yyesa, &yyes, &yyes_capacity, yyssp, yytoken))    \
        {                                                               \
        case YYENOMEM:                                                  \
          YYNOMEM;                                                      \
        case 1:                                                         \
          goto yyerrlab;                                                \
        }                                                               \
    }                                                                   \
} while (0)

/* Discard any previous initial lookahead context because of Event,
   which may be a lookahead change or an invalidation of the currently
   established initial context for the current lookahead.

   The most common example of a lookahead change is a shift.  An example
   of both cases is syntax error recovery.  That is, a syntax error
   occurs when the lookahead is syntactically erroneous for the
   currently established initial context, so error recovery manipulates
   the parser stacks to try to find a new initial context in which the
   current lookahead is syntactically acceptable.  If it fails to find
   such a context, it discards the lookahead.  */
#if PKL_TAB_DEBUG
# define YY_LAC_DISCARD(Event)                                           \
do {                                                                     \
  if (yy_lac_established)                                                \
    {                                                                    \
      YYDPRINTF ((stderr, "LAC: initial context discarded due to "       \
                  Event "\n"));                                          \
      yy_lac_established = 0;                                            \
    }                                                                    \
} while (0)
#else
# define YY_LAC_DISCARD(Event) yy_lac_established = 0
#endif

/* Given the stack whose top is *YYSSP, return 0 iff YYTOKEN can
   eventually (after perhaps some reductions) be shifted, return 1 if
   not, or return YYENOMEM if memory is exhausted.  As preconditions and
   postconditions: *YYES_CAPACITY is the allocated size of the array to
   which *YYES points, and either *YYES = YYESA or *YYES points to an
   array allocated with YYSTACK_ALLOC.  yy_lac may overwrite the
   contents of either array, alter *YYES and *YYES_CAPACITY, and free
   any old *YYES other than YYESA.  */
static int
yy_lac (yy_state_t *yyesa, yy_state_t **yyes,
        YYPTRDIFF_T *yyes_capacity, yy_state_t *yyssp, yysymbol_kind_t yytoken)
{
  yy_state_t *yyes_prev = yyssp;
  yy_state_t *yyesp = yyes_prev;
  /* Reduce until we encounter a shift and thereby accept the token.  */
  YYDPRINTF ((stderr, "LAC: checking lookahead %s:", yysymbol_name (yytoken)));
  if (yytoken == YYSYMBOL_YYUNDEF)
    {
      YYDPRINTF ((stderr, " Always Err\n"));
      return 1;
    }
  while (1)
    {
      int yyrule = yypact[+*yyesp];
      if (yypact_value_is_default (yyrule)
          || (yyrule += yytoken) < 0 || YYLAST < yyrule
          || yycheck[yyrule] != yytoken)
        {
          /* Use the default action.  */
          yyrule = yydefact[+*yyesp];
          if (yyrule == 0)
            {
              YYDPRINTF ((stderr, " Err\n"));
              return 1;
            }
        }
      else
        {
          /* Use the action from yytable.  */
          yyrule = yytable[yyrule];
          if (yytable_value_is_error (yyrule))
            {
              YYDPRINTF ((stderr, " Err\n"));
              return 1;
            }
          if (0 < yyrule)
            {
              YYDPRINTF ((stderr, " S%d\n", yyrule));
              return 0;
            }
          yyrule = -yyrule;
        }
      /* By now we know we have to simulate a reduce.  */
      YYDPRINTF ((stderr, " R%d", yyrule - 1));
      {
        /* Pop the corresponding number of values from the stack.  */
        YYPTRDIFF_T yylen = yyr2[yyrule];
        /* First pop from the LAC stack as many tokens as possible.  */
        if (yyesp != yyes_prev)
          {
            YYPTRDIFF_T yysize = yyesp - *yyes + 1;
            if (yylen < yysize)
              {
                yyesp -= yylen;
                yylen = 0;
              }
            else
              {
                yyesp = yyes_prev;
                yylen -= yysize;
              }
          }
        /* Only afterwards look at the main stack.  */
        if (yylen)
          yyesp = yyes_prev -= yylen;
      }
      /* Push the resulting state of the reduction.  */
      {
        yy_state_fast_t yystate;
        {
          const int yylhs = yyr1[yyrule] - YYNTOKENS;
          const int yyi = yypgoto[yylhs] + *yyesp;
          yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyesp
                     ? yytable[yyi]
                     : yydefgoto[yylhs]);
        }
        if (yyesp == yyes_prev)
          {
            yyesp = *yyes;
            YY_IGNORE_USELESS_CAST_BEGIN
            *yyesp = YY_CAST (yy_state_t, yystate);
            YY_IGNORE_USELESS_CAST_END
          }
        else
          {
            if (yy_lac_stack_realloc (yyes_capacity, 1,
#if PKL_TAB_DEBUG
                                      " (", ")",
#endif
                                      yyes, yyesa, &yyesp, yyes_prev))
              {
                YYDPRINTF ((stderr, "\n"));
                return YYENOMEM;
              }
            YY_IGNORE_USELESS_CAST_BEGIN
            *++yyesp = YY_CAST (yy_state_t, yystate);
            YY_IGNORE_USELESS_CAST_END
          }
        YYDPRINTF ((stderr, " G%d", yystate));
      }
    }
}

/* Context of a parse error.  */
typedef struct
{
  yy_state_t *yyssp;
  yy_state_t *yyesa;
  yy_state_t **yyes;
  YYPTRDIFF_T *yyes_capacity;
  yysymbol_kind_t yytoken;
  YYLTYPE *yylloc;
} yypcontext_t;

/* Put in YYARG at most YYARGN of the expected tokens given the
   current YYCTX, and return the number of tokens stored in YYARG.  If
   YYARG is null, return the number of expected tokens (guaranteed to
   be less than YYNTOKENS).  Return YYENOMEM on memory exhaustion.
   Return 0 if there are more than YYARGN expected tokens, yet fill
   YYARG up to YYARGN. */
static int
yypcontext_expected_tokens (const yypcontext_t *yyctx,
                            yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;

  int yyx;
  for (yyx = 0; yyx < YYNTOKENS; ++yyx)
    {
      yysymbol_kind_t yysym = YY_CAST (yysymbol_kind_t, yyx);
      if (yysym != YYSYMBOL_YYerror && yysym != YYSYMBOL_YYUNDEF)
        switch (yy_lac (yyctx->yyesa, yyctx->yyes, yyctx->yyes_capacity, yyctx->yyssp, yysym))
          {
          case YYENOMEM:
            return YYENOMEM;
          case 1:
            continue;
          default:
            if (!yyarg)
              ++yycount;
            else if (yycount == yyargn)
              return 0;
            else
              yyarg[yycount++] = yysym;
          }
    }
  if (yyarg && yycount == 0 && 0 < yyargn)
    yyarg[0] = YYSYMBOL_YYEMPTY;
  return yycount;
}




/* The kind of the lookahead of this context.  */
static yysymbol_kind_t
yypcontext_token (const yypcontext_t *yyctx) YY_ATTRIBUTE_UNUSED;

static yysymbol_kind_t
yypcontext_token (const yypcontext_t *yyctx)
{
  return yyctx->yytoken;
}

/* The location of the lookahead of this context.  */
static YYLTYPE *
yypcontext_location (const yypcontext_t *yyctx) YY_ATTRIBUTE_UNUSED;

static YYLTYPE *
yypcontext_location (const yypcontext_t *yyctx)
{
  return yyctx->yylloc;
}

/* User defined function to report a syntax error.  */
static int
yyreport_syntax_error (const yypcontext_t *yyctx, struct pkl_parser *pkl_parser);

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, struct pkl_parser *pkl_parser)
{
  YY_USE (yyvaluep);
  YY_USE (yylocationp);
  YY_USE (pkl_parser);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yykind)
    {
    case YYSYMBOL_INTEGER: /* "integer literal"  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3102 "pkl-tab.c"
        break;

    case YYSYMBOL_CHAR: /* "character literal"  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3130 "pkl-tab.c"
        break;

    case YYSYMBOL_STR: /* "string"  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3158 "pkl-tab.c"
        break;

    case YYSYMBOL_IDENTIFIER: /* "identifier"  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3186 "pkl-tab.c"
        break;

    case YYSYMBOL_TYPENAME: /* "type name"  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3214 "pkl-tab.c"
        break;

    case YYSYMBOL_UNIT: /* "offset unit"  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3242 "pkl-tab.c"
        break;

    case YYSYMBOL_OFFSET: /* "offset"  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3270 "pkl-tab.c"
        break;

    case YYSYMBOL_ATTR: /* "attribute"  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3298 "pkl-tab.c"
        break;

    case YYSYMBOL_start: /* start  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3326 "pkl-tab.c"
        break;

    case YYSYMBOL_program: /* program  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3354 "pkl-tab.c"
        break;

    case YYSYMBOL_program_elem_list: /* program_elem_list  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3382 "pkl-tab.c"
        break;

    case YYSYMBOL_program_elem: /* program_elem  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3410 "pkl-tab.c"
        break;

    case YYSYMBOL_load: /* load  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3438 "pkl-tab.c"
        break;

    case YYSYMBOL_integer: /* integer  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3466 "pkl-tab.c"
        break;

    case YYSYMBOL_identifier: /* identifier  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3494 "pkl-tab.c"
        break;

    case YYSYMBOL_expression_list: /* expression_list  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3522 "pkl-tab.c"
        break;

    case YYSYMBOL_expression_opt: /* expression_opt  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3550 "pkl-tab.c"
        break;

    case YYSYMBOL_expression: /* expression  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3578 "pkl-tab.c"
        break;

    case YYSYMBOL_bconc: /* bconc  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3606 "pkl-tab.c"
        break;

    case YYSYMBOL_map: /* map  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3634 "pkl-tab.c"
        break;

    case YYSYMBOL_primary: /* primary  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3662 "pkl-tab.c"
        break;

    case YYSYMBOL_funcall: /* funcall  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3690 "pkl-tab.c"
        break;

    case YYSYMBOL_funcall_arg_list: /* funcall_arg_list  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3718 "pkl-tab.c"
        break;

    case YYSYMBOL_funcall_arg: /* funcall_arg  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3746 "pkl-tab.c"
        break;

    case YYSYMBOL_format_arg_list: /* format_arg_list  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3774 "pkl-tab.c"
        break;

    case YYSYMBOL_format_arg: /* format_arg  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3802 "pkl-tab.c"
        break;

    case YYSYMBOL_struct_field_list: /* struct_field_list  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3830 "pkl-tab.c"
        break;

    case YYSYMBOL_struct_field: /* struct_field  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3858 "pkl-tab.c"
        break;

    case YYSYMBOL_array: /* array  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3886 "pkl-tab.c"
        break;

    case YYSYMBOL_array_initializer_list: /* array_initializer_list  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3914 "pkl-tab.c"
        break;

    case YYSYMBOL_array_initializer: /* array_initializer  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3942 "pkl-tab.c"
        break;

    case YYSYMBOL_function_specifier: /* function_specifier  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3970 "pkl-tab.c"
        break;

    case YYSYMBOL_function_arg_list: /* function_arg_list  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3998 "pkl-tab.c"
        break;

    case YYSYMBOL_function_arg: /* function_arg  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4026 "pkl-tab.c"
        break;

    case YYSYMBOL_function_arg_initial: /* function_arg_initial  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4054 "pkl-tab.c"
        break;

    case YYSYMBOL_type_specifier: /* type_specifier  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4082 "pkl-tab.c"
        break;

    case YYSYMBOL_typename: /* typename  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4110 "pkl-tab.c"
        break;

    case YYSYMBOL_string_type_specifier: /* string_type_specifier  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4138 "pkl-tab.c"
        break;

    case YYSYMBOL_simple_type_specifier: /* simple_type_specifier  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4166 "pkl-tab.c"
        break;

    case YYSYMBOL_cons_type_specifier: /* cons_type_specifier  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4194 "pkl-tab.c"
        break;

    case YYSYMBOL_integral_type_specifier: /* integral_type_specifier  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4222 "pkl-tab.c"
        break;

    case YYSYMBOL_dynamic_integral_type_specifier: /* dynamic_integral_type_specifier  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4250 "pkl-tab.c"
        break;

    case YYSYMBOL_ref_type: /* ref_type  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4278 "pkl-tab.c"
        break;

    case YYSYMBOL_offset_type_specifier: /* offset_type_specifier  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4306 "pkl-tab.c"
        break;

    case YYSYMBOL_array_type_specifier: /* array_type_specifier  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4334 "pkl-tab.c"
        break;

    case YYSYMBOL_function_type_specifier: /* function_type_specifier  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4362 "pkl-tab.c"
        break;

    case YYSYMBOL_function_type_arg_list: /* function_type_arg_list  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4390 "pkl-tab.c"
        break;

    case YYSYMBOL_function_type_arg: /* function_type_arg  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4418 "pkl-tab.c"
        break;

    case YYSYMBOL_struct_type_specifier: /* struct_type_specifier  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4446 "pkl-tab.c"
        break;

    case YYSYMBOL_integral_struct: /* integral_struct  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4474 "pkl-tab.c"
        break;

    case YYSYMBOL_struct_type_elem_list: /* struct_type_elem_list  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4502 "pkl-tab.c"
        break;

    case YYSYMBOL_struct_type_computed_field: /* struct_type_computed_field  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4530 "pkl-tab.c"
        break;

    case YYSYMBOL_struct_type_field: /* struct_type_field  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4558 "pkl-tab.c"
        break;

    case YYSYMBOL_struct_type_field_identifier: /* struct_type_field_identifier  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4586 "pkl-tab.c"
        break;

    case YYSYMBOL_struct_type_field_label: /* struct_type_field_label  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4614 "pkl-tab.c"
        break;

    case YYSYMBOL_struct_type_field_optcond_pre: /* struct_type_field_optcond_pre  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4642 "pkl-tab.c"
        break;

    case YYSYMBOL_struct_type_field_optcond_post: /* struct_type_field_optcond_post  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4670 "pkl-tab.c"
        break;

    case YYSYMBOL_simple_declaration: /* simple_declaration  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4698 "pkl-tab.c"
        break;

    case YYSYMBOL_declaration: /* declaration  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4726 "pkl-tab.c"
        break;

    case YYSYMBOL_defvar_list: /* defvar_list  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4754 "pkl-tab.c"
        break;

    case YYSYMBOL_defvar: /* defvar  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4782 "pkl-tab.c"
        break;

    case YYSYMBOL_deftype_list: /* deftype_list  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4810 "pkl-tab.c"
        break;

    case YYSYMBOL_deftype: /* deftype  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4838 "pkl-tab.c"
        break;

    case YYSYMBOL_defunit_list: /* defunit_list  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4866 "pkl-tab.c"
        break;

    case YYSYMBOL_defunit: /* defunit  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4894 "pkl-tab.c"
        break;

    case YYSYMBOL_comp_stmt: /* comp_stmt  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4922 "pkl-tab.c"
        break;

    case YYSYMBOL_stmt_decl_list: /* stmt_decl_list  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4950 "pkl-tab.c"
        break;

    case YYSYMBOL_simple_stmt_list: /* simple_stmt_list  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4978 "pkl-tab.c"
        break;

    case YYSYMBOL_simple_stmt: /* simple_stmt  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 5006 "pkl-tab.c"
        break;

    case YYSYMBOL_stmt: /* stmt  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 5034 "pkl-tab.c"
        break;

    case YYSYMBOL_funcall_stmt: /* funcall_stmt  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 5062 "pkl-tab.c"
        break;

    case YYSYMBOL_funcall_stmt_arg_list: /* funcall_stmt_arg_list  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 5090 "pkl-tab.c"
        break;

    case YYSYMBOL_funcall_stmt_arg: /* funcall_stmt_arg  */
#line 383 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 5118 "pkl-tab.c"
        break;

      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (struct pkl_parser *pkl_parser)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined PKL_TAB_LTYPE_IS_TRIVIAL && PKL_TAB_LTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

    /* The location stack: array, bottom, top.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls = yylsa;
    YYLTYPE *yylsp = yyls;

    yy_state_t yyesa[20];
    yy_state_t *yyes = yyesa;
    YYPTRDIFF_T yyes_capacity = 20 < YYMAXDEPTH ? 20 : YYMAXDEPTH;

  /* Whether LAC context is established.  A Boolean.  */
  int yy_lac_established = 0;
  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[3];



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = PKL_TAB_EMPTY; /* Cause a token to be read.  */


/* User initialization code.  */
#line 30 "pkl-tab.y"
{
    yylloc.first_line = yylloc.last_line = pkl_parser->init_line;
    yylloc.first_column = yylloc.last_column = pkl_parser->init_column;
}

#line 5224 "pkl-tab.c"

  yylsp[0] = yylloc;
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yyls1, yysize * YYSIZEOF (*yylsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
        yyls = yyls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == PKL_TAB_EMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, &yylloc, scanner);
    }

  if (yychar <= PKL_TAB_EOF)
    {
      yychar = PKL_TAB_EOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == PKL_TAB_error)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = PKL_TAB_UNDEF;
      yytoken = YYSYMBOL_YYerror;
      yyerror_range[1] = yylloc;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    {
      YY_LAC_ESTABLISH;
      goto yydefault;
    }
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      YY_LAC_ESTABLISH;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;

  /* Discard the shifted token.  */
  yychar = PKL_TAB_EMPTY;
  YY_LAC_DISCARD ("shift");
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  yyerror_range[1] = yyloc;
  YY_REDUCE_PRINT (yyn);
  {
    int yychar_backup = yychar;
    switch (yyn)
      {
  case 2: /* pushlevel: %empty  */
#line 595 "pkl-tab.y"
                {
                  pkl_parser->env = pkl_env_push_frame (pkl_parser->env);
                }
#line 5446 "pkl-tab.c"
    break;

  case 3: /* start: START_EXP expression  */
#line 610 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5456 "pkl-tab.c"
    break;

  case 4: /* start: START_EXP expression ','  */
#line 616 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                  YYACCEPT;
                }
#line 5467 "pkl-tab.c"
    break;

  case 5: /* start: START_DECL declaration  */
#line 623 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5477 "pkl-tab.c"
    break;

  case 6: /* start: START_DECL declaration ','  */
#line 629 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5487 "pkl-tab.c"
    break;

  case 7: /* start: START_STMT stmt  */
#line 635 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5497 "pkl-tab.c"
    break;

  case 8: /* start: START_STMT stmt ';'  */
#line 641 "pkl-tab.y"
                {
                  /* This rule is to allow the presence of an extra
                     ';' after the sentence.  This to allow the poke
                     command manager to ease the handling of
                     semicolons in the command line.  */
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5511 "pkl-tab.c"
    break;

  case 9: /* start: START_STMT load  */
#line 651 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5521 "pkl-tab.c"
    break;

  case 10: /* start: START_STMT load ';'  */
#line 657 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5531 "pkl-tab.c"
    break;

  case 11: /* start: START_PROGRAM program  */
#line 663 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5541 "pkl-tab.c"
    break;

  case 12: /* program: %empty  */
#line 672 "pkl-tab.y"
                {
                  (yyval.ast) = NULL;
                }
#line 5549 "pkl-tab.c"
    break;

  case 15: /* program_elem_list: program_elem_list program_elem  */
#line 681 "pkl-tab.y"
                {
                  if ((yyvsp[0].ast) != NULL)
                    (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast));
                  else
                    (yyval.ast) = (yyvsp[-1].ast);
                }
#line 5560 "pkl-tab.c"
    break;

  case 17: /* program_elem: IMMUTABLE declaration  */
#line 692 "pkl-tab.y"
          {
            PKL_AST_DECL_IMMUTABLE_P ((yyvsp[0].ast)) = 1;
            (yyval.ast) = (yyvsp[0].ast);
          }
#line 5569 "pkl-tab.c"
    break;

  case 20: /* load: "keyword `load'" "identifier" ';'  */
#line 702 "pkl-tab.y"
                {
                  char *filename = NULL;
                  int ret = load_module (pkl_parser,
                                         PKL_AST_IDENTIFIER_POINTER ((yyvsp[-1].ast)),
                                         &(yyval.ast), 0 /* filename_p */, &filename);
                  if (ret == 2)
                    /* The sub-parser should have emitted proper error
                       messages.  No need to be verbose here.  */
                    YYERROR;
                  else if (ret == 1)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-1]),
                                 "cannot load `%s'",
                                 PKL_AST_IDENTIFIER_POINTER ((yyvsp[-1].ast)));
                      YYERROR;
                    }

                  /* Prepend and append SRC nodes to handle the change of
                     source files.  */
                  {
                      pkl_ast_node src1 = pkl_ast_make_src (pkl_parser->ast,
                                                            filename);
                      pkl_ast_node src2 = pkl_ast_make_src (pkl_parser->ast,
                                                            pkl_parser->filename);

                      (yyval.ast) = pkl_ast_chainon (src1, (yyval.ast));
                      (yyval.ast) = pkl_ast_chainon ((yyval.ast), src2);
                  }

                  (yyvsp[-1].ast) = ASTREF ((yyvsp[-1].ast));
                  pkl_ast_node_free ((yyvsp[-1].ast));
                  free (filename);
                }
#line 5607 "pkl-tab.c"
    break;

  case 21: /* load: "keyword `load'" "string" ';'  */
#line 736 "pkl-tab.y"
                {
                  char *filename = PKL_AST_STRING_POINTER ((yyvsp[-1].ast));
                  int ret = load_module (pkl_parser,
                                         filename,
                                         &(yyval.ast), 1 /* filename_p */, NULL);
                  if (ret == 2)
                    /* The sub-parser should have emitted proper error
                       messages.  No need to be verbose here.  */
                    YYERROR;
                  else if (ret == 1)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-1]),
                                 "cannot load module from file `%s'",
                                 filename);
                      YYERROR;
                    }

                  /* Prepend and append SRC nodes to handle the change of
                     source files.  */
                  {
                      pkl_ast_node src1 = pkl_ast_make_src (pkl_parser->ast,
                                                            filename);
                      pkl_ast_node src2 = pkl_ast_make_src (pkl_parser->ast,
                                                            pkl_parser->filename);

                      (yyval.ast) = pkl_ast_chainon (src1, (yyval.ast));
                      (yyval.ast) = pkl_ast_chainon ((yyval.ast), src2);
                  }

                  (yyvsp[-1].ast) = ASTREF ((yyvsp[-1].ast));
                  pkl_ast_node_free ((yyvsp[-1].ast));
                }
#line 5644 "pkl-tab.c"
    break;

  case 23: /* integer: LEXER_EXCEPTION  */
#line 777 "pkl-tab.y"
          {
            (yyval.ast) = NULL; /* To avoid bison warning.  */
            pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                       (yyvsp[0].exception_msg));
            free ((yyvsp[0].exception_msg));
            YYERROR;
          }
#line 5656 "pkl-tab.c"
    break;

  case 26: /* expression_list: %empty  */
#line 801 "pkl-tab.y"
                  { (yyval.ast) = NULL; }
#line 5662 "pkl-tab.c"
    break;

  case 28: /* expression_list: expression_list ',' expression  */
#line 804 "pkl-tab.y"
                  {
                    (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                  }
#line 5670 "pkl-tab.c"
    break;

  case 29: /* expression_opt: %empty  */
#line 810 "pkl-tab.y"
                 { (yyval.ast) = NULL; }
#line 5676 "pkl-tab.c"
    break;

  case 32: /* expression: unary_operator expression  */
#line 817 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_unary_exp (pkl_parser->ast,
                                               (yyvsp[-1].opcode), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5686 "pkl-tab.c"
    break;

  case 33: /* expression: expression "attribute"  */
#line 823 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_ATTR,
                                                (yyvsp[-1].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5697 "pkl-tab.c"
    break;

  case 34: /* expression: expression "attribute" '(' expression ')'  */
#line 830 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_ternary_exp (pkl_parser->ast, PKL_AST_OP_ATTR,
                                                 (yyvsp[-4].ast), (yyvsp[-3].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyvsp[-3].ast)) = (yylsp[-3]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5708 "pkl-tab.c"
    break;

  case 35: /* expression: expression "addition operator" expression  */
#line 837 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_ADD,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5718 "pkl-tab.c"
    break;

  case 36: /* expression: expression "subtraction operator" expression  */
#line 843 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_SUB,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5728 "pkl-tab.c"
    break;

  case 37: /* expression: expression "multiplication operator" expression  */
#line 849 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_MUL,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5738 "pkl-tab.c"
    break;

  case 38: /* expression: expression "division operator" expression  */
#line 855 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_DIV,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5748 "pkl-tab.c"
    break;

  case 39: /* expression: expression "ceiling division operator" expression  */
#line 861 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_CEILDIV, (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5757 "pkl-tab.c"
    break;

  case 40: /* expression: expression "power operator" expression  */
#line 866 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_POW, (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5766 "pkl-tab.c"
    break;

  case 41: /* expression: expression "modulus operator" expression  */
#line 871 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_MOD,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5776 "pkl-tab.c"
    break;

  case 42: /* expression: expression "left shift operator" expression  */
#line 877 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_SL,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5786 "pkl-tab.c"
    break;

  case 43: /* expression: expression "right shift operator" expression  */
#line 883 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_SR,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5796 "pkl-tab.c"
    break;

  case 44: /* expression: expression "equality operator" expression  */
#line 889 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_EQ,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5806 "pkl-tab.c"
    break;

  case 45: /* expression: expression "inequality operator" expression  */
#line 895 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_NE,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5816 "pkl-tab.c"
    break;

  case 46: /* expression: expression "less-than operator" expression  */
#line 901 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_LT,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5826 "pkl-tab.c"
    break;

  case 47: /* expression: expression "bigger-than operator" expression  */
#line 907 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_GT,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5836 "pkl-tab.c"
    break;

  case 48: /* expression: expression "less-or-equal operator" expression  */
#line 913 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_LE,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5846 "pkl-tab.c"
    break;

  case 49: /* expression: expression "bigger-or-equal-than operator" expression  */
#line 919 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_GE,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5856 "pkl-tab.c"
    break;

  case 50: /* expression: expression "bit-wise or operator" expression  */
#line 925 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_IOR,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5866 "pkl-tab.c"
    break;

  case 51: /* expression: expression "bit-wise xor operator" expression  */
#line 931 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_XOR,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5876 "pkl-tab.c"
    break;

  case 52: /* expression: expression "bit-wise and operator" expression  */
#line 937 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_BAND,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5886 "pkl-tab.c"
    break;

  case 53: /* expression: expression "logical and operator" expression  */
#line 943 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_AND,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5896 "pkl-tab.c"
    break;

  case 54: /* expression: expression "logical or operator" expression  */
#line 949 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_OR,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5906 "pkl-tab.c"
    break;

  case 55: /* expression: expression "logical implication operator" expression  */
#line 955 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_IMPL,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5916 "pkl-tab.c"
    break;

  case 56: /* expression: expression "keyword `in'" expression  */
#line 961 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_IN,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5926 "pkl-tab.c"
    break;

  case 57: /* expression: expression "cast operator" simple_type_specifier  */
#line 967 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_cast (pkl_parser->ast, (yyvsp[0].ast), (yyvsp[-2].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5935 "pkl-tab.c"
    break;

  case 58: /* expression: expression "type identification operator" simple_type_specifier  */
#line 972 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_isa (pkl_parser->ast, (yyvsp[0].ast), (yyvsp[-2].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5944 "pkl-tab.c"
    break;

  case 59: /* expression: expression '?' expression ':' expression  */
#line 977 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_cond_exp (pkl_parser->ast,
                                              (yyvsp[-4].ast), (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5954 "pkl-tab.c"
    break;

  case 60: /* expression: comp_stmt "conditional on exception operator" expression  */
#line 983 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_EXCOND,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5964 "pkl-tab.c"
    break;

  case 61: /* expression: expression "conditional on exception operator" expression  */
#line 989 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_EXCOND,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5974 "pkl-tab.c"
    break;

  case 62: /* expression: "offset unit"  */
#line 995 "pkl-tab.y"
                {
                  if ((yyvsp[0].ast) == NULL)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                                 "invalid unit in offset");
                      YYERROR;
                    }

                    (yyval.ast) = pkl_ast_make_offset (pkl_parser->ast, NULL, (yyvsp[0].ast));
                    PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                    if (PKL_AST_TYPE ((yyvsp[0].ast)))
                        PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[0].ast))) = (yylsp[0]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5993 "pkl-tab.c"
    break;

  case 63: /* expression: "offset"  */
#line 1010 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6002 "pkl-tab.c"
    break;

  case 64: /* expression: expression "offset unit"  */
#line 1015 "pkl-tab.y"
                {
                  if ((yyvsp[0].ast) == NULL)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                                 "invalid unit in offset");
                      YYERROR;
                    }

                    (yyval.ast) = pkl_ast_make_offset (pkl_parser->ast, (yyvsp[-1].ast), (yyvsp[0].ast));
                    PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                    if (PKL_AST_TYPE ((yyvsp[0].ast)))
                        PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[0].ast))) = (yylsp[0]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6021 "pkl-tab.c"
    break;

  case 65: /* expression: "increment operator" expression  */
#line 1030 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_incrdecr (pkl_parser->ast, (yyvsp[0].ast),
                                              PKL_AST_ORDER_PRE, PKL_AST_SIGN_INCR);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6031 "pkl-tab.c"
    break;

  case 66: /* expression: "decrement operator" expression  */
#line 1036 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_incrdecr (pkl_parser->ast, (yyvsp[0].ast),
                                              PKL_AST_ORDER_PRE, PKL_AST_SIGN_DECR);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6041 "pkl-tab.c"
    break;

  case 69: /* bconc: expression "bit-concatenation operator" expression  */
#line 1047 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_BCONC,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6051 "pkl-tab.c"
    break;

  case 70: /* mapop: "map operator"  */
#line 1055 "pkl-tab.y"
             { (yyval.integer) = 1; }
#line 6057 "pkl-tab.c"
    break;

  case 71: /* mapop: "non-strict map operator"  */
#line 1056 "pkl-tab.y"
                { (yyval.integer) = 0; }
#line 6063 "pkl-tab.c"
    break;

  case 72: /* map: simple_type_specifier mapop expression  */
#line 1061 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_map (pkl_parser->ast, (yyvsp[-1].integer),
                                         (yyvsp[-2].ast), NULL, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6073 "pkl-tab.c"
    break;

  case 73: /* map: simple_type_specifier mapop expression ':' expression  */
#line 1067 "pkl-tab.y"
                 {
                   (yyval.ast) = pkl_ast_make_map (pkl_parser->ast, (yyvsp[-3].integer),
                                          (yyvsp[-4].ast), (yyvsp[-2].ast), (yyvsp[0].ast));
                   PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6083 "pkl-tab.c"
    break;

  case 74: /* unary_operator: "subtraction operator"  */
#line 1075 "pkl-tab.y"
                             { (yyval.opcode) = PKL_AST_OP_NEG; }
#line 6089 "pkl-tab.c"
    break;

  case 75: /* unary_operator: "addition operator"  */
#line 1076 "pkl-tab.y"
                             { (yyval.opcode) = PKL_AST_OP_POS; }
#line 6095 "pkl-tab.c"
    break;

  case 76: /* unary_operator: '~'  */
#line 1077 "pkl-tab.y"
                             { (yyval.opcode) = PKL_AST_OP_BNOT; }
#line 6101 "pkl-tab.c"
    break;

  case 77: /* unary_operator: '!'  */
#line 1078 "pkl-tab.y"
                             { (yyval.opcode) = PKL_AST_OP_NOT; }
#line 6107 "pkl-tab.c"
    break;

  case 78: /* unary_operator: "unmap operator"  */
#line 1079 "pkl-tab.y"
                             { (yyval.opcode) = PKL_AST_OP_UNMAP; }
#line 6113 "pkl-tab.c"
    break;

  case 79: /* unary_operator: "remap operator"  */
#line 1080 "pkl-tab.y"
                             { (yyval.opcode) = PKL_AST_OP_REMAP; }
#line 6119 "pkl-tab.c"
    break;

  case 80: /* primary: "identifier"  */
#line 1085 "pkl-tab.y"
                  {
                  /* Search for a variable definition in the
                     compile-time environment, and create a
                     PKL_AST_VAR node with it's lexical environment,
                     annotated with its initialization.  */

                  int back, over;
                  const char *name = PKL_AST_IDENTIFIER_POINTER ((yyvsp[0].ast));

                  pkl_ast_node decl
                    = pkl_env_lookup (pkl_parser->env,
                                      PKL_ENV_NS_MAIN,
                                      name, &back, &over);
                  if (!decl
                      || (PKL_AST_DECL_KIND (decl) != PKL_AST_DECL_KIND_VAR
                          && PKL_AST_DECL_KIND (decl) != PKL_AST_DECL_KIND_FUNC))
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                                 "undefined variable '%s'", name);
                      YYERROR;
                    }

                  (yyval.ast) = pkl_ast_make_var (pkl_parser->ast,
                                         (yyvsp[0].ast), /* name.  */
                                         decl,
                                         back, over);
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[0]);
                }
#line 6152 "pkl-tab.c"
    break;

  case 81: /* primary: integer  */
#line 1114 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_LOC (PKL_AST_TYPE ((yyval.ast))) = (yyloc);
                }
#line 6162 "pkl-tab.c"
    break;

  case 82: /* primary: "character literal"  */
#line 1120 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_LOC (PKL_AST_TYPE ((yyval.ast))) = (yyloc);
                }
#line 6172 "pkl-tab.c"
    break;

  case 83: /* primary: "string"  */
#line 1126 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_LOC (PKL_AST_TYPE ((yyval.ast))) = (yyloc);
                }
#line 6182 "pkl-tab.c"
    break;

  case 84: /* primary: '(' expression ')'  */
#line 1132 "pkl-tab.y"
                {
                  if (PKL_AST_CODE ((yyvsp[-1].ast)) == PKL_AST_VAR)
                    PKL_AST_VAR_IS_PARENTHESIZED ((yyvsp[-1].ast)) = 1;
                  else if (PKL_AST_CODE ((yyvsp[-1].ast)) == PKL_AST_STRUCT_REF)
                    PKL_AST_STRUCT_REF_IS_PARENTHESIZED ((yyvsp[-1].ast)) = 1;
                  (yyval.ast) = (yyvsp[-1].ast);
                }
#line 6194 "pkl-tab.c"
    break;

  case 86: /* primary: primary ".>" identifier  */
#line 1141 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_struct_ref (pkl_parser->ast, (yyvsp[-2].ast), (yyvsp[0].ast));
                    PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6204 "pkl-tab.c"
    break;

  case 87: /* primary: primary "dot operator" identifier  */
#line 1147 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_struct_ref (pkl_parser->ast, (yyvsp[-2].ast), (yyvsp[0].ast));
                    PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6214 "pkl-tab.c"
    break;

  case 88: /* primary: primary "indirection operator" identifier  */
#line 1153 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_struct_ref (pkl_parser->ast, (yyvsp[-2].ast), (yyvsp[0].ast));
                    PKL_AST_STRUCT_REF_INDIRECTION_P ((yyval.ast)) = 1;
                    PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6225 "pkl-tab.c"
    break;

  case 89: /* primary: primary '[' expression ']'  */
#line 1160 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_indexer (pkl_parser->ast, (yyvsp[-3].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6234 "pkl-tab.c"
    break;

  case 90: /* primary: primary '[' expression "range separator" expression ']'  */
#line 1165 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_trimmer (pkl_parser->ast,
                                             (yyvsp[-5].ast), (yyvsp[-3].ast), NULL, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6244 "pkl-tab.c"
    break;

  case 91: /* primary: primary '[' expression ':' expression ']'  */
#line 1171 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_trimmer (pkl_parser->ast,
                                             (yyvsp[-5].ast), (yyvsp[-3].ast), (yyvsp[-1].ast), NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6254 "pkl-tab.c"
    break;

  case 92: /* primary: primary '[' ':' ']'  */
#line 1177 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_trimmer (pkl_parser->ast,
                                             (yyvsp[-3].ast), NULL, NULL, NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6264 "pkl-tab.c"
    break;

  case 93: /* primary: primary '[' ':' expression ']'  */
#line 1183 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_trimmer (pkl_parser->ast,
                                             (yyvsp[-4].ast), NULL, (yyvsp[-1].ast), NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6274 "pkl-tab.c"
    break;

  case 94: /* primary: primary '[' expression ':' ']'  */
#line 1189 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_trimmer (pkl_parser->ast,
                                             (yyvsp[-4].ast), (yyvsp[-2].ast), NULL, NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6284 "pkl-tab.c"
    break;

  case 96: /* primary: '(' funcall_stmt ')'  */
#line 1196 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[-1].ast);
                }
#line 6292 "pkl-tab.c"
    break;

  case 97: /* $@1: %empty  */
#line 1200 "pkl-tab.y"
                {
                  /* function_specifier needs to know whether we are
                     in a function declaration or a method
                     declaration.  */
                  pkl_parser->in_method_decl_p = 0;
                }
#line 6303 "pkl-tab.c"
    break;

  case 98: /* primary: "keyword `lambda'" $@1 function_specifier  */
#line 1207 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_lambda (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6312 "pkl-tab.c"
    break;

  case 99: /* primary: "keyword `asm'" simple_type_specifier ':' '(' expression ')'  */
#line 1212 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_asm_exp (pkl_parser->ast, (yyvsp[-4].ast), (yyvsp[-1].ast), NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6321 "pkl-tab.c"
    break;

  case 100: /* primary: "keyword `asm'" simple_type_specifier ':' '(' expression ':' expression_list ')'  */
#line 1217 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_asm_exp (pkl_parser->ast, (yyvsp[-6].ast), (yyvsp[-3].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6330 "pkl-tab.c"
    break;

  case 101: /* primary: "keyword `format'" '(' "string" format_arg_list ')'  */
#line 1222 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_format (pkl_parser->ast, (yyvsp[-2].ast), (yyvsp[-1].ast),
                                            0 /* printf_p */);
                  PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                }
#line 6342 "pkl-tab.c"
    break;

  case 102: /* primary: expression "increment operator"  */
#line 1230 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_incrdecr (pkl_parser->ast, (yyvsp[-1].ast),
                                              PKL_AST_ORDER_POST, PKL_AST_SIGN_INCR);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6352 "pkl-tab.c"
    break;

  case 103: /* primary: expression "decrement operator"  */
#line 1236 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_incrdecr (pkl_parser->ast, (yyvsp[-1].ast),
                                              PKL_AST_ORDER_POST, PKL_AST_SIGN_DECR);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6362 "pkl-tab.c"
    break;

  case 104: /* primary: "keyword `typeof'" '(' expression ')'  */
#line 1242 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_unary_exp (pkl_parser->ast, PKL_AST_OP_TYPEOF, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-3]);
                }
#line 6371 "pkl-tab.c"
    break;

  case 105: /* primary: "keyword `typeof'" '(' simple_type_specifier ')'  */
#line 1247 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_unary_exp (pkl_parser->ast, PKL_AST_OP_TYPEOF, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-3]);
                }
#line 6380 "pkl-tab.c"
    break;

  case 106: /* primary: "keyword `sizeof'" '(' simple_type_specifier ')'  */
#line 1252 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_unary_exp (pkl_parser->ast, PKL_AST_OP_SIZEOF, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-3]);
                }
#line 6389 "pkl-tab.c"
    break;

  case 107: /* primary: "keyword `apush'" '(' expression ',' expression ')'  */
#line 1257 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_APUSH,
                                                (yyvsp[-3].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-5]);
                }
#line 6399 "pkl-tab.c"
    break;

  case 108: /* primary: "keyword `apop'" '(' expression ')'  */
#line 1263 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_unary_exp (pkl_parser->ast, PKL_AST_OP_APOP,
                                               (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-3]);
                }
#line 6409 "pkl-tab.c"
    break;

  case 109: /* primary: cons_type_specifier '(' expression_list opt_comma ')'  */
#line 1269 "pkl-tab.y"
                {
                  /* This syntax is only used for array
                     constructors.  */
                  if (PKL_AST_TYPE_CODE ((yyvsp[-4].ast)) != PKL_TYPE_ARRAY)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-4]),
                                 "expected array type in constructor");
                      YYERROR;
                    }

                  (yyval.ast) = pkl_ast_make_cons (pkl_parser->ast, (yyvsp[-4].ast), (yyvsp[-2].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6427 "pkl-tab.c"
    break;

  case 110: /* primary: typename '{' struct_field_list opt_comma '}'  */
#line 1283 "pkl-tab.y"
                {
                  pkl_ast_node astruct;

                  /* This syntax is only used for struct
                     constructors.  */
                  if (PKL_AST_TYPE_CODE ((yyvsp[-4].ast)) != PKL_TYPE_STRUCT)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-4]),
                                 "expected struct type in constructor");
                      YYERROR;
                    }

                  astruct = pkl_ast_make_struct (pkl_parser->ast,
                                           0 /* nelem */, (yyvsp[-2].ast));
                  PKL_AST_LOC (astruct) = (yyloc);

                  (yyval.ast) = pkl_ast_make_cons (pkl_parser->ast, (yyvsp[-4].ast), astruct);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6451 "pkl-tab.c"
    break;

  case 111: /* funcall: primary '(' funcall_arg_list ')'  */
#line 1306 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_funcall (pkl_parser->ast,
                                             (yyvsp[-3].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6461 "pkl-tab.c"
    break;

  case 112: /* funcall_arg_list: %empty  */
#line 1315 "pkl-tab.y"
                { (yyval.ast) = NULL; }
#line 6467 "pkl-tab.c"
    break;

  case 114: /* funcall_arg_list: funcall_arg_list ',' funcall_arg  */
#line 1318 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 6475 "pkl-tab.c"
    break;

  case 115: /* funcall_arg: expression  */
#line 1325 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_funcall_arg (pkl_parser->ast,
                                                 (yyvsp[0].ast), NULL /* name */);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6485 "pkl-tab.c"
    break;

  case 116: /* format_arg_list: %empty  */
#line 1334 "pkl-tab.y"
                { (yyval.ast) = NULL; }
#line 6491 "pkl-tab.c"
    break;

  case 118: /* format_arg_list: format_arg_list ',' format_arg  */
#line 1337 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 6499 "pkl-tab.c"
    break;

  case 119: /* format_arg: expression  */
#line 1344 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_format_arg (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6508 "pkl-tab.c"
    break;

  case 122: /* struct_field_list: %empty  */
#line 1357 "pkl-tab.y"
                { (yyval.ast) = NULL; }
#line 6514 "pkl-tab.c"
    break;

  case 124: /* struct_field_list: struct_field_list ',' struct_field  */
#line 1360 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 6522 "pkl-tab.c"
    break;

  case 125: /* struct_field: expression  */
#line 1367 "pkl-tab.y"
                  {
                    (yyval.ast) = pkl_ast_make_struct_field (pkl_parser->ast,
                                                    NULL /* name */,
                                                    (yyvsp[0].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6533 "pkl-tab.c"
    break;

  case 126: /* struct_field: identifier '=' expression  */
#line 1374 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_struct_field (pkl_parser->ast,
                                                    (yyvsp[-2].ast),
                                                    (yyvsp[0].ast));
                    PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6545 "pkl-tab.c"
    break;

  case 127: /* array: '[' array_initializer_list opt_comma ']'  */
#line 1385 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_array (pkl_parser->ast,
                                             0 /* nelem */,
                                             0 /* ninitializer */,
                                             (yyvsp[-2].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6557 "pkl-tab.c"
    break;

  case 129: /* array_initializer_list: array_initializer_list ',' array_initializer  */
#line 1397 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 6565 "pkl-tab.c"
    break;

  case 130: /* array_initializer: expression  */
#line 1404 "pkl-tab.y"
                  {
                    (yyval.ast) = pkl_ast_make_array_initializer (pkl_parser->ast,
                                                         NULL, (yyvsp[0].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6575 "pkl-tab.c"
    break;

  case 131: /* array_initializer: "dot operator" '[' expression ']' '=' expression  */
#line 1410 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_array_initializer (pkl_parser->ast,
                                                         (yyvsp[-3].ast), (yyvsp[0].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6585 "pkl-tab.c"
    break;

  case 132: /* pushlevel_args: %empty  */
#line 1423 "pkl-tab.y"
                {
                  /* Push the lexical frame for the function's
                     arguments.  */
                  pkl_parser->env = pkl_env_push_frame (pkl_parser->env);

                  /* If in a method, register an argument SELF for the
                     initial implicit argument.  */
                  if (pkl_parser->in_method_decl_p)
                    {
                      pkl_ast_node arg_type
                        = pkl_ast_make_any_type (pkl_parser->ast);
                      pkl_ast_node arg_name
                        = pkl_ast_make_identifier (pkl_parser->ast,
                                                   "SELF");
                      int registered_p __attribute__ ((unused));

                      pkl_ast_node arg
                        = pkl_ast_make_func_arg (pkl_parser->ast,
                                                 arg_type, arg_name,
                                                 NULL /* initial */);

                      registered_p = pkl_register_arg (pkl_parser, arg);
                      assert (registered_p);
                    }
                }
#line 6615 "pkl-tab.c"
    break;

  case 133: /* function_specifier: '(' pushlevel_args function_arg_list ')' simple_type_specifier ':' comp_stmt  */
#line 1452 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_func (pkl_parser->ast,
                                          (yyvsp[-2].ast), (yyvsp[-4].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 6629 "pkl-tab.c"
    break;

  case 134: /* function_specifier: simple_type_specifier ':' pushlevel_args comp_stmt  */
#line 1462 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_func (pkl_parser->ast,
                                          (yyvsp[-3].ast), NULL, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 6643 "pkl-tab.c"
    break;

  case 136: /* function_arg_list: function_arg ',' function_arg_list  */
#line 1476 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 6651 "pkl-tab.c"
    break;

  case 137: /* function_arg: simple_type_specifier identifier function_arg_initial  */
#line 1483 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_func_arg (pkl_parser->ast,
                                              (yyvsp[-2].ast), (yyvsp[-1].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyvsp[-1].ast)) = (yylsp[-1]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  if (!pkl_register_arg (pkl_parser, (yyval.ast)))
                      YYERROR;
                }
#line 6665 "pkl-tab.c"
    break;

  case 138: /* function_arg: identifier "varargs indicator"  */
#line 1493 "pkl-tab.y"
                {
                  pkl_ast_node type
                    = pkl_ast_make_any_type (pkl_parser->ast);
                  pkl_ast_node array_type
                    = pkl_ast_make_array_type (pkl_parser->ast,
                                               type,
                                               NULL /* bound */);

                  PKL_AST_LOC (type) = (yylsp[-1]);
                  PKL_AST_LOC (array_type) = (yylsp[-1]);

                  (yyval.ast) = pkl_ast_make_func_arg (pkl_parser->ast,
                                              array_type,
                                              (yyvsp[-1].ast),
                                              NULL /* initial */);
                  PKL_AST_FUNC_ARG_VARARG ((yyval.ast)) = 1;
                  PKL_AST_LOC ((yyvsp[-1].ast)) = (yylsp[-1]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  if (!pkl_register_arg (pkl_parser, (yyval.ast)))
                      YYERROR;
                }
#line 6692 "pkl-tab.c"
    break;

  case 139: /* function_arg_initial: %empty  */
#line 1518 "pkl-tab.y"
                                      { (yyval.ast) = NULL; }
#line 6698 "pkl-tab.c"
    break;

  case 140: /* function_arg_initial: '=' expression  */
#line 1519 "pkl-tab.y"
                                { (yyval.ast) = (yyvsp[0].ast); }
#line 6704 "pkl-tab.c"
    break;

  case 144: /* typename: "type name"  */
#line 1534 "pkl-tab.y"
                  {
                  pkl_ast_node decl = pkl_env_lookup (pkl_parser->env,
                                                      PKL_ENV_NS_MAIN,
                                                      PKL_AST_IDENTIFIER_POINTER ((yyvsp[0].ast)),
                                                      NULL, NULL);
                  assert (decl != NULL
                          && PKL_AST_DECL_KIND (decl) == PKL_AST_DECL_KIND_TYPE);
                  (yyval.ast) = PKL_AST_DECL_INITIAL (decl);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  (yyvsp[0].ast) = ASTREF ((yyvsp[0].ast)); pkl_ast_node_free ((yyvsp[0].ast));
                }
#line 6720 "pkl-tab.c"
    break;

  case 145: /* string_type_specifier: "string type specifier"  */
#line 1549 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_string_type (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6729 "pkl-tab.c"
    break;

  case 146: /* simple_type_specifier: "any type specifier"  */
#line 1557 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_any_type (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6738 "pkl-tab.c"
    break;

  case 147: /* simple_type_specifier: "void type specifier"  */
#line 1562 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_void_type (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6747 "pkl-tab.c"
    break;

  case 156: /* integral_type_specifier: integral_type_sign integer "bigger-than operator"  */
#line 1581 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_integral_type (pkl_parser->ast,
                                                     PKL_AST_INTEGER_VALUE ((yyvsp[-1].ast)),
                                                     (yyvsp[-2].integer));
                    (yyvsp[-1].ast) = ASTREF ((yyvsp[-1].ast)); pkl_ast_node_free ((yyvsp[-1].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6759 "pkl-tab.c"
    break;

  case 157: /* dynamic_integral_type_specifier: integral_type_sign "multiplication operator" "bigger-than operator"  */
#line 1592 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_integral_type (pkl_parser->ast, 0, (yyvsp[-2].integer));
                    PKL_AST_TYPE_I_DYN_P ((yyval.ast)) = 1;
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6769 "pkl-tab.c"
    break;

  case 158: /* integral_type_sign: "int type constructor"  */
#line 1600 "pkl-tab.y"
                           { (yyval.integer) = 1; }
#line 6775 "pkl-tab.c"
    break;

  case 159: /* integral_type_sign: "uint type constructor"  */
#line 1601 "pkl-tab.y"
                            { (yyval.integer) = 0; }
#line 6781 "pkl-tab.c"
    break;

  case 160: /* ref_type: %empty  */
#line 1605 "pkl-tab.y"
                                      { (yyval.ast) = NULL; }
#line 6787 "pkl-tab.c"
    break;

  case 161: /* ref_type: ',' simple_type_specifier  */
#line 1606 "pkl-tab.y"
                                    { (yyval.ast) = (yyvsp[0].ast); }
#line 6793 "pkl-tab.c"
    break;

  case 162: /* offset_type_specifier: "offset type constructor" simple_type_specifier ',' identifier ref_type "bigger-than operator"  */
#line 1611 "pkl-tab.y"
                {
                  pkl_ast_node decl
                    = pkl_env_lookup (pkl_parser->env,
                                      PKL_ENV_NS_UNITS,
                                      PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)),
                                      NULL, NULL);

                  if (!decl)
                    {
                      /* This could be the name of a type.  Try it out.  */
                      decl = pkl_env_lookup (pkl_parser->env,
                                             PKL_ENV_NS_MAIN,
                                             PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)),
                                             NULL, NULL);

                      if (!decl)
                        {
                          pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-2]),
                                     "invalid unit in offset type");
                          YYERROR;
                        }
                    }

                  (yyval.ast) = pkl_ast_make_offset_type (pkl_parser->ast,
                                                 (yyvsp[-4].ast),
                                                 PKL_AST_DECL_INITIAL (decl),
                                                 (yyvsp[-1].ast));

                  (yyvsp[-2].ast) = ASTREF ((yyvsp[-2].ast)); pkl_ast_node_free ((yyvsp[-2].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6829 "pkl-tab.c"
    break;

  case 163: /* offset_type_specifier: "offset type constructor" simple_type_specifier ',' integer ref_type "bigger-than operator"  */
#line 1643 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_offset_type (pkl_parser->ast,
                                                   (yyvsp[-4].ast), (yyvsp[-2].ast), (yyvsp[-1].ast));
                    PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[-2].ast))) = (yylsp[-2]);
                    PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6841 "pkl-tab.c"
    break;

  case 164: /* array_type_specifier: simple_type_specifier '[' ']'  */
#line 1654 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_array_type (pkl_parser->ast, (yyvsp[-2].ast),
                                                NULL /* bound */);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6851 "pkl-tab.c"
    break;

  case 165: /* array_type_specifier: simple_type_specifier '[' expression ']'  */
#line 1660 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_array_type (pkl_parser->ast, (yyvsp[-3].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6860 "pkl-tab.c"
    break;

  case 166: /* function_type_specifier: '(' function_type_arg_list ')' simple_type_specifier  */
#line 1668 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_function_type (pkl_parser->ast,
                                                   (yyvsp[0].ast), 0 /* narg */,
                                                   (yyvsp[-2].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6871 "pkl-tab.c"
    break;

  case 167: /* function_type_specifier: '(' ')' simple_type_specifier  */
#line 1675 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_function_type (pkl_parser->ast,
                                                   (yyvsp[0].ast), 0 /* narg */,
                                                   NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6882 "pkl-tab.c"
    break;

  case 169: /* function_type_arg_list: function_type_arg ',' function_type_arg_list  */
#line 1686 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 6890 "pkl-tab.c"
    break;

  case 170: /* function_type_arg: simple_type_specifier  */
#line 1693 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_func_type_arg (pkl_parser->ast,
                                                   (yyvsp[0].ast), NULL /* name */);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6900 "pkl-tab.c"
    break;

  case 171: /* function_type_arg: simple_type_specifier '?'  */
#line 1699 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_func_type_arg (pkl_parser->ast,
                                                   (yyvsp[-1].ast), NULL /* name */);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_FUNC_TYPE_ARG_OPTIONAL ((yyval.ast)) = 1;
                }
#line 6911 "pkl-tab.c"
    break;

  case 172: /* function_type_arg: "varargs indicator"  */
#line 1706 "pkl-tab.y"
                {
                  pkl_ast_node type
                    = pkl_ast_make_any_type (pkl_parser->ast);
                  pkl_ast_node array_type
                    = pkl_ast_make_array_type (pkl_parser->ast,
                                               type, NULL /* bound */);

                  PKL_AST_LOC (type) = (yylsp[0]);
                  PKL_AST_LOC (array_type) = (yylsp[0]);

                  (yyval.ast) = pkl_ast_make_func_type_arg (pkl_parser->ast,
                                                   array_type, NULL /* name */);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_FUNC_TYPE_ARG_VARARG ((yyval.ast)) = 1;
                }
#line 6931 "pkl-tab.c"
    break;

  case 173: /* struct_type_specifier: pushlevel struct_type_pinned struct_or_union integral_struct '{' '}'  */
#line 1726 "pkl-tab.y"
                  {
                    (yyval.ast) = pkl_ast_make_struct_type (pkl_parser->ast,
                                                   0 /* nelem */,
                                                   0 /* nfield */,
                                                   0 /* ndecl */,
                                                   (yyvsp[-2].ast),
                                                   NULL /* elems */,
                                                   (yyvsp[-4].integer), (yyvsp[-3].integer));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);

                    /* The pushlevel in this rule and the subsequent
                       pop_frame, while not strictly needed, is to
                       avoid shift/reduce conflicts with the next
                       rule.  */
                    pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 6952 "pkl-tab.c"
    break;

  case 174: /* $@2: %empty  */
#line 1744 "pkl-tab.y"
                {
                  /* Register dummies for the locals used in
                     pkl-gen.pks:struct_mapper (not counting
                     OFFSET).  */
                  pkl_register_dummies (pkl_parser, 5);

                  /* Now register OFFSET with a type of
                     offset<uint<64>,1> */
                  {
                    pkl_ast_node decl, type;
                    pkl_ast_node offset_identifier
                      = pkl_ast_make_identifier (pkl_parser->ast, "OFFSET");
                    pkl_ast_node offset_magnitude
                      = pkl_ast_make_integer (pkl_parser->ast, 0);
                    pkl_ast_node offset_unit
                      = pkl_ast_make_integer (pkl_parser->ast, 1);
                    pkl_ast_node offset;

                    type = pkl_ast_make_integral_type (pkl_parser->ast, 64, 0);
                    PKL_AST_TYPE (offset_magnitude) = ASTREF (type);
                    PKL_AST_TYPE (offset_unit) = ASTREF (type);

                    offset = pkl_ast_make_offset (pkl_parser->ast,
                                                  offset_magnitude,
                                                  offset_unit);
                    type = pkl_ast_make_offset_type (pkl_parser->ast,
                                                     type,
                                                     offset_unit,
                                                     NULL /* ref_type */);
                    PKL_AST_TYPE (offset) = ASTREF (type);

                    decl = pkl_ast_make_decl (pkl_parser->ast,
                                              PKL_AST_DECL_KIND_VAR,
                                              offset_identifier,
                                              offset,
                                              NULL /* source */);

                    if (!pkl_env_register (pkl_parser->env,
                                           PKL_ENV_NS_MAIN,
                                           PKL_AST_IDENTIFIER_POINTER (offset_identifier),
                                           decl))
                      PK_UNREACHABLE ();
                  }
                }
#line 7001 "pkl-tab.c"
    break;

  case 175: /* struct_type_specifier: pushlevel struct_type_pinned struct_or_union integral_struct '{' $@2 struct_type_elem_list '}'  */
#line 1789 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_struct_type (pkl_parser->ast,
                                                   0 /* nelem */,
                                                   0 /* nfield */,
                                                   0 /* ndecl */,
                                                   (yyvsp[-4].ast),
                                                   (yyvsp[-1].ast),
                                                   (yyvsp[-6].integer), (yyvsp[-5].integer));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);

                    /* Pop the frame pushed in the `pushlevel' above.  */
                    pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 7019 "pkl-tab.c"
    break;

  case 176: /* struct_or_union: "keyword `struct'"  */
#line 1805 "pkl-tab.y"
                        { (yyval.integer) = 0; }
#line 7025 "pkl-tab.c"
    break;

  case 177: /* struct_or_union: "keyword `union'"  */
#line 1806 "pkl-tab.y"
                        { (yyval.integer) = 1; }
#line 7031 "pkl-tab.c"
    break;

  case 178: /* struct_type_pinned: %empty  */
#line 1810 "pkl-tab.y"
                        { (yyval.integer) = 0; }
#line 7037 "pkl-tab.c"
    break;

  case 179: /* struct_type_pinned: "keyword `pinned'"  */
#line 1811 "pkl-tab.y"
                        { (yyval.integer) = 1; }
#line 7043 "pkl-tab.c"
    break;

  case 180: /* integral_struct: %empty  */
#line 1815 "pkl-tab.y"
                         { (yyval.ast) = NULL; }
#line 7049 "pkl-tab.c"
    break;

  case 181: /* integral_struct: simple_type_specifier  */
#line 1816 "pkl-tab.y"
                                { (yyval.ast) = (yyvsp[0].ast); }
#line 7055 "pkl-tab.c"
    break;

  case 182: /* integral_struct: dynamic_integral_type_specifier  */
#line 1817 "pkl-tab.y"
                                          { (yyval.ast) = (yyvsp[0].ast); }
#line 7061 "pkl-tab.c"
    break;

  case 186: /* struct_type_elem_list: struct_type_elem_list declaration  */
#line 1825 "pkl-tab.y"
                  { (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast)); }
#line 7067 "pkl-tab.c"
    break;

  case 187: /* struct_type_elem_list: struct_type_elem_list struct_type_field  */
#line 1827 "pkl-tab.y"
                { (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast)); }
#line 7073 "pkl-tab.c"
    break;

  case 188: /* struct_type_elem_list: struct_type_elem_list struct_type_computed_field  */
#line 1829 "pkl-tab.y"
                { (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast)); }
#line 7079 "pkl-tab.c"
    break;

  case 189: /* endianness: %empty  */
#line 1833 "pkl-tab.y"
                        { (yyval.integer) = PKL_AST_ENDIAN_DFL; }
#line 7085 "pkl-tab.c"
    break;

  case 190: /* endianness: "keyword `little'"  */
#line 1834 "pkl-tab.y"
                        { (yyval.integer) = PKL_AST_ENDIAN_LSB; }
#line 7091 "pkl-tab.c"
    break;

  case 191: /* endianness: "keyword `big'"  */
#line 1835 "pkl-tab.y"
                             { (yyval.integer) = PKL_AST_ENDIAN_MSB; }
#line 7097 "pkl-tab.c"
    break;

  case 192: /* struct_type_computed_field: "keyword `computed'" type_specifier identifier ';'  */
#line 1840 "pkl-tab.y"
                  {
                    (yyval.ast) = pkl_ast_make_struct_type_field (pkl_parser->ast, (yyvsp[-1].ast), (yyvsp[-2].ast),
                                                         NULL /* constraint */,
                                                         NULL /* initializer */,
                                                         NULL /* label */,
                                                         PKL_AST_ENDIAN_DFL,
                                                         NULL /* optcond_pre */,
                                                         NULL /* optcond_post */);
                    PKL_AST_STRUCT_TYPE_FIELD_COMPUTED_P ((yyval.ast)) = 1;
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  }
#line 7113 "pkl-tab.c"
    break;

  case 193: /* $@3: %empty  */
#line 1856 "pkl-tab.y"
                  {
                    /* Register a variable in the current environment
                       for the field.  We do it in this mid-rule so
                       the element can be used in the constraint.  */

                    pkl_ast_node dummy, decl;
                    pkl_ast_node identifier
                      = ((yyvsp[0].ast) != NULL
                         ? (yyvsp[0].ast)
                         : pkl_ast_make_identifier (pkl_parser->ast, ""));


                    dummy = pkl_ast_make_integer (pkl_parser->ast, 0);
                    PKL_AST_TYPE (dummy) = ASTREF ((yyvsp[-1].ast));
                    decl = pkl_ast_make_decl (pkl_parser->ast,
                                              PKL_AST_DECL_KIND_VAR,
                                              identifier, dummy,
                                              NULL /* source */);
                    PKL_AST_DECL_STRUCT_FIELD_P (decl) = 1;
                    PKL_AST_LOC (decl) = (yyloc);

                    if (!pkl_env_register (pkl_parser->env,
                                           PKL_ENV_NS_MAIN,
                                           PKL_AST_IDENTIFIER_POINTER (identifier),
                                           decl))
                      {
                        pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                                   "duplicated struct element '%s'",
                                   PKL_AST_IDENTIFIER_POINTER ((yyvsp[0].ast)));
                        YYERROR;
                      }

                    if (identifier)
                      {
                        identifier = ASTREF (identifier);
                        pkl_ast_node_free (identifier);
                      }
                  }
#line 7156 "pkl-tab.c"
    break;

  case 194: /* struct_type_field: struct_type_field_optcond_pre endianness type_specifier struct_type_field_identifier $@3 struct_type_field_constraint_and_init struct_type_field_label struct_type_field_optcond_post ';'  */
#line 1896 "pkl-tab.y"
                  {
                    pkl_ast_node constraint = (yyvsp[-3].field_const_init).constraint;
                    pkl_ast_node initializer = (yyvsp[-3].field_const_init).initializer;
                    int impl_constraint_p = (yyvsp[-3].field_const_init).impl_constraint_p;

                    if (initializer)
                      {
                        pkl_ast_node field_decl, field_var;
                        int back, over;

                        /* We need a field name.  */
                        if ((yyvsp[-5].ast) == NULL)
                          {
                            pkl_error (pkl_parser->compiler, pkl_parser->ast, (yyloc),
                                       "no initializer allowed in anonymous field");
                            YYERROR;
                          }

                        /* Build a constraint derived from the
                           initializer if a constraint has not been
                           specified.  */
                        if (impl_constraint_p)
                          {
                            field_decl = pkl_env_lookup (pkl_parser->env,
                                                         PKL_ENV_NS_MAIN,
                                                         PKL_AST_IDENTIFIER_POINTER ((yyvsp[-5].ast)),
                                                         &back, &over);
                            assert (field_decl);

                            field_var = pkl_ast_make_var (pkl_parser->ast,
                                                          (yyvsp[-5].ast),
                                                          field_decl,
                                                          back, over);
                            PKL_AST_LOC (field_var) = PKL_AST_LOC (initializer);

                            constraint = pkl_ast_make_binary_exp (pkl_parser->ast,
                                                                  PKL_AST_OP_EQ,
                                                                  field_var,
                                                                  initializer);
                            PKL_AST_LOC (constraint) = PKL_AST_LOC (initializer);
                          }
                      }

                    (yyval.ast) = pkl_ast_make_struct_type_field (pkl_parser->ast, (yyvsp[-5].ast), (yyvsp[-6].ast),
                                                         constraint, initializer,
                                                         (yyvsp[-2].ast), (yyvsp[-7].integer), (yyvsp[-8].ast), (yyvsp[-1].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);

                    if (constraint)
                      {
                        char *code = pkl_loc_to_source (pkl_parser,
                                                        PKL_AST_LOC (constraint),
                                                        256);
                        if (impl_constraint_p)
                          {
                            assert ((yyvsp[-5].ast) != NULL);
                            code = pk_str_concat (PKL_AST_IDENTIFIER_POINTER ((yyvsp[-5].ast)),
                                                  " == ", code, NULL);
                          }

                        PKL_AST_STRUCT_TYPE_FIELD_CONSTRAINT_SRC ((yyval.ast))
                          = code;
                      }

                    /* If endianness is empty or there is no
                       pre-condition in the field, bison includes the
                       blank characters before the type field as if
                       they were part of this rule.  Therefore the
                       location should be adjusted here.  */
                    if ((yyvsp[-7].integer) == PKL_AST_ENDIAN_DFL || (yyvsp[-8].ast) == NULL)
                      {
                        PKL_AST_LOC ((yyval.ast)).first_line = (yylsp[-5]).first_line;
                        PKL_AST_LOC ((yyval.ast)).first_column = (yylsp[-5]).first_column;
                      }

                    if ((yyvsp[-5].ast) != NULL)
                      {
                        PKL_AST_LOC ((yyvsp[-5].ast)) = (yylsp[-5]);
                        PKL_AST_TYPE ((yyvsp[-5].ast)) = pkl_ast_make_string_type (pkl_parser->ast);
                        PKL_AST_TYPE ((yyvsp[-5].ast)) = ASTREF (PKL_AST_TYPE ((yyvsp[-5].ast)));
                        PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[-5].ast))) = (yylsp[-5]);
                      }
                  }
#line 7244 "pkl-tab.c"
    break;

  case 195: /* struct_type_field_identifier: %empty  */
#line 1982 "pkl-tab.y"
                        { (yyval.ast) = NULL; }
#line 7250 "pkl-tab.c"
    break;

  case 196: /* struct_type_field_identifier: identifier  */
#line 1983 "pkl-tab.y"
                            { (yyval.ast) = (yyvsp[0].ast); }
#line 7256 "pkl-tab.c"
    break;

  case 197: /* struct_type_field_label: %empty  */
#line 1988 "pkl-tab.y"
                {
                  (yyval.ast) = NULL;
                }
#line 7264 "pkl-tab.c"
    break;

  case 198: /* struct_type_field_label: "map operator" expression  */
#line 1992 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[0]);
                }
#line 7273 "pkl-tab.c"
    break;

  case 199: /* struct_type_field_constraint_and_init: %empty  */
#line 2000 "pkl-tab.y"
                {
                  (yyval.field_const_init).constraint = NULL;
                  (yyval.field_const_init).initializer = NULL;
                  (yyval.field_const_init).impl_constraint_p = 0;
                }
#line 7283 "pkl-tab.c"
    break;

  case 200: /* struct_type_field_constraint_and_init: ':' expression  */
#line 2006 "pkl-tab.y"
                {
                  (yyval.field_const_init).constraint = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.field_const_init).constraint) = (yylsp[0]);
                  (yyval.field_const_init).initializer = NULL;
                  (yyval.field_const_init).impl_constraint_p = 0;
                }
#line 7294 "pkl-tab.c"
    break;

  case 201: /* struct_type_field_constraint_and_init: '=' expression  */
#line 2013 "pkl-tab.y"
                {
                  (yyval.field_const_init).constraint = NULL;
                  (yyval.field_const_init).initializer = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.field_const_init).initializer) = (yylsp[0]);
                  (yyval.field_const_init).impl_constraint_p = 0;
                }
#line 7305 "pkl-tab.c"
    break;

  case 202: /* struct_type_field_constraint_and_init: '=' expression ':' expression  */
#line 2020 "pkl-tab.y"
                {
                  (yyval.field_const_init).constraint = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.field_const_init).constraint) = (yylsp[0]);
                  (yyval.field_const_init).initializer = (yyvsp[-2].ast);
                  PKL_AST_LOC ((yyval.field_const_init).initializer) = (yylsp[-2]);
                  (yyval.field_const_init).impl_constraint_p = 0;
                }
#line 7317 "pkl-tab.c"
    break;

  case 203: /* struct_type_field_constraint_and_init: ':' expression '=' expression  */
#line 2028 "pkl-tab.y"
                {
                  (yyval.field_const_init).constraint = (yyvsp[-2].ast);
                  PKL_AST_LOC ((yyval.field_const_init).constraint) = (yylsp[-2]);
                  (yyval.field_const_init).initializer = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.field_const_init).initializer) = (yylsp[0]);
                  (yyval.field_const_init).impl_constraint_p = 0;
                }
#line 7329 "pkl-tab.c"
    break;

  case 204: /* struct_type_field_constraint_and_init: "equality operator" expression  */
#line 2036 "pkl-tab.y"
                {
                  (yyval.field_const_init).constraint = NULL;
                  (yyval.field_const_init).initializer = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.field_const_init).initializer) = (yylsp[0]);
                  (yyval.field_const_init).impl_constraint_p = 1;
                }
#line 7340 "pkl-tab.c"
    break;

  case 205: /* struct_type_field_optcond_pre: %empty  */
#line 2046 "pkl-tab.y"
                {
                  (yyval.ast) = NULL;
                }
#line 7348 "pkl-tab.c"
    break;

  case 206: /* struct_type_field_optcond_pre: "keyword `if'" '(' expression ')'  */
#line 2050 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[-1].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-1]);
                }
#line 7357 "pkl-tab.c"
    break;

  case 207: /* struct_type_field_optcond_post: %empty  */
#line 2058 "pkl-tab.y"
                {
                  (yyval.ast) = NULL;
                }
#line 7365 "pkl-tab.c"
    break;

  case 208: /* struct_type_field_optcond_post: "keyword `if'" expression  */
#line 2062 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[0]);
                }
#line 7374 "pkl-tab.c"
    break;

  case 209: /* simple_declaration: "keyword `var'" defvar_list  */
#line 2073 "pkl-tab.y"
                               { (yyval.ast) = (yyvsp[0].ast); }
#line 7380 "pkl-tab.c"
    break;

  case 210: /* simple_declaration: "keyword `type'" deftype_list  */
#line 2074 "pkl-tab.y"
                               { (yyval.ast) = (yyvsp[0].ast); }
#line 7386 "pkl-tab.c"
    break;

  case 211: /* simple_declaration: "keyword `unit'" defunit_list  */
#line 2075 "pkl-tab.y"
                               { (yyval.ast) = (yyvsp[0].ast); }
#line 7392 "pkl-tab.c"
    break;

  case 212: /* @4: %empty  */
#line 2080 "pkl-tab.y"
                {
                  /* In order to allow for the function to be called
                     from within itself (recursive calls) we should
                     register a partial declaration in the
                     compile-time environment before processing the
                     `function_specifier' below.  */

                  (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                               PKL_AST_DECL_KIND_FUNC, (yyvsp[0].ast),
                                               NULL /* initial */,
                                               pkl_parser->filename);
                  PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  if (!pkl_env_register (pkl_parser->env,
                                         PKL_ENV_NS_MAIN,
                                         PKL_AST_IDENTIFIER_POINTER ((yyvsp[0].ast)),
                                         (yyval.ast)))
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                                 "function, method or variable `%s' already defined",
                                 PKL_AST_IDENTIFIER_POINTER ((yyvsp[0].ast)));
                      YYERROR;
                    }

                  /* function_specifier needs to know whether we are
                     in a function declaration or a method
                     declaration.  */
                  pkl_parser->in_method_decl_p = ((yyvsp[-1].integer) == IS_METHOD);
                }
#line 7427 "pkl-tab.c"
    break;

  case 213: /* declaration: defun_or_method identifier @4 '=' function_specifier  */
#line 2111 "pkl-tab.y"
                {
                  /* Complete the declaration registered above with
                     it's initial value, which is the specifier of the
                     function being defined.  */
                  PKL_AST_DECL_INITIAL ((yyvsp[-2].ast))
                    = ASTREF ((yyvsp[0].ast));
                  (yyval.ast) = (yyvsp[-2].ast);

                  /* If the reference counting of the declaration is
                     bigger than 1, this means there are recursive
                     calls in the function body.  Reset the refcount
                     to 1, since these references are weak.  */
                  if (PKL_AST_REFCOUNT ((yyvsp[-2].ast)) > 1)
                    PKL_AST_REFCOUNT ((yyvsp[-2].ast)) = 1;

                  /* Annotate the function to be a method whenever
                     appropriate.  */
                  if ((yyvsp[-4].integer) == IS_METHOD)
                    PKL_AST_FUNC_METHOD_P ((yyvsp[0].ast)) = 1;

                  pkl_parser->in_method_decl_p = 0;
                }
#line 7454 "pkl-tab.c"
    break;

  case 214: /* declaration: simple_declaration ';'  */
#line 2133 "pkl-tab.y"
                                 { (yyval.ast) = (yyvsp[-1].ast); }
#line 7460 "pkl-tab.c"
    break;

  case 215: /* defun_or_method: "keyword `fun'"  */
#line 2137 "pkl-tab.y"
                               { (yyval.integer) = IS_DEFUN; }
#line 7466 "pkl-tab.c"
    break;

  case 216: /* defun_or_method: "keyword `method'"  */
#line 2138 "pkl-tab.y"
                        { (yyval.integer) = IS_METHOD; }
#line 7472 "pkl-tab.c"
    break;

  case 218: /* defvar_list: defvar_list ',' defvar  */
#line 2144 "pkl-tab.y"
          { (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast)); }
#line 7478 "pkl-tab.c"
    break;

  case 219: /* defvar: identifier '=' expression  */
#line 2149 "pkl-tab.y"
            {
                (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                        PKL_AST_DECL_KIND_VAR, (yyvsp[-2].ast), (yyvsp[0].ast),
                                        pkl_parser->filename);
                PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
                PKL_AST_LOC ((yyval.ast)) = (yyloc);

                if (!pkl_env_register (pkl_parser->env,
                                       PKL_ENV_NS_MAIN,
                                       PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)),
                                       (yyval.ast)))
                  {
                    pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-2]),
                               "the variable `%s' is already defined",
                               PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)));
                    YYERROR;
                  }
          }
#line 7501 "pkl-tab.c"
    break;

  case 221: /* deftype_list: deftype_list ',' deftype  */
#line 2172 "pkl-tab.y"
          { (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast)); }
#line 7507 "pkl-tab.c"
    break;

  case 222: /* deftype: identifier '=' type_specifier  */
#line 2177 "pkl-tab.y"
          {
            (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                    PKL_AST_DECL_KIND_TYPE, (yyvsp[-2].ast), (yyvsp[0].ast),
                                    pkl_parser->filename);
            PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
            PKL_AST_LOC ((yyval.ast)) = (yyloc);

            PKL_AST_TYPE_NAME ((yyvsp[0].ast)) = ASTREF ((yyvsp[-2].ast));

            if (!pkl_env_register (pkl_parser->env,
                                   PKL_ENV_NS_MAIN,
                                   PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)),
                                   (yyval.ast)))
              {
                pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-2]),
                           "the type `%s' is already defined",
                           PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)));
                YYERROR;
              }
          }
#line 7532 "pkl-tab.c"
    break;

  case 224: /* defunit_list: defunit_list ',' defunit  */
#line 2202 "pkl-tab.y"
          { (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast)); }
#line 7538 "pkl-tab.c"
    break;

  case 225: /* defunit: identifier '=' expression  */
#line 2207 "pkl-tab.y"
            {
              /* We need to cast the expression to uint<64> here,
                 instead of pkl-promo, because the installed
                 initializer is used as earlier as in the lexer.  Not
                 pretty.  */
              pkl_ast_node type
                = pkl_ast_make_integral_type (pkl_parser->ast,
                                              64, 0);
              pkl_ast_node cast
                = pkl_ast_make_cast (pkl_parser->ast,
                                     type, (yyvsp[0].ast));

              (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                      PKL_AST_DECL_KIND_UNIT, (yyvsp[-2].ast), cast,
                                      pkl_parser->filename);

              PKL_AST_LOC (type) = (yylsp[0]);
              PKL_AST_LOC (cast) = (yylsp[0]);
              PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
              PKL_AST_LOC ((yyval.ast)) = (yyloc);

              if (!pkl_env_register (pkl_parser->env,
                                     PKL_ENV_NS_UNITS,
                                     PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)),
                                     (yyval.ast)))
                {
                  pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-2]),
                             "the unit `%s' is already defined",
                             PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)));
                  YYERROR;
                }
            }
#line 7575 "pkl-tab.c"
    break;

  case 226: /* comp_stmt: pushlevel '{' '}'  */
#line 2246 "pkl-tab.y"
            {
              (yyval.ast) = pkl_ast_make_comp_stmt (pkl_parser->ast, NULL);
              PKL_AST_LOC ((yyval.ast)) = (yyloc);

              /* Pop the frame pushed by the `pushlevel' above.  */
              pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
            }
#line 7587 "pkl-tab.c"
    break;

  case 227: /* comp_stmt: pushlevel '{' stmt_decl_list '}'  */
#line 2254 "pkl-tab.y"
            {
              (yyval.ast) = pkl_ast_make_comp_stmt (pkl_parser->ast, (yyvsp[-1].ast));
              PKL_AST_LOC ((yyval.ast)) = (yyloc);

              /* Pop the frame pushed by the `pushlevel' above.  */
              pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
            }
#line 7599 "pkl-tab.c"
    break;

  case 229: /* stmt_decl_list: stmt_decl_list stmt  */
#line 2266 "pkl-tab.y"
                  { (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast)); }
#line 7605 "pkl-tab.c"
    break;

  case 231: /* stmt_decl_list: stmt_decl_list declaration  */
#line 2269 "pkl-tab.y"
                  { (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast)); }
#line 7611 "pkl-tab.c"
    break;

  case 232: /* ass_exp_op: "power-and-assign operator"  */
#line 2273 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_POW; }
#line 7617 "pkl-tab.c"
    break;

  case 233: /* ass_exp_op: "multiply-and-assign operator"  */
#line 2274 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_MUL; }
#line 7623 "pkl-tab.c"
    break;

  case 234: /* ass_exp_op: "divide-and-assing operator"  */
#line 2275 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_DIV; }
#line 7629 "pkl-tab.c"
    break;

  case 235: /* ass_exp_op: "modulus-and-assign operator"  */
#line 2276 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_MOD; }
#line 7635 "pkl-tab.c"
    break;

  case 236: /* ass_exp_op: "add-and-assing operator"  */
#line 2277 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_ADD; }
#line 7641 "pkl-tab.c"
    break;

  case 237: /* ass_exp_op: "subtract-and-assign operator"  */
#line 2278 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_SUB; }
#line 7647 "pkl-tab.c"
    break;

  case 238: /* ass_exp_op: "shift-left-and-assign operator"  */
#line 2279 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_SL; }
#line 7653 "pkl-tab.c"
    break;

  case 239: /* ass_exp_op: "shift-right-and-assign operator"  */
#line 2280 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_SR; }
#line 7659 "pkl-tab.c"
    break;

  case 240: /* ass_exp_op: "bit-and-and-assign operator"  */
#line 2281 "pkl-tab.y"
                { (yyval.integer) = PKL_AST_OP_BAND; }
#line 7665 "pkl-tab.c"
    break;

  case 241: /* ass_exp_op: "bit-or-and-assign operator"  */
#line 2282 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_IOR; }
#line 7671 "pkl-tab.c"
    break;

  case 242: /* ass_exp_op: "bit-xor-and-assign operator"  */
#line 2283 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_XOR; }
#line 7677 "pkl-tab.c"
    break;

  case 243: /* simple_stmt_list: %empty  */
#line 2287 "pkl-tab.y"
                 { (yyval.ast) = NULL; }
#line 7683 "pkl-tab.c"
    break;

  case 245: /* simple_stmt_list: simple_stmt_list ',' simple_stmt  */
#line 2290 "pkl-tab.y"
                 { (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast)); }
#line 7689 "pkl-tab.c"
    break;

  case 246: /* simple_stmt: primary '=' expression  */
#line 2295 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_ass_stmt (pkl_parser->ast,
                                              (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7699 "pkl-tab.c"
    break;

  case 247: /* simple_stmt: primary ass_exp_op expression  */
#line 2301 "pkl-tab.y"
                {
                  pkl_ast_node exp
                    = pkl_ast_make_binary_exp (pkl_parser->ast,
                                               (yyvsp[-1].integer), (yyvsp[-2].ast), (yyvsp[0].ast));

                  (yyval.ast) = pkl_ast_make_ass_stmt (pkl_parser->ast,
                                              (yyvsp[-2].ast), exp);
                  PKL_AST_LOC (exp) = (yyloc);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7714 "pkl-tab.c"
    break;

  case 248: /* simple_stmt: bconc '=' expression  */
#line 2312 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_ass_stmt (pkl_parser->ast,
                                              (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7724 "pkl-tab.c"
    break;

  case 249: /* simple_stmt: map '=' expression  */
#line 2318 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_ass_stmt (pkl_parser->ast,
                                              (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7734 "pkl-tab.c"
    break;

  case 250: /* simple_stmt: expression  */
#line 2324 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_exp_stmt (pkl_parser->ast,
                                              (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7744 "pkl-tab.c"
    break;

  case 251: /* simple_stmt: "keyword `printf'" '(' "string" format_arg_list ')'  */
#line 2330 "pkl-tab.y"
                {
                  pkl_ast_node format =
                    pkl_ast_make_format (pkl_parser->ast, (yyvsp[-2].ast), (yyvsp[-1].ast),
                                         1 /* printf_p */);
                  (yyval.ast) = pkl_ast_make_print_stmt (pkl_parser->ast,
                                                1 /* printf_p */, format);
                  PKL_AST_LOC (format) = (yyloc);
                  PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
                  if (PKL_AST_TYPE ((yyvsp[-2].ast)))
                    PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[-2].ast))) = (yylsp[-2]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7761 "pkl-tab.c"
    break;

  case 252: /* simple_stmt: "keyword `assert'" '(' expression ')'  */
#line 2343 "pkl-tab.y"
                {
                  if (((yyval.ast) = pkl_make_assertion (pkl_parser, (yyvsp[-1].ast), NULL, (yyloc)))
                      == NULL)
                    YYERROR;
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7772 "pkl-tab.c"
    break;

  case 253: /* simple_stmt: "keyword `assert'" '(' expression ',' expression ')'  */
#line 2350 "pkl-tab.y"
                {
                  if (((yyval.ast) = pkl_make_assertion (pkl_parser, (yyvsp[-3].ast), (yyvsp[-1].ast), (yyloc)))
                      == NULL)
                    YYERROR;
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7783 "pkl-tab.c"
    break;

  case 254: /* simple_stmt: funcall_stmt  */
#line 2357 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_exp_stmt (pkl_parser->ast,
                                              (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7793 "pkl-tab.c"
    break;

  case 255: /* simple_stmt: "keyword `asm'" '(' expression ')'  */
#line 2363 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_asm_stmt (pkl_parser->ast,
                                              (yyvsp[-1].ast), NULL, NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7803 "pkl-tab.c"
    break;

  case 256: /* simple_stmt: "keyword `asm'" '(' expression ':' expression_list ')'  */
#line 2369 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_asm_stmt (pkl_parser->ast,
                                              (yyvsp[-3].ast), NULL, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7813 "pkl-tab.c"
    break;

  case 257: /* simple_stmt: "keyword `asm'" '(' expression ':' expression_list ':' expression_list ')'  */
#line 2375 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_asm_stmt (pkl_parser->ast,
                                              (yyvsp[-5].ast), (yyvsp[-1].ast), (yyvsp[-3].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7823 "pkl-tab.c"
    break;

  case 259: /* stmt: ';'  */
#line 2385 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_null_stmt (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7832 "pkl-tab.c"
    break;

  case 260: /* stmt: simple_stmt ';'  */
#line 2390 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[-1].ast);
                }
#line 7840 "pkl-tab.c"
    break;

  case 261: /* stmt: "keyword `if'" '(' expression ')' stmt  */
#line 2394 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_if_stmt (pkl_parser->ast,
                                             (yyvsp[-2].ast), (yyvsp[0].ast), NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7850 "pkl-tab.c"
    break;

  case 262: /* stmt: "keyword `if'" '(' expression ')' stmt "keyword `else'" stmt  */
#line 2400 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_if_stmt (pkl_parser->ast,
                                             (yyvsp[-4].ast), (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7860 "pkl-tab.c"
    break;

  case 263: /* stmt: "keyword `while'" '(' expression ')' stmt  */
#line 2406 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_loop_stmt (pkl_parser->ast,
                                               PKL_AST_LOOP_STMT_KIND_WHILE,
                                               NULL, /* iterator */
                                               (yyvsp[-2].ast),   /* condition */
                                               NULL, /* head */
                                               NULL, /* tail */
                                               (yyvsp[0].ast));  /* body */
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7875 "pkl-tab.c"
    break;

  case 264: /* stmt: "keyword `for'" '(' pushlevel simple_declaration ';' expression_opt ';' simple_stmt_list ')' stmt  */
#line 2417 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_loop_stmt (pkl_parser->ast,
                                               PKL_AST_LOOP_STMT_KIND_FOR,
                                               NULL, /* iterator */
                                               (yyvsp[-4].ast),   /* condition */
                                               (yyvsp[-6].ast),   /* head */
                                               (yyvsp[-2].ast),   /* tail */
                                               (yyvsp[0].ast)); /* body */
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 7894 "pkl-tab.c"
    break;

  case 265: /* stmt: "keyword `for'" '(' ';' expression_opt ';' simple_stmt_list ')' stmt  */
#line 2432 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_loop_stmt (pkl_parser->ast,
                                               PKL_AST_LOOP_STMT_KIND_FOR,
                                               NULL, /* iterator */
                                               (yyvsp[-4].ast),   /* condition */
                                               NULL, /* head */
                                               (yyvsp[-2].ast),   /* tail */
                                               (yyvsp[0].ast));  /* body */

                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7910 "pkl-tab.c"
    break;

  case 266: /* @5: %empty  */
#line 2444 "pkl-tab.y"
                {
                  /* Push a new lexical level and register a variable
                     with name IDENTIFIER.  Note that the variable is
                     created with a dummy INITIAL, as there is none.  */

                  pkl_ast_node dummy = pkl_ast_make_integer (pkl_parser->ast,
                                                             0);
                  PKL_AST_LOC (dummy) = (yylsp[-3]);

                  (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                               PKL_AST_DECL_KIND_VAR,
                                               (yyvsp[-3].ast),
                                               dummy,
                                               pkl_parser->filename);
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-3]);

                  if (!pkl_env_register (pkl_parser->env,
                                         PKL_ENV_NS_MAIN,
                                         PKL_AST_IDENTIFIER_POINTER ((yyvsp[-3].ast)),
                                         (yyval.ast)))
                    /* This should never happen.  */
                    PK_UNREACHABLE ();
                }
#line 7938 "pkl-tab.c"
    break;

  case 267: /* stmt: "keyword `for'" '(' "identifier" "keyword `in'" expression pushlevel @5 ')' stmt  */
#line 2468 "pkl-tab.y"
                {
                  pkl_ast_node iterator
                    = pkl_ast_make_loop_stmt_iterator (pkl_parser->ast,
                                                       (yyvsp[-2].ast), /* decl */
                                                       (yyvsp[-4].ast)); /* container */
                  PKL_AST_LOC (iterator) = (yyloc);

                  (yyval.ast) = pkl_ast_make_loop_stmt (pkl_parser->ast,
                                               PKL_AST_LOOP_STMT_KIND_FOR_IN,
                                               iterator,
                                               NULL, /* condition */
                                               NULL, /* head */
                                               NULL, /* tail */
                                               (yyvsp[0].ast));  /* body */
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Free the identifier.  */
                  (yyvsp[-6].ast) = ASTREF ((yyvsp[-6].ast)); pkl_ast_node_free ((yyvsp[-6].ast));

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 7966 "pkl-tab.c"
    break;

  case 268: /* @6: %empty  */
#line 2492 "pkl-tab.y"
                {
                  /* XXX: avoid code replication here.  */

                  /* Push a new lexical level and register a variable
                     with name IDENTIFIER.  Note that the variable is
                     created with a dummy INITIAL, as there is none.  */

                  pkl_ast_node dummy = pkl_ast_make_integer (pkl_parser->ast,
                                                             0);
                  PKL_AST_LOC (dummy) = (yylsp[-3]);

                  (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                               PKL_AST_DECL_KIND_VAR,
                                               (yyvsp[-3].ast),
                                               dummy,
                                               pkl_parser->filename);
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-3]);

                  if (!pkl_env_register (pkl_parser->env,
                                         PKL_ENV_NS_MAIN,
                                         PKL_AST_IDENTIFIER_POINTER ((yyvsp[-3].ast)),
                                         (yyval.ast)))
                    /* This should never happen.  */
                    PK_UNREACHABLE ();
                }
#line 7996 "pkl-tab.c"
    break;

  case 269: /* stmt: "keyword `for'" '(' "identifier" "keyword `in'" expression pushlevel @6 "keyword `where'" expression ')' stmt  */
#line 2518 "pkl-tab.y"
                {
                  pkl_ast_node iterator
                    = pkl_ast_make_loop_stmt_iterator (pkl_parser->ast,
                                                       (yyvsp[-4].ast), /* decl */
                                                       (yyvsp[-6].ast)); /* container */
                  PKL_AST_LOC (iterator) = (yyloc);

                  (yyval.ast) = pkl_ast_make_loop_stmt (pkl_parser->ast,
                                               PKL_AST_LOOP_STMT_KIND_FOR_IN,
                                               iterator,
                                               (yyvsp[-2].ast), /* condition */
                                               NULL, /* head */
                                               NULL, /* tail */
                                               (yyvsp[0].ast)); /* body */
                  PKL_AST_LOC ((yyvsp[-8].ast)) = (yylsp[-8]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 8022 "pkl-tab.c"
    break;

  case 270: /* stmt: "keyword `break'" ';'  */
#line 2540 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_break_continue_stmt (pkl_parser->ast,
                                                         PKL_AST_BREAK_CONTINUE_STMT_KIND_BREAK);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8032 "pkl-tab.c"
    break;

  case 271: /* stmt: "keyword `continue'" ';'  */
#line 2546 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_break_continue_stmt (pkl_parser->ast,
                                                         PKL_AST_BREAK_CONTINUE_STMT_KIND_CONTINUE);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8042 "pkl-tab.c"
    break;

  case 272: /* stmt: "keyword `return'" ';'  */
#line 2552 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_return_stmt (pkl_parser->ast,
                                                 NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8052 "pkl-tab.c"
    break;

  case 273: /* stmt: "keyword `return'" expression ';'  */
#line 2558 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_return_stmt (pkl_parser->ast,
                                                 (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8062 "pkl-tab.c"
    break;

  case 274: /* stmt: "keyword `try'" stmt "keyword `catch'" comp_stmt  */
#line 2564 "pkl-tab.y"
                {
                  pkl_ast_node body
                    = pkl_ast_make_try_stmt_body (pkl_parser->ast, (yyvsp[-2].ast));
                  pkl_ast_node handler
                    = pkl_ast_make_try_stmt_handler (pkl_parser->ast, (yyvsp[0].ast));

                  (yyval.ast) = pkl_ast_make_try_stmt (pkl_parser->ast,
                                              PKL_AST_TRY_STMT_KIND_CATCH,
                                              body, handler, NULL, NULL);
                  PKL_AST_LOC (body) = (yylsp[-2]);
                  PKL_AST_LOC (handler) = (yylsp[0]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8080 "pkl-tab.c"
    break;

  case 275: /* stmt: "keyword `try'" stmt "keyword `catch'" "keyword `if'" expression comp_stmt  */
#line 2578 "pkl-tab.y"
                {
                  pkl_ast_node body
                    = pkl_ast_make_try_stmt_body (pkl_parser->ast, (yyvsp[-4].ast));
                  pkl_ast_node handler
                    = pkl_ast_make_try_stmt_handler (pkl_parser->ast, (yyvsp[0].ast));

                  (yyval.ast) = pkl_ast_make_try_stmt (pkl_parser->ast,
                                              PKL_AST_TRY_STMT_KIND_CATCH,
                                              body, handler, NULL, (yyvsp[-1].ast));
                  PKL_AST_LOC (body) = (yylsp[-4]);
                  PKL_AST_LOC (handler) = (yylsp[0]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8098 "pkl-tab.c"
    break;

  case 276: /* stmt: "keyword `try'" stmt "keyword `catch'" '(' pushlevel function_arg ')' comp_stmt  */
#line 2592 "pkl-tab.y"
                {
                  pkl_ast_node body
                    = pkl_ast_make_try_stmt_body (pkl_parser->ast, (yyvsp[-6].ast));
                  pkl_ast_node handler
                    = pkl_ast_make_try_stmt_handler (pkl_parser->ast, (yyvsp[0].ast));

                  (yyval.ast) = pkl_ast_make_try_stmt (pkl_parser->ast,
                                              PKL_AST_TRY_STMT_KIND_CATCH,
                                              body, handler, (yyvsp[-2].ast), NULL);
                  PKL_AST_LOC (body) = (yylsp[-6]);
                  PKL_AST_LOC (handler) = (yylsp[0]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 8120 "pkl-tab.c"
    break;

  case 277: /* stmt: "keyword `try'" stmt "keyword `until'" expression ';'  */
#line 2610 "pkl-tab.y"
                {
                  pkl_ast_node body = pkl_ast_make_try_stmt_body (pkl_parser->ast,
                                                                  (yyvsp[-3].ast));

                  (yyval.ast) = pkl_ast_make_try_stmt (pkl_parser->ast,
                                              PKL_AST_TRY_STMT_KIND_UNTIL,
                                              body, NULL /* handler */,
                                              NULL /* arg */, (yyvsp[-1].ast));
                  PKL_AST_LOC (body) = (yylsp[-3]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8136 "pkl-tab.c"
    break;

  case 278: /* stmt: "keyword `raise'" ';'  */
#line 2622 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_raise_stmt (pkl_parser->ast,
                                                NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8146 "pkl-tab.c"
    break;

  case 279: /* stmt: "keyword `raise'" expression ';'  */
#line 2628 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_raise_stmt (pkl_parser->ast,
                                                (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8156 "pkl-tab.c"
    break;

  case 280: /* stmt: "keyword `print'" expression ';'  */
#line 2634 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_print_stmt (pkl_parser->ast,
                                                0 /* printf_p */, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8166 "pkl-tab.c"
    break;

  case 281: /* stmt: "keyword `printf'" "string" format_arg_list ';'  */
#line 2640 "pkl-tab.y"
                {
                  pkl_ast_node format =
                    pkl_ast_make_format (pkl_parser->ast, (yyvsp[-2].ast), (yyvsp[-1].ast),
                                         1 /* printf_p */);
                  (yyval.ast) = pkl_ast_make_print_stmt (pkl_parser->ast,
                                                1 /* printf_p */, format);
                  PKL_AST_LOC (format) = (yyloc);
                  PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
                  if (PKL_AST_TYPE ((yyvsp[-2].ast)))
                    PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[-2].ast))) = (yylsp[-2]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8183 "pkl-tab.c"
    break;

  case 282: /* funcall_stmt: primary funcall_stmt_arg_list  */
#line 2656 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_funcall (pkl_parser->ast,
                                             (yyvsp[-1].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8193 "pkl-tab.c"
    break;

  case 284: /* funcall_stmt_arg_list: funcall_stmt_arg_list funcall_stmt_arg  */
#line 2666 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast));
                }
#line 8201 "pkl-tab.c"
    break;

  case 285: /* funcall_stmt_arg: ':' "identifier" expression  */
#line 2673 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_funcall_arg (pkl_parser->ast,
                                                 (yyvsp[0].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyvsp[-1].ast)) = (yylsp[-1]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8212 "pkl-tab.c"
    break;


#line 8216 "pkl-tab.c"

        default: break;
      }
    if (yychar_backup != yychar)
      YY_LAC_DISCARD ("yychar change");
  }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == PKL_TAB_EMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      {
        yypcontext_t yyctx
          = {yyssp, yyesa, &yyes, &yyes_capacity, yytoken, &yylloc};
        if (yychar != PKL_TAB_EMPTY)
          YY_LAC_ESTABLISH;
        if (yyreport_syntax_error (&yyctx, pkl_parser) == 2)
          YYNOMEM;
      }
    }

  yyerror_range[1] = yylloc;
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= PKL_TAB_EOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == PKL_TAB_EOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, pkl_parser);
          yychar = PKL_TAB_EMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, yylsp, pkl_parser);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  /* If the stack popping above didn't lose the initial context for the
     current lookahead token, the shift below will for sure.  */
  YY_LAC_DISCARD ("error recovery");

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  ++yylsp;
  YYLLOC_DEFAULT (*yylsp, yyerror_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, pkl_parser, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != PKL_TAB_EMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, pkl_parser);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, yylsp, pkl_parser);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  if (yyes != yyesa)
    YYSTACK_FREE (yyes);

  return yyresult;
}

#line 2712 "pkl-tab.y"


/* Handle syntax errors.  */

int
yyreport_syntax_error (const yypcontext_t *ctx,
                       struct pkl_parser *pkl_parser)
{
  int res = 0;
  yysymbol_kind_t lookahead = yypcontext_token (ctx);

  /* if the unexpected token is alien, then report
     pkl_parser->alien_err_msg.  */
  if (lookahead == YYSYMBOL_ALIEN)
    {
      pkl_tab_error (yypcontext_location (ctx),
                     pkl_parser,
                     pkl_parser->alien_errmsg);
      free (pkl_parser->alien_errmsg);
      pkl_parser->alien_errmsg = NULL;
    }
  else
    {
      /* report tokens expected at this point.  */
      yysymbol_kind_t expected[YYNTOKENS];
      int nexpected = yypcontext_expected_tokens (ctx, expected, YYNTOKENS);

      if (nexpected < 0)
        /* forward errors to yyparse.  */
        res = nexpected;
      else
        {
          char *errmsg = strdup ("syntax error");

          if (!errmsg)
            return YYENOMEM;

          if (lookahead != YYSYMBOL_YYEMPTY)
            {
              char *tmp = pk_str_concat (errmsg,
                                         ": unexpected ",
                                         yysymbol_name (lookahead),
                                         NULL);
              free (errmsg);
              if (!tmp)
                return YYENOMEM;
              errmsg = tmp;
            }

          pkl_tab_error (yypcontext_location (ctx), pkl_parser, errmsg);
          free (errmsg);
        }
    }

  return res;
}
